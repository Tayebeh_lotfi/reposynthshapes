% % % Here is an example of using nlinfit(). For simplicity, none of
% % % of the fitted parameters are actually nonlinear!
% % % Define the data to be fit
% % x=(0:1:10)'; % Explanatory variable
% % y = 5 + 3*x + 7*x.^2; % Response variable (if response were perfect)
% % y = y + 20*randn((size(x)));% Add some noise to response variable
% % % Define function that will be used to fit data
% % % (F is a vector of fitting parameters)
indx = 100; indy = 100;
% x = dispvec(:,1);
% y = dispvec(:,2);
x = dispvec;
p = pbs(indx,indy,:);
f = @(F,x,z) F(1) ./ ( F(2) + exp( F(3).* x(:,1) )* exp( F(4).* y ) ) + F(5) ./ ( F(6) + exp( F(7).* x(:,1) )* exp( F(8).* y ) );
beta0 = ones(1,8);
F_fitted = nlinfit(x,p,f,beta0);
% Display fitted coefficients
disp(['F = ',num2str(F_fitted)])
% Plot the data and fit
figure(1)
plot(x,y,'*',x,f(F_fitted,x),'g');
legend('data','fit')