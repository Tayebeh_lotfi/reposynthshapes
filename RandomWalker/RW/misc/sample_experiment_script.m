

function pbs_script( Y, maxT )

max_RW_iteration = 20;
N_MAX_USER_FEEDBACK= 30; % consider this is = total clicks we ask from user

for n=1:ntrials 
  % loop over images, where src+tar is corrupted at noise level Y, and differ by warp of maximum displacement maxT  
  src=create_pair( tar, Y, maxT )
  
  for method={'random', 'sl', 'al'}
    for lambda = [l1,l2]
     for gamma = [g1,g2]

       % algm_parameters is adjusted according to gamma, lambda
       
       switch method

           case 'random'
                    e=RWreg( algm_parameters, t )
          case 'sl'
               for cert_nstd=[.5,.75 ,1]                   
                   % algm_parameters is adjusted accordingly
                   e=RWreg( algm_parameters, t )
               end
          case 'al'
              for nseeds=[N_MAX_USER_FEEDBACK, N_MAX_USER_FEEDBACK / max_RW_iteration ]
                   t = MAX_USER_FEEDBACK/ nseeds;
                   e=RWreg( algm_parameters, t )
              end
       end
       errors( a,b,c,d) = e; %save the err here
end
end
end
end

