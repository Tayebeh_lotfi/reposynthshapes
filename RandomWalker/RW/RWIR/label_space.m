function label_space(Lbl_mod, MCase, img_num)
%% Creating Data
add_paths()

%%
Visualization = 0; 
real_mode = 2; synth = 0; test3D = 0;
[debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%Lbl_mod: % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT 
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05; trial_num = 1;
% load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);
% src3D = resc(src3D);
% tar3D = resc(tar3D);
load ('../data/native_slices_ax2_41');
%%
if (MCase == 1)
    load '../data/Bspline.mat';
else
    load '../data/Diffusion.mat';
end
if(nargin<3),
    sze = [256 256];
    [m n] = ndgrid(1: sze(1), 1: sze(2));
    for img_num = 1:size(Im, 3),    
        if(MCase == 1)
            spacing.height = 10;
            spacing.width = 10;
            magnitude = 5;
            I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
            [IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], magnitude );
            D(:,:,1) = TxW; D(:,:,2) = TyW;
            invD = invertDefField(D);
            invTxW = invD(:,:,1); invTyW = invD(:,:,2);
            meanwarpb = mean(invTxW(:) .^2 + invTyW(:) .^2);
        else
            I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
            IWF = interp2(n, m, I, n + invTyW, m + invTxW, '*linear');
            meanwarpd = mean(invTxW(:) .^2 + invTyW(:) .^2);
        end      
        tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
        src_seg = interp2(n, m, tar_seg, n + invTyW, m + invTxW, '*linear');
        mask = src_seg>0.1;
        noise_sigma = 0.005;        
%% Creating Landmarks & Data
        distx = 1; disty = 1;
        tlands(:,1) = [distx; disty]; 
        tlands(:,2) = [distx; sze(2)-disty]; 
        tlands(:,3) = [sze(1)-distx; disty]; 
        tlands(:,4) = [sze(1)-distx; sze(2)-disty];
        slands = tlands;
        src = mat2gray(IWF); tar = mat2gray(I); 
        Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
        num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
        [points edges]=lattice( sze(1), sze(2), 0);
        slands_pre2D = slands; tlands_pre = tlands;
        Max_seeds = num_seed*Max_Iterations;
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod);
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
        slands = slands_pre2D; 
        tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
        nseeds = size(tlands,2); 
%% Solving the problem
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, sze, alpha, beta, gamma, dterm_mode, slands);
%% Calculating warped segment
        Warp_Err_Mean(img_num) = mean( sqrt(  (dx(mask) - invTx(mask)).^2 + (dy(mask) - invTy(mask)).^2 ) ); 
        Warp_Err_Std(img_num) = std( sqrt(  (dx(mask) - invTx(mask)).^2 + (dy(mask) - invTy(mask)).^2 ) ); 
        Warp_Err_Sprd_Mean(img_num) = mean( sqrt(  (dx_sprd(mask) - invTx(mask)).^2 + (dy_sprd(mask) - invTy(mask)).^2 )); 
        Warp_Err_Sprd_Std(img_num) = std( sqrt(  (dx_sprd(mask) - invTx(mask)).^2 + (dy_sprd(mask) - invTy(mask)).^2 )); 
    end 
    if(MCase == 1)
        save (['../results/err_label_space_b-spline', num2str(Lbl_mod), '.mat'], 'Warp_Err_Mean', 'Warp_Err_Std', 'Warp_Err_Sprd_Mean', 'Warp_Err_Sprd_Std'); 
    else
        save (['../results/err_label_space_diff', num2str(Lbl_mod), '.mat'], 'Warp_Err_Mean', 'Warp_Err_Std', 'Warp_Err_Sprd_Mean', 'Warp_Err_Sprd_Std'); 
    end
else
    sze = [100 100];
    [m n] = ndgrid(1: sze(1), 1: sze(2));    
    if(MCase == 1)
        spacing.height = 10;
        spacing.width = 10;
        magnitude = 4;
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        [IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], magnitude );
        D(:,:,1) = TxW; D(:,:,2) = TyW;
        invD = invertDefField(D);
        invTxW = invD(:,:,1); invTyW = invD(:,:,2);
        meanwarpb = mean(invTxW(:) .^2 + invTyW(:) .^2);
    else
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        invTyW = imresize(invTyW, [sze(1) sze(2)]);
        invTxW = imresize(invTxW, [sze(1) sze(2)]);
        TyW = imresize(TyW, [sze(1) sze(2)]);
        TxW = imresize(TxW, [sze(1) sze(2)]);
        invTxW = 0.8*invTxW;
        invTyW = 0.8*invTyW;
        IWF = interp2(n, m, I, n + invTyW, m + invTxW, '*linear');
        meanwarpd = mean(invTxW(:) .^2 + invTyW(:) .^2);
    end
    tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
    src_seg = interp2(n, m, tar_seg, n + invTyW, m + invTxW, '*linear');
    mask = src_seg>0.1;
    noise_sigma = 0.005;
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    src = mat2gray(IWF); tar = mat2gray(I); 
    Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    [points edges]=lattice( sze(1), sze(2), 0);
    slands_pre2D = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
%% Calculating the likelihood
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    slands = slands_pre2D; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, sze, alpha, beta, dterm_mode, slands);
%% Calculating Uncertainty    
    [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode); 
    Uncert_Mode = 2; % 0: KNN, 1: All, 2: KNN&KDE
    epsilon_ = 10e-4; accuracy=3; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    nlabs_size = floor(sqrt(size(dispvec,1)-1));
    data_mode = 2;
    [Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc GMM2_l GMM2_u GMM2_avg GMM2_disc ...
    GMM3_l GMM3_u GMM3_avg GMM3_disc Unc_Kde Unc_Knn time] = ...
    Calc_Uncertainty_MEX(pbs, dispvec, sze, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
    file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'Unc_Kde', 'Unc_Knn', 'dx_sprd', 'dy_sprd', 'dx', 'dy', 'invTx', 'invTy', 'time'};
    for i = 1:13,
        if(MCase == 1)
            save (num2str([savepath, 'uncert_warp_all/', file_name{i}, '_bspline', 'img_', num2str(img_num), '.mat']), file_name{i}); 
        else
            save (num2str([savepath, 'uncert_warp_all/', file_name{i}, '_diff', 'img_', num2str(img_num), '.mat']), file_name{i}); 
        end
    end
end
%% Construct the matrix for the labels
% for Lbl_mod = 1:5,
%     load (['../results/err_label_space_bsp', num2str(Lbl_mod)]);
%     Warp_ErrM(:, Lbl_mod) = Warp_Err_Mean;
%     Warp_ErrS(:, Lbl_mod) = Warp_Err_Std;
%     Warp_ErrM_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Mean;
%     Warp_ErrS_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Std;
% end
% figure, errorbar(mean(Warp_ErrM,1), std(Warp_ErrM,1), 'r-*'), title('Origial Error');
% figure, errorbar(mean(Warp_ErrM_Sprd,1), std(Warp_ErrM_Sprd,1), 'r-*'), title('WProb Error');
%% Figures
% load '../data/Bspline.mat';
% invTyWb = invTyW;
% invTxWb = invTxW;
% load '../data/Diffusion.mat';
% invTyWd = invTyW;
% invTxWd = invTxW;    
% for img_num = 1:size(Im, 3),
%         Imf(:,:,img_num) = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
%         Imwb(:,:,img_num) = interp2(n, m, Imf(:,:,img_num), n + invTyWb, m + invTxWb, '*linear');
%         Imwd(:,:,img_num) = interp2(n, m, Imf(:,:,img_num), n + invTyWd, m + invTxWd, '*linear');
% end
% meanwarpb = mean(invTxWb(:) .^2 + invTyWb(:) .^2);
% meanwarpd = mean(invTxWd(:) .^2 + invTyWd(:) .^2);
% elnm = 6; j = 1;
% figure, subplot 131, montage(reshape(mat2gray(Imf(:,:,(j-1)*elnm+1:j*elnm)),[size(Imf,1)  size(Imf,2) 1  elnm]  )),
% colormap(gray), 
% A1 = title('Fixed');
% LEG = findobj(A1,'type','text');
% set(LEG,'FontSize',25)
% 
%         
% subplot 132, montage(reshape(mat2gray(Imwb(:,:,(j-1)*elnm+1:j*elnm)),[size(Imwb,1)  size(Imwb,2) 1  elnm]  )),
% colormap(gray), 
% A2 = title(['Moving Bsp, Err: ', num2str(meanwarpb)]);
% LEG = findobj(A2,'type','text');
% set(LEG,'FontSize',25)
% 
% subplot 133, montage(reshape(mat2gray(Imwd(:,:,(j-1)*elnm+1:j*elnm)),[size(Imwd,1)  size(Imwd,2) 1  elnm]  )),
% colormap(gray), 
% A2 = title(['Moving Dif, Err: ', num2str(meanwarpd)]);
% LEG = findobj(A2,'type','text');
% set(LEG,'FontSize',25)

%% Error of different Label space
% c = jet(120);
% shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
% 
% Str = {'Uni Cart';
%     'Non-uni polar';
%     'Uni polar';
%     'Dsmple GT';
%     'VQ'};
% for Lbl_mod = 1:5,
%     load (['../results/err_labelspace/err_label_space_bsp', num2str(Lbl_mod)]);
%     Warp_ErrM(:, Lbl_mod) = Warp_Err_Mean;
%     Warp_ErrS(:, Lbl_mod) = Warp_Err_Std;
%     Warp_ErrM_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Mean;
%     Warp_ErrS_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Std;
% end
% figure, 
% errorbar(mean(Warp_ErrM,1), std(Warp_ErrM,1), 'Color', c(17,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), 
% set(gca,'XLim',[0.5 5.5])
% set(gca,'XTick',1:5)
% set(gca,'XTickLabel', Str), 
% hold on, errorbar(mean(Warp_ErrM_Sprd,1), std(Warp_ErrM_Sprd,1), 'Color', c(38,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), 
% set(gca,'XLim',[0.5 5.5])
% set(gca,'XTick',1:5)
% set(gca,'XTickLabel', Str), 
% set(gca,'FontSize',25)
% 
% % 
% for Lbl_mod = 1:5,
%     load (['../results/err_labelspace/err_label_space_diff', num2str(Lbl_mod)]);
%     Warp_ErrM(:, Lbl_mod) = Warp_Err_Mean;
%     Warp_ErrS(:, Lbl_mod) = Warp_Err_Std;
%     Warp_ErrM_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Mean;
%     Warp_ErrS_Sprd(:, Lbl_mod) = Warp_Err_Sprd_Std;
% end
% hold on, errorbar(mean(Warp_ErrM,1), std(Warp_ErrM,1), 'Color', c(51,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), 
% set(gca,'XLim',[0.5 5.5])
% set(gca,'XTick',1:5)
% set(gca,'XTickLabel', Str), 
% hold on, errorbar(mean(Warp_ErrM_Sprd,1), std(Warp_ErrM_Sprd,1), 'Color', c(68,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), 
% set(gca,'XLim',[0.5 5.5])
% set(gca,'XTick',1:5)
% set(gca,'XTickLabel', Str), 
% set(gca,'FontSize',25)
% ylabel('Error');
% title('Error of Different Warpings'); 
% AX = legend('Origial Err bsp', 'WProb Err bsp', 'Origial Err dif','WProb Err dif','Location','SouthEastOutSide');
% LEG = findobj(AX,'type','text');
% set(LEG,'FontSize',25)

%% Save the whole matrix
% file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'Unc_Kde', 'Unc_Knn', 'dx_sprd', 'dy_sprd', 'dx', 'dy', 'invTx', 'invTy'};
% savepath = '../results/';
% for MCase = 1:2,
%     for img_num = 1:5,
%         for i = 1:12,
%             if(MCase == 1)
%                 load (num2str([savepath 'uncert_warp_all/' file_name{i}, '_bspline', 'img_', num2str(img_num), '.mat'])); 
%             else
%                 load (num2str([savepath 'uncert_warp_all/' file_name{i}, '_diff', 'img_', num2str(img_num), '.mat'])); 
%             end
%         end
%         if(MCase == 1)
%             Shannon_Vec_bsp(:,:,img_num) = Shannon;
%             Shannon_Sprd_Vec_bsp(:,:,img_num) = Shannon_Sprd;
%             src_Vec_bsp(:,:,img_num) = src;
%             tar_Vec_bsp(:,:,img_num) = tar;
%             Unc_Kde_Vec_bsp(:,:,img_num) = Unc_Kde;
%             Unc_Knn_Vec_bsp(:,:,img_num) = Unc_Knn;
%             dx_Vec_bsp(:,:,img_num) = dx;
%             dy_Vec_bsp(:,:,img_num) = dy;
%             dx_sprd_Vec_bsp(:,:,img_num) = dx_sprd;
%             dy_sprd_Vec_bsp(:,:,img_num) = dy_sprd;
%             invTx_Vec_bsp(:,:,img_num) = invTx;
%             invTy_Vec_bsp(:,:,img_num) = invTy;
%         else
%             Shannon_Vec_dif(:,:,img_num) = Shannon;
%             Shannon_Sprd_Vec_dif(:,:,img_num) = Shannon_Sprd;
%             src_Vec_dif(:,:,img_num) = src;
%             tar_Vec_dif(:,:,img_num) = tar;
%             Unc_Kde_Vec_dif(:,:,img_num) = Unc_Kde;
%             Unc_Knn_Vec_dif(:,:,img_num) = Unc_Knn;
%             dx_Vec_dif(:,:,img_num) = dx;
%             dy_Vec_dif(:,:,img_num) = dy;
%             dx_sprd_Vec_dif(:,:,img_num) = dx_sprd;
%             dy_sprd_Vec_dif(:,:,img_num) = dy_sprd;
%             invTx_Vec_dif(:,:,img_num) = invTx;
%             invTy_Vec_dif(:,:,img_num) = invTy;
%         end
%     end
%     if(MCase == 1)
%       save (num2str([savepath 'uncert_warp_all/' 'uncert_bspline', '.mat']), 'Shannon_Vec_bsp', 'Shannon_Sprd_Vec_bsp', 'src_Vec_bsp', 'tar_Vec_bsp', ...
%           'Unc_Kde_Vec_bsp', 'Unc_Knn_Vec_bsp', 'dx_Vec_bsp', 'dy_Vec_bsp', 'dx_sprd_Vec_bsp', 'dy_sprd_Vec_bsp', ...
%           'invTx_Vec_bsp', 'invTy_Vec_bsp'); 
%     else
%       save (num2str([savepath 'uncert_warp_all/' 'uncert_diffusion', '.mat']), 'Shannon_Vec_dif', 'Shannon_Sprd_Vec_dif', 'src_Vec_dif', 'tar_Vec_dif', ...
%           'Unc_Kde_Vec_dif', 'Unc_Knn_Vec_dif', 'dx_Vec_dif', 'dy_Vec_dif', 'dx_sprd_Vec_dif', 'dy_sprd_Vec_dif', ...
%           'invTx_Vec_dif', 'invTy_Vec_dif'); 
%     end
% end
% for img = 1:4,
%     mask = mat2gray(tar_Vec_dif(:,:,1)) > 0.1;
% 
%     Warp_Err = sqrt((dx_Vec_dif(:,:,img) - invTx_Vec_dif(:,:,img)).^2 + (dx_Vec_dif(:,:,img) - invTy_Vec_dif(:,:,img)).^2 );
%     Warp_Err_Sprd = sqrt((dx_sprd_Vec_dif(:,:,img) - invTx_Vec_dif(:,:,img)).^2 + (dx_sprd_Vec_dif(:,:,img) - invTy_Vec_dif(:,:,img)).^2 );
% 
% %     Stat_Err_Unc(Shannon_Vec_dif(:,:,img), Shannon_Sprd_Vec_dif(:,:,img), Warp_Err, Warp_Err_Sprd, mask)
% %     Stat_Err_Unc(Shannon_Vec_dif(:,:,img), Unc_Kde_Vec_dif(:,:,img), Warp_Err, Warp_Err_Sprd, mask)
%     Stat_Err_Unc(Shannon_Vec_dif(:,:,img), Unc_Knn_Vec_dif(:,:,img), Warp_Err, Warp_Err_Sprd, mask)
% end
end    