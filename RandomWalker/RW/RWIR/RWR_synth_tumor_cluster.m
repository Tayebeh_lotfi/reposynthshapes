function RWR_synth_tumor_cluster(Mode, MCase)
    add_paths()
    [ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
    noise_sigma = 0.005; 
    Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
    simil_measure = 5; %MIND

if (MCase < 6)
    switch MCase
        case 1
            I = phantom('Modified Shepp-Logan',100);
            IWF = phantom('Modified Shepp-Logan',100);
            IWF = Circle_Artfct(IWF, 50, 50, 7);
            intx = 50; inty = 50; rad = 40; m = size(I);
            [rr cc] = ndgrid(1:m(1), 1:m(2));
            mask = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
        case 2
            load ../data/tumor_data/tumor.mat;
            I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);%mat2gray(I); IWF = mat2gray(IWF);
            mask = IWF>0.1;
%             I = imresize(mat2gray(rgb2gray(imread('../data/test_img1.jpg'))),[50 50], 'bilinear');
%             IWF = imresize(mat2gray(rgb2gray(imread('../data/test_img2.jpg'))),size(I), 'bilinear');
        case 3
%             load('../data/real_img_w_seg.mat');
            load('../data/real_case');
            I = mat2gray(imresize(I, [100 100])); IWF = mat2gray(imresize(IWF, [100 100]));
            mask = IWF>0.1;
            IWF = Circle_Artfct(IWF, 25, 65, 10);
%             IWF(20:25,20:25) = 1;
        case 4
            load ('../data/native_slices_ax2_41');
            sze = [100 100]; img_num = 1;
            [m n] = ndgrid(1: sze(1), 1: sze(2));    
            spacing.height = 10;
            spacing.width = 10;
            magnitude = 4;
            I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
            [IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], magnitude );
            D(:,:,1) = TxW; D(:,:,2) = TyW;
            invD = invertDefField(D);
            invTxW = invD(:,:,1); invTyW = invD(:,:,2);
            tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
            src_seg = interp2(n, m, tar_seg, n + invTyW, m + invTxW, '*linear');
            mask = src_seg>0.1;
        case 5
            load ('../data/native_slices_ax2_41');
            sze = [100 100]; img_num = 1;
            [m n] = ndgrid(1: sze(1), 1: sze(2));
            I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
            invTyW = imresize(invTyW, [sze(1) sze(2)]);
            invTxW = imresize(invTxW, [sze(1) sze(2)]);
            TyW = imresize(TyW, [sze(1) sze(2)]);
            TxW = imresize(TxW, [sze(1) sze(2)]);
            invTxW = 0.8*invTxW;
            invTyW = 0.8*invTyW;
            IWF = interp2(n, m, I, n + invTyW, m + invTxW, '*linear');
            tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
            src_seg = interp2(n, m, tar_seg, n + invTyW, m + invTxW, '*linear');
            mask = src_seg>0.1;
            noise_sigma = 0.005;
            
%     load ../data/Bspline.mat;
%             I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);
%             I = mat2gray(I); IWF = mat2gray(IWF);
%             mask = IWF>0.1;
%             tar_seg = seg;
%         case 5
%             load '../data/Diffusion.mat';
%             I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);
%             I = mat2gray(I); IWF = mat2gray(IWF);
%             mask = IWF>0.1;
%             tar_seg = seg;    
    end
    noise_sigma = 0.005;
    IWF = imnoise( IWF, 'gaussian', 0, (Mode-1) * noise_sigma );
else
    hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05; trial_num = 1;
    load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);
    noise_sigma = 0.01;
    src3D = resc(src3D);
    tar3D = resc(tar3D);
%% Starting the Algorithm
    t = 5; s = 20; I = tar3D(:,:,t); IWF = tar3D(:,:,s); 
    src_seg = segment3D(:,:,s); tar_seg = segment3D(:,:,t);
    I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);
    src_seg = imresize(src_seg, [100 100]); tar_seg = imresize(tar_seg, [100 100]);
    mask = IWF>0.1;
    mask1 = ( src_seg > 0.1); se = strel('disk',15); mask1 = imdilate(mask1,se);
    mask1 = mask1 .* mask;
    b=7; h = fspecial('average', b); homg = imfilter(IWF,h, 'same'); mask2 = mat2gray(abs(IWF - homg));
    mask2 = mask2 .* mask;
    switch(Mode)
        case 2
            IWF = Circle_Artfct(IWF, 60, 65, 3);
        case {3,4,5,6,7,8,9,10,11,12} % Adding Crop + Noise to Datasets
            IWF = Circle_Artfct(IWF, 60, 65, 3);
            IWF = imnoise( IWF, 'gaussian', 0, (Mode-2) * noise_sigma );
        case 14
            IWF = Circle_Artfct(IWF, 60, 65, 5);
        case {15,16,17,18,19,20,21,22,23,24} % Adding Crop + Noise to Datasets
            IWF = Circle_Artfct(IWF, 60, 65, 5);
            IWF = imnoise( IWF, 'gaussian', 0, (Mode-14) * noise_sigma );
        case 26
            IWF = Circle_Artfct(IWF, 60, 65, 10);
        case {27,28,29,30,31,32,33,34,35,36} % Adding Crop + Noise to Datasets
            IWF = Circle_Artfct(IWF, 60, 65, 10);
            IWF = imnoise( IWF, 'gaussian', 0, (Mode-26) * noise_sigma );
        case {38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48} % Adding Noise to Datasets
            IWF = imnoise( IWF, 'gaussian', 0, (Mode-37) * noise_sigma );
    end
end
if (MCase ~= 4 && MCase ~= 5)
    TxW = 10*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
end
    %% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
%% Calculating the likelihood
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    slands = slands_pre; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
    
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
%% Shannon's Entropy
    [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2); 
%     mask = ones(isz);
if(MCase < 6)
    Uncert_Mode = 2; % 0: KNN, 1: KNN&WCov, 2: KNN&KDE&WCov,  3: All,
    epsilon_ = 10e-5; accuracy=4; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    nlabs_size = floor(sqrt(size(dispvec,1)-1));
    data_mode = 2;
    w = [3.3 2.2]; 
    [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
        pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
%     [time.delta Unc_Delta] = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
    [Unc_Err_Dist Unc_Err_Dist_Map] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);
%% Saving Results
    file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd', 'Unc_Kde', 'Unc_Knn', 'Unc_Err_Dist_Map', 'Unc_Err_Dist', 'dx_sprd', 'dy_sprd', 'dx', 'dy', 'time' };    
    % 'Unc_Delta', 
    for i = 1:15,
%         save (num2str([savepath 'Uncert_all_tumor_noise/uncert_all_tumor/' file_name{i} '_' num2str(Mode) '_Case' num2str(MCase) '.mat']), file_name{i}); 
        save (num2str([savepath 'Uncert_all_tumor_noise/uncert_all_noise/' file_name{i} '_' num2str(Mode) '_Case' num2str(MCase) '.mat']), file_name{i}); 

    end
else
    [mx my] = ndgrid(1:isz(1), 1:isz(2));
    dsrc_seg_sprd = interp2( my, mx, src_seg, my + dy_sprd, mx + dx_sprd,  '*linear', 0); % nearest
    dsrc_seg = interp2( my, mx, src_seg, my + dy, mx + dx,  '*linear', 0); % nearest
%% Segment Errors
    Err_bef_seg = sqrt( sum( (src_seg(:)-tar_seg(:)) .^2 ) );
    Err_aft_seg = sqrt( sum( (dsrc_seg(:)-tar_seg(:)) .^2 ) );
    Err_aft_seg_sprd = sqrt( sum( (dsrc_seg_sprd(:)-tar_seg(:)) .^2 ) );
    Err_bef = sqrt( sum( (src(:)-tar(:)) .^2 ) );
    Err_aft = sqrt( sum( (dsrc(:)-tar(:)) .^2 ) );
    Err_aft_sprd = sqrt( sum( (dsrc_sprd(:)-tar(:)) .^2 ) );

    file_name = { 'Shannon', 'Shannon_Sprd', 'dsrc_seg', 'src', 'tar', 'dsrc_sprd', 'dsrc', 'src_seg', 'tar_seg', ...
            'Err_bef_seg', 'Err_aft_seg', 'Err_aft_seg_sprd', 'Err_aft', 'Err_aft_sprd', 'Err_bef' };    
    for i = 1:15,
        save (num2str([savepath 'Uncert_all_tumor_case3_new/' file_name{i} '_' num2str(Mode)  '_Case' num2str(MCase) '.mat']), file_name{i}); 
%         save (num2str([savepath 'Uncert_all_tumor_noise/uncert_all_tumor/' file_name{i} '_' num2str(Mode)  '_Case' num2str(MCase) '.mat']), file_name{i}); 
    end
end
%% %% Save the results in one .mat file
%   ldpath = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\Uncert_all_tumor_noise\uncert_all_tumor\';
%   
%   for MCase = 1:5,
%     switch MCase
%      case {1,2,3,4,5}
%         file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd', 'Unc_Kde', 'Unc_Knn', 'time'};    
%         for Mode = 1:48,
%           for i = 1:9,
%               load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase), '.mat'])); 
%           end
%           Shannon_Vec(:,:,Mode) = Shannon;
%           Shannon_Sprd_Vec(:,:,Mode) = Shannon_Sprd;
%           src_Vec(:,:,Mode) = src;
%           tar_Vec = tar;
%           dsrc_Vec(:,:,Mode) = dsrc;
%           dsrc_sprd_Vec(:,:,Mode) = dsrc_sprd;
%           Knn_Vec(:,:,Mode) = Unc_Knn;
%           Kde_Vec(:,:,Mode) = Unc_Kde;
%           time_Vec(Mode) = time;
%         end
%         save (['../results/uncert_real', '_Case', num2str(MCase), '.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
%           'src_Vec', 'tar_Vec', 'dsrc_Vec', 'dsrc_sprd_Vec', 'Knn_Vec', 'Kde_Vec', 'time_Vec');
%       case 6
%          file_name = { 'Shannon', 'Shannon_Sprd', 'dsrc_seg', 'src', 'tar', 'dsrc_sprd', 'dsrc', 'src_seg', 'tar_seg', ...
%             'Err_bef_seg', 'Err_aft_seg', 'Err_aft_seg_sprd', 'Err_aft', 'Err_aft_sprd', 'Err_bef' };
%          for Mode = 1:48,
%             for i = 1:15,
%               load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase) '.mat'])); 
%             end
%             Shannon_Vec(:,:,Mode) = Shannon;
%             Shannon_Sprd_Vec(:,:,Mode) = Shannon_Sprd;
%             dsrc_seg_Vec(:,:,Mode) = dsrc_seg;
%             src_seg_Vec = src_seg;
%             tar_seg_Vec = tar_seg;
%             src_Vec(:,:,Mode) = src;
%             tar_Vec = tar;
%             dsrc_Vec(:,:,Mode) = dsrc;
%             dsrc_sprd_Vec(:,:,Mode) = dsrc_sprd;
%             Err_bef_seg_Vec(Mode) = Err_bef_seg;
%             Err_bef_Vec(Mode) = Err_bef;
%             Err_aft_seg_Vec(Mode) = Err_aft_seg;
%             Err_aft_seg_sprd_Vec(Mode) = Err_aft_seg_sprd;
%             Err_aft_Vec(Mode) = Err_aft;
%             Err_aft_sprd_Vec(Mode) = Err_aft_sprd;
%          end
%          save (['../results/real_case_synth_tumor_' num2str(MCase) '.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
%            'dsrc_seg_Vec', 'src_seg_Vec', 'tar_seg_Vec', 'src_Vec', 'tar_Vec', 'dsrc_Vec', ...
%            'Err_bef_seg_Vec', 'Err_bef_Vec', 'Err_aft_seg_Vec', 'Err_aft_Vec');
%     end
%   end
%% %% Visualize Results case 1-4
%     intx = 50; inty = 50; rad = 25; m = size(src_Vec);
%     [rr cc] = ndgrid(1:m(1), 1:m(2));
%     mask1 = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
%     mask2 = src_Vec(:,:,1)>0.1;
%     mask = mask2; % mask1 | mask2; % 
%     tar_Vec = tar_Vec .* mask; % mat2gray(tar_Vec) .* mask;
%     elnm = 10; j =1;
%     src_Vec = mat2gray(src_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_Vec = mat2gray(dsrc_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_sprd_Vec = mat2gray(dsrc_sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     Shannon_Vec = mat2gray(Shannon_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     Shannon_Sprd_Vec = mat2gray(Shannon_Sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     Kde_Vec = mat2gray(Kde_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     Knn_Vec = mat2gray(Knn_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%         dockf; montage(reshape(mat2gray(repmat(tar_Vec, [1 1 elnm])),[size(tar_Vec,1)  size(tar_Vec,2) 1  elnm]  )),
%         colormap(jet),  title('fixed Image'), cbar
%         dockf; montage(reshape(mat2gray(src_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Moving Image'), cbar
%         dockf; montage(reshape(mat2gray(dsrc_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Warped Image'), cbar
%         dockf; montage(reshape(mat2gray(dsrc_sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Modfd Warped Image'), cbar
%         dockf; montage(reshape(mat2gray(Shannon_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Shannon Entropy'), cbar
%         dockf; montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Modified Shannon Entropy'), cbar
%         dockf; montage(reshape(mat2gray(Kde_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Kde_Vec,1)  size(Kde_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Kde'), cbar
%         dockf; montage(reshape(mat2gray(Knn_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Knn_Vec,1)  size(Knn_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Knn'), cbar

%% Visualize Results case 5
%    for j = 1:4, 
%         dockf; montage(reshape(mat2gray(repmat(tar_Vec, [1 1 12])),[size(tar_Vec,1)  size(tar_Vec,2) 1  12]  )),
%         colormap(jet),  title('fixed Image')
%         dockf; montage(reshape(mat2gray(src_Vec(:,:,(j-1)*12+1:j*12)),[size(src_Vec,1)  size(src_Vec,2) 1  12]  )),
%         colormap(jet), title('Moving Image'),
%         dockf; montage(reshape(mat2gray(dsrc_Vec(:,:,(j-1)*12+1:j*12)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  12]  )),
%         colormap(jet), title('Warped Image'),
%         dockf; montage(reshape(mat2gray(repmat(tar_seg_Vec, [1 1 12])),[size(tar_seg_Vec,1)  size(tar_seg_Vec,2) 1  12]  )),
%         colormap(jet),  title('Fixed Image Segment')
%         dockf; montage(reshape(mat2gray(repmat(src_seg_Vec, [1 1 12])),[size(src_seg_Vec,1)  size(src_seg_Vec,2) 1  12]  )),
%         colormap(jet),  title('Moving Image Segment')
% 
%         dockf; montage(reshape(mat2gray(dsrc_seg_Vec(:,:,(j-1)*12+1:j*12)),[size(dsrc_seg_Vec,1)  size(dsrc_seg_Vec,2) 1  12]  )),
%         colormap(jet), title('Warped Image Segment'),
%         dockf; montage(reshape(mat2gray(Shannon_Vec(:,:,(j-1)*12+1:j*12)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  12]  )),
%         colormap(jet), title('Shannon Entropy'),
%         dockf; montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,(j-1)*12+1:j*12)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  12]  )),
%         colormap(jet), title('Modified Shannon Entropy'),
% 
%       
%         dockf;
%         plot( Err_bef_seg_Vec((j-1)*12+1:j*12), '-or' ), hold on
%         plot( Err_aft_seg_Vec((j-1)*12+1:j*12), '-vb' ), hold on
%         title( 'TRE Registration Error' ), xlabel('Modes'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
%         legend('TRE Before', 'TRE After', 'Location', 'SouthEastOutside'),
%         dockf;
%         plot( Err_bef_Vec((j-1)*12+1:j*12), '-*g' ), hold on
%         plot( Err_aft_Vec((j-1)*12+1:j*12), '-ok' ), hold on
%         title( 'SSD Registration Error' ), xlabel('Modes'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
%         legend('Image SSD Before', 'Image SSD After', 'Location', 'SouthEastOutside'),
%     end
%% Second Visualization
% %% Visualizing Data
% % close all;
%     dockf; 
%     subplot 221, imagesc(tar); title('target image'),colormap(gray)
%     subplot 222, imagesc(src); title('source image'),colormap(gray)
%     subplot 223, imagesc(dsrc), colormap(gray), title('Warped Moving Image');
%     subplot 224, imagesc(dsrc_sprd), colormap(gray), title('Warped Moving Image Modfd');
%     dockf; 
%     plot(dispvec(:,2), dispvec(:,1), 'b.'), xlim([mn(1)-epsln mn(2)+epsln]),ylim([mn(1)-epsln mn(2)+epsln]), title('Labelset');
%     dockf; 
%     subplot 131, imagesc(abs(src-tar)), title(['Orig. Diff., Err = ', num2str(Err_bef)]), cbar;
%     subplot 132, imagesc(abs(dsrc-tar)), title(['Aft. Reg. Diff., Err = ', num2str(Err_aft)]), cbar;
%     subplot 133, imagesc(abs(dsrc_sprd-tar)), title(['Aft. Reg. Diff. Modfd, Err = ', num2str(Err_aft_sprd)]), cbar;
%     dockf; 
%     subplot 131, imagesc(tar_seg), title('Fixed Segment');
%     subplot 132,  imagesc(src_seg), title('Moving Segment');
%     subplot 133, imagesc(dsrc_seg_sprd), title('Warped Moving Segment');
%     dockf;
%     subplot 131, imagesc(abs(src_seg-tar_seg)), title(['Orig. Diff. Seg., Err = ', num2str(Err_bef_seg)]), cbar;
%     subplot 132, imagesc(abs(dsrc_seg-tar_seg)), title(['Aft. Reg. Diff. Seg., Err = ', num2str(Err_aft_seg)]), cbar;
%     subplot 133, imagesc(abs(dsrc_seg_sprd-tar_seg)), title(['Aft. Reg. Diff. modfd Seg., Err = ', num2str(Err_aft_seg_sprd)]), cbar;
%     
%     dockf; 
%     subplot 121, imagesc(mat2gray(mat2gray(Shannon) .* mask1))
%     subplot 122, imagesc(mat2gray(mat2gray(Shannon_Sprd) .* mask1))


%% TRE Target Registration Error
%     Svect = []; Tvect = []; dSvect = []; counter = 10; c = jet(200);
% 
%     dockf; subplot 133, imagesc(dsrc_sprd), colormap(gray), title('Registered Image (Moving -> Fix)'),
%     subplot 132, imagesc(tar), colormap(gray), title('Fixed Image'),
%     subplot 131, imagesc(src), colormap(gray), title('Moving Image'),
%     [y,x,button] = ginput(1);
%     x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*'),
%     Svect = [Svect; [x y]];
%     Sinds = sub2ind(size(dx), x, y);
%     dSx = x + dx(Sinds);
%     dSy = y + dy(Sinds);
%     dSvect = [dSvect; [dSx dSy]];
%     subplot 133, imagesc(dsrc), colormap(gray), title('Registered Image (Moving -> Fix)'),
%     hold on, plot(dSvect(:,2), dSvect(:,1), 'Color', c(counter, :), 'Marker', '*');
%     subplot 132, imagesc(tar), colormap(gray), title('Fixed Image'),
%     [y,x,button] = ginput(1);
%     x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*'),
%     Tvect = [Tvect; [x y]];
%     while (button ~=3)
%         counter = counter + 10;
%         subplot 133, imagesc(dsrc_sprd), colormap(gray), hold on, plot(dSvect(:,2), dSvect(:,1), 'r*'),
%         title('Registered Image (Moving -> Fix)'),
%         subplot 132, imagesc(tar), colormap(gray),hold on, plot(Tvect(:,2), Tvect(:,1), 'r*'),
%         title('Fixed Image');
%         subplot 131, imagesc(src), colormap(gray),hold on, plot(Svect(:,2), Svect(:,1), 'r*'), 
%         title('Moving Image');
%         [y,x,button] = ginput(1);
%         x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*');
%         Svect = [Svect; [x y]];
%         Sinds = sub2ind(size(dx), x, y);
%         dSx = x + dx(Sinds);
%         dSy = y + dy(Sinds);
%         dSvect = [dSvect; [dSx dSy]];
%         subplot 133, imagesc(dsrc_sprd), colormap(gray), title('Registered Image (Moving -> Fix)'),
%         hold on, plot(dSy, dSx, 'Color', c(counter, :), 'Marker', '*');
%         subplot 132, imagesc(tar), colormap(gray), hold on, plot(Tvect(:,2), Tvect(:,1), 'r*'),
%         title('Fixed Image');
%         [y,x,button] = ginput(1);
%         x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*');
%         Tvect = [Tvect; [x y]];    
%     end
% 
%     Svect = Svect'; Tvect = Tvect'; Svect(:,end) = []; Tvect(:,end) = [];
%     dSvect = dSvect'; dSvect(:,end) = [];
%     subplot 131, imagesc(src), colormap(gray),hold on, plot(Svect(2,:), Svect(1,:), 'r*'), 
%     title('Moving Image');
%     subplot 132, imagesc(tar), colormap(gray),hold on, plot(Tvect(2,:), Tvect(1,:), 'r*'),
%     title('Fixed Image');
%     subplot 133, imagesc(dsrc), colormap(gray), hold on, plot(dSvect(2,:), dSvect(1,:), 'r*'),
%     title('Registered Image (Moving -> Fix)'),
% 
%     Sinds = sub2ind(size(dx), Svect(1,:), Svect(2,:));
%     % dSvect(1,:) = Svect(1,:) + dx(Sinds);
%     % dSvect(2,:) = Svect(2,:) + dy(Sinds);
%     TRE_bef = sum(diag(dist2(Svect', Tvect'))); 
%     TRE_aft = sum(diag(dist2(dSvect', Tvect'))); % sqrt(sum( (src2D>1 - tar2D>0) .^2 ));
% 
%     dockf; imagesc(dsrc); colormap(gray), title('registered image')
%     dockf; imagesc( Unc_Shannon_Sprd ), title([Unc_str{1} ' Entropy on Spreaded Probabilities']), cbar;
% %     dockf; imagesc( diff ), title('intensity diff'), cbar;
% 

end