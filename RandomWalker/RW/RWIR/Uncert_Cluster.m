function Uncert_Cluster(Mode, gamma)
    
    add_paths()
%     root = '/home/ghassan/students_less/tlotfima';
%     savepath = [root '/RWRegistration_Final2D/results/'];
    savepath = '../results/';
    real_mode = 1; % gamma = 1;
    switch(Mode)
        case 1
           nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
%            [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
           [pbs dispvec] = Creating_Spatial_Test_Unc_Edited(nlabs_size, Visualize_Labels, sample_num);
           nsample = size(pbs,1); data_mode = 2;
           nlabs = size(dispvec,1);
           isz = [nsample 1]; mask = ones(nsample,1);  
           G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
           G = G ./ repmat(sum(G,2), 1, nlabs);
           pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2);
        case 2
            nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
%             [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
            [pbs dispvec] = Creating_Spatial_Test_Unc_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 2;
            nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);  
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G;  [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2);
        case 3
            nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
%             [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
            [pbs dispvec] = Creating_Spatial_Test_Unc3D_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2); dz = dispvec(q,3);
        case 4
            nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
%             [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
            [pbs dispvec] = Creating_Spatial_Test_Unc3D_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2); dz = dispvec(q,3);
    end
    %%
    Uncert_Mode = 3; % 0: KNN, 1: KNN&WCov, 2: KNN&KDE&WCov,  3: All,
    accuracy=3; epsilon_ = 1e-3; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    w = [3.3 2.2];
%     [Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc GMM2_l GMM2_u GMM2_avg GMM2_disc ...
%     GMM3_l GMM3_u GMM3_avg GMM3_disc Unc_Kde Unc_Knn] = ...
%     Calc_Uncertainty_MEX(pbs, pbs_sprd, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
    [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
        pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);  
%     [Wcov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
%%
    if(0)
        opt.accuracy = 3; opt.epsilon = 1e-3; %accuracy, pbs .* 10^(opt.k)
        opt.iter1 = 5000; Visualize_MoG_KDE_Edited(dispvec, pbs, opt);
    end
%%
    tic
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
%     [time.delta Unc_Delta] = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
    time.Unc_Shannon = toc ;
    if (Mode < 3)
        tic
        [Unc_ExpEr Unc_ExpEr_Map] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);
        time.Unc_ExpEr = toc; time.Unc_ExpEr_Map = time.Unc_ExpEr;
    else
        tic
        [Unc_ExpEr Unc_ExpEr_Map] = Calc_Uncertainty_Err_Dist2D_3D(pbs, dispvec, dx, dy, dz);
        time.Unc_Err_Dist = toc; time.Unc_Err_Dist_Map = time.Unc_Err_Dist;     
%         [Wcov SGauss] = Calc_Uncertainty_WCov3D(pbs, dispvec, isz, mask, epsilon_, data_mode);           

    end
%     if(synth==0 && real_mode ==0)
%         Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, ...
%             GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, ...
%             GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, nlabs_size, sample_num, Unc_str, data_mode);
%     end
    file_name = {'Unc_Shannon', 'Unc_Shannon_Sprd', 'Wcov', 'SGauss', 'GMM2_avg', 'Unc_Kde', 'Unc_Knn', 'Unc_ExpEr', 'Unc_ExpEr_Map', 'time'}; % 'Unc_Delta',
    for i = 1:10,
        save (num2str([savepath 'res_unc/' file_name{i} '_' num2str(Mode) '_gamma', num2str(gamma) '.mat']), file_name{i}); 
    end
end