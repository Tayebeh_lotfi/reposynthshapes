%% Creating Data
% cd 'H:\students_less\tlotfima\RWRegistration_Final2D\RWIR'
add_paths()
clc, close all, clear all,
global count;
global_initialize(1);
cond1 = 0;
cond2 = 0;
Phantom = 1;
Checkerboard = ~Phantom;
Mode = 11; Visualization = 0; 
real_mode = 0; synth = 0; test3D = 0;
[debug flag Max_Iterations step parameter1 parameter2 beta alpha noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
    
spacing.height = 20; spacing.width = 20;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
direction = 2;

switch Mode % 1: real data, 2: Phantom, or Checkerboard, 3: Synthetic data
    case 1
%         trial_num = 1;
%         load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);  
%         IWF = double(mat2gray(src3D(:,:,1)));
%         I = double(mat2gray(tar3D(:,:,1)));
% %         tlands2D = tlands3D(:,:,1); slands2D = slands3D(:,:,1); 
%         TxW= Tx3D(:,:,1); TyW= Ty3D(:,:,1); invTxW = invTx3D(:,:,1); invTyW = invTy3D(:,:,1); 
          load ../data/Bspline.mat;
    case 2
        if Phantom
            II = phantom('Modified Shepp-Logan',50);
            I = zeros(250,250);
            I(25:224,25:224) = II;
            h = fspecial('gaussian', hsize, sigma);
            I = imfilter(I,h);
%             I = imnoise(I,'gaussian',mean_,vari_);
%             mask1 = I;
            [IWF TxW TyW] = Trans_Warp( I, mag, direction );
%            [IWF TxW TyW] = affineWarp( I, mag, direction );  
%             [IWF TxW TyW] = rBSPwarp( I, [spacing.height spacing.width], mag, mask1 );
            mask1 = (IWF > 0.1);
            IWF = imnoise(IWF,'gaussian',mean_,vari_);       
            D(:,:,1) = TxW; D(:,:,2) = TyW;
            invD = invertDefField(D);
            invTxW = invD(:,:,1); invTyW = invD(:,:,2);
    %     dockf;imagesc(P);
        else
                II = checkerboard(25);I = zeros(250,250);
                I(25:224,25:224) = II;
                h = fspecial('gaussian', hsize, sigma);
                I = imfilter(I,h);
%                 I = imnoise(I,'gaussian',mean_,vari_);
                mask1 = I;
                [IWF TxW TyW] = Trans_Warp( mask1, mag, direction );
%                 [IWF TxW TyW] = rBSPwarp( mask, [spacing.height spacing.width], mag, mask );
                mask1 = (IWF > 0.1);
                IWF = imnoise(IWF,'gaussian',mean_,vari_);       
                D(:,:,1) = TxW; D(:,:,2) = TyW;
                invD = invertDefField(D);
                invTxW = invD(:,:,1); invTyW = invD(:,:,2);
        end
    case 3 
        mag = 30;
        IWF = mat2gray(rgb2gray(imread('1_2.bmp')));
        I = mat2gray(rgb2gray(imread('1_1.bmp')));
        TxW = 50*ones(size(I)); TyW = zeros(size(I)); invTxW = TxW; invTyW = TyW;
    case 4
        mag = 2;
        I = zeros(100,100);
        I(40:60,40:60) = 1;
%         I(42:58,42:58) = 0;
        IWF = zeros(100,100);
        IWF(40:60,42:62) = 1;
%         IWF(42:58,44:60) = 0;
        TxW = zeros(size(I)); TyW = zeros(size(I)); invTxW = TxW; invTyW = TyW; 
        invTyW(40:60,40:60) = -mag;
    case 5
        s = 10;
        mag = s/10;
        I = rand(s,s);
%         I(40:60,40:60) = 1;
        IWF = zeros(s,s);
        IWF(:,s/10+1:s) = I(:, 1:s*9/10);
%         IWF(40:60,41:61) = 1;
        TxW = zeros(size(I)); TyW = mag*ones(size(I)); invTxW = TxW; 
        invTyW = -mag*ones(size(I));
    case 6 
        mag = 20;
        I = zeros(30,50);
        I(10:20,10:20) = 1;
        IWF = zeros(30,50);
        IWF(10:20,30:40) = 1;
        TxW = zeros(size(I)); TyW = zeros(size(I)); invTxW = TxW; invTyW = TyW; 
        invTyW(10:20,30:40) = -mag;
    case 7
        load '../data/Lung_Img';
        I = mat2gray(Lung_Img(:,:,30));
        mask1 = I;
        [IWF TxW TyW] = rBSPwarp( I, [spacing.height spacing.width], mag, mask1 );
        D(:,:,1) = TxW; D(:,:,2) = TyW;
        invD = invertDefField(D);
        invTxW = invD(:,:,1); invTyW = invD(:,:,2);
    case 8
        load '../data/Diffusion.mat';
    case 9
        load('Y:\students_less\lisat\report\UGReg\UncertaintyGuidedRegistration\RWImgRegCode\RWexample_pbs.mat','pbs');
    case 10
        load('Y:\students_less\tlotfima\RWRegistration_Final2D\data\Abnormal_final.mat')
        load('Y:\students_less\tlotfima\RWRegistration_Final2D\data\Normal_final.mat')
        
    case 11
        [debug flag Max_Iterations step parameter1 parameter2 beta alpha noise_sigma noise_mean Mode_Str ...
        image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();  
        hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05; trial_num = 1;
        real_mode = 1;
        load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);
        src3D = resc(src3D);
        tar3D = resc(tar3D);
        % segment3D = resc(segment3D);
        t = 5; s = 20; I = tar3D(:,:,t); IWF = tar3D(:,:,s); 
        src_seg = segment3D(:,:,s); tar_seg = segment3D(:,:,t);
        mask = src_seg>0.1;
        noise_sigma = 0.01;
        TxW = zeros(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
end
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    %Tx2D = Tx; Ty2D = Ty; %
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Visualizing Data
    % close all;
%     dockf; subplot 121, imagesc(tar2D); title('target image'),colormap(gray)
%     subplot 122, imagesc(src2D); title('source image'),colormap(gray)
    % if (Mode ~=3)
    %     dockf;quiverR(zeros(size(invTy2D)), zeros(size(invTx2D)), invTy2D, invTx2D, 20, 2, 'c' ), title('ground truth');
    % end
    % dockf; clr_labels = [1:97 2 1 1]; %random labels
    % plotvec( rand(3,100), clr_labels );
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre2D = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
    
%% Calculating the likelihood
    % [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar2D, src2D, Tx2D, Ty2D, nseeds, res, mag);
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    if (cond1==1)
        i=1; j=1;
        % tstart=tic;
        cert_nstd = parameter1(j);
        lambda = parameter2(i);
    end
    slands = slands_pre2D; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
%% Calculating uncertainties
    mind_src=MIND_descriptor2d(src,2);
    mind_tar=MIND_descriptor2d(dsrc,2);
    diff = sum( (mind_tar - mind_src).^2, 3 );
    %     diff2D = dsrc2D - src2D;

    % homogeneous = mat2gray(Calc_Uncertainty(dterm));
    % b = 7; homogeneous = nlfilter(src2D, [b b], @local_edge_count);
    % m = mean(homogeneous(:)); sig = std(homogeneous(:));
    % homogeneous(homogeneous < m + sig ) = 0; homogeneous(homogeneous ~= 0) = 1;
    b=7; h = fspecial('average', b);
    homg = imfilter(src,h, 'same');
    homogeneous = mat2gray(abs(src - homg));
    if (Mode==3)
        mask1 = (src < 0.4);
    else
        if(Mode ~= 2)
            mask1 = (src > 0.1);
        end
    end
    mask2 = (homogeneous > mean(homogeneous(:))+0.3*std(homogeneous(:)));
    if(Mode == 1 || Mode == 8)
        mask3 = ( src_seg > 0); se = strel('disk',15); mask3 = imdilate(mask3,se);
    %     mask1 = mask1 .* mask3;
    end
    mask = mask1;
%% Calculating Uncertainty
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);
if(Mode == 11)   
%% Calculating warped segment
    [mx my] = ndgrid(1:isz(1), 1:isz(2));
    dsrc_seg = interp2( my, mx, src_seg, my + dy, mx + dx,  '*linear', 0); % nearest
%% Segment Errors
    Err_bef_seg = sqrt( sum( (src_seg(:)-tar_seg(:)) .^2 ) );
    Err_aft_seg = sqrt( sum( (dsrc_seg(:)-tar_seg(:)) .^2 ) );
    Err_bef = sqrt( sum( (src(:)-tar(:)) .^2 ) );
    Err_aft = sqrt( sum( (dsrc(:)-tar(:)) .^2 ) );
else
    if(Mode == 1 || Mode == 8)
        Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 )); 
        Warp_Err_Sprd = (sqrt(  (dx_sprd - invTx).^2 + (dy_sprd - invTy).^2 )); 
    end
end   
%% Calculating Entropy of Error
%     [err_pbs err_pbs_sprd] = Calculate_Prob_Error(dispvec, pbs, pbs_sprd, invTx2D, invTy2D, numel(mask));
%     err_mode = mode(err_pbs,2); err_mode_sprd = mode(err_pbs_sprd,2);
%     err_mean = mean(err_pbs,2); err_mean_sprd = mean(err_pbs_sprd,2);
%     err_mode = reshape(err_mode, [isz(1), isz(2)]); err_mode_sprd = reshape(err_mode_sprd, [isz(1), isz(2)]);
%     err_mean = reshape(err_mean, [isz(1), isz(2)]); err_mean_sprd = reshape(err_mean_sprd, [isz(1), isz(2)]);
%     [err_entrpy err_entrpy_sprd] = Calc_Uncertainty_Shannon(err_pbs, err_pbs_sprd, 2);
%     err_entrpy = reshape(err_entrpy, [isz(1), isz(2)]); err_entrpy_sprd = reshape(err_entrpy_sprd, [isz(1), isz(2)]);
%     err_entrpy = mat2gray(err_entrpy); % 
%%
    % mask = mask1;
    % %% Added Part
    % w = [0.2 0.3 0.1];
    % Unc8 = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
%% Adding Part
    % inds = find(mask>0);
    % pix = inds(2);
    % z = reshape(pbs, prod(isz), []);
    % cftool( dispvec(:,1), dispvec(:,2), z(pix,:) )
    % a1 ./ (b1+(exp(-c1*x) * exp(-d1*y))) + a2 ./ (b2+(exp(-c2*x) * exp(-d2*y)))

%% End of commented Part
if(real_mode)
%% Showing results
    dockf; subplot 221, imagesc(tar); title('target image'),
    subplot 222, imagesc(src); title('source image'),
    subplot 223, imagesc(dsrc); colormap(gray), title('registered image'),
    subplot 224, imagesc(dsrc_sprd); colormap(gray), title('modfd registered image'),

    
    dockf; 
    subplot 121, imagesc(flowToColor(invTx-dx, invTy-dy )), title('Warping Error'), cbar, colormap jet;
    subplot 122, imagesc(flowToColor(invTx-dx_sprd, invTy-dy_sprd )), title('Warping Error Modfd'), cbar, colormap jet;

    dockf; 
    subplot 131, imagesc( Unc_Shannon ), title([Unc_str{1} ' Entropy']), cbar;
    subplot 132, imagesc( Unc_Shannon_Sprd ), title(['Modified ', Unc_str{1}]), cbar;
    subplot 133, imagesc( diff ), title('MIND diff'), cbar;
    
%     subplot 224, imagesc(mask), title('Mask'), cbar; % Homogeneous Regions

    Stat_Err_Unc(Unc_Shannon_Sprd, Warp_Err, Warp_Err_Sprd, mask);

    %%   Visualization
    if(Visualization)
%     Visualize_MoG(dispvec, pbs, opt);
    
        image = src;
        dockf; imagesc(image), colormap(gray),
        [y,x,button] = ginput(1);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
        while (button ~=3)
%               for ind = 1:size(vector,2),
                image = src;
                x = round(x); y = round(y);
                prob1 = sqz(pbs(x,y,2:end));
                prob2 = sqz(pbs_sprd(x,y,1:end-1));
                prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
                prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);
                imagesc(image), colormap(gray), hold on, plot(y,x, 'r*'), 
                title(['pdf at point (', num2str(x), ',', num2str(y), ')']);
                dockf;
                subplot 121, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                subplot 122, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                colormap(jet)
%               dockf; 
%                 subplot 132, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'), 
%                 subplot 133, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs_sprd(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'),
%        %     end
            dockf; imagesc(image), colormap(gray),
            [y,x,button] = ginput(1);
        end
    end
%% Statistics
        S = find(mask==1);
        Initial_Warp = ( sqrt( (invTx(S)) .^2 + (invTy(S)) .^2 ) );
        Warp_Err_mask = (sqrt(  (dx(S) - invTx(S)).^2 + (dy(S) - invTy(S)).^2 )); 

        dockf; hist(Initial_Warp), 
        h = findobj(gca,'Type','patch');
        set(h,'FaceColor','r','EdgeColor','w','facealpha',0.7)
        hold on
        hist(Warp_Err_mask), title('                 Warping histgram, before and after registration'), 
        h = findobj(gca,'Type','patch');
        set(h,'facealpha',0.7);
        legend('Initial\_Warp', 'Warp\_Err', 'Location', 'SouthEastOutside');
       
         % - flowToColor(dx, dy ));
%         colormap jet;
%         dockf; imagesc(flowToColor(invTx2D, invTy2D )), cbar;
        dockf; 
        subplot 121, imagesc(abs(invTx - dx )), cbar, title('Err x');
        subplot 122, imagesc(abs(invTy - dy )), cbar, title('Err y');
%         text(-200,-520,['solution with regularization ', num2str(alpha)]);
%         dockf; subplot 121, imagesc(abs(invTx2D-dx)), title('Err x'), subplot 122, imagesc(abs(invTy2D-dy)), title('Err y')
%         dockf;hist(Tx2D(:)), title('Initial warp hist');
%         dockf;hist(dx(:)), title('Warp error hist');
%         dockf;
%         subplot 121, plot(unique(dx(:))), title('Solution warp');
%         subplot 122, plot(unique(Tx2D(:))), title('Initial warp');
%         dockf;
%         subplot 121,plot(unique(dy(:))), title('Solution warp');
%         subplot 122, plot(unique(Ty2D(:))), title('Initial warp');
    %% Uncertainty
% GMM4_1, GMM4_2, GMM4_3, GMM4_4, GMM9_1, GMM9_2, GMM9_3, GMM9_4, GMM25_1, GMM25_2, GMM25_3, GMM25_4,
        S = find(mask3==1);        
        for Unc_Mode = [1 18],
            switch Unc_Mode
                case 1
                    Unc = Unc_Shannon;
                case 2
                    Unc = Wcov;
                case 3
                    Unc = SGauss;
                case 4
                    Unc = GMM25_3; % Unc4;
                case 16
                    Unc = Unc_kde;
                case 17
                    Unc = Unc_Knn;
                case 18
                    Unc = Unc_Shannon_Sprd;
            end
            idata(:,:,1) = Unc; 
            if (Mode == 3)
                Error = diff; 
            else
                Error = Warp_Err; % err_entrpy; % Warp_Err; %  err_mean; % 
            end
            idata(:,:,2) = Error;
            b = 3; %9; %[23 23 8];%10; [ b b b ]
            cc(:,:,Unc_Mode) = blockproc(idata, [b b], @local_corr_fun);
            local_correlation = cc(1:2:end,2:2:end,Unc_Mode);%,1:2:end);
            gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
            idata1 = double(Unc(S));
            idata2 = double(Error(S));
            Mut_Info(Unc_Mode) = mutualinfo( idata1, idata2) ./ ...
                ( ( entropy(idata1) + entropy(idata2) ) ./ 2 );
            
            dockf; imagesc(local_correlation), title(['Local correlation uncert ' Unc_str{Unc_Mode}]), cbar;
            dockf; plot(Unc(S), Error(S), 'r*'), title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
                ' MI = ', num2str(Mut_Info(Unc_Mode))]), xlabel(['Uncertainty ', Unc_str{Unc_Mode}]), ylabel('Error');
            [p rsq] = lin_regress(Unc(S), Error(S), reg_mod);
%             if (reg_mod == 1)
%                 hold on, plot(Unc(S), p(1)*Unc(S) + p(2)*Unc(S), 'bo');
%             else
%                 hold on, plot(Unc(S), p(1)*Unc(S).^3 + p(2)*Unc(S).^2 + p(3)*Unc(S) + p(4), 'bo');
%             end
        end
end
%% Synthetic or real, controlled by real_mode variable
if(real_mode)
%     isz = isz; 
    data_mode = 2;
else
    if(synth)
        nsample = 1000; nlabs = 100;
        [dispvec pbs] = synthetic_sample_simplex(nsample,nlabs);
        isz = [nsample 1]; mask = ones(nsample,1);
    else
        nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
        [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
        nsample = size(pbs,1); data_mode = 2;
    end 
    nlabs = size(dispvec,1);
     isz = [nsample 1]; mask = ones(nsample,1);  
     G = 1 ./ (dist2(dispvec, dispvec) + 1);
     G = G ./ repmat(sum(G,2), 1, nlabs);
     pbs_sprd = pbs * G;
end
if(test3D)
    nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
    [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
    nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
    isz = [nsample 1]; mask = ones(nsample,1);
     G = 1 ./ (dist2(dispvec, dispvec) + 1);
     G = G ./ repmat(sum(G,2), 1, nlabs);
     pbs_sprd = pbs * G;
end

MoGk = 9; Uncert_Mode = 1; % 0: KNN, 1: All, 2: KNN&KDE
% mask = mask;
Sig = [1e-1 0; 0 1e-1]; mu = [0 0]; accuracy=3; epsilon_ = 10e-4; %accuracy, pbs .* 10^(opt.k)
k = 60; %k nearest neighbors 
iter1 = 5000; %gmdistribution iterations
% opt.iter2 = 2000; %kmeans iterations
    [Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc GMM2_l GMM2_u GMM2_avg GMM2_disc ...
    GMM3_l GMM3_u GMM3_avg GMM3_disc Unc_Kde Unc_Knn] = ...
    Calc_Uncertainty_MEX(pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
% %% Running MEX File
% EXE = 'Calc_Uncertainty_MEX'; % Calc_Uncertainty_MEX(pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode)
% cd '../utils/';
% cmd = sprintf(EXE); %([EXE ' pbs dispvec isz mask accuracy epsilon_ iter1 data_mode Uncert_Mode']); %
% save func_variables pbs dispvec isz mask accuracy epsilon_ iter1 data_mode Uncert_Mode;
% disp('Now calculates different uncertainty methods...')
% tic
% system([ 'start /WAIT /AFFINITY 0x50 ' cmd])
% toc
% load ../utils/Uncertainties;
% cd '../RWIR/';
% %% test MoG
% % emiter(fin_obj,dispvec);
%%
if(synth==0 && real_mode ==0)
    Visualize_Labels_Unc_Test(pbs, dispvec, Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, ...
        GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, ...
        GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, nlabs_size, sample_num, Unc_str, data_mode);
end
    
% if(Visualization)
%     if(real_mode)
%     %     option.Unc4 = Unc4;
%         option.Unc_str = Unc_str;
%     else
%         S = find(opt.mask==1);
%         option.Unc4_1 = Unc4_1; option.Unc4_2 = Unc4_2; 
%         option.Unc4_3 = Unc4_3; option.Unc4_4 = Unc4_4;
%         option.Unc_str = {'Shannon' 'Weighted Covariance' 'Gaussian distribution' 'Mixture of Gaussian_Avg' ...
%         'Kernel Density Estimation' 'K Nearest Neighbour' 'Mixture of Gaussian_low' ...
%         'Mixture of Gaussian_high' 'Mixture of Gaussian_disc'};
%     end
%     option.Unc1 = Unc_Shannon; option.Unc2 = Wcov; option.Unc3 = SGauss;
%     option.Unc5 = Unc_Knn; option.Unc_gauss = Unc_kde;
%     option.S = S; 
%     [Mut_Info_U, Global_Corr_U] = Uncert_corr(option);
% end
% %     clear slands; ten_perc = 5; maxd = 10;
% %     slands(1, :)  = round( (size(src2D, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
% %     slands(2, :)  = round( (size(src2D, 2) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
% %     inds = sub2ind(isz, slands(1,:), slands(2,:));
% %     my_pbs = reshape(pbs, [prod(isz) nlabs]);
% %     for ii = 1:length(inds),
% %         dockf; hist(my_pbs(inds(ii),:));
% %     end
% 
% %     [local_correlation1 local_correlation2 local_correlation3 local_correlation5 local_correlation6]