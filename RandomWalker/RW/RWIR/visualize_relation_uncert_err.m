function visualize_relation_uncert_err(tar, Unc, Error, Unc_Mode, Unc_str)

    [X1 Y1] = meshgrid(1:sz(tar,2),1:sz(tar,1)); temp=zeros(sz(tar)); 
    IND1=sub2ind(size(temp),X1(1:3:end),Y1(1:3:end));
    temp(IND1)=1;
    S = IND1; 
    gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
    idata1 = double(Unc(S));
    idata2 = double(Error(S));
    Mut_Info(Unc_Mode) = mutualinfo( resc(idata1), resc(idata2));
    if Unc_Mode == 1,
    figure, subplot 141, axis equal, axis tight,
    end
    if Unc_Mode == 5,
        subplot 142,  axis equal, axis tight,
    end
    if Unc_Mode == 3,
        subplot 143, axis equal, axis tight,
    end
    if Unc_Mode == 4,
        subplot 144, axis equal, axis tight,
    end
    plot(Unc(S), Error(S), 'b*'), set(gca, 'FontSize', 35); title(['GCC = ', num2str(gcc(1,2,Unc_Mode))], 'FontSize', 35), 
    xlabel( Unc_str{Unc_Mode}, 'FontSize', 35), ylabel('Error', 'FontSize', 35);
     set(gca,'YTick',[]); set(gca,'XTick',[]);
end


% 
%     temp=zeros(sz(tar));
%     %temp(:,40:60)=0;
% 
%     [X1,Y1]=meshgrid(10:40,10:40);
%     [X2,Y2]=meshgrid(10:40,60:90);
%     [X3 Y3] = meshgrid(1:sz(tar,2),1:sz(tar,1));
%     IND1=sub2ind(size(temp),X1(:),Y1(:));
%     IND2=sub2ind(size(temp),X2(:),Y2(:));
%     IND3=sub2ind(size(temp),X3(:),Y3(:));
% 
%     temp(IND1)=1;
%     temp(IND2)=2;
%     temp(IND3)=3;
% 
% %     idata(:,:,1) = Unc; 
% %     idata(:,:,2) = Error;
% 
% %     mn = mnx(Error);
% %     Unc = resc(Unc)*mn(2);
%     
% %     S = IND1; 
% %     gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
% %     idata1 = double(Unc(S));
% %     idata2 = double(Error(S));
% %     Mut_Info(Unc_Mode) = mutualinfo( idata1, idata2);
% %     dockf;   
% %     plot(Unc(S), Error(S), 'r*'),title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
% %     ' MI = ', num2str(Mut_Info(Unc_Mode))], 'FontSize', 10), xlabel(['Uncertainty ', Unc_str{Unc_Mode}], 'FontSize', 10), ylabel('Error', 'FontSize', 10);
% % %     dockf;plotregression(Unc(S), Error(S))%
% % 
% %     S = IND2; 
% %     gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
% %     idata1 = double(Unc(S));
% %     idata2 = double(Error(S));
% %     Mut_Info(Unc_Mode) = mutualinfo( resc(idata1), resc(idata2));
% %     dockf;  plot(Unc(S), Error(S), 'b*'),title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
% %     ' MI = ', num2str(Mut_Info(Unc_Mode))], 'FontSize', 10), xlabel(['Uncertainty ', Unc_str{Unc_Mode}], 'FontSize', 10), ylabel('Error', 'FontSize', 10);
% 
% 
%     S = IND3; 
%     gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
%     idata1 = double(Unc(S));
%     idata2 = double(Error(S));
%     Mut_Info(Unc_Mode) = mutualinfo( resc(idata1), resc(idata2));
%     if Unc_Mode == 1,
%     dockf; subplot 131,
%     end
%     if Unc_Mode == 2,
%         subplot 132,  
%     end
%     if Unc_Mode == 4,
%         subplot 133,
%     end
% %     plot(Unc(S), Error(S), 'b*'), set(gca, 'FontSize', 25); title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
% %     ' MI = ', num2str(Mut_Info(Unc_Mode))], 'FontSize', 25), xlabel(['Uncertainty ', Unc_str{Unc_Mode}], 'FontSize', 25), ylabel('Error', 'FontSize', 25);
%     plot(Unc(S), Error(S), 'b*'), set(gca, 'FontSize', 25); title(['GCC = ', num2str(gcc(1,2,Unc_Mode))], 'FontSize', 25), 
%     xlabel( Unc_str{Unc_Mode}, 'FontSize', 25), ylabel('Error', 'FontSize', 25);
% % %  
% % %     dockf;  plotregression(Unc(S), Error(S))%
% 
% %     S =[IND1; IND2]; 
% %     gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
% %     idata1 = double(Unc(S));
% %     idata2 = double(Error(S));
% %     Mut_Info(Unc_Mode) = mutualinfo( idata1, idata2);
% %     dockf;  plot(Unc(S), Error(S), 'g*'),title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
% %     ' MI = ', num2str(Mut_Info(Unc_Mode))], 'FontSize', 10), xlabel(['Uncertainty ', Unc_str{Unc_Mode}], 'FontSize', 10), ylabel('Error', 'FontSize', 10);
% %     dockf;  plotregression(Unc(S), Error(S))%
% end

        