function Regression(trial, mcase)
%% Creating Data
% cd 'H:\students_less\tlotfima\RWRegistration_Final2D\RWIR'

add_paths()
% clc, clear, trial = 3; 
% mcase = 1; % 1: uncertainties vs other features 2: features with/without uncertainties 
%%
[ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
trial_num = 1;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; %MIND
% load(['../data/Precomputed/trial' num2str(trial_num) str '_downsample.mat']);
load ('../data/native_slices_ax2_41');
sze = [256 256];
%% Simplest case, Same warping

spacing.height = 20;
spacing.width = 20;
%% 
%for trial = 2:5,
%    for mcase = 1:2,
        countertrn = 1; countertes = 1;
        base = 1000;
        max_size = base*trial;
        for img = 1 : 20 % 5 % 20 %10, %  sz(tar3D,3),
            tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
            for warp = 1 : 5 % 2 % 5 %1, % 
                [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta, noise_sigma);
%% Calculating the classes of error, 1: low error, 2: medium error 3: high error
                thresh = mnx(Output);
                thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.15; %.05
                thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.6; % 65
                Outputrn = Output;
                Outputrn(Output<thresh1) = 1;
                Outputrn(Output>thresh2) = 3;
                Outputrn(Output>thresh1 & Output<thresh2) = 2;
%% Randomizing the order of pixels  
                ind1 = find(Outputrn == 1);
                ind2 = find(Outputrn == 2);
                ind3 = find(Outputrn == 3);
                p1 = randperm(size(ind1));
                p2 = randperm(size(ind2));
                p3 = randperm(size(ind3));

                Inputrn1 = Input(ind1(p1(1:max_size*1/5)),:); Outputrn1 = Output(ind1(p1(1:max_size*1/5)),:);
                Inputrn1( max_size*1/5 + 1 : max_size*4/5, :) = Input(ind2(p2(1:max_size*3/5)),:); 
                Outputrn1( max_size*1/5 + 1 : max_size*4/5, :) = Output(ind2(p2(1:max_size*3/5)),:);
                Inputrn1( max_size*4/5 + 1 : max_size*5/5, :) = Input(ind3(p3(1:max_size*1/5)),:); 
                Outputrn1( max_size*4/5 + 1 : max_size*5/5, :) = Output(ind3(p3(1:max_size*1/5)),:);
%% 
                if mcase == 1,
                    Inputrn1_Less = Inputrn1(:,1:end-18); % end-2
                else
%%        
                    Inputrn1_Less = Inputrn1(:,end-17:end); % end-1:end
                    Inputrn1 = Inputrn1(:,1:end-18); % end-2
                end
% 
%                 p = randperm(sz(Input,1));
%                 Input = Input(p,:); Outputrn = Outputrn(p,:);
%                 Input_Less = Input_Less(p,:); %Output_Less = Outputrn(p,:);
%% Choosing some randon pixels from the whole image
                trainInput(countertrn:countertrn + max_size - 1, :) = Inputrn1(1:max_size,:);
                trainOutput(countertrn:countertrn + max_size - 1, :) = Outputrn1(1:max_size,:);
                trainInput_Less(countertrn:countertrn + max_size - 1, :) = Inputrn1_Less(1:max_size,:);
%                 trainOutput_Less(countertrn:countertrn + max_size - 1, :) = Outputrn(1:max_size,:);        
                countertrn = countertrn + max_size;
                fprintf('\nstart of iteration : %d, image number: %d\n', warp, img);
            end
        end
        % X1 = trainInput(1:4:end,:);
        % Y1 = trainOutput(1:4:end,:);
        B_Less=TreeBagger(80,trainInput_Less,trainOutput,'method', 'regression','OOBPred','on','oobvarimp','on'); % ); % 
        B=TreeBagger(80,trainInput,trainOutput,'method', 'regression', 'OOBPred','on','oobvarimp','on'); % ); % 'method', 'regression'

        if mcase == 1,
            save (['../results/diff_pixel_num/regress_without_unc_another', num2str(trial), '_noise', num2str(noise_sigma*100), '.mat'], '-v7.3', 'B_Less');
            save (['../results/diff_pixel_num/regress_with_unc_another', num2str(trial), '_noise', num2str(noise_sigma*100), '.mat'], '-v7.3', 'B');
        else
            save (['../results/diff_pixel_num/regress_unc_feat_another', num2str(trial), '_noise', num2str(noise_sigma*100), '.mat'], '-v7.3', 'B_Less');
            save (['../results/diff_pixel_num/regress_img_feat_another', num2str(trial), '_noise', num2str(noise_sigma*100), '.mat'], '-v7.3', 'B');
        end
%    end
%end

%% Testing the model, how well it is trained, error of training
% dockf; plot(oobError(B_Less)),
% xlabel('number of grown trees')
% ylabel('out-of-bag classification error'),
% title('Error of Prediction Error');
% dockf; bar(B_Less.OOBPermutedVarDeltaError);
% xlabel('Feature number');
% ylabel('Out-of-bag feature importance');
% title('Feature importance results');
% 
% dockf; plot(oobError(B)),
% xlabel('number of grown trees')
% ylabel('out-of-bag classification error'),
% title('Error of Prediction Error');
% dockf; bar(B.OOBPermutedVarDeltaError);
% xlabel('Feature number');
% ylabel('Out-of-bag feature importance');
% title('Feature importance results');

%% Testing the Error, how well the actuall error and the predicted error are correlated 
% for img = 21 : 21 %10, %  sz(tar3D,3),    
%     tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
%     for warp = 1 : 1 %1, % 
%         [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta);
%         Input_Less = Input(:,1:end-2);
%         testInput = Input;
% %         testOutput = Output;
%         thresh = mnx(Output);
%         thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.01;
%         thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.5;
%         testOutput = Output;
%         testOutput(Output<thresh1) = 1;
%         testOutput(Output>thresh2) = 3;
%         testOutput(Output>thresh1 & Output<thresh2) = 2;            
%         testInput_Less = Input_Less;
%         Pred_Err_Less = B_Less.predict(testInput_Less);
%         Pred_Err = B.predict(testInput); 
%         
%         for i = 1:sz(Pred_Err),
%             Err_Less(i) = double(Pred_Err_Less{i} - '0');
%             Err(i) = double(Pred_Err{i} - '0');
%         end
%         clear Pred_Err_Less, clear Pred_Err
%         Pred_Err_Less = Err_Less;
%         Pred_Err = Err;
%         
%         sum(abs(Pred_Err_Less(:) -  testOutput(:)))
%         sum(abs(Pred_Err(:) -  testOutput(:)))
% %         dockf; plotregression(Pred_Err_Less, testOutput)%, title('Corr. Error/ Predicted Error, No Uncert.'),
% %         dockf; plotregression(Pred_Err, testOutput)%, title('Corr. Error/ Predicted Error, With Uncert.'),
%     end
% end  

%% Testing the Error, how well the actuall error and the predicted error are correlated 
% [ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
%     image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
% trial_num = 1;
% hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
% Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
% %              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
% simil_measure = 5; %MIND
% load ('../data/native_slices_ax2_41');
% load ('../results/testB'); load ('../results/testB_Less');
% sze = [256 256]; reg_mod = 1; eps = .3;
% c = jet(120);
% shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
% for img = 22 : 26 %10, %  sz(tar3D,3),    
%     tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
%     for warp = 1 : 1 %1, % 
%         [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta, noise_sigma);
%         Input_Less = Input(:,1:end-18);
%         testInput = Input;
%         testOutput = Output;           
%         testInput_Less = Input_Less;
%         Pred_Err_Less = B_Less.predict(testInput_Less);
%         Pred_Err = B.predict(testInput); 
%         gcc = corrcoef(Pred_Err, testOutput);
%         gcc_Less = corrcoef(Pred_Err_Less, testOutput);
%         Pred_Err_Less_Mat(:,img-21) = Pred_Err_Less; testOutput_Mat(:,img-21) = testOutput; Pred_Err_Mat(:,img-21) = Pred_Err;
% %         dockf; plotregression(Pred_Err_Less, testOutput),set(gca, 'FontSize', 35);%, title('Corr. Error/ Predicted Error, No Uncert.'),
%         [p,rsq] = polyfit(Pred_Err_Less, testOutput,1);
%         out_fit = polyval(p, range, rsq);
%         figure,
%         plot(range, out_fit, 'Marker', shape{1}, 'Color', 'k', 'LineWidth', 3, 'MarkerSize', 12), 
%         hold on, plot(Pred_Err_Less(1:5:end),testOutput(1:5:end), 'bo');
%         title(['Corr. Coef. = ', num2str(gcc_Less(1,2)) ' Without Unc'], 'FontSize', 35);
%         set(gca, 'FontSize', 35);  
%         legend('Fit', 'Data', 'Location', 'SouthEastOutside'),
%         set(gca, 'FontSize', 35);  xlabel('Registration Error', 'FontSize', 35); ylabel('Registration Error Prediction', 'FontSize', 35); 
% 
%         [p,rsq] = polyfit(Pred_Err, testOutput,1);
%         out_fit = polyval(p, range, rsq);
%         figure,
%         plot(range, out_fit, 'Marker', shape{1}, 'Color', 'k', 'LineWidth', 3, 'MarkerSize', 12), 
%         hold on, plot(Pred_Err(1:5:end),testOutput(1:5:end), 'bo');
%         title(['Corr. Coef. = ', num2str(gcc(1,2)) ' With Unc'], 'FontSize', 35);
%         set(gca, 'FontSize', 35);  
%         legend('Fit', 'Data', 'Location', 'SouthEastOutside'),
%         set(gca, 'FontSize', 35);  xlabel('Registration Error', 'FontSize', 35); ylabel('Registration Error Prediction', 'FontSize', 35); 
%     end
% end       
end