function Tayebeh_RWRPrior_Test(set_num, trial_num, MCase) 

% clear, trial_num = 1; set_num = 6; MCase = 2;

%% Getting two images, pre-setting landmarks
[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();

Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

simil_measure = 5; %MIND
%% Reading data
sze = [128 128];
tar = zeros(sze);
tar(50:60, 50:60) = 1;
src = zeros(sze);
src(70:80, 70:80) = 1;
invTx = 35*ones(sze); invTy = 35*ones(sze);
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(tar);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    slands_pre = slands; tlands_pre = tlands;
%% Build graph with 8 lattice
        isz = size(src);
        [points edges]=lattice( isz(1), isz(2), 1);
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        nseeds = size(tlands,2); 
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; 
%% Running probabilistic registration and Calculating the warping error
            [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);        
            [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);

%% Test of probabilities            
        image = src;
        dockf; imagesc(image), colormap(gray),
        [y,x,button] = ginput(1);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
        while (button ~=3)
%               for ind = 1:size(vector,2),
                image = src;
                x = round(x); y = round(y);
                prob1 = sqz(pbs(x,y,1:end));
%                 prob1 = sqz(dterm(x,y,1:end));
                prob2 = sqz(pbs_sprd(x,y,1:end));
                prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
                prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);
                imagesc(image), colormap(gray), hold on, plot(y,x, 'r*'), 
                title(['pdf at point (', num2str(x), ',', num2str(y), ')']);
                dockf;
                subplot 121, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                subplot 122, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                colormap(jet)
%               dockf; 
%                 subplot 132, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'), 
%                 subplot 133, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs_sprd(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'),
%        %     end
            dockf; imagesc(image), colormap(gray),
            [y,x,button] = ginput(1);
        end
end
