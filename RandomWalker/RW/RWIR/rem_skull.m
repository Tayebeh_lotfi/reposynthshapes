function rem_skull(vol_num)
    add_paths();
    filename  = (['../data/nifti_', num2str(vol_num), '.nii']);
    

nii = load_nii(filename);
I = nii.img;
I = volresize(I,[70 70 65],'*linear');

fileprefix = '../data/nifti_low';
 nii = make_nii(I);
 save_nii(nii, [fileprefix '_' num2str(vol_num) '.nii'])

filename  = (['..\data\nifti_low_', num2str(vol_num), '.nii']);
T = 50;
k = 2.3;
skullstripping(filename, T, k);


end