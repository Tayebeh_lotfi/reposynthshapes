function RWR_real_tumor_cluster(MCase)

    add_paths()
    [debug flag Max_Iterations step parameter1 parameter2 beta alpha noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters(); 
    load('../data/tumor_data/Abnormal_final.mat'); % P_final_trn.mat
    load('../data/tumor_data/Normal_final.mat'); % J_final_trn.mat
%     for MCase = 1:10,
    Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
    %              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
    simil_measure = 5; %MIND
    I = mat2gray(imrotate(J_final(:,:,MCase),-90)); IWF = mat2gray(imrotate(P_final(:,:,MCase), -90));
%         mask = IWF>0.1;
    TxW = mag*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);

%% Calculating the likelihood
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    slands = slands_pre; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
%% Shannon's Entropy
    [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2); 
%         
%         Shannon_Vect(:,:,MCase) = Shannon;
%         Shannon_Sprd_Vect(:,:,MCase) = Shannon_Sprd;
%         src_Vect(:,:,MCase) = src;
%         tar_Vect(:,:,MCase) = tar;
%         dsrc_Vect(:,:,MCase) = dsrc;
%         dsrc_sprd_Vect(:,:,MCase) = dsrc_sprd;
%     end

%%    %% Saving Results
    file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd' };    
    for i = 1:6,
        save (num2str([savepath 'Real_Tumor_Shan_MShan/' file_name{i} '_Case' num2str(MCase) '.mat']), file_name{i}); 
    end
%     end

%% Save the results in one .mat file
%     ldpath = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\Real_Tumor_Shan_MShan\';
%     file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd'};    
% 
%   for MCase = 1:90,
%         for i = 1:6,
%             load (num2str([ldpath, file_name{i}, '_Case', num2str(MCase), '.mat'])); 
%         end
%         Shannon_Vec(:,:,MCase) = Shannon;
%         Shannon_Sprd_Vec(:,:,MCase) = Shannon_Sprd;
%         src_Vec(:,:,MCase) = src;
%         tar_Vec(:,:,MCase) = tar;
%         dsrc_Vec(:,:,MCase) = dsrc;
%         dsrc_sprd_Vec(:,:,MCase) = dsrc_sprd;
%    end
%     save (['../results/Real_Tumor_Shan_MShan', '_Case', num2str(MCase), '.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
%       'src_Vec', 'tar_Vec', 'dsrc_Vec', 'dsrc_sprd_Vec');
%   end
%% Visualize Results 
% 
%     intx = 20; inty = 20; rad = 15; m = size(src_Vec);
%     [rr cc] = ndgrid(1:m(1), 1:m(2));
%     mask = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
%     mask = src_Vec(:,:,1)>0.1;
%     tar_Vec = mat2gray(tar_Vec) .* mask;
%     elnm = 10; j =1;
%     src_Vec = mat2gray(src_Vec) .* repmat(mask, [1, 1, elnm]);
%     dsrc_Vec = mat2gray(dsrc_Vec) .* repmat(mask, [1, 1, elnm]);
%     dsrc_sprd_Vec = mat2gray(dsrc_sprd_Vec) .* repmat(mask, [1, 1, elnm]);
%     Shannon_Vec = mat2gray(Shannon_Vec) .* repmat(mask, [1, 1, elnm]);
%     Shannon_Sprd_Vec = mat2gray(Shannon_Sprd_Vec) .* repmat(mask, [1, 1, elnm]);
%         dockf; montage(reshape(mat2gray(tar_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm]  )),
%         colormap(jet),  title('fixed Image'), cbar
%         dockf; montage(reshape(mat2gray(src_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Moving Image'), cbar
%         dockf; montage(reshape(mat2gray(dsrc_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Warped Image'), cbar
%         dockf; montage(reshape(mat2gray(dsrc_sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Modfd Warped Image'), cbar
%         dockf; montage(reshape(mat2gray(Shannon_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Shannon Entropy'), cbar
%         dockf; montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  elnm]  )),
%         colormap(jet), title('Modified Shannon Entropy'), cbar
end