function RWR_synth_noise_cluster(Mode, MCase, img_num)
    add_paths()
    [ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%     noise_sigma = 0.005; 
    Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
    simil_measure = 5; %MIND
    
    load ('../data/native_slices_ax2_41');
    sze = [100 100]; % img_num = 1;
    [m n] = ndgrid(1: sze(1), 1: sze(2));    
    spacing.height = 10;
    spacing.width = 10;
    magnitude = 4;
    I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
    [IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], magnitude );
    D(:,:,1) = TxW; D(:,:,2) = TyW;
    invD = invertDefField(D);
    invTxW = invD(:,:,1); invTyW = invD(:,:,2);
    tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
    src_seg = interp2(n, m, tar_seg, n + invTyW, m + invTxW, '*linear');
    mask = src_seg>0.1;
    noise_sigma = 0.005;
    IWF = imnoise( IWF, 'gaussian', 0, (Mode-1) * noise_sigma );

%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
%% Calculating the likelihood
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    slands = slands_pre; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
    
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
%% Shannon's Entropy
    [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2); 
        Uncert_Mode = 2; % 0: KNN, 1: KNN&WCov, 2: KNN&KDE&WCov,  3: All,
    epsilon_ = 10e-5; accuracy=4; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    nlabs_size = floor(sqrt(size(dispvec,1)-1));
    data_mode = 2;
    w = [3.3 2.2]; 
%     [WCov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
    [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
        pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
%     [time.delta Unc_Delta] = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
    [Unc_Err_Dist Unc_Err_Dist_Map] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);
%% Saving Results
    file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd', 'Unc_Kde', 'Unc_Knn', 'Unc_Err_Dist_Map', 'Unc_Err_Dist', 'dx_sprd', 'dy_sprd', 'WCov', 'SGauss', 'dx', 'dy', 'time' };    
    for i = 1:17,
        save (num2str([savepath 'Uncert_all_tumor_noise/uncert_all_noise/' file_name{i} '_' num2str(Mode) '_Case' num2str(MCase) '_Img' num2str(img_num) '.mat']), file_name{i}); 
    end
    
end

%% 
