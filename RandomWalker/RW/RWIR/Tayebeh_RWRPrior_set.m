function Tayebeh_RWRPrior_set(set_num, trial_num, MCase) 

% trial_num = 1;
% matlabpool
% matlabpool close
 
% add_paths();
%% Getting two images, pre-setting landmarks
[debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%% Reading data
% if(MCase == 1)
% %    load(['../data/Precomputed/trial' num2str(trial_num) '_large_warp_corner.mat']); 
% %    load(['../data/Precomputed/trial' num2str(trial_num) '_simwarp' str '_corner.mat']);  
%     load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);  
% %    load(['../data/Precomputed/trial' num2str(trial_num) '_new.mat']); 
% %    load(['../data/Precomputed/trial' num2str(trial_num) '_new_noise5_info.mat']); 
% %    load(['../data/Precomputed/trial' num2str(trial_num) '_new_noise10_info.mat']); 
%     slands_pre3D = slands3D; 
%     tlands_pre3D = tlands3D;
% else
    load ('../data/native_slices_ax2_41');
   %%
   if (MCase == 1)
       load '../data/Bspline.mat';
   else
       load '../data/Diffusion.mat';
   end
% end
%%
Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

Pre_Max_Iterations = Max_Iterations;


start_img = (set_num-1)*image_set_size; scale = 10E5;
simil_measure = 5; %MIND

for img_num = start_img + 1 : start_img + image_set_size,
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    [mm nn] = ndgrid(1: sze(1), 1: sze(2));
%%    
    if(MCase ~= 1)
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        IWF = interp2(nn, mm, I, nn + TyW, mm + TxW, '*linear');
        tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
        src_seg = interp2(nn, mm, tar_seg, nn + TyW, mm + TxW, '*linear');
        IWF(isnan(IWF))=0;
        src_seg(isnan(src_seg))=0;
        src = mat2gray(IWF); tar = mat2gray(I); 
        Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    else
%%                
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        IWF = interp2(nn, mm, I, nn + TyW, mm + TxW, '*linear');
        tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
        src_seg = interp2(nn, mm, tar_seg, nn + TyW, mm + TxW, '*linear');
        src = mat2gray(IWF); tar = mat2gray(I); 
        Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
%         src = src3D(:,:,img_num);    tar = tar3D(:,:,img_num);
%         src = double(mat2gray(src)); tar = double(mat2gray(tar));
%         tar_seg = segment3D(:,:,img_num);    src_seg = src_segment3D(:,:,img_num);     
%         slands = slands3D(:,:,img_num);    slands_pre = slands_pre3D(:,:,img_num);
%         tlands = tlands3D(:,:,img_num);    tlands_pre = tlands_pre3D(:,:,img_num);
%         Tx = Tx3D(:,:,img_num);    Ty = Ty3D(:,:,img_num);
%         Tx = Tx3D(:,:,2); Ty = Ty3D(:,:,2);
%         invTx = invTx3D(:,:,img_num);    invTy = invTy3D(:,:,img_num);
%         D(:,:,1) = Tx; D(:,:,2) = Ty;
%         invD = invertDefField(D);
%         invTx = invD(:,:,1); invTy = invD(:,:,2);
    end
    mask = tar>0.1;
    b=7; h = fspecial('average', b); homg = imfilter(src, h, 'same'); homogeneous = mat2gray(abs(src - homg));
    homogeneous = homogeneous .* mask;
    mask1 = (homogeneous > mean(homogeneous(:))+0.3*std(homogeneous(:))); %(src > 0.1); % .* 
    mask2 = ( src_seg > 0); se = strel('disk',15); mask2 = imdilate(mask2,se);
    Noise_Level = 1;   noise_sigma = 0.05;  
    src = imnoise(src, 'gaussian', 0, (Noise_Level-1) * noise_sigma );

%%
    tlands_pre = tlands; slands_pre = slands;
    Max_Iterations = Pre_Max_Iterations;
%     for i = 1:len(parameter2), 
%         for j = 1:len(parameter1),
    nseeds = size(tlands,2);
%% Calculating the likelihood    
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd; 

    i=1; j=1;
%%
    for Mode = [2,4], %1:7, % [2 6 7], %:  % 1: Random Selection       2: Self Learning      3: Active Learning
        tstart=tic;
        cert_nstd = parameter1(j);
        alpha = parameter2(i);
        slands = slands_pre; 
        tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; 
        if (Mode==7), Max_Iterations = 2; end
%% Build graph with 8 lattice
        isz = size(src); nseeds = len(tlands);
        [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
        reg_iteration = 1;
        while(reg_iteration <= Max_Iterations) 
           fprintf('start of iteration : %d, Mode : %s, image number: %d\n', reg_iteration, Mode_Str{Mode}, img_num);
           fprintf('regularization = %f, cert_nstd = %f\n', parameter2(i), parameter1(j));
%             Show_Landmarks(tlands, slands, tar, src, debug, reg_iteration, step, Mode, Mode_Str);

%% Running probabilistic registration and Calculating the warping error
%             [dx dy pbs pbs_sprd dsrc] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
%             [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, reg_iteration);
            [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma);
%% Show registration results
%             Show_Reg_Res(src, tar, dsrc, dx, dy, d, slands, tlands, debug, reg_iteration, step, Mode, Mode_Str);               
%% Calculating Uncertainty
            
            mask = mask .* mask2; % .* mask2;   
            S = find(mask);
            [mx my] = ndgrid(1:isz(1), 1:isz(2)); 
            src_seg_wrp = interp2(my, mx, src_seg, my+dy, mx+dx, '*linear',0);
            dice = sum((abs(tar_seg(S)-src_seg_wrp(S))));
            total_field_err = Calc_Warp_Err(dx, dy, invTx, invTy, S);
            initial_warp = Calc_Warp_Err(zeros(sz(invTx)), zeros(sz(invTy)), invTx, invTy, S);
            real_mode = 2;
            
            [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
            epsilon_ = 1e-2; data_mode = 2;
%             tic
%             Unc_WCov = mat2gray(Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode));
%             toc
            aa = 1/3 : .1 : 1;
            bb = 1/3 : .1 : 1;
            cc= 1/3 : .1 : 1;
% count_bb = 1; count_cc = 1;
            for count_aa = 1 : 7,
                for count_bb = 1 : 7,
                    for count_cc = 1 : 7,
%             aa = 1/3; bb = 1/3; cc = 1/3;
                        [gxx gxy] = gradient(dx);
                        [gyx gyy] = gradient(dy);
                        Smooth_Val = gxx .* gxy - gyx .* gyy;
                        uncertainty = aa(count_aa) .* mat2gray(Unc_Shannon_Sprd) + bb(count_bb) .* mat2gray(homogeneous) + cc(count_cc) .* mat2gray(Smooth_Val); % Unc_WCov; % 
                        Unc(:, :, count_aa, count_bb, count_cc, reg_iteration, Mode) = uncertainty;
%             Analyze_Err_Unc(invTx, invTy, dx, dy, mask, Unc_Shannon_Sprd, reg_iteration, Mode, Mode_Str, img_num);
%             Unc(:,:,reg_iteration) = Unc1;
%             dx_iter(:,:,reg_iteration) = dx;
%             dy_iter(:,:,reg_iteration) = dy;       
%% Picking top certain pixels
%             if(reg_iteration == 1)
%                 [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
%                 cert_thres2 = quantile(uncertainty(S),.999);
%                 uncertainty1 = uncertainty .* mask;
%                 SS=( uncertainty1 > cert_thres2); % & uncertainty < cert_thres2);
%                 slands_inds = find(SS);
%                 slands = [];
%                 slands(1,1) = mx( slands_inds(1) )';
%                 slands(2,1) = my( slands_inds(1) )';
%             end
                        ops.pick_mode = 6; % 1: Original, 2: Edge, 3: Reduced, 4: Original Uncer, 5: Edge Uncer, 6: Reduce Uncer
                        ops.num_seed = num_seed;
                        if (Mode == 1)
                              AllErr.RandG( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = total_field_err; % dice; %
                              SegErr.RandG( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = dice; %total_field_err; %
                              [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, invTx, invTy, ops, slands, tlands, reg_iteration, MCase);
            %                   [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid_User(src, tar, uncertainty, invTx, invTy, src_seg, cert_nstd, maxd, ops, slands, tlands, reg_iteration);                  
                        end 
                       if (Mode==2)
                           AllErr.AL( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = total_field_err; % dice; %
                           SegErr.AL( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = dice; %total_field_err; %
                           [slands_new tlands_new slands tlands nseeds indexes] = Pick_Top_AL_Exper(src, mask, uncertainty, invTx, invTy, cert_nstd, ops, slands, tlands, tlands_pre, MCase);
            %                iter_index(reg_iteration,:) = indexes;
            %                 [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_User(src, tar, uncertainty, invTx, invTy, src_seg, cert_nstd, maxd, ops, slands, tlands, tlands_pre);
                       end
                       if (Mode==3)
                            AllErr.SL100( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = total_field_err; % dice; %
                            SegErr.SL100( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = dice; %total_field_err; % 
                            AllErr.seed_SL100( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, reg_iteration) = nseeds;
                            ops.num_seed = 100; % num_seed;
                            [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert(src, mask, uncertainty, dx, dy, cert_nstd, ops, slands, tlands, MCase);              
                       end
                       if (Mode==4)
                            AllErr.SL_All( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = total_field_err; %dice; %
                            SegErr.SL_All( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, count_aa, count_bb, count_cc, reg_iteration ) = dice; %total_field_err; %
                            AllErr.seed_SL_All( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, reg_iteration) = nseeds;
                            ops.num_seed = 200;
                            [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert(src, mask, uncertainty, dx, dy, cert_nstd, ops, slands, tlands, MCase);
                       end
                       if (Mode==5)
                           AllErr.SL_Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = total_field_err; %dice; %
                           SegErr.SL_Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = dice; %total_field_err; %
                           AllErr.seed_SL_Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, reg_iteration) = nseeds;
                           ops.num_seed = 50;
                           [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Rand_Exper(src, dx, dy, src_seg, maxd, ops, slands, tlands, MCase);
                       end
                        if (Mode == 6)
                            AllErr.Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = total_field_err; %dice; %
                            SegErr.Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)*len(parameter1) + j, reg_iteration ) = dice; %total_field_err; %
                           [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, src_seg, maxd, ops, slands, tlands, MCase);
%                           [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_User(src, tar, uncertainty, invTx, invTy, src_seg, cert_nstd, maxd, ops, slands, tlands);
                        end 
                       if (Mode ==7)
                           ops.num_seed = num_seed * Pre_Max_Iterations;
                           AllErr.AL_baseline( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, reg_iteration ) = total_field_err; % dice; %
                           SegErr.AL_baseline( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j, reg_iteration ) = dice; %total_field_err; %              
                           [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper(src, mask, uncertainty, invTx, invTy, cert_nstd, ops, slands, tlands, tlands_pre, MCase);
%                           [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_User(src, tar, uncertainty, invTx, invTy, src_seg, cert_nstd, maxd, ops, slands, tlands, tlands_pre);               
                       end            
%                      AllErr.RandG( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.AL( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.SL100( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.SL_All( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.SL_Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.Rand( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;
%                      AllErr.AL_baseline( (img_num-start_img-1)*(len(parameter1)*len(parameter2)) + (i-1)* len(parameter1) + j,1) = initial_warp;

                        Disp = [dx(:) dy(:)];
                        for my_index = 1:size(Disp,1),
                            m = find(Disp(my_index,1)==dispvec(:,1)&Disp(my_index,2)==dispvec(:,2));
                            ind(my_index) = m(1);
                        end
            %             siz_z_p = size(pbs,3);
                        if(Mode==1), Uncertainty.RandG(:,:,reg_iteration,img_num-start_img) = uncertainty; Displace_field.RandG(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==2), Uncertainty.AL(:,:,reg_iteration,img_num-start_img) = uncertainty; Displace_field.AL(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==3), Uncertainty.SL100(:,:,reg_iteration,img_num-start_img) = uncertainty;  Displace_field.SL100(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==4), Uncertainty.SL_All(:,:,reg_iteration,img_num-start_img) = uncertainty;  Displace_field.SL_All(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==5), Uncertainty.SL_Rand(:,:,reg_iteration,img_num-start_img) = uncertainty;  Displace_field.SL_Rand(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==6), Uncertainty.Rand(:,:,reg_iteration,img_num-start_img) = uncertainty; Displace_field.Rand(:,reg_iteration,img_num-start_img) = ind; end
                        if(Mode==7), Uncertainty.AL_Base(:,:,reg_iteration,img_num-start_img) = uncertainty; Displace_field.AL_Base(:,reg_iteration,img_num-start_img) = ind; end
                        if (Mode ~= 3 && Mode ~= 4 && Mode ~= 5)
                           [ dispvec2 regterm2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands_new, slands_new, nseeds);
                            dispvec = [dispvec; dispvec2];
                            dterm_length = size(dterm, 3);
                            dterm2_length = size(dterm2, 3);
                            dterm(:,:, dterm_length+1:dterm_length+dterm2_length) = dterm2;
                            nlabs =  nlabs +  nlabs2;
                            dterm = Change_Dterm(src, slands_new, dterm, nlabs, nlabs2, dterm_mode);
                        else
                            dterm = Change_Dterm_SL(src, slands_new, dterm, pbs, nlabs, dterm_mode);
                        end
                        dterm = Change_Dterm_Neighbors(src, slands_new, dterm, dterm_mode);
                    end
                end
            end

           fprintf('end of iteration : %d\n', reg_iteration);
           reg_iteration = reg_iteration + 1;

        end              
        a=toc(tstart);
        if (Mode== 1), Time.RandG(img_num-start_img) = a; end
        if (Mode== 2), Time.AL(img_num-start_img) = a; end
        if (Mode== 3), Time.SL100(img_num-start_img) = a; end
        if (Mode== 4), Time.SL_All(img_num-start_img) = a; end
        if (Mode== 5), Time.SL_Rand(img_num-start_img) = a; end
        if (Mode== 6), Time.Rand(img_num-start_img) = a; end
        if (Mode== 7), Time.Base(img_num-start_img) = a; end
              
        if (Mode== 1), 
            Landmarks.slands_RandG(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_RandG(:,:,img_num-start_img) = tlands; 
            Label_set.RandG(:,:,img_num-start_img) = dispvec; 
        end
        if (Mode== 2), 
            Landmarks.slands_AL(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_AL(:,:,img_num-start_img) = tlands; 
            Label_set.AL(:,:,img_num-start_img) = dispvec; 
        end
        if (Mode== 3), 
            Landmarks.slands_SL100(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_SL100(:,:,img_num-start_img) = tlands; 
            Label_set.SL100(:,:,img_num-start_img) = dispvec; 
        end
        if (Mode== 4), 
            Landmarks.slands_SL_All(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_SL_All(:,:,img_num-start_img) = tlands; 
            Label_set.SL_All(:,:,img_num-start_img) = dispvec; 
        end        
        if (Mode== 5),
            Landmarks.slands_SL_Rand(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_SL_Rand(:,:,img_num-start_img) = tlands; 
            Label_set.SL_Rand(:,:,img_num-start_img) = dispvec; 
        end
        if (Mode== 6),
            Landmarks.slands_Rand(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_Rand(:,:,img_num-start_img) = tlands; 
            Label_set.Rand(:,:,img_num-start_img) = dispvec; 
        end
        if (Mode== 7),
            Landmarks.slands_AL_Base(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_AL_Base(:,:,img_num-start_img) = tlands; 
            Label_set.AL_Base(:,:,img_num-start_img) = dispvec; 
        end
    end
end   
Save_Mat_set( SegErr, AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Time, trial_num, set_num, str, Landmarks, Noise_Level, MCase);
end
