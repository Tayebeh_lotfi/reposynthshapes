function Tayebeh_RWRPrior_Regression_Parallel(set_num, trial_num, Mode) 

% clear, trial_num = 3; set_num = 6; MCase = 1;
% matlabpool
% matlabpool close
 
add_paths();
%% Getting two images, pre-setting landmarks
[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();

Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

start_img = (set_num-1)*image_set_size; scale = 10E5;
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch
%% Reading data
load ('../data/native_slices_ax2_41');
sze = [256 256];
spacing.height = 20;
spacing.width = 20;
rng(trial_num*10+set_num);

load (['../results/diff_pixel_num/regress_with_unc' num2str(5) '_noise' num2str(noise_sigma*100) '.mat']);
%%
for img_num = start_img + 1 : start_img + image_set_size,
    tar = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
    tar_segs = imrotate(imresize(mat2gray(segs(:,:,img_num)), [sze(1) sze(2)]), 90);
    [src  Tx  Ty] = rBSPwarp( tar, [spacing.height spacing.width], mag, trial_num*10+set_num );
    [mx my]=ndgrid(1:sz(src,1), 1:sz(src,2));
    src_segs =  interp2(my, mx, tar_segs, my + Ty, mx + Tx, 'nearest', 0);      
    D(:,:,1) = Tx; D(:,:,2) = Ty;
    invD = invertDefField(D);
    invTx = invD(:,:,1); invTy = invD(:,:,2);
    src(isnan(src)) = 0;
    src = mat2gray(src);
    mask = src > 0.1;
    S = find(mask>0);
    tar = resc(tar);
    src = resc(imnoise( resc(src), 'gaussian', 0, rand*noise_sigma ));
%    resc(src, imnoise);
    num_seed = 4; Max_Iterations = Pre_Max_Iterations;  reg_mod = 1; 
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(tar);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    slands_pre = slands; tlands_pre = tlands;
    %slands_pre_Less = slands_pre; tlands_pre_Less = tlands_pre;
%% Build graph with 8 lattice
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        nseeds = size(tlands,2); 
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; 
%%
%     for Mode = 1:16, %[1,3,5,6,8,10,11,13,15,16],% 1:16, %1:7, % [2 6 7], %:  % 1: Random Selection       2: Self Learning      3: Active Learning
        slands = slands_pre; tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; 
%% Build graph with 8 lattice
        isz = size(src); nseeds = len(tlands); %nseeds_Less = len(tlands_Less);
        [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
        reg_iteration = 1;
        while(reg_iteration <= Max_Iterations) 
           fprintf('start of iteration : %d, Mode : %d, image number: %d\n', reg_iteration, Mode, img_num);
%% Running probabilistic registration and Calculating the warping error
%             [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma);
            [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, reg_iteration);
%% Calculating Features         
%             [Input Output] = Extract_Features_Less(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err);         
            [Input Output] = Extract_Features(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S); 
%% One of these lines should be used:
%%            
            Pred_Err = B.predict(Input);
            Uncert= Pred_Err; 
            uncertainty = zeros(sz(mask)); 
            uncertainty(S) = Uncert;                   
            Warp_Err = (sqrt(  (dx + invTx).^2 + (dy + invTy).^2 ));
            total_field_err = mean(Warp_Err(S));
            
            [mx my] = ndgrid(1:isz(1), 1:isz(2)); 
            %dsrc_segs = interp2( my, mx, src_segs, my + dy, mx + dx,  'nearest',0); 
            %out = evalSeg(tar_segs, dsrc_segs, unique(tar_segs(:)));     
            
%% Picking top certain pixels
            ops.pick_mode = 6; % 1: Original, 2: Edge, 3: Reduced, 4: Original Uncer, 5: Edge Uncer, 6: Reduce Uncer
            ops.num_seed = num_seed;
           if (Mode==1)
               ops.num_seed = 2;
               AllErr.Rand1( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Rand1_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_segs, maxd, ops, slands, tlands, 1);
           end
           if (Mode==2)
               ops.num_seed = 5;
               AllErr.Rand2( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Rand2_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_segs, maxd, ops, slands, tlands, 1);
           end
          if (Mode==3)
               ops.num_seed = 20;
               AllErr.Rand3( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Rand3_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_segs, maxd, ops, slands, tlands, 1);
           end
           if (Mode==4)
               ops.num_seed = 2;
               AllErr.Grid1( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Grid1_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, invTx, invTy, ops, slands, tlands, reg_iteration, Max_Iterations, 1);
           end
           if (Mode==5)
               ops.num_seed = 5;
               AllErr.Grid2( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Grid2_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, invTx, invTy, ops, slands, tlands, reg_iteration, Max_Iterations, 1);
           end
          if (Mode==6)
               ops.num_seed = 20;
               AllErr.Grid3( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               %AllErr.Grid3_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, invTx, invTy, ops, slands, tlands, reg_iteration, Max_Iterations, 1);
           end
           if (Mode==7)
               ops.num_seed = 2;
               AllErr.AL1( img_num-start_img, reg_iteration ) = total_field_err; % dice; %
              % AllErr.AL1_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands, tlands_pre);
           end
           if (Mode==8)
               ops.num_seed = 5;
               AllErr.AL2( img_num-start_img, reg_iteration ) = total_field_err; % dice; %
               %AllErr.AL2_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands, tlands_pre);
           end
           if (Mode==9)
               ops.num_seed = 20;
               AllErr.AL3( img_num-start_img, reg_iteration ) = total_field_err; % dice; %
               %AllErr.AL3_Seg( reg_iteration ) = out;
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands, tlands_pre);
           end
            if (Mode==10)
                AllErr.SL1( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                %AllErr.SL100_Seg( reg_iteration ) = out;
                AllErr.seed_SL1( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 2;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
            end
            if (Mode==11)
                AllErr.SL2( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                %AllErr.SL200_Seg( reg_iteration ) = out;
                AllErr.seed_SL2( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 5;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
           end
            if (Mode==12)
                AllErr.SL3( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                %AllErr.SL200_Seg( reg_iteration ) = out;
                AllErr.seed_SL3( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 20;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
           end
            if (Mode==13)
                AllErr.SL4( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                %AllErr.SL200_Seg( reg_iteration ) = out;
                AllErr.seed_SL4( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 50;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
           end          
            if (Mode==14)
                AllErr.SL5( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                %AllErr.SL200_Seg( reg_iteration ) = out;
                AllErr.seed_SL5( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 200;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
            end
            if(numel(slands_new) ~= 0 && numel(tlands_new) ~= 0 )
                slands_n = slands;
                slands_n(:,1:4) = [];
%                 if (Mode ~= 10 && Mode ~= 11 && Mode ~= 12)
                   [ dispvec2 regterm2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands_new, slands_new, nseeds);
                    dispvec = [dispvec; dispvec2];
                    dterm_length = size(dterm, 3);
                    dterm2_length = size(dterm2, 3);
                    dterm(:,:, dterm_length+1:dterm_length+dterm2_length) = dterm2;
                    nlabs =  nlabs +  nlabs2;
                    dterm = Change_Dterm(src, slands_n, dterm, nlabs, nlabs2, dterm_mode);
%                 else
%                     dterm = Change_Dterm_SL(src, slands_n, dterm, pbs, nlabs, dterm_mode);
%                 end
                dterm = Change_Dterm_Neighbors(src, slands_n, dterm, dterm_mode);
%             else
                
            end
           fprintf('end of iteration : %d\n', reg_iteration);
           reg_iteration = reg_iteration + 1;

        end  
        switch Mode
            case 1
                Landmarks.slands_Rand1(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Rand1(:,:,img_num-start_img) = tlands; 
            case 2
                Landmarks.slands_Rand2(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Rand2(:,:,img_num-start_img) = tlands;         
            case 3
                Landmarks.slands_Rand3(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Rand3(:,:,img_num-start_img) = tlands; 
            case 4
                Landmarks.slands_Grid1(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Grid1(:,:,img_num-start_img) = tlands; 
            case 5
                Landmarks.slands_Grid2(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Grid2(:,:,img_num-start_img) = tlands;       
            case 6
                Landmarks.slands_Grid3(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_Grid3(:,:,img_num-start_img) = tlands; 
            case 7
                Landmarks.slands_AL1(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_AL1(:,:,img_num-start_img) = tlands; 
            case 8
                Landmarks.slands_AL2(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_AL2(:,:,img_num-start_img) = tlands;       
            case 9
                Landmarks.slands_AL3(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_AL3(:,:,img_num-start_img) = tlands; 
            case 10
                Landmarks.slands_SL1(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_SL1(:,:,img_num-start_img) = tlands; 
            case 11
                Landmarks.slands_SL2(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_SL2(:,:,img_num-start_img) = tlands;       
            case 12
                Landmarks.slands_SL3(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_SL3(:,:,img_num-start_img) = tlands; 
            case 13
                Landmarks.slands_SL4(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_SL4(:,:,img_num-start_img) = tlands;                        
             case 14
                Landmarks.slands_SL5(:,:,img_num-start_img) = slands; 
                Landmarks.tlands_SL5(:,:,img_num-start_img) = tlands;
        end                
%     end
end
Save_Mat_Regress_Parallel( AllErr, Landmarks, savepath, trial_num, set_num, str, noise_sigma, Mode);
end
