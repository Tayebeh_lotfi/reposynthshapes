function Tayebeh_RWRPrior_Set_Regression_test_(set_num, trial_num, MCase) 

% clear, trial_num = 3; set_num = 6; MCase = 1;
% matlabpool
% matlabpool close
 
add_paths();
%% Getting two images, pre-setting landmarks
[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();

Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

start_img = (set_num-1)*image_set_size; scale = 10E5;
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch
%% Reading data
load ('../data/native_slices_ax2_41');
sze = [256 256];
spacing.height = 20;
spacing.width = 20;
% if(strcmp(str,'_small'))
%      magnitude = 7;
% else if (strcmp(str, '_large'))
%         magnitude = 10;
%     end
% end
%%
image_set_size = 4; Pre_Max_Iterations = 5;
for img_num = start_img + 1 : start_img + image_set_size,
%   if (img_num > 28), %img_num = 31; end
    tar = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
    [src  Tx  Ty] = rBSPwarp( tar, [spacing.height spacing.width], mag );
    D(:,:,1) = Tx; D(:,:,2) = Ty;
    invD = invertDefField(D);
    invTx = invD(:,:,1); invTy = invD(:,:,2);
    src(isnan(src)) = 0;
    src = mat2gray(src);
    mask = src > 0.1;
    S = find(mask>0);
    tar = resc(tar);
    src = resc(imnoise( resc(src), 'gaussian', 0, rand*noise_sigma ));
%    resc(src, imnoise);
    num_seed = 4; Max_Iterations = Pre_Max_Iterations;  reg_mod = 1; 
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(tar);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    slands_pre = slands; tlands_pre = tlands;
%     slands_pre_Less = slands_pre; tlands_pre_Less = tlands_pre;
%% Build graph with 8 lattice
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        nseeds = size(tlands,2); 
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; 
%%
    for Mode = [1,2,4], %1:7, % [2 6 7], %:  % 1: Random Selection       2: Self Learning      3: Active Learning  
        tstart=tic; 
        sland = slands_pre; tlands = tlands_pre;
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; 
        if (Mode==7), Max_Iterations = 2; end
%% Build graph with 8 lattice
        isz = size(src); nseeds = len(tlands);
        [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
        reg_iteration = 1;
        while(reg_iteration <= Max_Iterations) 
           fprintf('start of iteration : %d, Mode : %s, image number: %d\n', reg_iteration, Mode_Str{Mode}, img_num);
%% Running probabilistic registration and Calculating the warping error
%             [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma);
                [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, Mode, reg_iteration);
%% Calculating Features         
%             [Input Output] = Extract_Features_Less(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err);         
                [ExpEr ExpErMap] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);    
%% One of these lines should be used:
%% Or this:
%%         
            uncertainty = ExpErMap;
            Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 ));           
            total_field_err = mean(Warp_Err(S));
%% Picking top certain pixels
            ops.pick_mode = 6; % 1: Original, 2: Edge, 3: Reduced, 4: Original Uncer, 5: Edge Uncer, 6: Reduce Uncer
            ops.num_seed = num_seed;
            
           if (Mode==1)
               AllErr.Rand1( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, src_seg, maxd, ops, slands, tlands, 2);
%                ops.num_seed = 4;
%                 AllErr.AL4( img_num-start_img, reg_iteration ) = total_field_err;
%                [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands,  tlands_pre);
           end            
           if (Mode==2)
               AllErr.Rand2( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, src_seg, maxd, ops, slands, tlands, 1);
%                 AllErr.AL10( img_num-start_img, reg_iteration ) = total_field_err;
%                [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands,  tlands_pre);
           end
           if (Mode==3)
                ops.num_seed = 200;
                AllErr.SLGT200( img_num-start_img, reg_iteration ) = total_field_err;
                AllErr.seed_SLGT200( img_num-start_img, reg_iteration ) = nseeds;
%                 [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands);
           end            
           if (Mode==4)
                ops.num_seed = 200;
                AllErr.SL200( img_num-start_img, reg_iteration ) = total_field_err;
                AllErr.seed_SL200( img_num-start_img, reg_iteration ) = nseeds;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
%                 [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands);
           end 
            Disp = [dx(:) dy(:)];
            for my_index = 1:size(Disp,1),
                m = find(Disp(my_index,1)==dispvec(:,1)&Disp(my_index,2)==dispvec(:,2));
                ind(my_index) = m(1);
            end
            
          if(Mode==2), 
               Uncertainty.AL(:,:,reg_iteration,img_num-start_img) = uncertainty; 
               Displace_field.AL(:,reg_iteration,img_num-start_img) = ind; 
           end
           if(Mode==4), 
               Uncertainty.SL200(:,:,reg_iteration,img_num-start_img) = uncertainty;  
               Displace_field.SL_All(:,reg_iteration,img_num-start_img) = ind; 
           end   
           
            if(numel(slands_new) ~= 0 && numel(tlands_new) ~= 0 )
                slands_n = slands;
                slands_n(:,1:4) = [];
%                 slands_n_Less = slands;
%                 slands_n_Less(:,1:4) = [];
                if (Mode ~= 3 && Mode ~= 4)
                   [ dispvec2 regterm2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands_new, slands_new, nseeds);
                    dispvec = [dispvec; dispvec2];
                    dterm_length = size(dterm, 3);
                    dterm2_length = size(dterm2, 3);
                    dterm(:,:, dterm_length+1:dterm_length+dterm2_length) = dterm2;
                    nlabs =  nlabs +  nlabs2;                   
                    dterm = Change_Dterm(src, slands_n, dterm, nlabs, nlabs2, dterm_mode);
                else
                    dterm = Change_Dterm_SL(src, slands_n, dterm, pbs, nlabs, dterm_mode);
                end
                dterm = Change_Dterm_Neighbors(src, slands_n, dterm, dterm_mode);
%             else
                
            end
           fprintf('end of iteration : %d\n', reg_iteration);
           reg_iteration = reg_iteration + 1;

        end              
        a=toc(tstart);
        if (Mode== 2), Time.AL(img_num-start_img) = a; end
        if (Mode== 4), Time.SL200(img_num-start_img) = a; end      
        
        if (Mode== 2), 
            Landmarks.slands_AL(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_AL(:,:,img_num-start_img) = tlands; 
            Label_set.AL(:,:,img_num-start_img) = dispvec;             
        end        
        if (Mode== 4), 
            Landmarks.slands_SL200(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_SL200(:,:,img_num-start_img) = tlands; 
            Label_set.SL200(:,:,img_num-start_img) = dispvec;
        end  
    end   
end
Save_Mat_set_Regress_test( AllErr, Label_set, Displace_field, Uncertainty, Landmarks, savepath, ...,
    Max_Iterations, num_seed, Time, trial_num, set_num, str, MCase, noise_sigma);
end
