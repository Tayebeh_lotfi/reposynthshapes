function [TVx TVy TVz]=label2field( L, T , show)
% function [TVx TVy]=label2field( L, T )
%       converts label L to vector field T

    T=chkDim(T);

    [ndim nlabs]=size(T);
    
    if ndim==2;
%             hon;
            TVx=zeros(sz(L));
            TVy=TVx; 
            for l=1:max(L(:))
                ii=find( L == l);
                TVx(ii) = T(1,l);
                TVy(ii) = T(2,l);
            end

        if nargin==3
        c=  histc(L(:),1:max(L(:)));
        T(:, ( find(c) ) )
        end
    elseif ndim==3
        
        TVx=zeros(sz(L));
        TVy=TVx;  TVz = TVx;
        for l=1:max(L(:))
            ii=find( L == l);
            TVx(ii) = T(1,l);
            TVy(ii) = T(2,l);
            TVz(ii) = T(3,l);
        end

    end
    
