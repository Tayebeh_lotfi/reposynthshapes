%% Creating Data
add_paths()
clc, close all, clear all,

real_mode = 2; 
[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
Visualization = 0;    
spacing.height = 20; spacing.width = 20;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
direction = 2; data_mode = 2; epsilon_ = 1e-2;
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch
load '../data/texture/texture1.mat';
I = I(151:350, 151:350);
% I = imresize(I, [100 100]);
% I(:, 100:end) = 0;
I = Circle_Artfct(I, 60, 65, 15);
spacing.height = 10;
spacing.width = 10;
magnitude = 5;
[IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], magnitude );
D(:,:,1) = TxW; D(:,:,2) = TyW;
invD = invertDefField(D);
invTxW = invD(:,:,1); invTyW = invD(:,:,2);
meanwarpb = mean(invTxW(:) .^2 + invTyW(:) .^2);
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    %Tx2D = Tx; Ty2D = Ty; %
    num_seed = 4; Max_Iterations = 5; alpha = 5000; reg_mod = 3; beta = 0.0005; %alpha = 5000.00;
    mask = ones(sz(I)); % zeros(sz(I)); mask(50:130, 50:130) = 1;
%% Visualizing Data
    % close all;
    % if (Mode ~=3)
    %     dockf;quiverR(invTy2D, invTx2D, 20, 2, 'c' ), title('ground truth');
    % end
    % dockf; clr_labels = [1:97 2 1 1]; %random labels
    % plotvec( rand(3,100), clr_labels );
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre2D = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);    
%% Calculating the likelihood
% for res = [10 50 100 200 1000], %[2 5 6],
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
    %%   Visualization
    if(Visualization)    
        image = src;
        dockf; imagesc(image), colormap(gray),
        [y,x,button] = ginput(1);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
        while (button ~=3)
                image = src;
                x = round(x); y = round(y);
                prob1 = sqz(pbs(x,y,1:end));
                prob2 = sqz(pbs_sprd(x,y,1:end));
                prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
                prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);
                imagesc(image), colormap(gray), hold on, plot(y,x, 'r*'), 
                title(['pdf at point (', num2str(x), ',', num2str(y), ')']);
                dockf;
                subplot 121, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                subplot 122, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                colormap(jet)
%               dockf; 
%                 subplot 132, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'), 
%                 subplot 133, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs_sprd(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'),
%        %     end
            dockf; imagesc(image), colormap(gray),
            [y,x,button] = ginput(1);
        end
    end
    
%% Calculating Uncertainty
    [Unc_Shannon, Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, data_mode);
    [Unc_ExpEr, Unc_ExpErMap] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);   
    [WCov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
    
    [Unc_ExpEr_polar, Unc_ExpErMap_polar] = Calc_Uncertainty_Err_Polar(pbs, dispvec, dx, dy);
    
%    [Unc_dterm ~] = Calc_Uncertainty_Shannon(dterm);

%     Uncert_Mode = 2; % 0: KNN, 1: KNN&WCov, 2: KNN&KDE&WCov,  3: All,
%     accuracy=3; %accuracy, pbs .* 10^(opt.k)
%     iter1 = 1000; nlabs_size = 23;
%     [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
%         pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);

    Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 )); 
    Warp_Err_Sprd = (sqrt(  (dx_sprd - invTx).^2 + (dy_sprd - invTy).^2 ));
    Unc_Shannon1 = resc(Unc_Shannon);
%     Unc_Shannon_Sprd1 = resc(Unc_Shannon_Sprd);
    WCov1 = resc(WCov);
    Unc_ExpEr1 = resc(Unc_ExpEr);
    Unc_ExpErMap1 = resc(Unc_ExpErMap);
    Unc_ExpEr_polar1 = resc(Unc_ExpEr_polar);
    Unc_ExpErMap_polar1 = resc(Unc_ExpErMap_polar);
    
    
    dockf; subplot 131, imagesc(I); title('Target Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 132, imagesc(IWF); title('Source Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 133, imagesc(dsrc); title('Registered Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    
    figure, subplot 231, imagesc(Warp_Err), title('Reg Error', 'FontSize', 35), axis equal, colormap(gray), axis tight, %cbar, set(gca, 'FontSize', 35);
    set(gca,'YTick',[]); set(gca,'XTick',[]);
%     subplot 232, imagesc(WCov1), title('WCov', 'FontSize', 35), axis equal, colormap(gray), axis tight,%, cbar, set(gca, 'FontSize', 35);
%     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 232, imagesc(Unc_Shannon1), title('ShEnt', 'FontSize', 35), axis equal, colormap(gray), axis tight,%, cbar, set(gca, 'FontSize', 35);
    set(gca,'YTick',[]); set(gca,'XTick',[]);

    subplot 233, imagesc(Unc_ExpEr1), title('ExpEr', 'FontSize', 35), axis equal, colormap(gray), axis tight, %cbar, set(gca, 'FontSize', 35);
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 234, imagesc(Unc_ExpEr_polar1), title('ErPol', 'FontSize', 35), axis equal, colormap(gray), axis tight,
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 235, imagesc(Unc_ExpErMap1), title('ExpErMAP', 'FontSize', 35), axis equal, colormap(gray), axis tight, %cbar, set(gca, 'FontSize', 35);
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 236, imagesc(Unc_ExpErMap_polar1), title('ErMAPPol', 'FontSize', 35), axis equal, colormap(gray), axis tight, 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
     
     
     
%     dockf; imagesc(Unc_Err_Dist1), title('Error Distribution Uncertainty', 'FontSize', 25), cbar, set(gca, 'FontSize', 25);
%     set(gca,'YTick',[]); set(gca,'XTick',[]);
%     dockf; imagesc(Unc_Shannon1), title('Shannon Uncertainty', 'FontSize', 25), cbar, set(gca, 'FontSize', 25);
%     set(gca,'YTick',[]); set(gca,'XTick',[]);    
    S = find(mask==1);        
    for Unc_Mode = [1 3 4 5], %[1 5], %
        switch Unc_Mode
            case 1
                Unc = Unc_Shannon1;
            case 2
                 Unc = Unc_Shannon_Sprd1; % WCov1; % 
            case 3
                Unc = Unc_ExpEr1; % Wcov1;
            case 4
                Unc = Unc_ExpErMap1; % SGauss1;
            case 5
                Unc = WCov1;
            case 11
                Unc = GMMavg2; % Unc4;
            case 17
                Unc = Unc_kde;
            case 18
                Unc = Unc_Knn;
            case 19
               Unc = Unc_Delta;
        end
        Error = Warp_Err; % err_entrpy; % Warp_Err; %  err_mean; % 
        visualize_relation_uncert_err(tar, Unc, Error, Unc_Mode, Unc_str);   
    end
% end

%%

% img  =  zeros(sz(src,1), sz(src,2), 3 );
% mn = mnx(Warp_Err); mask = tar>0.1;
% S = Warp_Err < (mn(1) + 0.1 * (mn(2)-mn(1))) .* mask;
% img(:,:,1) = S;
% 
% 
% S = (Warp_Err < (mn(1) + 0.6 * (mn(2)-mn(1)))) & (Warp_Err > (mn(1) + 0.1 * (mn(2)-mn(1)))) .* mask;
% img(:,:,2) = S;
% 
% 
% S = (Warp_Err > (mn(1) + 0.6*(mn(2)-mn(1)))) .* mask;
% img(:,:,3) = S;
% figure,
% subplot 121, imagesc(tar), title('Original Image', 'FontSize', 35), 
% set(gca,'YTick',[]); set(gca,'XTick',[]); 
% subplot 122, imagesc(img), title('Error Map', 'FontSize', 35), 
% set(gca,'YTick',[]); set(gca,'XTick',[]); 
% cbar, set(gca, 'FontSize', 35) 