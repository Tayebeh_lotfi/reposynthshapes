function Regression_Cluster(trial)
%% Creating Data
add_paths()
% clc, close all, clear all,
%%
[ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
% trial = 3;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; %MIND
% load(['../data/Precomputed/trial' num2str(trial_num) str '_downsample.mat']);
load ('../data/native_slices_ax2_41');
sze = [256 256];
%% Simplest case, Same warping

spacing.height = 20;
spacing.width = 20;
%% 
countertrn = 1; countertes = 1;
max_size = 1000 * trial;
for img = 1 : 20 %10, %  sz(tar3D,3),
    tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
    for warp = 1 : 5 %1, % 
        [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta);
        Input_Less = Input(:,1:end-2); % Input_Less = Input(:,1:end-2); % Input(:,end-1:end); 
%         Input = Input(:,1:end-2); 
%         [Input_Less, Output_Less] = RunAlg_Less(tar, gamma, dterm_mode, res, spacing, magnitude, Lbl_mod, simil_measure);
        p = randperm(sz(Input,1));
        Input = Input(p,:); Output = Output(p,:);
        Input_Less = Input_Less(p,:);
        trainInput(countertrn:countertrn + max_size - 1, :) = Input(1:max_size,:);
        trainOutput(countertrn:countertrn + max_size - 1, :) = Output(1:max_size,:);
        trainInput_Less(countertrn:countertrn + max_size - 1, :) = Input_Less(1:max_size,:);
        trainOutput_Less(countertrn:countertrn + max_size - 1, :) = Output(1:max_size,:);        
        countertrn = countertrn + max_size - 1;
        fprintf('\nstart of iteration : %d, image number: %d\n', warp, img);
    end
end
% X1 = trainInput(1:4:end,:);
% Y1 = trainOutput(1:4:end,:);
B_Less=TreeBagger(80,trainInput_Less,trainOutput_Less,'method', 'regression','OOBPred','on','oobvarimp','on');
B=TreeBagger(80,trainInput,trainOutput,'method', 'regression','OOBPred','on','oobvarimp','on');

save (['../results/Learning/New/learning_without_unc', num2str(trial), '.mat'], 'B_Less');
save (['../results/Learning/New/learning_with_unc', num2str(trial), '.mat'], 'B');
end
            