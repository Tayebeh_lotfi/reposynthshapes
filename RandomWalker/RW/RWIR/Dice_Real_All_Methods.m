clc, close all, clear all,
add_paths();

%% Reading data

[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
savepath = '../results/AL_SL/Regression/ExpEr/windows/';
load ('../data/slice103_10vols');
load (['../results/diff_pixel_num/regress_with_unc' num2str(3) '_noise' num2str(noise_sigma*100) '.mat']);

Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements
scale = 10E5;
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch

for img_num = 1:10,
    for img_num2 = 1:10,
        for Mode = 1:8,
            if(img_num ~= img_num2)
%% Getting two images, pre-setting landmarks
                rng(img_num*10 + img_num2);
                tar = (mat2gray(imgs(:,:,img_num)));
                tar_segs = segs(:,:,img_num);
                invTx = 15*ones(sz(tar)); invTy = invTx;
                tar = resc(tar);
                num_seed = 4; Max_Iterations = Pre_Max_Iterations; reg_mod = 1; 
%%
                src = (mat2gray(imgs(:,:,img_num2)));
                src_segs = segs(:,:,img_num2);
                src(isnan(src)) = 0;
                src = mat2gray(src);
                mask = src > 0.1;
                S = find(mask>0);
                src = resc(imnoise( resc(src), 'gaussian', 0, rand*noise_sigma ));
%%
                tlands = []; slands = [];
                distx = 1; disty = 1;
                sze = size(tar);
                tlands(:,1) = [distx; disty]; 
                tlands(:,2) = [distx; sze(2)-disty]; 
                tlands(:,3) = [sze(1)-distx; disty]; 
                tlands(:,4) = [sze(1)-distx; sze(2)-disty];
                slands = tlands;
                slands_pre = slands; tlands_pre = tlands;
%% Build graph with 8 lattice
                nseeds = size(tlands,2);
%% Calculating the likelihood
                [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
                dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; 
%%
                tstart=tic; 
                slands = slands_pre; tlands = tlands_pre;
                dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; 
%% Build graph with 8 lattice
                isz = size(src); nseeds = len(tlands);
                [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
                reg_iteration = 1;
                while(reg_iteration <= Max_Iterations) 
                    fprintf('start of iteration : %d, Mode : %d, image number: %d\n', reg_iteration, Mode, img_num);
%% Running probabilistic registration and Calculating the warping error
                    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, Mode, reg_iteration);
%% Calculating Features         
                    [Input Output] = Extract_Features(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S); 

%%         
                    Pred_Err = B.predict(Input);
                    Uncert= Pred_Err; 
                    uncertainty = zeros(sz(mask)); 
                    uncertainty(S) = Uncert;                   

                    [mx my] = ndgrid(1:isz(1), 1:isz(2)); 
                    dsrc_segs = interp2( my, mx, src_segs, my + dy, mx + dx,  'nearest',0); 
                    out = evalSeg(tar_segs, dsrc_segs, unique(tar_segs(:)));     

%% Picking top certain pixels
                    ops.pick_mode = 6; % 1: Original, 2: Edge, 3: Reduced, 4: Original Uncer, 5: Edge Uncer, 6: Reduce Uncer
                    ops.num_seed = num_seed;
                   if (Mode==1)
                       ops.num_seed = 5;
                       AllErr.Rand1_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, dx, dy, mask, src_segs, maxd, ops, slands, tlands, 2);
                   end
                  if (Mode==2)
                       ops.num_seed = 20;
                       AllErr.Rand2_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, dx, dy, mask, src_segs, maxd, ops, slands, tlands, 2);
                   end
                   if (Mode==3)
                       ops.num_seed = 5;
                       AllErr.Grid1_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, dx, dy, ops, slands, tlands, reg_iteration, Max_Iterations, 2);
                   end
                  if (Mode==4)
                       ops.num_seed = 20;
                       AllErr.Grid2_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, dx, dy, ops, slands, tlands, reg_iteration, Max_Iterations, 2);
                   end

                   if (Mode==5)
                       ops.num_seed = 5;
                       AllErr.AL1_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, dx, dy, ops, slands, tlands, tlands_pre, 2);
                   end
                  if (Mode==6)
                       ops.num_seed = 20;
                       AllErr.AL2_Seg( reg_iteration ) = out;
                       [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, dx, dy, ops, slands, tlands, tlands_pre, 2);
                   end
                    if (Mode==7)
                        AllErr.SL1_Seg( reg_iteration ) = out;
                        AllErr.seed_SL1(reg_iteration ) = nseeds;
                        ops.num_seed = 5;
                        [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
                    end
                    if (Mode==8)
                        AllErr.SL2_Seg( reg_iteration ) = out;
                        AllErr.seed_SL2(reg_iteration ) = nseeds;
                        ops.num_seed = 20;
                        [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
                    end

                    if(numel(slands_new) ~= 0 && numel(tlands_new) ~= 0 )
                        slands_n = slands;
                        slands_n(:,1:4) = [];
                           [ dispvec2 regterm2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands_new, slands_new, nseeds);
                            dispvec = [dispvec; dispvec2];
                            dterm_length = size(dterm, 3);
                            dterm2_length = size(dterm2, 3);
                            dterm(:,:, dterm_length+1:dterm_length+dterm2_length) = dterm2;
                            nlabs =  nlabs +  nlabs2;                   
                            dterm = Change_Dterm(src, slands_n, dterm, nlabs, nlabs2, dterm_mode);
                        dterm = Change_Dterm_Neighbors(src, slands_n, dterm, dterm_mode);
                    end
                   fprintf('end of iteration : %d\n', reg_iteration);
                   reg_iteration = reg_iteration + 1;

                end 

                switch Mode
                    case 1
                        Landmarks.slands_Rand1 = slands; 
                        Landmarks.tlands_Rand1 = tlands; 
                    case 2
                        Landmarks.slands_Rand2 = slands; 
                        Landmarks.tlands_Rand2 = tlands;         
                    case 3
                        Landmarks.slands_Grid1 = slands; 
                        Landmarks.tlands_Grid1 = tlands; 
                    case 4
                        Landmarks.slands_Grid2 = slands; 
                        Landmarks.tlands_Grid2 = tlands;       
                    case 5
                        Landmarks.slands_AL1 = slands; 
                        Landmarks.tlands_AL1 = tlands; 
                    case 6
                        Landmarks.slands_AL2 = slands; 
                        Landmarks.tlands_AL2 = tlands;       
                    case 7
                        Landmarks.slands_SL1 = slands; 
                        Landmarks.tlands_SL1 = tlands; 
                    case 8
                        Landmarks.slands_SL2 = slands; 
                        Landmarks.tlands_SL2 = tlands;                        
                end       
                file_name = {'AllErr' 'Landmarks'};    
                for i = 1:2,%6
                    save (num2str([savepath file_name{i} num2str(img_num) '_to_img' num2str(img_num2) '_Mode' num2str(Mode) '_regress' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
                end
%                 Save_Mat_Regress_seg( AllErr, Landmarks, savepath, img_num, img_num2, noise_sigma, Mode);
            end
        end
    end
end

