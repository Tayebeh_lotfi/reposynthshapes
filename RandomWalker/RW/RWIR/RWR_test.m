%% Creating Data
% cd 'H:\students_less\tlotfima\RWRegistration_Final2D\RWIR'
add_paths()
clc, close all, clear all,

global count;
global_initialize(1);
%%
cond1 = 0;
cond2 = 0;
Phantom = 1;
Checkerboard = ~Phantom;
Mode = 8; Visualization = 0; 
real_mode = 1; synth = 0; test3D = 0;

[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();

spacing.height = 20; spacing.width = 20;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
direction = 2;
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch
switch Mode % 1: real data, 2: Phantom, or Checkerboard, 3: Synthetic data
    case 1
        trial_num = 1;
        load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);  
        IWF = double(mat2gray(src3D(:,:,1)));
        I = double(mat2gray(tar3D(:,:,1)));
% %         tlands2D = tlands3D(:,:,1); slands2D = slands3D(:,:,1); 
%         TxW= Tx3D(:,:,1); TyW= Ty3D(:,:,1); invTxW = invTx3D(:,:,1); invTyW = invTy3D(:,:,1); 
          %load ../data/Bspline.mat;
    case 2
        if Phantom
            II = phantom('Modified Shepp-Logan',50);
            I = zeros(250,250);
            I(25:224,25:224) = II;
            h = fspecial('gaussian', hsize, sigma);
            I = imfilter(I,h);
%             I = imnoise(I,'gaussian',mean_,vari_);
%             mask1 = I;
            [IWF TxW TyW] = Trans_Warp( I, mag, direction );
%            [IWF TxW TyW] = affineWarp( I, mag, direction );  
%             [IWF TxW TyW] = rBSPwarp( I, [spacing.height spacing.width], mag, mask1 );
            mask1 = (IWF > 0.1);
            IWF = imnoise(IWF,'gaussian',mean_,vari_);       
            D(:,:,1) = TxW; D(:,:,2) = TyW;
            invD = invertDefField(D);
            invTxW = invD(:,:,1); invTyW = invD(:,:,2);
    %     dockf;imagesc(P);
        else
                II = checkerboard(25);I = zeros(250,250);
                I(25:224,25:224) = II;
                h = fspecial('gaussian', hsize, sigma);
                I = imfilter(I,h);
%                 I = imnoise(I,'gaussian',mean_,vari_);
                mask1 = I;
                [IWF TxW TyW] = Trans_Warp( mask1, mag, direction );
%                 [IWF TxW TyW] = rBSPwarp( mask, [spacing.height spacing.width], mag, mask );
                mask1 = (IWF > 0.1);
                IWF = imnoise(IWF,'gaussian',mean_,vari_);       
                D(:,:,1) = TxW; D(:,:,2) = TyW;
                invD = invertDefField(D);
                invTxW = invD(:,:,1); invTyW = invD(:,:,2);
        end
    case 3 
        mag = 30;
        IWF = mat2gray(rgb2gray(imread('1_2.bmp')));
        I = mat2gray(rgb2gray(imread('1_1.bmp')));
        TxW = 50*ones(size(I)); TyW = zeros(size(I)); invTxW = TxW; invTyW = TyW;
    case 4
        load '../data/Lung_Img';
        I = mat2gray(Lung_Img(:,:,30));
        mask1 = I;
        [IWF TxW TyW] = rBSPwarp( I, [spacing.height spacing.width], mag, mask1 );
        D(:,:,1) = TxW; D(:,:,2) = TyW;
        invD = invertDefField(D);
        invTxW = invD(:,:,1); invTyW = invD(:,:,2);
    case 5
        load '../data/Diffusion.mat';
    case 6
%         load('Y:\students_less\lisat\report\UGReg\UncertaintyGuidedRegistration\RWImgRegCode\RWexample_pbs.mat','pbs');
        load ../data/tumor_data/tumor.mat;
        I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);%mat2gray(I); IWF = mat2gray(IWF);
        mask = IWF>0.1;
        TxW = 10*ones(size(I)); TyW = zeros(size(I)); invTxW = TxW; invTyW = TyW;
    case 7
        [debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
            image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();  
        hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05; trial_num = 1;
        real_mode = 1;
        load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);
        src3D = resc(src3D);
        tar3D = resc(tar3D);
        % segment3D = resc(segment3D);
        t = 5; s = 20; I = tar3D(:,:,t); IWF = tar3D(:,:,s); 
        src_seg = segment3D(:,:,s); tar_seg = segment3D(:,:,t);
        mask = src_seg>0.1;
        noise_sigma = 0.01;
        TxW = 15*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
    case 8
        load ('../data/native_slices_ax2_41');
        sze = [256 256];
        spacing.height = 20;
        spacing.width = 20; img_num = 5;
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        [IWF  TxW  TyW] = rBSPwarp( I, [spacing.height spacing.width], mag );
        D(:,:,1) = TxW; D(:,:,2) = TyW;
        invD = invertDefField(D);
        invTxW = invD(:,:,1); invTyW = invD(:,:,2);
        IWF(isnan(IWF)) = 0;
        IWF = mat2gray(IWF);
        mask = IWF > 0.1;
        S = find(mask>0);
        I = resc(I);
end
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    %Tx2D = Tx; Ty2D = Ty; %
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Visualizing Data
    % close all;
%     dockf; subplot 121, imagesc(tar2D); title('target image'),colormap(gray)
%     subplot 122, imagesc(src2D); title('source image'),colormap(gray)
    % if (Mode ~=3)
    %     dockf;quiverR(invTy, invTx, 20, 2, 'c' ), title('ground truth');
    % end
    % dockf; clr_labels = [1:97 2 1 1]; %random labels
    % plotvec( rand(3,100), clr_labels );
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
    
%% Calculating the likelihood
    % [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar2D, src2D, Tx2D, Ty2D, nseeds, res, mag);
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    if (cond1==1)
        i=1; j=1;
        % tstart=tic;
        cert_nstd = parameter1(j);
        lambda = parameter2(i);
    end
    slands = slands_pre; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 
%% Solving the problem
    [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
%% Calculating uncertainties
    mind_src=MIND_descriptor2d(src,2);
    mind_tar=MIND_descriptor2d(dsrc,2);
    diff = sum( (mind_tar - mind_src).^2, 3 );
    %     diff2D = dsrc2D - src2D;

    % homogeneous = mat2gray(Calc_Uncertainty(dterm));
    % b = 7; homogeneous = nlfilter(src2D, [b b], @local_edge_count);
    % m = mean(homogeneous(:)); sig = std(homogeneous(:));
    % homogeneous(homogeneous < m + sig ) = 0; homogeneous(homogeneous ~= 0) = 1;
    b=7; h = fspecial('average', b);
    homg = imfilter(src,h, 'same');
    homogeneous = mat2gray(abs(src - homg));
    if (Mode==3)
        mask1 = (src < 0.4);
    else
        if(Mode ~= 2)
            mask1 = (src > 0.1);
        end
    end
    mask2 = (homogeneous > mean(homogeneous(:))+0.3*std(homogeneous(:)));
    if(Mode == 1 || Mode == 5)
        mask3 = ( src_seg > 0); se = strel('disk',15); mask3 = imdilate(mask3,se);
    %     mask1 = mask1 .* mask3;
    end
    mask = mask1;
%% Calculating Uncertainty
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);
    [Unc_dterm ~] = Calc_Uncertainty_Shannon(dterm);
    if(Mode == 7)   
%% Calculating warped segment
    [mx my] = ndgrid(1:isz(1), 1:isz(2));
    dsrc_seg = interp2( my, mx, src_seg, my + dy, mx + dx,  '*linear', 0); % nearest
    dsrc_seg_sprd = interp2( my, mx, src_seg, my + dy_sprd, mx + dx_sprd,  '*linear', 0); % nearest
%% Segment Errors
    Err_bef_seg = sqrt( sum( (src_seg(:)-tar_seg(:)) .^2 ) );
    Err_aft_seg = sqrt( sum( (dsrc_seg(:)-tar_seg(:)) .^2 ) );
    Err_aft_seg_sprd = sqrt( sum( (dsrc_seg_sprd(:)-tar_seg(:)) .^2 ) );
    Err_bef = sqrt( sum( (src(:)-tar(:)) .^2 ) );
    Err_aft = sqrt( sum( (dsrc(:)-tar(:)) .^2 ) );
    Err_aft_sprd = sqrt( sum( (dsrc_sprd(:)-tar(:)) .^2 ) );
    else
        if(Mode == 1 || Mode == 5)
            Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 )); 
            Warp_Err_Sprd = (sqrt(  (dx_sprd - invTx).^2 + (dy_sprd - invTy).^2 )); 
            figure, plotmatrix(mat2gray(Unc_Shannon_Sprd(mask)),Warp_Err(mask));
            xlabel('Uncertainty', 'FontSize', 25), ylabel('Error', 'FontSize', 25), title('Error vs Uncertainty', 'FontSize', 25);            
%             MCase = 2;
%             Stat_Err_Unc(Unc_Shannon, Unc_Shannon_Sprd, zeros(size(Unc_Shannon)), zeros(size(Unc_Shannon)), Warp_Err, Warp_Err_Sprd, mask, 2, 1, MCase); 
        end
    end   
%% Calculating Entropy of Error
%     [err_pbs err_pbs_sprd] = Calculate_Prob_Error(dispvec, pbs, pbs_sprd, invTx2D, invTy2D, numel(mask));
%     err_mode = mode(err_pbs,2); err_mode_sprd = mode(err_pbs_sprd,2);
%     err_mean = mean(err_pbs,2); err_mean_sprd = mean(err_pbs_sprd,2);
%     err_mode = reshape(err_mode, [isz(1), isz(2)]); err_mode_sprd = reshape(err_mode_sprd, [isz(1), isz(2)]);
%     err_mean = reshape(err_mean, [isz(1), isz(2)]); err_mean_sprd = reshape(err_mean_sprd, [isz(1), isz(2)]);
%     [err_entrpy err_entrpy_sprd] = Calc_Uncertainty_Shannon(err_pbs, err_pbs_sprd, 2);
%     err_entrpy = reshape(err_entrpy, [isz(1), isz(2)]); err_entrpy_sprd = reshape(err_entrpy_sprd, [isz(1), isz(2)]);
%     err_entrpy = mat2gray(err_entrpy); % 
%%
    % mask = mask1;
    % %% Added Part
    % w = [0.2 0.3 0.1];
    % Unc8 = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
%% Adding Part
    % inds = find(mask>0);
    % pix = inds(2);
    % z = reshape(pbs, prod(isz), []);
    % cftool( dispvec(:,1), dispvec(:,2), z(pix,:) )
    % a1 ./ (b1+(exp(-c1*x) * exp(-d1*y))) + a2 ./ (b2+(exp(-c2*x) * exp(-d2*y)))

%% End of commented Part
if(real_mode)
%% Showing results
    dockf; subplot 221, imagesc(tar); title('target image'),
    subplot 222, imagesc(src); title('source image'),
    subplot 223, imagesc(dsrc); colormap(gray), title('registered image'),
    subplot 224, imagesc(dsrc_sprd); colormap(gray), title('modfd registered image'),

    if(Mode == 7)
        dockf; 
        subplot 131, imagesc(src_seg), title(['Error before registration= ', num2str(Err_bef_seg)]);
        subplot 132, imagesc(dsrc_seg), title(['Error = ', num2str(Err_aft_seg)]);
        subplot 133, imagesc(dsrc_seg_sprd), title(['Error Modfd = ', num2str(Err_aft_seg_sprd)]);
    else
        dockf; 
        subplot 121, imagesc(flowToColor(invTx-dx, invTy-dy )), title('Warping Error'), cbar, colormap jet;
        subplot 122, imagesc(flowToColor(invTx-dx_sprd, invTy-dy_sprd )), title('Warping Error Modfd'), cbar, colormap jet;
        Stat_Err_Unc(Unc_Shannon, Unc_Shannon_Sprd, Warp_Err, Warp_Err_Sprd, mask);
    end
    dockf; 
    subplot 131, imagesc( mat2gray(Unc_Shannon) ), title([Unc_str{1} ' Entropy']), cbar;
    subplot 132, imagesc( mat2gray(Unc_Shannon_Sprd) ), title(['Modified ', Unc_str{1}]), cbar;
    subplot 133, imagesc( diff ), title('MIND diff'), cbar;
    dockf; subplot 121, quiverR(dy, dx, 2, 2, 'c'), title('Solution');
    subplot 122, quiverR(dy_sprd, dx_sprd, 2, 2, 'c'), title('Solution Modified');
%     subplot 224, imagesc(mask), title('Mask'), cbar; % Homogeneous Regions


    %%   Visualization
    if(Visualization)
%     Visualize_MoG(dispvec, pbs, opt);
    
        image = tar;
        dockf; imagesc(image), colormap(gray),
        [y,x,button] = ginput(1);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
        while (button ~=3)
%               for ind = 1:size(vector,2),
                image = tar;
                x = round(x); y = round(y);
                prob1 = sqz(pbs(x,y,1:end));
%                 prob1 = sqz(dterm(x,y,1:end));
                prob2 = sqz(pbs_sprd(x,y,1:end));
                prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
                prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);
                imagesc(image), colormap(gray), hold on, plot(y,x, 'r*'), 
                title(['pdf at point (', num2str(x), ',', num2str(y), ')']);
                dockf; subplot 121, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
                set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
                subplot 122, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
                set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
%                 Calc_Vis_dist(x, y, tar, src, dispvec, pbs, pbs_sprd, prob1, prob2, res, invTx, invTy, maxd);
              dockf; 
%                 subplot 132, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'), zlabel('prob'), 
%                 subplot 133, plot3(dispvec(:,1) , dispvec(:,2) , 100000*sqz(pbs_sprd(x,y,:)), 'b*'), ...
%                     xlabel('vector x'), ylabel('vector y'),
%                     zlabel('prob'),
%        %     end
            dockf; imagesc(image), colormap(gray),
            [y,x,button] = ginput(1);
            
            
            
%             
%             figure,quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on, surf(gridX, gridY, prob1)
%             
%             figure,quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on, surf(gridX, gridY, prob1)
%             xlabel('x'), ylabel('y'),zlabel('prob (p)'),

            
        end
    end
    
    
    %% TRE Target Registration Error
    if( Mode ==7 )
%         dsrc_old = dsrc;
%         dsrc = dsrc_sprd;
        Svect = []; Tvect = []; dSvect = []; counter = 10; c = jet(200);

        dockf; subplot 133, imagesc(dsrc), colormap(gray), title('Moving -> Fix', 'FontSize', 35),
        subplot 132, imagesc(tar), colormap(gray), title('Fixed Image', 'FontSize', 35),
        subplot 131, imagesc(src), colormap(gray), title('Moving Image', 'FontSize', 35),
        [y,x,button] = ginput(1);
        x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*'),
        Svect = [Svect; [x y]];
        Sinds = sub2ind(size(dx), x, y);
%         hold on, quiverR(y, x, dy(Sinds),dx(Sinds),1,2,'r');           
        dSx = x - dx(Sinds);
        dSy = y - dy(Sinds);
        dSvect = [dSvect; [dSx dSy]];
        subplot 133, imagesc(dsrc), colormap(gray), title('Moving -> Fix', 'FontSize', 35), 
        hold on, plot(dSvect(:,2), dSvect(:,1), 'Color', c(counter, :), 'Marker', '*');
        subplot 132, imagesc(tar), colormap(gray), title('Fixed Image', 'FontSize', 35),
        [y,x,button] = ginput(1);
        x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*'),
        Tvect = [Tvect; [x y]];
        while (button ~=3)
            counter = counter + 10;
            subplot 133, imagesc(dsrc), colormap(gray), hold on, plot(dSvect(:,2), dSvect(:,1), 'r*'),
            title('Moving -> Fix', 'FontSize', 35),
            subplot 132, imagesc(tar), colormap(gray),hold on, plot(Tvect(:,2), Tvect(:,1), 'r*'),
            title('Fixed Image', 'FontSize', 35);
            subplot 131, imagesc(src), colormap(gray),hold on, plot(Svect(:,2), Svect(:,1), 'r*'), 
            title('Moving Image', 'FontSize', 35);
            [y,x,button] = ginput(1);
%             hold on, quiverR(y,x,1,2,'r');
            x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*');
            Svect = [Svect; [x y]];
            Sinds = sub2ind(size(dx), x, y);
            dSx = x - dx(Sinds);
            dSy = y - dy(Sinds);
            dSvect = [dSvect; [dSx dSy]];
            subplot 133, imagesc(dsrc), colormap(gray), title('Moving -> Fix', 'FontSize', 35),
            hold on, plot(dSy, dSx, 'Color', c(counter, :), 'Marker', '*');
            subplot 132, imagesc(tar), colormap(gray), hold on, plot(Tvect(:,2), Tvect(:,1), 'r*'),
            title('Fixed Image', 'FontSize', 35);
            [y,x,button] = ginput(1);
            x = round(x); y = round(y); hold on, plot(y, x, 'Color', c(counter, :), 'Marker', '*');
            Tvect = [Tvect; [x y]];    
        end

        Svect = Svect'; Tvect = Tvect'; Svect(:,end) = []; Tvect(:,end) = [];
        dSvect = dSvect'; dSvect(:,end) = [];
        subplot 131, imagesc(src), colormap(gray),hold on, plot(Svect(2,:), Svect(1,:), 'r*'), 
        title('Moving Image', 'FontSize', 35);
        subplot 132, imagesc(tar), colormap(gray),hold on, plot(Tvect(2,:), Tvect(1,:), 'r*'),
        title('Fixed Image', 'FontSize', 35);
        subplot 133, imagesc(dsrc), colormap(gray), hold on, plot(dSvect(2,:), dSvect(1,:), 'r*'),
        title('Moving -> Fix', 'FontSize', 35),

        Sinds = sub2ind(size(dx), Svect(1,:), Svect(2,:));
        % dSvect(1,:) = Svect(1,:) + dx(Sinds);
        % dSvect(2,:) = Svect(2,:) + dy(Sinds);
        TRE_bef = sum(diag(dist2(Svect', Tvect'))); 
        TRE_aft = sum(diag(dist2(dSvect', Tvect'))); % sqrt(sum( (src2D>1 - tar2D>0) .^2 ));
    end
    
    
%% Statistics
        S = find(mask==1);
        Initial_Warp = ( sqrt( (invTx(S)) .^2 + (invTy(S)) .^2 ) );
        Warp_Err_mask = (sqrt(  (dx(S) - invTx(S)).^2 + (dy(S) - invTy(S)).^2 )); 

        dockf; hist(Initial_Warp), 
        h = findobj(gca,'Type','patch');
        set(h,'FaceColor','r','EdgeColor','w','facealpha',0.7)
        hold on
        hist(Warp_Err_mask), title('                 Warping histgram, before and after registration'), 
        h = findobj(gca,'Type','patch');
        set(h,'facealpha',0.7);
        legend('Initial\_Warp', 'Warp\_Err', 'Location', 'SouthEastOutside');
       
         % - flowToColor(dx, dy ));
%         colormap jet;
%         dockf; imagesc(flowToColor(invTx2D, invTy2D )), cbar;
%         dockf; 
%         subplot 121, imagesc(abs(invTx - dx )), cbar, title('Err x');
%         subplot 122, imagesc(abs(invTy - dy )), cbar, title('Err y');

%         dockf;
%         subplot 221, plot(unique(dx(:))), title('Solution warp');
%         subplot 222, plot(unique(Tx(:))), title('Initial warp');
%         subplot 223,plot(unique(dy(:))), title('Solution warp');
%         subplot 224, plot(unique(Ty(:))), title('Initial warp');
    %% Uncertainty
        S = find(mask3==1);        
        for Unc_Mode = [1 18],
            switch Unc_Mode
                case 1
                    Unc = Unc_Shannon;
                case 2
                    Unc = Wcov;
                case 3
                    Unc = SGauss;
                case 4
                    Unc = GMM25_3; % Unc4;
                case 16
                    Unc = Unc_kde;
                case 17
                    Unc = Unc_Knn;
                case 18
                    Unc = Unc_Shannon_Sprd;
            end
            idata(:,:,1) = Unc; 
            if (Mode == 3)
                Error = diff; 
            else
                Error = Warp_Err; % err_entrpy; % Warp_Err; %  err_mean; % 
            end
            idata(:,:,2) = Error;
            b = 3; %9; %[23 23 8];%10; [ b b b ]
            cc(:,:,Unc_Mode) = blockproc(idata, [b b], @local_corr_fun);
            local_correlation = cc(1:2:end,2:2:end,Unc_Mode);%,1:2:end);
            gcc(:,:,Unc_Mode) = corrcoef(Unc(S), Error(S));
            idata1 = double(Unc(S));
            idata2 = double(Error(S));
            Mut_Info(Unc_Mode) = mutualinfo( idata1, idata2) ./ ...
                ( ( entropy(idata1) + entropy(idata2) ) ./ 2 );
            
            dockf; imagesc(local_correlation), title(['Local correlation uncert ' Unc_str{Unc_Mode}]), cbar;
            dockf; plot(Unc(S), Error(S), 'r*'), title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ...
                ' MI = ', num2str(Mut_Info(Unc_Mode))]), xlabel(['Uncertainty ', Unc_str{Unc_Mode}]), ylabel('Error');
            [p rsq] = lin_regress(Unc(S), Error(S), reg_mod);
%             if (reg_mod == 1)
%                 hold on, plot(Unc(S), p(1)*Unc(S) + p(2)*Unc(S), 'bo');
%             else
%                 hold on, plot(Unc(S), p(1)*Unc(S).^3 + p(2)*Unc(S).^2 + p(3)*Unc(S) + p(4), 'bo');
%             end
        end
end
%% Synthetic or real, controlled by real_mode variable
if(real_mode)
%     isz = isz; 
    data_mode = 2;
else
    if(synth)
        nsample = 1000; nlabs = 100;
        [dispvec pbs] = synthetic_sample_simplex(nsample,nlabs);
        isz = [nsample 1]; mask = ones(nsample,1);
    else
        nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
        [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
        nsample = size(pbs,1); data_mode = 2;
    end 
    nlabs = size(dispvec,1);
     isz = [nsample 1]; mask = ones(nsample,1);  
     G = 1 ./ (dist2(dispvec, dispvec) + 1);
     G = G ./ repmat(sum(G,2), 1, nlabs);
     pbs_sprd = pbs * G;
end
if(test3D)
    nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
    [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
    nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
    isz = [nsample 1]; mask = ones(nsample,1);
     G = 1 ./ (dist2(dispvec, dispvec) + 1);
     G = G ./ repmat(sum(G,2), 1, nlabs);
     pbs_sprd = pbs * G;
end

MoGk = 9; Uncert_Mode = 1; % 0: KNN, 1: All, 2: KNN&KDE
% mask = mask;
Sig = [1e-1 0; 0 1e-1]; mu = [0 0]; accuracy=3; epsilon_ = 10e-4; %accuracy, pbs .* 10^(opt.k)
k = 60; %k nearest neighbors 
iter1 = 5000; %gmdistribution iterations
% opt.iter2 = 2000; %kmeans iterations
tic
    [Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc GMM2_l GMM2_u GMM2_avg GMM2_disc ...
    GMM3_l GMM3_u GMM3_avg GMM3_disc Unc_Kde Unc_Knn] = ...
    Calc_Uncertainty_MEX(pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
toc

tic
[Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);
toc
% %% Running MEX File
% EXE = 'Calc_Uncertainty_MEX'; % Calc_Uncertainty_MEX(pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode)
% cd '../utils/';
% cmd = sprintf(EXE); %([EXE ' pbs dispvec isz mask accuracy epsilon_ iter1 data_mode Uncert_Mode']); %
% save func_variables pbs dispvec isz mask accuracy epsilon_ iter1 data_mode Uncert_Mode;
% disp('Now calculates different uncertainty methods...')
% tic
% system([ 'start /WAIT /AFFINITY 0x50 ' cmd])
% toc
% load ../utils/Uncertainties;
% cd '../RWIR/';
% %% test MoG
% % emiter(fin_obj,dispvec);
%%
if(synth==0 && real_mode ==0)
    Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, ...
        GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, ...
        GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, nlabs_size, sample_num, Unc_str, data_mode);
end
  





load ('../data/native_slices_ax2_41');
sze = [256 256];
spacing.height = 20;
spacing.width = 20; img_num = 5;
T = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
[R  TxW  TyW] = rBSPwarp( T, [spacing.height spacing.width], mag );
D(:,:,1) = TxW; D(:,:,2) = TyW;
invD = invertDefField(D);
invTxW = invD(:,:,1); invTyW = invD(:,:,2);
R(isnan(R)) = 0;
R = mat2gray(R);
mask = R > 0.1;
S = find(mask>0);
T = resc(T);
omega = [0 18 0 18]; m = sz(T);
inter('reset','inter','splineInter','regularizer','moments','theta',1e-1);
distance('reset','distance','SSD');
center = (omega(2:2:end)-omega(1:2:end))'/2;
trafo('reset','trafo','rotation2D','c',center); 
w0 = trafo('w0'); beta = 0; M =[]; wRef = []; % disable regularization
xc = getCellCenteredGrid(omega,m); 
Rc = inter(R,omega,xc);
fctn = @(wc) PIRobjFctn(T,Rc,omega,m,beta,M,wRef,xc,wc);

[wc, his, parameters] = GaussNewton(fctn,w0,'Plots',@FAIRplots,'solver','direct');
Tc = reshape(parameters.Tc, [m(1) m(2)]); Rc = reshape(parameters.Rc, [m(1) m(2)]);
figure, subplot 141, imagesc(Tc), set(gca,'YTick',[]), set(gca,'XTick',[]),
subplot 142, imagesc(Rc), set(gca,'YTick',[]), set(gca,'XTick',[]),
subplot 143, imagesc(Rc), set(gca,'YTick',[]), set(gca,'XTick',[]),
subplot 144, imagesc(abs(Tc-Rc)), set(gca,'YTick',[]), set(gca,'XTick',[]),
colormap(gray),
% figure, subplot 121, imagesc(Tc); subplot 122, imagesc(Rc);




% if(Visualization)
%     if(real_mode)
%     %     option.Unc4 = Unc4;
%         option.Unc_str = Unc_str;
%     else
%         S = find(opt.mask==1);
%         option.Unc4_1 = Unc4_1; option.Unc4_2 = Unc4_2; 
%         option.Unc4_3 = Unc4_3; option.Unc4_4 = Unc4_4;
%         option.Unc_str = {'Shannon' 'Weighted Covariance' 'Gaussian distribution' 'Mixture of Gaussian_Avg' ...
%         'Kernel Density Estimation' 'K Nearest Neighbour' 'Mixture of Gaussian_low' ...
%         'Mixture of Gaussian_high' 'Mixture of Gaussian_disc'};
%     end
%     option.Unc1 = Unc_Shannon; option.Unc2 = Wcov; option.Unc3 = SGauss;
%     option.Unc5 = Unc_Knn; option.Unc_gauss = Unc_kde;
%     option.S = S; 
%     [Mut_Info_U, Global_Corr_U] = Uncert_corr(option);
% end
% %     clear slands; ten_perc = 5; maxd = 10;
% %     slands(1, :)  = round( (size(src2D, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
% %     slands(2, :)  = round( (size(src2D, 2) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
% %     inds = sub2ind(isz, slands(1,:), slands(2,:));
% %     my_pbs = reshape(pbs, [prod(isz) nlabs]);
% %     for ii = 1:length(inds),
% %         dockf; hist(my_pbs(inds(ii),:));
% %     end
% 
% %     [local_correlation1 local_correlation2 local_correlation3 local_correlation5 local_correlation6]