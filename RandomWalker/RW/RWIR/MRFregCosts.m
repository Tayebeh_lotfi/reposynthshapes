% function [ Disp Sc Dc Dc2 I1p I2p]=MRFregCosts( I1, I2 , tlands, slands, maxD,  opt )
function [ Disp Sc Dc ] = MRFregCosts( I1, I2 , maxD, Tx, Ty,  opt ) %I2 is src, I1 is tar
resolution = opt.resolution;
% maxD = mag;%1;
I1= double(mat2gray(I1));%double(I1/max(I1(:)));
I2= double(mat2gray(I2));%/max(I2(:)));
I1(isnan(I1)) = 0;
I2(isnan(I2)) = 0;

%% Defining Labels and Sc, if not given
if maxD ==0
    nl = 1; Disp = [0 0];
    Sc =0;
end
 
% else
%     if isfield(opt, 'res')
%         res = opt.res;
%     else
%         res=1;

%% Meshgrid distribution of labels
switch  opt.mode   
    case 1
        disp 'Using uniform sampling (ndgrid) to generate label set'
        res = 2*maxD / round(sqrt(resolution));
        [Ux Uy]=ndgrid(-maxD:res:maxD);
        Disp=[Ux(1:end)' Uy(1:end)']; 
    case 2
        disp 'Using polar sampling to generate label set'
        rmin = 0.1; rmax = maxD;
        Disp = Polar_Vectors(rmin, rmax, resolution);
    case 3
        disp 'Using uniform polar sampling to generate label set'
        Disp = Polar_Uniform(maxD, resolution);
    case 4
        disp 'Downsampling GT to generate label set, please wait...'
        r = round(numel(Tx)/resolution);
        Disp = [Tx(1:r:end); Ty(1:r:end)]';
    case 5
        disp 'Using VQ to generate label set, please wait...'
        r=2;
       dispvec_samples_GT =[reshape(Tx(1:r:end,1:r:end) ,1,[] ); reshape(Ty(1:r:end,1:r:end) ,1,[] ) ];
       [M, P, DH]=vqsplit(dispvec_samples_GT, resolution); %500 labels
       Disp = M';
    case 6
    %% Kmeans
        disp 'Using Kmeans to generate label set, please wait...'
        label(:,1) = Tx(:);
        label(:,2) = Ty(:);
        opts2 = statset( 'Display', 'final', 'MaxIter', 1000, 'TolTypeFun', 'rel', 'TolFun', 1e-2, 'TolTypeX', 'rel', 'TolX', 1e-2 );       
        [Disp_idx Disp] = kmeans(label, resolution, 'Options', opts2); 
    %     T = [Tx(:) Ty(:)];
    %     [IDX D] = knnsearch(T, Disp, 'k', size(T,1), 'distance', 'euclidean');
    %     mn = mnx(D);   
    case 7
        disp 'Using only five labels to generate the label set'
        Disp = [maxD 0; 0 maxD; -maxD 0; 0 -maxD];
end
%     %49 labels  Disp={-3,-2,-1,0,1,2,3}^2  (for x and y direction)
%% Simple example
% 
%     zero_vec = [0, 0];
%     Disp = [Disp; zero_vec];

    nl=size(Disp,1);

    %% Creating Smoothness Cost
    Sc=zeros(nl); % #lables x #lables         Sc  <-- |U(p)-U(q)|
    for i=1:nl
        for j=1:nl
            %Sc(i,j)=abs(Disp(i,1)-Disp(j,1))+abs(Disp(i,2)-Disp(j,2));
            Sc(i,j) = sqrt((Disp(i,1)-Disp(j,1)).^2 + (Disp(i,2)-Disp(j,2)).^2);%Change the equation here
        end
    end
% end

% nl=size(Disp,1);
imsz=size(I1); 

%% Creating Data Cost 
 
Dc=zeros(imsz(1),imsz(2), nl);
[mx my]=ndgrid(1:imsz(1), 1:imsz(2));
mask = [];
    
datacost = 'SD';
if ~isempty(opt)
    if isfield( opt, 'datacost' );
    datacost = opt.datacost;
    end
    if isfield( opt, 'mask' );
    mask = opt.mask; 
    end
end
if isfield( opt, 'metric_npix' )
npix = opt.metric_npix;
else
npix = 3; 
end

disp (['# of disp. vec. labels = ' num2str(nl)])

if  strcmp(datacost, 'Mind') %I2 is src, I1 is tar
    %%
    if isempty(mask ) 
        rad = 1;
        for l=1:nl
            d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
            mind_tar=MIND_descriptor2d(I1,rad);
            mind_src=MIND_descriptor2d(d2,rad);
            Dc(:,:,l) =sqrt( sum( (mind_tar - mind_src) .^2, 3) );
        end    
    else
        for l=1:nl
            d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
            mind_tar=MIND_descriptor2d(I1,rad);
            mind_src=MIND_descriptor2d(d2,rad);
            Dc(:,:,l) =mask .* sqrt(sum( (mind_tar - mind_src).^2, 3 ));
        end    
    end      

elseif  strcmp(datacost, 'SD')
    %%
    if isempty(mask )  %I2 is src, I1 is tar
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =( d2 -  I1 ).^2;    
        end    
    else
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        end    
    end
 
elseif strcmp(datacost, 'SDPatch')
    %%
    n = 5;
    h = fspecial('average', [n n]);
    if isempty(mask )  %I2 is src, I1 is tar
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );        
        d2 = imfilter(d2, h);
        Dc(:,:,l) =( d2 -  imfilter(I1, h) ).^2;    
        end    
    else
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        d2 = imfilter(d2, h);
        Dc(:,:,l) =mask .* ( d2 -  imfilter(I1, h) ).^2;    
        end    
    end
    
elseif strcmp(datacost, 'GRADIENTMAG')
    %%
        [gx gy]=gradient( I1 );
        I1= sqrt( gx.^2 + gy.^2);
        
        [gx gy]=gradient( I2 );
        I2= sqrt( gx.^2 + gy.^2);
       

     if isempty(mask )
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =( d2 -  I1 ).^2;    
        %h=waitbar(l / nl )
        %figure(h)
        end    
    else
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        %waitbar(l / nl )
        end    
     end
    
     
elseif strcmp(datacost, 'MDZI-GRAD')
    %%
    [gx1 gy1]=gradient( I1 );
    [gx2o gy2o]=gradient( I2 );

     if isempty(mask )
        for l=1:nl
        gx2=interp2( my, mx, gx2o , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        gy2=interp2( my, mx, gy2o , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        
        Dc(:,:,l) =1- (gx1 .* gx2 + gy1.*gy2 ) ./  sqrt( gx1.* gx1 + gy1.* gy1 + 1e-5) ./ sqrt( gx2.* gx2 + gy2.* gy2 + 1e-5) ;
        end    
    else
        for l=1:nl
        d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        end    
     end
    

elseif strcmp(datacost, 'GRADIENT')
    %%
        [gx gy]=gradient( I1 );
        [gx2 gy2]=gradient( I2 );

     if isempty(mask )
        for l=1:nl
        dgx2=interp2( my, mx, gx2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        dgy2=interp2( my, mx, gy2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) = sqrt( (dgx2 -  gx ).^2 + (gy - dgy2).^2 );    
        end    
    else
        for l=1:nl
        dgx2=interp2( my, mx, gx2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        dgy2=interp2( my, mx, gy2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =mask .* sqrt( (dgx2 -  gx ).^2 + (gy - dgy2).^2 );    
        end    
     end
    

 
    
elseif  strcmp(datacost, 'NCC') ||  strcmp(datacost, 'gNCC') 
        %%
    if strcmp(datacost, 'gNCC') 
    
        [gx gy]=gradient( I1 );
        I1= sqrt( gx.^2 + gy.^2);
        
        [gx gy]=gradient( I2 );
        I2= sqrt( gx.^2 + gy.^2);
    
    end
    
    if isempty(mask )        
        for l=1:nl
        dI2=double(interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 ));
        Dc(:,:,l) =  ( - dI2 .* I1 ); 
        end    
    else
        for l=1:nl
        dI2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );                
        Dc(:,:,l) = mask .*(dI2 .* I1 ).^2 ./ sqrt(dI2 .^2 .* I1.^2 + 1e-3);       
        end       
    end
     
    
elseif strcmp(datacost, 'NSSD')
    %%
        
    I1=double(I1);
    I2=double(I2);
    I1p  = getNormalizedLocalOffset( I1, npix );
    I2p  = getNormalizedLocalOffset( I2 , npix );
                 
    if isempty(mask )
    
        for l=1:nl
        d2=interp2( my, mx, I2p , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) =( d2 -  I1p ).^2;    
        end    
    else
        for l=1:nl
        d2=interp2( my, mx, I2p , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        Dc(:,:,l) = mask .* ( d2 -  I1p ).^2;    
        end    
    
    end
elseif strcmp(datacost, 'GRAD')
    
    I1=double(I1);
    I2=double(I2);

    if isfield( opt, 'metric_npix' )
    npix = opt.metric_npix;
    else
    npix = 3; 
    end
    
    I1= smooth(I1 , npix );
    I1= smooth(I1 , npix );
    [gx1 gy1]= get_gradient ( I1 );
    [gx2 gy2]= get_gradient ( I2 );
     
        
    if isempty(mask )
        for l=1:nl
        dgx2=interp2( my, mx, gx2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        dgy2=interp2( my, mx, gy2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );    
        Dc(:,:,l) = ( gx1 .* dgx2 + gy1 .* dgy2 );   
        %waitbar(l / nl )
        end 
    else
        for l=1:nl
        dgx2=interp2( my, mx, gx2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
        dgy2=interp2( my, mx, gy2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );    
        Dc(:,:,l) = mask .* ( gx1 .* dgx2 + gy1 .* dgy2 );   
        %waitbar(l / nl )
        end 
    end     
end


% resize Dc from (m x n x l) to (l x p )
if nargout>3
    npix  =imsz(1)*imsz(2);
    Dc2=zeros(nl,npix);

    for i=1:nl
        D=Dc(:,:,i);
        Dc2(i,:)=(D(:))';
    end
end

%%

d = mnx(Dc);
k=abs(d(1) );
Dc=  Dc +  k;
k=diff(d);
Dc=Dc / k;

%%%%% Added part
% Dc=ones(imsz(1),imsz(2), nl) / k;
%%%%% Added part
   
%% helper functions --- begin here

function m= smooth( I1 , npix )
    I1=double(I1);
    g=fspecial( 'gaussian' ,[npix npix ], sqrt(npix)*2 );
    m=imfilter( I1, g );


function [gx gy]= get_gradient ( I1 )

     [gx gy]=gradient( I1 );

     nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
     gx=gx ./ nrm;
     gy=gy ./ nrm;
     
function I1p = getNormalizedLocalOffset( I1 , npix )
        
    m1=smooth( I1, npix );
    m1b= sqrt( smooth( (I1 - m1).^2 , npix ));
    I1p=(I1 - m1 ) ./ (1e-5+ m1b);
        
       
 
 
    