path = '../results/Uncert_all_tumor_noise/uncert_tumor/';%real/';
file_name = { 'Shannon', 'WCov', 'src', 'tar', 'dsrc', 'Unc_ExpErMap', 'Unc_ExpEr'};    %'Unc_Kde', 'Unc_Knn' , 'time' 
imgs = 1; cols = 6;
for slice = 1:13,
for i = 1:7,
    load ([path, file_name{i}, '_synth_img', num2str(slice), '.mat']); % _real_img
end
figure(1), 
subplot(imgs,cols,1), imagesc(tar), title(['Fxd Img' num2str(slice)], 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight,
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,2), imagesc(src), title('Mvg Img', 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight,
 set(gca,'YTick',[]); set(gca,'XTick',[]);
% subplot(imgs,3,3), imagesc(dsrc), title('Reg Img', 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight
%  set(gca,'YTick',[]); set(gca,'XTick',[]);
% figure(2), 
subplot(imgs,cols,3), imagesc(Shannon), title('ShEnt', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25), 
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,4), imagesc(WCov), title('WCov', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,5), imagesc(Unc_ExpEr), title('ExpEr', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,6), imagesc(Unc_ExpErMap), title('ExpErMap', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
cnt = 1;
pause();
end