%% Creating Data
% cd 'H:\students_less\tlotfima\RWRegistration_Final2D\RWIR'
add_paths()
clc, close all, clear all,
global count;
global_initialize(1);
%%
Visualization = 0; 
real_mode = 0; synth = 0; test3D = 0;
[debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; %MIND
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05; trial_num = 1;
real_mode = 1;
load(['../data/Precomputed/trial' num2str(trial_num) str '_corner.mat']);
src3D = resc(src3D);
tar3D = resc(tar3D);
%%
for Lbl_mod = 1:6,
    for i = 1:size(src3D,3),
            % segment3D = resc(segment3D);
            IWF = src3D(:,:,i); I = tar3D(:,:,i);

            src_seg = src_segment3D(:,:,i); tar_seg = segment3D(:,:,i);
            mask = src_seg>0.1;
            noise_sigma = 0.01;
    %% Creating Landmarks & Data
        distx = 1; disty = 1;
        m = size(I);
        tlands(:,1) = [distx; disty]; 
        tlands(:,2) = [distx; m(2)-disty]; 
        tlands(:,3) = [m(1)-distx; disty]; 
        tlands(:,4) = [m(1)-distx; m(2)-disty];
        slands = tlands;
        src = IWF; tar = I; Tx = Tx3D(:,:,i); Ty = Ty3D(:,:,i); invTx = invTx3D(:,:,i); invTy = invTy3D(:,:,i); 
        %Tx2D = Tx; Ty2D = Ty; %
        num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
    %% Build graph with 8 lattice
        isz = size(src);
        [points edges]=lattice( isz(1), isz(2), 0);
        slands_pre2D = slands; tlands_pre = tlands;
        Max_seeds = num_seed*Max_Iterations;
        nseeds = size(tlands,2);

    %% Calculating the likelihood
        % [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar2D, src2D, Tx2D, Ty2D, nseeds, res, mag);
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
    %% RW Starts here
        slands = slands_pre2D; 
        tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
        nseeds = size(tlands,2); 
    %% Solving the problem
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
    %% Calculating Uncertainty
        [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);
        [Unc_dterm ~] = Calc_Uncertainty_Shannon(dterm);

    %% Calculating warped segment
            Warp_Err_Mean(i, Lbl_mod) = mean( sqrt(  (dx(mask) - invTx(mask)).^2 + (dy(mask) - invTy(mask)).^2 ) ); 
            Warp_Err_Std(i, Lbl_mod) = std( sqrt(  (dx(mask) - invTx(mask)).^2 + (dy(mask) - invTy(mask)).^2 ) ); 
            Warp_Err_Sprd_Mean(i, Lbl_mod) = mean( sqrt(  (dx_sprd(mask) - invTx(mask)).^2 + (dy_sprd(mask) - invTy(mask)).^2 )); 
            Warp_Err_Sprd_Std(i, Lbl_mod) = std( sqrt(  (dx_sprd(mask) - invTx(mask)).^2 + (dy_sprd(mask) - invTy(mask)).^2 )); 
    end   
end