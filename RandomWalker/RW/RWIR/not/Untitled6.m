    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
    tlands_pre = tlands;
%     mask1 = ( src > 0.2);
    
    ops.alpha = 1e-3; %0.3
    length = size(slands,2);
    pre_length = length;
    cert_thres1 = quantile(uncertainty(find(mask>0)),.999);
%     cert_thres2 = quantile(uncertainty(find(mask>0)),.98);

    uncertainty = uncertainty .* mask;
    S=( uncertainty > cert_thres1); % & uncertainty < cert_thres2);
    
%         a = uncertainty(find(mask>0)); mn = mean(a);
%         dockf;hist(a)
%         hold on, plot(mn, 1:10000, 'r.-'), hold on, plot(cert_thres, 1:10000, 'g.-'), 
        
    candidate_inds = find(S);
    indexes = candidate_inds;
    candidates(1,:) = mx( candidate_inds )';
    candidates(2,:) = my( candidate_inds )';
%     candidates(3,:) = mz( candidate_inds )';
%     landmarks.X = slands(1,:); landmarks.Y = slands(2,:); landmarks.Z = slands(3,:);
 
st = 25;
figure, subplot 121, imagesc(src), colormap(gray), hold on, plot(slands(2,st:end), slands(1, st:end), 'r*'), 
subplot 122, imagesc(tar), colormap(gray), hold on, plot(tlands(2,st:end), tlands(1, st:end), 'r*')


figure, subplot 121, quiverR(invTy, invTx, 20, 2, 'c' ), title('ground truth');
subplot 122, quiverR(dy, dx, 20, 2, 'c' ), title('solution');
figure, imagesc(abs(dx-invTx)), cbar
figure, imagesc(dsrc)

gamma = 0.7;
data_prior = exp(- reshape( dterm, prod(isz), [] )/gamma);

dsrc = interp2( my, mx, src, my + dy, mx + dx,  '*linear',0);
figure, imagesc(dsrc)
figure, imagesc(dsrc - tar)