function unc = WCov(pbs, dispvec) % disp_probs is a square matrix with elements that sum to 1.

for i = 1:sz(pbs,1),
    disp = sqz(pbs(i, :));
%     d = (size(disp,1)-1)/2; % Assume displacements up distance d.
%     [x_pts, y_pts] = meshgrid(-d:d);
    W_mu = sum([disp(:) .* dispvec(:,1), disp(:) .* dispvec(:,2)]);
    x_pts = dispvec(:,1) - W_mu(1);
    y_pts = dispvec(:,2) - W_mu(2);
    Sigma(:,:,i) = [x_pts(:)'; y_pts(:)'] * diag(disp(:)) * [x_pts(:), y_pts(:)];
    unc(i) = trace(Sigma(:,:,i));
end
end