
    c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;

         
    for i = 1:8,
        AllErr.SL3D(i,:) = AllErr.SL3D(i,:) ./ max(AllErr.SL3D(i,:));
     end
       
    
   figure,
    for i = 1:8,
    plot( AllErr.SL3D(i,:), 'Color', c(i*15,:), 'Marker', shape{i}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    end
%     errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(11*10,:), 'Marker', shape{11}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Seg-based Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('SSD Error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('Image Pair1', 'Image Pair2', 'Image Pair3', 'Image Pair4', 'Image Pair5', 'Image Pair6', 'Image Pair7', 'Image Pair8', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
    
   figure,
    errorbar( mean(sqz(AllErr.SL3D)), std(sqz(AllErr.SL3D)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
%     errorbar( mean(sqz(AllErr.AL10)), std(sqz(AllErr.AL10)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Seg-based Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('SSD Error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
    
    my_path = 'Y:/students_less/tlotfima/RWRegistration_Final3D/results/AL_SL/Regression/';
 
    for i = 1:8
    load ([my_path 'AllErr_large_vol' num2str(i) '_to_vol8_regress_noise5.mat']);
    AllErr1.SL3D(i,:) = AllErr.SL3D;
    end
    
    save ([my_path 'AllErr1_8.mat'], 'AllErr')
    
    
    my_path = 'Y:/students_less/tlotfima/RWRegistration_Final3D/results/AL_SL/Regression/Bspline/';
 
    for i = 1:8
    load ([my_path 'AllErr_large_vol' num2str(i) 'bspwrp_regress_noise5.mat']);
    AllErr1.SL3D(i,:) = AllErr.SL3D;
    end
    AllErr = AllErr1;
    save ([my_path 'AllErr1_8.mat'], 'AllErr')
    
    save ([my_path 'AllErr_ALL1_8.mat'], 'AllErr_All')
    
    save ([my_path 'AllErr_Less1_8.mat'], 'AllErr_Less')
    
    figure,
    errorbar( mean(sqz(AllErr.SL3D)), std(sqz(AllErr.SL3D)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_All.SL3D)), std(sqz(AllErr_All.SL3D)), 'Color', c(3*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Less.SL3D)), std(sqz(AllErr_Less.SL3D)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
%     errorbar( mean(sqz(AllErr_Less.SL3D)), std(sqz(AllErr_Less.SL3D)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     set(gca, 'FontSize', 35);
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 5%% noise']);
    title( error_measure, 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
    legend('SL Unc', 'SL W Unc', 'SL WO Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
    
    
    
 %% 3D bspline warping   
    my_path = 'Y:/students_less/tlotfima/RWRegistration_Final3D/results/AL_SL/Regression/Bspline/';
    for i = 1:8
    load ([my_path 'AllErr_large_vol' num2str(i) 'bspwrp_regress_noise5.mat']);
    AllErr1.SL3D(i,:) = AllErr.SL3D;
    end  
    
   figure,
    for i = 1:8,
    plot( AllErr.SL3D(i,:), 'Color', c(i*15,:), 'Marker', shape{i}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    end
%     errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(11*10,:), 'Marker', shape{11}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('Image Pair1', 'Image Pair2', 'Image Pair3', 'Image Pair4', 'Image Pair5', 'Image Pair6', 'Image Pair7', 'Image Pair8', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    
   figure,
    errorbar( mean(sqz(AllErr.SL3D)), std(sqz(AllErr.SL3D)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
%     errorbar( mean(sqz(AllErr.AL10)), std(sqz(AllErr.AL10)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    
  %% 2D  
    
    
    path = '../results/AL_SL/Regression/ExpEr/Seg_Eval/To_Other_Imgs/';
    
    for i = 1:8,
    load ([path 'AllErr' num2str(i) '_small_to_img' num2str(5*i) '_regress_ExpErU_noise5.mat']);
    AllErr1.SL200((i-1)*4+1:i*4,:) = AllErr.SL200;
    end
     for i = 1:12,
        AllErr1.SL200(i,:) = AllErr1.SL200(i,:) ./ max(AllErr1.SL200(i,:));
     end
     clear AllErr;
     AllErr.SL200 = AllErr1.SL200(1:10,:);
     
     figure,
    for i = 1:10,
    plot( AllErr.SL200(i,:), 'Color', c(i*10,:), 'Marker', shape{i}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    end
%     errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(11*10,:), 'Marker', shape{11}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Seg-based Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('SSD Error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('Image Pair1', 'Image Pair2', 'Image Pair3', 'Image Pair4', 'Image Pair5', 'Image Pair6', 'Image Pair7', 'Image Pair8', 'Image Pair9', 'Image Pair10', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
     
      figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35); xlim = [1 6]; ylim = [1 5];
%     errorbar( mean(sqz(AllErr.AL10)), std(sqz(AllErr.AL10)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Seg-based Error, MRI brain images with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('SSD Error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
                
%% 
         image = tar;
        dockf; imagesc(image), colormap(gray),
        [y,x,button] = ginput(1);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];

        image = tar;
        x = round(x); y = round(y);
        
        prob1 = sqz(pbs(x,y,1:end));
        prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);

        dockf; 
        subplot 131, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]); 
%         set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 

        prob1 = sqz(pbs(x,y,1:end));
        prob2 = prob1 * 10^5;
        prob2(prob2<mean(prob2)-0.4*std(prob2)) = 0; prob2 = prob2 ./ sum(prob2); ind = find(prob2 ~=0); probpp = prob2(ind); 
        probpp(352:361) = mean(prob2);
        resolution = 1;
        [gridX gridY] = ndgrid(-maxd+1:resolution:maxd-1);
        probpp = reshape(probpp, [size(gridX,1) size(gridX,2)]);
        subplot 132, surf(gridX, gridY, probpp), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
%         set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
        
        prob1 = sqz(pbs(x,y,1:end));
        prob3 = prob1 * 10^5; 
        prob3(prob3<mean(prob3)-1.15*std(prob3)) = 0; prob3 = prob3 ./ sum(prob3); ind = find(prob3 ~=0); probpp = prob3(ind); 
        probpp(482:484) = mean(prob3);
        resolution = 1;
        [gridX gridY] = ndgrid(-maxd-1:resolution:maxd);
        probpp = reshape(probpp, [size(gridX,1) size(gridX,2)]);
        subplot 133, surf(gridX, gridY, probpp), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
%         set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 

       
        grid on, 
        
        prob3 = prob1 * 10^6;
        prob3(prob3<mean(prob3)-2.25*std(prob3)) = 0;
        prob3 = reshape(prob3, [size(gridX,1) size(gridX,2)]);
        subplot 133, surf(gridX, gridY, prob3), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
        set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 