figure, subplot 121, quiverR(invTy, invTx, 20, 2, 'c' ), title('ground truth');
subplot 122, quiverR(dy, dx, 20, 2, 'c' ), title('solution');

gamma = 0.7;
data_prior = exp(- reshape( dterm, prod(isz), [] )/gamma);

dsrc = interp2( my, mx, src, my + dy, mx + dx,  '*linear',0);
figure, imagesc(dsrc)
figure, imagesc(dsrc_sprd)
