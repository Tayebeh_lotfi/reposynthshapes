function test_mog(options)
%% MoG test
    switch(options)
        case 1
            my_obj.Sigma = [10e6 0 ; 0 10e6]; my_obj.mu = [0 0]; my_obj.NComponents = 1; my_obj.PComponents = 1;
            [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj);
            unt_est = 1/2 .* log((2*pi*exp(1)).^2 .* det(my_obj.Sigma));
        case 2
            my_obj.Sigma = [10e4 0 ; 0 10e4];
            [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj);
            unt_est = 1/2 .* log((2*pi*exp(1)).^2 .* det(my_obj.Sigma));
        case 3
            my_obj.Sigma = [10 0 ; 0 10];
            [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj);
            unt_est = 1/2 .* log((2*pi*exp(1)).^2 .* det(my_obj.Sigma));
        case 4
            my_obj.Sigma = [10 0 ; 0 10]; my_obj.mu = [4   7];
            [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj);
            unt_est = 1/2 .* log((2*pi*exp(1)).^2 .* det(my_obj.Sigma));
        case 5
            my_obj.Sigma(:,:,1) = [10 0; 0 10]; my_obj.Sigma(:,:,2) = [20 0; 0 10];
            my_obj.mu = [4 7; 3 5]; my_obj.PComponents = [0.6 0.4]; my_obj.NComponents = 2;
            [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj);
            component = zeros(1,2);
            unt_est = 0;
            for i = -50:50,
                for j = -50:50,
                    X = [i j];
                    for l = 1:2,
                        Siginv = pinv(my_obj.Sigma(:,:,l));
                        component(l) = 1 / sqrt(2*pi*det(my_obj.Sigma(:,:,l))) .* exp( -(X -  my_obj.mu(l,:)) * ...
                          Siginv * (X -  my_obj.mu(l,:))' ) .* my_obj.PComponents(l);
                    end
                    f = sum(component);
                    unt_est = unt_est - f * log(f);
                end
            end
    end
    unt_est, unt_mog_lower, unt_mog_upper
end

function [unt_mog_lower unt_mog_upper] = ent_mog_calc(my_obj)
    for l = 1:my_obj.NComponents,
        opt.MoGk = my_obj.NComponents;
        opt.l = l;
        h_mog_lower(l) = mixgauss_distribution(my_obj, opt);
        h_mog_upper(l) = my_obj.PComponents(l) .* (-log(my_obj.PComponents(l)) + ...
            1/2*log( (2*pi*exp(1)) .^ 2 .* det(my_obj.Sigma(:,:,l))));
    end
    unt_mog_lower = sum(h_mog_lower);
    unt_mog_upper = sum(h_mog_upper);
end
