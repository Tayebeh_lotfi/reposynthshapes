
opt.Sig = [0.2, 0.4];
S.Sigma = opt.Sig;
a = 2; b = 20;
count = 1;
pix = 1;
for delt = 7:0.2:11
    dispvec = [a+delt; b-delt];
    S.mu = dispvec;
    pbs = [0.5 0.5];
    data = 0:0.1:30;
    y1 = normpdf(data, S.mu(1), S.Sigma(1));
    y2 = normpdf(data, S.mu(2), S.Sigma(2));
    nlabs = size(dispvec,1);
    for l = 1:nlabs,
        opt.l = l;
        X = dispvec(l,:);     
        Sig = opt.Sig(1)+opt.Sig(2);
        Siginv = pinv(Sig);
        for k = 1:nlabs,
            mu = dispvec(k,:);
            component(k) = 1 / sqrt(2*pi*Sig) .* exp( -(X - mu) * ...
            Siginv * (X - mu)' ) .* pbs(pix,k);
        end
        h_gauss_lower(l) = -pbs(pix,opt.l) .* log(sum(component));
%                     h_gauss_upper(l) = pbs(pix,l) .* (-log(pbs(pix,l))) + ...
%                         1/2*log( (2*pi*exp(1)) .^ size(dispvec_rep,2) .* det(obj.Sigma(:,:,l))));
    end
     h_gauss_upper = sum(pbs(pix,:) .* (-log(pbs(pix,:))+ ...
        1/2 .* log(repmat((2*pi*exp(1)) .^ size(dispvec,2),1,2) .* (opt.Sig))));
    Unc(count) = ( sum(h_gauss_lower) + h_gauss_upper ) / 2;       
    subplot(5,4,count), plot(data, y1+y2, 'r-'), title(['Uncertainty = ', num2str(Unc(count))]);
    count = count+1;
end