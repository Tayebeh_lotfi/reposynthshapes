
        

        opt.epsilon = 1e-3; epsilon = .5; opt.accuracy = 3;

        pix1 = sub2ind(sz(src), 120, 171); pbs = reshape(pbs, prod(sz(src)), []);
        dispvec_rep = replicate_vect_pbs(dispvec, pbs, opt.accuracy, pix1);
        dispvec_rep = dispvec_rep';
        
        S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
        [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
        S.mu = ([x(:), y(:)]);
        [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
        sig = D(1,2)/(6);

        S.Sigma = repmat([sig 0; 0 sig], [1 1 size(S.mu,1)]);
        S.PComponents(1:size(S.mu,1)) = 1/size(S.mu,1);


        obj = gmdistribution.fit(dispvec,size(S.mu,1),'Regularize',opt.epsilon, 'Options', opts1, 'Start', S);

        [X1,X2] = meshgrid(linspace(mn(1)-epsilon,mn(2)+epsilon,529)',linspace(mn(1)-epsilon,mn(2)+epsilon,529)');

        X = [X1(:) X2(:)]; 
        ezsurf(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]),
%         for i = 1:MoGk,
%             p = mvnpdf(X,S.mu(i,:),S.Sigma(:,:,i));
%             hold on, surf(X1,X2,reshape(p,529,529)), title('GMM', 'FontSize', 25),
%             xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]); 
%             set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);
%         end

        Xp = reshape( pbs, prod(sz(src)), [] );
        a =( Xp .* ( Xp * pdist2(dispvec, dispvec) ) ); 
        aa = a(1,:)'; aa = aa./sum(aa);
        resolution = 2*maxd / round(sqrt(res));
                [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
     
        aa = reshape(aa, [size(gridX,1) size(gridX,2)]);
        dockf; surf(gridX, gridY, aa), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
        set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
        
        
        
        
         Disp = [dx(:) dy(:)];
         amap =  Xp .* pdist2(Disp, dispvec);
         aamap = amap(1,:)'; aamap = aamap./sum(aamap);
        resolution = 2*maxd / round(sqrt(res));
                [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
     
        aamap = reshape(aamap, [size(gridX,1) size(gridX,2)]);
        dockf; surf(gridX, gridY, aamap), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);                
        set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
        
        
         