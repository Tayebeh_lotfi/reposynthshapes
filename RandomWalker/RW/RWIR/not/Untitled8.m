    Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 )); 
    Warp_Err_Sprd = (sqrt(  (dx_sprd - invTx).^2 + (dy_sprd - invTy).^2 ));
    Unc_Shannon1 = resc(Unc_Shannon);
%     Unc_Shannon_Sprd1 = resc(Unc_Shannon_Sprd);
    WCov1 = resc(WCov);
    Unc_ExpEr1 = resc(Unc_ExpEr);
    Unc_ExpErMap1 = resc(Unc_ExpErMap);
    Unc_ExpEr_polar1 = resc(Unc_ExpEr_polar);
    Unc_ExpErMap_polar1 = resc(Unc_ExpErMap_polar);
    
    dockf; subplot 131, imagesc(I); title('Target Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 132, imagesc(IWF); title('Source Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 133, imagesc(dsrc); title('Registered Image', 'FontSize', 35),colormap(gray), set(gca, 'FontSize', 35);%set(gca,'FontSize',25), 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    
%     dockf; 
     dockf, subplot 151, imagesc(Warp_Err), title('Reg Error', 'FontSize', 35), axis equal, colormap(gray), axis tight, %cbar, set(gca, 'FontSize', 35);
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     subplot 152, imagesc(Unc_ExpEr_polar1), title('ExpErPol Unc', 'FontSize', 35), axis equal, colormap(gray), axis tight,
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     subplot 153, imagesc(Unc_ExpEr1), title('ExpEr Unc', 'FontSize', 35), axis equal, colormap(gray), axis tight, 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     subplot 154, imagesc(Unc_ExpErMap_polar1), title('ExpErMAPPol Unc', 'FontSize', 35), axis equal, colormap(gray), axis tight, 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     subplot 155, imagesc(Unc_ExpErMap1), title('ExpErMAP Unc', 'FontSize', 35), axis equal, colormap(gray), axis tight, 
     set(gca,'YTick',[]); set(gca,'XTick',[]);

