add_paths()
root = '/home/ghassan/students_less/tlotfima';
savepath = [root '/RWRegistration_Final2D/results/'];
real_mode = 0;

nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
[pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
nsample = size(pbs,1); data_mode = 2;
nlabs = size(dispvec,1);
isz = [nsample 1]; mask = ones(nsample,1);  


for MCase = 1:2,
    for Noise_Level = 2:4,
        if (MCase == 1)
            dispvec = dispvec + 0.05 .* (Noise_Level-1) .* rand(nlabs,2) + ...
            ( (-1) .^ round(rand(nlabs,2))  .* (-0.05) .* (Noise_Level-1) );
            %       figure, quiver(zeros(sz(dispvec,1),1), zeros(sz(dispvec,1),1), dispvec(:,2), dispvec(:,1), 'Color', 'c');
            % 

        else
            if(MCase == 2)
            pbs = pbs + 0.01 * (Noise_Level-1) * rand(sz(pbs,1),sz(pbs,2));
            pbs = pbs ./ repmat(sum(pbs, 2), [1 nlabs]);
            %             pbs_sprd = pbs_sprd + 0.01 * (Noise_Level-1)* rand(sz(pbs,1),sz(pbs,2));
            %             pbs_sprd = pbs_sprd ./ repmat(sum(pbs_sprd, 2), [1 nlabs]);            
            % 
            %             mn = mnx(dispvec);
            %             [gridX gridY] = ndgrid(mn(1):1:mn(2));
            %             X = [gridX(:), gridY(:)]; 
            %             prob1 = sqz(pbs(9,1:end));             
            %             prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
            %             figure, surf(gridX, gridY, prob1), xlim([mn(1)-.5 mn(2)+.5]), ylim([mn(1)-.5 mn(2)+.5]);
            %             colormap(jet)
            end
        end
        G = 1 ./ (dist2(dispvec, dispvec) + 1);
        G = G ./ repmat(sum(G,2), 1, nlabs);
        pbs_sprd = pbs * G;

        [Unc_Shannon(:,Noise_Level-1,MCase) Unc_Shannon_Sprd(:,Noise_Level-1,MCase)] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
    end
end


