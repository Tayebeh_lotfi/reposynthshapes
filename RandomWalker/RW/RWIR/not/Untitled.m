nlabs    :   one

[nlabs, nlabs-1]     :    two adjacent

nlabs, nlabs-nlabs_size+1 :    two medium
%nlabs, 1   two far


[1, nlabs, floor(nlabs/2) + 1] :    three


[1, nlabs, floor(nlabs/2) + 1, nlabs_size, nlabs-nlabs_size+1] :    five
figure, 
for ind = nlabs, ind = [nlabs, nlabs-1], ind = [nlabs, nlabs-nlabs_size+1], ind = [1, nlabs, floor(nlabs/2) + 1], ind = [1, nlabs, floor(nlabs/2) + 1, nlabs_size, nlabs-nlabs_size+1],
    subplot 151, quiver(zeros(25,1), zeros(25,1), dispvec(:,2), dispvec(:,1), 1, 'Color', 'k');
    hold on, quiver(zeros(sz(ind,2),1),zeros(sz(ind,2),1),dispvec(ind,2),dispvec(ind,1), 1, 'Color', 'r', 'LineWidth', 4), hold off,
    title('Case 1', 'FontSize', 35)
    set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
end

figure,
midle = floor(nlabs/2) + 1;
ind = nlabs, ind = [nlabs, nlabs-1], ind = [nlabs, nlabs-nlabs_size+1], ind = [nlabs, nlabs_size*nlabs_size],
ind = [1 nlabs_size midle nlabs_size*nlabs_size-nlabs_size+1 nlabs_size*nlabs_size],
ind = [1 nlabs_size*nlabs_size middle nlabs-nlabs_size*nlabs_size+1 nlabs],
clear ind
subplot 236, quiver3(zeros(125,1), zeros(125,1), zeros(125,1), dispvec(:,1), dispvec(:,2), dispvec(:,3), 1, 'Color', 'k');
hold on, quiver3(zeros(sz(ind,2),1), zeros(sz(ind,2),1), zeros(sz(ind,2),1), dispvec(ind,1), dispvec(ind,2), dispvec(ind,3), 1, 'Color', 'r', 'LineWidth', 4), hold off,
title('Case 6', 'FontSize', 35)
set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
