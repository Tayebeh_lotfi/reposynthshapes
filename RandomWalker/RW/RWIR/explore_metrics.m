function explore_metrics(set_num, trial_num, MCase) 

% trial_num = 1;
% matlabpool
% matlabpool close
 
add_paths();
%% Getting two images, pre-setting landmarks
[debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%% Reading data
    load ('../data/native_slices_ax2_41');
   %%
   if (MCase == 1)
       load '../data/Bspline.mat';
   else
       load '../data/Diffusion.mat';
   end
% end
%%
Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

Pre_Max_Iterations = Max_Iterations;
start_img = (set_num-1)*image_set_size; scale = 10E5;
alpha = [100 1000 2000 5000 7000 10000];
alpha_len = len(alpha);
for img_num = start_img + 1 : start_img + image_set_size,
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    [mm nn] = ndgrid(1: sze(1), 1: sze(2));
%%    
    if(MCase ~= 1)
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        IWF = interp2(nn, mm, I, nn + TyW, mm + TxW, '*linear');
        tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
        src_seg = interp2(nn, mm, tar_seg, nn + TyW, mm + TxW, '*linear');
        IWF(isnan(IWF))=0;
        src_seg(isnan(src_seg))=0;
        src = mat2gray(IWF); tar = mat2gray(I); 
        Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    else
%%                
        I = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
        IWF = interp2(nn, mm, I, nn + TyW, mm + TxW, '*linear');
        tar_seg = imrotate(imresize(segs(:,:,img_num), [sze(1) sze(2)]), 90);
        src_seg = interp2(nn, mm, tar_seg, nn + TyW, mm + TxW, '*linear');
        src = mat2gray(IWF); tar = mat2gray(I); 
        Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    end
    mask = tar>0.1;
    b=7; h = fspecial('average', b); homg = imfilter(src, h, 'same'); homogeneous = mat2gray(abs(src - homg));
    homogeneous = homogeneous .* mask;
    mask1 = (homogeneous > mean(homogeneous(:))+0.3*std(homogeneous(:))); %(src > 0.1); % .* 
    mask2 = ( src_seg > 0); se = strel('disk',15); mask2 = imdilate(mask2,se);
    Noise_Level = 1;   noise_sigma = 0.05;  
    src = imnoise(src, 'gaussian', 0, (Noise_Level-1) * noise_sigma );
%%
    tlands_pre = tlands; slands_pre = slands;
    Max_Iterations = Pre_Max_Iterations;
    nseeds = size(tlands,2);
  
%% Build graph with 8 lattice
    isz = size(src); nseeds = len(tlands);
    [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
    reg_iteration = 1;
    for simil_measure = 1:5,
%% Calculating the likelihood     
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);

        for regular = 1:alpha_len,
%% Running probabilistic registration and Calculating the warping error
%           [dx dy pbs pbs_sprd dsrc] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
            [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha(regular), beta, gamma, dterm_mode, slands, reg_iteration);
%           [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma);           
%% Calculating Uncertainty
            mask = mask .* mask2; % .* mask2;   
            S = find(mask);
            [mx my] = ndgrid(1:isz(1), 1:isz(2)); 
            src_seg_wrp = interp2(my, mx, src_seg, my+dy, mx+dx, '*linear',0);
            dice = sum((abs(tar_seg(S)-src_seg_wrp(S))));
            total_field_err = Calc_Warp_Err(dx, dy, invTx, invTy, S);
            initial_warp = Calc_Warp_Err(zeros(sz(invTx)), zeros(sz(invTy)), invTx, invTy, S);
            real_mode = 2;

            [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
            epsilon_ = 1e-2; data_mode = 2;
        %    tic
        %    Unc_WCov = mat2gray(Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode));
        %    toc
              uncertainty(:, :, regular, simil_measure, img_num) = Unc_Shannon_Sprd; % dsrc; %  + bb(count_bb) .* mat2gray(homogeneous) + cc(count_cc) .* mat2gray(Smooth_Val); % Unc_WCov; % 
              T(:, :, 1, regular, simil_measure, img_num) = dx;
              T(:, :, 2, regular, simil_measure, img_num) = dy;
        end
    end
%       ErrTre(img_num) = dice; % regular, simil_measure, 
%       ErrWarp(img_num) = total_field_err; % regular, simil_measure, 
end
save ('../results/explore_metrics.mat', 'uncertainty', 'T');
end 

