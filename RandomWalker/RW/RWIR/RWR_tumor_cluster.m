function RWR_tumor_cluster() % img_num
%%
add_paths()
    [ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%     noise_sigma = 0.005; 
    Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
    simil_measure = 5; %MIND
    
    str = {'2_12', '1_11', '2_6', '2_9', '6_14', '6_9', '7_16', '8_10', '8_14', '11_19', '11_12', '12_19', '17_20'};
%     str = {'1_3', '2_6', '3_7', '5_2', '5_6', '5_8', '5_10', '8_4', '9_3', '9_7'};
%% 
    for slice = 1:13, % 10, % 
      load (['../data/tumor_data/test_synth' str{slice} '.mat']);
%         load (['../data/tumor_data/test_real' str{slice} '.mat']);
        TxW = 20*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
%% Creating Landmarks & Data
        distx = 1; disty = 1;
        m = size(I);
        tlands(:,1) = [distx; disty]; 
        tlands(:,2) = [distx; m(2)-disty]; 
        tlands(:,3) = [m(1)-distx; disty]; 
        tlands(:,4) = [m(1)-distx; m(2)-disty];
        slands = tlands;
        src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
        mask = src>0.1;
        num_seed = 4; Max_Iterations = 5; alpha = 50000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
        isz = size(src);
        [points edges]=lattice( isz(1), isz(2), 0);
        slands_pre = slands; tlands_pre = tlands;
        Max_seeds = num_seed*Max_Iterations;
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
        slands = slands_pre; 
        tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
        nseeds = size(tlands,2); 

%% Solving the problem
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
%% Shannon's Entropy
        [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2); 
        [Unc_ExpEr, Unc_ExpErMap] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);
        [Unc_ExpErPol, Unc_ExpErMapPol] = Calc_Uncertainty_Err_Polar(pbs, dispvec, dx, dy);
        data_mode = 2; epsilon_ = 1e-2;
        [WCov, GDist] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
%         figure, subplot 131, imagesc(tar), title('Fixed Image', 'FontSize', 25), set(gca,'FontSize',25),
%         subplot 132, imagesc(src), title('Moving Image', 'FontSize', 25), set(gca,'FontSize',25),
%         subplot 133, imagesc(dsrc), title('Registered Image', 'FontSize', 25), set(gca,'FontSize',25), colormap(gray)
%         figure, subplot 221, imagesc(Shannon), title('ShEnt', 'FontSize', 25), cbar, set(gca,'FontSize',25), 
%         subplot 222, imagesc(Shannon_Sprd), title('WProb', 'FontSize', 25), cbar, set(gca,'FontSize',25), 
%         subplot 223, imagesc(Unc_Err_Dist), title('Error Dist', 'FontSize', 25), cbar, set(gca,'FontSize',25),
%         subplot 224, imagesc(Unc_Err_Dist_Map), title('Error Dist Map', 'FontSize', 25), cbar, set(gca,'FontSize',25),
%% Saving Results
        file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd', 'Unc_ExpErMap', 'Unc_ExpEr', 'Unc_ExpErMapPol', 'Unc_ExpErPol', 'WCov', 'GDist', 'dx_sprd', 'dy_sprd', 'dx', 'dy', '-v7.3', 'pbs'};    %'Unc_Kde', 'Unc_Knn' , 'time' 
        for i = 1:16,
%             save (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/' file_name{i} '_img' num2str(slice) '.mat']), file_name{i}); 
            save (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/' file_name{i} '_synth_img' num2str(slice) '.mat']), file_name{i}); 
        end
    end
end