function Tayebeh_RWRPrior_Set_Regression_(set_num, trial_num, MCase) 

% clear, trial_num = 3; set_num = 6; MCase = 1;
% matlabpool
% matlabpool close
 
add_paths();
%% Getting two images, pre-setting landmarks
[debug flag Pre_Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();

Lbl_mod = 1; % 1:Mesh, 2: Non-uniform Polar , 3: Uniform Polar, 4: sampling GT, 
%              5: VQ, 6: Kmeans GT, 7: 4 displacements

start_img = (set_num-1)*image_set_size; scale = 10E5;
simil_measure = 5; % 1: MDZI-GRAD, 2: gNCC, 3: NSSD, 4: SD, 5: MIND, 6: SDPatch
%% Reading data
load ('../data/native_slices_ax2_41');
sze = [256 256];
spacing.height = 20;
spacing.width = 20;
% if(strcmp(str,'_small'))
%      magnitude = 7;
% else if (strcmp(str, '_large'))
%         magnitude = 10;
%     end
% end
if MCase == 1, 
%     load (['../results/diff_pixel_num/learning_with_unc' num2str(trial_num) '.mat']);
%     load (['../results/diff_pixel_num/learning_without_unc' num2str(trial_num) '.mat']);        
    load (['../results/diff_pixel_num/regress_without_unc' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']);
    load (['../results/diff_pixel_num/regress_with_unc' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']);

else    
%     load (['../results/diff_pixel_num/learning_img_feat' num2str(trial_num) '.mat']);
%     load (['../results/diff_pixel_num/learning_unc_feat' num2str(trial_num) '.mat']); % '../results/diff_pixel_num/test/learning_unc_feat'
    load (['../results/diff_pixel_num/regress_img_feat' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']);
    load (['../results/diff_pixel_num/regress_unc_feat' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']);
end
%%
for img_num = start_img + 1 : start_img + image_set_size,
%   if (img_num > 28), %img_num = 31; end
    tar = imrotate(imresize(mat2gray(Im(:,:,img_num)), [sze(1) sze(2)]), 90);
    tar_seg = imrotate(imresize(mat2gray(segs(:,:,img_num)), [sze(1) sze(2)]), 90);
    [src  Tx  Ty] = rBSPwarp( tar, [spacing.height spacing.width], mag );
    [mx my]=ndgrid(1:sz(src,1), 1:sz(src,2));
    src_seg =  interp2(my, mx, tar_seg, my + Ty, mx + Tx, '*linear', 0);      
    D(:,:,1) = Tx; D(:,:,2) = Ty;
    invD = invertDefField(D);
    invTx = invD(:,:,1); invTy = invD(:,:,2);
    src(isnan(src)) = 0;
    src = mat2gray(src);
    mask = src > 0.1;
    S = find(mask>0);
    tar = resc(tar);
    src = resc(imnoise( resc(src), 'gaussian', 0, rand*noise_sigma ));
%    resc(src, imnoise);
    num_seed = 4; Max_Iterations = Pre_Max_Iterations;  reg_mod = 1; 
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(tar);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
    slands_pre = slands; tlands_pre = tlands;
    slands_pre_Less = slands_pre; tlands_pre_Less = tlands_pre;
%% Build graph with 8 lattice
        nseeds = size(tlands,2);
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        nseeds = size(tlands,2); 
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; 
%%
    for Mode = [1, 2, 4], %1:7, % [2 6 7], %:  % 1: Random Selection       2: Self Learning      3: Active Learning
        tstart=tic;     
        slands = slands_pre; tlands = tlands_pre; 
        slands_Less = slands_pre_Less; tlands_Less = tlands_pre_Less;
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; 
        dispvec_Less = dispvec_pre; dterm_Less = dterm_pre; nlabs_Less = nlabs_pre; 
        if (Mode==7), Max_Iterations = 2; end
%% Build graph with 8 lattice
        isz = size(src); nseeds = len(tlands); nseeds_Less = len(tlands_Less);
        [points edges]=lattice( isz(1), isz(2), 1);
%% Starting iteration
        reg_iteration = 1;
        while(reg_iteration <= Max_Iterations) 
           fprintf('start of iteration : %d, Mode : %s, image number: %d\n', reg_iteration, Mode_Str{Mode}, img_num);
%% Running probabilistic registration and Calculating the warping error
%             [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma);
            [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, reg_iteration);
            if (reg_iteration == 1)
                dx_sprd_Less = dx_sprd; dy_sprd_Less = dy_sprd; dx_Less = dx; dy_Less = dy; pbs_Less = pbs; pbs_sprd_Less = pbs_sprd; dsrc_Less = dsrc; dsrc_sprd_Less = dsrc_sprd;
            else
                [dx_sprd_Less dy_sprd_Less dx_Less dy_Less pbs_Less pbs_sprd_Less dsrc_Less dsrc_sprd_Less] = Running_Prob_Reg_Prior(src, dispvec_Less, nlabs_Less, edges, dterm_Less, isz, alpha, beta, gamma, dterm_mode, slands_Less, reg_iteration);
            end
%% Calculating Features         
%             [Input Output] = Extract_Features_Less(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err);         
            [Input Output] = Extract_Features(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S); 
             if (reg_iteration == 1)
                Input_Less = Input; Output_Less = Output;
             else
                [Input_Less Output_Less] = Extract_Features(src, tar, dsrc_Less, dx_Less, dy_Less, pbs_Less, pbs_sprd_Less, dispvec_Less, isz, mask, S); 
             end
%% One of these lines should be used:
            if MCase == 1,
                Input_Less(:,end-17:end) = []; % end-1:end
            else
%% Or this:
                Input_Less = Input_Less(:,end-17:end);  Input(:,end-17:end) = []; % end-1:end
            end
%%            
            Pred_Err_Less = B_Less.predict(Input_Less);
            Pred_Err = B.predict(Input);

%             thresh = mnx(Pred_Err); threshl = mnx(Pred_Err_Less);
%             thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.1; threshl1 = threshl(1)+ (threshl(2)-threshl(1))*.1; %.05
%             thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.5; threshl2 = threshl(1)+ (threshl(2)-threshl(1))*.5; % 65
             Uncert= Pred_Err; Uncert_Less = Pred_Err_Less;
            uncertainty = zeros(sz(mask)); uncertainty_Less = uncertainty;
            uncertainty(S) = Uncert; uncertainty_Less(S) = Uncert_Less;
%             uncertainty(Pred_Err<thresh1) = 1; uncertainty_Less(Pred_Err_Less<threshl1) = 1;
%             uncertainty(Pred_Err>thresh2) = 3; uncertainty_Less(Pred_Err_Less>threshl2) = 3;
%             uncertainty(Pred_Err>thresh1 & Pred_Err<thresh2) = 2; uncertainty_Less(Pred_Err_Less>threshl1 & Pred_Err_Less<threshl2) = 2;
                      
            Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 ));
            Warp_Err_Less = (sqrt(  (dx_Less - invTx).^2 + (dy_Less - invTy).^2 ));
            total_field_err = mean(Warp_Err(S));
            total_field_err_Less = mean(Warp_Err_Less(S));
            
%             Visualize_Erronous_Parts(src, invTx, invTy, Warp_Err(S), Pred_Err, mask);
%             Visualize_Erronous_Parts(src, invTx, invTy, Warp_Err_Less(S), Pred_Err_Less, mask);
            
%% Picking top certain pixels
            ops.pick_mode = 6; % 1: Original, 2: Edge, 3: Reduced, 4: Original Uncer, 5: Edge Uncer, 6: Reduce Uncer
            ops.num_seed = num_seed;
           if (Mode==1)
%                AllErr.AL( img_num-start_img, reg_iteration ) = total_field_err; % dice; %
%                 AllErr_Less.AL( img_num-start_img, reg_iteration ) = total_field_err_Less; %dice; %
% %                SegErr.AL( img_num-start_img, reg_iteration ) = total_field_err_pr; % dice; %total_field_err; %
%                [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands, tlands_pre);
%                [slands_new_Less tlands_new_Less slands_Less tlands_Less nseeds_Less] = Pick_Top_AL_Exper_(src, mask, uncertainty_Less, invTx, invTy, ops, slands_Less, tlands_Less,  tlands_pre_Less);
               ops.num_seed = 4;
               AllErr.Rand1( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_seg, maxd, ops, slands, tlands, 2);
               slands_new_Less = slands_new; tlands_new_Less = tlands_new; tlands_Less = tlands; slands_Less = slands; nseeds_Less = nseeds;
%                [slands_new_Less tlands_new_Less slands_Less tlands_Less nseeds_Less] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_seg, maxd, ops, slands_Less, tlands_Less, 2);
 
           end
           if (Mode==2)
               ops.num_seed = 4;
               AllErr.Rand2( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_seg, maxd, ops, slands, tlands, 1);
               slands_new_Less = slands_new; tlands_new_Less = tlands_new; tlands_Less = tlands; slands_Less = slands; nseeds_Less = nseeds;
%                [slands_new_Less tlands_new_Less slands_Less tlands_Less nseeds_Less] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_seg, maxd, ops, slands_Less, tlands_Less, 1);
           end
           if (Mode==4)
                AllErr.SL200( img_num-start_img, reg_iteration ) = total_field_err; %dice; %
                AllErr_Less.SL200( img_num-start_img, reg_iteration ) = total_field_err_Less; %dice; %
%                 SegErr.SL_All( img_num-start_img, reg_iteration ) = total_field_err_pr; % dice; %total_field_err; %
                AllErr.seed_SL200( img_num-start_img, reg_iteration ) = nseeds;
                ops.num_seed = 200;
                [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands);
                [slands_new_Less tlands_new_Less slands_Less tlands_Less nseeds_Less] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty_Less, dx_Less, dy_Less, ops, slands_Less, tlands_Less);
           end
           if (Mode ==7)
               ops.num_seed = num_seed * Pre_Max_Iterations;
               AllErr.AL_baseline( img_num-start_img, reg_iteration ) = total_field_err; % dice; %
                AllErr_Less.AL_baseline( img_num-start_img, reg_iteration ) = total_field_err_Less; %dice; %
%                SegErr.AL_baseline( img_num-start_img, reg_iteration ) = total_field_err_pr; % dice; %total_field_err; %              
               [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_(src, mask, uncertainty, invTx, invTy, ops, slands, tlands, tlands_pre);
               [slands_new_Less tlands_new_Less slands_Less tlands_Less nseeds_Less] = Pick_Top_AL_Exper_(src, mask, uncertainty_Less, invTx, invTy, ops, slands_Less, tlands_Less,  tlands_pre_Less);

           end            

            Disp = [dx(:) dy(:)];
            for my_index = 1:size(Disp,1),
                m = find(Disp(my_index,1)==dispvec(:,1)&Disp(my_index,2)==dispvec(:,2));
                ind(my_index) = m(1);
            end
            Disp_Less = [dx_Less(:) dy_Less(:)];
            for my_index = 1:size(Disp_Less,1),
                m_Less = find(Disp_Less(my_index,1)==dispvec_Less(:,1)&Disp_Less(my_index,2)==dispvec_Less(:,2));
                ind_Less(my_index) = m_Less(1);
            end
               
           if(Mode==2), 
               Uncertainty.Rand(:,:,reg_iteration,img_num-start_img) = uncertainty; 
               Displace_field.Rand(:,reg_iteration,img_num-start_img) = ind; 
               Uncertainty_Less.Rand(:,:,reg_iteration,img_num-start_img) = uncertainty_Less; 
               Displace_field_Less.Rand(:,reg_iteration,img_num-start_img) = ind_Less;
           end
           if(Mode==4), 
               Uncertainty.SL200(:,:,reg_iteration,img_num-start_img) = uncertainty;  
               Displace_field.SL_All(:,reg_iteration,img_num-start_img) = ind; 
               Uncertainty_Less.SL200(:,:,reg_iteration,img_num-start_img) = uncertainty_Less;  
               Displace_field_Less.SL_All(:,reg_iteration,img_num-start_img) = ind_Less;
           end
              
            if(numel(slands_new) ~= 0 && numel(tlands_new) ~= 0 )
                slands_n = slands;
                slands_n(:,1:4) = [];
                slands_n_Less = slands_Less;
                slands_n_Less(:,1:4) = [];
                if (Mode ~= 3 && Mode ~= 4)
                   [ dispvec2 regterm2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands_new, slands_new, nseeds);
                   [ dispvec2_Less regterm2_Less dterm2_Less nlabs2_Less ] = Calc_Likelihood2(tar, src, tlands_new_Less, slands_new_Less, nseeds_Less);
                    dispvec = [dispvec; dispvec2];
                    dterm_length = size(dterm, 3);
                    dterm2_length = size(dterm2, 3);
                    dterm(:,:, dterm_length+1:dterm_length+dterm2_length) = dterm2;
                    nlabs =  nlabs +  nlabs2;
                    dispvec_Less = [dispvec_Less; dispvec2_Less];
                    dterm_length_Less = size(dterm_Less, 3);
                    dterm2_length_Less = size(dterm2_Less, 3);
                    dterm_Less(:,:, dterm_length_Less+1:dterm_length_Less+dterm2_length_Less) = dterm2_Less;
                    nlabs_Less =  nlabs_Less +  nlabs2_Less;
                    dterm = Change_Dterm(src, slands_n, dterm, nlabs, nlabs2, dterm_mode);
                    dterm_Less = Change_Dterm(src, slands_n_Less, dterm_Less, nlabs_Less, nlabs2_Less, dterm_mode);
                else
                    dterm = Change_Dterm_SL(src, slands_n, dterm, pbs, nlabs, dterm_mode);
                    dterm_Less = Change_Dterm_SL(src, slands_n_Less, dterm_Less, pbs_Less, nlabs_Less, dterm_mode);
                end
                dterm = Change_Dterm_Neighbors(src, slands_n, dterm, dterm_mode);
                dterm_Less = Change_Dterm_Neighbors(src, slands_n_Less, dterm_Less, dterm_mode);
%             else
                
            end
           fprintf('end of iteration : %d\n', reg_iteration);
           reg_iteration = reg_iteration + 1;

        end  
        
        a=toc(tstart);
        if (Mode== 2), Time.Rand(img_num-start_img) = a; end
        if (Mode== 4), Time.SL200(img_num-start_img) = a; end      
        
        if (Mode== 2), 
            Landmarks.slands_Rand(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_Rand(:,:,img_num-start_img) = tlands; 
            Label_set.Rand(:,:,img_num-start_img) = dispvec; 
            Landmarks_Less.slands_Rand(:,:,img_num-start_img) = slands_Less; 
            Landmarks_Less.tlands_Rand(:,:,img_num-start_img) = tlands_Less; 
            Label_set_Less.Rand(:,:,img_num-start_img) = dispvec_Less;             
        end        
        if (Mode== 4), 
            Landmarks.slands_SL200(:,:,img_num-start_img) = slands; 
            Landmarks.tlands_SL200(:,:,img_num-start_img) = tlands; 
            Label_set.SL200(:,:,img_num-start_img) = dispvec; 
            Landmarks_Less.slands_SL200(:,:,img_num-start_img) = slands_Less; 
            Landmarks_Less.tlands_SL200(:,:,img_num-start_img) = tlands_Less; 
            Label_set_Less.SL200(:,:,img_num-start_img) = dispvec_Less;             
        end        
%     end
    end   
end
Save_Mat_set_Regress_( AllErr, AllErr_Less, Label_set, Label_set_Less, Displace_field, Displace_field_Less, ...,
    Uncertainty, Uncertainty_Less, Landmarks, Landmarks_Less, savepath, Max_Iterations, num_seed, Time, ...,
    trial_num, set_num, str, MCase, noise_sigma);
end
