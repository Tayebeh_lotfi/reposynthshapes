function RWR_synth_tumor_matlab()
    add_paths()
    [debug flag Max_Iterations step parameter1 parameter2 beta alpha noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters(); 

    I = mat2gray(rgb2gray((imread('../data/tumor2_1.jpg'))));
    IWF = mat2gray(rgb2gray((imread('../data/tumor2_2.jpg'))));
    I = imresize(I, [100 100]);
    IWF = imresize(IWF, [100 100]);
    mask = IWF>0.1;
    TxW = 10*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
    Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
    %              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
    simil_measure = 5; %MIND
%% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    isz = size(tar);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
    Noise = 0.01;
    for Mode = 1:24, 
        src = imnoise(IWF, 'gaussian', 0, (Mode-1) * Noise ); 

%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
        slands = slands_pre; 
        tlands = tlands_pre; 
        dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
        nseeds = size(tlands,2); 
%% Solving the problem
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
%% Shannon's Entropy
        [Shannon Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 2);
        [Unc_dterm ~] = Calc_Uncertainty_Shannon(dterm);
        Shannon_Vec(:,:,Mode) = Shannon;
        Shannon_Sp_Vec(:,:,Mode) = Shannon_Sprd;
        src_Vec(:,:,Mode) = src;
    end
    elnm = 20;
    dockf; montage(reshape(mat2gray(Shannon_Vec(:,:,1:elnm)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  elnm]  )),
    title('Shannon Uncertainty map'), colormap jet;
    dockf; montage(reshape(mat2gray(Shannon_Sp_Vec(:,:,1:elnm)),[size(Shannon_Sp_Vec,1)  size(Shannon_Sp_Vec,2) 1  elnm]  )),
    title('Modified Shannon Uncertainty map'), colormap jet;
    dockf; montage(reshape(mat2gray(repmat(tar, [1 1 elnm])),[size(tar,1)  size(tar,2) 1  elnm]  )),
    colormap(jet),  title('Fixed Image'), cbar
    dockf; montage(reshape(mat2gray(src_Vec(:,:,1:elnm)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm]  )),
    title('moving Image'), colormap jet;
end

