function Uncert_Add_Noise_Cluster(Mode, MCase, Noise_Level)

    if (nargin < 2)
        MCase = 2;
        Noise_Level = 1;
    end
    add_paths()
    root = '/home/ghassan/students_less/tlotfima';
    savepath = [root '/RWRegistration_Final2D/results/'];
    real_mode = 1; gamma = 2.2;
    switch(Mode)
        case 1
           nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
           [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
%            [pbs dispvec] = Creating_Spatial_Test_Unc_Edited(nlabs_size, Visualize_Labels, sample_num);
           nsample = size(pbs,1); data_mode = 2;
           nlabs = size(dispvec,1);
           isz = [nsample 1]; mask = ones(nsample,1);  
           G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
           G = G ./ repmat(sum(G,2), 1, nlabs);
           pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2);
        case 2
            nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
            [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
%             [pbs dispvec] = Creating_Spatial_Test_Unc_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 2;
            nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);  
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G;  [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2);
        case 3
            nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
            [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
%             [pbs dispvec] = Creating_Spatial_Test_Unc3D_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2); dz = dispvec(q,3);
        case 4
            nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
            [pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
%             [pbs dispvec] = Creating_Spatial_Test_Unc3D_Edited(nlabs_size, Visualize_Labels, sample_num);
            nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
            isz = [nsample 1]; mask = ones(nsample,1);
            G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
            G = G ./ repmat(sum(G,2), 1, nlabs);
            pbs_sprd = pbs * G; [~, q] = max(pbs'); dx = dispvec(q,1); dy = dispvec(q,2); dz = dispvec(q,3);
    end
    if (MCase == 1)
        dispvec = dispvec + 0.1 .* (Noise_Level-1) .* randn(nlabs,2); % + ... %0.05
%             ( (-1) .^ round(rand(nlabs,2))  .* (-0.05) .* (Noise_Level-1) );
%       figure, quiver(zeros(sz(dispvec,1),1), zeros(sz(dispvec,1),1), dispvec(:,2), dispvec(:,1), 'Color', 'c');
%         
    else
        if(MCase == 2)
            pbs = pbs + 0.05 * (Noise_Level-1) * rand(sz(pbs,1),sz(pbs,2)); % 0.01
            pbs = pbs ./ repmat(sum(pbs, 2), [1 nlabs]);
%             pbs_sprd = pbs_sprd + 0.01 * (Noise_Level-1)* rand(sz(pbs,1),sz(pbs,2));
%             pbs_sprd = pbs_sprd ./ repmat(sum(pbs_sprd, 2), [1 nlabs]);            
% 
%             mn = mnx(dispvec);
%             [gridX gridY] = ndgrid(mn(1):1:mn(2));
%             X = [gridX(:), gridY(:)]; 
%             prob1 = sqz(pbs(9,1:end));             
%             prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
%             figure, surf(gridX, gridY, prob1), xlim([mn(1)-.5 mn(2)+.5]), ylim([mn(1)-.5 mn(2)+.5]);
%             colormap(jet)
        end
    end
    G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
    G = G ./ repmat(sum(G,2), 1, nlabs);
    pbs_sprd = pbs * G;
%%
    Uncert_Mode = 3; % 0: KNN, 1: KNN&WCov, 2: KNN&KDE&WCov,  3: All,
    accuracy=4; epsilon_ = 1e-2; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    w = [3.3 2.2];
    [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = ...
    Calc_Uncertainty_MEX(pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
    %%
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
%     [time.delta Unc_Delta] = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs_sprd, dispvec, mask, w);

    if (Mode < 3)
        tic
        [Unc_ExpEr Unc_ExpEr_Map] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);
        time.Unc_ExpEr = toc; time.Unc_ExpEr_Map = time.Unc_ExpEr;
    else
        tic
        [Unc_ExpEr Unc_ExpEr_Map] = Calc_Uncertainty_Err_Dist2D_3D(pbs, dispvec, dx, dy, dz);
        time.Unc_Err_Dist = toc; time.Unc_Err_Dist_Map = time.Unc_Err_Dist;     
%         [Wcov SGauss] = Calc_Uncertainty_WCov3D(pbs, dispvec, isz, mask, epsilon_, data_mode);           

    end
%     if(synth==0 && real_mode ==0)
%         Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, ...
%             GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, ...
%             GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, nlabs_size, sample_num, Unc_str, data_mode);
%     end
    file_name = {'Unc_Shannon', 'Unc_Shannon_Sprd', 'Wcov', 'SGauss', 'GMM2_avg', 'Unc_Kde', 'Unc_Knn', 'Unc_ExpEr', 'Unc_ExpEr_Map', 'time'}; %'Unc_Delta',  
    if (MCase == 1)
        for i = 1:10,
            save (num2str([savepath 'res_unc_add_noise/' file_name{i} '_disp_addnoise' num2str(Noise_Level-1) '.mat']), file_name{i}); 
        end
    else
        for i = 1:9,
        save (num2str([savepath 'res_unc_add_noise/' file_name{i} '_prob_addnoise' num2str(Noise_Level-1) '.mat']), file_name{i}); 
        end
    end
    
%% Merge the matrices
% %%   MCase = 1: Uncert_noisedisp, 2: Uncert_noiseprob
%     file_name = {'Unc_Shannon', 'Unc_Shannon_Sprd', 'Wcov', 'SGauss', 'GMM2_avg', 'Unc_Kde', 'Unc_Knn', 'Unc_ExpEr', 'Unc_ExpEr_Map'}; % , 'time'
%     for MCase = 1:2,
%       for Noise_Level = 1:11,
%         if( MCase == 1) 
%             for i = 1:9,
%                load (['../results/res_unc_add_noise/' file_name{i} '_disp_addnoise' num2str(Noise_Level-1) '.mat']);
%             end
%          else
%             for i = 1:9,
%                load (['../results/res_unc_add_noise/' file_name{i} '_prob_addnoise' num2str(Noise_Level-1) '.mat']);
%             end
%          end
%          Shannon_Vec(:,Noise_Level) = Unc_Shannon;
%          Shannon_Sprd_Vec(:,Noise_Level) = Unc_Shannon_Sprd;
%          Wcov_Vec(:,Noise_Level) = Wcov;
%          SGauss_Vec(:,Noise_Level) = SGauss;
%          GMM2_avg_Vec(:,Noise_Level) = GMM2_avg;
%          Kde_Vec(:,Noise_Level) = Unc_Kde;    
%          Knn_Vec(:,Noise_Level) = Unc_Knn; 
%          ExpEr_Vec(:,Noise_Level) = Unc_ExpEr; 
%          ExpEr_Map_Vec(:,Noise_Level) = Unc_ExpEr_Map; 
% %          Delta_Vec(:, Noise_Level) = Unc_Delta;
%          %time_Vec(Noise_Level) = time;
%       end
%       if( MCase == 1) 
%          save ('../results/res_unc_add_noise/Uncert_noisedisp.mat', 'Shannon_Vec', 'Shannon_Sprd_Vec', 'Wcov_Vec', 'SGauss_Vec', 'GMM2_avg_Vec', 'Kde_Vec', 'Knn_Vec', 'Unc_ExpEr', 'Unc_ExpEr_Map'); % , 'time_Vec' 'Delta_Vec',
%       else
%          save ('../results/res_unc_add_noise/Uncert_noiseprob.mat', 'Shannon_Vec', 'Shannon_Sprd_Vec', 'Wcov_Vec', 'SGauss_Vec', 'GMM2_avg_Vec', 'Kde_Vec', 'Knn_Vec', 'Unc_ExpEr', 'Unc_ExpEr_Map'); %, 'time_Vec' 'Delta_Vec',
%       end
%     end

end