
function [Icor  Tx  Ty] = rBSPwarp( im, Spacing, mag, trial_num)
% function [Icor  Tx  Ty] = rBSPwarp( im, Spacing, mag )
% Applies random warp computed with B-spline bases to im
% 
% Usage: rBSPWarp( image, [10 15], 2)
%   
%           - Spacing of control point grid (lesser spacing, more irregular the warp is)
%           - max displacement magnitude of displaced control points
% clf; 
% subplot 121; ims( Icor ); title 'warped image'
% [mx my]=meshgrid(1:ht, 1:wd);
% d=interp2( mx, my, im , mx + Ty, my  + Tx, 'linear' , 0);
% subplot 122; ims( d - Icor ); title 'Evaluate'
% 
% Adapted from code provided by D. Kroon 
 
[wd ht]=size(im);
% [wd ht]=size(mask);

% plot(dx,dy,'.');

[O_trans]=make_init_grid(Spacing, [wd ht]);
N=sz(O_trans(:,:,1));
W=1;
B=mag; %B = 7 %15 %20
if(nargin>3)
    rng(trial_num);
end
t=rand(N)*2*pi;
r=rand(N)*0.02*W+(B+((-1)^round(rand))*(-0.02)*W);
dx=r.*cos(t);
dy=r.*sin(t);
d(:,:,1) = dx;
d(:,:,2) = dy;
O_trans =O_trans + d;

% Transform the input image with the found optimal grid.
[Icor  Tx  Ty]=bspline_transform(O_trans, im,Spacing,3); 

 
