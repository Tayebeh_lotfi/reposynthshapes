function newI = zero_padding(I , rows, columns)

[h w d ] = size(I);

height = h + rows;
width = w + columns ;

newI = zeros(height, width,d );

for k = 1:d
    for i = 1:h
        for j = 1:w

            newI(i,j,k) = I(i,j,k);

        end
    end
end





end