function out =  squareDeformation(in)


% figure
% imshow(in , []);
[height width dim] = size(in);
factor = 8;
cx = width/2 ;
cy = height/2; 

len = width/4;
breadth = height/4;

col_num_left = cx - len ;
col_num_right = cx + len ;

row_num_top = cy - breadth ;
row_num_bottom = cy + breadth;


Yr = row_num_top : row_num_bottom ;dyr = factor* zeros(1,1 + 2*len);
Yl = row_num_bottom :-1: row_num_top ;dyl = factor* zeros(1,1 + 2*len);
Yt = row_num_top*ones(1,1 + 2*breadth);dyt = factor* ones(1,1 + 2*breadth);
Yb = row_num_bottom *ones(1,1 + 2*breadth);dyb = -factor* ones(1,1 + 2*breadth);


Xt = col_num_left : col_num_right ;dxt = factor* zeros(1,1 + 2*len);
Xb = col_num_right:-1: col_num_left ; dxb = factor* zeros(1,1 + 2*len);
Xr = col_num_right*ones(1,1 + 2*len);dxr = -factor* ones(1,1 + 2*breadth);
Xl = col_num_left *ones(1,1 + 2*len);dxl = factor* ones(1,1 + 2*breadth);

X = [Xt , Xr , Xb , Xl];
dx = [dxt,dxr,dxb,dxl];



Y = [Yt , Yr,  Yb,  Yl];
dy = [dyt,dyr,dyb,dyl];

% size(Xt)
% size(dxt)
% 
% size(Xr)
% size(dxr)
% 
% size(Xb)
% size(dxb)
% 
% size(Xl)
% size(dxl)
% pause

% size(Y)
% size(dy)


X= X(:);
Y = Y(:);
dx = dx(:);
dy = dy(:);

Fx = TriScatteredInterp(X,Y,dx);
Fy = TriScatteredInterp(X,Y,dy);

[qx,qy] = meshgrid(1:height, 1:width);
qzx = Fx(qx,qy);
qzy = Fy(qx,qy);


A = isnan(qzx);
A= ~A;
% figure
% imshow(A,[]);
%mesh(qx,qy,qz);
%hold on;
%plot3(x,y,z,'o');
qzx(A==0)=0;
qzy(A==0)=0;


% figure;
% imshow(qx + qzx , []);
% figure;
% imshow( qy + qzy, []);

out = interp2(in, (qx + qzx ), (qy + qzy ));
% 
% figure(101)
% imshow(uint8(qzx)  );
% 
% figure(102)
% imshow(uint8(qzy) );
% 
% 
% 
% figure
% imshow(qzx.^2 + qzy.^2  , []);



figure(104)
imshow(uint8(out));


















end