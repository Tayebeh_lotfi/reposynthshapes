function [I_transformed,qzx,qzy] = increase_resolution( I  , Mx, My)

[height width d] = size(Mx);

height2 = 2 *height;
width2 =  2 * width;

[X,Y] = meshgrid(1:width, 1: height);

X = 2*X ;
X = X(:);
Zx = Mx(:);


Y = 2*Y ;
Y = Y(:);
Zy = My(:);


Fx = TriScatteredInterp(X,Y,2*Zx);
Fy = TriScatteredInterp(X,Y,2*Zy);




[X2 Y2] = meshgrid(1 :width2 , 1:height2);
qzx = Fx(X2,Y2);
qzy = Fy(X2,Y2);


A = isnan(qzx);
A= ~A;

qzx(A==0)=0;
qzy(A==0)=0;




I_transformed = movepixels(I, qzx, qzy);

figure
quiver(X2 , Y2 , qzx, qzy);
% pause








end