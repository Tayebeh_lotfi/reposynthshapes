clc
clear all
close all


%%%%%%%%%% artificially deformed image %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  img=im2double(imread('D:\research\FastRW\FastRW\images\checkboard.png')); 
%  img_ref=im2double(imread('D:\research\FastRW\FastRW\images\checkboard.png'));
 img = checkerboard(20);
 img_ref = img;
 img = squareDeformation(img);

 img= zero_padding(img, 3,3); %make height and width multiple of 4
 img_ref= zero_padding(img_ref, 3,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





[h,w] = size(img_ref);

img_ref_4 = imresize(img_ref,0.25);
img_4  = imresize(img,0.25);
img_ref_2 = imresize(img_ref,0.5);
img_2  = imresize(img,0.5);



%%%%%%%%%%%%%%%%%%%%%%%%computation of Level 2 of Pyramid%%%%%%%%%%%%%%%%%%
 alpha = 1;
 window = 30;
 beta = 0.0005 ;

 gamma = 10E-4;
 s_factor_x = 0.05;
 s_factor_y = 0.05;

[dsrc, dx, dy] = Register_Image(img_4,img_ref_4,  alpha , beta, gamma, s_factor_x,s_factor_y, window );


%compute the source at next level
[img_2,~,~] =  increase_resolution( img_2 , -s_factor_x*dx, -s_factor_y*dy);

clear dsrc dx dy

 

 
 
 %%%%%%%%%%%%%%%%%%%%%computation of Level 1 of Pyramid%%%%%%%%%%%%%%%%%%%%
 
 window = 20;
 beta = 0.0005 ;

 gamma = 10E-4;
 s_factor_x = 0.5;
 s_factor_y = 0.5;


 [dsrc, dx, dy] = Register_Image(img_2,img_ref_2,  alpha , beta, gamma, s_factor_x,s_factor_y, window );
 
 
 [img,~,~] =  increase_resolution( img , -s_factor_x*dx, -s_factor_y*dy);
 
 clear dsrc dx dy

 
%%%%%%%%%%%%%%%%%%%%%computation of Level 0 of Pyramid%%%%%%%%%%%%%%%%%%%%%
 
 window = 15;
 beta = 0.0005 ;

 gamma = 10E-4;
 s_factor_x = 0.5;
 s_factor_y = 0.5;

 [dsrc, dx, dy] = Register_Image(img,img_ref,  alpha , beta, gamma, s_factor_x,s_factor_y, window );
 
 figure
 subplot(1,3,1);
 imshow(img,[]);
 subplot(1,3,2);
 imshow(img_ref,[]);
 subplot(1,3,3);
 imshow(dsrc,[]);
 
 
 SSD_score = norm((dsrc - img_ref),2)
 
 
 
 
 
 
 
