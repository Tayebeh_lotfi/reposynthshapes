
% Usage: 
%[M, Mx, My] = Register_Image(img,img_ref , alpha ,
%                  beta, gamma, s_factor_x,s_factor_y, window )
%
% output : M  = registered image
%          Mx = x-deformation field
%          My = y-deformation field
%

function [dsrc, dx, dy] = Register_Image(img,img_ref , alpha , beta, gamma, s_factor_x,s_factor_y, window )

%seed_list is optional, if input should be a S x 3 matrix of [x,y,r] tuples

nLabels = (2*window + 1) * (2*window + 1);

% Set the default input parameters


% Check for the function makeweights to see if the graph toolbox is already
% part of the search path.
if ~exist('makeweights')
    if ~exist('graphAnalysisToolbox-1.0', 'dir')
        disp('Error: Please install Graph Toolbox')
        return
    elseif exist('graphAnalysisToolbox-1.0', 'dir')
        addpath([pwd, '/graphAnalysisToolbox-1.0']);
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%PRIOR COMPUTATION%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Discretization of prior space using the Graph Cut formulation of Image
% Registration.
% x + D(x) in Eqn. 5 can be any non-integer valued vector, and J(x + D(x)) 
%needs to be computed using an interpolation function. 



%Quoting from the paper :
%To convert our optimization to a labeling problem, (D(x) belongs to R^d) should be
%limited into a finite set. Here, we perform the second discretization step. Also
%acting as a restriction of how far a pixel can be displaced, a discretized window
%W = {0,�s,�2s, ...,�ws}^d of dimension d is chosen such that (D(x) belongs to W). Note
%that W is the discretization of the continuous dimension-d region [-ws, ws]^d
%with sampling period s along all directions.
%In our implementation we call  s = scale_factor



% Input:
% img -  grey scale image to get registered
% img_ref -  grey scale reference image 
% window - number of displacement labels for each pixel / control point in
% each direction , in each dimension : total of (2*window + 1)*(2*window +
% 1)displacement labels for each pixel. (Discrete formulation of
% registration using Graph Cut)

% Computation Metric used : SSD : exp( - alpha^2*(img(x + D(x)) - img_ref(x)).^2);
%
% Output:
% priors - an N by (2*window + 1)*(2*window + 1) matrix with  prior probabilities for each 
%   possible displacement of each pixels .
%

%priors = Calc_Priors_modified(img,img_ref,window, s_factor_x, s_factor_y, alpha);

[height width]  = size(img);

%OUT OF MEMORY ERROR ?
%img_ref_cols = repmat(img_ref(:), 1 ,  (2*window + 1)*(2*window + 1) );
%warped_img_cols =  zeros(height*width, (2*window + 1)*(2*window + 1) );
        
priors =  zeros(height*width, (2*window + 1)*(2*window + 1) );   

        for wy = 1 : (2*window + 1) %[-ws, ws] along vertical
           
            Zy = s_factor_y*(wy-window-1)*ones(height,width);
            

            for wx = 1 : (2*window + 1) %[-ws, ws] along horizontal

                
                
                Zx = s_factor_x*(wx-window-1)*ones(height,width);
              

                warped_img = movepixels(img , -Zx, -Zy); %interpolation function used to compute img(x + D(x))

                similarity_matrix =  exp( - alpha^2*( warped_img - img_ref).^2); 
                
                priors(:,(2*window + 1)*(wy-1)+ wx) = similarity_matrix(:);
                
                clear warped_img 

            end
        end

% OUT OF MEMORY error for vector computation       
%priors = exp( - alpha^2*( warped_img_cols - img_ref_cols).^2);        
priors = priors./repmat(sum(priors,2),1,(2*window + 1)*(2*window + 1));
%clear warped_img_cols img_ref_cols 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END OF COMPUTATION%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Create the image graph and find its Laplacian L
[X Y]=size(img);

width  = Y;
height = X;

[points, edges]=lattice(X,Y, 0);

weights = makeweights(edges,img(:),beta);
%weights = makeweights(edges,img(:),beta);

L = laplacian(edges, weights);
clear edges, clear points, clear weights

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%solving the equation%%%%%%%%%%%%%%%%%%%%%%%%%
seeds = [];      
num_seeds = length(seeds);
N = length(L);
LHS = L + (gamma)*speye(N-num_seeds);
RHS =   (gamma)*priors;
prob_seg = LHS\RHS ;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%end of solution%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





% Create a binary segmentation
%binary_seg = Threshold_Probs(probs);
%block_img = reshape(binary_seg(:),[X Y Z length(unique(seeds(:,2)))]);


X_Res = reshape(prob_seg, [height width nLabels]);
[~, reg_result] = max(X_Res, [], 3);


dx = size(reg_result);
dy = size(reg_result);



%%%%%%%recover deformation field from the assigned labels%%%%%%%%%%%%%%%%%%
for i = 1:height
    for j = 1:width
        
        displacement = reg_result(i,j);
        dx(i,j) = mod(displacement,2*window + 1)-(window + 1);
        dy(i,j)= floor(displacement/(2*window + 1))+1 -(window + 1);
        
        
    end
end

close all

%apply deformation field on the source image to register
dsrc=movepixels(img,-s_factor_x*dx,-s_factor_y*dy);



dockf;imagesc(dsrc);

dockf;imagesc(dx);

dockf;imagesc(dy);



















