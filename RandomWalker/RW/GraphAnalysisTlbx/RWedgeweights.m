function [ gw weights]=RWedgeweights(points,edges, ops)

% function gw=RWedgeweights(points,edges, ops)
% 
%   ops.weightFun = {'reciprocal', 'gaussian' }

if ~isfield( ops, 'weightFunc' ) ops.weightFunc =  'gaussian'; end
if ~isfield( ops, 'beta' ) ops.beta =  1000; end

numOfEdges = size(edges,1);
valDistances = zeros([numOfEdges 1]);

%as we may have out of memory problems
numOfDivisions = 5;
numOfEdgesPerIteration = floor(numOfEdges  / numOfDivisions);
edgesDiv = 1:numOfEdgesPerIteration:numOfEdges;
numOfIterations = length(edgesDiv);

if(edgesDiv(numOfIterations) ~= numOfEdges)
     edgesDiv = [edgesDiv numOfEdges];
end
%%
weights = ones( 1,length(edgesDiv) );
gws=weights;
 
numOfIterations = length(edgesDiv);
if exist( 'ops', 'var') && isfield( ops,'Im')
    for i = 1:numOfIterations-1
    fromEdges = edgesDiv(i);
    ToEdges = edgesDiv(i+1);
    currentEdges = fromEdges:ToEdges;
    edge1Index = edges(currentEdges,1);
    edge1 = points(edge1Index,:);
    edge2Index = edges(currentEdges,2);
    edge2 = points(edge2Index,:);
 
    in1= sub2ind( sz(ops.Im), 1+edge1(:,2), 1+ edge1(:,1) );
    in2= sub2ind( sz(ops.Im), 1+edge2(:,2), 1+ edge2(:,1) );
    
    
    weights(currentEdges) =  (  ops.Im( in1 )  -  ops.Im( in2)  ).^2 ; 
    end 
    
     weights=scale2range(weights);
end

for i = 1:numOfIterations-1
    fromEdges = edgesDiv(i);
    ToEdges = edgesDiv(i+1);
    currentEdges = fromEdges:ToEdges;
    edge1Index = edges(currentEdges,1);
    edge1 = points(edge1Index,:);
    edge2Index = edges(currentEdges,2);
    edge2 = points(edge2Index,:);
    % dist on lattice     
    deltaT = edge1 - edge2; 
    deltaT_scan = deltaT(:,1).^2 +  deltaT(:,2).^2 ;
    valDistances(currentEdges,1) = weights(currentEdges)' .* sqrt(deltaT_scan);
end
 
 
 valDistances=  valDistances/max(valDistances); %Normalize to [0,1]


%Compute Gaussian weights
% weights=exp(-(valScale*valDistances))+...
%      EPSILON;

%reciporcal weighting function
EPSILON = 1e-5;
 
if strcmp( ops.weightFunc, 'gaussian')  % minimize  
    %%
    gw = exp( - (  valDistances* ops.beta).^2  ) ;
%     hist(gw,100)

elseif strcmp( ops.weightFunc, 'reciprocal')  % minimize  
    %%
    gw = ops.beta ./ (valDistances+EPSILON);
    gw =( gw - mean(gw)) / std(gw);
    gw = scale2range(gw);
%     hist(gw,100)
end


    
