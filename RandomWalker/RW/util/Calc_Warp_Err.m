% function [total_land_err total_field_err cross] = Calc_Warp_Err(dx, dy, isz, tlands_test, slands_test, scene, ...,
function [total_land_err] = Calc_Warp_Err(dx, dy, invTx, invTy, seeds)
%     mask = src_segment>0;
   total_land_err = mean(sqrt(  (dx(seeds) - invTx(seeds)).^2 + (dy(seeds) - invTy(seeds)).^2 )); % dx - invTx: SL not good, dx + invTx: All bad ?!!!, dx + invTy: All bad, dx - invTy: All bad
%    total_land_err = mean(sqrt(  (dx(mask) + Ty(mask)).^2 + (dy(mask) + Tx(mask)).^2 )); 
end
