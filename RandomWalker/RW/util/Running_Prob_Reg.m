function [dx_sprd dy_sprd dx dy pbs_bef pbs_aft dsrc dsrc_sprd d] = Running_Prob_Reg(src, tar, slands, tlands, dispvec, nlabs, edges, dterm, isz, alpha, gamma)

%% find closest corrlabels for the corresp.
nseeds = size(slands,2);
corrlabels = [];
for s=1:nseeds
d=tlands(:,s)  - slands(:,s);
[seederrs(s) a]=min(sum( (dispvec - repmat( d', [ nlabs 1] ) ).^2, 2) );
corrlabels = [corrlabels a];
end

% seedIndx = sub2ind( isz, round(slands(1,:)), round(slands(2,:)) );
seedIndx = sub2ind( isz, round(slands(1,1:nseeds)), round(slands(2,1:nseeds)) );
% seedIndx = sub2ind( isz, round(slands(1,1:nseeds)), round(slands(2,1:nseeds)), round(slands(3,1:nseeds)) );
% For 3D landmarks

disp(sprintf('Mean err in corresp. labels = %.2f', mean(seederrs)))

%% setup matrix encoding boundary conditions
% nlabs = size(dispvec,1);
%  bound_conds=zeros(nseeds,nlabs);
%  for j=1:nseeds
%      for k=1:nlabs
% %         bound_conds(:,k)=(corrlabels(j)==k);
%         bound_conds(j,k)=(corrlabels(j)==k);
%      end
%  end
      
nlabs = size(dispvec,1);
bound_conds=zeros(nseeds,nlabs);
 for k=1:nlabs
    b=zeros(nseeds, 1); 
    b(corrlabels==k ) =1;
    bound_conds(:,k)=b;
 end
 
 % the graph weights, may feed in options. (3rd arg); if so, call L=laplacian(edges)graphWeights);
% tic
% graphWeights= RWedgeweights(points,edges, [] );
% toc

% Laplacian matrix
L=laplacian(edges);
% beta = 0.05;

%%  Solve!
% gamma = .5;
% reshape for input to solver; note the "-" sign. 
data_prior = exp(- reshape( dterm, prod(isz), [] )/gamma);
mn1 = min(data_prior,[],2);
mn2 = max(data_prior,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
data_prior = (data_prior - minmat) ./ (maxmat-minmat);
data_prior(isnan(data_prior)) = 1;
% %gamma = .91; % [0,1]: more trust is given to corresp. when gamma is high 

% % gamma = 0.1;
% 
% %%%% Added part
% % gamma = 0.1;
% %%%% Added Part
alpha = 5000;
disp('Now solves the problemb w/ RW...')
tic
pbs_bef=RWinvert(L, seedIndx, bound_conds,data_prior,alpha, isz);
toc
% pbs_bef = pbs_bef ./ repmat(sum(pbs_bef,2), [1, nlabs]);
gam = 1; % 2.2;
G = 1 ./ (dist2(dispvec, dispvec) .^ gam + 1);
G = G ./ repmat(sum(G,2), 1, nlabs);
pbs_aft = pbs_bef * G;

%% calc most prob. solution    
[~, q] = max(pbs_bef');
Label = reshape(q , isz(1), []);    
[~, q_sp] = max(pbs_aft');
Label_sp = reshape(q_sp , isz(1), []);     

pbs_bef = reshape(pbs_bef,[ isz(1) isz(2) nlabs]);
pbs_aft = reshape(pbs_aft,[ isz(1) isz(2) nlabs]);

% %     dockf; hist(sqz(pbs(120,100,:)), 30), title('before spreading labels');
% %     dockf; hist(sqz(pbs_sprd(120,100,:)), 30), title('after spreading labels');
% 
% %     data_priorn = reshape(data_priorn,[ isz(1) isz(2) nlabs]);
% %     data_prior = reshape(data_prior,[ isz(1) isz(2) nlabs]);
%%     Compute Displacement Vector Field
dx_sprd = zeros(isz );
dy_sprd = zeros(isz );
dx = zeros(isz );
dy = zeros(isz );    
method='max';
switch method 
    case 'weighted' % not a confirmed approach
        dx_sprd=zeros(isz); dy_sprd=dx_sprd;
        dx=zeros(isz); dy=dx;
%             pbs2= pbs_bef ./ repmat( sum( pbs_bef, 3), [1 1 nlabs] ); %normalize for 2D data
%         pbs2= pbs ./ repmat( sum( pbs, 4), [1 1 1 nlabs] ); %normalize for 3D data       
        for i=1:nlabs
                dx = dx + pbs_bef(:,:,i).*dispvec(i,1);
                dy = dy + pbs_bef(:,:,i).*dispvec(i,2);  
                dx_sprd = dx_sprd + pbs_aft(:,:,i).*dispvec(i,1);
                dy_sprd = dy_sprd + pbs_aft(:,:,i).*dispvec(i,2);
%                 dz = dz + pbs2(:,:,:,i) .* dispvec(i,3); % For 3D data
        end    
    case 'max'
        [dx dy]=label2field( Label, dispvec );
        [dx_sprd dy_sprd]=label2field( Label_sp, dispvec );
%         [dx dy dz]=label2field( Label, dispvec );    
    otherwise
        % come up w/ your own conversion scheme
end

[mx my] = ndgrid(1:isz(1), 1:isz(2)); 
dsrc = interp2( my, mx, src, my + dy, mx + dx,  '*linear',0);     
dsrc_sprd = interp2( my, mx, src, my + dy_sprd, mx + dx_sprd,  '*linear',0); 

d=dispvec( corrlabels,:);  % correspondences' closest labels  