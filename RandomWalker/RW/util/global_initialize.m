function global_initialize(num)
    global count;

    if nargin > 0
        count = num;
    end
end