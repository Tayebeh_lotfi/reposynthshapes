function slands = Estimate_Corresp(tlands, src, tar, maxD)
    resolution = 500;
    res = 2*maxD / round(sqrt(resolution));
    tar= double(tar/max(tar(:)));
    src= double(src/max(src(:)));
    tar(isnan(tar)) = 0;
    src(isnan(src)) = 0;

%% Defining Labels and Sc, if not given
    if maxD ==0
        nl = 1; Disp = [0 0];
    end
    
%% I changed the code here to define new labels based on the landmarks directions!
% Instead of the below two lines:
    [Ux Uy]=ndgrid(-maxD:res:maxD);
    Disp=[Ux(1:end)' Uy(1:end)']; %49 labels  Disp={-3,-2,-1,0,1,2,3}^2  (for x and y direction)
    [mx my] = ndgrid(1:size(src,1), 1:size(src,2));
    tar_lands_inds = sub2ind( size(tar), round(tlands(1,:)), round(tlands(2,:)) );
%     Disp=[Disp; tlands' - slands'];
    nl=size(Disp,1);
    for land_num = 1 : size(tlands, 2),
        for label = 1:nl,
            slands_calc(:,label) = [tlands(1,land_num)  - Disp(label,1); tlands(2,land_num) - Disp(label,2)];

%             tar_lands_inds = sub2ind( size(tar), round(tlands(1,:)), round(tlands(2,:)) );
%             src_lands_diff(land_num,label)=tar(tar_lands_inds(land_num)) - interp2( my, mx, src , tlands(2,land_num) - Disp(label,2), tlands(1,land_num)  - Disp(label,1), '*linear' , 0);
%             src_lands_est = sub2ind(size(src), round(tlands(1,land_num) - Disp(lable,1)), round(tlands(2,land_num)  - Disp(lable,2)), round(tlands(3,land_num) - Disp(lable,3)));
%             tar(tar_lands_inds(land_num)) - src_lands_val(land_num,lable);
        end
        src_lands_inds = sub2ind( size(src), round(slands_calc(1,:)), round(slands_calc(2,:)) );
        img_diff(land_num,:) = tar(repmat(tar_lands_inds(land_num),[1 nl])) - src(src_lands_inds);
        [dif, sel_label] = min(abs(img_diff(land_num,:)));
        slands(:,land_num) = tlands(:,land_num) - Disp(sel_label,:)';
    end
end