function IWF = Circle_Artfct(IWF, intx, inty, rad)
    
    m = size(IWF);
    [rr cc] = ndgrid(1:m(1), 1:m(2));
    C = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
    IWF(C) = IWF(round(size(IWF,1)/2),round(size(IWF,2)/2)); % 1; 
%     IWF = IWF + C .* IWF;
end