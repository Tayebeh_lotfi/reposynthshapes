function Simulate_Warp()
    clc, close all, clear all
    path1 = 'H:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/';
    trial_num = 1; first_img = 1;
%     path2 = 'H:/students_less/tlotfima/RWRegistration_Final2D/data';
    load(num2str([path1 'trial' num2str(trial_num) '_large_warp_corner.mat']));
    Tx = Tx3D(:,:,first_img); Ty = Ty3D(:,:,first_img);
    clear Tx3D; clear Ty3D; clear invTx3D; clear invTy3D;
    D(:,:,1) = Tx; D(:,:,2) = Ty;
    invD = invertDefField(D);
    clear src_segment3D; clear src3D;
    
    isz = 256;
    m = [isz isz];
    [u v] = meshgrid(1:m(2),1:m(1));
    for img_num = 1:size(tar3D,3),
        src3D(:,:,img_num) =  interp2(u,v,tar3D(:,:,img_num),u+Ty(:,:,first_img),v+Tx(:,:,first_img), '*linear');
        src_segment3D(:,:,img_num) =  interp2(u,v,segment3D(:,:,img_num),u+Ty(:,:,first_img),v+Tx(:,:,first_img), '*linear');
        invTx3D(:,:,img_num) = invD(:,:,1); invTy3D(:,:,img_num) = invD(:,:,2);
        Tx3D(:,:,img_num) = Tx; Ty3D(:,:,img_num) = Ty;
    end
    
%     save( ['/home/ghassan/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(trial_num) '_large_warp_corner.mat'], ...
%         'tar3D', 'src3D', 'ground_src3D', 'Tx3D', 'Ty3D', 'invTx3D', 'invTy3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');  

    save( ['H:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(trial_num) '_sim_large_crnr.mat'], ... 
        'tar3D', 'src3D', 'ground_src3D', 'Tx3D', 'Ty3D', 'invTx3D', 'invTy3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');
end