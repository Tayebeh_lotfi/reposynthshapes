function Diff_set = Calc_Similarity (Warped_set, tar3D,iter)
%     iter1 = 10; iter2 = 2;
%     Warped_Vol = Calculate_Warped_Vol(src3D(:,:,:), Label_set, Displace_field);
     Diff_set.SL50 = Warped_set.SL50 - repmat(tar3D,[1 1 1 iter]);
     Diff_set.SL100 = Warped_set.SL100 - repmat(tar3D,[1 1 1 iter]);
     Diff_set.SL200 = Warped_set.SL200 - repmat(tar3D,[1 1 1 iter]);     
     Diff_set.AL = Warped_set.AL - repmat(tar3D,[1 1 1 iter]);
     Diff_set.Rand = Warped_set.Rand - repmat(tar3D,[1 1 1 iter]);
     Diff_set.RandG = Warped_set.RandG - repmat(tar3D,[1 1 1 iter]);
     Diff_set.AL_Base = Warped_set.AL_Base - repmat(tar3D,[1 1 1 iter]);    
end