% function [all_pix_err] = Show_Reg_Res(src, tar, dsrc, dx, dy, d, cross, slands, tlands)
function Show_Reg_Res(src, tar, dsrc, dx, dy, d, slands, tlands, debug, reg_iteration, step, Mode, Mode_Str)

    if ( debug == 1 && (step == 1 || mod(reg_iteration, step) == 1) )
        res = 5; %3; % quiver-plot's resln
        if ~exist('f1', 'var'); 
            f1=dockf; 
            figure(f1), 
        else
            f2=dockf;
            figure(f2), 
        end

        figure(f1), clf,  subplot 331,ims( tar' ),title ([Mode_Str{Mode} ' Mode,  ' 'Tar']);
        subplot 332,ims( src' ),title (['iteration:' num2str(reg_iteration) ', Src']);
        subplot 333,ims(dsrc'),title 'Warped Src (WSrc) ';
        subplot 334,ims(src' );
        quiverr(dx, dy, [1 1]*res, 0 ,'r');
        plot( slands(1,1:end), slands(2,1:end), '*g' );
        plot( tlands(1,1:end), tlands(2,1:end), '*b' );
        % quiver( slands(1,1:end), slands(2,1:end), d(1:end,1)' , d(1:end,2)', 0 ,'g');
        quiver( slands(1,1:end), slands(2,1:end), d(1:end,1)' , d(1:end,2)', 0 ,'g');
        title 'Src + Sln + Provided corresp';

        subplot 335,ims( abs(tar'- src') ),title '|Tar - Src |';
        subplot 336,ims( abs(tar'- dsrc') ),title '|Tar - WSrc |';
%          subplot 337, ims( abs(ground_src'- tar') ),title '|Tar - GSrc |';
%          subplot 338, ims( abs(src' - ground_src') ),title '|GSrc - Src |';
%          subplot 339, ims( abs(dsrc' - ground_src') ),title (['|GSrc - WSrc |' 'should become zero']);

%          title([Mode_Str{Mode} ' Mode']);
        cmapg;
    end
end
% all_pix_err = sqrt(sum(sum((tar - dsrc).^2)));