function Show_Landmarks(tlands, slands, tar, src, debug, reg_iteration, step, Mode, Mode_Str)
    if ( debug == 1 && (step == 1 || mod(reg_iteration, step) == 1) )
    %    if ~exist('f1', 'var'); 
            f1=dockf; 
            figure(f1),  
            m = size(tar);    
            subplot 121, imagesc(tar), colormap(gray), title([Mode_Str{Mode} ' Mode ']);
            hold on, plot(tlands(2,:), tlands(1,:), 'r*');
            subplot 122, imagesc(src), colormap(gray), title(['iteration:' num2str(reg_iteration)]);
            hold on, plot(slands(2,:), slands(1,:), 'g*');
    end
end