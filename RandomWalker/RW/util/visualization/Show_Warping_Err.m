function Show_Warping_Err(allerr_Rand, allerr_SL, allerr_AL, param1_size, param2_size, param1, param2)

error_measure = {'MSE Field Warping Error,     ' 'Intensity diff to original' 'Intensity diff to ground truth' 'Landmark diff' };

for i = 1 : size(allerr_Rand, 1),
  figure, 
  image_number = floor(i/(param1_size*param2_size) ) + 1;
  residual = mod(i, (param1_size*param2_size) );
  lambda_ind = round(residual/param1_size) + 1;
  cert_ind = mod(residual, param1_size);
  if(cert_ind == 0) 
      cert_ind = param1_size;
  end  
  plot(allerr_Rand(i, 1:end), '-.','color',[0.4 0.2 0], 'LineWidth', 2, 'MarkerEdgeColor','k', 'MarkerFaceColor','g', 'MarkerSize',3), title('all pixels error after registration'),xlabel('iteration'),ylabel('Mean Square Error');
  hold on, plot(allerr_SL(i, 1:end), '--','color',[0.1 0.5 0.3], 'LineWidth', 2, 'MarkerEdgeColor','k', 'MarkerFaceColor','g', 'MarkerSize',3), title('all pixels error after registration'),xlabel('iteration'),ylabel('Mean Square Error');
  hold on, plot(allerr_AL(i, 1:end), '-*','color',[0.5 0 0.4], 'LineWidth', 2, 'MarkerEdgeColor','k', 'MarkerFaceColor','g', 'MarkerSize',3), title('all pixels error after registration'),xlabel('iteration'),ylabel('Mean Square Error');
  title( [error_measure{1} 'image = ' num2str(image_number) '   lambda = ' num2str(param2(lambda_ind)) '   cert-nstd = ' num2str(param1(cert_ind)) ]);legend('Random', 'SL', 'AL', 'Location', 'SouthEastOutside'),
end
end