   path = 'H:/students/tlotfima/RWRegistration_Final2D/results/final_results/five_trials/';   
   filename = { 'AllErr' 'Displace_field' 'Label_set' 'Landmarks' 'Uncertainty' };
   load (['H:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(1) '_info.mat']);
   for i = 1:5,
        load ([path filename{i} num2str(1) '_set' num2str(1) '.mat']);
   end
    [mx my] = ndgrid(1:size(src3D,1), 1:size(src3D,2));
    for iter = 1:10,
        for img_num = 1:20,
%             for pixel = 1:size(mx(:),1)
                nmx = mx(:) + Label_set.SL(Displace_field.SL(:,iter,img_num),1,img_num);
                nmy = my(:) + Label_set.SL(Displace_field.SL(:,iter,img_num),2,img_num);
        %         nmz = mz(:) + Label_set.SL(Displace_field.SL(:,img_num,iter),3);
                nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
                nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
        %         nmz = reshape(nmz,size(src3D));
                Warped_Vol.SL(:,:,iter,img_num) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');
%             end
        end
    end
    iter1 = 5;
%     Warped_Vol = Calculate_Warped_Vol(src3D(:,:,:), Label_set, Displace_field);
     Diff_Vol.SL = Warped_Vol.SL - repmat(tar3D,[1 1 1 iter1]);
       
%     Diff_Vol = Calc_Similarity(Warped_Vol, tar_all3D(:,:,:,vol_num));
     Edge_Vol.SL = gradient(src3D);
figure,imre(Uncertainty.SL(:,:,:,1),3,10);colormap(gray), hold on, plot(Landmarks.slands_SL3D(2,:),Landmarks.slands_SL3D(1,:),'r*')
figure,imre(Diff_Vol.SL(:,:,:,1),3,10);colormap(gray)