dockf; plot(oobError(B_Less)),
xlabel('number of grown trees')
ylabel('out-of-bag classification error'),
title('Error of Prediction Error');
dockf; bar(B_Less.OOBPermutedVarDeltaError);
xlabel('Feature number');
ylabel('Out-of-bag feature importance');
title('Feature importance results');

dockf; plot(oobError(B)),
xlabel('number of grown trees')
ylabel('out-of-bag classification error'),
title('Error of Prediction Error');
dockf; bar(B.OOBPermutedVarDeltaError);
xlabel('Feature number');
ylabel('Out-of-bag feature importance');
title('Feature importance results');

% Testing the Error, how well the actuall error and the predicted error are correlated 
for img = 21 : 21 %10, %  sz(tar3D,3),    
    tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
    for warp = 1 : 1 %1, % 
        [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta);
        Input_Less = Input(:,1:end-2);
        testInput = Input;
%         testOutput = Output;
        thresh = mnx(Output);
        thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.01;
        thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.5;
        testOutput = Output;
        testOutput(Output<thresh1) = 1;
        testOutput(Output>thresh2) = 3;
        testOutput(Output>thresh1 & Output<thresh2) = 2;            
        testInput_Less = Input_Less;
        Pred_Err_Less = B_Less.predict(testInput_Less);
        Pred_Err = B.predict(testInput); 
        
        for i = 1:sz(Pred_Err),
            Err_Less(i) = double(Pred_Err_Less{i} - '0');
            Err(i) = double(Pred_Err{i} - '0');
        end
        clear Pred_Err_Less, clear Pred_Err
        Pred_Err_Less = Err_Less;
        Pred_Err = Err;
        
        sum(abs(Pred_Err_Less(:) -  testOutput(:)))
        sum(abs(Pred_Err(:) -  testOutput(:)))
%         dockf; plotregression(Pred_Err_Less, testOutput)%, title('Corr. Error/ Predicted Error, No Uncert.'),
%         dockf; plotregression(Pred_Err, testOutput)%, title('Corr. Error/ Predicted Error, With Uncert.'),
    end
end            
