%    elnm = 12;
%    elnm2 = 10;
%     for j = 1:1, 
%         figure, montage(reshape(mat2gray(repmat(tar_Vec, [1 1 elnm2])),[size(tar_Vec,1)  size(tar_Vec,2) 1  elnm2]  )),
%         colormap(jet),  title('fixed Image')
%         figure, montage(reshape(mat2gray(src_Vec(:,:,(j-1)*elnm+1:(j-1)*elnm+elnm2)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm2]  )),
%         colormap(jet), title('Moving Image'),
%         figure, montage(reshape(mat2gray(dsrc_Vec(:,:,(j-1)*elnm+1:(j-1)*elnm+elnm2)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm2]  )),
%         colormap(jet), title('Warped Image'),
%         figure, montage(reshape(mat2gray(repmat(tar_seg_Vec, [1 1 elnm2])),[size(tar_seg_Vec,1)  size(tar_seg_Vec,2) 1  elnm2]  )),
%         colormap(jet),  title('Fixed Image Segment')
%         figure, montage(reshape(mat2gray(repmat(src_seg_Vec, [1 1 elnm2])),[size(src_seg_Vec,1)  size(src_seg_Vec,2) 1  elnm2]  )),
%         colormap(jet),  title('Moving Image Segment')
% 
%         figure, montage(reshape(mat2gray(dsrc_seg_Vec(:,:,(j-1)*elnm+1:(j-1)*elnm+elnm2)),[size(dsrc_seg_Vec,1)  size(dsrc_seg_Vec,2) 1  elnm2]  )),
%         colormap(jet), title('Warped Image Segment'),
%         figure, montage(reshape(mat2gray(Shannon_Vec(:,:,(j-1)*elnm+1:(j-1)*elnm+elnm2)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  elnm2]  )),
%         colormap(jet), title('Shannon Entropy'),
%         figure, montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,(j-1)*elnm+1:(j-1)*elnm+elnm2)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  elnm2]  )),
%         colormap(jet), title('Modified Shannon Entropy'),
% 
%       
%         figure,
%         plot( Err_bef_seg_Vec((j-1)*elnm+1:(j-1)*elnm+elnm2), '-or' ), hold on
%         plot( Err_aft_seg_Vec((j-1)*elnm+1:(j-1)*elnm+elnm2), '-vb' ), hold on
%         title( 'TRE Registration Error' ), xlabel('Modes'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
%         legend('TRE Before', 'TRE After', 'Location', 'SouthEastOutside'),
%         figure,
%         plot( Err_bef_Vec((j-1)*elnm+1:(j-1)*elnm+elnm2), '-*g' ), hold on
%         plot( Err_aft_Vec((j-1)*elnm+1:(j-1)*elnm+elnm2), '-ok' ), hold on
%         title( 'SSD Registration Error' ), xlabel('Modes'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
%         legend('Image SSD Before', 'Image SSD After', 'Location', 'SouthEastOutside'),
%     end
%     
%       
%     ldpath = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\Uncert_all_tumor_noise\uncert_all_tumor\';
%     MCase = 5;
%     
%              file_name = { 'Shannon', 'Shannon_Sprd', 'dsrc_seg', 'src', 'tar', 'dsrc_sprd', 'dsrc', 'src_seg', 'tar_seg', ...
%             'Err_bef_seg', 'Err_aft_seg', 'Err_aft_seg_sprd', 'Err_aft', 'Err_aft_sprd', 'Err_bef' };
%          for Mode = 25:36,
%             for i = 1:15,
%               load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase) '.mat'])); 
%             end
%             Shannon_Vec(:,:,Mode-24) = Shannon;
%             Shannon_Sprd_Vec(:,:,Mode-24) = Shannon_Sprd;
%             dsrc_seg_Vec(:,:,Mode-24) = dsrc_seg;
%             src_seg_Vec = src_seg;
%             tar_seg_Vec = tar_seg;
%             src_Vec(:,:,Mode-24) = src;
%             tar_Vec = tar;
%             dsrc_Vec(:,:,Mode-24) = dsrc;
%             dsrc_sprd_Vec(:,:,Mode-24) = dsrc_sprd;
%             Err_bef_seg_Vec(Mode-24) = Err_bef_seg;
%             Err_bef_Vec(Mode-24) = Err_bef;
%             Err_aft_seg_Vec(Mode-24) = Err_aft_seg;
%             Err_aft_seg_sprd_Vec(Mode-24) = Err_aft_seg_sprd;
%             Err_aft_Vec(Mode-24) = Err_aft;
%             Err_aft_sprd_Vec(Mode-24) = Err_aft_sprd;
%          end
%          save (['../results/real_case_synth_tumor_' num2str(MCase) '.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
%            'dsrc_seg_Vec', 'src_seg_Vec', 'tar_seg_Vec', 'src_Vec', 'tar_Vec', 'dsrc_Vec', ...
%            'Err_bef_seg_Vec', 'Err_bef_Vec', 'Err_aft_seg_Vec', 'Err_aft_Vec');
% 


src_Vec = src_Vec_dif;
tar_Vec = tar_Vec_dif;
%     dsrc_Vec = dsrc_Vec_dif;
Shannon_Vec = Shannon_Vec_dif;
Kde_Vec = Unc_Kde_Vec_dif;
Knn_Vec = Unc_Knn_Vec_dif;
Shannon_Sprd_Vec = Shannon_Sprd_Vec_dif;
dx_Vec = dx_Vec_dif;
dy_Vec = dy_Vec_dif;
dx_sprd_Vec = dx_sprd_Vec_dif;
dy_sprd_Vec = dy_sprd_Vec_dif;
invTx_Vec = invTx_Vec_dif;
invTy_Vec = invTy_Vec_dif;



intx = 50; inty = 50; rad = 25; m = size(src_Vec);
[rr cc] = ndgrid(1:m(1), 1:m(2));
mask1 = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
mask2 = src_Vec(:,:,1)>0.1;
mask =  mask2; %mask1 | mask2; % 
tar_Vec = tar_Vec .* (tar_Vec>0.1); %mask; % mat2gray(tar_Vec) .* mask;
elnm = 5; j =1;
src_Vec = mat2gray(src_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_Vec = mat2gray(dsrc_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_sprd_Vec = mat2gray(dsrc_sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
Shannon_Vec = mat2gray(Shannon_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
Shannon_Sprd_Vec = mat2gray(Shannon_Sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
Kde_Vec = mat2gray(Kde_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
Knn_Vec = mat2gray(Knn_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
figure, montage(reshape(mat2gray(repmat(tar_Vec, [1 1 elnm])),[size(tar_Vec,1)  size(tar_Vec,2) 1  elnm]  )),
colormap(jet),  title('fixed Image'), cbar
figure, montage(reshape(mat2gray(src_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(src_Vec,1)  size(src_Vec,2) 1  elnm]  )),
colormap(jet), title('Moving Image Case 3'), cbar
figure, montage(reshape(mat2gray(dsrc_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
colormap(jet), title('Warped Image'), cbar
figure, montage(reshape(mat2gray(dsrc_sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  elnm]  )),
colormap(jet), title('Modfd Warped Image'), cbar
figure, montage(reshape(mat2gray(Shannon_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  elnm]  )),
colormap(jet), title('Shannon Entropy'), cbar
figure, montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  elnm]  )),
colormap(jet), title('Modified Shannon Entropy'), cbar
figure, montage(reshape(mat2gray(Kde_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Kde_Vec,1)  size(Kde_Vec,2) 1  elnm]  )),
colormap(jet), title('Kde'), cbar
figure, montage(reshape(mat2gray(Knn_Vec(:,:,(j-1)*elnm+1:j*elnm)),[size(Knn_Vec,1)  size(Knn_Vec,2) 1  elnm]  )),
colormap(jet), title('Knn'), cbar

en = 15;
elnm = 4;
src_V(:,:,2) = src_Vec(:,:,1);
src_V(:,:,1) = tar_Vec;
src_V(:,:,3) = src_Vec(:,:,6);
src_V(:,:,4) = src_Vec(:,:,en);
figure, 
subplot 141, imagesc(src_V(:,:,1)),
subplot 142, imagesc(src_V(:,:,2)),
subplot 143, imagesc(src_V(:,:,3)),
subplot 144, imagesc(src_V(:,:,4)),%)montage(reshape(mat2gray(src_V(:,:,(j-1)*elnm+1:j*elnm)),[size(src_V,1)  size(src_V,2) 1  elnm]  )),
colormap(gray), 
%         t = title('Case 2');
%         set(t, 'FontSize', 30); 

for i = 1:en,
    kdesu(i) = sum(sum(Kde_Vec(:,:,i))); 
    kdem(i) = mean(mean(Kde_Vec(:,:,i)));
    kdestd(i) = std(std(Kde_Vec(:,:,i)));
    knnsu(i) = sum(sum(Knn_Vec(:,:,i))); 
    knnm(i) = mean(mean(Knn_Vec(:,:,i)));
    knnstd(i) = std(std(Knn_Vec(:,:,i)));
    shnsu(i) = sum(sum(Shannon_Vec(:,:,i)));
    shnm(i) = mean(mean(Shannon_Vec(:,:,i)));
    shnstd(i) = std(std(Shannon_Vec(:,:,i)));   
    shnsu_sprd(i) = sum(sum(Shannon_Sprd_Vec(:,:,i)));
    shnm_sprd(i) = mean(mean(Shannon_Sprd_Vec(:,:,i)));
    shnstd_sprd(i) = std(std(Shannon_Sprd_Vec(:,:,i)));   
end
shnm = mat2gray(shnm); kdem = mat2gray(kdem);
shnm_sprd = mat2gray(shnm_sprd); knnm = mat2gray(knnm);
    figure, 
%     subplot 121, 
    plot(1:en, kdem, 'r-*','LineWidth',2, 'MarkerSize', 3), 
    set(gca,'FontSize',25)
    hold on, plot(1:en, shnm, 'g-.','LineWidth',2, 'MarkerSize', 3), 
    set(gca,'FontSize',25)
    hold on, plot(1:en, shnm_sprd, 'k--','LineWidth',2, 'MarkerSize', 3),
    set(gca,'FontSize',25)
    hold on, plot(1:en, knnm, 'b-o','LineWidth',2, 'MarkerSize', 3), 
    set(gca,'FontSize',25)
    title('Bspline warping with up to 15% noise'), xlabel('Percent of noise'), ylabel('Uncertainty mean')
    AX = legend('KDE', 'ShEnt', 'WProb', 'KNN', 'Location', 'SouthEast');
    LEG = findobj(AX,'type','text'); 
    set(LEG,'FontSize',25)
    
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path = '../results/res_unc/gamma/';
data_mode = 2; mode = 2;
% data_mode = 3; mode = 3;
filename = {'GMM2_avg_' 'SGauss_' 'Unc_ExpEr_' 'Unc_ExpEr_Map_' 'Unc_Kde_' 'Unc_Knn_' 'Unc_Shannon_' 'Unc_Shannon_Sprd_' 'Wcov_'};
for fname = 1:9,
    load ([path, filename{fname}, num2str(mode), '_gamma1']);
end
 Visualize_Labels_Unc_Test_Edited(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss,GMM2_avg, Unc_Kde, Unc_Knn, Unc_ExpEr, Unc_ExpEr_Map, data_mode);    % Unc_Delta,
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
load ../results/res_unc_add_noise/Uncert_noisedisp.mat;
% load ../results/res_unc_add_noise/Uncert_noiseprob.mat;
elnm = 11;
st = 2; en = 2; stp = 1;
for i = 1:elnm,
    kdem(i) = mean(Kde_Vec(st:stp:en,i));
    erexpm(i) = mean(ExpEr_Vec(st:stp:en,i));
    erexpmapm(i) = mean(ExpEr_Map_Vec(st:stp:en,i));
%     delm(i) = mean(Delta_Vec(st:stp:en,i));
    gmmm(i) = mean(GMM2_avg_Vec(st:stp:en,i));    
    wcovm(i) = mean(Wcov_Vec(st:stp:en,i));    
    sgaum(i) = mean(SGauss_Vec(st:stp:en,i));    
    knnm(i) = mean(Knn_Vec(st:stp:en,i));    
    shnm(i) = mean(Shannon_Vec(st:stp:en,i));    
    %shnm_sprd(i) = mean(Shannon_Sprd_Vec(st:stp:en,i));
end
shnm = mat2gray(shnm); kdem = mat2gray(kdem); erexpm = mat2gray(erexpm );
 %delm= mat2gray(delm); shnm_sprd = mat2gray(shnm_sprd); 
knnm = mat2gray(knnm); erexpmapm = mat2gray(erexpmapm );
sgaum= mat2gray(sgaum); wcovm= mat2gray(wcovm); gmmm= mat2gray(gmmm);

Uncert_arry(1,:) = shnm; %Uncert_arry(2,:) = shnm_sprd;
Uncert_arry(2,:) = kdem; Uncert_arry(3,:) = knnm;
Uncert_arry(4,:) = gmmm; Uncert_arry(5,:) = wcovm;
Uncert_arry(6,:) = sgaum; Uncert_arry(7,:) = erexpm; 
Uncert_arry(8,:) = erexpmapm; %Uncert_arry(8,:) = delm;
figure, 
for Unc_Mode = 1:8,    
    hold on, plot(Uncert_arry(Unc_Mode, :), 'Color', c(Unc_Mode*15,:), 'Marker', shape{Unc_Mode}, 'LineWidth',5, 'MarkerSize', 12), 
    set(gca,'FontSize',35)
end
%title('a)'), 
xlabel('Percent of noise'), ylabel('Uncertainty mean')
AX = legend('ShEnt', 'KDE', 'KNN' , 'GMM', 'Wcov', 'SGauss', 'ExpEr', 'ExpErMap',  'Location', 'SouthEastOutside'); % 'WProb', 'Delta' ,
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',35)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
figure,
plot( sqz(AllErr.Rand), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
plot( sqz(AllErr.RandG), 'Color', c(2*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
plot(sqz(AllErr.AL), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
plot( sqz(AllErr.SL_All), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
plot( sqz(repmat(AllErr.AL_baseline(2),[1 5])), 'Color', c(5*20,:), 'Marker', shape{5}, 'LineWidth',2, 'MarkerSize', 10),
xlim([mn(1)-epsilon mn(2)+epsilon]), 
error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'and 10%% noise']);
title( error_measure, 'FontSize', 25 ), xlabel('Iterations', 'FontSize', 25), ylabel('MSE error', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
set(gca,'FontSize',25)
AX = legend('Random', 'Grid', 'AL', 'SL', 'AL Baseline', 'Location', 'SouthEastOutside');
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',25)

%%
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
figure,
errorbar( sqz(mean(AllErr.SL100)), sqz(std(AllErr.SL100)), 'Color', c(1*30,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
errorbar( sqz(mean(AllErr_Less.SL100)), sqz(std(AllErr_Less.SL100)), 'Color', c(2*30,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), 
xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'and 10%% noise']);
title( error_measure, 'FontSize', 25 ), xlabel('Iterations', 'FontSize', 25), ylabel('MSE error', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
set(gca,'FontSize',25)
AX = legend('SL', 'SL Less', 'Location', 'SouthEastOutside');
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',25)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mn = mnx(dispvec);
[gridX gridY] = ndgrid(mn(1):1:mn(2));
X = [gridX(:), gridY(:)];
        
prob1 = sqz(pbs(1,1:end));             
prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
figure, surf(gridX, gridY, prob1), xlim([mn(1)-.5 mn(2)+.5]), ylim([mn(1)-.5 mn(2)+.5]);
colormap(jet)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
eln1 = 1;
eln2 = 10;
figure, montage(reshape(mat2gray(src_Vec(:,:,eln1:eln2)),[size(src_Vec,1)  size(src_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('moving image'), cbar
figure, montage(reshape(mat2gray(tar_Vec(:,:,eln1:eln2)),[size(tar_Vec,1)  size(tar_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('fixed image'), cbar
figure, montage(reshape(mat2gray(dsrc_Vec(:,:,eln1:eln2)),[size(dsrc_Vec,1)  size(dsrc_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('warped image'), cbar
figure, montage(reshape(mat2gray(dsrc_sprd_Vec(:,:,eln1:eln2)),[size(dsrc_sprd_Vec,1)  size(dsrc_sprd_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('modfd warped image'), cbar
figure, montage(reshape(mat2gray(Shannon_Vec(:,:,eln1:eln2)),[size(Shannon_Vec,1)  size(Shannon_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('Shannon Entropy'), cbar
figure, montage(reshape(mat2gray(Shannon_Sprd_Vec(:,:,eln1:eln2)),[size(Shannon_Sprd_Vec,1)  size(Shannon_Sprd_Vec,2) 1  eln2-eln1+1]  )),
colormap(jet), title('Modified Shannon Entropy'), cbar


ldpath = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\Uncert_all_tumor_noise\uncert_all_tumor\';
  
MCase = 3;
file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar', 'dsrc', 'dsrc_sprd', 'Unc_Kde', 'Unc_Knn' };    
for Mode = 1:10,
  for i = 1:8,
      load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase), '.mat'])); 
  end
  Shannon_Vec(:,:,Mode) = Shannon;
  Shannon_Sprd_Vec(:,:,Mode) = Shannon_Sprd;
  src_Vec(:,:,Mode) = src;
  tar_Vec = tar;
  dsrc_Vec(:,:,Mode) = dsrc;
  dsrc_sprd_Vec(:,:,Mode) = dsrc_sprd;
  Knn_Vec(:,:,Mode) = Unc_Knn;
  Kde_Vec(:,:,Mode) = Unc_Kde;
end
save (['../results/uncert_real', '_Case', num2str(MCase), '_new.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
  'src_Vec', 'tar_Vec', 'dsrc_Vec', 'dsrc_sprd_Vec', 'Knn_Vec', 'Kde_Vec');

S = find(mask);Warp_Err = (sqrt(  (dx(S) - invTx(S)).^2 + (dy(S) - invTy(S)).^2 ));
Warp_Err_Sprd = (sqrt(  (dx_sprd(S) - invTx(S)).^2 + (dy_sprd(S) - invTy(S)).^2 ));
m = find(Warp_Err_Sprd);
[ix iy] = ind2sub(size(I), m);
figure, imagesc(I), hold on, plot(iy,ix,'r*')

Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 ));
Warp_Err_Sprd = (sqrt(  (dx_sprd - invTx).^2 + (dy_sprd - invTy).^2 ));
dockf; 
subplot 121, imagesc(Warp_Err .* mask), title('Warping Error'), cbar, colormap jet;
subplot 122, imagesc(Warp_Err_Sprd .* mask), title('Warping Error Modfd'), cbar, colormap jet;
    
dockf; 
subplot 131, imagesc(mat2gray(Unc_Shannon).*mask), cbar, 
subplot 132, imagesc(mat2gray(Unc_Shannon_Sprd).*mask), cbar
subplot 133, imagesc(Unc_dterm .* mask), cbar
dockf; 
subplot 121, imagesc(abs(Unc_dterm .* mask-mat2gray(Unc_Shannon).*mask)), cbar, 
subplot 122, imagesc(abs(Unc_dterm .* mask-mat2gray(Unc_Shannon_Sprd).*mask)), cbar

dockf; subplot 121, imagesc(tar),subplot 122, imagesc(src),
dockf; subplot 131, imagesc(dsrc), title('Original RW, Uniform Polar'),  
subplot 132, imagesc(dsrc_sprd), title('Mdfd RW')
subplot 133, imagesc(src), title('Src image');

masku = mat2gray(Unc_Shannon_Sprd) .* mask>0.975; %0.99 for real tumour
figure, imagesc(masku),

masku = mat2gray(Unc_Shannon) .* mask>0.975;
figure, imagesc(masku),
%         
%         noise_sigma = 0.01;
%         for Mode = 1:10,
%             src_Vec(:,:,Mode) = imnoise( IWF, 'gaussian', 0, (Mode-1) * noise_sigma );
%         end


%% Evaluation of the new uncertainty methods
  ldpath = 'Y:/students_less/tlotfima/RWRegistration_Final2D/results/Uncert_all_tumor_noise/uncert_all_noise/';
  MCase = 4;
    for img_num = 1:10,
        file_name = { 'Shannon', 'Shannon_Sprd', 'src', 'tar',  'dsrc', 'dsrc_sprd', 'Unc_Knn', 'Unc_Kde', 'Unc_Err_Dist_Map', 'Unc_Err_Dist', 'WCov', 'SGauss', 'time' };    
        for Mode = 1:11,
          for i = 1:13,
%               load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase), '.mat'])); 
              load (num2str([ldpath, file_name{i}, '_', num2str(Mode), '_Case', num2str(MCase), '_Img', num2str(img_num), '.mat'])); 
          end
          Shannon_Vec(:,:,Mode) = Shannon;
          Shannon_Sprd_Vec(:,:,Mode) = Shannon_Sprd;
          src_Vec(:,:,Mode) = src;
          tar_Vec = tar;
          dsrc_Vec(:,:,Mode) = dsrc;
          dsrc_sprd_Vec(:,:,Mode) = dsrc_sprd;
          Knn_Vec(:,:,Mode) = Unc_Knn;
          Kde_Vec(:,:,Mode) = Unc_Kde;
          WCov_Vec(:,:,Mode) = WCov;
          SGauss_Vec(:,:,Mode) = SGauss;          
          ErrDMap_Vec(:,:,Mode) = Unc_Err_Dist_Map;
          ErrD_Vec(:,:,Mode) = Unc_Err_Dist;
          time_Vec(Mode) = time;
        end
        save (['../results/Uncert_all_tumor_noise/uncert_real', '_Case', num2str(MCase), '_Img', num2str(img_num), '.mat'], 'Shannon_Vec', 'Shannon_Sprd_Vec', ...
          'src_Vec', 'tar_Vec', 'dsrc_Vec', 'dsrc_sprd_Vec', 'Knn_Vec', 'Kde_Vec', 'ErrDMap_Vec', 'ErrD_Vec', 'WCov_Vec', 'SGauss_Vec', 'time_Vec');
    end
%% Visualization
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
markersize = 7;
MCase = 4;
% load ../results/res_unc_add_noise/Uncert_noisedisp.mat;
for img_num = 1: 10, % 1, % 
load(['../results/Uncert_all_tumor_noise/uncert_real', '_Case', num2str(MCase), '_Img', num2str(img_num), '.mat']);
en = 11; % 1; % 
% load ../results/res_unc_add_noise/Uncert_noiseprob.mat;
for i = 1:en,
%     kdesu(i) = sum(sum(Kde_Vec(:,:,i))); 
%     kdem(i) = mean(mean(Kde_Vec(:,:,i)));
%     kdestd(i) = std(std(Kde_Vec(:,:,i))); 
%     knnsu(i) = sum(sum(Knn_Vec(:,:,i))); 
%     knnm(i) = mean(mean(Knn_Vec(:,:,i)));
    shnsu(i) = sum(sum(Shannon_Vec(:,:,i)));
    shnm(i) = mean(mean(Shannon_Vec(:,:,i)));
%     shnsu_sprd(i) = sum(sum(Shannon_Sprd_Vec(:,:,i)));
%     shnm_sprd(i) = mean(mean(Shannon_Sprd_Vec(:,:,i)));
    wcovsu(i) = sum(sum(WCov_Vec(:,:,i)));
    wcovm(i) = mean(mean(WCov_Vec(:,:,i)));
    sgssu(i) = sum(sum(SGauss_Vec(:,:,i)));
    sgsm(i) = mean(mean(SGauss_Vec(:,:,i)));
    errdsu(i) = sum(sum(ErrD_Vec(:,:,i))); 
    errdm(i) = mean(mean(ErrD_Vec(:,:,i)));
    errdmapsu(i) = sum(sum(ErrDMap_Vec(:,:,i))); 
    errdmapm(i) = mean(mean(ErrDMap_Vec(:,:,i)));
end
ShV(img_num, :) = mat2gray(shnm); %KdeV(img_num, :) = mat2gray(kdem);
%ShSpV(img_num, :) = mat2gray(shnm_sprd); KnnV(img_num, :) = mat2gray(knnm);
ErrDV(img_num, :) = mat2gray(errdm); ErrDMapV(img_num, :) = mat2gray(errdmapm);
WCovV(img_num, :) = mat2gray(wcovm); SGSV(img_num, :) = mat2gray(sgsm);
end
    figure, 
% %     subplot 121, 
%     errorbar(0:en-1, mean(sqz(KdeV)), std(sqz(KdeV)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', markersize), 
%     set(gca,'FontSize',25), xlim([-0.2 10.2]), 
%     hold on, errorbar(0:en-1, mean(sqz(ShSpV)), std(sqz(ShSpV)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', markersize),
%     set(gca,'FontSize',25), xlim([-0.2 10.2]), 
%     hold on, errorbar(0:en-1, mean(sqz(KnnV)), std(sqz(KnnV)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', markersize), 
%     set(gca,'FontSize',25), xlim([-0.2 10.2]), 
    errorbar(0:en-1, mean(sqz(WCovV)), std(sqz(WCovV)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',4, 'MarkerSize', markersize), 
    set(gca,'FontSize',35), xlim([-0.2 10.2]),
    hold on, errorbar(0:en-1, mean(sqz(SGSV)), std(sqz(SGSV)), 'Color', c(2*20,:), 'Marker', shape{1}, 'LineWidth',4, 'MarkerSize', markersize), 
    set(gca,'FontSize',35), xlim([-0.2 10.2]),
    hold on, errorbar(0:en-1, mean(sqz(ShV)), std(sqz(ShV)), 'Color', c(3*20,:), 'Marker', shape{2}, 'LineWidth',4, 'MarkerSize', markersize), 
    set(gca,'FontSize',35), xlim([-0.2 10.2]), 
    hold on, errorbar(0:en-1, mean(sqz(ErrDV)), std(sqz(ErrDV)), 'Color', c(4*20,:), 'Marker', shape{5}, 'LineWidth',4, 'MarkerSize', markersize), 
    set(gca,'FontSize',35), xlim([-0.2 10.2]), 
    hold on, errorbar(0:en-1, mean(sqz(ErrDMapV)), std(sqz(ErrDMapV)), 'Color', c(5*20,:), 'Marker', shape{6}, 'LineWidth',4, 'MarkerSize', markersize), 
    set(gca,'FontSize',35), xlim([-0.2 10.2]),  
    title('Bspline warping with up to 10% noise'), xlabel('Percent of noise'), ylabel('Uncertainty mean')
    AX = legend('WCov', 'SGauss', 'ShEnt', 'ErExp', 'ErExpMAP', 'Location', 'SouthEastOutside'); % 'KDE', 'WProb', 'KNN', 
    LEG = findobj(AX,'type','text'); 
    set(LEG,'FontSize',35)
    grid on,
 %%    
    src_Vec2 = src_Vec;
    src_Vec2(:,:,2) = [];
    src_Vec2(:,:,3) = [];
    src_Vec2(:,:,4:7) = [];
    figure, montage(reshape(mat2gray(src_Vec2), [size(src_Vec2, 1)  size(src_Vec2, 2) 1  4]  )),
    colormap(gray), title('Moving Image (0-10%) Noise', 'FontSize',25), 
    
    noise_sigma = .005;
    tar_Vec2(:,:,1) = tar_Vec;
    tar_Vec2(:,:,2) = src_Vec(:,:,1); %resc(imnoise(tar_Vec, 'gaussian', 0, (3-1) * noise_sigma ));
    tar_Vec2(:,:,3) = src_Vec(:,:,2); %resc(imnoise(tar_Vec, 'gaussian', 0, (6-1) * noise_sigma ));
    tar_Vec2(:,:,4) = src_Vec(:,:,5);%resc(imnoise(tar_Vec, 'gaussian', 0, (11-1) * noise_sigma ));
    tar_Vec2(:,:,5) = src_Vec(:,:,7);%resc(imnoise(tar_Vec, 'gaussian', 0, (11-1) * noise_sigma ));
    tar_Vec2 = mat2gray(tar_Vec2);

    figure,
    subplot 151, imagesc(tar_Vec2(:,:,1)), colormap(gray), 
%     set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 152, imagesc(tar_Vec2(:,:,2)), colormap(gray), 
%     set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 153, imagesc(tar_Vec2(:,:,3)), colormap(gray), 
%     set(gca,'FontSize',25), 
	title('Moving Image (0-10%) Noise', 'FontSize',35),   
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 154, imagesc(tar_Vec2(:,:,4)), colormap(gray), 
%     set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot 155, imagesc(tar_Vec2(:,:,5)), colormap(gray), 
%     set(gca,'FontSize',25), 
    set(gca,'YTick',[]); set(gca,'XTick',[]);


    
    
    
    %% Real detecting Tumor with uncertainty map
add_paths()
[ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
%     noise_sigma = 0.005; 
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; %MIND
file_name = 'src'; slice = 1;
load (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/real/' file_name '_img' num2str(slice) '.mat'])); 
TxW = 20*ones(size(src)); TyW = TxW;
maxD = 1 + round(max(max(TxW(:), TyW(:)))); 
resolution = 2*maxD / round(sqrt(res));
[Ux Uy]=ndgrid(-maxD:resolution:maxD);
dispvec=[Ux(1:end)' Uy(1:end)']; 
%%
for slice = [1 5 8], %[1 5 6 8 12],% :13,
    file_name = { 'Shannon', 'src', 'tar', 'dsrc', 'Unc_Err_Dist_Map', 'WCov'};    %'Unc_Kde', 'Unc_Knn' , 'time', 'Unc_Err_Dist', 'pbs' 
    for i = 1:6,
        load (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/real/' file_name{i} '_img' num2str(slice) '.mat'])); 
    end
%     isz = sz(src); mask = ones(isz); data_mode = 2; epsilon_ = 1e-3;
%     [WCov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
%     save (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/WCov_img' num2str(slice) '.mat']), 'Wcov'); 
%     save (num2str(['../results/Uncert_all_tumor_noise/uncert_tumor/SGauss_img' num2str(slice) '.mat']), 'SGauss'); 

    figure, 
    subplot 131, imagesc(tar), title('Fixed Image', 'FontSize', 40), set(gca,'FontSize',40), axis equal, colormap(gray), axis tight,
    subplot 132, imagesc(src), title('Moving Image', 'FontSize', 40), set(gca,'FontSize',40), axis equal, colormap(gray), axis tight,
    subplot 133, imagesc(dsrc), title('Registered Image', 'FontSize', 40), set(gca,'FontSize',40), axis equal, colormap(gray), axis tight,
    figure,
    subplot 131, imagesc(mat2gray(Shannon)), title('ShEnt', 'FontSize', 40), set(gca,'FontSize',30), axis equal, colormap(gray), axis tight, %xlim([0 150]), ylim([0 180]),
    subplot 132, imagesc(mat2gray(WCov)), title('WCov', 'FontSize', 40), set(gca,'FontSize',30), axis equal, colormap(gray), axis tight, %xlim([0 150]), ylim([0 180]), % axis equal
    subplot 133, imagesc(mat2gray(Unc_Err_Dist_Map)), title('Error Dist Map', 'FontSize', 40), set(gca,'FontSize',30), axis equal, colormap(gray), axis tight, %xlim([0 150]), ylim([0 180]), % axis equal
end



%%
path = '../results/Uncert_all_tumor_noise/uncert_tumor/';%real/';
file_name = { 'Shannon', 'WCov', 'src', 'tar', 'dsrc', 'Unc_ExpErMap', 'Unc_ExpEr'};    %'Unc_Kde', 'Unc_Knn' , 'time' 
imgs = 4; cols = 6;
slice = 2;
for i = 1:7,
    load ([path, file_name{i}, '_synth_img', num2str(slice), '.mat']); % _real_img
end
figure(1), 
subplot(imgs,cols,1), imagesc(tar), title('Fxd Img', 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight,
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,2), imagesc(src), title('Mvg Img', 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight,
 set(gca,'YTick',[]); set(gca,'XTick',[]);
% subplot(imgs,3,3), imagesc(dsrc), title('Reg Img', 'FontSize', 35), set(gca,'FontSize',25), colormap(gray), axis equal, axis tight
%  set(gca,'YTick',[]); set(gca,'XTick',[]);
% figure(2), 
subplot(imgs,cols,3), imagesc(Shannon), title('ShEnt', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25), 
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,4), imagesc(WCov), title('WCov', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,5), imagesc(Unc_ExpEr), title('ExpEr', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
subplot(imgs,cols,6), imagesc(Unc_ExpErMap), title('ExpErMap', 'FontSize', 35), colormap(gray), axis equal, axis tight,%cbar, set(gca,'FontSize',25),
 set(gca,'YTick',[]); set(gca,'XTick',[]);
cnt = 1;
for slice = [6, 8, 13], %[5, 6, 8],
    for i = 1:7,
        load ([path, file_name{i}, '_synth_img', num2str(slice), '.mat']); % _real_img
    end
    figure(1),
    subplot(imgs,cols,cnt*cols+1), imagesc(tar), colormap(gray), axis equal, axis tight,%title('Fixed Image', 'FontSize', 25), set(gca,'FontSize',25),
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot(imgs,cols,cnt*cols+2), imagesc(src), colormap(gray), axis equal, axis tight,%title('Moving Image', 'FontSize', 25), set(gca,'FontSize',25),
     set(gca,'YTick',[]); set(gca,'XTick',[]);
%     subplot(imgs,cols,cnt*cols+3), imagesc(dsrc), axis equal, axis tight,%title('Registered Image', 'FontSize', 25), set(gca,'FontSize',25), colormap(gray)
%      set(gca,'YTick',[]); set(gca,'XTick',[]);
%     figure(2), 
     subplot(imgs,cols,cnt*cols+3), imagesc(Shannon), colormap(gray), axis equal, axis tight,%title('ShEnt', 'FontSize', 25), %cbar, set(gca,'FontSize',25), 
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot(imgs,cols,cnt*cols+4), imagesc(WCov), colormap(gray), axis equal, axis tight,%title('WCov', 'FontSize', 25), %cbar, set(gca,'FontSize',25),
     set(gca,'YTick',[]); set(gca,'XTick',[]);
    subplot(imgs,cols,cnt*cols+5), imagesc(Unc_ExpEr), colormap(gray), axis equal, axis tight,%title('EDistMap', 'FontSize', 25), %cbar, set(gca,'FontSize',25),
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     subplot(imgs,cols,cnt*cols+6), imagesc(Unc_ExpErMap), colormap(gray), axis equal, axis tight,%title('EDistMap', 'FontSize', 25), %cbar, set(gca,'FontSize',25),
     set(gca,'YTick',[]); set(gca,'XTick',[]);
     cnt = cnt + 1;
end