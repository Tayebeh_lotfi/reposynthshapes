function Show_Labels(dterm, nlabs, pad_size, show_non_unique_flag, dispvec, threshold)
%% Visualizing all displacements for a specific pixel

pix_Vec = [];
if(show_non_unique_flag == 0)
    if ~exist('f1', 'var');
        f1=dockf;
        figure(f1),
    else
        f2=dockf;
        figure(f2),
    end
    pixel_x = 112; pixel_y = 123;
    stem(1:nlabs,squeeze(dterm(pixel_x,pixel_y,:)));
end
if(show_non_unique_flag == 1)
    for i = pad_size:size(dterm,1)-pad_size
        for j = pad_size:size(dterm,2)-pad_size
            count = 0;
            %for k = 1:nlabs
%                 if(dterm(i,j,k) ~= 1)
%                     count = count + 1;
%                 end
%            end
            %count = length(find(dterm(i, j,:) ~= 1));
            count = length(unique(dterm(i, j, :)));
            %if count > 70 
            if count > 10
                if(find (dterm(i, j, :) < 0.6))
                   pix = [i;j];
                   pix_Vec = [pix_Vec, pix];%figure,stem(1:nlabs,squeeze(dterm(i,j,:)))
                end
            end
        end
    end
% [r c v] = find(dterm ~= 1);
% pix_Vec = [r';c'];
% if(size(pix_Vec,1) > 70)
    m = size(pix_Vec,2);
    p = ceil(rand(1) * m);
    pixel_x = pix_Vec(1,p); pixel_y = pix_Vec(2,p);

    if ~exist('f1', 'var');
        f1=dockf;
        figure(f1),
    else
        f2=dockf;
        figure(f2),
    end
    %pix_Vec = [];
    stem(1:nlabs,squeeze(dterm(pixel_x,pixel_y,:))), 
    title(['Cost of all the labels for pixel : [ ',num2str(pixel_x),' , ',num2str(pixel_y), ']']);
    
    n = find(dterm(pixel_x, pixel_y, :) < threshold);

    if ~exist('f1', 'var');
        f1=dockf;
        figure(f1),
    else
        f2=dockf;
        figure(f2),
    end
    quiver(zeros(size(n,1),1),zeros(size(n,1),1),dispvec(n,1),dispvec(n,2)),
    title(['Labels with cost less than  ',num2str(threshold), ' for pixel : [ ',num2str(pixel_x),' , ',num2str(pixel_y), ']']);
end