% % Testing the model, how well it is trained, error of training
% dockf; plot(oobError(B_Less)),
% xlabel('number of grown trees', 'FontSize', 35)
% ylabel('out-of-bag classification error', 'FontSize', 35),
% title('Error of Prediction Error', 'FontSize', 35);
% dockf; bar(B_Less.OOBPermutedVarDeltaError);
% xlabel('Feature number', 'FontSize', 35);
% ylabel('Out-of-bag feature importance', 'FontSize', 35);
% title('Feature importance results', 'FontSize', 35);
% 
% dockf; plot(oobError(B)),
% xlabel('number of grown trees', 'FontSize', 35)
% ylabel('out-of-bag classification error', 'FontSize', 35),
% title('Error of Prediction Error', 'FontSize', 35);
% dockf; bar(B.OOBPermutedVarDeltaError);
% xlabel('Feature number', 'FontSize', 35);
% ylabel('Out-of-bag feature importance', 'FontSize', 35);
% title('Feature importance results', 'FontSize', 35);

[ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters();
trial_num = 1;
hsize = [3 3]; sigma = 0.05; mean_ = 0; vari_ = 0.05;
Lbl_mod = 1; % 1:Mesh, 2: 4 displacements, 3: Kmeans GT, 4: VQ, 
%              5: Non-uniform Polar, 6: Uniform Polar, 7: sampling GT
simil_measure = 5; %MIND

load ('../data/native_slices_ax2_41');
sze = [256 256];
spacing.height = 20;
spacing.width = 20;
% Testing the Error, how well the actuall error and the predicted error are correlated 
for img = 22 : 26   
    tar = imrotate(imresize(mat2gray(Im(:,:,img)), [sze(1) sze(2)]), 90);
    for warp = 1 : 1 %1, % 
        [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta, noise_sigma);
        Input_Less = Input(:,1:end-18);
        testInput = Input;
        testOutput = Output;            
        testInput_Less = Input_Less;
      
%         thresh = mnx(Output);
%         thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.1; %.05
%         thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.5; % 65
%         Outputrn = Output;
%         Outputrn(Output<thresh1) = 1;
%         Outputrn(Output>thresh2) = 3;
%         Outputrn(Output>thresh1 & Output<thresh2) = 2;
% %% Randomizing the order of pixels  
%         ind1 = find(Outputrn == 1);
%         ind2 = find(Outputrn == 2);
%         ind3 = find(Outputrn == 3);
%         p1 = randperm(size(ind1));
%         p2 = randperm(size(ind2));
%         p3 = randperm(size(ind3));
%         Inputrn1 = Input(ind1(p1(1:max_size*1/5)),:); Outputrn1 = Output(ind1(p1(1:max_size*1/5)),:);
%         Inputrn1( max_size*1/5 + 1 : max_size*4/5, :) = Input(ind2(p2(1:max_size*3/5)),:); 
%         Outputrn1( max_size*1/5 + 1 : max_size*4/5, :) = Output(ind2(p2(1:max_size*3/5)),:);
%         Inputrn1( max_size*4/5 + 1 : max_size*5/5, :) = Input(ind3(p3(1:max_size*1/5)),:); 
%         Outputrn1( max_size*4/5 + 1 : max_size*5/5, :) = Output(ind3(p3(1:max_size*1/5)),:);
%         Inputrn1_Less = Inputrn1(:,1:end-18);
        
        Pred_Err_Less = B_Less.predict(testInput_Less);
        Pred_Err = B.predict(testInput); 
        
        
        c = jet(120);
        shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
        thresh_Less = mnx(Pred_Err_Less); thresh = mnx(Pred_Err);
        thresh1_Less = thresh_Less(1)+ (thresh_Less(2)-thresh_Less(1))*.1; thresh1 = thresh(1)+ (thresh(2)-thresh(1))*.1; %.05
        thresh2_Less = thresh_Less(1)+ (thresh_Less(2)-thresh_Less(1))*.5; thresh2 = thresh(1)+ (thresh(2)-thresh(1))*.5; % 65    
        
        ind1_Less = find(Pred_Err_Less < thresh1_Less); ind1 = find(Pred_Err < thresh1);
        Out1_Less = Pred_Err_Less(ind1_Less); Out1 = Pred_Err(ind1);
        [a_Less b_Less] = sort(Out1_Less); [a b] = sort(Out1);
        range = 1:20;
        figure, plot(range, Out1_Less(b_Less(range)), 'r-.', 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
        plot(range, Out1(b(range)), 'b-.', 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
        title( 'Choosing Most Certain Voxels as Seeds' , 'FontSize', 35), 
        xlabel('Number of Selected Seeds', 'FontSize', 35), ylabel('Registraiton Error', 'FontSize', 35),

        legend('Img/Wrp', 'Img/Wrp+Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
        set(gca, 'FontSize', 35);
        grid on
%% Randomizing the order of pixels  
        
        
        
        
%         dockf; plotregression(Pred_Err_Less, Outputrn1)%, title('Corr. Error/ Predicted Error, No Uncert.'),
%         dockf; plotregression(Pred_Err, Outputrn1)%, title('Corr. Error/ Predicted Error, With Uncert.'),
    end
end            
