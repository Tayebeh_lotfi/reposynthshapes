function Show_All_Labels(tlands, slands, debug, dispvec, pad_size, nlabs, dterm)%, show_non_unique_flag)
if debug == 1
    %figure, quiver(zeros(size(dispvec,1),1),zeros(size(dispvec,1),1),dispvec(:,1),dispvec(:,2),'Color',round(rand(1,3)))
    if ~exist('f1', 'var');
        f1=dockf;
        figure(f1),
    else
        f2=dockf;
        figure(f2),
    end
    %figure(f1),
    dockf;
    for i = 1:1:size(dispvec,1)
        hold on, quiver(0,0,dispvec(i,1),dispvec(i,2),0,'Color',round(rand(1,3)), 'LineWidth',2),
        title('All labels');
    end
    
    hold on, quiver(zeros(size(tlands-slands, 2),1), zeros(size(tlands-slands, 2),1), (tlands(1,:)-slands(1,:))', (tlands(2,:)-slands(2,:))','LineWidth',5)
    
    
     figure, imagesc(src), colormap(gray), title('warped image');
%      slands = [1 1; 1 2; 1 3; 2 1; 2 2; 2 3; 1 256; 2 256; 3 256; 1 255; 2 255; 3 255; 256 1; 256 2; 256 3; 255 1; 255 2; 255 3; 256 256; 255 256; 255 255; 254 255; 255 254]';
     slands = [1 1; 1 256; 256 1; 256 256]';
     tlands = [10 15; 8 245; 250 15; 251 253]';
     hold on, quiver(ones(size(tlands-slands, 2),1)*100, ones(size(tlands-slands, 2),1)*100, (tlands(1,:)-slands(1,:))', (tlands(2,:)-slands(2,:))','LineWidth',2, 'Color', 'r')

     figure, imagesc(uncertainty), colormap(gray), hold on, plot(slands(2,:), slands(1,:), 'r*');
end

% % i,j
% % %pause;
% % drawnow;
% % end
% % end
    hold on, quiver(ones(size(tlands-slands, 2),1)*100, ones(size(tlands-slands, 2),1)*100, ((tlands(1,:)-slands(1,:))*3)', ((tlands(2,:)-slands(2,:))*3)','LineWidth',2, 'Color', 'r')
