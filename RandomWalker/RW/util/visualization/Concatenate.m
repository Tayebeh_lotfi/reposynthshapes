function [src3D tar3D] = Concatenate(strt_frst, end_frst, path1, path2, file_name, trial_num, img_set_sz, str)

%     clc, clear all, close all,       
%     load(num2str([path1 'trial' num2str(trial_num) '_large_warp_corner.mat']));
%     load(num2str([path1 'trial' num2str(trial_num) str '_corner.mat']));
    load(num2str([path1 'trial' num2str(trial_num) str '_corner.mat']));
%     load(num2str([path1 'trial' num2str(trial_num) '_simwarp' str '_corner.mat'])); 

    for set_num = 1:8,
        for i = 1:6,
          load (num2str([path2 file_name{i} num2str(set_num) str '_corner_set' num2str(trial_num) '_region.mat']));
        end   
    %     AllErr_All = AllErr; Time_All = Time; Landmarks_All = Landmarks;
    %     Uncertainty_All = Uncertainty; 
    %     Displace_field_All = Displace_field;
    %     Label_set_All = Label_set;

    %     for i = 1:6,
    %       load (num2str([path2 file_name{i} num2str(trial_num) '_large_corner_set2.mat'])); 
    %     end
        strt_arr = (set_num-1)*img_set_sz + 1;
        end_arr = set_num*img_set_sz;
        AllErr_All.Rand(strt_arr:end_arr,:) = AllErr.Rand(strt_frst:end_frst,:); AllErr_All.AL(strt_arr:end_arr,:) = AllErr.AL(strt_frst:end_frst,:);
        AllErr_All.SL_All(strt_arr:end_arr,:) = AllErr.SL_All(strt_frst:end_frst,:); AllErr_All.SL100(strt_arr:end_arr,:) = AllErr.SL100(strt_frst:end_frst,:); 
        AllErr_All.SL_Rand(strt_arr:end_arr,:) = AllErr.SL_Rand(strt_frst:end_frst,:); AllErr_All.RandG(strt_arr:end_arr,:) = AllErr.RandG(strt_frst:end_frst,:);
        AllErr_All.AL_baseline(strt_arr:end_arr,:) = AllErr.AL_baseline(strt_frst:end_frst,:);

        Displace_field_All.Rand(:,:,strt_arr:end_arr) = Displace_field.Rand(:,:,strt_frst:end_frst); 
        Displace_field_All.RandG(:,:,strt_arr:end_arr) = Displace_field.RandG(:,:,strt_frst:end_frst); 
        Displace_field_All.SL_All(:,:,strt_arr:end_arr) = Displace_field.SL_All(:,:,strt_frst:end_frst);
        Displace_field_All.SL100(:,:,strt_arr:end_arr) = Displace_field.SL100(:,:,strt_frst:end_frst);
        Displace_field_All.SL_Rand(:,:,strt_arr:end_arr) = Displace_field.SL_Rand(:,:,strt_frst:end_frst);
        Displace_field_All.AL(:,:,strt_arr:end_arr) = Displace_field.AL(:,:,strt_frst:end_frst); 
        Displace_field_All.AL_Base(:,:,strt_arr:end_arr) = Displace_field.AL_Base(:,:,strt_frst:end_frst);

        Uncertainty_All.Rand(:,:,:,strt_arr:end_arr) = Uncertainty.Rand(:,:,:,strt_frst:end_frst); 
        Uncertainty_All.RandG(:,:,:,strt_arr:end_arr) = Uncertainty.RandG(:,:,:,strt_frst:end_frst); 
        Uncertainty_All.AL(:,:,:,strt_arr:end_arr) = Uncertainty.AL(:,:,:,strt_frst:end_frst);
        Uncertainty_All.SL_All(:,:,:,strt_arr:end_arr) = Uncertainty.SL_All(:,:,:,strt_frst:end_frst); 
        Uncertainty_All.SL100(:,:,:,strt_arr:end_arr) = Uncertainty.SL100(:,:,:,strt_frst:end_frst); 
        Uncertainty_All.SL_Rand(:,:,:,strt_arr:end_arr) = Uncertainty.SL_Rand(:,:,:,strt_frst:end_frst); 
        Uncertainty_All.AL_Base(:,:,:,strt_arr:end_arr) = Uncertainty.AL_Base(:,:,:,strt_frst:end_frst);

        Label_set_All.Rand(:,:,strt_arr:end_arr) = Label_set.Rand(:,:,strt_frst:end_frst); 
        Label_set_All.RandG(:,:,strt_arr:end_arr) = Label_set.RandG(:,:,strt_frst:end_frst);     
        Label_set_All.SL_All(:,:,strt_arr:end_arr) = Label_set.SL_All(:,:,strt_frst:end_frst);
        Label_set_All.SL100(:,:,strt_arr:end_arr) = Label_set.SL100(:,:,strt_frst:end_frst);
        Label_set_All.SL_Rand(:,:,strt_arr:end_arr) = Label_set.SL_Rand(:,:,strt_frst:end_frst);
        Label_set_All.AL(:,:,strt_arr:end_arr) = Label_set.AL(:,:,strt_frst:end_frst); 
        Label_set_All.AL_Base(:,:,strt_arr:end_arr) = Label_set.AL_Base(:,:,strt_frst:end_frst);

        Time_All.Rand(strt_arr:end_arr) = Time.Rand(strt_frst:end_frst); 
        Time_All.RandG(strt_arr:end_arr) = Time.RandG(strt_frst:end_frst); 
        Time_All.AL(strt_arr:end_arr) = Time.AL(strt_frst:end_frst);
        Time_All.SL_All(strt_arr:end_arr) = Time.SL_All(strt_frst:end_frst); 
        Time_All.SL100(strt_arr:end_arr) = Time.SL100(strt_frst:end_frst); 
        Time_All.SL_Rand(strt_arr:end_arr) = Time.SL_Rand(strt_frst:end_frst); 
        Time_All.Base(strt_arr:end_arr) = Time.Base(strt_frst:end_frst);

        Landmarks_All.slands_Rand(:,:,strt_arr:end_arr) = Landmarks.slands_Rand(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_Rand(:,:,strt_arr:end_arr) = Landmarks.tlands_Rand(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_RandG(:,:,strt_arr:end_arr) = Landmarks.slands_RandG(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_RandG(:,:,strt_arr:end_arr) = Landmarks.tlands_RandG(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_AL(:,:,strt_arr:end_arr) = Landmarks.slands_AL(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_AL(:,:,strt_arr:end_arr) = Landmarks.tlands_AL(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_SL_All(:,:,strt_arr:end_arr) = Landmarks.slands_SL_All(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_SL_All(:,:,strt_arr:end_arr) = Landmarks.tlands_SL_All(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_SL100(:,:,strt_arr:end_arr) = Landmarks.slands_SL100(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_SL100(:,:,strt_arr:end_arr) = Landmarks.tlands_SL100(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_SL_Rand(:,:,strt_arr:end_arr) = Landmarks.slands_SL_Rand(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_SL_Rand(:,:,strt_arr:end_arr) = Landmarks.tlands_SL_Rand(:,:,strt_frst:end_frst); 
        Landmarks_All.slands_AL_Base(:,:,strt_arr:end_arr) = Landmarks.slands_AL_Base(:,:,strt_frst:end_frst); 
        Landmarks_All.tlands_AL_Base(:,:,strt_arr:end_arr) = Landmarks.tlands_AL_Base(:,:,strt_frst:end_frst); 
    end
    clear AllErr; clear Time; clear Landmarks;
    clear Label_set; clear Displace_field; clear Uncertainty;
    
    AllErr = AllErr_All; Uncertainty = Uncertainty_All; Time = Time_All;
    Landmarks = Landmarks_All; Displace_field = Displace_field_All; 
    Label_set = Label_set_All;

    clear AllErr_All; clear Time_All; clear Landmarks_All;
    clear Label_set_All; clear Displace_field_All; clear Uncertsinty_All;

    for i = 1:6,
        save (num2str([path2 file_name{i} num2str(trial_num) str '_corner_all.mat']), file_name{i}); 
    end
         
end