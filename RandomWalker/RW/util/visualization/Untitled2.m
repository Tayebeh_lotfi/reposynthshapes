%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Showing Relation Between Error and Uncertainty %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for MCase = 1:2,
    if MCase == 1,
%% Choosing One of the Warpings Bspline
        load '../results/uncert_warp_all/uncert_bspline.mat';
        src_Vec = src_Vec_bsp;
        tar_Vec = tar_Vec_bsp;
        Shannon_Vec = Shannon_Vec_bsp;
        Kde_Vec = Unc_Kde_Vec_bsp;
        Knn_Vec = Unc_Knn_Vec_bsp;
        Shannon_Sprd_Vec = Shannon_Sprd_Vec_bsp;
        dx_Vec = dx_Vec_bsp;
        dy_Vec = dy_Vec_bsp;
        dx_sprd_Vec = dx_sprd_Vec_bsp;
        dy_sprd_Vec = dy_sprd_Vec_bsp;
        invTx_Vec = invTx_Vec_bsp;
        invTy_Vec = invTy_Vec_bsp;
        Warp_Err_Vec = ( (dx_Vec - invTx_Vec).^2 + (dx_Vec - invTx_Vec).^2 );
        Warp_Err_Sprd_Vec = ( (dx_Vec - invTx_Vec).^2 + (dx_Vec - invTx_Vec).^2 );  
    else
%%  Choosing One of the Warpings Diffusion  
        load '../results/uncert_warp_all/uncert_diffusion.mat';
        src_Vec = src_Vec_dif;
        tar_Vec = tar_Vec_dif;
        Shannon_Vec = Shannon_Vec_dif;
        Kde_Vec = Unc_Kde_Vec_dif;
        Knn_Vec = Unc_Knn_Vec_dif;
        Shannon_Sprd_Vec = Shannon_Sprd_Vec_dif;
        dx_Vec = dx_Vec_dif;
        dy_Vec = dy_Vec_dif;
        dx_sprd_Vec = dx_sprd_Vec_dif;
        dy_sprd_Vec = dy_sprd_Vec_dif;
        invTx_Vec = invTx_Vec_dif;
        invTy_Vec = invTy_Vec_dif;
        Warp_Err_Vec = ( (dx_Vec - invTx_Vec).^2 + (dx_Vec - invTx_Vec).^2 );
        Warp_Err_Sprd_Vec = ( (dx_Vec - invTx_Vec).^2 + (dx_Vec - invTx_Vec).^2 );
    end
 %%    
    intx = 50; inty = 50; rad = 25; m = size(src_Vec);
    [rr cc] = ndgrid(1:m(1), 1:m(2));
    mask1 = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
    mask2 = src_Vec(:,:,1)>0.1;
    mask =  mask2; %mask1 | mask2; % 
    tar_Vec = tar_Vec .* (tar_Vec>0.1); %mask; % mat2gray(tar_Vec) .* mask;
    elnm = 5; j =1;
    src_Vec = mat2gray(src_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_Vec = mat2gray(dsrc_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
%     dsrc_sprd_Vec = mat2gray(dsrc_sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
    Shannon_Vec = mat2gray(Shannon_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
    Shannon_Sprd_Vec = mat2gray(Shannon_Sprd_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
    Kde_Vec = mat2gray(Kde_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);
    Knn_Vec = mat2gray(Knn_Vec(:,:,1:elnm)) .* repmat(mask, [1, 1, elnm]);    
    
    for i = 1:elnm,
        mask(:,:,i) = src_Vec(:,:,i)>0.1;
        Stat_Err_Unc(Shannon_Vec(:,:,i), Shannon_Sprd_Vec(:,:,i), Kde_Vec(:,:,i), Knn_Vec(:,:,i), Warp_Err_Vec(:,:,i), Warp_Err_Sprd_Vec(:,:,i), mask(:,:,i), 2, i, MCase);
    end    
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Showing Relation Between Error and Uncertainty %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Add Noise to DispVec and Prob %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
figure, quiver(zeros(sz(dispvec(1:10:end,1),1),1), zeros(sz(dispvec(1:10:end,1),1),1), ...
    dispvec(1:10:end,2), dispvec(1:10:end,1), 'Color', 'k', 'LineWidth',2)
set(gca,'YTick',[]); set(gca,'XTick',[]);


 figure; 
 subplot 131, imagesc(flowToColor(invTx, invTy )), cbar, colormap jet; set(gca,'YTick',[]); set(gca,'XTick',[]);
 subplot 132, imagesc(flowToColor(dx, dy )), cbar, colormap jet; set(gca,'YTick',[]); set(gca,'XTick',[]);
 subplot 133, imagesc(flowToColor(invTx-dx, invTy-dy )), cbar, colormap jet; set(gca,'YTick',[]); set(gca,'XTick',[]);



c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
num_sample = 8;
for MCase = 1:2,
    if MCase == 1,
        load ../results/res_unc_add_noise/Uncert_noisedisp.mat;
    else
        load ../results/res_unc_add_noise/Uncert_noiseprob.mat;
    end
    elnm = 11;
    for st = [8 1], %[8 16 24 32 40 48 56 1],
%     st = 57; en = 2; stp = 1;
        for i = 1:elnm,
            kdem(i) = (Kde_Vec(st,i));
            delm(i) = (Delta_Vec(st,i));
            gmmm(i) = (GMM2_avg_Vec(st,i));    
            wcovm(i) = (Wcov_Vec(st,i));    
            sgaum(i) = (SGauss_Vec(st,i));    
            knnm(i) = (Knn_Vec(st,i));    
            shnm(i) = (Shannon_Vec(st,i));    
            shnm_sprd(i) = (Shannon_Sprd_Vec(st,i));
        end
        shnm = shnm; kdem = kdem; delm= delm;
        shnm_sprd = shnm_sprd; knnm = knnm; 
        sgaum= sgaum; wcovm= wcovm; gmmm= gmmm;

        Uncert_arry(1,:) = shnm; Uncert_arry(2,:) = shnm_sprd;
        Uncert_arry(3,:) = kdem; Uncert_arry(4,:) = delm;
        Uncert_arry(5,:) = gmmm; Uncert_arry(6,:) = wcovm;
        Uncert_arry(7,:) = sgaum; Uncert_arry(8,:) = knnm;
        Unc_str = {'ShEnt', 'WProb', 'KDE', 'KNN' , 'GMM', 'Wcov', 'SGauss', 'Delta'}; 
        for Unc_Mode = 1:8,    
            dockf; plot(0:10, Uncert_arry(Unc_Mode, :), 'Color', c(Unc_Mode*10,:), 'Marker', shape{Unc_Mode}, 'LineWidth',2, 'MarkerSize', 10), 
            set(gca,'FontSize',15)
%         end
        if st ~= 1,
            pix_case = (floor((st-2) / num_sample) + 1);
        else
            pix_case = 8;
        end
        if MCase == 1,
            title(['Disp, ', num2str(pix_case), ', ' Unc_str{Unc_Mode}]), %Noise on case ', Uncertainty'
        else
            title(['Prob, ', num2str(pix_case), ', ' Unc_str{Unc_Mode}]), %Probabilities   Displacements
        end
        xlabel('Percent of noise'), ylabel('Uncertainty mean')
        end
%         AX = legend('ShEnt', 'WProb', 'KDE', 'KNN' , 'GMM', 'Wcov', 'SGauss', 'Delta' , 'Location', 'SouthEastOutside');
%         LEG = findobj(AX,'type','text'); 
%         set(LEG,'FontSize',25)
    end
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Add Noise to DispVec and Prob %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Error Over Iteration for Different Methods %%%%%%%%%%%%%%%%%%%%%%%%% 
% AL_SL
savepath = '../results/';
file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Time' 'Landmarks'};   
set_num = 1; Noise_Level = 2; trial_num = 1; str = '_large';
MCase = 2;
% for MCase = 1:2,
    for i = 1:1,
        if(MCase == 1)
            load (num2str([savepath 'AL_SL/' file_name{i} num2str(set_num) str '_noise' num2str(Noise_Level-1) '_trial' num2str(trial_num) '_bsp' '.mat'])); 
        else
            load (num2str([savepath 'AL_SL/' file_name{i} num2str(set_num) str '_noise' num2str(Noise_Level-1) '_trial' num2str(trial_num) '_diff' '.mat'])); 
        end
    end
    c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
    dockf;
    errorbar( mean(sqz(AllErr.Rand)), std(sqz(AllErr.Rand)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), 
    xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
    errorbar( mean(sqz(AllErr.RandG)), std(sqz(AllErr.RandG)), 'Color', c(2*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), 
    xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
    errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), 
    xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
    errorbar( mean(sqz(AllErr.SL_All)), std(sqz(AllErr.SL_All)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), 
    xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
    plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 5]))), 'Color', c(5*20,:), 'Marker', shape{5}, 'LineWidth',2, 'MarkerSize', 10),
    xlim([mn(1)-epsilon mn(2)+epsilon]),
    if(MCase == 1)
        error_measure = sprintf(['Field Warping MSE Error, 6%% bspline warping,' '\n' 'and 10%% noise']);
    else
        error_measure = sprintf(['Field Warping MSE Error, 6%% diffusion warping,' '\n' 'and 5%% noise']);
    end
    title( error_measure, 'FontSize', 25 ), xlabel('Iterations', 'FontSize', 25), ylabel('MSE error', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
    set(gca,'FontSize',25)
    AX = legend('Random', 'Grid', 'AL', 'SL', 'AL Baseline', 'Location', 'SouthEastOutside');
    LEG = findobj(AX,'type','text'); 
    set(LEG,'FontSize',25)
% end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Error Over Iteration for Different Methods %%%%%%%%%%%%%%%%%%%%%%%%%   
%% %%%%%%%%%%%%% Evaluation of Uncertainty on Different Smoothness Terms, Synthetic Data %%%%%%%%%%%%%%%%%%
add_paths()
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^' '*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^' '*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
gamma = 0:0.1:1.5;
gamma(17:20) = 2:1:5;
gamma(21:24) = [10 100 1000 10000];
ln = len(gamma);
for counter = 1:ln,    
%     root = '/home/ghassan/students_less/tlotfima';
%     savepath = [root '/RWRegistration_Final2D/results/'];
    real_mode = 0;
%     switch(Mode)
%         case 1
           nlabs_size = 5; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
           [pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
           nsample = size(pbs,1); data_mode = 2;
           nlabs = size(dispvec,1);
           isz = [nsample 1]; mask = ones(nsample,1);  
           G = 1 ./ (dist2(dispvec, dispvec) .^ gamma(counter) + 1);
           G = G ./ repmat(sum(G,2), 1, nlabs);
           pbs_sprd = pbs * G;
%     end
    
    [Unc_Shannon Unc_Shannon_Sprd(:, counter)] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, real_mode);
end
% a = Unc_Shannon_Sprd(1, :);
Unc = Unc_Shannon_Sprd(9:8:57, :);
Unc(8,:) = Unc_Shannon_Sprd(1, :);

Unc_Sh = Unc_Shannon(9:8:57, :);
Unc_Sh(8,:) = Unc_Shannon(1, :);

% dockf;
figure,
for counter = 1:ln,
    hold on, plot(1:8, (Unc(:, counter)), 'Color', c(counter*5,:), 'Marker', shape{counter}, 'LineWidth',2, 'MarkerSize', 10);
end
counter = counter + 1;
hold on, plot(1:8, Unc_Sh, 'Color', c(counter*4,:), 'Marker', shape{counter}, 'LineWidth',2, 'MarkerSize', 10);
title( 'Effect of Probability Smoothness on Uncertainty Synthetic', 'FontSize', 25 ), xlabel('Cases', 'FontSize', 25), ylabel('Uncertainty', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
set(gca,'FontSize',25)
% leg_str = [];
% for counter = 1:ln,    
%     leg_str = sprintf([leg_str, ['Gamma = ', num2str(gamma(counter)) '\n']]);
% end
AX = legend('Gamma = ', num2str(gamma(1)), 'Gamma =  ', num2str(gamma(2)), 'Gamma =  ', num2str(gamma(3)), 'Gamma =  ', num2str(gamma(4)), 'Gamma =  ', num2str(gamma(5)), ...
            'Gamma =  ', num2str(gamma(6)), 'Gamma =  ', num2str(gamma(7)), 'Gamma =  ', num2str(gamma(8)), 'Gamma =  ', num2str(gamma(9)), 'Gamma =  ', num2str(gamma(10)), ...
            'Gamma = ', num2str(gamma(11)), 'Gamma = ', num2str(gamma(12)), 'Gamma =  ', num2str(gamma(13)), 'Gamma =  ', num2str(gamma(14)), 'Gamma =  ', num2str(gamma(15)), ...
            'Gamma = ', num2str(gamma(16)), 'Gamma = ', num2str(gamma(17)), 'Gamma =  ', num2str(gamma(18)), 'Gamma =  ', num2str(gamma(19)), 'Gamma =  ', num2str(gamma(20)), ...
            'Gamma = ', num2str(gamma(21)), 'Gamma = ', num2str(gamma(22)), 'Gamma =  ', num2str(gamma(23)), 'Gamma =  ', num2str(gamma(24)), 'Shannon', 'Location', 'SouthEastOutside');
% AX = legend(num2str(leg_str), 'Shannon', 'Location', 'SouthEast');
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',25)

%% %%%%%%%%%%%%%%% Evaluation of Uncertainty on Different Smoothness Terms, Synthetic Data %%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%% Evaluation of Uncertainty on Different Smoothness Terms, Real Data %%%%%%%%%%%%%%%%%%
add_paths()
[debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters(); 
noise_sigma = 0.005; lbl_md = 1;
load ../data/Bspline.mat;
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^' '*' 'o' 'v' 'p'};
% gamma = 0.5:0.1:1.4;
gamma = 0.5:0.1:1.5;
gamma(12:15) = 2:1:5;
ln = len(gamma);
for counter = 1:ln,   
    for Mode = 1 :10,
        IWF = imnoise( IWF, 'gaussian', 0, (Mode-1) * noise_sigma );
        %TxW = 10*ones(size(I)); TyW = TxW; invTxW = TxW; invTyW = TyW;
        distx = 1; disty = 1;
        m = size(I);
        tlands(:,1) = [distx; disty]; 
        tlands(:,2) = [distx; m(2)-disty]; 
        tlands(:,3) = [m(1)-distx; disty]; 
        tlands(:,4) = [m(1)-distx; m(2)-disty];
        slands = tlands;
        src = mat2gray(IWF); tar = mat2gray(I); Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
        num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
        isz = size(src);
        [points edges]=lattice( isz(1), isz(2), 0);
        nseeds = size(tlands,2);
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, lbl_md);
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma(counter), dterm_mode, slands);
        [Shannon Shannon_Sprd(:,:,Mode, counter)] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, 1); 
    end
end
Unc = sqz(sum(sum(Shannon_Sprd,1),2));
dockf;
for counter = 1:ln,
    hold on, plot(1:10, mat2gray(Unc(:,counter)), 'Color', c(counter*7,:), 'Marker', shape{counter}, 'LineWidth',2, 'MarkerSize', 10);
end
title( 'Effect of Probability Smoothness on Uncertainty Real', 'FontSize', 25 ), xlabel('Noise', 'FontSize', 25), ylabel('Uncertainty', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
set(gca,'FontSize',25)
% leg_str = [];
% for counter = 1:ln,    
%     leg_str = [leg_str, ['Gamma = ', num2str(gamma(counter))]];
% end
AX = legend('Gamma = ', num2str(gamma(1)), 'Gamma =  ', num2str(gamma(2)), 'Gamma =  ', num2str(gamma(3)), 'Gamma =  ', num2str(gamma(4)), 'Gamma =  ', num2str(gamma(5)), ...
            'Gamma =  ', num2str(gamma(6)), 'Gamma =  ', num2str(gamma(7)), 'Gamma =  ', num2str(gamma(8)), 'Gamma =  ', num2str(gamma(9)), 'Gamma =  ', num2str(gamma(10)), ...
            'Gamma = ', num2str(gamma(11)), 'Gamma = ', num2str(gamma(12)), 'Gamma =  ', num2str(gamma(13)), 'Gamma =  ', num2str(gamma(14)), 'Gamma =  ', num2str(gamma(15)), 'Location', 'SouthEastOutside');
% AX = legend(leg_str, 'Location', 'SouthEastOutside');
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',25)
%% %%%%%%%%%%%%%%% Evaluation of Uncertainty on Different Smoothness Terms, Real Data %%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%% Evaluation of Delta Uncertainty on Different Weights, Synthetic Data %%%%%%%%%%%%%%%%%%
add_paths()
c = jet(120);
shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^' '*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^' '*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
gamma = 1;
real_mode = 0;
%%
nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
[pbs dispvec] = Creating_Spatial_Test_Unc(nlabs_size, Visualize_Labels, sample_num);
nsample = size(pbs,1); data_mode = 2;
nlabs = size(dispvec,1);
isz = [nsample 1]; mask = ones(nsample,1);  
G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
G = G ./ repmat(sum(G,2), 1, nlabs);
pbs_sprd = pbs * G;
%%
nlabs_size = 11; Visualize_Labels = 0; sample_num = 8; % number of epsilon changes
[pbs dispvec] = Creating_Spatial_Test_Unc3D(nlabs_size, Visualize_Labels, sample_num);
nsample = size(pbs,1); data_mode = 3; nlabs = size(dispvec,1);
isz = [nsample 1]; mask = ones(nsample,1);
G = 1 ./ (dist2(dispvec, dispvec) .^ gamma + 1);
G = G ./ repmat(sum(G,2), 1, nlabs);
pbs_sprd = pbs * G;
            %%
alpha = 3.3:.1:3.3;
beta = 2.2:.1:2.2; %1.8:.1:1.8;
ln_a = len(alpha);
ln_b = len(beta);
for counter_a = 1:ln_a,    
    for counter_b = 1:ln_b,   
        w = [alpha(counter_a) beta(counter_b)];
        Unc_Delta(:, counter_a, counter_b) = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w);
    end
end
%% 2D
Unc = Unc_Delta(9:8:57, :, :);
Unc(8,:,:) = Unc_Delta(1, :, :);
%% 3D
Unc = Unc_Delta(9:8:89, :, :);
Unc(12,:,:) = Unc_Delta(1, :, :);
%%
% dockf;
figure,
for counter_a = 1:ln_a,    
    for counter_b = 1:ln_b,   
    hold on, plot(1:8, (Unc(:, counter_a, counter_b)), 'Color', c(counter_a*17,:), 'Marker', shape{counter_a+counter_b}, 'LineWidth',2, 'MarkerSize', 10);
%     hold on, plot(1:12, (Unc(:, counter_a, counter_b)), 'Color', c(counter_a*17,:), 'Marker', shape{counter_a+counter_b}, 'LineWidth',2, 'MarkerSize', 10);
    end
end
title( 'Effect of Powers on Delta Uncertainty, Synthetic Data', 'FontSize', 25 ), xlabel('Cases', 'FontSize', 25), ylabel('Uncertainty', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
set(gca,'FontSize',25)
% AX = legend( ['Alpha = ', num2str(alpha(1)), ' Beta = ',  num2str(beta(1))], ['Alpha = ', num2str(alpha(1)), ' Beta = ',  num2str(beta(2))], ...
%     ['Alpha = ', num2str(alpha(1)), ' Beta = ',  num2str(beta(3))], ['Alpha = ', num2str(alpha(1)), ' Beta = ',  num2str(beta(4))], ...
%     ['Alpha = ', num2str(alpha(2)), ' Beta = ',  num2str(beta(1)) ], ['Alpha = ', num2str(alpha(2)), ' Beta = ',  num2str(beta(2))], ...
%     ['Alpha = ', num2str(alpha(2)), ' Beta = ',  num2str(beta(3))], ['Alpha = ', num2str(alpha(2)), ' Beta = ',  num2str(beta(4))], ...
%     ['Alpha = ', num2str(alpha(3)), ' Beta = ',  num2str(beta(1)) ], ['Alpha = ', num2str(alpha(3)), ' Beta = ',  num2str(beta(2))], ...
%     ['Alpha = ', num2str(alpha(3)), ' Beta = ',  num2str(beta(3))], ['Alpha = ', num2str(alpha(3)), ' Beta = ',  num2str(beta(4))], ...
%     ['Alpha = ', num2str(alpha(4)), ' Beta = ',  num2str(beta(1)) ], ['Alpha = ', num2str(alpha(4)), ' Beta = ',  num2str(beta(2))], ...
%     ['Alpha = ', num2str(alpha(4)), ' Beta = ',  num2str(beta(3))], ['Alpha = ', num2str(alpha(4)), ' Beta = ',  num2str(beta(4))], 'Location', 'SouthEastOutside');
% leg_str = [];
% for counter_a = 1:ln_a,    
%     for counter_b = 1:ln_b, 
%         leg_str = [leg_str, ['Alpha = ', num2str(alpha(counter_a)), ' Beta = ', num2str(counter_b)]];
%     end
% end

AX = legend( ['Alpha = ', num2str(alpha(1)), ' Beta = ',  num2str(beta(1))], 'Location', 'SouthEastOutside');
LEG = findobj(AX,'type','text'); 
set(LEG,'FontSize',25)
%% %%%%%%%%%%%%%%% Evaluation of Delta Uncertainty on Different Weights, Synthetic Data %%%%%%%%%%%%%%%%%%

nlabs_size = 3;
nlabs = nlabs_size * nlabs_size;
grid_size = floor(nlabs_size/2);
[grid_x grid_y] = ndgrid(-grid_size:grid_size,-grid_size:grid_size);
dispvec(1:nlabs,:) = [grid_x(:) grid_y(:)];
mn = mnx(dispvec);
epsil = 0.2;
figure, 
plot(dispvec(9,2), dispvec(9,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
hold on, plot(dispvec(3,2), dispvec(3,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
hold on, plot(dispvec(5,2), dispvec(5,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
hold on, plot(dispvec(7,2), dispvec(7,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
%     hold on, plot(dispvec(6,2), dispvec(6,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
hold on, plot(dispvec(1,2), dispvec(1,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
xlim([mn(1)-epsil mn(2)+epsil]), ylim([mn(1)-epsil mn(2)+epsil]);
plot(dispvec(:,2), dispvec(:,1), 'ro', 'MarkerFaceColor','r', 'MarkerSize', 40), 
 xlim([mn(1)-epsil mn(2)+epsil]), ylim([mn(1)-epsil mn(2)+epsil]);
    
figure, subplot 121, imagesc(abs(dsrc_sprd-tar)), 
title(['M Shannon Error: ', num2str(sqrt(sum( (dsrc_sprd(:)-tar(:)).^2 ) ))])
subplot 122, imagesc(abs(dsrc-tar)),
title(['Shannon Error: ', num2str(sqrt(sum( (dsrc(:)-tar(:)).^2 ) ))])


Unc_str = {'Shannon' 'WCov' 'GDist' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian' ...
        'GMM Avg Gaussian' 'GMM Discrete Gaussian' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian', ...
        'GMM Avg Gaussian' 'GMM Discrete Gaussian' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian', ...
            'GMM Avg Gaussian' 'GMM Discrete Gaussian' 'KDE' 'KNN' 'Shannon Modified'};
     
       Visualize_Labels_Unc_Test(pbs, dispvec, Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, ...
        GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, ...
        GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, nlabs_size, sample_num, Unc_str, data_mode);

    
%%
Unc_str = {'Shannon' 'Shannon Modified' 'WCov' 'GDist' 'GMM' 'KDE' 'KNN' 'ExpEr' 'ExpErMAP'};
data_mode = 3;     
Visualize_Labels_Unc_Test_Edited(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss,GMM2_avg, Unc_Kde, Unc_Knn, ...
    Unc_ExpEr, Unc_ExpEr_Map, data_mode)
    
    
%%
for data_mode = 2:3,
    Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, GMM2_avg, Unc_Kde, Unc_Knn, Unc_Delta, data_mode);
end

%%    
dockf; imagesc(src), title('moving img');
dockf; imagesc(tar), title('fixed img');
dockf; imagesc(dsrc), title('warped img');
dockf; imagesc(dsrc_sprd), title('mod_warped img');
dockf; imagesc(Shannon), title('Shannon img');
dockf; imagesc(Shannon_Sprd), title('Shannon Spread img');
dockf; imagesc(Unc_Kde), title('KDE Entropy'); 
dockf; imagesc(Unc_Knn), title('Knn Entropy');

intx = 20; inty = 20; rad = 20; m = size(src_Vec);
[rr cc] = ndgrid(1:m(1), 1:m(2));
C = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
figure, imagesc(C)

for i = 1:10,
    figure,  subplot 231, imagesc(src_Vec(:,:,i)), 
    subplot 232, imagesc(Shannon_Vec(:,:,i)), cbar, 
    title('Shannon')
    subplot 233, imagesc(Shannon_Sprd_Vec(:,:,i)), cbar, 
    title('Modfd Shannon')
    subplot 234, imagesc(Knn_Vec(:,:,i)), cbar, 
    title('Knn')
    subplot 235, imagesc(Kde_Vec(:,:,i)), cbar, 
    title('Kde')
end

figure, for i = 1:10, subplot 121, imagesc(J_final_test(:,:,i)), subplot 122, imagesc(P_final_test(:,:,i)), pause(), end

intx = 20; inty = 20; rad = 15; m = size(src_Vec);
[rr cc] = ndgrid(1:m(1), 1:m(2));
mask = sqrt((rr-intx).^2+(cc-inty).^2)<=rad;
mask = src_Vec(:,:,1)>0.1;
Shannon_Sprd_Vec = mat2gray(Shannon_Sprd_Vec) .* repmat(mask, [1, 1, elnm]);
Shannon_Vec = mat2gray(Shannon_Vec) .* repmat(mask, [1, 1, elnm]);
Kde_Vec = mat2gray(Kde_Vec) .* repmat(mask, [1, 1, elnm]);
Knn_Vec = mat2gray(Knn_Vec) .* repmat(mask, [1, 1, elnm]);
figure,
for i = 1:10,
    Unc = Shannon_Sprd_Vec(:,:,i);
    mm = quantile(Unc(find(mask>0)),.45);
    subplot 321, imagesc(src_Vec(:,:,i)), 
    subplot 322, imagesc(Unc > mm), title(' Modfd Shannon')
    Unc = Shannon_Vec(:,:,i);
    mm = quantile(Unc(find(mask>0)),.45);
    subplot 323, imagesc(Unc > mm), title('Shannon')
    Unc = Kde_Vec(:,:,i);
    mm = quantile(Unc(find(mask>0)),.45);
    subplot 324, imagesc(Unc > mm), title('Kde')
    Unc = Knn_Vec(:,:,i);
    mm = quantile(Unc(find(mask>0)),.45);
    subplot 325, imagesc(Unc > mm), title('Knn')
    pause()
end

%% Weited Covarience Method 
clear disp_probs
disp_probs(1, :) = [1 0 0 0 0 0 0 0 0]; disp_probs(2, :) = [1 1 0 0 0 0 0 0 0]/2;
disp_probs(3, :) = [1 0 1 0 0 0 0 0 0]/2; disp_probs(4, :) = [1 0 0 0 0 0 0 0 1]/2;
disp_probs(5, :) = [1 0 0 0 1 0 0 0 1]/3; disp_probs(6, :) = [1 0 1 0 0 0 1 0 1]/4;
disp_probs(7, :) = [1 0 1 0 1 0 1 0 1]/5; disp_probs(8, :) = ones(9,1)/9;
[dy dx] = ndgrid(- 1:1, -1:1); nsample = 8;
dispvec = [dx(:) dy(:)]; isz = [nsample 1]; mask = ones(nsample,1); epsilon = 1e-4;
data_mode = 2;
[Wcov SGauss] = Calc_Uncertainty_WCov(disp_probs, dispvec, isz, mask, epsilon_, data_mode);
%%