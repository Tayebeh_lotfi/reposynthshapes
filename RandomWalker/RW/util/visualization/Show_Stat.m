function  Show_Stat( mean_diff, mean_Uncert, mean_Jacob, Corr_Mat, AllErr )


    figure,
    errorbar( mean(sqz(AllErr.Rand)), std(sqz(AllErr.Rand)), '-or' ), hold on
    errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), '-*g' ), hold on
    errorbar( mean(sqz(AllErr.SL100)), std(sqz(AllErr.SL100)), '-.vm' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    errorbar( mean(sqz(AllErr.SL_Rand)), std(sqz(AllErr.SL_Rand)), '-.vc' ), hold on
    plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'SL100', 'SL\_Rand', 'AL Baseline', 'Location', 'SouthEastOutside'),

    figure,
    errorbar( mean(sqz(mean_diff.Rand)), std(sqz(mean_diff.Rand)), '-or' ), hold on
    errorbar( mean(sqz(mean_diff.AL)), std(sqz(mean_diff.AL)), '-*g' ), hold on
    errorbar( mean(sqz(mean_diff.SL100)), std(sqz(mean_diff.SL100)), '-.vm' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    plot( mean(sqz(repmat(mean_diff.AL_Base(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
    error_measure = sprintf(['Mean of Intensity Difference over iteration']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'SL100', 'AL Baseline', 'Location', 'SouthEastOutside'),
    
    
    figure,
    errorbar( mean(sqz(mean_Uncert.Rand)), std(sqz(mean_Uncert.Rand)), '-or' ), hold on
    errorbar( mean(sqz(mean_Uncert.AL)), std(sqz(mean_Uncert.AL)), '-*g' ), hold on
    errorbar( mean(sqz(mean_Uncert.SL100)), std(sqz(mean_Uncert.SL100)), '-.vm' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    plot( mean(sqz(repmat(mean_Uncert.AL_Base(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
    error_measure = sprintf(['Mean of Uncertainty over iteration']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'SL100', 'AL Baseline', 'Location', 'SouthEastOutside'),
    

    figure,
    errorbar( mean(sqz(mean_Jacob.Rand)), std(sqz(mean_Jacob.Rand)), '-or' ), hold on
    errorbar( mean(sqz(mean_Jacob.AL)), std(sqz(mean_Jacob.AL)), '-*g' ), hold on
    errorbar( mean(sqz(mean_Jacob.SL100)), std(sqz(mean_Jacob.SL100)), '-.vm' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    plot( mean(sqz(repmat(mean_Jacob.AL_Base(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
    error_measure = sprintf(['Mean of Jacobian over iteration']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'SL100', 'AL Baseline', 'Location', 'SouthEastOutside'),   
    
    figure,
    errorbar( mean(sqz(Corr_Mat.Rand(1,2,:,:))), std(sqz(Corr_Mat.Rand(1,2,:,:))), '-or' ), hold on
    errorbar( mean(sqz(Corr_Mat.AL(1,2,:,:))), std(sqz(Corr_Mat.AL(1,2,:,:))), '-*g' ), hold on
    errorbar( mean(sqz(Corr_Mat.SL100(1,2,:,:))), std(sqz(Corr_Mat.SL100(1,2,:,:))), '-.vm' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    plot( mean(sqz(repmat(Corr_Mat.AL_Base(1,2,:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
    error_measure = sprintf(['Mean of Correlation of Registration Error and Uncertainty over iteration']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'SL100', 'AL Baseline', 'Location', 'SouthEastOutside'),        
    
%     axis image
%     dockf;
   
    figure,
    errorbar( mean(sqz(AllErr.SL_All)), std(sqz(AllErr.SL_All)), '-og' ), hold on
    errorbar( mean(sqz(AllErr_Less.SL_All)), std(sqz(AllErr_Less.SL_All)), '-.vm' ), hold on
%     plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' ), hold on
%     plot( mean(sqz(repmat(AllErr_Less.AL_baseline(:,2),[1 11]))), '--g' )
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('SL w-uncertainty', 'SL wo- uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    
    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), '-og' , 'LineWidth', 5 ), hold on
    errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), '-.vm' , 'LineWidth', 5 ), hold on
%     plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' ), hold on
%     plot( mean(sqz(repmat(AllErr_Less.AL_baseline(:,2),[1 11]))), '--g' )
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'AL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), '-og' , 'LineWidth', 5 ), hold on
    errorbar( mean(sqz(AllErr.SLGT200)), std(sqz(AllErr.SLGT200)), '-.vm' , 'LineWidth', 5 ), hold on
%     plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' ), hold on
%     plot( mean(sqz(repmat(AllErr_Less.AL_baseline(:,2),[1 11]))), '--g' )
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'SL+GT', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    
    c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
    figure,
    plot( AllErr.SL200, 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), set(gca,  'FontSize', 35), hold on
    plot( AllErr.SLGT200, 'Color', c(2*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10 ), set(gca,  'FontSize', 35), hold on
    plot( AllErr.AL4, 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10 ), set(gca,  'FontSize', 35), hold on
    plot( AllErr.AL10, 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), set(gca,  'FontSize', 35), hold on

%     plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' ), hold on
%     plot( mean(sqz(repmat(AllErr_Less.AL_baseline(:,2),[1 11]))), '--g' )
    error_measure = sprintf(['MSE , 6%% warping,' '\n' '5%% noise']);
    title( error_measure, 'FontSize', 35 ), xlabel('Iterations', 'FontSize', 35 ), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'SL+GT', 'AL4', 'AL10', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    set(gca,  'FontSize', 35);
    
    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    errorbar( mean(sqz(AllErr.SLGT200)), std(sqz(AllErr.SLGT200)), 'Color', c(2*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10 ), hold on
    errorbar( mean(sqz(AllErr.AL4)), std(sqz(AllErr.AL4)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10 ), hold on
    errorbar( mean(sqz(AllErr.AL10)), std(sqz(AllErr.AL10)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10 ), hold on
%     plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' ), hold on
%     plot( mean(sqz(repmat(AllErr_Less.AL_baseline(:,2),[1 11]))), '--g' )
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'SL+GT', 'AL4', 'AL10', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     errorbar( mean(sqz(AllErr.AL10)), std(sqz(AllErr.AL10)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10 ), hold on   
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    
    
   %% 2D 3D Results 
     c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
   
    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Less.SL200)), std(sqz(AllErr_Less.SL200)), 'Color', c(5*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 6%% warping,' '\n' '5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL Img/Wrp + Unc', 'SL Img/Wrp', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

   
    
     figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Img.SL200)), std(sqz(AllErr_Img.SL200)), 'Color', c(3*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Unc.SL200)), std(sqz(AllErr_Unc.SL200)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 6%% warping,' '\n' '5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
%     legend('SL Img', 'SL Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL Img/Wrp + Unc', 'SL Img/Wrp', 'SL Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    grid on
    
    Path2 = 'Y:\students_less\tlotfima\RWRegistration_Final3D\results\AL_SL\Regression\Bspline\';
    load ([Path2 'AllErr1_8.mat']), load ([Path2 'AllErr_All1_8.mat']), load ([Path2 'AllErr_Less1_8.mat'])
    figure,
    errorbar( mean(sqz(AllErr_All.SL3D)), std(sqz(AllErr_All.SL3D)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Less.SL3D)), std(sqz(AllErr_Less.SL3D)), 'Color', c(3*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr.SL3D)), std(sqz(AllErr.SL3D)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
%     errorbar( mean(sqz(AllErr_Less.SL3D)), std(sqz(AllErr_Less.SL3D)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     set(gca, 'FontSize', 35);
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 5%% noise']);
    title( error_measure, 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
    legend('SL Img/Wrp + Unc', 'SL Img/Wrp', 'SL Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    grid on
    
    
    
    
    
    
    
    figure,
    errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr_Less.SL200)), std(sqz(AllErr_Less.SL200)), 'Color', c(3*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr.Rand1)), std(sqz(AllErr.Rand2)), 'Color', c(5*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    error_measure = sprintf(['Field Warping MSE Error, MRI brain images with 6%% warping,' '\n' '5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('MSE error', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
%     legend('SL Img', 'SL Unc', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('SL Img/Wrp + Unc', 'SL Img/Wrp', 'Rand Sel', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    grid on
    
    
    
    
    
    
    
Path1 = '../results/AL_SL/Regression/ExpEr/old/all_features_old/';
counter = 1;
for i = 1:10,
    for j = 1:10,
        if(i~=j)
            load ([Path1 'AllErr' num2str(i) '_to_img' num2str(j) '_regress_ExpErU_noise5.mat']);
            for k = 1:5,
            Dicesl(counter, k) = mean(AllErr.SL200(k).dice(1:22));
            Dicesl_1(counter, 1:22, k) = AllErr.SL200(k).dice(1:22);
            Diceal(counter, k) = mean(AllErr.AL10(k).dice(1:22));
            Diceal_1(counter, 1:22, k) = AllErr.AL10(k).dice(1:22);
            end
            counter = counter + 1;
        end
    end
end

  c = jet(120); fs = 45;
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
    figure,
    errorbar( mean(sqz(Dicesl)), std(sqz(Dicesl)), 'Color', c(5*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', fs);
%     errorbar( mean(sqz(Diceal)), std(sqz(Diceal)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     set(gca, 'FontSize', 35);
    error_measure = sprintf(['Dice Measure over 90 Pairs with 5%% noise']);
    title( error_measure , 'FontSize', fs), xlabel('Iterations', 'FontSize', fs), ylabel('Dice', 'FontSize', fs), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
%     legend('SL', 'AL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

grid on, axis tight
    
    range = 1:5;
    for i = 1:22,
        [p,rsq] = polyfit(1:5, mean(sqz(Dicesl_1(:,i,:)),1), 1);
        angle(i) = p(1);
    end
%         out_fit = polyval(p, range, rsq);
%         figure,
%         plot(range, out_fit, 'Marker', shape{1}, 'Color', 'k', 'LineWidth', 3, 'MarkerSize', 12), 
%         hold on, plot(range, mean(sqz(Dicesl_1(:,3,:)),1),'bo');
%         title(['Corr. Coef. = ', num2str(gcc(1,2)) ' With Unc'], 'FontSize', 35);
%         set(gca, 'FontSize', 35);  
    
    
   figure; hMulti = bar(angle,0.4,'b','EdgeColor',[1 0.5 0.5]); 
    set(hMulti,'LineWidth', 2, 'LineStyle',':'); set(gca, 'FontSize', 35);
    title('Dice Increase Rate for Different Regions, 2D Imgs', 'FontSize', 35);
        xlabel('Regions', 'FontSize', 35), ylabel('Dice Increase Rate', 'FontSize', 35),
grid on,
    

%% 3D


 clear
 
    
Path1 = 'Y:/students_less/tlotfima/RWRegistration_Final3D/results/AL_SL/Regression/old/another/';
counter = 1;
for i = [1:3], % 4
    for j = 8:10,
        if(i~=j)
            load ([Path1 'AllErr_vol' num2str(i) '_to_vol' num2str(j) '_regress_noise5.mat']);
            for k = 1:5,
            Dicesl(counter, k) = mean(AllErr.SL3D(k).dice(1:57));
            Dicesl_1(counter, 1:57, k) = AllErr.SL3D(k).dice(1:57);
            end
            counter = counter + 1;
        end
    end
end

  c = jet(120);
  fs=45;
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
    figure,
    errorbar( mean(sqz(Dicesl)), std(sqz(Dicesl)), 'Color', c(5*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', fs);
%     errorbar( mean(sqz(Diceal)), std(sqz(Diceal)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     set(gca, 'FontSize', 35);
    error_measure = sprintf(['Dice Measure over 45 Pairs with 5%% noise']);
    title( error_measure , 'FontSize', fs), xlabel('Iterations', 'FontSize', fs), ylabel('Dice', 'FontSize', fs), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
%     legend('SL', 'AL', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
axis tight
grid on,
    
    range = 1:5;
    for i = 1:57,
        [p,rsq] = polyfit(1:5, mean(sqz(Dicesl_1(:,i,:)),1), 1);
        angle(i) = p(1);
    end
%         out_fit = polyval(p, range, rsq);
%         figure,
%         plot(range, out_fit, 'Marker', shape{1}, 'Color', 'k', 'LineWidth', 3, 'MarkerSize', 12), 
%         hold on, plot(range, mean(sqz(Dicesl_1(:,3,:)),1),'bo');
%         title(['Corr. Coef. = ', num2str(gcc(1,2)) ' With Unc'], 'FontSize', 35);
%         set(gca, 'FontSize', 35);  
    
    
   figure; hMulti = bar(angle,0.4,'b','EdgeColor',[1 0.5 0.5]); 
    set(hMulti,'LineWidth', 2, 'LineStyle',':'); set(gca, 'FontSize', 35);
    title('Dice Increase Rate for Different Regions, 3D Imgs', 'FontSize', 35);
        xlabel('Regions', 'FontSize', 35), ylabel('Dice Increase Rate', 'FontSize', 35),
grid on,








        
   %%     
   stop = 6; stop = 11, stop = 16, stop = 21, stop = 26, stop = 31, stop = 36, stop = 41,    
   AllErr1.Rand1(stop:stop+set_size,:) =  AllErr.Rand1; AllErr1.Rand3(stop:stop+set_size,:) =  AllErr.Rand3;
   AllErr1.Rand5(stop:stop+set_size,:) =  AllErr.Rand5; AllErr1.Grid1(stop:stop+set_size,:) =  AllErr.Grid1;
   AllErr1.Grid3(stop:stop+set_size,:) =  AllErr.Grid3; AllErr1.Grid5(stop:stop+set_size,:) =  AllErr.Grid5;
   AllErr1.AL1(stop:stop+set_size,:) =  AllErr.AL1; AllErr1.AL3(stop:stop+set_size,:) =  AllErr.AL3; 
   AllErr1.AL5(stop:stop+set_size,:) =  AllErr.AL5; AllErr1.SL200(stop:stop+set_size,:) =  AllErr.SL200;
        
        
    c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
    figure,
    errorbar( mean(sqz(AllErr1.Rand1)), std(sqz(AllErr1.Rand1)), 'Color', c(1*12,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr1.Rand3)), std(sqz(AllErr1.Rand3)), 'Color', c(2*12,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35); 
    errorbar( mean(sqz(AllErr1.Rand5)), std(sqz(AllErr1.Rand5)), 'Color', c(3*12,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);  
    errorbar( mean(sqz(AllErr1.Grid1)), std(sqz(AllErr1.Grid1)), 'Color', c(4*12,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr1.Grid3)), std(sqz(AllErr1.Grid3)), 'Color', c(5*12,:), 'Marker', shape{5}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35); 
    errorbar( mean(sqz(AllErr1.Grid5)), std(sqz(AllErr1.Grid5)), 'Color', c(6*12,:), 'Marker', shape{6}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);  
    errorbar( mean(sqz(AllErr1.AL1)), std(sqz(AllErr1.AL1)), 'Color', c(7*12,:), 'Marker', shape{7}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);
    errorbar( mean(sqz(AllErr1.AL3)), std(sqz(AllErr1.AL3)), 'Color', c(8*12,:), 'Marker', shape{8}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35); 
    errorbar( mean(sqz(AllErr1.AL5)), std(sqz(AllErr1.AL5)), 'Color', c(9*12,:), 'Marker', shape{9}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35);    
    errorbar( mean(sqz(AllErr1.SL200)), std(sqz(AllErr1.SL200)), 'Color', c(10*12,:), 'Marker', shape{10}, 'LineWidth',2, 'MarkerSize', 10), hold on
    set(gca, 'FontSize', 35); 
%     errorbar( mean(sqz(Diceal)), std(sqz(Diceal)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), hold on
%     set(gca, 'FontSize', 35);
    error_measure = sprintf(['Dice Measure over 90 Pairs' '\n' 'with 5%% noise']);
    title( error_measure , 'FontSize', 35), xlabel('Iterations', 'FontSize', 35), ylabel('Dice', 'FontSize', 35), %, ' lambda = ' num2str(lambda)
%     legend('SL only image features', 'SL only uncertainty', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),
    legend('Rand1', 'Rand2', 'Rand3', 'Grid1', 'Grid2', 'Grid3', 'AL1', 'AL2', 'AL3', 'SL200', 'Location', 'SouthEastOutside'), % , 'AL Baseline w-unc', 'AL Baseline wo-unc', 'Location', 'SouthEastOutside'),

    
    
        counter = 1;
        for k = 1:5,
            Dicegd(counter, k) = mean(AllErr.AL5_Seg(k).dice(:));
            Dicegrd_1(counter, :, k) = AllErr.AL5_Seg(k).dice(:);
        end
    
            
%% 
path1 = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\AL_SL\Regression\All_Methods\';
filename = {'AllErr' 'Landmarks'};
for Mode = [1:14], % 3,5:17],%:18,
    for i = 5:8,%8
        for name = 1:2,
            load ([path1 filename{name} num2str(i) '_small_trial1_Mode' num2str(Mode) '_regress_unc_noise5.mat']);
        end
        switch (Mode)
            case 1
                AllErr1.Rand1((i-5)*5+1:(i-4)*5,:) = AllErr.Rand1; 
                Landmarks1.slands_Rand1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Rand1;
                Landmarks1.tlands_Rand1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Rand1;
            case 2
                AllErr1.Rand2((i-5)*5+1:(i-4)*5,:) = AllErr.Rand2; 
                Landmarks1.slands_Rand2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Rand2;
                Landmarks1.tlands_Rand2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Rand2;
            case 3
                AllErr1.Rand3((i-5)*5+1:(i-4)*5,:) = AllErr.Rand3;
                Landmarks1.slands_Rand3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Rand3;
                Landmarks1.tlands_Rand3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Rand3;
            case 4
                AllErr1.Grid1((i-5)*5+1:(i-4)*5,:) = AllErr.Grid1; 
                Landmarks1.slands_Grid1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Grid1;
                Landmarks1.tlands_Grid1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Grid1;
            case 5
                AllErr1.Grid2((i-5)*5+1:(i-4)*5,:) = AllErr.Grid2;
                Landmarks1.slands_Grid2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Grid2;
                Landmarks1.tlands_Grid2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Grid2;
            case 6
                AllErr1.Grid3((i-5)*5+1:(i-4)*5,:) = AllErr.Grid3;
                Landmarks1.slands_Grid3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_Grid3;
                Landmarks1.tlands_Grid3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_Grid3;
            case 7
                AllErr1.AL1((i-5)*5+1:(i-4)*5,:) = AllErr.AL1; 
                Landmarks1.slands_AL1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_AL1;
                Landmarks1.tlands_AL1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_AL1;
            case 8
                AllErr1.AL2((i-5)*5+1:(i-4)*5,:) = AllErr.AL2; 
                Landmarks1.slands_AL2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_AL2;
                Landmarks1.tlands_AL2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_AL2;
            case 9
                AllErr1.AL3((i-5)*5+1:(i-4)*5,:) = AllErr.AL3;
                Landmarks1.slands_AL3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_AL3;
                Landmarks1.tlands_AL3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_AL3;
            case 10
                AllErr1.SL1((i-5)*5+1:(i-4)*5,:) = AllErr.SL1;
                Landmarks1.slands_SL1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_SL1;
                Landmarks1.tlands_SL1(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_SL1;
            case 11
                AllErr1.SL2((i-5)*5+1:(i-4)*5,:) = AllErr.SL2;
                Landmarks1.slands_SL2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_SL2;
                Landmarks1.tlands_SL2(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_SL2;
            case 12
                AllErr1.SL3((i-5)*5+1:(i-4)*5,:) = AllErr.SL3;
                Landmarks1.slands_SL3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_SL3;
                Landmarks1.tlands_SL3(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_SL3;
            case 13
                AllErr1.SL4((i-5)*5+1:(i-4)*5,:) = AllErr.SL4;
                Landmarks1.slands_SL4(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_SL4;
                Landmarks1.tlands_SL4(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_SL4;

            case 14
                AllErr1.SL5((i-5)*5+1:(i-4)*5,:) = AllErr.SL5;
                Landmarks1.slands_SL5(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.slands_SL5;
                Landmarks1.tlands_SL5(:,:,(i-5)*5+1:(i-4)*5) = Landmarks.tlands_SL5;
        end
    end
end

%%
    clear barval, clear barerr
    barval(1,:) = mean(sqz(AllErr1.Rand1));    barerr(1,:) = std(sqz(AllErr1.Rand1));
    barval(2,:) = mean(sqz(AllErr1.Rand2));    barerr(2,:) = std(sqz(AllErr1.Rand2));
    barval(3,:) = mean(sqz(AllErr1.Rand3));    barerr(3,:) = std(sqz(AllErr1.Rand3));
    barval(4,:) = mean(sqz(AllErr1.Grid1));    barerr(4,:) = std(sqz(AllErr1.Grid1));
    barval(5,:) = mean(sqz(AllErr1.Grid2));    barerr(5,:) = std(sqz(AllErr1.Grid2));
    barval(6,:) = mean(sqz(AllErr1.Grid3));    barerr(6,:) = std(sqz(AllErr1.Grid3));
    barval(7,:) = mean(sqz(AllErr1.AL1));    barerr(7,:) = std(sqz(AllErr1.AL1));
    barval(8,:) = mean(sqz(AllErr1.AL2));    barerr(8,:) = std(sqz(AllErr1.AL2));
    barval(9,:) = mean(sqz(AllErr1.AL3));    barerr(9,:) = std(sqz(AllErr1.AL3));
    barval(10,:) = mean(sqz(AllErr1.SL1));  barerr(10,:) = std(sqz(AllErr1.SL1));   
    barval(11,:) = mean(sqz(AllErr1.SL2));  barerr(11,:) = std(sqz(AllErr1.SL2)); 
    barval(12,:) = mean(sqz(AllErr1.SL3));  barerr(12,:) = std(sqz(AllErr1.SL3));
    barval(13,:) = mean(sqz(AllErr1.SL4));  barerr(13,:) = std(sqz(AllErr1.SL4)); 
    barval(14,:) = mean(sqz(AllErr1.SL5));  barerr(14,:) = std(sqz(AllErr1.SL5)); 
   
    
    groupnames1 = {'R1'; 'R2'; 'R3'; 'G1'; 'G2'; 'G3'; 'A1'; 'A2'; 'A3'; 'S1'; 'S2'; 'S3'; 'S4'; 'S5'};
    width = .5; max_iter = 4;
    bw_title = [], bw_xlabel = [], bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barval(:,1:max_iter), barerr(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Warping Error for Different Methods', 'FontSize', 35), ylabel('Warping Error', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Itr4', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    
    

%%

path1 = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\AL_SL\Regression\All_Methods\';
start = 4;
for img = 1:sz(Landmarks1.slands_Rand1,3)
for Mode = [1:14], % 3,5:17],%:18,
    for iter = 1:4%8
        switch (Mode)
            case 1
                seed_num = 2;
                a = Landmarks1.slands_Rand1(:,start+1:start+(iter)*seed_num, img);
%                 landsr1 = pdist2(a',a');
%                 landsr1(1:sz(landsr1,1)+1:end) = [];
                meanr1(img, iter) = sqrt(sum(mean(a,2) .^2) );
                stdr1(img, iter) = sqrt(sum(std(a,0,2) .^2) );
            case 2
                seed_num = 5;
                a = Landmarks1.slands_Rand2(:,start+1:start+(iter)*seed_num,img);
                landsr2 = pdist2(a',a');
                landsr2(1:sz(landsr2,1)+1:end) = [];
                meanr2(img, iter) = mean(landsr2);
                stdr2(img, iter) = std(landsr2);
            case 3
                seed_num = 20;
                a = Landmarks1.slands_Rand3(:,start+1:start+(iter)*seed_num,img);
                landsr3 = pdist2(a',a');
                landsr3(1:sz(landsr3,1)+1:end) = [];
                meanr3(img, iter) = mean(landsr3);
                stdr3(img, iter) = std(landsr3);
            case 4
                seed_num = 2;
                a = Landmarks1.slands_Grid1(:,start+1:start+(iter)*seed_num,img);
%                 landsg1 = pdist2(a',a');
%                 landsg1(1:sz(landsg1,1)+1:end) = [];
                meang1(img, iter) = sqrt(sum(mean(a,2) .^2) );
                stdg1(img, iter) = sqrt(sum(std(a,0,2) .^2) );
            case 5
                seed_num = 5;
                a = Landmarks1.slands_Grid2(:,start+1:start+(iter)*seed_num,img);
                landsg2 = pdist2(a',a');
                landsg2(1:sz(landsg2,1)+1:end) = [];
                meang2(img, iter) = mean(landsg2);
                stdg2(img, iter) = std(landsg2);
            case 6
                seed_num = 20;
                a = Landmarks1.slands_Grid3(:,start+1:start+(iter)*seed_num,img);
                landsg3 = pdist2(a',a');
                landsg3(1:sz(landsg3,1)+1:end) = [];
                meang3(img, iter) = mean(landsg3);
                stdg3(img, iter) = std(landsg3); 
            case 7
                seed_num = 2;
                a = Landmarks1.slands_AL1(:,start+1:start+(iter)*seed_num,img);
%                 landsa1 = pdist2(a',a');
%                 landsa1(1:sz(landsa1,1)+1:end) = [];
                meana1(img, iter) = sqrt(sum(mean(a,2) .^2) );
                stda1(img, iter) = sqrt(sum(std(a,0,2) .^2) );
            case 8
                seed_num = 5;
                a = Landmarks1.slands_AL2(:,start+1:start+(iter)*seed_num,img);
                landsa2 = pdist2(a',a');
                landsa2(1:sz(landsa2,1)+1:end) = [];
                meana2(img, iter) = mean(landsa2);
                stda2(img, iter) = std(landsa2);
            case 9
                seed_num = 20;
                a = Landmarks1.slands_AL3(:,start+1:start+(iter)*seed_num,img);
                landsa3 = pdist2(a',a');
                landsa3(1:sz(landsa3,1)+1:end) = [];
                meana3(img, iter) = mean(landsa3);
                stda3(img, iter) = std(landsa3); 
            case 10
                seed_num = 2;
                a = Landmarks1.slands_SL1(:,start+1:start+(iter)*seed_num,img);
%                 landss1 = pdist2(a',a');
%                 landss1(1:sz(landss1,1)+1:end) = [];
                means1(img, iter) = sqrt(sum(mean(a,2) .^2) );
                stds1(img, iter) = sqrt(sum(std(a,0,2) .^2) );
            case 11
                seed_num = 5;
                a = Landmarks1.slands_SL2(:,start+1:start+(iter)*seed_num,img);
                landss2 = pdist2(a',a');
                landss2(1:sz(landss2,1)+1:end) = [];
                means2(img, iter) = mean(landss2);
                stds2(img, iter) = std(landss2);
            case 12
                seed_num = 20;
                a = Landmarks1.slands_SL3(:,start+1:start+(iter)*seed_num,img);
                landss3 = pdist2(a',a');
                landss3(1:sz(landss3,1)+1:end) = [];
                means3(img, iter) = mean(landss3);
                stds3(img, iter) = std(landss3);

            case 13
                seed_num = 50;
                a = Landmarks1.slands_SL4(:,start+1:start+(iter)*seed_num,img);
                landss4 = pdist2(a',a');
                landss4(1:sz(landss4,1)+1:end) = [];
                means4(img, iter) = mean(landss4);
                stds4(img, iter) = std(landss4);
            case 14
                seed_num = 50;
                a = Landmarks1.slands_SL5(:,start+1:start+(iter)*seed_num,img);
                landss5 = pdist2(a',a');
                landss5(1:sz(landss4,1)+1:end) = [];
                means5(img, iter) = mean(landss5);
                stds5(img, iter) = std(landss5);
        end
    end
end
end

    clear barval, clear barerr,

    barval(1,:) = mean(sqz(stdr1));    barerr(1,:) = std(sqz(stdr1));
    barval(2,:) = mean(sqz(stdr2));    barerr(2,:) = std(sqz(stdr2));
    barval(3,:) = mean(sqz(stdr3));    barerr(3,:) = std(sqz(stdr3));    
    barval(4,:) = mean(sqz(stdg1));    barerr(4,:) = std(sqz(stdg1));
    barval(5,:) = mean(sqz(stdg2));    barerr(5,:) = std(sqz(stdg2));
    barval(6,:) = mean(sqz(stdg3));    barerr(6,:) = std(sqz(stdg3));
    barval(7,:) = mean(sqz(stda1));    barerr(7,:) = std(sqz(stda1));
    barval(8,:) = mean(sqz(stda2));    barerr(8,:) = std(sqz(stda2));
    barval(9,:) = mean(sqz(stda3));    barerr(9,:) = std(sqz(stda3));
    barval(10,:) = mean(sqz(stds1));    barerr(10,:) = std(sqz(stds1));
    barval(11,:) = mean(sqz(stds2));    barerr(11,:) = std(sqz(stds2));   
    barval(12,:) = mean(sqz(stds3));    barerr(12,:) = std(sqz(stds3)); 
    barval(13,:) = mean(sqz(stds4));    barerr(13,:) = std(sqz(stds4)); 
    barval(14,:) = mean(sqz(stds5));    barerr(14,:) = std(sqz(stds5)); 



    groupnames1 = {'R1'; 'R2'; 'R3'; 'G1'; 'G2'; 'G3'; 'A1'; 'A2'; 'A3'; 'S1'; 'S2'; 'S3'; 'S4'; 'S5'};
    width = .5; max_iter = 3;
    bw_title = [], bw_xlabel = [], bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barval(:,1:max_iter), barerr(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Spread of Seeds for Different Methods', 'FontSize', 35), ylabel('Seed Spread (Pixels)', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    
    

    
    
%%
    clear barval, clear barerr,
    barval(1,:) = mean(sqz(AllErr1.Rand2));    barerr(1,:) = std(sqz(AllErr1.Rand2));
    barval(2,:) = mean(sqz(AllErr1.Rand3));    barerr(2,:) = std(sqz(AllErr1.Rand3));    
    barval(3,:) = mean(sqz(AllErr1.Grid2));    barerr(3,:) = std(sqz(AllErr1.Grid2));
    barval(4,:) = mean(sqz(AllErr1.Grid3));    barerr(4,:) = std(sqz(AllErr1.Grid3));
    barval(5,:) = mean(sqz(AllErr1.AL2));      barerr(5,:) = std(sqz(AllErr1.AL2));
    barval(6,:) = mean(sqz(AllErr1.AL3));      barerr(6,:) = std(sqz(AllErr1.AL3));
% %     barval(7,:) = mean(sqz(AllErr1.AL6));    barerr(7,:) = std(sqz(AllErr1.AL6));
    barval(7,:) = mean(sqz(AllErr1.SL2));  barerr(7,:) = std(sqz(AllErr1.SL2));   
    barval(8,:) = mean(sqz(AllErr1.SL3));  barerr(8,:) = std(sqz(AllErr1.SL3)); 
    barval(9,:) = mean(sqz(AllErr1.SL5));  barerr(9,:) = std(sqz(AllErr1.SL5)); 

    clear barvaldist, clear barerrdist,

    barvaldist(1,:) = mean(sqz(stdr2));    barerrdist(1,:) = std(sqz(stdr2));
    barvaldist(2,:) = mean(sqz(stdr3));    barerrdist(2,:) = std(sqz(stdr3));    
    barvaldist(3,:) = mean(sqz(stdg2));    barerrdist(3,:) = std(sqz(stdg2));
    barvaldist(4,:) = mean(sqz(stdg3));    barerrdist(4,:) = std(sqz(stdg3));
    barvaldist(6,:) = mean(sqz(stda2));    barerrdist(6,:) = std(sqz(stda2));
    barvaldist(5,:) = mean(sqz(stda3));    barerrdist(5,:) = std(sqz(stda3));
    barvaldist(7,:) = mean(sqz(stds2));    barerrdist(7,:) = std(sqz(stds2));   
    barvaldist(8,:) = mean(sqz(stds3));    barerrdist(8,:) = std(sqz(stds3)); 
    barvaldist(9,:) = mean(sqz(stds5));    barerrdist(9,:) = std(sqz(stds5)); 



    groupnames1 = {'R1'; 'R2'; 'G1'; 'G2'; 'A1'; 'A2'; 'S1'; 'S2'; 'S3'};
    width = .5; max_iter = 3;
    bw_title = []; bw_xlabel = []; bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barvaldist(:,1:max_iter), barerrdist(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Spread of Seeds for Different Methods', 'FontSize', 35), ylabel('Seed Spread (Pixels)', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    
    
    groupnames1 = {'R1'; 'R2'; 'G1'; 'G2'; 'A1'; 'A2'; 'S1'; 'S2'; 'S3'};
    width = .5; max_iter = 4;
    bw_title = []; bw_xlabel = []; bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barval(:,1:max_iter), barerr(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Warping Error for Different Methods', 'FontSize', 35), ylabel('Warping Error', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Itr4', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    
    
    
%%
path1 = '../results/AL_SL/Regression/ExpEr/';  % another2/
filename = {'AllErr' 'Landmarks'};

seg_length = 22; 
counter = 1;
for i = 1:10, %[2,4:10], % [1,3], % 
    for j =  1:10, %1:10, % 1:7, % 
        if (i~=j)
            for Mode = 1:8,
                for name = 1:2,
                    load ([path1 filename{name} num2str(i) '_to_img' num2str(j) '_Mode' num2str(Mode) '_regress_noise5.mat']);
                end
                for reg_num = 1:4,
                    switch (Mode)
                        case 1
                            a = AllErr.Rand1_Seg(reg_num).dice;
                            AllErr1.Rand1_Seg(counter,1:seg_length,reg_num) = a(1:seg_length); 
                            Landmarks1.slands_Rand1_Seg(:,:,counter) = Landmarks.slands_Rand1;
                            Landmarks1.tlands_Rand1_Seg(:,:,counter) = Landmarks.tlands_Rand1;
                        case 2
                            a = AllErr.Rand2_Seg(reg_num).dice;
                            AllErr1.Rand2_Seg(counter,1:seg_length,reg_num) = a(1:seg_length); 
                            Landmarks1.slands_Rand2_Seg(:,:,counter) = Landmarks.slands_Rand2;
                            Landmarks1.tlands_Rand2_Seg(:,:,counter) = Landmarks.tlands_Rand2;
                        case 3
                            a = AllErr.Grid1_Seg(reg_num).dice;
                            AllErr1.Grid1_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_Grid1_Seg(:,:,counter) = Landmarks.slands_Grid1;
                            Landmarks1.tlands_Grid1_Seg(:,:,counter) = Landmarks.tlands_Grid1;
                        case 4
                            a = AllErr.Grid2_Seg(reg_num).dice;
                            AllErr1.Grid2_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_Grid2_Seg(:,:,counter) = Landmarks.slands_Grid2;
                            Landmarks1.tlands_Grid2_Seg(:,:,counter) = Landmarks.tlands_Grid2;
                        case 5
                            a = AllErr.AL1_Seg(reg_num).dice;
                            AllErr1.AL1_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_AL1_Seg(:,:,counter) = Landmarks.slands_AL1;
                            Landmarks1.tlands_AL1_Seg(:,:,counter) = Landmarks.tlands_AL1;
                        case 6
                            a = AllErr.AL2_Seg(reg_num).dice;
                            AllErr1.AL2_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_AL2_Seg(:,:,counter) = Landmarks.slands_AL2;
                            Landmarks1.tlands_AL2_Seg(:,:,counter) = Landmarks.tlands_AL2;
                        case 7
                            a = AllErr.SL1_Seg(reg_num).dice;
                            AllErr1.SL1_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_SL1_Seg(:,:,counter) = Landmarks.slands_SL1;
                            Landmarks1.tlands_SL1_Seg(:,:,counter) = Landmarks.tlands_SL1;
                        case 8
                            a = AllErr.SL2_Seg(reg_num).dice;
                            AllErr1.SL2_Seg(counter,1:seg_length,reg_num) = a(1:seg_length);
                            Landmarks1.slands_SL2_Seg(:,:,counter) = Landmarks.slands_SL2;
                            Landmarks1.tlands_SL2_Seg(:,:,counter) = Landmarks.tlands_SL2;

                    end
                end
            end
            counter = counter + 1;            
        end
    end
end
 
    
%%

path1 = 'Y:\students_less\tlotfima\RWRegistration_Final2D\results\AL_SL\Regression\All_Methods\';
start = 4;
for img = 1:sz(Landmarks1.slands_Rand1_Seg,3)
for Mode = [1:8], % 3,5:17],%:18,
    for iter = 1:4%8
        switch (Mode)
            case 1
                seed_num = 5;
                a = Landmarks1.slands_Rand1_Seg(:,start+1:start+(iter)*seed_num, img);
                landsr1 = pdist2(a',a');
                landsr1(1:sz(landsr1,1)+1:end) = [];
                meanr1(img, iter) = mean(landsr1);
                stdr1(img, iter) = std(landsr1);
            case 2
                seed_num = 20;
                a = Landmarks1.slands_Rand2_Seg(:,start+1:start+(iter)*seed_num,img);
                landsr2 = pdist2(a',a');
                landsr2(1:sz(landsr2,1)+1:end) = [];
                meanr2(img, iter) = mean(landsr2);
                stdr2(img, iter) = std(landsr2);
            case 3
                seed_num = 5;
                a = Landmarks1.slands_Grid1_Seg(:,start+1:start+(iter)*seed_num,img);
                landsg1 = pdist2(a',a');
                landsg1(1:sz(landsg1,1)+1:end) = [];
                meang1(img, iter) = mean(landsg1);
                stdg1(img, iter) = std(landsg1);
            case 4
                seed_num = 20;
                a = Landmarks1.slands_Grid2_Seg(:,start+1:start+(iter)*seed_num,img);
                landsg2 = pdist2(a',a');
                landsg2(1:sz(landsg2,1)+1:end) = [];
                meang2(img, iter) = mean(landsg2);
                stdg2(img, iter) = std(landsg2);
            case 5
                seed_num = 5;
                a = Landmarks1.slands_AL1_Seg(:,start+1:start+(iter)*seed_num,img);
                landsa1 = pdist2(a',a');
                landsa1(1:sz(landsa1,1)+1:end) = [];
                meana1(img, iter) = mean(landsa1);
                stda1(img, iter) = std(landsa1);
            case 6
                seed_num = 20;
                a = Landmarks1.slands_AL2_Seg(:,start+1:start+(iter)*seed_num,img);
                landsa2 = pdist2(a',a');
                landsa2(1:sz(landsa2,1)+1:end) = [];
                meana2(img, iter) = mean(landsa2);
                stda2(img, iter) = std(landsa2);
            case 7
                seed_num = 5;
                a = Landmarks1.slands_SL1_Seg(:,start+1:start+(iter)*seed_num,img);
                landss1 = pdist2(a',a');
                landss1(1:sz(landss1,1)+1:end) = [];
                means1(img, iter) = mean(landss1);
                stds1(img, iter) = std(landss1);
            case 8
                seed_num = 20;
                a = Landmarks1.slands_SL2_Seg(:,start+1:start+(iter)*seed_num,img);
                landss2 = pdist2(a',a');
                landss2(1:sz(landss2,1)+1:end) = [];
                means2(img, iter) = mean(landss2);
                stds2(img, iter) = std(landss2);
        end
    end
end
end
%%
  

    
    
       clear barval, clear barerr, clear barvaldist, clear barerrdist,
    
    DiceRand1 = sqz(mean(AllErr1.Rand1_Seg,1)); DiceRand1_1 = AllErr1.Rand1_Seg;
    DiceRand2 = sqz(mean(AllErr1.Rand2_Seg,1)); DiceRand2_1 = AllErr1.Rand2_Seg;
    DiceGrid1 = sqz(mean(AllErr1.Grid1_Seg,1)); DiceGrid1_1 = AllErr1.Grid1_Seg;
    DiceGrid2 = sqz(mean(AllErr1.Grid2_Seg,1)); DiceGrid2_1 = AllErr1.Grid2_Seg;    
    DiceAL1 = sqz(mean(AllErr1.AL1_Seg,1)); DiceAL5_1 = AllErr1.AL1_Seg;    
    DiceAL2 = sqz(mean(AllErr1.AL2_Seg,1)); DiceAL6_1 = AllErr1.AL2_Seg;    
    DiceSL1 = sqz(mean(AllErr1.SL1_Seg,1)); DiceSL100_1 = AllErr1.SL1_Seg;    
    DiceSL2 = sqz(mean(AllErr1.SL2_Seg,1)); DiceSL200_1 = AllErr1.SL2_Seg; 
    
    
    
    max_iter = 4;
    img_max = 2;
    barval1(1,:) = mean(sqz(DiceRand1(1:img_max,1:max_iter)));    barerr1(1,:) = std(sqz(DiceRand1(1:img_max,1:max_iter)));
    barval1(2,:) = mean(sqz(DiceRand2(1:img_max,1:max_iter)));    barerr1(2,:) = std(sqz(DiceRand2(1:img_max,1:max_iter)));
    barval1(3,:) = mean(sqz(DiceGrid1(1:img_max,1:max_iter)));    barerr1(3,:) = std(sqz(DiceGrid1(1:img_max,1:max_iter)));
    barval1(4,:) = mean(sqz(DiceGrid2(1:img_max,1:max_iter)));    barerr1(4,:) = std(sqz(DiceGrid2(1:img_max,1:max_iter)));
    barval1(5,:) = mean(sqz(DiceAL1(1:img_max,1:max_iter)));      barerr1(5,:) = std(sqz(DiceAL1(1:img_max,1:max_iter)));
    barval1(6,:) = mean(sqz(DiceAL2(1:img_max,1:max_iter)));      barerr1(6,:) = std(sqz(DiceAL2(1:img_max,1:max_iter)));
    barval1(7,:) = mean(sqz(DiceSL1(1:img_max,1:max_iter)));    barerr1(7,:) = std(sqz(DiceSL1(1:img_max,1:max_iter)));
    barval1(8,:) = mean(sqz(DiceSL2(1:img_max,1:max_iter)));    barerr1(8,:) = std(sqz(DiceSL2(1:img_max,1:max_iter)));

    barvaldist(1,:) = mean(sqz(stdr1));    barerrdist(1,:) = std(sqz(stdr1));
    barvaldist(2,:) = mean(sqz(stdr2));    barerrdist(2,:) = std(sqz(stdr2));
    barvaldist(3,:) = mean(sqz(stdg1));    barerrdist(3,:) = std(sqz(stdg1));
    barvaldist(4,:) = mean(sqz(stdg2));    barerrdist(4,:) = std(sqz(stdg2));
    barvaldist(5,:) = mean(sqz(stda1));    barerrdist(5,:) = std(sqz(stda1));
    barvaldist(6,:) = mean(sqz(stda2));    barerrdist(6,:) = std(sqz(stda2));
    barvaldist(7,:) = mean(sqz(stds1));    barerrdist(7,:) = std(sqz(stds1));
    barvaldist(8,:) = mean(sqz(stds2));    barerrdist(8,:) = std(sqz(stds2));   
    

    groupnames1 = {'R1'; 'R2'; 'G1'; 'G2'; 'A1'; 'A2'; 'S1'; 'S2'};
    width = .5; max_iter = 4;
    bw_title = [], bw_xlabel = [], bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barval1(:,1:max_iter), barerr1(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Dice Measure for Different Methods', 'FontSize', 35), ylabel('Dice', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Itr4', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    


    groupnames1 = {'R1'; 'R2'; 'G1'; 'G2'; 'A1'; 'A2'; 'S1'; 'S2'};
    width = .5; max_iter = 3;
    bw_title = [], bw_xlabel = [], bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barvaldist(:,1:max_iter), barerrdist(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Spread of Seeds for Different Methods', 'FontSize', 35), ylabel('Seed Spread (Pixels)', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    
    


%%
    
    
    
    
    save ('../results/first_test_regress.mat', 'barval', 'barerr', 'barval1', 'barerr1');

  
    
    
    %%
    Path1 = '../results/AL_SL/Regression/ExpEr/';
counter = 1;


      clear barval, clear barerr, clear barvaldist, clear barerrdist,
    ind1 = 1:22; %ind1 = [1 4 5 8 9 17 18 19 21 22]; ind2 = [1 4 5 8 9 17 18 19 21 22];
    DiceRand1 = sqz(mean(AllErr1.Rand1_Seg(:,ind1,:),2)); DiceRand1_1 = AllErr1.Rand1_Seg(:,ind1,:);
    DiceRand2 = sqz(mean(AllErr1.Rand2_Seg(:,ind1,:),2)); DiceRand2_1 = AllErr1.Rand2_Seg(:,ind1,:);
    DiceGrid1 = sqz(mean(AllErr1.Grid1_Seg(:,ind1,:),2)); DiceGrid1_1 = AllErr1.Grid1_Seg(:,ind1,:);
    DiceGrid2 = sqz(mean(AllErr1.Grid2_Seg(:,ind1,:),2)); DiceGrid2_1 = AllErr1.Grid2_Seg(:,ind1,:);    
    DiceAL1 = sqz(mean(AllErr1.AL1_Seg(:,ind1,:),2)); DiceAL1_1 = AllErr1.AL1_Seg(:,ind1,:);    
    DiceAL2 = sqz(mean(AllErr1.AL2_Seg(:,ind1,:),2)); DiceAL2_1 = AllErr1.AL2_Seg(:,ind1,:);    
    DiceSL1 = sqz(mean(AllErr1.SL1_Seg(:,ind1,:),2)); DiceSL_1 = AllErr1.SL1_Seg(:,ind1,:);    
    DiceSL2 = sqz(mean(AllErr1.SL2_Seg(:,ind1,:),2)); DiceSL2_1 = AllErr1.SL2_Seg(:,ind1,:);   

    max_iter = 4;
    img_max = 13;%72;
    barval1(1,:) = mean(sqz(DiceRand1(1:img_max,1:max_iter)));    barerr1(1,:) = std(sqz(DiceRand1(1:img_max,1:max_iter)));
    barval1(2,:) = mean(sqz(DiceRand2(1:img_max,1:max_iter)));    barerr1(2,:) = std(sqz(DiceRand2(1:img_max,1:max_iter)));
    barval1(3,:) = mean(sqz(DiceGrid1(1:img_max,1:max_iter)));    barerr1(3,:) = std(sqz(DiceGrid1(1:img_max,1:max_iter)));
    barval1(4,:) = mean(sqz(DiceGrid2(1:img_max,1:max_iter)));    barerr1(4,:) = std(sqz(DiceGrid2(1:img_max,1:max_iter)));
    barval1(5,:) = mean(sqz(DiceAL1(1:img_max,1:max_iter)));      barerr1(5,:) = std(sqz(DiceAL1(1:img_max,1:max_iter)));
    barval1(6,:) = mean(sqz(DiceAL2(1:img_max,1:max_iter)));      barerr1(6,:) = std(sqz(DiceAL2(1:img_max,1:max_iter)));
    barval1(7,:) = mean(sqz(DiceSL1(1:img_max,1:max_iter)));    barerr1(7,:) = std(sqz(DiceSL1(1:img_max,1:max_iter)));
    barval1(8,:) = mean(sqz(DiceSL2(1:img_max,1:max_iter)));    barerr1(8,:) = std(sqz(DiceSL2(1:img_max,1:max_iter)));

    groupnames1 = {'R1'; 'R2'; 'G1'; 'G2'; 'A1'; 'A2'; 'S1'; 'S2'};
    width = .5; max_iter = 4;
    bw_title = [], bw_xlabel = [], bw_ylabel = []; bw_colormap = jet; gridstatus = 'xy'; bw_legend = []; error_sides = 2; legend_type = 'plot';

    figure, barweb(barval1(:,1:max_iter), barerr1(:,1:max_iter), width, groupnames1, bw_title, bw_xlabel, bw_ylabel, bw_colormap, gridstatus, bw_legend, error_sides, legend_type);    
    title('Dice Measure for Different Methods', 'FontSize', 35), ylabel('Dice', 'FontSize', 35),  
    legend('Itr1', 'Itr2', 'Itr3', 'Itr4', 'Location', 'BestOutside');set(gca, 'FontSize', 35);    

grid on,

    
end
