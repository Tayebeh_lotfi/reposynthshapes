function Showing_Results1()
    clc, clear all, close all,    
    image_number = 20; lambda = 0.95; cert = 0.5;
%      path = 'H:/students_less/tlotfima/RWRegistration_Final2D/results/final_results/five_trials/'; 
    path = 'H:/students/tlotfima/RWRegistration_Final2D/results/'; 
    path2 = 'H:/students/tlotfima/RWRegistration_Final2D/results/'; 
    file_name = {'AllErr', 'Label_set', 'Displace_field', 'Uncertainty', 'Time', 'Landmarks'};
    for trial_num = 1 : 2,
        for i = 1:6,
          load (num2str([path file_name{i} num2str(trial_num) '_set1.mat']));
          
        end
        
        AllErr_All = AllErr; Time_All = Time; Landmarks_All = Landmarks;
%         Warped_vol_All = Warped_vol; Diff_vol_All = Diff_vol; Edge_vol_All = Edge_vol; 
        Uncertainty_All = Uncertainty; 
        Displace_field_All.Rand = Displace_field.Rand; Displace_field_All.SL = Displace_field.SL;
        Displace_field_All.AL = Displace_field.AL; Displace_field_All.AL_Base = Displace_field.AL_Base;
        
        Label_set_All.Rand = Label_set.Rand; Label_set_All.SL = Label_set.SL;
        Label_set_All.AL = Label_set.AL; Label_set_All.AL_Base = Label_set.AL_Base;
        for i = 1:6,
          load (num2str([path file_name{i} num2str(trial_num) '_set2.mat'])); 
        end
        AllErr_All.Rand(21:40,:) = AllErr.Rand(21:40,:); AllErr_All.AL(21:40,:) = AllErr.AL(21:40,:);
        AllErr_All.SL(21:40,:) = AllErr.SL(21:40,:); AllErr_All.AL_baseline(21:40,:) = AllErr.AL_baseline(21:40,:);
        
        Uncertainty_All.Rand(:,:,:,21:40) = Uncertainty.Rand(:,:,:,21:40); Uncertainty_All.AL(:,:,:,21:40) = Uncertainty.AL(:,:,:,21:40);
        Uncertainty_All.SL(:,:,:,21:40) = Uncertainty.SL(:,:,:,21:40); Uncertainty_All.AL_Base(:,:,:,21:40) = Uncertainty.AL_Base(:,:,:,21:40);

        Displace_field_All.Rand(:,:,21:40) = Displace_field.Rand(:,:,21:40); Displace_field_All.SL(:,:,21:40) = Displace_field.SL(:,:,21:40);
        Displace_field_All.AL(:,:,21:40) = Displace_field.AL(:,:,21:40); Displace_field_All.AL_Base(:,:,21:40) = Displace_field.AL_Base(:,:,21:40);
        
        Label_set_All.Rand(:,:,21:40) = Label_set.Rand(:,:,21:40); Label_set_All.SL(:,:,21:40) = Label_set.SL(:,:,21:40);
        Label_set_All.AL(:,:,21:40) = Label_set.AL(:,:,21:40); Label_set_All.AL_Base(:,:,21:40) = Label_set.AL_Base(:,:,21:40);
        
        Time_All.Rand(21:40) = Time.Rand(21:40); Time_All.AL(21:40) = Time.AL(21:40);
        Time_All.SL(21:40) = Time.SL(21:40); Time_All.Base(21:40) = Time.Base(21:40);
        
        Landmarks_All.slands_Rand(:,:,21:40) = Landmarks.slands_Rand(:,:,21:40); 
        Landmarks_All.tlands_Rand(:,:,21:40) = Landmarks.tlands_Rand(:,:,21:40); 
        Landmarks_All.slands_AL(:,:,21:40) = Landmarks.slands_AL(:,:,21:40); 
        Landmarks_All.tlands_AL(:,:,21:40) = Landmarks.tlands_AL(:,:,21:40); 
        Landmarks_All.slands_SL(:,:,21:40) = Landmarks.slands_SL(:,:,21:40); 
        Landmarks_All.tlands_SL(:,:,21:40) = Landmarks.tlands_SL(:,:,21:40); 
        Landmarks_All.slands_AL_Base(:,:,21:40) = Landmarks.slands_AL_Base(:,:,21:40); 
        Landmarks_All.tlands_AL_Base(:,:,21:40) = Landmarks.tlands_AL_Base(:,:,21:40); 

        clear AllErr; clear Uncertsinty; clear Time; clear Landmarks;
%         clear Warped_vol; clear Diff_vol; clear Edge_vol;
        clear Label_set; clear Displace_field;
        
        AllErr = AllErr_All; Uncertainty = Uncertainty_All; Time = Time_All;
%         Warped_vol = Warped_vol_All; Diff_vol = Diff_vol_All; Edge_vol = Edge_vol_All; 
        Landmarks = Landmarks_All; Displace_field = Displace_field_All; Label_set = Label_set_All;
        
        clear AllErr_All; clear Uncertsinty_All; clear Time_All; clear Landmarks_All;
%         clear Warped_vol_All; clear Diff_vol_All; clear Edge_vol_All;
        clear Label_set_All; clear Displace_field_All;
        for i = 1:6,
            save (num2str([path2 file_name{i} num2str(trial_num) '_all.mat']), file_name{i}); 
        end
    end
    for trial_num = 1:2,
        for i = 1:6,
              load (num2str([path file_name{i} num2str(trial_num) '_all.mat']));
        end
        
        load(['../data/Precomputed/trial' num2str(trial_num) '_info.mat']);

        number = 1;
        path = 'H:/students/tlotfima/RWRegistration_Final2D/results/';
        load ([path 'AllErr' num2str(number) '_new_set2']);
        AllErr_all.SL(1:5,:) = AllErr.SL(21:25,:);
        AllErr_all.AL(1:5,:) = AllErr.AL(21:25,:);
        AllErr_all.AL_baseline(1:5,:) = AllErr.AL_baseline(21:25,:);
        AllErr_all.Rand(1:5,:) = AllErr.Rand(21:25,:);
        load ([path 'AllErr' num2str(number) '_new_set1']);
        AllErr_all.SL(6:10,:) = AllErr.SL(1:5,:);
        AllErr_all.AL(6:10,:) = AllErr.AL(1:5,:);
        AllErr_all.AL_baseline(6:10,:) = AllErr.AL_baseline(1:5,:);
        AllErr_all.Rand(6:10,:) = AllErr.Rand(1:5,:);
        clear AllErr;
        AllErr = AllErr_all;

        c = jet(120);
        shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'}; mn = [1 5]; epsilon = 0.2;
        figure,
        errorbar( mean(sqz(AllErr.Rand)), std(sqz(AllErr.Rand)), 'Color', c(1*20,:), 'Marker', shape{1}, 'LineWidth',2, 'MarkerSize', 10), 
        xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
        errorbar( mean(sqz(AllErr.RandG)), std(sqz(AllErr.RandG)), 'Color', c(2*20,:), 'Marker', shape{2}, 'LineWidth',2, 'MarkerSize', 10), 
        xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
        errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), 'Color', c(3*20,:), 'Marker', shape{3}, 'LineWidth',2, 'MarkerSize', 10), 
        xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
        errorbar( mean(sqz(AllErr.SL_All)), std(sqz(AllErr.SL_All)), 'Color', c(4*20,:), 'Marker', shape{4}, 'LineWidth',2, 'MarkerSize', 10), 
        xlim([mn(1)-epsilon mn(2)+epsilon]), hold on
    %     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
        plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 5]))), 'Color', c(5*20,:), 'Marker', shape{5}, 'LineWidth',2, 'MarkerSize', 10),
        xlim([mn(1)-epsilon mn(2)+epsilon]), 
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
        error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'and 15%% noise']);
        title( error_measure, 'FontSize', 25 ), xlabel('Iterations', 'FontSize', 25), ylabel('MSE error', 'FontSize', 25), %, ' lambda = ' num2str(lambda)
        set(gca,'FontSize',25)
        AX = legend('Random', 'Grid', 'AL', 'SL', 'AL Baseline', 'Location', 'SouthEastOutside');
        LEG = findobj(AX,'type','text'); 
        set(LEG,'FontSize',25)

        img_number = 1; max_iteration = 5;
        Warped_set = Calculate_Warped_set(src3D, Label_set, Displace_field, img_number, max_iteration);
        Diff_set = Calc_Similarity(Warped_set, tar3D);
        Edge_set = Calc_Edge(src3D);            

        iter1 = 10; iter2 = 2;
        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,iter1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Warped_set.AL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Active Learning Warped image');
        subplot 222, montage(reshape(mat2gray(sqz(Warped_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline Warped image');
        subplot 223, montage(reshape(mat2gray(sqz(Warped_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection Warped image');
        subplot 224, montage(reshape(mat2gray(sqz(Warped_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning Warped image');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Diff_set.AL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Active Learning image Difference');
        subplot 222, montage(reshape(mat2gray(sqz(Diff_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline image Difference');
        subplot 223, montage(reshape(mat2gray(sqz(Diff_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection image Difference');
        subplot 224, montage(reshape(mat2gray(sqz(Diff_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning image Difference');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Edge_set.AL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Active Learning image Edge');
        subplot 222, montage(reshape(mat2gray(sqz(Edge_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline image Edge');
        subplot 223, montage(reshape(mat2gray(sqz(Edge_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection image Edge');
        subplot 224, montage(reshape(mat2gray(sqz(Edge_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning image Edge');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Uncertainty.AL(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Active Learning Uncertainty Field');
        subplot 222, montage(reshape(mat2gray(sqz(Uncertainty.AL_Base(:,:,iter2,:))),[szx  szy 1  szz]  )), title('Active Learning Baseline Uncertainty Field');
        subplot 223, montage(reshape(mat2gray(sqz(Uncertainty.Rand(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Random Selection Uncertainty Field');
        subplot 224, montage(reshape(mat2gray(sqz(Uncertainty.SL(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Self Learning Uncertainty Field');

        [szx szy szz] = size(src3D);
        figure, 
        subplot 121, montage(reshape(mat2gray(sqz(tar3D)),[szx  szy 1  szz]  )), title('Original Image Set');
        subplot 122, montage(reshape(mat2gray(sqz(src3D)),[szx  szy 1  szz]  )), title('Warped Image Set');

    end
    
    figure, for i = 1 : 12, subplot 121, imagesc(Solution_field.Rand(:,:,i)), colormap(gray), subplot 122, imagesc(Uncertainty.Rand(:,:,10)), colormap(gray), pause(); end
    
%     
%     
% my_pbs = pbs;
% a = my_pbs(50,50,:);
% a = (a - min(a(:))) ./ (max(a(:)) - min(a(:)));
% % a = a/ sum(a);
% my_pbs(50,50,:) = my_pbs(50,50,:) ./ (max(my_pbs(50,50,:)));
% figure,hist(sqz(a))
% figure,subplot 121, hist(sqz(my_pbs(2,3,:))), title('probability before scaling'), subplot 122, hist(sqz(a)), title('probability after scaling');
% 
%     figure, 
%     subplot 121, montage(reshape(mat2gray(tar3D),[size(tar3D,1)  size(tar3D,2) 1  size(tar3D,3)]  ))
% %     figure, 
%     subplot 122, montage(reshape(mat2gray(src3D),[size(src3D,1)  size(src3D,2) 1  size(src3D,3)]  ))
%     
%     for i = 1:16, hist(sqz(dterm(50,50,:)*100)), pause(); end
%     ind = sub2ind(size(src), 60, 50);
%     hist(sqz(data_prior(ind,:)))
%     
%     slands_new(1,:) =  mx( inds); %     slands_next(2,:) =  mx( inds);
% slands_new(2,:) =  my( inds); %    slands_next(2,:) =  my( inds);
% tlands_new(1,:) =  mx( inds)-nx; % -ny y/n, -nx y, +nx no, +ny no
% tlands_new(2,:) =  my( inds)-ny; % -nx
% 
% subplot 121, imagesc(tar), colormap(gray), hold on, plot(tlands_new(1,1:20:200), tlands_new(2,1:20:200), 'r*'), 
% subplot 122, imagesc(src), colormap(gray), hold on, plot(slands_new(1,1:20:200), slands_new(2,1:20:200), 'r*')
%     
% figure,
% subplot 121, imagesc(tar), colormap(gray), hold on, plot(tlands_new(1,:), tlands_new(2,:), 'r*'), 
% subplot 122, imagesc(src), colormap(gray), hold on, plot(slands_new(1,:), slands_new(2,:), 'r*')
% impixelinfo
% 
% data_prior = exp(- reshape( dterm, prod(isz), [] )/gamma);
% data_prior = exp(- reshape( dterm, prod(isz), [] )/gamma)*100;
% ind = sub2ind(size(src), 50, 50);
% figure, subplot 121, hist(sqz(data_prior(ind, :))),title('data prior'), subplot 122, hist(sqz(data_prior1(ind, :))),title('scaled data prior')
% 
% 
% my_pbs = pbs;
% for i = 1:size(src,1),
%     for j = 1:size(src,2),
%         a = my_pbs(i,j,:);
%         a = (a - min(a(:))) ./ (max(a(:)) - min(a(:)));
%         my_pbs(i,j,:) = a;
%     end
% end
% 
% for i = 1:30, imagesc(my_pbs(:,:,i)), colormap(gray), pause, end
% 
% 
% slands_new = [50 60]';
% ind = sub2ind(size(src), 60, 50);
% slands_new = []; tlands_new = [];
%  slands_new(1,:) = mx(ind);
%  slands_new(2,:) = my(ind);
% tlands_new(1,:) = mx(ind) +Ty(ind)
% tlands_new(2,:) = my(ind) +Tx(ind)
% 



figure, 
    montage(reshape(mat2gray(tar3D(:,:,1:6)),[size(tar3D(:,:,1:6),1)  size(tar3D(:,:,1:6),2) 1  size(tar3D(:,:,1:6),3)]  )), title('2D Original Image Set with 10% noise'),
figure, 
    montage(reshape(mat2gray(src3D(:,:,1:6)),[size(src3D(:,:,1:6),1)  size(src3D(:,:,1:6),2) 1  size(src3D(:,:,1:6),3)]  )), title('2D Warping Image Set with 10% noise'),

    
    
        AllErr_all.SL50(11:20,:) = AllErr.SL50;
        AllErr_all.SL100(11:20,:) = AllErr.SL100;
        AllErr_all.SL200(11:20,:) = AllErr.SL200;
        AllErr_all.AL(11:20,:) = AllErr.AL;
        AllErr_all.AL_baseline(11:20,:) = AllErr.AL_baseline;
        AllErr_all.Rand(11:20,:) = AllErr.Rand;
        AllErr_all.RandG(11:20,:) = AllErr.RandG;


        AllErr_all.SL50(6:10,:) = AllErr.SL50;
        AllErr_all.SL100(6:10,:) = AllErr.SL100;
        AllErr_all.SL200(6:10,:) = AllErr.SL200;
        AllErr_all.AL(6:10,:) = AllErr.AL;
        AllErr_all.AL_baseline(6:10,:) = AllErr.AL_baseline;
        AllErr_all.Rand(6:10,:) = AllErr.Rand;
        AllErr_all.RandG(6:10,:) = AllErr.RandG;


        clear AllErr;
        AllErr = AllErr_all;
    

        
        figure,
        errorbar( mean(sqz(AllErr.RandG)), std(sqz(AllErr.RandG)), '-or' ), hold on
        errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), '-*g' ), hold on
        errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), '-.vm' ), hold on
    %     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
        plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
        error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
        title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
        legend('Grid', 'AL', 'SL', 'AL Baseline', 'Location', 'SouthEastOutside'),

        
        
        figure,
        errorbar( mean(sqz(AllErr.SL50)), std(sqz(AllErr.SL50)), '-.vr' ), hold on
        errorbar( mean(sqz(AllErr.SL100)), std(sqz(AllErr.SL100)), '-.vg' ), hold on
        errorbar( mean(sqz(AllErr.SL200)), std(sqz(AllErr.SL200)), '-.vm' ), hold on
    %     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
%         plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
        error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
        title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
        legend('SL1', 'SL10', 'SL50', 'Location', 'SouthEastOutside'),
    
    
    
end
