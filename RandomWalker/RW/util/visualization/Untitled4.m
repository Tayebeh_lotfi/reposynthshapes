add_paths()
[debug flag Max_Iterations step parameter1 parameter2 beta alpha noise_sigma noise_mean Mode_Str ...
image_set_size num_seed savepath str mag dterm_mode res Unc_str] = initialize_parameters(); 
noise_sigma = 0.01; lbl_md = 1;
load ../data/Bspline.mat;
I = imresize(I, [100 100]); IWF = imresize(IWF, [100 100]);
mask = IWF>0.1;
tar_seg = seg;
for Mode = 1:15,
    IWF = imnoise( IWF, 'gaussian', 0, (Mode-1) * noise_sigma );
    %% Creating Landmarks & Data
    distx = 1; disty = 1;
    m = size(I);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; m(2)-disty]; 
    tlands(:,3) = [m(1)-distx; disty]; 
    tlands(:,4) = [m(1)-distx; m(2)-disty];
    slands = tlands;
    src = IWF; tar = I; Tx = TxW; Ty = TyW; invTx = invTxW; invTy = invTyW; 
    num_seed = 4; Max_Iterations = 5; alpha = 5000.00; reg_mod = 1; beta = 0.0005;
%% Build graph with 8 lattice
    isz = size(src);
    [points edges]=lattice( isz(1), isz(2), 0);
    slands_pre = slands; tlands_pre = tlands;
    Max_seeds = num_seed*Max_Iterations;
    nseeds = size(tlands,2);
%% Calculating the likelihood
    [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, lbl_md);
    dispvec_pre = dispvec; dterm_pre = dterm; nlabs_pre = nlabs; maxd_pre = maxd;
%% RW Starts here
    slands = slands_pre; 
    tlands = tlands_pre; 
    dispvec = dispvec_pre; dterm = dterm_pre; nlabs = nlabs_pre; maxd = maxd_pre;
    nseeds = size(tlands,2); 

%% Solving the problem
    [dx_sprd_Vec(:,:,Mode) dy_sprd_Vec(:,:,Mode) dx_Vec(:,:,Mode) dy_Vec(:,:,Mode) pbs pbs_sprd dsrc dsrc_sprd] = ...
        Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, dterm_mode, slands);
    
    for Mode = 1:15,
        dx = dx_Vec(:,:,Mode); dy = dy_Vec(:,:,Mode); dx_Sprd = dx_sprd_Vec(:,:,Mode); dy_Sprd = dy_sprd_Vec(:,:,Mode);
    Warp_Err_Vec(:,:,Mode) = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 )); 
        Warp_Err_Sprd_Vec(:,:,Mode) = (sqrt(  (dx_Sprd - invTx).^2 + (dy_Sprd - invTy).^2 )); 
    end
    
    for Mode = 1:5:15,
        Stat_Err_Unc(Shannon_Vec(:,:,Mode), Shannon_Sprd_Vec(:,:,Mode), Warp_Err_Vec(:,:,Mode), Warp_Err_Sprd_Vec(:,:,Mode), mask, Mode);
    end
    
end



 
Uncert_str = {'ShEnt' 'WProb' 'KDE' 'KNN'};
S = find(mask==1);   reg_mod = 3;     
for Unc_Mode = 1:4,
    switch Unc_Mode
        case 1
            Unc = Shannon_Vec;
        case 2
            Unc = Shannon_Sprd_Vec;
        case 3
            Unc = Kde_Vec;
        case 4
            Unc = Knn_Vec;
    end

    mind_tar = MIND_descriptor2d(tar_Vec,2);
    for i = 1:5:15,
        mind_src = MIND_descriptor2d(src_Vec(:,:,i),2);
        diff = sqrt( sum( (mind_tar - mind_src).^2, 3 ) );
        unc = Unc(:,:,i);
        
        gcc(:,:,Unc_Mode,i) = corrcoef(mat2gray(unc(S)), mat2gray(diff(S)));
        idata1 = double(mat2gray(unc(S)));
        idata2 = double(mat2gray(diff(S)));
        Mut_Info(Unc_Mode,i) = mutualinfo( idata1, idata2) ./ ...
            ( ( entropy(idata1) + entropy(idata2) ) ./ 2 );       
        dockf; plot(mat2gray(unc(S)), mat2gray(diff(S)),'r*'), xlabel('Uncert'), ylabel('Error'), title(['Uncert ', Uncert_str{Unc_Mode}, ', noise', num2str(i), '%', ...
            'GCC = ', num2str(gcc(1,2,Unc_Mode,i)), ' MI = ', num2str(Mut_Info(Unc_Mode,i))]);
        [p rsq] = lin_regress(mat2gray(unc(S)), mat2gray(diff(S)), reg_mod);
        if (reg_mod == 1)
            hold on, plot(mat2gray(unc(S)), p(1)*mat2gray(unc(S)) + p(2)*mat2gray(unc(S)), 'bo');
        else
            hold on, plot(mat2gray(unc(S)), p(1)*mat2gray(unc(S)).^3 + p(2)*mat2gray(unc(S)).^2 + p(3)*mat2gray(unc(S)) + p(4), 'bo');
        end        
    end
end