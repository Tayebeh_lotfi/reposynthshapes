function Showing_Results_10imgs()
    clc, clear all, close all,    
    image_number = 5; lambda = 0.95; cert = 0.5;
%      path = 'H:/students_less/tlotfima/RWRegistration_Final2D/results/final_results/five_trials/'; 
    path = 'H:/students/tlotfima/RWRegistration_Final2D/results/'; 
    path2 = 'H:/students/tlotfima/RWRegistration_Final2D/results/'; 
%     file_name = {'AllErr', 'Label_set', 'Displace_field', 'Uncertainty', 'Solution_field', 'Time', 'Landmarks'};
        file_name = {'AllErr', 'Label_set', 'Displace_field', 'Uncertainty', 'Time', 'Landmarks'};

    for trial_num = 1 : 2,
        for i = 1:6,
          load (num2str([path file_name{i} num2str(trial_num) '_new_set1.mat']));
          
        end
        
        AllErr_All = AllErr; Time_All = Time; Landmarks_All = Landmarks;
%         Warped_vol_All = Warped_vol; Diff_vol_All = Diff_vol; Edge_vol_All = Edge_vol; 
        Uncertainty_All = Uncertainty; 
%         Solution_field_All = Solution_field;
        Displace_field_All.Rand = Displace_field.Rand; Displace_field_All.SL = Displace_field.SL;
        Displace_field_All.AL = Displace_field.AL; Displace_field_All.AL_Base = Displace_field.AL_Base;
        
        Label_set_All.Rand = Label_set.Rand; Label_set_All.SL = Label_set.SL;
        Label_set_All.AL = Label_set.AL; Label_set_All.AL_Base = Label_set.AL_Base;
        for i = 1:6,
          load (num2str([path file_name{i} num2str(trial_num) '_new_set2.mat'])); 
        end
        AllErr_All.Rand(6:10,:) = AllErr.Rand(1:5,:); AllErr_All.AL(6:10,:) = AllErr.AL(1:5,:);AllErr_All.SL2(6:10,:) = AllErr.SL2(1:5,:);
        AllErr_All.SL40(6:10,:) = AllErr.SL40(1:5,:); AllErr_All.AL_baseline(6:10,:) = AllErr.AL_baseline(1:5,:);
        AllErr_All.SL10(6:10,:) = AllErr.SL10(1:5,:);
        
        Uncertainty_All.Rand(:,:,:,6:10) = Uncertainty.Rand(:,:,:,1:5); Uncertainty_All.AL(:,:,:,6:10) = Uncertainty.AL(:,:,:,1:5);
        Uncertainty_All.SL(:,:,:,6:10) = Uncertainty.SL(:,:,:,1:5); Uncertainty_All.AL_Base(:,:,:,6:10) = Uncertainty.AL_Base(:,:,:,1:5);

%         Solution_field_All.Rand(:,:,:,:,6:10) = Solution_field.Rand(:,:,:,:,1:5); Solution_field_All.AL(:,:,:,:,6:10) = Solution_field.AL(:,:,:,:,1:5);
%         Solution_field_All.SL(:,:,:,:,6:10) = Solution_field.SL(:,:,:,:,1:5); Solution_field_All.AL_Base(:,:,:,:,6:10) = Solution_field.AL_Base(:,:,:,:,1:5);

        Displace_field_All.Rand(:,:,6:10) = Displace_field.Rand(:,:,1:5); Displace_field_All.SL(:,:,6:10) = Displace_field.SL(:,:,1:5);
        Displace_field_All.AL(:,:,6:10) = Displace_field.AL(:,:,1:5); Displace_field_All.AL_Base(:,:,6:10) = Displace_field.AL_Base(:,:,1:5);
        
        Label_set_All.Rand(:,:,6:10) = Label_set.Rand(:,:,1:5); Label_set_All.SL(:,:,6:10) = Label_set.SL(:,:,1:5);
        Label_set_All.AL(:,:,6:10) = Label_set.AL(:,:,1:5); Label_set_All.AL_Base(:,:,6:10) = Label_set.AL_Base(:,:,1:5);
        
        Time_All.Rand(6:10) = Time.Rand(1:5); Time_All.AL(6:10) = Time.AL(1:5);
        Time_All.SL(6:10) = Time.SL(1:5); Time_All.Base(6:10) = Time.Base(1:5);
        
        Landmarks_All.slands_Rand(:,:,6:10) = Landmarks.slands_Rand(:,:,1:5); 
        Landmarks_All.tlands_Rand(:,:,6:10) = Landmarks.tlands_Rand(:,:,1:5); 
        Landmarks_All.slands_AL(:,:,6:10) = Landmarks.slands_AL(:,:,1:5); 
        Landmarks_All.tlands_AL(:,:,6:10) = Landmarks.tlands_AL(:,:,1:5); 
        Landmarks_All.slands_SL(:,:,6:10) = Landmarks.slands_SL(:,:,1:5); 
        Landmarks_All.tlands_SL(:,:,6:10) = Landmarks.tlands_SL(:,:,1:5); 
        Landmarks_All.slands_AL_Base(:,:,6:10) = Landmarks.slands_AL_Base(:,:,1:5); 
        Landmarks_All.tlands_AL_Base(:,:,6:10) = Landmarks.tlands_AL_Base(:,:,1:5); 

        clear AllErr; clear Uncertsinty; clear Time; clear Landmarks;
        clear Label_set; clear Displace_field;
        
        AllErr = AllErr_All; Uncertainty = Uncertainty_All; Time = Time_All;
        Landmarks = Landmarks_All; Displace_field = Displace_field_All; Label_set = Label_set_All;
        
        clear AllErr_All; clear Uncertsinty_All; clear Time_All; clear Landmarks_All;
        clear Label_set_All; clear Displace_field_All;
        for i = 1:6,
            save (num2str([path2 file_name{i} num2str(trial_num) '_all5.mat']), file_name{i}); 
        end
    end
    for trial_num = 1:2,
        for i = 1:6,
              load (num2str([path file_name{i} num2str(trial_num) '_all5.mat']));
        end
        
        load(['../data/Precomputed/trial' num2str(trial_num) '_new.mat']);
        load(['../data/Precomputed/trial' num2str(trial_num) '_large_warp.mat']);

%         number = 1;
%         path = 'H:/students/tlotfima/RWRegistration_Final2D/results/';
%         load ([path 'AllErr' num2str(number) '_new_set2']);
%         AllErr_all.SL(1:5,:) = AllErr.SL(21:25,:);
%         AllErr_all.AL(1:5,:) = AllErr.AL(21:25,:);
%         AllErr_all.AL_baseline(1:5,:) = AllErr.AL_baseline(21:25,:);
%         AllErr_all.Rand(1:5,:) = AllErr.Rand(21:25,:);
%         load ([path 'AllErr' num2str(number) '_new_set1']);
%         AllErr_all.SL(6:10,:) = AllErr.SL(1:5,:);
%         AllErr_all.AL(6:10,:) = AllErr.AL(1:5,:);
%         AllErr_all.AL_baseline(6:10,:) = AllErr.AL_baseline(1:5,:);
%         AllErr_all.Rand(6:10,:) = AllErr.Rand(1:5,:);
%         clear AllErr;
        AllErr = AllErr_all;
        
        figure,
        errorbar( mean(sqz(AllErr.Rand)), std(sqz(AllErr.Rand)), '-or' ), hold on
        errorbar( mean(sqz(AllErr.AL)), std(sqz(AllErr.AL)), '-*g' ), hold on
        errorbar( mean(sqz(AllErr.SL40)), std(sqz(AllErr.SL40)), '-.vm' ), hold on
    %     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
        plot( mean(sqz(repmat(AllErr.AL_baseline(:,2),[1 11]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
%         error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
        error_measure = sprintf(['Field Warping MSE Error', '\n', 'dataset1 with 6%% warping ', 'without noise' '\n' 'using gradient-based similarity metric']);
        title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
        legend('Random', 'AL', 'SL', 'AL Baseline', 'Location', 'SouthEastOutside'),

        
        
        figure,
        errorbar( mean(sqz(AllErr.SL2)), std(sqz(AllErr.SL2)), '-.vr' ), hold on
        errorbar( mean(sqz(AllErr.SL10)), std(sqz(AllErr.SL10)), '-.vg' ), hold on
        errorbar( mean(sqz(AllErr.SL40)), std(sqz(AllErr.SL40)), '-.vm' ), hold on
    %     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
%             error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 20 seeds totally without noise']);
%         error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'without noise']);
        error_measure = sprintf(['Field Warping MSE Error', '\n', 'dataset1 with 6%% warping ', 'without noise' '\n' 'using gradient-based similarity metric']);
        title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
        legend('SL2seeds', 'SL10seeds', 'SL40seeds', 'Location', 'SouthEastOutside'),

        
        img_number = 10;
        Warped_set = Calculate_Warped_set(src3D, Label_set, Displace_field, img_number);
        Diff_set = Calc_Similarity(Warped_set, tar3D(:,:,1:img_number));
        Edge_set = Calc_Edge(src3D);            

        iter1 = 10; iter2 = 2;
        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,iter1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Warped_set.AL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Active Learning Warped image');
        subplot 222, montage(reshape(mat2gray(sqz(Warped_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline Warped image');
        subplot 223, montage(reshape(mat2gray(sqz(Warped_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection Warped image');
        subplot 224, montage(reshape(mat2gray(sqz(Warped_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning Warped image');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Diff_set.AL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Active Learning image Difference');
        subplot 222, montage(reshape(mat2gray(sqz(Diff_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline image Difference');
        subplot 223, montage(reshape(mat2gray(sqz(Diff_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection image Difference');
        subplot 224, montage(reshape(mat2gray(sqz(Diff_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning image Difference');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        montage(reshape(mat2gray(sqz(Edge_set(:,:,1:10))),[szx  szy 1  szz]  )), title('Active Learning image Edge');
        subplot 222, montage(reshape(mat2gray(sqz(Edge_set.AL_Base(:,:,:,iter2))),[szx  szy 1  szz]  )), title('Active Learning Baseline image Edge');
        subplot 223, montage(reshape(mat2gray(sqz(Edge_set.Rand(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Random Selection image Edge');
        subplot 224, montage(reshape(mat2gray(sqz(Edge_set.SL(:,:,:,iter1))),[szx  szy 1  szz]  )), title('Self Learning image Edge');

        [szx szy szz] = size(sqz(Warped_set.Rand(:,:,:,1)));
        figure, 
        subplot 221, montage(reshape(mat2gray(sqz(Uncertainty.AL(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Active Learning Uncertainty Field');
        subplot 222, montage(reshape(mat2gray(sqz(Uncertainty.AL_Base(:,:,iter2,:))),[szx  szy 1  szz]  )), title('Active Learning Baseline Uncertainty Field');
        subplot 223, montage(reshape(mat2gray(sqz(Uncertainty.Rand(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Random Selection Uncertainty Field');
        subplot 224, montage(reshape(mat2gray(sqz(Uncertainty.SL(:,:,iter1,:))),[szx  szy 1  szz]  )), title('Self Learning Uncertainty Field');

        [szx szy szz] = size(src3D);
        figure, 
        subplot 121, montage(reshape(mat2gray(sqz(tar3D)),[szx  szy 1  szz]  )), title('Original Image Set');
        subplot 122, montage(reshape(mat2gray(sqz(src3D)),[szx  szy 1  szz]  )), title('Warped Image Set');

    end
end
