function Calc_Vis_dist(indx, indy, tar, src, dispvec, pbs, pbs_sprd, prob1, prob2, res, invTx, invTy, maxd)
    resolution = 2*maxd / round(sqrt(res));
    [gridX gridY] = ndgrid(-maxd:resolution:maxd);
    X = [gridX(:), gridY(:)];
    isz = sz(src);
    iszz = sz(pbs);
    nlabs = iszz(3);
    mask = zeros(sz(src));
    nlabs_size = sz(dispvec,1); data_mode = 2;
    indx = 120; indy = 171; indx = round(indx); indy = round(indy);
    mask(indx,indy) = 1; 
    pix1 = sub2ind(sz(src), indx, indy); 
    pbs = reshape(pbs, prod(isz), []); 
    mn = mnx(dispvec);
 %% Original Distribution, GDist   
    gam = 1; % 1; % 1.5; %2.2;
    G = 1 ./ (pdist2(dispvec, dispvec) .^ gam + 1);
    G = G ./ repmat(sum(G,2), 1, nlabs);
    pbs_aft = pbs * G;
   
    prob1 = sqz(pbs(pix1,1:end));
    prob2 = sqz(pbs_aft(pix1,1:end));
    prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
    prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);
    
    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
    hold on, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
    title('Original Distribution', 'FontSize', 45); 
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,
    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
    hold on, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]); 
    title('Single Gaussian', 'FontSize', 45); 
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,                
%%
    prob1 = sqz(pbs(pix1,1:end));
    [indval indmax] = max(prob1);
    [indval indmin] = min(prob1);    
    prob3 = prob1; % (end:-1:1);
    [indval indmax] = max(prob1);
    [indval indmin] = min(prob1);
    prob3(indmax-20:indmax+20) = prob1(indmax-20:indmax+20);
    prob3(indmax-83:indmax-43) = prob1(indmax-20:indmax+20);
    prob3 = prob3 ./ sum(prob3);
    prob4 = prob3;
    prob4(indmax-83:indmax-43) = prob3(end-160:end-120);
    prob4(end-160:end-120) = prob3(indmax-83:indmax-43);
    prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
    prob3 = reshape(prob3, [size(gridX,1) size(gridX,2)]);
    prob4 = reshape(prob4, [size(gridX,1) size(gridX,2)]);

    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
    ind = find(prob3 == max(prob3(:)));
    ind(2) = ind(1) + 3;
    hold on, quiver(zeros(2,1),zeros(2,1),dispvec(ind,2),dispvec(ind,1),1,'r', 'LineWidth', 4)
    set(gca,'FontSize', 45);
    hold on, surf(gridX, gridY, prob3), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
    title('Probability Distribution of Pixel X_1', 'FontSize', 45); 
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,

    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
    ind = find(prob4 == max(prob4(:)));
    hold on, quiver(zeros(2,1),zeros(2,1),dispvec(ind,2),dispvec(ind,1),1,'r', 'LineWidth', 4)
    set(gca,'FontSize', 45);
    hold on, surf(gridX, gridY, prob4), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
    title('Probability Distribution of Pixel X_2', 'FontSize', 45); 
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,
    
    %%
    figure, 
    subplot 121, quiver(zeros(round(sz(dispvec,1)/10),1),zeros(round(sz(dispvec,1)/10),1),dispvec(1:10:end,2), dispvec(1:10:end,1), 1,'k', 'LineWidth', 2),
    title('Prob. Dist., Pixel X_1','FontSize', 35), set(gca,'FontSize', 35); 
    ind = find(prob3 == max(prob3(:)));
    ind(2) = ind(1) + 3;
    hold on, quiver(zeros(2,1),zeros(2,1),dispvec(ind,2),dispvec(ind,1),1,'r', 'LineWidth', 4)
    set(gca,'FontSize', 35); 
    
%     subplot 122, quiverR(zeros(sz(dispvec,1),1),zeros(sz(dispvec,1),1),dispvec(:,2), dispvec(:,1), 10, 1,'k'),
    subplot 122, quiver(zeros(round(sz(dispvec,1)/10),1),zeros(round(sz(dispvec,1)/10),1),dispvec(1:10:end,2), dispvec(1:10:end,1), 1,'k', 'LineWidth', 2),
    title('Prob. Dist., Pixel X_2','FontSize', 35), set(gca,'FontSize', 35); 
    ind = find(prob4 == max(prob4(:)));
    hold on, quiver(zeros(2,1),zeros(2,1),dispvec(ind,2),dispvec(ind,1),1,'r', 'LineWidth', 4)
    set(gca,'FontSize', 35); 
%%    
      
    dSx = indx + invTx(pix1);
    dSy = indy + invTy(pix1);

    %% Added Part: GMM   
    iter1 = 5000; %gmdistribution iterations
    opts1 = statset( 'Display', 'final', 'MaxIter', iter1, 'TolTypeFun', 'rel', 'TolFun', 1e-6, 'TolTypeX', 'rel', 'TolX', 1e-6, 'OutputFcn', @emiter );
    MoGk = 25;
    opt.epsilon = 1e-3; epsilon = .5; opt.accuracy = 3;
    pbs = reshape(pbs, prod(sz(src)), []);
    dispvec_rep = replicate_vect_pbs(dispvec, pbs, opt.accuracy, pix1);
    dispvec_rep = dispvec_rep';

    S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
    [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
    S.mu = ([x(:), y(:)]);
    [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
    sig = D(1,2)/(6);

    S.Sigma = repmat([sig 0; 0 sig], [1 1 size(S.mu,1)]);
    S.PComponents(1:size(S.mu,1)) = 1/size(S.mu,1);


    obj = gmdistribution.fit(dispvec,size(S.mu,1),'Regularize',opt.epsilon, 'Options', opts1, 'Start', S);

    [X1,X2] = meshgrid(linspace(mn(1)-epsilon,mn(2)+epsilon,529)',linspace(mn(1)-epsilon,mn(2)+epsilon,529)');

    X = [X1(:) X2(:)]; 
    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
    hold on, ezsurf(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]), title('Gaussian Mixture Model', 'FontSize', 45);
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45);% 
%% KDE test 
    % dockf;
    clear S;
    MoGk = size(dispvec,1); 
%     MoGk = 49;
    [xg yg] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
    S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
    S.mu = dispvec;
%     [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
%     S.mu = ([x(:), y(:)]);
    [IDX D] = knnsearch(S.mu, S.mu, 'k', 2,'distance','euclidean'); %'minkowski','p',5); );            
    sig = D(1,2)/(4);
    S.Sigma(:,:,1:MoGk) = repmat([sig 0; 0 sig], [1 1 MoGk]);
    obj_kde = gmdistribution(S.mu, S.Sigma, pbs(pix1,:));
    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
    ezsurf(@(xg,yg)pdf(obj_kde,[xg yg]),[mn(1)-epsilon mn(2)+epsilon],[mn(1)-epsilon mn(2)+epsilon]), title('Kernel Density Estimation', 'FontSize', 45);
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
    set(gca,'FontSize', 45);% 
%     set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);   

%% Added Part: Knn
    resolution = 2*maxd / round(sqrt(res));
    [gridX gridY] = ndgrid(-maxd:resolution:maxd);
    [gridX1 gridY1] = ndgrid(-maxd:resolution+.02:maxd);
    [gridX2 gridY2] = ndgrid(-maxd:resolution+.3:maxd);
    a = randperm(nlabs);
    prob1 = sqz(pbs(pix1,1:end))';
    prob1(a(1:23*23-22*22)) = [];
    prob2 = sqz(pbs(pix1,1:end))';    
    prob2(a(1:23*23-17*17)) = [];
    prob1 = reshape(prob1, [size(gridX1,1) size(gridX1,2)]);
    prob2 = reshape(prob2, [size(gridX2,1) size(gridX2,2)]);

    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
    surf(gridX1, gridY1, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]), title('KNN, High Sampling', 'FontSize', 45); 
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,
    
    figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
    surf(gridX2, gridY2, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]), title('KNN, Low Sampling', 'FontSize', 45);                
    xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob', 'FontSize', 45); 
    set(gca,'FontSize', 45); grid on,   
%% ExpEr & ExpErMAP
        Xp = reshape( pbs, prod(sz(src)), [] );
        a =( Xp .* ( Xp * pdist2(dispvec, dispvec) ) ); 
        aa = a(1,:)'; aa = aa./sum(aa);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];
     
        aa = reshape(aa, [size(gridX,1) size(gridX,2)]);
        figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
        surf(gridX, gridY, aa), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]), title('Error Expectation', 'FontSize', 45); 
        xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('ExpEr', 'FontSize', 45); 
        set(gca,'FontSize', 45);
               
         Disp = [dx(:) dy(:)];
         amap =  Xp .* pdist2(Disp, dispvec);
         aamap = amap(1,:)'; aamap = aamap./sum(aamap);
        resolution = 2*maxd / round(sqrt(res));
        [gridX gridY] = ndgrid(-maxd:resolution:maxd);
        X = [gridX(:), gridY(:)];    
        aamap = reshape(aamap, [size(gridX,1) size(gridX,2)]);
        figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
        surf(gridX, gridY, aamap), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]), title('Error Expectation MAP', 'FontSize', 45); 
        xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('ExpErMAP', 'FontSize', 45); 
        set(gca,'FontSize', 45);% set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); 
        
%%
        gam = 1;
        G = 1 ./ (pdist2(dispvec, dispvec) .^ gam + 1);
        G = G ./ repmat(sum(G,2), 1, nlabs);
        pbs_sprd = pbs * G;
        
        pix1 = sub2ind(sz(src), indx, indy); 
        pbs = reshape(pbs, prod(isz), []); 
        mn = mnx(dispvec);

                
        prob1 = sqz(pbs(pix1,1:end));
        prob2 = sqz(pbs_sprd(pix1,1:end));
        prob1 = reshape(prob1, [size(gridX,1) size(gridX,2)]);
        prob2 = reshape(prob2, [size(gridX,1) size(gridX,2)]);

        
        X = [gridX(:), gridY(:)];    
        figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); hold on,
        hold on, surf(gridX, gridY, prob1), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]);
        title('Original Distribution', 'FontSize', 45); 
        xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
        set(gca,'FontSize', 45); grid on,
    
        figure, quiver(zeros(sz(gridX(1:10:end)'),1),zeros(sz(gridX(1:10:end)'),1),gridX(1:10:end)', gridY(1:10:end)'); 
        hold on, surf(gridX, gridY, prob2), xlim([-maxd-.5 maxd+.5]), ylim([-maxd-.5 maxd+.5]); 
        title('Smoothed by apha = 1', 'FontSize', 45); 
        xlabel('x', 'FontSize', 45); ylabel('y', 'FontSize', 45); zlabel('Prob (p)', 'FontSize', 45); 
        set(gca,'FontSize', 45); grid on,                

        

end