function invD = invertDefField(D)
% function invD = invertDefField(D)
%
% Invert a deformation field using the approach in
%
%     M. Chen, W. Lu, Q. Chen, K. J Ruchala, and G. H Olivera. 
%     A simple fixed-point approach to invert a deformation 
%     field. Medical Physics, 35:81, 2008.
%
% Arguments:
%      D - The defornation field to invert
%
% Returns:
%      invD - The inverted deformation field
%
  
  imSz = size(D);
  invD = zeros(size(D));

  if numel(imSz) == 3,
    [yy xx] = meshgrid(1:imSz(2), 1:imSz(1));
  else
    [yy xx zz] = meshgrid(1:imSz(2), 1:imSz(1), 1:imSz(3));
  end;
    
  N = 250;
  tau = cumprod(imSz)*0.05;
  tau = tau(end);

  for i = 1:N,
    oldID = invD;
    if exist('zz') > 0,
      tmpX = xx + invD(:, :, :, 1);
      tmpY = yy + invD(:, :, :, 2);
    else
      tmpX = xx + invD(:, :, 1);
      tmpY = yy + invD(:, :, 2);
    end;

    tmpX(tmpX < 1) = 1; % This should not be necessary!
    tmpX(tmpX > imSz(1)) = imSz(1);
    tmpY(tmpY < 1) = 1;
    tmpY(tmpY > imSz(2)) = imSz(2);
        
    if exist('zz') > 0,
      tmpZ = zz + invD(:, :, :, 3);
      
      tmpZ(tmpZ < 1) = 1;
      tmpZ(tmpZ > imSz(3)) = imSz(3);

      invD(:,:,:,1) = -interp3(yy, xx, zz, D(:,:,:,1), tmpY, tmpX, tmpZ);
      invD(:,:,:,2) = -interp3(yy, xx, zz, D(:,:,:,2), tmpY, tmpX, tmpZ);
      invD(:,:,:,3) = -interp3(yy, xx, zz, D(:,:,:,3), tmpY, tmpX, tmpZ);
    else
      invD(:,:,1) = -interp2(yy, xx, D(:,:,1), tmpY, tmpX);
      invD(:,:,2) = -interp2(yy, xx, D(:,:,2), tmpY, tmpX);
    end;
    
    if sum(abs(oldID(:) - invD(:))) < tau,
      break;
    end;
  end;
  
  if i == N,
    disp([sum(abs(oldID(:) - invD(:))), tau]);
    disp('Inversion of deformation field failed!');
  end;
  