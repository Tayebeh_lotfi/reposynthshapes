function Visualize_MoG_KDE(dispvec, pbs, opt)
%     for pix = [100 200 300 400],
    mn = mnx(dispvec);
    epsilon = 1e-5; % 0.2;
    counter_gmm = 1; pix_len_gmm = 4;  pix_len_kde =2;
    Case = 8; 
    for pix = 1:8:57,
%         dockf, 
%         for i = 1:size(dispvec,1),
%             hold on, quiver(zeros(1,1), zeros(1,1), dispvec(i,2),  dispvec(i,1), round(1000*pbs(pix,i)), 'color', rand(3,1) ), title(['Weighted Label set, pixel: ', num2str(pix)]);
%         end
        pix1 = pix;
        if(pix1 ~= 1), 
            pix1 = pix - 1; 
        else
            Case = 8; 
        end
        dispvec_rep = replicate_vect_pbs(dispvec, pbs, opt.accuracy, pix1);
        dispvec_rep = dispvec_rep';
          
%         dockf,  
%         for i = 1:size(dispvec_rep,1),
%             hold on, quiverR(zeros(1,1), zeros(1,1), dispvec_rep(i,2),  dispvec_rep(i,1), 1, 'color', rand(3,1) ), title(['Weighted Label set, pixel: ', num2str(pix)]);
%         end
        clear S;
        for MoGk = [4,9,25], %[3 5 9 13 17 20],
            opts1 = statset( 'Display', 'final', 'MaxIter', opt.iter1, 'TolTypeFun', 'rel', 'TolFun', 1e-10, 'TolTypeX', 'rel', 'TolX', 1e-10 );
%             step = round((size(dispvec,1)-1)/(MoGk-1));
            
%% Kmeans            
%             opts2 = statset( 'Display', 'final', 'MaxIter', opt.iter2, 'TolTypeFun', 'rel', 'TolFun', 1e-5, 'TolTypeX', 'rel', 'TolX', 1e-5 );
%            [Disp_idx Disp] = kmeans(dispvec, MoGk, 'Options', opts2);
%            figure, plot(Disp(:,2), Disp(:,1), 'r*');
%%
%             [IDX D] = knnsearch(Disp, Disp, 'k', MoGk,'distance','euclidean'); %'minkowski','p',5); );            
%             sig = sum(D(:))/(5*MoGk*MoGk);
            S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
%             S.mu = Disp;
%             switch(MoGk)
%                 case {2, 3}
%                     S.mu = dispvec(1:step:end,:);
%                 case 5
%                     S.mu = [-2 -2; -2 2; 0 0; 2 -2; 2 2];
%                 case 9
%                     S.mu = [-2 -2; -2 0; 0 -2; 0 0; 0 2; 2 0; -2 2; 2 -2; 2 2];
%                 case 25
%                     S.mu = [-2 -2; -2 -1; -2 0; -2 1; -2 2; -1 -2; -1 -1; -1 0; -1 1; -1 2; 0 -2; ...
%                         0 -1; 0 0; 0 1; 0 2; 1 -2; 1 -1; 1 0; 1 1; 1 2; 2 -2; 2 -1; 2 0; 2 1; 2 2];
%             end
%             for MoGk = [4,9,25],
                [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
                S.mu = ([x(:), y(:)]);
                [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
                sig = D(1,2)/(6);
            
            S.Sigma = repmat([sig 0; 0 sig], [1 1 size(S.mu,1)]);
            S.PComponents(1:size(S.mu,1)) = 1/size(S.mu,1);
%             figure, plot(S.mu(:,2), S.mu(:,1), 'r*'),xlim([mn(1) mn(2)]), ylim([mn(1) mn(2)])
            obj = gmdistribution.fit(dispvec_rep,size(S.mu,1),'Regularize',opt.epsilon, 'Options', opts1, 'Start', S);
%             figure, plot(obj.mu(:,2), obj.mu(:,1), 'r*'),xlim([mn(1) mn(2)]), ylim([mn(1) mn(2)])
            [X1,X2] = meshgrid(linspace(mn(1)-epsilon,mn(2)+epsilon,25)',linspace(mn(1)-epsilon,mn(2)+epsilon,25)');
            X = [X1(:) X2(:)];
            figure(1),
            subplot(pix_len_gmm, 6, (Case-1)*3+counter_gmm),
            for i = 1:MoGk,
                p = mvnpdf(X,S.mu(i,:),S.Sigma(:,:,i));
                hold on, surf(X1,X2,reshape(p,25,25)), title(['Case', num2str(Case), ', ' num2str(MoGk), 'GMM'], 'FontSize', 25),
                xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]); 
                set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);
            end
%             subplot(2,2,2),
%             for i = 1:MoGk,
%                 p = mvnpdf(X,obj.mu(i,:),obj.Sigma(:,:,i));
%                 hold on, surf(X1,X2,reshape(p,25,25)), title(['Result of gmdist.fit, pixel: ', num2str(pix), ', ' num2str(MoGk), ' Gaussians']),
%                 xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]);
%            end

            %subplot(2,2,3), ezcontour(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]), title(['MoG fit, pixel: ', num2str(pix), ', ' num2str(MoGk), ' Gaussians']),
            %xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]);
            figure(2),
            subplot(pix_len_gmm, 6, (Case-1)*3+counter_gmm), ezsurf(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]), title(['Case', num2str(Case), ', ' num2str(MoGk), 'GMM'], 'FontSize', 25), %subplot(1,2,2), 
             set(gca,'ZTick',[]); xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]); 
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            counter_gmm = counter_gmm + 1;
        end
        counter_gmm = 1;
        clear S; 
%% KDE test 
        figure(3),
        clear S;
        MoGk = size(dispvec,1);
        S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
        S.mu = dispvec;
        [IDX D] = knnsearch(S.mu, S.mu, 'k', 2,'distance','euclidean'); %'minkowski','p',5); );            
        sig = D(1,2)/(6);
        S.Sigma(:,:,1:size(dispvec,1)) = repmat([sig 0; 0 sig], [1 1 size(dispvec,1)]);
        obj_kde = gmdistribution(S.mu, S.Sigma, pbs(pix1,:));
%         subplot(pix_len_kde, 4, counter_kde), ezcontour(@(x,y)pdf(obj_kde,[x y]),[mn(1)-epsilon mn(2)+epsilon],[mn(1)-epsilon mn(2)+epsilon]), title(['KDE fit, pxl: ', num2str(pix) ' sigma(1,1) = ' num2str(i*10^-2)], 'FontSize', 25);
%         set(gca,'YTick',[]); set(gca,'XTick',[]);
        subplot(pix_len_kde, 4, Case), ezsurf(@(x,y)pdf(obj_kde,[x y]),[mn(1)-epsilon mn(2)+epsilon],[mn(1)-epsilon mn(2)+epsilon]), title(['Case', num2str(Case)], 'FontSize', 25); % ' sigma(1,1) = ' num2str(i*10^-2)
        set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);
%         counter_kde = counter_kde + 1; 
        if (pix1 == 1), Case = 0; end
        Case = Case + 1;
    end    
%    
%     for i = 1:size(dispvec,1),
%             hold on, quiver(zeros(1,1), zeros(1,1), dispvec(i,2),  dispvec(i,1), 1, 'color', rand(3,1) ), title('Label set');
%     end
%     
%     for n=1:size(pbs,1), 
%         covw(:,:,n) = ( ( repmat(pbs(n,:),2,1) .* (data(:,:,n) - datam(:,:,n))' ) * (data(:,:,n) - datam(:,:,n)) ) ./ ( 1 - sum(pbs(n,:) .^ 2) );
%         s(n) = det(covw(:,:,n));
%     end
%     plotyy(1:56, s(1:56), 1:56, diff(pbs(1:57,25)))
%     plotyy(1:24, s(1:24), 1:24, diff(pbs(1:25,25)))
%     mode = {'One preff', 'Two adjacent pref', 'Two far pref', 'Two farthest pref' 'Four pref'};
%     for i = 1:5,
%         figure, plot( s(eps_len*(i-1)+2:eps_len*i+1)), title(mode{i});
%     end
% %     for n = 130:150,
%     for i = 1:5,
%         mean_data = sqz(datam(1,:,eps_len*(i-1)+2:eps_len*i+1))';
% %         [mean_val mean_ind] = sort( mean_data(:,2) );
%         figure,
%         for j = 1:eps_len,
% %             hold off, 
%             plot(mean_data(j,2), mean_data(j,1), 'r*'), title([mode{i} ' epsilon = ' num2str(epsilon(j))]);
%             xlim([mn(1) mn(2)]), ylim([mn(1) mn(2)])
%             point_pos_str = sprintf(['x= ' num2str(mean_data(j,1)) '\n' 'y= ' num2str(mean_data(j,2))]);
% %             mTextBoxPosition = [mean_data(j,2) mean_data(j,1)];
%             text(mean_data(j,2), mean_data(j,1), point_pos_str);
%             if i==1,
%                 hold on, plot(dispvec(end,2), dispvec(end,1), 'bo');
%             else
%                 if i==2,
%                     hold on, plot(dispvec(end-1:end,2), dispvec(end-1:end,1), 'bo');
%                 else 
%                     if i==3,
%                         hold on, plot(dispvec(end,2), dispvec(end,1), 'bo');
%                         hold on, plot(dispvec(end-4,2), dispvec(end-4,1), 'bo');
%                     else 
%                         if i==4,
%                             hold on, plot(dispvec(end,2), dispvec(end,1), 'bo');
%                             hold on, plot(dispvec(1,2), dispvec(1,1), 'bo');
%                         else 
%                             if i==5,
%                                 hold on, plot(dispvec(end,2), dispvec(end,1), 'bo');
%                                 hold on, plot(dispvec(end-4,2), dispvec(end-4,1), 'bo');
%                                 hold on, plot(dispvec(5,2), dispvec(5,1), 'bo');
%                                 hold on, plot(dispvec(1,2), dispvec(1,1), 'bo'); 
%                             end
%                         end
%                     end
%                 end
%             end
% %             mTextBoxPosition = get(mTextBox,'Position');
% %             pause()
%             
%         end
%     end
% %     end
%     for n = 1:151, find(diff(datam(1:25,:,n))~=0), end
% %     figure, plot(s(:))
end
