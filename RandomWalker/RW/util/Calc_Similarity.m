function similarity = Calc_Similarity(I1, I2, simil_measure)
    switch (simil_measure)
        case 1
            ops.datacost = 'MDZI-GRAD';
        case 2
            ops.datacost = 'gNCC';
        case 3
            ops.datacost = 'SD';
        case 4
            ops.datacost = 'Mind2';
        case 5
            ops.datacost = 'SDPatch';
        case 6
            ops.datacost = 'GRADIENTMAG';      
    end
    if ~isempty(ops)
        if isfield( ops, 'datacost' );
            datacost = ops.datacost;
        end
        if isfield( ops, 'mask' );
            mask = ops.mask; 
        end
    end
    if isfield( ops, 'metric_npix' )
        npix = ops.metric_npix;
    else
        npix = 3; 
    end
    
    if  strcmp(datacost, 'Mind1') %I2 is src, I1 is tar
        %%
        rad = 1;
        mind_tar=MIND_descriptor2d(I1,rad);
        mind_src=MIND_descriptor2d(I2,rad);
        similarity =sqrt( sum( (mind_tar - mind_src) .^2, 3) );
    elseif  strcmp(datacost, 'Mind2') %I2 is src, I1 is tar
        %%
        rad = 2;
        mind_tar=MIND_descriptor2d(I1,rad);
        mind_src=MIND_descriptor2d(I2,rad);
        similarity =sqrt( sum( (mind_tar - mind_src) .^2, 3) );
    elseif strcmp(datacost, 'Mind3') %I2 is src, I1 is tar
        %%
        rad = 3;
        mind_tar=MIND_descriptor2d(I1,rad);
        mind_src=MIND_descriptor2d(I2,rad);
        similarity =sqrt( sum( (mind_tar - mind_src) .^2, 3) );
    elseif  strcmp(datacost, 'SD')
        %%
        similarity =( I2 -  I1 ).^2;    
    elseif strcmp(datacost, 'SDPatch')
        %%
        n = 5;
        h = fspecial('average', [n n]);  
        I2 = imfilter(I2, h);
        similarity =( I2 -  imfilter(I1, h) ).^2;    
    elseif strcmp(datacost, 'GRADIENTMAG')
        %%
        [gx gy]=gradient( I1 );
        I1= sqrt( gx.^2 + gy.^2);
        [gx gy]=gradient( I2 );
        I2= sqrt( gx.^2 + gy.^2);
        similarity =( I2 -  I1 ).^2;    
    elseif strcmp(datacost, 'MDZI-GRAD')
        %%
        [gx1 gy1]=gradient( I1 );
        [gx2 gy2]=gradient( I2 );
        similarity =1- (gx1 .* gx2 + gy1.*gy2 ) ./  sqrt( gx1.* gx1 + gy1.* gy1 + 1e-5) ./ sqrt( gx2.* gx2 + gy2.* gy2 + 1e-5) ;
    elseif strcmp(datacost, 'GRADIENT')
        %%
        [gx gy]=gradient( I1 );
        [gx2 gy2]=gradient( I2 );
        similarity = sqrt( (gx2 -  gx ).^2 + (gy - gy2).^2 );    
    elseif  strcmp(datacost, 'NCC') ||  strcmp(datacost, 'gNCC') 
            %%
        if strcmp(datacost, 'gNCC') 
            [gx gy]=gradient( I1 );
            I1= sqrt( gx.^2 + gy.^2);

            [gx gy]=gradient( I2 );
            I2= sqrt( gx.^2 + gy.^2);
        end
        similarity =  ( - I2 .* I1 );    
    elseif strcmp(datacost, 'GRAD')    
        I1=double(I1);
        I2=double(I2);
        if isfield( ops, 'metric_npix' )
            npix = ops.metric_npix;
        else
            npix = 3; 
        end
        I1= smooth(I1 , npix );
        I2= smooth(I2 , npix );
        [gx1 gy1]= get_gradient ( I1 );
        [gx2 gy2]= get_gradient ( I2 );          
        similarity = ( gx1 .* gx2 + gy1 .* gy2 );   
    end
end       