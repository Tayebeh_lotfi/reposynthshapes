function landmarks = pick_points_old(landmarks, candidates, ops, Values)
%     alpha=0.1;
    nMax = ops.num_seed;
    land_length = size(landmarks,2);
    for i = 1: nMax
        Dist = Calc_Dist(landmarks,candidates);
        [indx ~] = find(Dist == 0);
        candidates(:,indx) = [];
        Dist(indx,:) = [];
        candidate_inds = sub2ind( size(Values), candidates(1,:), candidates(2,:));
%         Dist = Calc_Dist(landmarks,candidates);
        max_dist = max(Dist(:));
        cand_land_Dist = resc(sum(Dist, 2) ./ max_dist);
        cost = ops.alpha .* (Values(candidate_inds)/abs(max(Values(:))))' + (1-ops.alpha) .* cand_land_Dist; %sum(Dist, 2) ./ max_dist; ( (sum(Dist(:))/2 + sum(diag(Dist))/2)); % * max_dist );
%         if(ops.Mode == 2 | ops.Mode == 4) % AL or AL_Base
            [val ind] = max(cost); 
%         else
%             if (ops.Mode == 3) % SL
%             [val ind] = min(cost); 
%             end
%         end
        landmarks(:,land_length+i) = candidates(:,ind);
        candidates(:, ind) = [];
    end
% plot3(candidates(1,:), candidates(2,:), candidates(3,:), 'r.'), hold on, plot3(landmarks(1,1:2), landmarks(2,1:2), landmarks(3,1:2), 'g.'), 
% hold on, plot3(landmarks(1,3:end), landmarks(2,3:end), landmarks(3,3:end), 'b.'), 
% Values(inds)
% cost(n(end))
end