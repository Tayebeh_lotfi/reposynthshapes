function Precompute_Data_2D(trial_num)
% clc, clear all, close all
 
add_paths();
%% Getting two images, pre-setting landmarks
[ debug flag Max_Iterations step parameter1 parameter2 beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str magnitude dterm_mode resolution Unc_str] = initialize_parameters();  
[ src3D tar3D slands3D tlands3D Tx3D Ty3D invTx3D invTy3D segment3D src_segment3D] = Reading_Images(noise_sigma, noise_mean, str);

save( ['../data/Precomputed/trial' num2str(trial_num) str '.mat'], ... %_downsample
    'tar3D', 'src3D', 'Tx3D', 'Ty3D', 'invTx3D', 'invTy3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');
% save( ['/home/ghassan/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(trial_num) '_noise5_new.mat'], ... 
%     'tar3D', 'src3D', 'ground_src3D', 'Tx3D', 'Ty3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');
% save( ['/home/ghassan/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(trial_num) '_noise10_new.mat'], ... 
%     'tar3D', 'src3D', 'ground_src3D', 'Tx3D', 'Ty3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');
% save( ['H:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/trial' num2str(trial_num) str '_corner.mat'], ... 
%     'tar3D', 'src3D', 'ground_src3D', 'Tx3D', 'Ty3D', 'invTx3D', 'invTy3D', 'tlands3D', 'slands3D', 'segment3D', 'src_segment3D', 'noise_sigma', 'noise_mean');
