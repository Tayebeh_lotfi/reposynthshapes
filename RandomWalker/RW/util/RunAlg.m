function [Input, Output] = RunAlg(tar, gamma, dterm_mode, res, spacing, mag, Lbl_mod, simil_measure, alpha, beta, noise_sigma)
%%
        [src  Tx  Ty] = rBSPwarp( tar, [spacing.height spacing.width], mag );
        D(:,:,1) = Tx; D(:,:,2) = Ty;
        invD = invertDefField(D);
        invTx = invD(:,:,1); invTy = invD(:,:,2);
        src(isnan(src)) = 0;
        src = mat2gray(src);
        mask = src > 0.1;
        S = find(mask>0);
        tar = resc(tar);
        src = resc(imnoise( resc(src), 'gaussian', 0, rand*noise_sigma ));        
    %     tlands = tlands3D(:,:,img); slands = slands3D(:,:,img); 
        num_seed = 4; Max_Iterations = 5; reg_mod = 1;
%%
    tlands = []; slands = [];
    distx = 1; disty = 1;
    sze = size(tar);
    tlands(:,1) = [distx; disty]; 
    tlands(:,2) = [distx; sze(2)-disty]; 
    tlands(:,3) = [sze(1)-distx; disty]; 
    tlands(:,4) = [sze(1)-distx; sze(2)-disty];
    slands = tlands;
%% Build graph with 8 lattice
        isz = size(src);
        [points edges]=lattice( isz(1), isz(2), 0);
    %     Max_seeds = num_seed*Max_Iterations;
        nseeds = size(tlands,2);
    %     eps = 0.01;
%% Calculating the likelihood
        [ dispvec regterm dterm nlabs maxd ] = Calc_Likelihood(tar, src, invTx, invTy, nseeds, res, Lbl_mod, simil_measure);
        nseeds = size(tlands,2); 
        [dx_sprd dy_sprd dx dy pbs pbs_sprd dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands);
        Warp_Err = (sqrt(  (dx + invTx).^2 + (dy + invTy).^2 ));
%% Calculating Features
        [Input Output] = Extract_Features(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err);
%         else
%         end
end