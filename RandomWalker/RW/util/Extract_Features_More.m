function [Input Output] = Extract_Features_More(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err, Warp_Err_Mode)
%% Calculating Uncertainties
    Uncert_Mode = 1; % 0: KNN, 1: All, 2: KNN&KDE
    accuracy=3; epsilon_ = 10e-4; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    data_mode = 2; nlabs_size = 5;
    [Wcov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
%     [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
%     pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, data_mode);
%% Feature extraction
    cnt = 1;
    for simil_measure = 1:6,   
        similarity = Calc_Similarity(tar, dsrc, simil_measure);
%         dterm = sort(dterm,3);
%         temp = dterm(:,:,end-3:end);
%         for ii = 1:4,
%            tempp = temp(:,:,ii);
           Input(1:size(S), cnt) = similarity(S);
           cnt = cnt + 1;
%         end
    end
% %     rad = 1;
% %     I1 = MIND_descriptor2d(src,rad);
% %     I2 = MIND_descriptor2d(tar,rad);
% %     for j = 1:4*(rad+1),
% %         II = I1(:,:,j);
% %         Input(1:size(S), cnt+j-1) = II(S);
% %         II = I2(:,:,j);
% %         Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
% %     end
% %     cnt = cnt + j + 4*(rad+1);
    rad = 2;
    I1 = MIND_descriptor2d(src,rad);
    I2 = MIND_descriptor2d(tar,rad);
    for j = 1:4*(rad+1),
        II = I1(:,:,j);
        Input(1:size(S), cnt+j-1) = II(S);
        II = I2(:,:,j);
        Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
    end
    cnt = cnt + 2*4*(rad+1);
% %     rad = 3;
% %     I1 = MIND_descriptor2d(src,rad);
% %     I2 = MIND_descriptor2d(tar,rad);
% %     for j = 1:4*(rad+1),
% %         II = I1(:,:,j);
% %         Input(1:size(S), cnt+j-1) = II(S);
% %         II = I2(:,:,j);
% %         Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
% %     end
% %     cnt = cnt + j + 4*(rad+1);
% %     Input(1:size(S), cnt) = src(S);
% %     Input(1:size(S), cnt+1) = tar(S);
%     grads = gradient(src); gradt = gradient(tar);
%     Input(1:size(S), cnt) = grads(S);
%     Input(1:size(S), cnt+1) = gradt(S);
%     cnt = cnt+2;
    [gxx gxy] = gradient(dx);
    [gyx gyy] = gradient(dy);
    Smooth_Val = gxx .* gxy - gyx .* gyy;
    Input(1:size(S), cnt) = Smooth_Val(S);
%     cnt = 0;
    Input(1:size(S), cnt+1) = Unc_Shannon(S); 
    Input(1:size(S), cnt+2) = Unc_Shannon_Sprd(S);
    Input(1:size(S), cnt+3) = Wcov(S);
    Input(1:size(S), cnt+4) = SGauss(S);
%     Input(1:size(S), cnt+5) = randn(sz(S,1),1);
%     Input(1:size(S), cnt+5) = Warp_Err(S);
%     [all ~] = evalFld( cat(3, dx,dy), S );
%     X(1:size(S), cnt+5) = repmat(all.mea(1), [size(S) 1]);
%     X(1:size(S), cnt+6) = repmat(all.mea(2), [size(S) 1]);
%     X(1:size(S), cnt+7) = repmat(all.mea(3), [size(S) 1]);
%     
%     [all ~] = evalFld( cat(3, dx,dy), S, cat(3,ops.gx,ops.gy) );
%     X(1:size(S), cnt+5) = Unc_Kde(S);
%     X(1:size(S), cnt+6) = Unc_Knn(S);
    if nargin>12
%         Input(1:size(S), cnt+5) = randn(sz(S,1),1);
        Input(1:size(S), cnt+3) = Warp_Err(S);
    end 
    if nargin>11
       Output(1:size(S),1) = Warp_Err(S);
    else
        Output(1:size(S),1) = zeros(sz(S),1);
    end
end