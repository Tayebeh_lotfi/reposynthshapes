function [U1 U1_sp] = Calc_Uncertainty_Shannon(X, X_sp, mode)
    
    isz = size(X);
    nlabs = isz(end);
    epsilon = 0;
    ent = X .* log(X+epsilon);
    if (nargin<2)
        U1 = sum(ent,3)*(-1)/(log(nlabs+epsilon));
        U1_sp = U1;
    else
        ent_sp = X_sp .* log(X_sp+epsilon);
        ent(isnan(ent)) = 0;
        ent_sp(isnan(ent_sp)) = 0;
        if(mode == 1)
            U1 = sum(ent,2)*(-1)/(log(nlabs+epsilon));
            U1_sp = sum(ent_sp,2)*(-1)/(log(nlabs+epsilon));
        end
        if(mode == 2)
            U1 = sum(ent,3)*(-1)/(log(nlabs+epsilon));
            U1_sp = sum(ent_sp,3)*(-1)/(log(nlabs+epsilon));
        end
        if(mode == 3)
            U1 = sum(ent,4)*(-1)/(log(nlabs+epsilon));
            U1_sp = sum(ent_sp,4)*(-1)/(log(nlabs+epsilon));
        end
    end
end