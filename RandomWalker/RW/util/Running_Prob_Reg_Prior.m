function [dx_sprd dy_sprd dx dy pbs pbs_aft dsrc dsrc_sprd] = Running_Prob_Reg_Prior(src, dispvec, nlabs, edges, dterm, isz, alpha, beta, gamma, dterm_mode, slands, Mode, reg_iteration)
   
%% Laplacian matrix
if (nargin < 13)
    reg_iteration = 1;
end
if(reg_iteration ~=1 && Mode > 4)
    slands_new = slands;
    slands_new(:, 1:4) = [];
    weights = makeweights(edges,src(:),beta, isz, slands_new);
%     weights = makeweights(edges);
    L=laplacian(edges, weights, prod(isz));
else
    L=laplacian(edges);
end
%%  Solve!
%%     Reshape for Input to Solver; Note the "-" Sign. 
    clear data_prior, clear pbs, clear Prior_Mat,
    switch dterm_mode
        case 1
            data_prior = exp(- reshape( dterm, prod(isz), [] ));
        case 2
            data_prior = reshape((1-dterm), prod(isz), [] );
    end
    mn1 = min(data_prior,[],2);
    mn2 = max(data_prior,[],2);
    minmat = mn1(:,ones(1,nlabs));
    maxmat = mn2(:,ones(1,nlabs));
    data_prior = (data_prior - minmat) ./ (maxmat-minmat);
    data_prior(isnan(data_prior)) = 1;
    Prior_Mat = speye(size(L,1)); %0*L
    Prior_Vec = sum(data_prior,2);
    Prior_Mat(1:prod(isz)+1:end) = Prior_Vec;
    disp('Now solves the problem w/ RW...')
%     alpha = 5000;
    tic
%     pbs = data_prior ./ repmat(Prior_Vec, 1, nlabs);
    pbs = (alpha*L + Prior_Mat)\(data_prior);

%     pbs_aft = (alpha*2*L + Prior_Mat)\(data_prior);
    toc
%     gam = 1; % 1; % 1.5; %2.2;
%     G = 1 ./ (pdist2(dispvec, dispvec) .^ gam + 1);
%     G = G ./ repmat(sum(G,2), 1, nlabs);
%     pbs_aft = pbs * G;
    pbs_aft = pbs;   
%% calc most prob. solution    
    [~, q] = max(pbs');
    Label = reshape(q , isz(1), []);    
    [~, q_sp] = max(pbs_aft');
    Label_sp = reshape(q_sp , isz(1), []);     


        pbs = reshape(pbs,[ isz(1) isz(2) nlabs]);
        pbs_aft = reshape(pbs_aft,[ isz(1) isz(2) nlabs]);

% %     dockf; hist(sqz(pbs(120,100,:)), 30), title('before spreading labels');
% %     dockf; hist(sqz(pbs_sprd(120,100,:)), 30), title('after spreading labels');
% 
% %     data_priorn = reshape(data_priorn,[ isz(1) isz(2) nlabs]);
% %     data_prior = reshape(data_prior,[ isz(1) isz(2) nlabs]);
%%     Compute Displacement Vector Field
    dx_sprd = zeros(isz );
    dy_sprd = zeros(isz );
    dx = zeros(isz );
    dy = zeros(isz );    

    method='max';
    switch method 
        case 'weighted' % not a confirmed approach    
            for i=1:nlabs
                    dx = dx + pbs(:,:,i).*dispvec(i,1);
                    dy = dy + pbs(:,:,i).*dispvec(i,2);  
                    dx_sprd = dx_sprd + pbs_aft(:,:,i).*dispvec(i,1);
                    dy_sprd = dy_sprd + pbs_aft(:,:,i).*dispvec(i,2);
            end    
        case 'max'
                [dx dy]=label2field( Label, dispvec );
                [dx_sprd dy_sprd]=label2field( Label_sp, dispvec );  
        otherwise
            % come up w/ your own conversion scheme
    end

        [mx my] = ndgrid(1:isz(1), 1:isz(2)); 
        dsrc = interp2( my, mx, src, my + dy, mx + dx,  '*linear',0);     
        dsrc_sprd = interp2( my, mx, src, my + dy_sprd, mx + dx_sprd,  '*linear',0); 
end