function Final_Disp_Field = Calc_Displace_Field( src3D, Displace_field, Label_set, trial_num, max_iter, IMG_MAX_NUM ) 

%     path1 = 'H:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/';
%     path2 = 'H:/students/tlotfima/RWRegistration_Final2D/results/forty_images_smooth_1seed/'; %All_75_75_25/';
%     file_name = {'AllErr' 'Label_set' 'Displace_field' 'Time' 'Uncertainty' 'Landmarks'};
% %     trial_num = 1;
%     
%     load(num2str([path1 'trial' num2str(trial_num) '_large_warp_corner.mat']));
% 
%     for i = 1:5,
%       load (num2str([path2 file_name{i} num2str(trial_num) '_large_corner_all.mat']));         
%     end
%     IMG_MAX_NUM = size(Label_set.AL,3); %size(src3D,3);
    for iter = 1:max_iter,  
        for img_num = 1:IMG_MAX_NUM,
            Displacement_field.Rand(:,1) = Label_set.Rand(Displace_field.Rand(:,iter,img_num),1);
            Displacement_field.Rand(:,2) = Label_set.Rand(Displace_field.Rand(:,iter,img_num),2);
            Displacement_field.Rand = reshape(Displacement_field.Rand, [size(src3D,1), size(src3D,2), 2]);

            Displacement_field.RandG(:,1) = Label_set.RandG(Displace_field.Rand(:,iter,img_num),1);
            Displacement_field.RandG(:,2) = Label_set.RandG(Displace_field.Rand(:,iter,img_num),2);
            Displacement_field.RandG = reshape(Displacement_field.RandG, [size(src3D,1), size(src3D,2), 2]);

            Displacement_field.AL(:,1) = Label_set.AL(Displace_field.AL(:,iter,img_num),1);
            Displacement_field.AL(:,2) = Label_set.AL(Displace_field.AL(:,iter,img_num),2);
            Displacement_field.AL = reshape(Displacement_field.AL, [size(src3D,1), size(src3D,2), 2]);

            Displacement_field.SL_All(:,1) = Label_set.SL_All(Displace_field.SL_All(:,iter,img_num),1);
            Displacement_field.SL_All(:,2) = Label_set.SL_All(Displace_field.SL_All(:,iter,img_num),2);
            Displacement_field.SL_All = reshape(Displacement_field.SL_All, [size(src3D,1), size(src3D,2), 2]);

            Displacement_field.SL100(:,1) = Label_set.SL100(Displace_field.SL100(:,iter,img_num),1);
            Displacement_field.SL100(:,2) = Label_set.SL100(Displace_field.SL100(:,iter,img_num),2);
            Displacement_field.SL100 = reshape(Displacement_field.SL100, [size(src3D,1), size(src3D,2), 2]);

            Displacement_field.SL_Rand(:,1) = Label_set.SL_Rand(Displace_field.SL_Rand(:,iter,img_num),1);
            Displacement_field.SL_Rand(:,2) = Label_set.SL_Rand(Displace_field.SL_Rand(:,iter,img_num),2);
            Displacement_field.SL_Rand = reshape(Displacement_field.SL_Rand, [size(src3D,1), size(src3D,2), 2]);

            if iter < 3,  
                Displacement_field.AL_Base(:,1) = Label_set.AL_Base(Displace_field.AL_Base(:,iter,img_num),1);
                Displacement_field.AL_Base(:,2) = Label_set.AL_Base(Displace_field.AL_Base(:,iter,img_num),2);
                Displacement_field.AL_Base = reshape(Displacement_field.AL_Base, [size(src3D,1), size(src3D,2), 2]);
            else
                Displacement_field.AL_Base(:,1) = Label_set.AL_Base(Displace_field.AL_Base(:,2,img_num),1);
                Displacement_field.AL_Base(:,2) = Label_set.AL_Base(Displace_field.AL_Base(:,2,img_num),2);
                Displacement_field.AL_Base = reshape(Displacement_field.AL_Base, [size(src3D,1), size(src3D,2), 2]);
            end
            Final_Disp_Field(img_num,iter) = Displacement_field;
            clear Displacement_field;
        end
    end
end