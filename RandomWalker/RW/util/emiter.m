function emiter(mu, Sigma)
global count
% pix = 1;%     fin_obj = x.data;
MOVIE = 1;
if(MOVIE),
    writerObj1 = VideoWriter(['outmu' num2str(count) '.avi']);
    writerObj1.FrameRate = 2;
    open(writerObj1);
    writerObj2 = VideoWriter(['outdist' num2str(count) '.avi']);
    writerObj2.FrameRate = 2;
    open(writerObj2);
end
mn = [-2 2];
    c = jet(200);
    p = randperm(size(c,1));
    c = c(p,:);
    [X1,X2] = meshgrid(linspace(mn(1),mn(2),25)',linspace(mn(1),mn(2),25)');
    A = [X1(:) X2(:)];    
%     for pix = 1:8:57,
        FID1 = dockf;
            for i = 1:size(mu,3),
                for gauss = 1:9,
%                     figure{FID1};
                    subplot(3,3,gauss), 
                    hold on, scatter(sqz(mu(gauss,2,i)), sqz(mu(gauss,1,i)), 10, c(i), 'filled'),
                    title(['Initial structure, pixel: ', num2str(count), ', ' 'Gaussian ' num2str(gauss)]);
                    if (i~=size(mu,3))
                        X = [sqz(mu(gauss,1,i)) sqz(mu(gauss,1,i+1))];
                        Y = [sqz(mu(gauss,2,i)) sqz(mu(gauss,2,i+1))];
                        hold on, line(Y, X);
                    end
                    xlim([mn(1)-1 mn(2)+1]), ylim([mn(1)-1 mn(2)+1]);
                end
                
                if (MOVIE), pause(0.005); frame = getframe(gcf);       
                    writeVideo(writerObj1,frame);
%                 else
%                     pause;
                end
                
                
%                 pause(0.01)
            end 
            
            FID2 = dockf;
            for i = 1:size(mu,3),
                for gauss = 1:9,                    
%                     figure{FID2},
                    subplot(3,3,gauss), 
                    p = mvnpdf(A,sqz(mu(gauss,:,i)),Sigma(:,:,gauss,i)); 
                    hold on, surf(X1,X2,reshape(p,25,25)), title(['Initial structure, pixel: ', num2str(count), ', ' 'Gaussian ' num2str(gauss)]);
                    xlim([mn(1)-1 mn(2)+1]), ylim([mn(1)-1 mn(2)+1]);
                end
               if (MOVIE), pause(0.005); frame = getframe(gcf);       
                    writeVideo(writerObj2,frame);
%                 else
%                     pause;
                end 
            end  
%     end
if (MOVIE), close(writerObj1); end
if (MOVIE), close(writerObj2); end
count = count+1;
end


