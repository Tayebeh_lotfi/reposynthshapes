function [ debug flag Max_Iterations step beta alpha gamma noise_sigma noise_mean Mode_Str ...
    image_set_size num_seed savepath str magnitude dterm_mode resolution Unc_str] = initialize_parameters()
    debug = 0;% showing the results
    flag = 1; % showing a pixel labels with non unique cost at diff labels
    Max_Iterations= 4; 
    step = 1;
%     parameter1 = 0.5;%[0.36 0.5 1.5 2];% cert_nstd .36
%     parameter2 = .95; % 2;%[0.85 0.9 0.95]; % lambda .9  % 20000
    beta = 0.0005; %500; % 
    alpha = 5000; % .95; % 
    gamma = 0.75;
%     magnitude = 15;
    noise_sigma = 0.05; % 0.05 % 0; %  0; % 0.1
    noise_mean = 0;
    image_set_size = 5;
    resolution = 500; % 10; %
    Max_User_Interaction = 100; % 40
    dterm_mode = 2; %1:exp(-dterm), 2:1-dterm
    str = '_small'; % '_large'; % 
    if(strcmp(str,'_small'))
        magnitude = 10;
    else
        if (strcmp(str, '_large'))
            magnitude = 15;
        end
    end
    Unc_str = {'ShEnt' 'WProb' 'ExpEr' 'ExpErMAP' 'WCov' 'GDist' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian' ...
        'GMM Avg' 'GMM Discrete Gaussian' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian', ...
        'GMM Avg' 'GMM Discrete Gaussian' 'GMM Lower Bound Gaussian' 'GMM Upper Bound Gaussian', ...
            'GMM Avg' 'GMM Discrete Gaussian' 'KDE' 'KNN' 'Delta'};
    num_seed =Max_User_Interaction / Max_Iterations;
    Mode_Str = {'Random_Grid' 'AL' 'SL100seeds' 'SL200seeds' 'SL_Randseeds' 'Random' 'AL_Baseline' }; 
%    if strcmp( computer, 'GLNX')
%         root = '/cs/grad1/tlotfima';
          root = '/home/ghassan/students_less/tlotfima';

%    else
%         root = 'H:/students/tlotfima';
%         root = 'Y:/students/tlotfima';
%    end
  
   savepath = [root '/RWRegistration_Final2D/results/'];
end