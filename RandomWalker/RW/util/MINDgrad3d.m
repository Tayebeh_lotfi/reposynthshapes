function [Sx,Sy,Sz,S]=MINDgrad3d(mind1,mind2)
[m,n,o]=size(mind1);

A1=mean(abs(volshift(mind2,1,0,0)-mind1),3);
A2=mean(abs(volshift(mind2,-1,0,0)-mind1),3);
Sx=A1-A2;
A1=mean(abs(volshift(mind2,0,1,0)-mind1),3);
A2=mean(abs(volshift(mind2,0,-1,0)-mind1),3);
Sy=A1-A2;
A1=mean(abs(volshift(mind2,0,0,1)-mind1),3);
A2=mean(abs(volshift(mind2,0,0,-1)-mind1),3);
Sz=A1-A2;
S=mean(abs(mind2-mind1),3);

Sx(:,[1,n],:)=0;
Sy([1,m],:,:)=0;
Sz(:,:,[1,o])=0;

function vol1shift=volshift(vol1,x,y)

x=round(x);
y=round(y);

[m,n,o]=size(vol1);

vol1shift=zeros(size(vol1));

x1s=max(1,x+1);
x2s=min(n,n+x);

y1s=max(1,y+1);
y2s=min(m,m+y);

x1=max(1,-x+1);
x2=min(n,n-x);

y1=max(1,-y+1);
y2=min(m,m-y);


vol1shift(y1:y2,x1:x2,:)=vol1(y1s:y2s,x1s:x2s,:);
