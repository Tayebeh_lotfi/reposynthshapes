function [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Rand_Exper(src, dx, dy, src_segment, maxd, ops, slands, tlands, MCase)
    
    [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2) );
    mask1 = ( src > 0.1);
    mask2 = ( src_segment > 0);
    se = strel('disk',10);
    mask2 = imdilate(mask2,se);
    mask = mask1 .* mask2;
    start = 3;
    length = size(slands,2);
    pick_img = src .* mask;
    ten_perc = 1000;
    clear slands_new; clear tlands_new;
    clear slands_next1; clear tlands_next1;
    slands_next(1, :)  = round( (size(src, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
    slands_next(2, :)  = round( (size(src, 2) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
    cnt = 1; i = 1;
    while(1),
        if(pick_img(slands_next(1,i), slands_next(2,i)) > 0 && cnt <= ops.num_seed)
            slands_new(:,cnt) = slands_next(:, i); cnt = cnt+1;
        end
        i = i+1;
        if(cnt > ops.num_seed)
            break
        end
    end
    isz = size(src);
    inds = sub2ind(isz, slands_new(1,:), slands_new(2,:));
    if(MCase == 1)
        nx=dx(inds);
        ny=dy(inds);
    else
        nx=-dx(inds);
        ny=-dy(inds);
    end    
%     nx=dx(inds);
%     ny=dy(inds);

    tlands_new(1,:) =  mx( inds)-nx; %  +nx
    tlands_new(2,:) =  my( inds)-ny; %  
    length = size(slands,2);
    adding_length = size(slands_new,2);
    slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
    tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
%     tlands(1,1) = mx(inds(1))-nx(1);
%     tlands(2,1) = my(inds(1))-ny(1);
    nseeds = size(slands,2);
end

