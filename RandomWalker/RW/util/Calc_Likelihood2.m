function [ dispvec2 smthcost2 dterm2 nlabs2 ] = Calc_Likelihood2(tar, src, tlands, slands, nseeds)

% ops.datacost = 'gNCC';
% ops.datacost = 'SD';
ops.datacost = 'MIND';
[ dispvec2 smthcost2 dterm2 ] = MRFregCosts2( tar, src, tlands, slands, ops);

nlabs2 =size(dispvec2,1);

end
