function p = mixgauss_distribution(obj, opt)
%     obj.mu; obj.Sigma; obj.PComponents;
%     [x(1) x(2)] = ind2sub(isz, pix);
    X = obj.mu(opt.l,:);
    for class = 1:opt.MoGk,
        sig = obj.Sigma(:,:,class)+obj.Sigma(:,:,opt.l);
        scal(class) = 1 ./ (sqrt(2*pi*det(sig)));
%     scal(2) = 1 ./ (sqrt(2*pi*det(obj.Sigma(:,:,2))));
%     scal(3) = 1 ./ (sqrt(2*pi*det(obj.Sigma(:,:,3))));
        exp_comp(class) = obj.PComponents(class) .* ...
            exp( -(X - obj.mu(class,:)) * pinv(sig) * (X - obj.mu(class,:))');
%     exp_comp(2) = exp( -(x - obj.mu(2,:)) * pinv(obj.Sigma(:,:,2)) * (x - obj.mu(2,:))');
%     exp_comp(3) = exp( -(x - obj.mu(3,:)) * pinv(obj.Sigma(:,:,3)) * (x - obj.mu(3,:))');
    end
    p = -obj.PComponents(opt.l) .* log(sum(scal .* exp_comp));
end