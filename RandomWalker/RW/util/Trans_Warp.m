function [IWF TxW TyW] = Trans_Warp( I, mag, direct )
    IWF = zeros(size(I));
    switch direct
        case 1
            IWF(1:end-mag,:) = I(mag:end-1, :);
            TxW = ones(size(I)) * (-mag); TyW = zeros(size(I));
        case 2
            IWF(:, 1:end-mag) = I(:, mag:end-1);
            TxW = zeros(size(I)); TyW = ones(size(I)) * (-mag);
        case 3
            IWF(mag:end-1, :) = I(1:end-mag, :);
            TxW = ones(size(I)) * mag; TyW = zeros(size(I));
        case 4
            IWF(:, mag:end-1) = I(:, 1:end-mag);
            TxW = zeros(size(I)); TyW = ones(size(I)) * mag;
    end
end