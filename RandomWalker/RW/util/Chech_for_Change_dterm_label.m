ind = [slands(1,6)+2; slands(2,6)+2];
dockf; hist(sqz(dterm(ind(1), ind(2), :)));
% dockf;quiverR(0, 0, -dy(ind(1),ind(2)), -dx(ind(1),ind(2)), 1, 2, 'c' );

dockf;quiver(zeros(1),zeros(1),-dy(ind(1),ind(2)), -dx(ind(1),ind(2)))

img_num = 5;
iter = 4;
d1 = Final_Disp_Field(img_num,iter).SL_All;
d2 = Final_Disp_Field(img_num,iter+1).SL_All;
dx = d2(:,:,1) - d1(:,:,1);
dy = d2(:,:,2) - d1(:,:,2);
dockf;quiverR(zeros(size(d1(:,:,2))), zeros(size(d1(:,:,1))), -d1(:,:,2), -d1(:,:,1), 10, 2, 'c' );
dockf;quiverR(zeros(size(d2(:,:,2))), zeros(size(d2(:,:,1))), -d2(:,:,2), -d2(:,:,1), 10, 2, 'c' );
dockf;quiverR(zeros(size(dy)), zeros(size(dx)), -dy, -dx, 10, 2, 'c' ), title('change in labels SL\_All, iter 4');

%% Calculate correlation
src = src2D; invTx = invTx2D; invTy = invTy2D; tar = tar2D; dsrc = dsrc2D;
mask = src > 0.1;
total_err = (sqrt(  (dx + invTx).^2 + (dy+ invTy).^2 )); 
% total_err = reshape((sqrt(  (dx(mask>0) + invTx2D(mask>0)).^2 + (dy(mask>0) + invTy2D(mask>0)).^2 )), [isz(1) isz(2)]); 

idata(:,:,1) = uncertainty .* mask; %uncertainty .* mask;
idata(:,:,2) = total_err .* mask; %total_err .* mask;
idata1 = idata(:,:,1);
idata2 = idata(:,:,2);
b =3;%[23 23 8];%10; [ b b b ]
cc = blockproc(idata, [b b], @local_corr_fun);%, 'Useparallel', 1, 'PadPartialBlocks',1);%);%
local_correlation = cc(1:2:end,2:2:end);
Mut_Info = mutualinfo( idata1, idata2);

%% Display
close all,
dockf; imagesc(src), title('moved image');
dockf; imagesc(tar), title('original image');
dockf;quiverR(zeros(size(invTy)), zeros(size(invTx)), invTy, invTx, 2, 2, 'c' ), title('ground truth');
dockf;quiverR(zeros(size(dy)), zeros(size(dx)), -dy, -dx, 2, 2, 'c' ), title('warping field');
dockf; imagesc(total_err),  title('error'), cbar
dockf; imagesc(local_correlation), title('correlation'),cbar
dockf; imagesc(tar - dsrc), title('difference'), cbar
dockf; imagesc(uncertainty), title('uncertainty'), cbar



