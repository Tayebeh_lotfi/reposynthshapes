function [all he jd re epe ae nD] = evalFld( fld, roi, fld_ref)
% function [all JE HE RE EPE AE Norm] = evalFld( fld, roi, fld_ref)
%  
% Input: 
%       fld: 2d or 3d  field
%       Optional:
%           roi: indices to voxels of interst
%           fld_ref: refernce field to compare fld against
% 
% Output:
%       all: a structure containing these measures (lower better):
%           - Mean harmonic energy (used in, e.g. Paragios et al. Boosted metric ISBI 2011)
%           - Fraction of negative jac. deter. 
%           - Mean jac. deter.  (used in, e.g. Hernandez et al. "Diffeomorphic Demons")
%           - Mean relative err
%           - Mean angular error
%           - Mean end-point-error (Eucl distance)
% 
%       all.des={'MHE' 'FNJD' 'MJD' 'MRE' 'MAE' 'MEPE'}; 
%
%       JD: jac. det. map (<0 means locally non-invertible; if close to 1, imcompressible constraint is met)
%       HE: harmonic energy map
%       RE: relative err map
%       EPE: end-pt-err map
%       AE: angular err 
%       Norm: norm of fld_ref
% 
% Contact: lisat(at)sfu.ca

nD=[]; epe=[]; ae=[]; re=[]; je=[];

sz=size(fld);

n=numel( sz ); %number of dims
nd= sqrt(sum( fld.^2 ,n))+1e-5;

if n==3
isz=prod(sz(1:2));
elseif n==4
isz=prod(sz(1:3));
end
if nargin == 1 roi=[]; end

[jd he]=jdhe(fld);
if  isempty(roi) roi=1:isz; npix=isz; else  npix=numel(roi);end
    
all.mea(1)=mean(he(roi));
all.mea(2)=numel(find(jd(roi)<0))/ npix;
all.mea(3)= mean(jd(roi));
all.des={'MHE' 'FNJD'  'MJD'};
    
all2.angle=acosd(sum( fld.*fld, n) ./nd./nd );%dot prod divided by norms

    
if nargin>2
    nD= sqrt(sum( fld_ref.^2 ,n))+1e-5;   
    epe=sqrt(  sum( (fld - fld_ref).^2, n) );
    re = epe ./ nD;
    ae=acosd(sum( fld.*fld_ref, n) ./ nD ./nd );%dot prod divided by norms
if nargin>2
    all.mea(4)=mean(re(roi));
    all.mea(5)=mean(ae(roi));    
    all.mea(6)=mean(epe(roi));
    all.mea(7)=std(epe(roi));
    all.mea(8)=mean(nD(roi));

    all.des={'MHE' 'FNJD' 'MJD' 'MRE' 'MAE' 'MEPE' ,'SEPE' };
end

end

function    [d he]= jdhe(T, varargin )
% function    [d HE]= jdhe(T)
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/319522
% 
%  HE: harmonic energy
% 
%     [gxy gxx gxz] = gradient(T(:,:,:,1),.5);
%     [gyy gyx gyz] = gradient(T(:,:,:,2),.5);
%     [gzy gzx gzz] = gradient(T(:,:,:,3),.5);
    
%     d = gxx .* ( gyy .* gzz - gzy .* gyz) - ...
%         gxy .* (gyx .* gzz - gzx .* gyz) + gxz .* (gyx .* gzy - gzx .* gyy);
         

d = length(sz(T));
if nargin>1
    if d==3
        T=cat(4, T, varargin{1},varargin{2} );
    else
        T=cat(3, T, varargin{1});
    end
    d = len(sz(T));
end

if d==4
    [uxy,uxx,uxz] = gradient(T(:,:,:,1));
    [uyy,uyx,uyz] = gradient(T(:,:,:,2));
    [uzy,uzx,uzz] = gradient(T(:,:,:,3));
    d= (1+uxx) .*((1+uyy).*(1+uzz) - (uyz.*uzy)) - uxy .*((uyx.*(1+uzz)) - (uyz.*uzx)) + uxz .*((uyx.*uzy) - ((1+uyy).*uzx));

    if nargout==2
    he = uxy.^2+uxx.^2 + uxz.^2+ uyy.^2+uyx.^2+uyz.^2+uzy.^2+uzx.^2+uzz.^2; 
    end
 
elseif d==3
    [xy,xx] = gradient(T(:,:,1));
    [yy,yx] = gradient(T(:,:,2));
    d=  (1+xx).*(yy+1) - xy.*yx;
    if nargout==2
    he = xy.^2+xx.^2 + yy.^2+ yx.^2;
    end
    if 0
    %%
 
    
    end
    
end
    
    

