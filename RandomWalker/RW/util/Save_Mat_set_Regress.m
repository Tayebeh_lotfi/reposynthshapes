% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set_Regress( AllErr_Less, AllErr, savepath, Max_Iterations, num_seed, trial_num, set_num, str, MCase, noise_sigma)
%     file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Solution_field' 'Time' 'Landmarks'};
    file_name = {'AllErr_Less' 'AllErr' };    
%     for i = 1:2,%6
        if MCase == 1,
            save (num2str([savepath 'AL_SL/Regression/' file_name{1} num2str(set_num) str '_trial' num2str(trial_num) '_regress_without_unc' '_noise' num2str(noise_sigma*100) '.mat']), file_name{1}); 
            save (num2str([savepath 'AL_SL/Regression/' file_name{2} num2str(set_num) str '_trial' num2str(trial_num) '_regress_with_unc' '_noise' num2str(noise_sigma*100) '.mat']), file_name{2}); 
        else
            save (num2str([savepath 'AL_SL/Regression/' file_name{1} num2str(set_num) str '_trial' num2str(trial_num) '_regress_uncertainties' '_noise' num2str(noise_sigma*100) '.mat']), file_name{1}); 
            save (num2str([savepath 'AL_SL/Regression/' file_name{2} num2str(set_num) str '_trial' num2str(trial_num) '_regress_image_features' '_noise' num2str(noise_sigma*100) '.mat']), file_name{2}); 
        end
%     end          
end