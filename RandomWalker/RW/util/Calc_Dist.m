function Dist = Calc_Dist(landmarks1, landmarks2, ops)
    if (nargin < 3)
        lands1.X = landmarks1(1,:); lands1.Y = landmarks1(2,:); 
        lands2.X = landmarks2(1,:); lands2.Y = landmarks2(2,:); 
    else
        lands1.X = landmarks1.x; lands1.Y = landmarks1.y;
        lands2.X = landmarks2.x; lands2.Y = landmarks2.y;
    end

    N = size(lands1.X, 2);
    M = size(lands2.X, 2);
    distX = repmat(lands1.X, [M 1]) - repmat(lands2.X', [1 N]);
    distY = repmat(lands1.Y, [M 1]) - repmat(lands2.Y', [1 N]);
    Dist = sqrt(distX .^ 2 + distY .^ 2);  
%     max_dist = max(dist(:));
end