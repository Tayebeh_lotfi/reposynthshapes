function [zVec_new k_start] = replicate_vect_pbs(dispvec, pbs, accuracy, pix)

    prob_l = sqz(pbs(pix,:));
    d=accuracy;
    r=round(prob_l*10^d);
    k_start = max(r)+1;
    cr=[0 cumsum(r)];
    s=sum(r);
    zVec_new = zeros(size(dispvec,2),s);
    % xy=rand(2,5)
    for k = 1:length(prob_l), 
        zVec_new(:,1+cr(k):cr(k+1))=repmat(dispvec(k,:)',[1,r(k)]); 
    end
    
end
