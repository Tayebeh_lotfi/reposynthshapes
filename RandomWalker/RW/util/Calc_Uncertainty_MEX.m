% function Calc_Uncertainty_MEX()
% function [Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc GMM2_l GMM2_u GMM2_avg GMM2_disc ...
%     GMM3_l GMM3_u GMM3_avg GMM3_disc Unc_Kde Unc_Knn] = ...
%     Calc_Uncertainty_MEX(pbs, pbs_sprd, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size)
function [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
    pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size)
%% Loading, defining, and Calculaing variables
%     load ../utils/func_variables;
    pbs = reshape(pbs, prod(isz), []);
    pbs_sprd = reshape(pbs_sprd, prod(isz), []);
    nlabs = size(pbs,2);
    Wcov = zeros(isz); SGauss = Wcov; Unc_Kde = Wcov; Unc_Knn = Wcov; %Unc_sig = Unc2; 
    GMM2_avg = Wcov; %GMM1_avg = Wcov; GMM3_avg = Wcov;
%     GMM1_l = Wcov;  GMM1_u = Wcov;  GMM1_disc = Wcov;
    %GMM2_l = Wcov; GMM2_u = Wcov; GMM2_disc = Wcov;
%     GMM3_l = Wcov; GMM3_u = Wcov;  GMM3_disc = Wcov;
    mn = mnx(dispvec);
    gam = 0.5772; epsilon = 10e-8; dimension = size(dispvec,2); %nlabs-1;
    norm_factor = 1;   
    time.wcov = 0;
    time.kde = 0;
    time.knn = 0;
    time.gmm = 0;
%% Weighted Covariance Pre-Calculation
%     if (Uncert_Mode ~= 0)
%         dispveco.x = repmat(dispvec(:,1)',prod(isz),1);
%         dispveco.y = repmat(dispvec(:,2)',prod(isz),1);
%         dispvecw.x = dispveco.x .* pbs;
%         dispvecw.y = dispveco.y .* pbs;
%         meanw.x = sum(dispvecw.x,2);                % weighted vectors' mean
%         meanw.y = sum(dispvecw.y,2);                % weighted vectors' mean
%         data(1:nlabs,1,1:prod(isz)) = dispveco.x';  
%         data(1:nlabs,2,1:prod(isz)) = dispveco.y';
%         if(data_mode == 3)
%             dispveco.z = repmat(dispvec(:,3)',prod(isz),1);
%             dispvecw.z = dispveco.z .* pbs;
%             meanw.z = sum(dispvecw.z,2);                % weighted vectors' mean
%             data(1:nlabs,3,1:prod(isz)) = dispveco.z';
%             datam(1:nlabs,3,1:prod(isz)) = repmat(meanw.z',nlabs,1);
%         end   
%         datam(1:nlabs,1,1:prod(isz)) = repmat(meanw.x',nlabs,1);
%         datam(1:nlabs,2,1:prod(isz)) = repmat(meanw.y',nlabs,1);
%     end
%% 
    S = ( mask > 0 );
    inds = find(S==1);   
    for pix = 1:len(inds),               
%% Weighted Nearest Neighbor Calculation Using Replication
        tic
        accuracy=3;
        [dispvec_rep k_start] = replicate_vect_pbs(dispvec, pbs, accuracy, inds(pix));       
        dispvec_rep = dispvec_rep';
        nlabs_rep = size(dispvec_rep,1);
        k_start = 1; 
        k = nlabs_rep;
        if(~isempty(dispvec_rep))
            [IDX D] = knnsearch(dispvec_rep, dispvec_rep, 'k', k,'distance','euclidean'); %'minkowski','p',5); );
            weight_neighbor = k_start:k;
            distance_k = sum(log(D(:,k_start:end)+epsilon),2);
%             Unc5(inds(pix)) = mean(sum(log2((gamma(d/2+1) .* weight_neighbor) ./ ( (pi .^ (d/2) * nlabs_rep) .* (D+epsilon) )),2));
%             Unc5(inds(pix)) = (dimension ./ nlabs_rep) .* distance_k(k) + log(pi.^(dimension/2) / gamma(dimension/2+1) ) ...
%                 - ( psi(k)) + log(nlabs) ;
            Unc_Knn(inds(pix)) = sum((dimension ./ (nlabs_rep-k_start+1)) .* distance_k' + log(pi.^(dimension/2) / gamma(dimension/2+1) ) ...
                - ( psi(weight_neighbor)) + log(nlabs)) ;            
        else
            Unc_Knn(inds(pix)) = 0;
        end
        time.knn = time.knn + toc;
        if (Uncert_Mode ~= 0) % 1 or 2 or 3
%% Weighted Covariance Calculation
%             if(data_mode == 3)
%                 covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),3,1) .* (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * (data(:,:,inds(pix)) - datam(:,:,inds(pix))) );% ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );%temp;
%             else
%                 covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),2,1) .* (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * (data(:,:,inds(pix)) - datam(:,:,inds(pix))) );% ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );%temp;
%             end
%             prob = sqz(pbs(inds(pix),:));
%             [value_ index_] = sort(prob,'descend');
%     %         covw_pozzi(:,:,pix) = weightedcov(dispvec(index_,:), value_);
            tic
            prob = sqz(pbs(inds(pix), :));
            if(data_mode == 2)
                W_mu = sum([prob(:) .* dispvec(:,1), prob(:) .* dispvec(:,2)]);
                x_pts = dispvec(:,1) - W_mu(1);
                y_pts = dispvec(:,2) - W_mu(2);
                Sigma = [x_pts(:)'; y_pts(:)'] * diag(prob(:)) * [x_pts(:), y_pts(:)];
            else         
                W_mu = sum([prob(:) .* dispvec(:,1), prob(:) .* dispvec(:,2) prob(:) .* dispvec(:,3)]);
                x_pts = dispvec(:,1) - W_mu(1);
                y_pts = dispvec(:,2) - W_mu(2);
                z_pts = dispvec(:,3) - W_mu(3);
                Sigma = [x_pts(:)'; y_pts(:)'; z_pts(:)' ] * diag(prob(:)) * [x_pts(:), y_pts(:), z_pts(:)];
            end
            Wcov(inds(pix)) = trace(Sigma); %det(covw(:,:,pix)); %det(temp);
            SGauss(inds(pix)) = 1/2 * log( (2*pi*exp(1)) .^ size(dispvec,2) .* (Wcov(inds(pix))+epsilon_) );  
            time.wcov = time.wcov + toc;
        end
        if (Uncert_Mode == 3)
%%      MoG Calculation
%     for kk = opt.init:opt.step:opt.end, %iter1,
        tic
        opts1 = statset( 'Display', 'final', 'MaxIter', iter1, 'TolTypeFun', 'rel', 'TolFun', 1e-6, 'TolTypeX', 'rel', 'TolX', 1e-6, 'OutputFcn', @emiter );
%% 
        [dispvec_rep_gmm k_start] = replicate_vect_pbs(dispvec, pbs_sprd, accuracy, inds(pix));       
        dispvec_rep_gmm = dispvec_rep_gmm';
        nlabs_rep_gmm = size(dispvec_rep_gmm,1);
%             if(inds(pix) ~= 9),
           ii = floor(linspace(2, nlabs_size,3));
           for MoGk_ind = 2:2,
            clear S;
            if(data_mode == 3)
                MoGk = ii(MoGk_ind) .^ 3;
                [x y z] = meshgrid(linspace(mn(1), mn(2), round((MoGk).^(1/3))), ...
                        linspace(mn(1), mn(2), round((MoGk).^(1/3))), ...
                        linspace(mn(1), mn(2), round((MoGk).^(1/3))));
                if MoGk>300,
                    MoGk = 300; 
                    mu = ([x(:), y(:), z(:)]);                    
                    opts2 = statset( 'Display', 'final', 'MaxIter', 1000, 'TolTypeFun', 'rel', 'TolFun', 1e-2, 'TolTypeX', 'rel', 'TolX', 1e-2 );       
                    [mu_idx mu_val] = kmeans(mu, MoGk, 'Options', opts2); 
                    S.mu = zeros(MoGk, 3); S.Sigma = zeros(3,3,MoGk);
                    S.mu = mu_val;               
                else
                    S.mu = zeros(MoGk, 3); S.Sigma = zeros(3,3,MoGk);
                    S.mu = ([x(:), y(:), z(:)]); 
                end
                [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
                sig = D(1,2)/(6);
                S.Sigma = repmat([sig 0 0; 0 sig 0; 0 0 sig], [1 1 size(S.mu,1)]);
            else  
                MoGk = ii(MoGk_ind) .^ 2;
%                 if MoGk>20, MoGk = 24; end
                S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
                [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
                S.mu = ([x(:), y(:)]);
                [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
                sig = D(1,2)/(6);
                S.Sigma = repmat([sig 0; 0 sig], [1 1 size(S.mu,1)]);
            end 
            S.PComponents(1:size(S.mu,1)) = 1/size(S.mu,1);               
            obj = gmdistribution.fit(dispvec_rep_gmm, size(S.mu,1),'Regularize', epsilon_, 'Options', opts1, 'Start', S);
            disp([' pixel: ', num2str(inds(pix)), ' GMM = ', num2str(MoGk)]);

%                h = ezcontour(@(x,y)pdf(obj,[x y]),[-11 11],[-11 11]);

                for l = 1:obj.NComponents,
                    opt.l = l;
                    opt.MoGk = MoGk;
%                     opt.X = obj.mu(l,:);
%                     opt.sigma = obj.Sigma(:,:,l);
                    h_mog_lower(l) = mixgauss_distribution(obj, opt);
                    h_mog_upper(l) = obj.PComponents(l) .* (-log(obj.PComponents(l)) + ...
                        1/2*log( (2*pi*exp(1)) .^ size(dispvec_rep_gmm,2) .* det(obj.Sigma(:,:,l))));
                end            
                Unc_est = 0;
                mn1 = mnx(dispvec_rep_gmm);
                
%                 if(data_mode == 3)
%                     for index_x = mn1(1):mn1(2),
%                         for index_y = mn1(1):mn1(2),
%                             for index_z = mn1(1):mn1(2),
%                                 X = [index_x index_y index_z];
%                                 for l = 1:obj.NComponents,
%                                     Siginv = pinv(obj.Sigma(:,:,l));
%                                     component(l) = 1 / sqrt(2*pi*det(obj.Sigma(:,:,l))) .* exp( -(X -  obj.mu(l,:)) * ...
%                                       Siginv * (X -  obj.mu(l,:))' ) .* obj.PComponents(l);
%                                 end
%                                 f = sum(component);
%                                 Unc_est = Unc_est - f * log(f);
%                             end
%                         end
%                     end
%                 else                               
%                     for index_x = mn1(1):mn1(2),
%                         for index_y = mn1(1):mn1(2),
%                             X = [index_x index_y];
%                             for l = 1:obj.NComponents,
%                                 Siginv = pinv(obj.Sigma(:,:,l));
%                                 component(l) = 1 / sqrt(2*pi*det(obj.Sigma(:,:,l))) .* exp( -(X -  obj.mu(l,:)) * ...
%                                   Siginv * (X -  obj.mu(l,:))' ) .* obj.PComponents(l);
%                             end
%                             f = sum(component);
%                             Unc_est = Unc_est - f * log(f);
%                         end
%                     end
%                 end
                if (MoGk_ind == 1)
%                     GMM1_l(inds(pix)) = sum(h_mog_lower);
%                     GMM1_u(inds(pix)) = sum(h_mog_upper);
                    GMM1_avg(inds(pix)) = sum(h_mog_lower + h_mog_upper)/2;
%                     GMM1_disc(inds(pix)) = Unc_est;
                end
                if (MoGk_ind == 2)
%                     GMM2_l(inds(pix)) = sum(h_mog_lower);
%                     GMM2_u(inds(pix)) = sum(h_mog_upper);
                    GMM2_avg(inds(pix)) = sum(h_mog_lower + h_mog_upper)/2;
%                     GMM2_disc(inds(pix)) = Unc_est;
                end
                if (MoGk_ind == 3)
%                     GMM3_l(inds(pix)) = sum(h_mog_lower);
%                     GMM3_u(inds(pix)) = sum(h_mog_upper);
                    GMM3_avg(inds(pix)) = sum(h_mog_lower + h_mog_upper)/2;
%                     GMM3_disc(inds(pix)) = Unc_est;
                end 
           end
%             else
%                 GMM2_avg(inds(pix)) = 0;
%             end
            time.gmm = time.gmm + toc;
        end       %            end
%%      Gaussian Kernel Density Estimation Calculation 
        if (Uncert_Mode > 1) % 2 or 3
            tic
            [IDX D] = knnsearch(dispvec, dispvec, 'k', 2,'distance','euclidean'); %'minkowski','p',5); );            
            sig = D(1,2)/(6);
            if(data_mode ==3)
                opt.Sig = [sig 0 0; 0 sig 0; 0 0 sig];
            else
                opt.Sig = [sig 0; 0 sig];
            end
            for l = 1:nlabs,
                opt.l = l;
                X = dispvec(l,:);
                h_gauss_lower(l) = gaussian_kernel(dispvec, X, pbs, inds(pix), opt);
%                     h_gauss_upper(l) = pbs(inds(pix),l) .* (-log(pbs(inds(pix),l))) + ...
%                         1/2*log( (2*pi*exp(1)) .^ size(dispvec_rep_gmm,2) .* det(obj.Sigma(:,:,l))));
            end


            h_gauss_upper = sum(pbs(inds(pix),:) .* (-log(pbs(inds(pix),:))+ ...
                1/2 .* log((2*pi*exp(1)) .^ size(dispvec,2) .* det(opt.Sig))));
            Unc_Kde(inds(pix)) = ( sum(h_gauss_lower) + h_gauss_upper ) / 2;                 
            time.kde = time.kde + toc;               
        end
    end
%%
    if (Uncert_Mode ~= 0)
        Wcov(isnan(Wcov)) = 0;
        time.sgauss = time.wcov;
        SGauss(isnan(SGauss)) = 0;
    end
    if (Uncert_Mode > 2)
%         GMM1_l(isnan(GMM1_l)) = 0;
%         GMM1_u(isnan(GMM1_u)) = 0;
%         GMM1_avg(isnan(GMM1_avg)) = 0;
%         GMM1_disc(isnan(GMM1_disc)) = 0;
%         GMM2_l(isnan(GMM2_l)) = 0;
%         GMM2_u(isnan(GMM2_u)) = 0;
        GMM2_avg(isnan(GMM2_avg)) = 0;
%         GMM2_disc(isnan(GMM2_disc)) = 0;
%         GMM3_l(isnan(GMM3_l)) = 0;
%         GMM3_u(isnan(GMM3_u)) = 0;
%         GMM3_avg(isnan(GMM3_avg)) = 0;
%         GMM3_disc(isnan(GMM3_disc)) = 0;   
    end
    if (Uncert_Mode > 1)
        Unc_Kde(isnan(Unc_Kde)) = 0;   
    end
%%
    Unc_Knn(isnan(Unc_Knn)) = 0;
    Unc_Knn = Unc_Knn - min(Unc_Knn(:));
%     Unc6 = mat2gray(reshape(Unc6, [isz(1) isz(2)]));
%     Unc7 = mat2gray(reshape(Unc7, [isz(1) isz(2)]));

%     save Uncertainties Wcov SGauss GMM1_l GMM1_u GMM1_avg GMM1_disc Unc_Kde Unc_Knn ...
%         GMM2_l GMM2_u GMM2_avg GMM2_disc GMM3_l GMM3_u GMM3_avg GMM3_disc; % Unc_sig Unc6 Unc7;
end