% function [ Disp Sc Dc Dc2 I1p I2p]=MRFregCosts( I1, I2 , tlands, slands, maxD,  opt )
function [ Disp2 Sc2 Dc2 ] = MRFregCosts2( I1, I2 , tlands, slands, opt )

I1= double(I1/max(I1(:)));
I2= double(I2/max(I2(:)));

I1(isnan(I1)) = 0;
I2(isnan(I2)) = 0;

%% Defining Labels and Sc, if not given
% if maxD ==0
%     nl = 1; Disp = [0 0 0];
%     Sc =0; 
% else
%     if isfield(opt, 'res')
%         res = opt.res;
%     else
%         res=1;
%     end
    
%% I changed the code here to define new labels based on the landmarks directions!
% Instead of the below two lines:
%     [Ux Uy Uz]=ndgrid(-maxD:res:maxD);
    Disp2=tlands' - slands'; %49 labels  Disp={-3,-2,-1,0,1,2,3}^2  (for x and y direction)

    nl=size(Disp2,1);

    %% Creating Smoothness Cost
    Sc=zeros(nl); % #lables x #lables         Sc  <-- |U(p)-U(q)|
    for i=1:nl
        for j=1:nl
            %Sc(i,j)=abs(Disp(i,1)-Disp(j,1))+abs(Disp(i,2)-Disp(j,2));
            Sc2(i,j) = sqrt((Disp2(i,1)-Disp2(j,1)).^2 + (Disp2(i,2)-Disp2(j,2)).^2);%Change the equation here
        end
    end

nl=size(Disp2,1);
imsz=size(I1); 

%% Creating Data Cost 
 
Dc2=zeros(imsz(1),imsz(2), nl);
[mx my]=ndgrid(1:imsz(1), 1:imsz(2));
% [mx my]=meshgrid(1:imsz(2), 1:imsz(1));
mask = [];
    
datacost = 'SD';
if ~isempty(opt)
    if isfield( opt, 'datacost' );
    datacost = opt.datacost;
    end
    if isfield( opt, 'mask' );
    mask = opt.mask; 
    end
end
if isfield( opt, 'metric_npix' )
npix = opt.metric_npix;
else
npix = 3; 
end

disp (['# of disp. vec. labels added = ' num2str(nl)])

if  strcmp(datacost, 'Mind') %I2 is src, I1 is tar
    %%
    if isempty(mask ) 
        rad = 0;
        for l=1:nl
            d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
            mind_tar=MIND_descriptor2d(I1,rad);
            mind_src=MIND_descriptor2d(d2,rad);
            Dc2(:,:,l) =sqrt( sum( (mind_tar - mind_src) .^2, 3) );
        end    
    else
        for l=1:nl
            d2=interp2( my, mx, I2 , my + Disp(l,2) , mx + Disp(l,1), '*linear', 0 );
            mind_tar=MIND_descriptor2d(I1,1);
            mind_src=MIND_descriptor2d(d2,1);
            Dc2(:,:,l) =mask .* sqrt(sum( (mind_tar - mind_src).^2, 3 ));
        end    
    end      

elseif  strcmp(datacost, 'SD')
    %%
    if isempty(mask )
        for l=1:nl
%             d3=interp3( my, mx, mz, I2 , my + Disp2(l,2), mx  + Disp2(l,1), mz + Disp2(l,3), '*linear' , 0);
        d2=interp2( my, mx, I2 , my + Disp2(l,2), mx  + Disp2(l,1), 'linear', 0 );
            Dc2(:,:,l) =( d2 -  I1 ).^2;    
        end    
    else
        for l=1:nl
%             d3=interp3( my, mx, mz, I2 , my + Disp2(l,2), mx  + Disp2(l,1), mz + Disp2(l,3), '*linear' , 0);
            d2=interp2( my, mx, I2 , my + Disp2(l,2), mx  + Disp2(l,1), 'linear', 0 );
            Dc2(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        end    
    end
 
    
elseif strcmp(datacost, 'GRADIENTMAG')
    %%
        [gx gy gz]=gradient( I1 );
        I1= sqrt( gx.^2 + gy.^2 + gz.^2);
        
        [gx gy gz]=gradient( I2 );
        I2= sqrt( gx.^2 + gy.^2 + gz.^2);
       

    if isempty(mask )
        for l=1:nl
%             d3=interp3( my, mx, mz, I2 , my + Disp2(l,2), mx  + Disp2(l,1), mz + Disp2(l,3), '*linear' , 0);
            d2=interp2( my, mx, I2 , my + Disp2(l,2), mx  + Disp2(l,1), '*linear', 0 );
            Dc2(:,:,l) =( d2 -  I1 ).^2;    
        end    
    else
        for l=1:nl
%             d3=interp3( my, mx, mz, I2 , my + Disp2(l,2), mx  + Disp2(l,1), mz + Disp2(l,3), '*linear' , 0);
            d2=interp2( my, mx, I2 , my + Disp2(l,2), mx  + Disp2(l,1), '*linear', 0 );
            Dc2(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        end    
    end
    
     
elseif strcmp(datacost, 'MDZI-GRAD')
    %%
    [gx1 gy1 gz1]=gradient( I1 );
    [gx2o gy2o gz2o]=gradient( I2 );

     if isempty(mask )
        for l=1:nl
            gx2=interp2( my, mx, gx2o , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            gy2=interp2( my, mx, gy2o , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
%            gx2 = interp3(my, mx, mz, gx2o, my - Disp(l,2), mx - Disp(l,1), mz - Disp(l,3), '*linear', 0);
%            gy2 = interp3(my, mx, mz, gy2o, my - Disp2(l,2), mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0);
%            gz2 = interp3(my, mx, mz, gz2o, my - Disp2(l,2), mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0);
            Dc2(:,:,l) =1- (gx1 .* gx2 + gy1.*gy2 ) ./  sqrt( gx1.* gx1 + gy1.* gy1 + 1e-5) ./ sqrt( gx2.* gx2 + gy2.* gy2 + 1e-5) ;
%            Dc2(:,:,:,l) =1- (gx1 .* gx2 + gy1.*gy2 + gz1.*gz2 ) ./  sqrt( gx1.* gx1 + gy1.* gy1 + gz1.*gz2 + 1e-5) ./ sqrt( gx2.* gx2 + gy2.* gy2 + gz1.*gz2 + 1e-5) ;        
        end    
    else
        for l=1:nl
%             d3=interp3( my, mx, mz, I2 , my + Disp2(l,2), mx  + Disp2(l,1), mz + Disp2(l,3), '*linear' , 0);
            d2=interp2( my, mx, I2 , my + Disp2(l,2), mx  + Disp2(l,1), '*linear', 0 );
            Dc2(:,:,l) =mask .* ( d2 -  I1 ).^2;                
%         d2=interp2( mx, my, I2 , mx - Disp(l,1) , my - Disp(l,2), 'linear', 0 );
%         Dc(:,:,l) =mask .* ( d2 -  I1 ).^2;    
        end    
     end
    

elseif strcmp(datacost, 'GRADIENT')
    %%
        [gx gy]=gradient( I1 );
        [gx2 gy2]=gradient( I2 );

     if isempty(mask )
        for l=1:nl
%             dgx3=interp3( my, mx, mz, gx2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             dgy3=interp3( my, mx, mz, gy2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             dgz3=interp3( my, mx, mz, gz2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             Dc2(:,:,:,l) = sqrt( (dgx3 -  gx ).^2 + (dgy3 - gy).^2 + (dgz3 - gz).^2 );                
            gx2=interp2( my, mx, gx2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            gy2=interp2( my, mx, gy2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            Dc2(:,:,l) = sqrt( (dgx2 -  gx ).^2 + (gy - dgy2).^2 );    
        end    
    else
        for l=1:nl
%             dgx3=interp3( my, mx, mz, gx2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             dgy3=interp3( my, mx, mz, gy2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             dgz3=interp3( my, mx, mz, gz2 , my - Disp2(l,2) , mx - Disp2(l,1), mz - Disp2(l,3), '*linear', 0 );
%             Dc2(:,:,:,l) = mask .* sqrt( (dgx3 -  gx ).^2 + (dgy3 - gy).^2 + (dgz3 - gz).^2 );   
            gx2=interp2( my, mx, gx2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            gy2=interp2( my, mx, gy2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            Dc2(:,:,l) =mask .* sqrt( (dgx2 -  gx ).^2 + (gy - dgy2).^2 );    
        end    
     end
    

 
    
elseif  strcmp(datacost, 'NCC') ||  strcmp(datacost, 'gNCC') 
        %%
    if strcmp(datacost, 'gNCC') 
    
        [gx gy]=gradient( I1 );
        I1= sqrt( gx.^2 + gy.^2);
        
        [gx gy]=gradient( I2 );
        I2= sqrt( gx.^2 + gy.^2);
    
    end
    
    if isempty(mask )        
        for l=1:nl
            dI2=double(interp2( my, mx, I2 , my + Disp2(l,2) , mx + Disp2(l,1), '*linear', 0 ));
%            dI3=double(interp3( my, mx, mz, I2 , my + Disp2(l,2) , mx + Disp2(l,1), mz + Disp2(l,3), '*linear', 0 ));        
            Dc2(:,:,l) =  ( - dI2 .* I1 ); 
        end    
    else
        for l=1:nl
%            dI3=double(interp3( my, mx, mz, I2 , my + Disp2(l,2) , mx + Disp2(l,1), mz + Disp2(l,3), '*linear', 0 ));        
%            Dc2(:,:,:,l) =  mask .* ( dI3 .* I1 ); 
            dI2=double(interp2( my, mx, I2 , my + Disp2(l,2) , mx + Disp2(l,1), '*linear', 0 ));
            Dc2(:,:,l) = mask .*(dI2 .* I1 ).^2 ./ sqrt(dI2 .^2 .* I1.^2 + 1e-3);       
        end       
    end
     
    
elseif strcmp(datacost, 'NSSD')
    %%
        
    I1=double(I1);
    I2=double(I2);
    I1p  = getNormalizedLocalOffset( I1, npix );
    I2p  = getNormalizedLocalOffset( I2 , npix );
                 
    if isempty(mask )
    
        for l=1:nl
%           d3=interp3( my, mx, mz, I2p ,  my - Disp2(l,2),mx - Disp2(l,1) , mz - Disp2(l,3), '*linear', 0 );
%           Dc2(:,:,:,l) =( d3 -  I1p ).^2;    
           d2=interp2( my, mx, I2p , my + Disp2(l,2),mx + Disp2(l,1), '*linear', 0 );
           Dc2(:,:,l) =( d2 -  I1p ).^2;    
        end    
    else
        for l=1:nl
%               d3=interp3( my, mx, mz, I2p ,  my - Disp2(l,2),mx - Disp2(l,1) , mz - Disp2(l,3), '*linear', 0 );
%               Dc2(:,:,:,l) = mask .* ( d3 -  I1p ).^2;    
               d2=interp2( my, mx, I2p , my + Disp2(l,2),mx + Disp2(l,1), '*linear', 0 );
               Dc2(:,:,l) = mask .* ( d2 -  I1p ).^2;    
        end    
    
    end
elseif strcmp(datacost, 'GRAD')
    
    I1=double(I1);
    I2=double(I2);

    if isfield( opt, 'metric_npix' )
    npix = opt.metric_npix;
    else
    npix = 3; 
    end
    
    I1= smooth(I1 , npix );
    I1= smooth(I1 , npix );
    [gx1 gy1]= get_gradient ( I1 );
    [gx2 gy2]= get_gradient ( I2 );
     
        
    if isempty(mask )
        for l=1:nl
%            dgx3=interp3( my, mx, mz, gx2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            dgy3=interp3( my, mx, mz, gy2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            dgz3=interp3( my, mx, mz, gz2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            Dc2(:,:,:,l) = ( gx1 .* dgx3 + gy1 .* dgy3 + gz1 .* dgz3 );   
            dgx2=interp2( my, mx, gx2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            dgy2=interp2( my, mx, gy2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );    
            Dc2(:,:,l) = ( gx1 .* dgx2 + gy1 .* dgy2 );   
        %waitbar(l / nl )
        end 
    else
        for l=1:nl
%            dgx3=interp3( my, mx, mz, gx2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            dgy3=interp3( my, mx, mz, gy2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            dgz3=interp3( my, mx, mz, gz2 , my - Disp(l,2), mx - Disp(l,1) , mz - Disp(l,3), '*linear', 0 );
%            Dc2(:,:,:,l) = mask .* ( gx1 .* dgx3 + gy1 .* dgy3 + gz1 .* dgz3 );             
            dgx2=interp2( my, mx, gx2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );
            dgy2=interp2( my, mx, gy2 , my + Disp(l,2), mx + Disp(l,1), '*linear', 0 );    
            Dc2(:,:,l) = mask .* ( gx1 .* dgx2 + gy1 .* dgy2 );   
        %waitbar(l / nl )
        end 
    end     
end


% resize Dc from (m x n x l) to (l x p )
if nargout>3
    npix  =imsz(1)*imsz(2)*imsz(3);
    Dc3=zeros(nl,npix);

    for i=1:nl
        D=Dc2(:,:,:,i);
        Dc3(i,:)=(D(:))';
    end
end

%%

d = mnx(Dc2);
k=abs(d(1) );
Dc2=  Dc2 +  k;
k=diff(d);
Dc2=Dc2 / k;

%%%%% Added part
% Dc=ones(imsz(1),imsz(2), nl) / k;
%%%%% Added part
   
%% helper functions --- begin here

function m= smooth( I1 , npix )
    I1=double(I1);
    g=fspecial( 'gaussian' ,[npix npix ], sqrt(npix)*2 );
    m=imfilter( I1, g );


function [gx gy]= get_gradient ( I1 )

     [gx gy]=gradient( I1 );

     nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
     gx=gx ./ nrm;
     gy=gy ./ nrm;
     
function I1p = getNormalizedLocalOffset( I1 , npix )
        
    m1=smooth( I1, npix );
    m1b= sqrt( smooth( (I1 - m1).^2 , npix ));
    I1p=(I1 - m1 ) ./ (1e-5+ m1b);
        
       
 
 