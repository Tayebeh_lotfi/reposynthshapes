function [ src tar slands tlands Tx Ty invTx invTy segment src_segment ] = Reading_Images(noise_sigma, noise_mean, str)

    spacing.height = 20;
    spacing.width = 20;
    if(strcmp(str,'_small'))
         magnitude = 7;
    else if (strcmp(str, '_large'))
            magnitude = 10;
        end
    end
    load '../data/native_slices_ax2_41.mat';
%     load '../data/anat_normal_brn1.mat';
%     load '../data/anat_normal_brn2.mat';
%     load '../data/anat_normal_brn3.mat';
    isz = 256 % 128; % 
    m = [isz isz];
    [u v] = ndgrid(1:m(1),1:m(2));
    for img_num = 1 : size(Im,3),    
        tar(:,:,img_num) =  imresize(Im(:,:,img_num), [m(1), m(2)]); %(Im(:,:,1)  .* (segs(:,:,1) > 0));
        tar(:,:,img_num) = imrotate(tar(:,:,img_num),90);
        segment(:,:,img_num) =  imresize(segs(:,:,img_num), [m(1), m(2)]);
        segment(:,:,img_num) = imrotate(segment(:,:,img_num),90);
        [src(:,:,img_num)  Tx(:,:,img_num)  Ty(:,:,img_num)] = rBSPwarp( tar(:,:,img_num), [spacing.height spacing.width], magnitude );
        tar(:,:,img_num) = resc(tar(:,:,img_num)); src(:,:,img_num) = resc(src(:,:,img_num));
%         tar(:,:,img_num) = imnoise( resc(tar(:,:,img_num)), 'gaussian', rand(1)*noise_mean, rand(1)*noise_sigma );
        src(:,:,img_num) = imnoise( resc(src(:,:,img_num)), 'gaussian', rand(1)*noise_mean, noise_sigma );
        D(:,:,1) = Tx(:,:,img_num); D(:,:,2) = Ty(:,:,img_num);
        invD = invertDefField(D);
        invTx(:,:,img_num) = invD(:,:,1); invTy(:,:,img_num) = invD(:,:,2);
        src_segment(:,:,img_num) =  interp2(v,u,segment(:,:,img_num),v+invTy(:,:,img_num),u+invTx(:,:,img_num), '*linear', 0);
        src(:,:,img_num) =  interp2(v,u,tar(:,:,img_num),v+invTy(:,:,img_num),u+invTx(:,:,img_num), '*linear', 0);
    end
    src_segment(isnan(src_segment)) = 0;
    src(isnan(src)) = 0;
%     distx = 65; disty = 65;        
    distx = 1; disty = 1; 
    maxd = 1 + round(max(max(Tx(:), Ty(:))));
    for img_num = 1 : size(Im,3), 
        tlands(:,1, img_num) = [distx; disty]; 
        tlands(:,2, img_num) = [distx; m(2)-disty]; 
        tlands(:,3, img_num) = [m(1)-distx; disty]; 
        tlands(:,4, img_num) = [m(1)-distx; m(2)-disty]; 

%         slands(:,:, img_num) = Estimate_Corresp(tlands(:,:, img_num), src(:,:,img_num), tar(:,:,img_num), maxd);
    end
 
    slands = tlands;
    
%     figure, subplot 121, imagesc(tar(:,:,img_num)),colormap(gray), hold on, plot(tlands(2,:,img_num), tlands(1,:,img_num), 'r*'),
%             subplot 122, imagesc(src(:,:,img_num)),colormap(gray), hold on, plot(slands(2,:,img_num), slands(1,:,img_num), 'r*'),

    figure, montage(reshape(mat2gray(tar),[size(tar,1)  size(tar,2) 1 size(tar,3)]  )), title('Fxd Img')
    figure, montage(reshape(mat2gray(src),[size(src,1)  size(src,2) 1 size(src,3)]  )), title('Mvg Img')
end