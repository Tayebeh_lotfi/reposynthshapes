function [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid(src, invTx, invTy, ops, slands, tlands, reg_iter, Max_Iter, MCase)

    
    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
    mseed = ops.num_seed * Max_Iter; seed = ops.num_seed;
    isz = size(src);
    length = size(slands,2);
    start = 35;
%     [seedx seedy] = ndgrid( start:round((isz(1)-2*start)/5):isz(1)-start, start:round((isz(2)-2*start)/5):isz(2)-start );
    [seedx seedy]=ndgrid(linspace(start, isz(1)-start, round(sqrt(mseed))+1), linspace(start, isz(1)-start, round(sqrt(mseed))+1));
%     slands_new = [ seedx(reg_iter:reg_iter+3); seedy(reg_iter:reg_iter+3) ];
%     slands_new = [ seedx(reg_iter*2-1:reg_iter*2); seedy(reg_iter*2-1:reg_iter*2) ];
    slands_new = [ seedx((reg_iter-1)*seed+1:reg_iter*seed); seedy((reg_iter-1)*seed+1:reg_iter*seed) ];    
%     slands_new = [ seedx(reg_iter); seedy(reg_iter) ];
    slands_new = round(slands_new);
    inds = sub2ind(isz, slands_new(1,:), slands_new(2,:));
    if(MCase == 1)
        nx=invTx(inds);% + .5 * randn(1, size(slands_new,2));
        ny=invTy(inds);% + .5 * randn(1, size(slands_new,2));
    else
        nx=-invTx(inds);% + .5 * randn(1, size(slands_new,2));
        ny=-invTy(inds);% + .5 * randn(1, size(slands_new,2));
    end
    tlands_new(1,:) =  mx( inds)+nx; % -nx
    tlands_new(2,:) =  my( inds)+ny; % -ny
    tlands(:,length+1:length+seed) = tlands_new;
    slands(:,length+1:length+seed) = slands_new;
%     tlands(1,1) = mx(inds(1))-nx(1);
%     tlands(2,1) = my(inds(1))-ny(1);
    nseeds = size(tlands,2);
end
