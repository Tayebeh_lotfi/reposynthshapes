function [ dispvec smthcost dterm nlabs maxd ] = Calc_Likelihood(tar, src, Tx, Ty, nseeds, res, Lbl_mod, simil_measure, magnitude)

%% ops.mode:
% 1: Uniform meshgrid, 3: Kmeans GT, 4: VQ GT, 5: Polar sampling, 6: Uniform polar

    if(nargin < 9)
        maxd = 1 + round(max(max(Tx(:), Ty(:)))); %g = groundtruth
    else
        maxd = magnitude;
    end
    ops.mode = Lbl_mod; % 7; % 5; % 6; % 4; % 3; % 

%% level of discret. of solution space                
%     if maxd <=5
%         ops.res= .5;
%     else
%         ops.res= 1;
%     end

    ops.resolution = res;
%% data term mode
disp 'Construct data prior matrix'
switch (simil_measure)
    case 1
        ops.datacost = 'MDZI-GRAD';
    case 2
        ops.datacost = 'gNCC';
    case 3
        ops.datacost = 'NSSD';
    case 4
        ops.datacost = 'SD';
    case 5
        ops.datacost = 'Mind';
    case 6
        ops.datacost = 'SDPatch';
end
    [ dispvec smthcost dterm ] = MRFregCosts( tar, src, maxd, Tx, Ty, ops);
    nlabs = size(dispvec,1);
    
%% %%%% to be finished: %%%%% %%%%% %%%%% 
    if nseeds<1 % determine corrsp. using DE term      
        % one may use SIFT-determined corresp. as better alternative!    
        % mask = imerode(src > 5,strel('disk', 3));
        % mask = zeros(isz);
        mask(30:end-30, 30:end-30 )=1;
        ims(mask)    
        input('Valid mask? press ENTER to cont. or modify the code here')
        for x=1:isz(1)
            for y=1:isz(2)            
                dc = sqz( dterm( x,y,: ) );            
                d=mnx(dc);         
                range = d(1):diff(d)/19:d(2);
                c=histc(dc,range);
                i=1;
                while c(i)==0, i=i+1; end
                i = find( dc <= range(i+2) );
                if len(i)==1 
                    dterm_trust(x,y)=0;  
                else                
                dterm_trust(x,y)=mean( pdist( dispvec(i,:) )); %lower, the more likely it is           
                end
            end
        end
        dterm_trust(  mask == 0 ) = max(dterm_trust(:));
        [ ~, i]=sort(dterm_trust(:));
        topn=200;    
        [sx sy]=ind2sub( isz, i(1:topn) );    
        ims(dterm_trust ); plot(sy ,sx, 'go');
    end
end