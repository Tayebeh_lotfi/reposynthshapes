function Analyze_Err_Unc(invTx, invTy, dx, dy, mask,Unc1, reg_iteration, Mode, Mode_Str, img_num)

    Warp_Err = (sqrt(  (dx - invTx).^2 + (dy - invTy).^2 ));   
    Unc = Unc1(find(mask));
    Err = Warp_Err(find(mask));
    
    Uncert_Mode = 1; Err_Mode = 1;
    my_str = {'Shannon' 'KNN'};
    my_str2 = {'Error' 'Entropy of Error'};
    
    bin_err = 100; mn = mnx(Unc);
    bin_cnt = linspace(mn(1), mn(2),bin_err);

    for bin_counter = 1:len(bin_cnt)-1,
        [~, ixx] = histc(Unc, bin_cnt(bin_counter:bin_counter+1));
        mean_Err(bin_counter) = mean(Err .* ixx);
        std_Err(bin_counter) = std(Err .* ixx);
    end
    dockf; errorbar(mean_Err, std_Err, 'r'), title(['Average Error Using ' my_str{Uncert_Mode}, ...
        ' Uncertainty']), ylabel(['Average ', my_str2{Err_Mode}, ...
        ' at iteration: ', num2str(reg_iteration), ' Mode: ', Mode_Str{Mode}, ...
        'Image: ', num2str(img_num)]), xlim([0 101]), ylim([-4 4]);
end