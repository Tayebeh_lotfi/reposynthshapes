function [out dist] = evalSeg(T,R,labels, bk_pix, show)
% function [MeanOverlap UnionOverlap TargetOverlap FN FP dice sensitivity specificity MAD distr ] = evalSeg(T,R,labels)
%   
%   Defns given in paper by Arno Klein et al. NeuroImage 2009 
% 
%   Returns:  
%       Overall measures: MeanOverlap UnionOverlap TargetOverlap FN FP
%       Rest are per region
%   
%       Should only use if T + R are probablistic segs:
%           MAD: mean absol. diff. between T and R (excluding background pixels as identified in T)
%           distr: distr. of differences betwn T and R 
% 
% Contact: lisat(at)sfu.ca

if nargin<5   show =0; end

nregions = size(chkDim(labels),2);
dice= zeros([nregions 1]);
sensitivity =  dice;
uni=dice;
tar_sz=dice; src_sz =dice;
inter=dice; fn=dice; fp=dice;

% nv=prod(size(T));
% all=1:nv;
% excludeT =[];
% excludeR =[];

for r = 1:nregions            
      a= T(:) == labels(r);      
      b= R(:) == labels(r);      
      
      src_sz(r) = sum(b);      
      tar_sz(r) = sum(a);      
      
      tp(r) = sum( a .* b ); %intersection 
      uni(r) = sum ((a + b >1)); %union      
      tsizes(r) =  sum(  a +b ); 
      dice(r) =  2*tp(r) / (sum(a)+sum(b ));
      
      tn(r) = sum( (1 - a ) .* (1 - b) );
      fn(r) = sum( ( a) .* (1- b)); % T \ R % in T, not in R
      fp(r) = sum( (1 - a) .*  b);   % R \ T   
      
      sensitivity(r) = tp(r) / (tp(r)+ fn(r));
      specificity(r) = tn(r) / (tn(r) + fp(r));
end
to = sum( tp ) / sum(tar_sz);
uo = sum( tp ) / sum(1e-5+ uni );  %mo= 2 * uo /( 1 + uo)
mo = 2*sum(tp) / sum( tsizes ); % uo = ( 2 - mo )/mo 

if  nregions==2
    %%   recall = sensiviity; precision = specifi.

out.recall = tp(2) / (tp(2) + fp(2) );
out.prec =  tp(2) / (tp(2) + fn(2) );

out.mcc=(tp(2) * tn(2) - fp(2)* fn(2) ) / sqrt( (tp(2)+fp(2))*(tp(2)+fn(2))*(tn(2)+fp(2))*(tn(2)+fn(2)) );

out.fscore= 2 * out.prec*out.recall /(out.prec+out.recall);
out.acc = (tp(2) + tn(2) )/( tp(2) + tn(2) + fp(2) +fn(2));
end

rto = tp ./ tar_sz';

out.mo =mo; 
out.uo=uo;
out.to =to;

out.dice =dice;
out.mdice = 2 * sum(tp )/sum( tar_sz + src_sz);
out.sen=sensitivity; % recall
out.spec=specificity;
out.rto=rto;
out.jaccard = out.dice ./ (2-out.dice);

if nargout >1
nbkpix= 1 - (T(:)== bk_pix );
s=abs( double(T(:))- double(R(:)) ) .* nbkpix ;
mad =  sum( s  )/len(nbkpix); 
d=mnx(labels );
dist = histc( s, d(1):diff(d)/19:d(2));
out.mad =mad; 
end

if show
plot(sensitivity); hon %? fraction of positives that are correctly detected
plot(specificity,'r') % ? fraction of negatives that are correctly detected
plot(dice,'g')
legend('sensitivity','specificity', 'dice')
xlabel 'i-th label'
ylabel 'measures'
title 'segmentation evaluation'
end