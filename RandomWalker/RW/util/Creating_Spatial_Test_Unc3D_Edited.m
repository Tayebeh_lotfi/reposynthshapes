function [pbs dispvec] = Creating_Spatial_Test_Unc3D_Edited(nlabs_size, Visualize_Labels, sample_num)
    
    nlabs = nlabs_size * nlabs_size * nlabs_size;
    pix = 1;
    epsilon(1) = 0; % 1e-8;
    eps_max = 1/nlabs;
%     sample_num = 3;
    for i = 2:sample_num,
        epsilon(i) = epsilon(1) + (eps_max - epsilon(1))/sample_num * (i-1);
    end
%     epsilon = [1e-5 5e-5 1e-4 3e-4 5e-4 8e-4 1e-3 3e-3 5e-3 7e-3 9e-3 1e-2 2e-2 3e-2];
%     epsilon = [1e-4 1e-3 1e-2 1e-2*3 1e-2*5 1e-2*6];
    eps_len = len(epsilon);
    pbs = zeros(7*eps_len+1,nlabs);
    for l = 1:nlabs,
            pbs(pix,l) = 1/nlabs;
    end
    pix = pix+1;
    for i = 1: eps_len
%% One preferred label
        for l = 1:nlabs,
            pbs(pix,l) = 1/nlabs - epsilon(i);
        end
        pbs(pix,nlabs) = 1/nlabs + (nlabs-1) * epsilon(i);
%% Two adjucent preferred labels     
        for l = 1:nlabs,
            pbs(pix+eps_len,l) = 1/nlabs - epsilon(i);
        end
        pbs(pix+eps_len,nlabs-1) = 1/nlabs + (nlabs-2)/2 * epsilon(i);
        pbs(pix+eps_len,nlabs) = 1/nlabs + (nlabs-2)/2 * epsilon(i);
%% Two medium distance preferred labels
        for l = 1:nlabs,
            pbs(pix+2*eps_len,l) = 1/nlabs - epsilon(i);
        end
%         pbs(pix+2*eps_len,nlabs-3:nlabs-1) = 1/nlabs - epsilon(i);
        pbs(pix+2*eps_len,nlabs-nlabs_size+1) = 1/nlabs + (nlabs-2)/2 * epsilon(i);
        pbs(pix+2*eps_len,nlabs) = 1/nlabs + (nlabs-2)/2 * epsilon(i);        
        
%% Two far distance preferred labels  

       for l = 1:nlabs,
            pbs(pix+3*eps_len,l) = 1/nlabs - epsilon(i);
       end
        pbs(pix+3*eps_len,1) = 1/nlabs + (nlabs-2)/2 * epsilon(i);
        pbs(pix+3*eps_len,nlabs_size*nlabs_size) = 1/nlabs + (nlabs-2)/2 * epsilon(i);  
        
%% Two farthest distance preferred labels
       for l = 1:nlabs,
            pbs(pix+4*eps_len,l) = 1/nlabs - epsilon(i);
       end
        pbs(pix+4*eps_len,1) = 1/nlabs + (nlabs-2)/2 * epsilon(i);
        pbs(pix+4*eps_len,nlabs) = 1/nlabs + (nlabs-2)/2 * epsilon(i); 
%% Three preferred labels, same slice
       for l = 1:nlabs,
            pbs(pix+5*eps_len,l) = 1/nlabs - epsilon(i);
       end
       midle = floor(nlabs_size * nlabs_size/2) + 1;
       pbs(pix+5*eps_len,1) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
       pbs(pix+5*eps_len,midle) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
       pbs(pix+5*eps_len,nlabs_size*nlabs_size) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
%% Three preferred labels, cube
       for l = 1:nlabs,
            pbs(pix+6*eps_len,l) = 1/nlabs - epsilon(i);
       end
       middle = floor(nlabs/2) + 1;
       pbs(pix+6*eps_len,1) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
       pbs(pix+6*eps_len,middle) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
       pbs(pix+6*eps_len,nlabs) = 1/nlabs + (nlabs-3)/3 * epsilon(i);
       
%% Four corner preferred labels, same slice
       for l = 1:nlabs,
            pbs(pix+7*eps_len,l) = 1/nlabs - epsilon(i);
       end
       for pref = [1 nlabs_size nlabs_size*nlabs_size-nlabs_size+1 nlabs_size*nlabs_size],
           pbs(pix+7*eps_len,pref) = 1/nlabs + (nlabs-4)/4 * epsilon(i);
       end
%         pbs(pix+3*eps_len,nlabs) = 1/nlabs + (nlabs-2)/2 * epsilon(i);          
%% Four corner cube preferred labels
       for l = 1:nlabs,
            pbs(pix+8*eps_len,l) = 1/nlabs - epsilon(i);
       end
       for pref = [1 nlabs_size*nlabs_size nlabs-nlabs_size*nlabs_size+1 nlabs],
           pbs(pix+8*eps_len,pref) = 1/nlabs + (nlabs-4)/4 * epsilon(i);
       end
       
%% Five preferred labels, same slice
       for l = 1:nlabs,
            pbs(pix+9*eps_len,l) = 1/nlabs - epsilon(i);
       end
       for pref = [1 nlabs_size midle nlabs_size*nlabs_size-nlabs_size+1 nlabs_size*nlabs_size],
           pbs(pix+9*eps_len,pref) = 1/nlabs + (nlabs-5)/5 * epsilon(i);
       end       
%% Five preferred labels, cube
       for l = 1:nlabs,
            pbs(pix+10*eps_len,l) = 1/nlabs - epsilon(i);
       end
       for pref = [1 nlabs_size*nlabs_size middle nlabs-nlabs_size*nlabs_size+1 nlabs],
           pbs(pix+10*eps_len,pref) = 1/nlabs + (nlabs-5)/5 * epsilon(i);
       end
 %% Next pixel
        pix = pix+1;        
    end
    grid_size = floor(nlabs_size/2);
    [grid_x grid_y grid_z] = ndgrid(-grid_size:grid_size, -grid_size:grid_size, -grid_size:grid_size);
    dispvec(1:nlabs,:) = [grid_x(:) grid_y(:) grid_z(:)];
 
%% Visualize the labels
    if(Visualize_Labels),
        dockf; plt_cnt = 1;
       for pix = 1:sample_num:size(pbs,1),
        pn = sqz(pbs(pix,:))';
         counter = mod(pix, eps_len)-1;
         if(counter==-1)
             counter = eps_len-1;
         end
         if(counter==0)
             counter = eps_len;
         end
         mn = mnx(dispvec); epsln = 0.5;pn1 = pn; pn1(pn1>0.4)=0.01;%if (pn>0.9) pn = 1-pn; end
         if (pix ~=1 && pix ~=41 && pix ~=49 && pix ~=57 && pix ~=65 && pix ~=73),
            subplot(2,3,plt_cnt), plt_cnt = plt_cnt+1;
            scatter3(dispvec(:,1),dispvec(:,2),dispvec(:,3),1+round(500*pn),[pn1 pn1 pn1],'filled'), xlim([mn(1)-epsln mn(2)+epsln]), ylim([mn(1)-epsln mn(2)+epsln]), zlim([mn(1)-epsln mn(2)+epsln]);    %scatter with size and color proportional to prob.
          switch logical(true)
             case pix>10*eps_len+1
                 title('Five lbls, cube', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]); %, epsilon = ' num2str(epsilon(counter)),         
             case pix>9*eps_len+1
                 title('Five lbls, slice', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>8*eps_len+1
                 title('Four lbls, cube', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>7*eps_len+1
                 title('Four lbls, slice', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>6*eps_len+1
                 title('Three lbls, cube', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>5*eps_len+1
                 title('Three lbls, slice', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>4*eps_len+1
                 title('Two farthest lbls', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>3*eps_len+1
                 title('Two far lbls', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>2*eps_len+1
                 title('Two med-dist lbls', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>eps_len+1
                 title('Two adj lbls', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>1
                 title('One lbl', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             case pix>0
                title('All lbls', 'FontSize', 25); set(gca,'YTick',[]); set(gca,'XTick',[]); set(gca,'ZTick',[]);
             otherwise
                 disp('invalid');
          end
         end 
       end
    end    

end