function [Input Output] = Extract_Features(src, tar, dsrc, dx, dy, pbs, pbs_sprd, dispvec, isz, mask, S, Warp_Err, Warp_Err_Mode)
%% Calculating Uncertainties
    Uncert_Mode = 1; % 0: KNN, 1: All, 2: KNN&KDE
    accuracy=3; epsilon_ = 10e-4; %accuracy, pbs .* 10^(opt.k)
    iter1 = 5000; %gmdistribution iterations
    data_mode = 2; nlabs_size = 5;
%     [Wcov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode);
%     [Wcov SGauss GMM2_avg Unc_Kde Unc_Knn time] = Calc_Uncertainty_MEX(...
%     pbs_sprd, pbs, dispvec, isz, mask, accuracy, epsilon_, iter1, data_mode, Uncert_Mode, nlabs_size);
    [Unc_Shannon Unc_Shannon_Sprd] = Calc_Uncertainty_Shannon(pbs, pbs_sprd, data_mode);
    [ErrDist ErrDistMap] = Calc_Uncertainty_Err_Dist(pbs, dispvec, dx, dy);    
%% Feature extraction
    cnt = 1;
    for simil_measure = 1:6,   
        similarity = Calc_Similarity(tar, dsrc, simil_measure);
%         for i = 1:sz(similarity,1),
%             for j = 1:sz(similarity,2),
                similarityL = similarity; similarityR = similarityL; 
                similarityU = similarityL; similarityD = similarityL;
                similarityLU = similarityL; similarityLD = similarityL;
                similarityRU = similarityL; similarityRD = similarityL;
                similarityL(1:end-1,:) = similarity(2:end,:);
                similarityR(2:end,:) = similarity(1:end-1,:);
                similarityU(:,1:end-1) = similarity(:,2:end);
                similarityD(:,2:end) = similarity(:,1:end-1);
                similarityLU(1:end-1,1:end-1) = similarity(2:end,2:end);
                similarityLD(1:end-1,2:end) = similarity(2:end,1:end-1);
                similarityRU(2:end,1:end-1) = similarity(1:end-1,2:end);
                similarityRD(2:end,2:end) = similarity(1:end-1,1:end-1);
%         dterm = sort(dterm,3);
%         temp = dterm(:,:,end-3:end);
%         for ii = 1:4,
%            tempp = temp(:,:,ii);
           Input(1:size(S), cnt) = similarity(S); 
           Input(1:size(S), cnt+1) = similarityL(S); Input(1:size(S), cnt+2) = similarityR(S); 
           Input(1:size(S), cnt+3) = similarityU(S); Input(1:size(S), cnt+4) = similarityD(S); 
           Input(1:size(S), cnt+5) = similarityLU(S); Input(1:size(S), cnt+6) = similarityLD(S); 
           Input(1:size(S), cnt+7) = similarityRU(S); Input(1:size(S), cnt+8) = similarityRD(S);
           cnt = cnt + 9;
%         end
    end
% %     rad = 1;
% %     I1 = MIND_descriptor2d(src,rad);
% %     I2 = MIND_descriptor2d(tar,rad);
% %     for j = 1:4*(rad+1),
% %         II = I1(:,:,j);
% %         Input(1:size(S), cnt+j-1) = II(S);
% %         II = I2(:,:,j);
% %         Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
% %     end
% %     cnt = cnt + j + 4*(rad+1);
    rad = 2;
    I1 = MIND_descriptor2d(src,rad);
    I2 = MIND_descriptor2d(tar,rad);
    for j = 1:4*(rad+1),
        II = I1(:,:,j);
        Input(1:size(S), cnt+j-1) = II(S);
        II = I2(:,:,j);
        Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
    end
    cnt = cnt + j + 4*(rad+1);
% %     rad = 3;
% %     I1 = MIND_descriptor2d(src,rad);
% %     I2 = MIND_descriptor2d(tar,rad);
% %     for j = 1:4*(rad+1),
% %         II = I1(:,:,j);
% %         Input(1:size(S), cnt+j-1) = II(S);
% %         II = I2(:,:,j);
% %         Input(1:size(S), cnt+j+4*(rad+1)-1) = II(S);
% %     end
% %     cnt = cnt + j + 4*(rad+1);
% %     Input(1:size(S), cnt) = src(S);
% %     Input(1:size(S), cnt+1) = tar(S);
%     grads = gradient(src); gradt = gradient(tar);
%     Input(1:size(S), cnt) = grads(S);
%     Input(1:size(S), cnt+1) = gradt(S);
%     cnt = cnt+2;
%% Warp Smoothness
    [gxx gxy] = gradient(dx);
    [gyx gyy] = gradient(dy);
    Smooth_Val = gxx .* gxy - gyx .* gyy;

    Smooth_ValL = Smooth_Val; Smooth_ValR = Smooth_ValL; 
    Smooth_ValU = Smooth_ValL; Smooth_ValD = Smooth_ValL;
    Smooth_ValLU = Smooth_ValL; Smooth_ValLD = Smooth_ValL;
    Smooth_ValRU = Smooth_ValL; Smooth_ValRD = Smooth_ValL;
    Smooth_ValL(1:end-1,:) = Smooth_Val(2:end,:);
    Smooth_ValR(2:end,:) = Smooth_Val(1:end-1,:);
    Smooth_ValU(:,1:end-1) = Smooth_Val(:,2:end);
    Smooth_ValD(:,2:end) = Smooth_Val(:,1:end-1);
    Smooth_ValLU(1:end-1,1:end-1) = Smooth_Val(2:end,2:end);
    Smooth_ValLD(1:end-1,2:end) = Smooth_Val(2:end,1:end-1);
    Smooth_ValRU(2:end,1:end-1) = Smooth_Val(1:end-1,2:end);
    Smooth_ValRD(2:end,2:end) = Smooth_Val(1:end-1,1:end-1);
     
    Input(1:size(S), cnt) = Smooth_Val(S);
    Input(1:size(S), cnt+1) = Smooth_ValL(S); Input(1:size(S), cnt+2) = Smooth_ValR(S); 
    Input(1:size(S), cnt+3) = Smooth_ValU(S); Input(1:size(S), cnt+4) = Smooth_ValD(S); 
    Input(1:size(S), cnt+5) = Smooth_ValLU(S); Input(1:size(S), cnt+6) = Smooth_ValLD(S); 
    Input(1:size(S), cnt+7) = Smooth_ValRU(S); Input(1:size(S), cnt+8) = Smooth_ValRD(S);
%% Uncertainty
           
    Unc_ShannonL = Unc_Shannon; Unc_ShannonR = Unc_ShannonL; 
    Unc_ShannonU = Unc_ShannonL; Unc_ShannonD = Unc_ShannonL;
    Unc_ShannonLU = Unc_ShannonL; Unc_ShannonLD = Unc_ShannonL;
    Unc_ShannonRU = Unc_ShannonL; Unc_ShannonRD = Unc_ShannonL;
    Unc_ShannonL(1:end-1,:) = Unc_Shannon(2:end,:);
    Unc_ShannonR(2:end,:) = Unc_Shannon(1:end-1,:);
    Unc_ShannonU(:,1:end-1) = Unc_Shannon(:,2:end);
    Unc_ShannonD(:,2:end) = Unc_Shannon(:,1:end-1);
    Unc_ShannonLU(1:end-1,1:end-1) = Unc_Shannon(2:end,2:end);
    Unc_ShannonLD(1:end-1,2:end) = Unc_Shannon(2:end,1:end-1);
    Unc_ShannonRU(2:end,1:end-1) = Unc_Shannon(1:end-1,2:end);
    Unc_ShannonRD(2:end,2:end) = Unc_Shannon(1:end-1,1:end-1);
    
    cnt = cnt + 9;
    Input(1:size(S), cnt) = Unc_Shannon(S);
    Input(1:size(S), cnt+1) = Unc_ShannonL(S); Input(1:size(S), cnt+2) = Unc_ShannonR(S); 
    Input(1:size(S), cnt+3) = Unc_ShannonU(S); Input(1:size(S), cnt+4) = Unc_ShannonD(S); 
    Input(1:size(S), cnt+5) = Unc_ShannonLU(S); Input(1:size(S), cnt+6) = Unc_ShannonLD(S); 
    Input(1:size(S), cnt+7) = Unc_ShannonRU(S); Input(1:size(S), cnt+8) = Unc_ShannonRD(S);    
    
    ErrDistMapL = ErrDistMap; ErrDistMapR = ErrDistMapL; 
    ErrDistMapU = ErrDistMapL; ErrDistMapD = ErrDistMapL;
    ErrDistMapLU = ErrDistMapL; ErrDistMapLD = ErrDistMapL;
    ErrDistMapRU = ErrDistMapL; ErrDistMapRD = ErrDistMapL;
    ErrDistMapL(1:end-1,:) = ErrDistMap(2:end,:);
    ErrDistMapR(2:end,:) = ErrDistMap(1:end-1,:);
    ErrDistMapU(:,1:end-1) = ErrDistMap(:,2:end);
    ErrDistMapD(:,2:end) = ErrDistMap(:,1:end-1);
    ErrDistMapLU(1:end-1,1:end-1) = ErrDistMap(2:end,2:end);
    ErrDistMapLD(1:end-1,2:end) = ErrDistMap(2:end,1:end-1);
    ErrDistMapRU(2:end,1:end-1) = ErrDistMap(1:end-1,2:end);
    ErrDistMapRD(2:end,2:end) = ErrDistMap(1:end-1,1:end-1);
    
    cnt = cnt + 9;
    Input(1:size(S), cnt) = ErrDistMap(S);
    Input(1:size(S), cnt+1) = ErrDistMapL(S); Input(1:size(S), cnt+2) = ErrDistMapR(S); 
    Input(1:size(S), cnt+3) = ErrDistMapU(S); Input(1:size(S), cnt+4) = ErrDistMapD(S); 
    Input(1:size(S), cnt+5) = ErrDistMapLU(S); Input(1:size(S), cnt+6) = ErrDistMapLD(S); 
    Input(1:size(S), cnt+7) = ErrDistMapRU(S); Input(1:size(S), cnt+8) = ErrDistMapRD(S);
    cnt = cnt + 9;
  
    
%     Input(1:size(S), cnt+3) = Wcov(S);
%     Input(1:size(S), cnt+4) = SGauss(S);
%     Input(1:size(S), cnt+5) = randn(sz(S,1),1);
%     Input(1:size(S), cnt+5) = Warp_Err(S);
%     [all ~] = evalFld( cat(3, dx,dy), S );
%     X(1:size(S), cnt+5) = repmat(all.mea(1), [size(S) 1]);
%     X(1:size(S), cnt+6) = repmat(all.mea(2), [size(S) 1]);
%     X(1:size(S), cnt+7) = repmat(all.mea(3), [size(S) 1]);
%     
%     [all ~] = evalFld( cat(3, dx,dy), S, cat(3,ops.gx,ops.gy) );
%     X(1:size(S), cnt+5) = Unc_Kde(S);
%     X(1:size(S), cnt+6) = Unc_Knn(S);
    if nargin>12
%         Input(1:size(S), cnt+5) = randn(sz(S,1),1);
        Input(1:size(S), cnt) = Warp_Err(S);
    end 
    if nargin>11
       Output(1:size(S),1) = Warp_Err(S);
    else
        Output(1:size(S),1) = zeros(sz(S),1);
    end
end