function [Unc_Err_Dist Unc_Err_Dist_Map] = Calc_Uncertainty_Err_Dist2_3(X, dispvec, dx, dy, dz)    
    isz = sz(X);
    iszz = sz(dx);
    X = reshape( X, prod(iszz), [] );   
    Unc_Err_Dist = sum( X .* ( X * pdist2(dispvec, dispvec) ), 2 ); 
    if (nargin < 5)
        Disp = [dx(:) dy(:)];
    else
        Disp = [dx(:) dy(:) dz(:)];
    end

    Unc_Err_Dist_Map = sum( X .* pdist2(Disp, dispvec), 2);
    if (nargin < 5)
        Unc_Err_Dist = reshape( Unc_Err_Dist, isz(1), [] ); 
        Unc_Err_Dist_Map = reshape( Unc_Err_Dist_Map, isz(1), [] ); 
    else
        Unc_Err_Dist = reshape( Unc_Err_Dist, isz(1), isz(2), [] ); 
        Unc_Err_Dist_Map = reshape( Unc_Err_Dist_Map, isz(1), isz(2), [] ); 
    end

end