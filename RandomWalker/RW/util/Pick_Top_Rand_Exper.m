function [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper(src, invTx, invTy, mask, src_segment, maxd, ops, slands, tlands, MCase)
    
    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
%     mask1 = ( src > 0.1);
    mask2 = ( src_segment > 0);
    se = strel('disk',10);
    mask2 = imdilate(mask2,se);
    mask = mask .* mask2;
    pick_img = src .* mask;
    ten_perc = 1000;
    clear slands_new; clear tlands_new;
    clear slands_next1; clear tlands_next1;
    slands_next(1, :)  = round( (size(src, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
    slands_next(2, :)  = round( (size(src, 2) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
    cnt = 1; i = 1;
    while(1),
        if(pick_img(slands_next(1,i), slands_next(2,i)) > 0 && cnt <= ops.num_seed)
            slands_new(:,cnt) = slands_next(:, i); cnt = cnt+1;
        end
        i = i+1;
        if(cnt > ops.num_seed)
            break
        end
    end
    isz = size(src);
    inds = sub2ind(isz, slands_new(1,:), slands_new(2,:));
    if(MCase == 1)
        nx=invTx(inds);% + .5 * randn(1, size(slands_new,2));
        ny=invTy(inds);% + .5 * randn(1, size(slands_new,2));
    else
        nx=-invTx(inds);% + .5 * randn(1, size(slands_new,2));
        ny=-invTy(inds);% + .5 * randn(1, size(slands_new,2));
    end    
    tlands_new(1,:) =  mx( inds)+nx; % Ty(indexs) -Tx no, -Ty no, +Ty yn
    tlands_new(2,:) =  my( inds)+ny; % Tx(indexs)  
    
   
%         nseeds = ten_perc;

    length = size(slands,2);
    adding_length = size(slands_new,2);
    slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
    tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
%     tlands(1,1) = mx(inds(1))-nx(1);
%     tlands(2,1) = my(inds(1))-ny(1);
    nseeds = size(slands,2);
end

% function [gx gy]= get_gradient ( I1 )
% 
%      [gx gy]=gradient( I1 );
% 
%      nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
%      gx=gx ./ nrm;
%      gy=gy ./ nrm;
% end
