% path1 = 'H:\data\UCLA_LONI\LPBA40\native_space\S';
load('Y:\students_less\tlotfima\RWRegistration_Final3D\data\original_data\6_fld.mat')
path1 = 'Y:\data\UCLA_LONI\LPBA40\native_space\S';
size_x = 256; size_y = 124; size_z = 256; Type = 'float';
i = 6;
filename = [path1 '0' num2str(i) '\S0' num2str(i) '.raw'];
fd=fopen(filename);
I=fread(fd,size_x *size_y *size_z,Type);   % Type : 'float', 'short',...
% I=fread(fd,size_x *size_y *size_z,'Type');   % Type : 'float', 'short',...
fclose(fd);
I=reshape(I,[size_x size_y size_z]);    
I1 = imrotate(permute(I, [1 3 2]),90);

filename = [path1 '0' num2str(i) '\tissue\S0' num2str(i) '.raw'];
fd=fopen(filename);
seg=fread(fd,size_x *size_y *size_z,Type);   % Type : 'float', 'short',...
% I=fread(fd,size_x *size_y *size_z,'Type');   % Type : 'float', 'short',...
fclose(fd);
seg=reshape(seg,[size_x size_y size_z]);
seg1 = imrotate(permute(seg, [1 3 2]),90);
clear I; clear seg;
I = I1(:,:,55); seg = seg1(:,:,55);
isz = size(I);

u2 = permute(u2, [2 3 1]); v2 = permute(v2, [2 3 1]); w2 = permute(w2, [2 3 1]);
u2_cut = imresize(u2(:,:,150), [size(I,1) size(I,2)]);
v2_cut = imresize(v2(:,:,150), [size(I,1) size(I,2)]);
w2_cut = imresize(w2(:,:,150), [size(I,1) size(I,2)]);
% dockf; plot(unique(u2_cut));
[mx my] = ndgrid(1:isz(1), 1:isz(2)); 
IWF = interp2( my, mx, I, my + v2_cut, mx + u2_cut,  '*linear',0); 
src_seg = interp2( my, mx, seg, my + v2_cut, mx + u2_cut,  '*linear',0); 

TxW = u2_cut; TyW = v2_cut;

D(:,:,1) = TxW; D(:,:,2) = TyW;
invD = invertDefField(D);
invTxW = invD(:,:,1); invTyW = invD(:,:,2);

dockf;quiverR(zeros(size(TyW)), zeros(size(TxW)), TyW, TxW, 20, 2, 'c' ), title('ground truth');
dockf;quiverR(zeros(size(invTyW)), zeros(size(invTxW)), invTyW, invTxW, 20, 2, 'c' ), title('ground truth');

save '../data/Diffusion.mat' I IWF seg src_seg TxW TyW invTxW invTyW