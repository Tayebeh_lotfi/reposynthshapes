function dispvec = Polar_Uniform(maxD, res)
    nrads=40; % res/maxD;%40;
%         maxD = round(masqrt(ops.g.^2+ops.gy(.^2)));
    dispvec=zeros( nrads*maxD, 2); 
    rr=1;
    for r=1:maxD
        nrads = r*6;
        [px py ]=pol2cart( 0:2*pi/ (nrads-1) :2*pi , r );            
        dispvec( rr:rr+len(px)-1, : )   =  [px; py]';
        rr=rr+len(px);
    end
end