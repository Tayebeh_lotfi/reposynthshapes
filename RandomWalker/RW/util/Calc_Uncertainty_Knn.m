function Knn_Ent = Calc_Uncertainty_Knn(pbs, dispvec, opt)
%% Defining, and Calculaing variables
    pbs = reshape(pbs, prod(opt.isz), []);
    nlabs = size(pbs,2);
    Knn_Ent = zeros(opt.isz); 
    epsilon = 10e-8; dimension = size(dispvec,2); %nlabs-1; gam = 0.5772; 
    S = ( opt.mask > 0 ); inds = find(S==1);
    for pix = 1:len(inds),               
%% Weighted Nearest Neighbor Calculation Using MCMC Sampling
        dispvec_rep = replicate_vect_pbs(dispvec, pbs, opt, pix);
        dispvec_rep = dispvec_rep';
        nlabs_rep = size(dispvec_rep,1);
        opt.k = nlabs_rep;
        if(~isempty(dispvec_rep))
            [IDX D] = knnsearch(dispvec_rep, dispvec_rep, 'k', opt.k,'distance','euclidean'); %'minkowski','p',5); );
            weight_neighbor = 1:opt.k;
%             distance_k = sum(log(D+epsilon),1);
%             Unc5(inds(pix)) = mean(sum(log2((gamma(d/2+1) .* weight_neighbor) ./ ( (pi .^ (d/2) * nlabs_rep) .* (D+epsilon) )),2));
%             Unc5(inds(pix)) = (dimension ./ nlabs_rep) .* distance_k(opt.k) + log(pi.^(dimension/2) / gamma(dimension/2+1) ) ...
%                 - ( psi(opt.k)) + log(nlabs) ;
            Knn_Ent(inds(pix)) = sum((dimension ./ nlabs_rep) .* sum(log(D+epsilon),1) + log(pi.^(dimension/2) / gamma(dimension/2+1) ) ...
                - ( psi(weight_neighbor)) + log(nlabs)) ;            
        else
            Knn_Ent(inds(pix)) = 0;
        end
    end
    Knn_Ent = (reshape(Knn_Ent, [opt.isz(1) opt.isz(2)]));
    Knn_Ent(isnan(Knn_Ent)) = 0;
end