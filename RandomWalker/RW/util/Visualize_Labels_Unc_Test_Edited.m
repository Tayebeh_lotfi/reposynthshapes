function Visualize_Labels_Unc_Test_Edited(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, GMM2_avg, Unc_Kde, Unc_Knn, Unc_ExpEr, Unc_ExpEr_Map, data_mode) % Unc_Delta,

%     nlabs = nlabs_size * nlabs_size;
%     epsilon(1) = 0;
    %     sample_num = 8;
%     eps_max = 1/nlabs;
    if (data_mode == 2)    
        nlabs = 5 * 5;
    else
        nlabs = 5 * 5 * 5;
    end        
    c = jet(140);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
    sample_num = 8;
    eps_min = 0;
    eps_max = 1/nlabs;
    for i = 1:sample_num,
        epsilon(i) = eps_min + (eps_max - eps_min)/sample_num * i;
    end
    elmn = 8; %8
    for elmn = 2:8,
%     Unc_str = {'ShEnt' 'WProb' 'WCov' 'GDist' 'GMM, 36 Gaussians' 'KDE' 'KNN' 'Delta'};
    counter1 = [1 2 3 5 7];
    counter2 = [1 2 3 4 10 11];
%     imm = floor(linspace(2, nlabs_size,3));
    for Uncert_ = 1:8, %7, % 
       switch Uncert_
           case 1
               Unc = Unc_Shannon;
%            case 2
%                Unc = Unc_Shannon_Sprd;
           case 2
               Unc = Wcov;
           case 3
               Unc = SGauss;
           case 4
               Unc = GMM2_avg;
          case 5
               Unc = Unc_Kde;
           case 6
               Unc = Unc_Knn;  
           case 7
               Unc = Unc_ExpEr;
           case 8
               Unc = Unc_ExpEr_Map;
       end
       Unc = mat2gray(Unc);
       if (data_mode == 2)
           for i = 1:len(counter1), %11, %7, %
               Unc_mat(Uncert_,i) = Unc((counter1(i)-1)*8+elmn);
        %        xlswrite('mydata.xls', Unc(1), 'name', Unc_str{Uncert_}); 
        %        xlswrite('mydata.xls', Unc((i-1)*8+elmn), 'name', Unc_str{Uncert_}); Unc(i*8+elmn)
           end
%            Unc_mat(Uncert_,len(counter1)+1) = Unc(1); %12
       else
          for i = 1:len(counter2), %11, %7, %
           Unc_mat(Uncert_,i) = Unc((counter2(i)-1)*8+elmn);
          end
%           Unc_mat(Uncert_,len(counter2)+1) = Unc(1); %12
       end
       Unc_mat(Uncert_,:) = mat2gray(Unc_mat(Uncert_,:));
    end
%     if(data_mode == 2)
    figure, %, subplot 121,
%     else
%         subplot 122,
%     end
    for Uncert_ = 1:8, %8, % 
        if(data_mode == 2),
            hold on, plot(1:len(counter1),Unc_mat(Uncert_,:),'Color', c(Uncert_*17,:), 'Marker', shape{Uncert_}, 'LineWidth',6, 'MarkerSize', 10), %1:12,
            set(gca,'FontSize',45), xlim([1 len(counter1)]) %12
        else
            hold on, plot(1:len(counter2),Unc_mat(Uncert_,:),'Color', c(Uncert_*17,:), 'Marker', shape{Uncert_}, 'LineWidth',6, 'MarkerSize', 10), %1:12,
            set(gca,'FontSize',45), xlim([1 len(counter2)]) %12
        end
    end,
    if(data_mode == 2)
        title('Label Space: 2D'); %, Original Probabilities
        AX=legend('ShEnt', 'WCov', 'GDist', 'GMM', 'KDE', 'KNN', 'ExpEr', 'ExpErMAP', 'Location', 'SouthEastOutSide'); % 'Delta', 'WProb', 
    else
        title('Label Space: 3D');
        AX=legend('ShEnt', 'WCov', 'GDist', 'GMM', 'KDE', 'KNN', 'ExpEr', 'ExpErMAP', 'Location', 'SouthEastOutSide'); % 'Delta', 'WProb',
    end        
    xlabel('Case'), ylabel('Uncertainty Value'),
%     if(data_mode == 3)
        LEG = findobj(AX,'type','text');
%     end
    set(gca,'FontSize',30)
    grid on,
    end
end
