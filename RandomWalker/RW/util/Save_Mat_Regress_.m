% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_Regress_( AllErr, savepath, Max_Iterations, trial_num, set_num, str, noise_sigma)

    file_name = {'AllErr'};    
    for i = 1:1,
        save (num2str([savepath 'AL_SL/Regression/All_Methods/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_unc' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
    end 
end