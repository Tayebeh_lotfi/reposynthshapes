function FEVal = local_corr_fun(x)
    FEVal = corrcoef( reshape(x.data(:,:,1),1,[]), reshape(x.data(:,:,2),1,[])  );
%     FEVal = corrcoef( x.data(:,1), x.data(:,2) );
end