function landmarks = pick_points(landmarks, candidates, ops, Values)

    nMax = ops.num_seed;
    land_length = size(landmarks,2);
    mn = mnx(Values); %(find(ops.mask))
    Values = (Values - mn(1)) ./ ( mn(2) - mn(1) );
    for i = 1: nMax
        land_mean = sum(landmarks, 2) ./ size(landmarks, 2);
%         [IDX Dist] = knnsearch(landmarks', candidates', 'k', size(candidates,2), 'distance', 'euclidean');
        Dist = dist2(landmarks', candidates'); %land_mean

%         [indx ~] = find(Dist == 0);
%         candidates(:,indx) = [];
%         Dist(indx,:) = [];
        candidate_inds = sub2ind( size(Values), candidates(1,:), candidates(2,:));
%         Dist = mat2gray(Dist);
%         Values = mat2gray(Values);

%         min_ = repmat(min(Dist,[],2), 1, land_length);
%         max_ = repmat(max(Dist,[],2), 1, land_length);
%         Dist = (Dist - min_) ./ (max_ - min_);
%         mn = [min(Dist,[],2) max(Dist,[],2)];
        mn = mnx(min(Dist,[], 1)); %mnx(std(Dist,0,1));
         if( numel(mn) ~= 0 && mn(1) ~= mn(2))
%         cand_land_Dist = ( std(Dist, 0, 1) - mn(1)) ./ ( mn(2) - mn(1) + 1 );
            cand_land_Dist = ( min(Dist, [], 1) - mn(1)) ./ ( mn(2) - mn(1) + 1 );
            cost = ops.alpha .* (Values(candidate_inds)) + (1-ops.alpha) .* cand_land_Dist; %sum(Dist, 2) ./ max_dist; ( (sum(Dist(:))/2 + sum(diag(Dist))/2)); % * max_dist );
            [val ind] = max(cost); 
            landmarks(:,land_length+i) = candidates(:,ind);
            candidates(:, ind) = [];
         else
             break
         end
    end
    
end
