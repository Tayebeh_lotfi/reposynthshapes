function [total_pix_err_grnd total_pix_err] = Calc_Warp_Err_All(tar, dsrc, ground_src)

%      [u, v]=meshgrid(1:isz(1), 1:isz(2));
%      [x, y] = tforminv(tform, u, v);
%      ground_tar = interp2(u,v,src,x,y);
%      ground_tar(isnan(ground_tar)) = 0;
%         
%       tlands2 = tform.tdata.T' * [slands; ones(1,size(slands,2))];
%       tlands1 = round(tlands2(1:2,:));
        
%       tlands2 = tform.tdata.T' * [slands; ones(1,size(slands,2))];
%       tlands1 = round(tlands2(1:2,:));
%         
%         total_err = sum(sum((tlands1(1,cross+1:end) - tlands(1,cross+1:end)) .^2));


        total_pix_err_grnd = sum(sqrt((ground_src(:) - dsrc(:)) .^2));% / ( isz(1) * isz(2) ) );
        total_pix_err = sum(sqrt((tar(:) - dsrc(:)) .^2));% / ( isz(1) * isz(2) ) );


end
