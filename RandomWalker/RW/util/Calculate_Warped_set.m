function Warped_set = Calculate_Warped_set(src3D, Label_set, Displace_field, img_number, max_iteration)
    [mx my] = ndgrid(1:size(src3D,2), 1:size(src3D,1));
    for img_num = 1:img_number,
        for iter = 1:max_iteration,  
            nmx = mx(:) + Label_set.Rand(Displace_field.Rand(:,iter, img_num),1);
            nmy = my(:) + Label_set.Rand(Displace_field.Rand(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.Rand(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');

            nmx = mx(:) + Label_set.RandG(Displace_field.RandG(:,iter, img_num),1);
            nmy = my(:) + Label_set.RandG(Displace_field.RandG(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.RandG(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');

            nmx = mx(:) + Label_set.AL(Displace_field.AL(:,iter, img_num),1);
            nmy = my(:) + Label_set.AL(Displace_field.AL(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.AL(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');

            nmx = mx(:) + Label_set.SL_All(Displace_field.SL_All(:,iter, img_num),1);
            nmy = my(:) + Label_set.SL_All(Displace_field.SL_All(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.SL_All(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');

            nmx = mx(:) + Label_set.SL100(Displace_field.SL100(:,iter, img_num),1);
            nmy = my(:) + Label_set.SL100(Displace_field.SL100(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.SL100(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');

            nmx = mx(:) + Label_set.SL_Rand(Displace_field.SL_Rand(:,iter, img_num),1);
            nmy = my(:) + Label_set.SL_Rand(Displace_field.SL_Rand(:,iter, img_num),2);
            nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
            nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
            Warped_set.SL_Rand(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');
            
            if iter < 3,    
                nmx = mx(:) + Label_set.AL_Base(Displace_field.AL_Base(:,iter, img_num),1);
                nmy = my(:) + Label_set.AL_Base(Displace_field.AL_Base(:,iter, img_num),2);
                nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
                nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
                Warped_set.AL_Base(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');
            else
                nmx = mx(:) + Label_set.AL_Base(Displace_field.AL_Base(:,2, img_num),1);
                nmy = my(:) + Label_set.AL_Base(Displace_field.AL_Base(:,2, img_num),2);
                nmx = reshape(nmx,[size(src3D,1) size(src3D,2)]);
                nmy = reshape(nmy,[size(src3D,1) size(src3D,2)]);
                Warped_set.AL_Base(:,:,img_num,iter) = interp2(my, mx, src3D(:,:,img_num), nmy, nmx, '*linear');
            end
        end
    end
end