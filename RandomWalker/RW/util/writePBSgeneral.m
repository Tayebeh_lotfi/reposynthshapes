function  writepbsgeneral( cmd , pbsf, ncpus, hrs, memsz , matlab, arch )
% function  writePBSgeneral(  cmd , pbs_file,  ncpus, nHrs, mem_sz, [matlab script?], [arch] )
%   pbs_file: pbs filename 
%   cmd: actual cmd to be executed on cluster
%   ncpus: increase if code is multi-threaded
%  
%   Note, other things that might need changing:
%           - arch = architecture type: i386, x86_64 or ia64 
% 
%   v1.0: release 1
%   v1.1: added version matlab
% 
% if strcmp( computer, 'GLNX')
%     root = '~';
% else
%     root = 'Z:';
% end
% writepbsgeneral('cd /cs/grad1/tlotfima/research/RWRegistration_Final/RWIR;  \n  Tayebeh_RWImgReg' , [root '/research/RWRegistration_Final/sample1.pbs'], 1, 15, '2gb', 1);
if 0
%% sample
    d=15
writepbsgeneral(sprintf( ['sl=%d;calc=0; res_filename= ''~/results/simlearn-stats/scp_slice%d'';'...
    '\ncd ~/code/matlab/convienience;\nmypaths.m; \n\nSLscript.m'], d,d),...
    ['x:/results/srp2_slice15.pbs'], 1, 5, '1gb', 1 )

    
end

if nargin < 7 
    arch = 'x86_64';
end
fd = fopen( pbsf, 'w' );
fwrite( fd,  sprintf('#! /bin/bash\n'));
fwrite( fd,  sprintf('#PBS -S /bin/bash\n'));
fwrite( fd,  sprintf('#PBS -l software=matlab\n'));
fwrite( fd,  sprintf('#PBS -l nodes=1:ppn = 1\n'));
if nargin==5    || matlab == 0
    fwrite( fd,  sprintf('#PBS -l arch=%s\n#PBS -l nodes=1,ncpus=%d,mem=%s' , arch , ncpus,memsz), 'char' );
    fwrite( fd,  sprintf( '\n#PBS -M tlotfima@sfu.ca\n#PBS -m a'), 'char');
    fwrite( fd, sprintf( '\n#PBS -l walltime=%d:00:00\n', hrs), 'char' );
    fwrite( fd, sprintf('\n%s \n', cmd ), 'char' );
else  
    fwrite( fd,  sprintf('#PBS -l arch=%s\n#PBS -l nodes=1:matlab,ncpus=%d,mem=%s' , arch, ncpus,memsz), 'char' );
    fwrite( fd,  sprintf( '\n#PBS -M tlotfima@sfu.ca\n#PBS -m a'), 'char');
    fwrite( fd, sprintf( '\n#PBS -l walltime=%d:00:00\n\n', hrs), 'char' );
    if version >1
        switch version
        case 8
        fwrite( fd,    'cd /usr/local-rscn/matlab2008a');
        case 9
        fwrite( fd,    'cd /usr/local-rscn/matlab2009a');
        case 10
        fwrite( fd,    'cd /usr/local-rscn/matlab2010a');
        end
    end
    fwrite( fd, 'matlab -nosplash -nodisplay << EOF');
    fwrite( fd, sprintf('\n%s \n', cmd ), 'char' );
    fwrite( fd, sprintf('\nexit\nEOF'), 'char');
end

fclose(fd); 