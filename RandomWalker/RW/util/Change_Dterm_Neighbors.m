function dterm = Change_Dterm_Neighbors(src, slands, dterm, dterm_mode)
    isz = sz(src);
    iszz = sz(dterm);
    nlabs = iszz(end);
    inds = sub2ind(sz(src), round(slands(1,:)), round(slands(2,:)));
    dterm = reshape(dterm, prod(isz), [] );
    for i = 1:sz(slands, 2)
        if (slands(1, i) + 1) < isz(1)-1
            cand(1, 1) = slands(1, i) + 1; cand(2, 1) = slands(2, i);
        end
        if (slands(2, i) + 1) < isz(2)-1
            cand(1, 2) = slands(1, i); cand(2, 2) = slands(2, i) + 1;
        end
        if (slands(1, i) - 1) > 1
            cand(1, 3) = slands(1, i) - 1; cand(2, 3) = slands(2, i);
        end
        if (slands(2, i) - 1) > 1
            cand(1, 4) = slands(1, i); cand(2, 4) = slands(2, i) - 1;
        end
        if ( (slands(1, i) + 1) < isz(1)-1 && (slands(2, i) - 1) > 1 )
            cand(1, 5) = slands(1, i) + 1; cand(2, 5) = slands(2, i) - 1;
        end
        if ( (slands(2, i) + 1) < isz(2)-1 && (slands(1, i) - 1) > 1 )
            cand(1, 6) = slands(1, i) - 1; cand(2, 6) = slands(2, i) + 1;
        end
        if ( (slands(1, i) - 1)> 1 && (slands(2, i) - 1) > 1 )
            cand(1, 7) = slands(1, i) - 1; cand(2, 7) = slands(2, i) - 1;
        end
        if ( (slands(1, i) + 1) < isz(1)-1 && (slands(2, i) + 1) < isz(2)-1 )
            cand(1, 8) = slands(1, i) + 1; cand(2, 8) = slands(2, i) + 1;
        end
        cand_ind = sub2ind(sz(src), round(cand(1,:)), round(cand(2,:)));
        n = find(dterm(inds(i),:)==0);  
        for mm = 1:sz(cand_ind,2)
            switch dterm_mode
                case 1
                    dterm(cand_ind(mm), :) = Inf;
                case 2
                    dterm(cand_ind(mm), :) = 1;
            end
            dterm(cand_ind(mm), n) = 0;
        end
    end
    dterm = reshape(dterm, [isz(1) isz(2) nlabs]);
end
