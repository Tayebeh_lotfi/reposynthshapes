function Visualize_MoG_KDE_Edited(dispvec, pbs, opt)
    mn = mnx(dispvec);
    epsilon = 1e-5; % 0.2;
    counter_gmm = 1; pix_len_gmm = 4;  pix_len_kde =1;
    Case = 1;
    for pix = 1:8:57,
        if (pix ~=1 && pix ~= 33 && pix ~= 49 && pix ~= 57),
        pix1 = pix;
        pix1 = pix - 1; 
%         Case = 4; 
        dispvec_rep = replicate_vect_pbs(dispvec, pbs, opt.accuracy, pix1);
        dispvec_rep = dispvec_rep';
        clear S;
        for MoGk = [4,9,25],
            opts1 = statset( 'Display', 'final', 'MaxIter', opt.iter1, 'TolTypeFun', 'rel', 'TolFun', 1e-10, 'TolTypeX', 'rel', 'TolX', 1e-10 );
%             step = round((size(dispvec,1)-1)/(MoGk-1));
            
%%            
            S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
                [x y] = meshgrid(linspace(mn(1), mn(2), sqrt(MoGk)), linspace(mn(1), mn(2), sqrt(MoGk)));
                S.mu = ([x(:), y(:)]);
                [IDX D] = knnsearch(S.mu, S.mu, 'k', 2, 'distance','euclidean');
                sig = D(1,2)/(6);
            
            S.Sigma = repmat([sig 0; 0 sig], [1 1 size(S.mu,1)]);
            S.PComponents(1:size(S.mu,1)) = 1/size(S.mu,1);
%             figure, plot(S.mu(:,2), S.mu(:,1), 'r*'),xlim([mn(1) mn(2)]), ylim([mn(1) mn(2)])
            obj = gmdistribution.fit(dispvec_rep,size(S.mu,1),'Regularize',opt.epsilon, 'Options', opts1, 'Start', S);
%             figure, plot(obj.mu(:,2), obj.mu(:,1), 'r*'),xlim([mn(1) mn(2)]), ylim([mn(1) mn(2)])
            [X1,X2] = meshgrid(linspace(mn(1)-epsilon,mn(2)+epsilon,25)',linspace(mn(1)-epsilon,mn(2)+epsilon,25)');
            X = [X1(:) X2(:)];
            figure(1),
            subplot(pix_len_gmm, 6, counter_gmm),
            for i = 1:MoGk,
                p = mvnpdf(X,S.mu(i,:),S.Sigma(:,:,i));
                hold on, surf(X1,X2,reshape(p,25,25)), title(['Case', num2str(Case), ', ' num2str(MoGk), 'Gaussian Mixture Model'], 'FontSize', 25),
                xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]); 
                set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);
            end
            
            
%             subplot(2,2,2),
%             for i = 1:MoGk,
%                 p = mvnpdf(X,obj.mu(i,:),obj.Sigma(:,:,i));
%                 hold on, surf(X1,X2,reshape(p,25,25)), title(['Result of gmdist.fit, pixel: ', num2str(pix), ', ' num2str(MoGk), ' Gaussians']),
%                 xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]);
%            end

            %subplot(2,2,3), ezcontour(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]), title(['MoG fit, pixel: ', num2str(pix), ', ' num2str(MoGk), ' Gaussians']),
            %xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]);
            figure(1),
            subplot(pix_len_gmm, 6, counter_gmm+1), ezsurf(@(x,y)pdf(obj,[x y]),[mn(1) mn(2)],[mn(1) mn(2)]), %title(['Case', num2str(Case), ', ' num2str(MoGk), 'GMM'], 'FontSize', 25), %subplot(1,2,2), 
            title('GMM Fit');
             set(gca,'ZTick',[]); xlim([mn(1)-epsilon mn(2)+epsilon]),ylim([mn(1)-epsilon mn(2)+epsilon]); 
            set(gca,'YTick',[]); set(gca,'XTick',[]);
            counter_gmm = counter_gmm + 2;
            
            figure,quiver(zeros(sz(X1(1:10:end)'),1),zeros(sz(X2(1:10:end)'),1),X1(1:10:end)', X2(1:10:end)'); hold on, surf(X1, X2, prob1)
            xlabel('x', 'FontSize', 25), ylabel('y', 'FontSize', 25),zlabel('prob (p)', 'FontSize', 25),
            
            
        end
%         counter_gmm = 1;
        clear S; 
%% KDE test 
        figure(2),
        clear S;
        MoGk = size(dispvec,1);
        S.mu = zeros(MoGk, 2); S.Sigma = zeros(2,2,MoGk);
        S.mu = dispvec;
        [IDX D] = knnsearch(S.mu, S.mu, 'k', 2,'distance','euclidean'); %'minkowski','p',5); );            
        sig = D(1,2)/(6);
        S.Sigma(:,:,1:size(dispvec,1)) = repmat([sig 0; 0 sig], [1 1 size(dispvec,1)]);
        obj_kde = gmdistribution(S.mu, S.Sigma, pbs(pix1,:));
%         subplot(pix_len_kde, 4, counter_kde), ezcontour(@(x,y)pdf(obj_kde,[x y]),[mn(1)-epsilon mn(2)+epsilon],[mn(1)-epsilon mn(2)+epsilon]), title(['KDE fit, pxl: ', num2str(pix) ' sigma(1,1) = ' num2str(i*10^-2)], 'FontSize', 25);
%         set(gca,'YTick',[]); set(gca,'XTick',[]);
        subplot(pix_len_kde, 4, Case), ezsurf(@(x,y)pdf(obj_kde,[x y]),[mn(1)-epsilon mn(2)+epsilon],[mn(1)-epsilon mn(2)+epsilon]), title(['Case', num2str(Case)], 'FontSize', 25); % ' sigma(1,1) = ' num2str(i*10^-2)
        set(gca,'ZTick',[]); set(gca,'YTick',[]); set(gca,'XTick',[]);
%         counter_kde = counter_kde + 1; 
        if (pix1 == 1), Case = 0; end
        Case = Case + 1;
        end
    end    
end
