function dterm = Change_Dterm(src, slands, dterm, nlabs, nlabs2, dterm_mode)
%     epsilon = 1e-10;
    isz = size(src);
    inds = sub2ind(size(src), slands(1,:), slands(2,:));
    dterm = reshape(dterm, prod(isz), [] );
    for i = 1: nlabs2,
        switch dterm_mode
            case 1
                dterm(inds(i), :) = Inf;
            case 2
                dterm(inds(i), :) = 1;
        end
        dterm(inds(i), nlabs-nlabs2+i) = 0;
    end
    dterm = reshape(dterm, [isz(1) isz(2) nlabs]);
end
