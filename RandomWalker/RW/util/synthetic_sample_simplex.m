function [synth_vect points] = synthetic_sample_simplex(nsample,nlabs)
    synth_vect = round(110*rand(nlabs,2))/10 .* (-1) .^ round(100*rand(nlabs,2));
    points = gamrnd(1,1,nsample,nlabs);
    points = points./repmat(sum(points,2),1,nlabs);
end
