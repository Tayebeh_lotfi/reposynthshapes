function [tdelta Unc] = Calc_Uncertainty_Phase_Dist_Prob(isz, pbs, dispvec, mask, w)
%     load func_variables;
    pbs = reshape(pbs, prod(isz), []);
    iszz = size(pbs);
    nlabs = iszz(end);
    Unc = zeros(isz);   
    S = ( mask > 0 );
    inds = find(S==1);
    epsilon = 0;
    G = (dist2(dispvec, dispvec) + epsilon);
    elmn = 25;
    mG = mnx(G); a(1) = mG(2) - mG(1);
    tdelta = 0;
    tic
    for pix = 1:len(inds),
        prob = sqz(pbs(inds(pix),:))';
%         mp = mnx(prob);
%         a(2) = mp(2) - mp(1);
%         [Val Ix] = sort(prob);
%         prob = prob(Ix);
%         G = G(Ix,:);

        Dist = ( (repmat(prob,1, size(prob,1)))  .* (repmat(prob', size(prob,1), 1))  ) + epsilon;      
%        Unc(inds(pix)) = sum(sum( ( (w(1) * G(1:elmn,:)) .* (w(2) * Dist(1:elmn,:)) .* (w(3) * Angle(1:elmn,:)) ) )) ./ 2;
        Unc(inds(pix)) = sum ( sum( (G(:) .^ w(1)) .* (Dist(:) .^ w(2)) ) ) ./ 2; %.* (w(3) * Angle(:))
    end
%     Unc = (reshape(Unc, [isz(1) isz(2)]));
    Unc = mat2gray(reshape(Unc, [isz(1) isz(2)]));  
    tdelta = tdelta + toc;
end



% num_sample = 8;
% [dy dx] = ndgrid(-2:2, -2:2);
% dispvec = [dx(:) dy(:)];
% ss = sz(dispvec);
% pbs = fix(10*rand(num_sample, ss(1)));
% pbs = pbs ./ repmat(sum(pbs,2), [1 ss(1)]);
% mask = ones(num_sample,1);
% w = [3.3 2.2];
% isz = [num_sample 1];






