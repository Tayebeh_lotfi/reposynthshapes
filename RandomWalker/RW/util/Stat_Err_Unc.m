function Stat_Err_Unc(Shannon, Wprob, Kde, Knn, Warp_Err, Warp_Err_Sprd, mask, Err_Mode, img_num, MCase, Noise)

%     if (nargin < 9)
%         err_entrpy = Warp_Err;
%          Err_Mode = 1;
%     else
%         Err_Mode = 2;
%     end
    if (nargin < 11)
        Noise = 0;
    end
%     my_str = {'Shannon' 'Wprob', 'Kde', 'Knn'};
    my_str2 = {'Error' 'Modified Error'};
    c = jet(120);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
    dockf;
    for Uncert_ = 2:2,
        switch (Uncert_)
            case 1
                Unc = Shannon(find(mask));
            case 2
                Unc = Wprob(find(mask));
            case 3
                 Unc = Kde(find(mask));
            case 4
                 Unc = Knn(find(mask));
        end        
        switch (Err_Mode)
            case 1
                Err = Warp_Err(find(mask));
            case 2
            Err = Warp_Err_Sprd(find(mask));
        end
    
        bin_err = 100; mn = mnx(Unc);
        bin_cnt = linspace(mn(1), mn(2),bin_err);

        for bin_counter = 1:len(bin_cnt)-1,
            [~, ixx] = histc(Unc, bin_cnt(bin_counter:bin_counter+1));
            mean_Err(bin_counter) = mean(Err(find(ixx~=0)));% Err .* ixx
            std_Err(bin_counter) = std(Err(find(ixx~=0))); % Err .* ixx
        end
        hold on, errorbar(mean_Err, std_Err, 'Color', c(Uncert_*30,:), 'Marker', shape{Uncert_}, 'LineWidth',2, 'MarkerSize', 10),
        A3 = ylabel(['Average of ', my_str2{Err_Mode}]);
        set(A3, 'FontSize',25) 
    end
    %title(['Average Error Using ' my_str{1}, 'with Noise = ', num2str(Noise)]),
    %ylabel(['Average ', my_str2{Err_Mode}]);    
    if(MCase == 1),
        A1 = title(['BSpline Warping, Image ', num2str(img_num)]);
    else
        A1 = title(['Diffusion Warping, Image ', num2str(img_num)]);
    end
    set(A1, 'FontSize',25)
    
    A2 = legend('Shannon', 'Wprob', 'Kde', 'Knn', 'Location', 'SouthEastOutside');
    LEG = findobj(A2,'type','text');
    set(LEG,'FontSize',25)
end

