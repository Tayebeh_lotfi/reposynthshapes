function [p rsq] = lin_regress(x, y, reg_mod)% Use polyfit to compute a linear regression that predicts y from x:

    p = polyfit(x,y,reg_mod); %3
    %  p(1) is the slope and p(2) is the intercept of the linear predictor. You can also obtain regression coefficients using the Basic Fitting GUI.
    % Call polyval to use p to predict y, calling the result yfit:

    yfit = polyval(p,x);
    % Using polyval saves you from typing the fit equation yourself, which in this case looks like:
    switch reg_mod
        case 1
            yfit =  p(1) * x + p(2);
        case 3
            yfit =  p(1) * x.^3 + p(2) * x.^2 + p(3) * x + p(4);
    end
    % Compute the residual values as a vector signed numbers:

    yresid = y - yfit;
    % Square the residuals and total them obtain the residual sum of squares:

    SSresid = sum(yresid.^2);
    % Compute the total sum of squares of y by multiplying the variance of y by the number of observations minus 1:

    SStotal = (length(y)-1) * var(y);
    % Compute R2 using the formula given in the introduction of this topic:

    rsq = 1 - SSresid/SStotal;
end