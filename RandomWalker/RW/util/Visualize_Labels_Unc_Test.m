% function Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss, GMM1_l, GMM1_u, GMM1_avg, GMM1_disc, GMM2_l, ...
%             GMM2_u, GMM2_avg, GMM2_disc, GMM3_l, GMM3_u, GMM3_avg, GMM3_disc, Unc_Kde, Unc_Knn, ...
%             nlabs_size, sample_num, Unc_str, data_mode)
function Visualize_Labels_Unc_Test(Unc_Shannon, Unc_Shannon_Sprd, Wcov, SGauss,GMM2_avg, Unc_Kde, Unc_Knn, Unc_Err, Unc_Err_Map, data_mode) % Unc_Delta,

%     nlabs = nlabs_size * nlabs_size;
%     epsilon(1) = 0;
    %     sample_num = 8;
%     eps_max = 1/nlabs;
    c = jet(140);
    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
    elmn = 8; %8
%     Unc_str = {'ShEnt' 'WProb' 'WCov' 'GDist' 'GMM, 36 Gaussians' 'KDE' 'KNN' 'Delta'};
    counter = [1 2 3 5 7];
%     imm = floor(linspace(2, nlabs_size,3));
    for Uncert_ = 1:7, % 8, %
       switch Uncert_
           case 1
               Unc = Unc_Shannon;
%            case 2
%                Unc = Unc_Shannon_Sprd;
           case 2
               Unc = Wcov;
           case 3
               Unc = SGauss;
           case 4
               Unc = GMM2_avg;
          case 5
               Unc = Unc_Kde;
%            case 6
%                Unc = Unc_Knn;  
           case 6
               Unc = Unc_Err;
           case 7
               Unc = Unc_Err_Map;
       end
       Unc = mat2gray(Unc);
       if (data_mode == 2)
           for i = 1:7, %11, %7, %
               Unc_mat(Uncert_,i) = Unc((i-1)*8+elmn);
        %        xlswrite('mydata.xls', Unc(1), 'name', Unc_str{Uncert_}); 
        %        xlswrite('mydata.xls', Unc((i-1)*8+elmn), 'name', Unc_str{Uncert_}); Unc(i*8+elmn)
           end
           Unc_mat(Uncert_,8) = Unc(1); %12
       else
          for i = 1:11, %11, %7, %
           Unc_mat(Uncert_,i) = Unc((i-1)*8+elmn);
          end
          Unc_mat(Uncert_,12) = Unc(1); %12
       end
       Unc_mat(Uncert_,:) = mat2gray(Unc_mat(Uncert_,:));
    end
    figure, 
    for Uncert_ = 1:7, %8, % 
        if(data_mode == 2),
            hold on, plot(1:8,Unc_mat(Uncert_,:),'Color', c(Uncert_*19,:), 'Marker', shape{Uncert_}, 'LineWidth',6, 'MarkerSize', 10), %1:12,
            set(gca,'FontSize',45), xlim([1 8]) %12
        else
            hold on, plot(1:12,Unc_mat(Uncert_,:),'Color', c(Uncert_*19,:), 'Marker', shape{Uncert_}, 'LineWidth',6, 'MarkerSize', 10), %1:12,
            set(gca,'FontSize',45), xlim([1 12]) %12
        end
    end,
    if(data_mode == 2)
        title('Label Space: 2D'); %, Original Probabilities
        AX=legend('ShEnt', 'WCov', 'GDist', 'GMM, 36 Gaussians', 'KDE', 'ErExp', 'ErExpMAP', 'Location', 'SouthEastOutSide'); % 'Delta', 'WProb', 'KNN', 
    else
        title('Label Space: 3D');
        AX=legend('ShEnt', 'WCov', 'GDist', 'GMM, 216 Gaussians', 'KDE', 'ErExp', 'ErExpMAP', 'Location', 'SouthEastOutSide'); % 'Delta', 'WProb','KNN', 
    end        
    xlabel('Case'), ylabel('Uncertainty Value'),
    LEG = findobj(AX,'type','text');
    set(gca,'FontSize',30)
    grid on,
end
        %     load ../utils/Uncertainties;   
%     nlabs = nlabs_size * nlabs_size;
%     epsilon(1) = 0;
% %     sample_num = 8;
%     eps_max = 1/nlabs;
%     for i = 2:sample_num,
%         epsilon(i) = epsilon(1) + (eps_max - epsilon(1))/sample_num * (i-1);
%     end
%    eps_len = len(epsilon);
%    c = jet(120);
%    shape = {'*' 'o' 'v' 'p' '+' 'd' '<' 's' '>' 'h' '^'};
%    imm = floor(linspace(2, nlabs_size,3));
% %    dockf;
%    ind_Unc = [1,2,3,4,11,17,18];
%    figure, 
% 
%    for my_counter = 1:4, % [1,2,3,4,5,6,7,8,9,10,11,14,16,17,18],
% % Uncert_ = 18;
%        Uncert_ = ind_Unc(my_counter);
%        switch Uncert_
%            case 1
%                Unc = Unc_Shannon;
%            case 2
%                Unc = Unc_Shannon_Sprd;
%            case 3
%                Unc = Wcov;
%            case 4
%                Unc = SGauss;
%            case 5
%                Unc = GMM1_l;
%                ng = 1;
%            case 6
%                Unc = GMM1_u;
%                ng = 1;
%            case 7
%                Unc = GMM1_avg;
%                ng = 1;
%            case 8
%                Unc = GMM1_disc;
%                ng = 1;
%            case 9
%                Unc = GMM2_l;
%                ng = 2;
%            case 10
%                Unc = GMM2_u;
%                ng = 2;
%            case 11
%                Unc = GMM2_avg;
%                ng = 2;
%            case 12
%                Unc = GMM2_disc;
%                ng = 2;
%            case 13
%                Unc = GMM3_l;
%                ng = 3;
%            case 14
%                Unc = GMM3_u;
%                ng = 3;
%            case 15
%                Unc = GMM3_avg;
%                ng = 3;
%            case 16
%                Unc = GMM3_disc;               
%                ng = 3;
%            case 17
%                Unc = Unc_Kde;
%            case 18
%                Unc = Unc_Knn;              
%        end
%        
% %        subplot(4,5,Uncert_),
% %        subplot(3,3,my_counter), 
%        subplot(2,2,my_counter),
%        plot(epsilon(1):eps_max/sample_num:epsilon(eps_len),repmat(Unc(1),[1 eps_len]), 'b', 'LineWidth',2)
%        if(data_mode == 2),
%            for ii = 1:7,
%     %            hold on, semilogx(1:eps_len,Unc(ii*eps_len+2:(ii+1)*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}); 
%                hold on, plot(epsilon(1):eps_max/sample_num:epsilon(eps_len),Unc((ii-1)*eps_len+2:ii*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}, 'LineWidth',2, 'MarkerSize', 3); 
%                set(gca,'FontSize',25)
%            end
%            if(Uncert_ > 4 && Uncert_ < 17)
%                title([num2str(Unc_str{Uncert_}) ': ' num2str(imm(ng).^2) ', label size: ' num2str(nlabs_size) '*' num2str(nlabs_size)] );
%                xlabel('epsilon'), ylabel('Uncertainty Value')
%            else
%                title([num2str(Unc_str{Uncert_}) ', label size: ' num2str(nlabs_size) '*' num2str(nlabs_size)] );
%                xlabel('epsilon'), ylabel('Uncertainty Value')
%            end         
%        else
%            if(data_mode == 3)
%                for ii = 1:11,
%         %            hold on, semilogx(1:eps_len,Unc(ii*eps_len+2:(ii+1)*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}); 
%                    hold on, plot(epsilon(1):eps_max/sample_num:epsilon(eps_len),Unc((ii-1)*eps_len+2:ii*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}, 'LineWidth',2, 'MarkerSize', 3); 
%                    set(gca,'FontSize',25)
%                end
%                if(Uncert_ > 4 && Uncert_ < 17)
%                    title([num2str(Unc_str{Uncert_}) ': ' num2str(imm(ng).^3) ', label size: ' num2str(nlabs_size) '*' num2str(nlabs_size) '*' num2str(nlabs_size)] );
%                    xlabel('epsilon'), ylabel('Uncertainty Value')
%                else
%                    title([num2str(Unc_str{Uncert_}) ', label size: ' num2str(nlabs_size) '*' num2str(nlabs_size) '*' num2str(nlabs_size)] );
%                    xlabel('epsilon'), ylabel('Uncertainty Value')
%                end
%            end
%        end
%    end
% 
%    if(data_mode == 2),
%        subplot(2,2,my_counter-3), %imagesc(img), colormap(gray)
%        plot(1:eps_len,repmat(Unc(1),[1 eps_len]), 'b', 'LineWidth',2)
%        for ii = 1:7,
%           hold on, plot(1:eps_len,Unc((ii-1)*eps_len+2:ii*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}, 'LineWidth',2, 'MarkerSize', 3); 
%        end
% 
%         AX=legend('Maximum Entropy', 'One preferred', 'Two Adjucent', 'Two Far', 'Two Farthest', ...
%                'Three Pref', 'Four Corners ', 'Five Pref ', 'Location', 'SouthEast');
%            LEG = findobj(AX,'type','text');
%            set(LEG,'FontSize',25)
%    else
%        if(data_mode == 3)
%            subplot(2,2,my_counter-3), %imagesc(img), colormap(gray)
%            plot(1:eps_len,repmat(Unc(1),[1 eps_len]), 'b', 'LineWidth',2)
%            for ii = 1:11,
%               hold on, plot(1:eps_len,Unc((ii-1)*eps_len+2:ii*eps_len+1), 'Color', c(ii*10,:), 'Marker', shape{ii}, 'LineWidth',2, 'MarkerSize', 3); 
%            end
%            AX=legend('Maximum Entropy', 'One preferred', 'Two Adjucent', 'Two Mediom', 'Two Far', 'Two Farthest', ...
%                    'Three Pref Slice', 'Three Pref Cube', 'Four Corners Slice', 'Four Corners Cube', ...
%                    'Five Pref Slice', 'Five Pref Cube', 'Location', 'SouthEast');
%                LEG = findobj(AX,'type','text');
%            set(LEG,'FontSize',15)
%        end
%    end