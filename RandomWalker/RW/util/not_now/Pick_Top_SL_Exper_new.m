function [slands_next, tlands_next nseeds] = Pick_Top_SL_Exper(src, tar, uncertainty, dx, dy, src_segment, cert_nstd, maxd, ops)

    [mx my] = meshgrid( 1:size(dx, 2), 1:size(dx, 1) );
    mask = ( src_segment > 0);
        % (Im(:,:,1)  .* (segs(:,:,1) > 0))
    a=uncertainty(mask>0);
    if (ops.pick_mode == 1 || ops.pick_mode == 3 || ops.pick_mode == 4 || ops.pick_mode == 6)
        uncertainty1 = uncertainty .* mask;
        a1 = uncertainty1(mask>0);
    elseif(ops.pick_mode == 2 || ops.pick_mode == 5)
         [gx gy] = get_gradient(src .* mask);
         grad_src = sqrt( gx.^2 + gy.^2);
        uncertainty1 = uncertainty .* grad_src;
        a1=uncertainty1(mask>0);
    end
    mn=mean((a) );
    sd=std((a));    
    mn1=mean((a1) );
    sd1=std((a1));            
    cert_thres =max( 0, mn-sd*cert_nstd ); % cert_nstd affects results [.5, 2]
    S=( uncertainty < cert_thres);
    cert_thres1 =max( 0, mn1-sd1*cert_nstd ); % cert_nstd affects results [.5, 2]
    S1=( uncertainty1 < cert_thres1);
    S = S | S1;
    inds=local_max( double(S)); % indices to potentiall good seeds

    nx=dx(inds);
    ny=dy(inds);    
    s=find( ( sqrt(  ny.^2 + nx.^2  ) ) > maxd );
    inds(s) =[];

    if ~isempty( inds )
        nx=dx(inds);
        ny=dy(inds);    
        slands_next1=[]; slands_next=[];
        tlands_next1=[]; tlands_next=[];
        slands_next1(1,:) =  mx( inds); %     slands_next(2,:) =  mx( inds);
        slands_next1(2,:) =  my( inds); %    slands_next(2,:) =  my( inds);
        tlands_next1(1,:) =  mx( inds)+nx; %+ny0  +nx0.5  -nx0.7 tlands_next(2,:) =  mx( inds)-nx; originally +nx  +ny  -nx
        tlands_next1(2,:) =  my( inds)+ny; % +nx0  +ny0.5  -ny0.7 tlands_next(1,:) =  my( inds)-ny; originally +ny  +nx  -ny
        ten_perc = min(ops.num_seed, len(inds));
        my_rand = randperm(len(inds));
        slands_next1 = slands_next1(:, my_rand);
        tlands_next1 = tlands_next1(:, my_rand);
        slands_next = slands_next1(:, 1 : ops.num_seed);
        tlands_next = tlands_next1(:, 1 : ops.num_seed);
        nseeds = ten_perc;
    end
end

function [gx gy]= get_gradient ( I1 )

     [gx gy]=gradient( I1 );

     nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
     gx=gx ./ nrm;
     gy=gy ./ nrm;
end