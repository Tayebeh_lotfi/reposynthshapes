function [U2 U3 U5 U6] = Calc_Uncertainty_Distributed(isz, pbs, dispvec, homogeneous)
    pbs = reshape(pbs, prod(isz), []);
    nlabs = size(pbs,2);
    U2 = zeros(isz); U3 = U2; U5 = U2; U6 = U2;
    dispveco.x = repmat(dispvec(:,1)',prod(isz),1);
    dispveco.y = repmat(dispvec(:,2)',prod(isz),1);
    dispvecw.x = dispveco.x .* pbs;
    dispvecw.y = dispveco.y .* pbs;
    meanw.x = sum(dispvecw.x,2);
    meanw.y = sum(dispvecw.y,2);
    ops = 1;
    data(1:nlabs,1,1:prod(isz)) = dispveco.x';
    data(1:nlabs,2,1:prod(isz)) = dispveco.y';
    
    datam(1:nlabs,1,1:prod(isz)) = repmat(meanw.x',nlabs,1);
    datam(1:nlabs,2,1:prod(isz)) = repmat(meanw.y',nlabs,1);
    gam = 0.5772; epsilon = 10e-8; d = 2; %nlabs-1;
    S = ( homogeneous > 0 );
    inds = find(S==1);
    for pix = 1:len(inds),
%         temp = (data(:,:,pix) - datam(:,:,pix))' * (data(:,:,pix) - datam(:,:,pix));;
        covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),2,1) .* (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * (data(:,:,inds(pix)) - datam(:,:,inds(pix))) ) ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );%temp;
        U2(inds(pix)) = det(covw(:,:,pix)); %det(temp);
        U3(inds(pix)) = 1/2 * log( (2*pi*exp(1)) .^ size(dispvec,2) .* (U2(inds(pix))+epsilon) );
        
        prob = sqz(pbs(inds(pix),:))';
        dispvecweight = [dispvecw.x(inds(pix),:); dispvecw.y(inds(pix),:)];
        Dist = abs( (repmat(prob,1, size(prob,1)))  - (repmat(prob', size(prob,1), 1))  );
        Dist2dw = Calc_Dist(dispvecweight, dispvecweight);
        Dist2d = Calc_Dist(dispvec', dispvec');
        Dist(1:size(dispvec,1)+1:end) = [];
        Dist = reshape(Dist(:), size(dispvec,1)-1, [])';
        
        Dist2dw(1:size(dispvec,1)+1:end) = [];
        Dist2dw = reshape(Dist2dw(:), size(dispvec,1)-1, [])';
        Dist2d(1:size(dispvec,1)+1:end) = [];
        Dist2d = reshape(Dist2d(:), size(dispvec,1)-1, [])';
        
        NearN = min(Dist, [], 2);
        NearN2d = min(Dist2dw, [], 2);
%         d=1; uncertainty_ap5(pix) = mean(log2(NearN)) + log2( (pi .^ (d/2) * (nlabs-1))/gamma(d/2+1)) + gam / log(2);
        U5(inds(pix)) = mean(log2(NearN2d+epsilon)) + log2( (pi .^ (d/2) * (nlabs-1))/gamma(d/2+1)) + gam / log(2);
        U6(inds(pix)) = sum( Dist2d(:) .* Dist(:) ) ./ 2;
    end
    U2 = mat2gray(reshape(U2, [isz(1) isz(2)]));
    U3 = mat2gray(reshape(U3, [isz(1) isz(2)]));
    U5 = mat2gray(reshape(U5, [isz(1) isz(2)]));
    U6 = mat2gray(reshape(U6, [isz(1) isz(2)]));

%     dockf;
%     for i = 1:1:size(dispvec,1)
%         hold on, quiver(0,0,dispvec(i,1),dispvec(i,2),0,'Color',round(rand(1,3))),
%         title('All labels');
%     end
% 
% 
%     dockf;
%     for i = 1:1:size(dispvecw,1)
%         hold on, quiver(0,0,dispvecw(i,1),dispvecw(i,2),0,'Color',round(rand(1,3))),
%         title('All labels');
%     end
% 

end