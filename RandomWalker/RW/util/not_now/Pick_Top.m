function [slands, tlands] = Pick_Top(uncertainty, scene, t, pbs, dispvec, src, tar)
uncertainty_org = uncertainty;
clear slands;
clear tlands;
cnt = 1;

        for j = 1:3
            [p q] = find(uncertainty == min(uncertainty(:)));
            % [p q] = min(A(:));
            % ind_z = ceil( q / (size(A,1)*size(A,2) ));
            % tmp = mod( q, size(A,1)*size(A,2) );
            % ind_x = mod(tmp, size(A,1));
            % ind_y = ceil(tmp / size(A,1));
            % A(ind_x, ind_y, ind_z)
            % A(ind_x, ind_y, ind_z) = 99;
            for i = 1:size(p,1),
%               if (p(i) > 2 && q(i) > 4)
%                    if p(i)==0
%                        return;
%                    end
                    slands(1,cnt) = p(i);
                    slands(2,cnt) = q(i);
%                    tlands(1,cnt) = p(i) - gx;
%                    tlands(2,cnt) = q(i) - gy;
                    cnt = cnt + 1;
                    uncertainty(p(i),q(i)) = max(uncertainty(:));
            end  
        end
            %count = count + size(p,1);
        
         for i = 1:size(slands,2)
            [val ind] = max(pbs(slands(1,i), slands(2,i),:)); 
            tlands(1,i) = slands(1,i) - dispvec(ind,1);
            tlands(2,i) = slands(2,i) - dispvec(ind,2);
         end
         
         
%          [val ind] = max(pbs(slands(1,8),slands(2,8),:);
         
         % switch scene
%     case 3
%         %gx = 2; gy = 4;  
%         for j = 1:3
%             [p q] = find(uncertainty == min(uncertainty(:)));
%             for i = 1:size(p,1),
% %                 if (p(i) > 2 && q(i) > 4)
% %                     if p(i)==0
% %                         return;
% %                     end
%                     slands(1,cnt) = p(i);
%                     slands(2,cnt) = q(i);
% %                     tlands(1,cnt) = p(i) - gx;
% %                     tlands(2,cnt) = q(i) - gy;
%                     cnt = cnt + 1;
% %                 end
%                 uncertainty(p(i),q(i)) = max(uncertainty(:));
%             end
%             %count = count + size(p,1);
%         end
%               for i = 1:size(slands,2)
%             [val ind] = max(pbs(slands(1,i), slands(2,i),:)); 
%             tlands(1,i) = slands(1,i) - dispvec(ind,1);
%             tlands(2,i) = slands(2,i) - dispvec(ind,2);
%          end
%     case { 6, 9}
%          slands1 = slands;
%          slands1(3,:) = 1;
%          %tlands1 = round( slands1' * t.tdata.Tinv' )';
%          tlands1 = round( slands1' * t.tdata.Tinv )';
%          %tlands4 = round( slands1' * t.tdata.T' )';
%          %tlands5 = round( slands1' * t.tdata.T )';
% %         slands1 = slands1';
%          tlands(1,:) = tlands1(1,:);tlands(2,:) = tlands1(2,:);
%            
%          tlands = round(tlands);
%          %slands1 = round(slands1);
%         
% %          sqrt(sum((tlands1(1,:) - tlands(1,:)).^2))
% %          subplot 121, imagesc(tar), colormap(gray), hold on, plot(tlands2(1,:), tlands2(2,:), 'r*');
% %          subplot 122, imagesc(src), colormap(gray), hold on, plot(slands1(1,:), slands1(2,:), 'g*');
%          
% %         slands1 = slands;
% %         slands1(3,:) = 1;
% %         tlands1 = round(t.tdata.T * slands1);
% %         tlands(1,:) = tlands1(1,:);tlands(2,:) = tlands1(2,:);
%          %tlands = round(t.tdata.Tinv * tlands);
%          %tlands = round(tformfwd(s,slands'));
%          %slands(:,cross+1:end) = round(slands1');
%         % otherwise
% end


% A(:,:,1) = [1 2 3 5; 4 3 2 7; 7 7 2 3; 4 3 2 6; 6 5 8 9];
% A(:,:,2) = [0 1 3 5; 2 3 2 5; 3 7 5 3; 3 0 2 6; 1 2 8 9];
% A(:,:,3) = [10 11 3 15; 12 13 2 2; 2 7 15 2; 3 17 2 16; 11 2 18 19];
% A(:,:,4) = [1 3 2 4; 12 2 12 12; 2 5 7 12; 13 2 7 1; 2 5 8 11];
% [p q r] = find(A == 2); % For 3D matrix A
%p1 = p;
% q1 = mod(q, size(A,2));
% r1 = ceil(q/size(A,2));
% q1 (q1 == 0) = size(A,2);
% ind_x = p1; ind_y = q1; ind_z = r1;
% [p q] = min(A(:));
% ind_z = ceil( q / (size(A,1)*size(A,2) ));
% tmp = mod( q, size(A,1)*size(A,2) );
% ind_x = mod(tmp, size(A,1));
% ind_y = ceil(tmp / size(A,1));
% A(ind_x, ind_y, ind_z)
% A(ind_x, ind_y, ind_z) = 99;