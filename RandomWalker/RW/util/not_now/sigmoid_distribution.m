function f = sigmoid_distribution(x,y)
    f = 1 ./ (1+(exp(-x) * exp(-y)));
end