% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set_region( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Time, trial_num, set_num, str, Landmarks)
%     file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Solution_field' 'Time' 'Landmarks'};
    file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Time' 'Landmarks'};    
    for i = 1:6,%6
      save (num2str([savepath file_name{i} num2str(set_num) str '_corner_set' num2str(trial_num) '_region.mat']), file_name{i}); 
%       save (num2str([savepath file_name{i} num2str(set_num) '_large_corner_set' num2str(trial_num) '.mat']), file_name{i});       
%       save (num2str([savepath file_name{i} num2str(trial_num) '_new2_noise5.mat']), file_name{i}); 
%       save (num2str([savepath file_name{i} num2str(trial_num) '_new2_noise10.mat']), file_name{i}); 

    end          
end