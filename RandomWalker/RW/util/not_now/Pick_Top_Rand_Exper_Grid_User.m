function [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_Grid_User(src, tar, uncertainty, invTx, invTy, src_segment, cert_nstd, maxd, ops, slands, tlands, reg_iter)

    
%     [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );

    isz = size(src);
    length = size(slands,2);
    start = 30;
    [seedx seedy] = ndgrid( start:round((isz(1)-2*start)/8):isz(1)-start, start:round((isz(2)-2*start)/8):isz(2)-start );
%     slands_new = [ seedx(reg_iter:reg_iter+3); seedy(reg_iter:reg_iter+3) ];
%     slands_new = [ seedx(reg_iter*2-1:reg_iter*2); seedy(reg_iter*2-1:reg_iter*2) ];
%     slands_new = [ seedx(reg_iter*4-3:reg_iter*4); seedy(reg_iter*4-3:reg_iter*4) ];    
    slands_new = [ seedx(reg_iter); seedy(reg_iter) ];
        
    tlands_new = Read_From_User( src, tar, slands_new );%, diff_img, edge_img );
    length = size(slands,2);
    adding_length = size(slands_new,2);
    slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
    tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);%+ops.num_seed) = slands_new;
    nseeds = size(tlands,2);
end