function [slands_next tlands_next nseeds] = Pick_Top_Rand_Exper_new(src, tar, uncertainty, Tx, Ty, src_segment, cert_nstd, maxd, ops)
    
    [sxgrid sygrid] = meshgrid( 1:size(Tx, 2), 1:size(Tx, 1) );
    mask = ( src_segment > 0);
    minmax = mnx(uncertainty(:));
    % (Im(:,:,1)  .* (segs(:,:,1) > 0))
    % cert_nstd = 1.5;
    a=uncertainty(mask>0);
    if (ops.pick_mode == 1 || ops.pick_mode == 3 || ops.pick_mode == 4 || ops.pick_mode == 6)
        uncertainty1 = uncertainty .* mask;
        a1 = uncertainty1(mask>0);
    elseif(ops.pick_mode == 2 || ops.pick_mode == 5)
         [gx gy] = get_gradient(double(src .* mask));
         grad_src = sqrt( gx.^2 + gy.^2);
        uncertainty1 = uncertainty .* grad_src;
        a1=uncertainty1(mask>0);
    end
    mn=mean((a) );
    sd=std((a));    
    mn1=mean((a1) );
    sd1=std((a1));            
    if(ops.pick_mode == 4 || ops.pick_mode == 5 || ops.pick_mode == 6)
        cert_thres =min( minmax(2), mn+sd*cert_nstd ); % cert_nstd affects results [.5, 2]
        cert_thres1 =min( minmax(2), mn1+sd1*cert_nstd ); % cert_nstd affects results [.5, 2]
        S=( uncertainty > cert_thres);
        S1=( uncertainty1 > cert_thres1);
    end
    if(ops.pick_mode == 1 || ops.pick_mode == 2 || ops.pick_mode == 3)
            cert_thres =max( minmax(1), mn-sd*cert_nstd ); % cert_nstd affects results [.5, 2]
            S=( uncertainty < cert_thres);
            cert_thres1 =max( minmax(1), mn1-sd1*cert_nstd ); % cert_nstd affects results [.5, 2]
            S1=( uncertainty1 < cert_thres1);
    end
    S = S | S1;
    inds=local_max( double(S)); % indices to potentiall good seeds
     % /10
%     nx=Tx(inds);
%     ny=Ty(inds);    
    nx=Tx(inds);
    ny=Ty(inds);    
    s=find( ( sqrt(  ny.^2 + nx.^2  ) ) > maxd );
    inds(s) =[];
    ten_perc = min(ops.num_seed, round(size(inds,2)));  
    slands_next = [];
    tlands_next = [];
    slands_next(1, :)  = round( (size(src, 1) - 2 * maxd - 1 ) * rand(1, ten_perc)) + maxd;
    slands_next(2, :)  = round( (size(src, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;

    txgrid_tmp = interp2(Ty, sygrid, sxgrid, 'linear', 0); %u(seedIndx(:));
    tygrid_tmp = interp2(Tx, sygrid, sxgrid, 'linear', 0); %v(seedIndx(:));
    txgrid = sxgrid - txgrid_tmp;
    tygrid = sygrid - tygrid_tmp;
    seedIndx = sub2ind(size(sxgrid), round(slands_next(2, :)), round(slands_next(1, :) ));
    tlands_next(1,:) = txgrid(seedIndx(:))'; %x(seedIndx(:));      tlands_next(2,:) = txgrid(seedIndx(:))';
    tlands_next(2,:) = tygrid(seedIndx(:))'; %y(seedIndx(:));      tlands_next(1,:) = tygrid(seedIndx(:))';
    nseeds = ten_perc;
%     if(ops.pick_mode == 3 || ops.pick_mode == 6)
%             slands_next1 = slands_next;
%             tlands_next1 = tlands_next;
%             slands_next=[];
%             tlands_next=[];
%             my_rand = randperm(nseeds);
%             slands_next1 = slands_next1(:, my_rand);
%             tlands_next1 = tlands_next1(:, my_rand);
%             slands_next = slands_next1(:, 1 : ops.num_seed);
%             tlands_next = tlands_next1(:, 1 : ops.num_seed);
%             nseeds = size(tlands_next,2);            
%     end
end

function [gx gy]= get_gradient ( I1 )

     [gx gy]=gradient( I1 );

     nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
     gx=gx ./ nrm;
     gy=gy ./ nrm;
end
