function p = sigmoid_kernel(dispvec, pbs, pix, isz)
    nlabs = size(dispvec,1);
%     Siginv = pinv(opt.Sig);
    [x1 x2] = ind2sub(isz, pix);
    x(:,1) = repmat(x1, [nlabs 1]) + dispvec(:,1);
    x(:,2) = repmat(x2, [nlabs 1]) + dispvec(:,2);    
    p = sum ( 1 ./ ( (1 + exp(-x(:,1))) .* (1 + exp(-x(:,2))) ) .* sqz(pbs(pix,:))' );
end