function tlands_next = Read_From_User_new(src, tar, resolution, slands_next, diff_img, edge_img, ...
    dx, dy, dz, pbs, dispvec, Candidate_plane_point)

    LMR = [];
    maxd = 5;
    inputname = {'floating image' 'target image'};
   if ~exist('f1', 'var'); 
        f1=dockf; 
        figure(f1), 
    else
        f2=dockf;
        figure(f2), 
   end
%   slands_next1 = (slands_next-1) * resolution + 1;  
  nseeds = size(slands_next,2);
  for i = 1:nseeds,
    pbs_lands(i, :) = pbs( round(slands_next(1,i)), round(slands_next(2,i)), round(slands_next(3,i)), : );
  end
   for m = 1 : size(slands_next,2),  
%        pbs(seedIndx(i))
       figure(f1);
        i = 1; j = 1; k = 1;

        plane = createPlane(round(Candidate_plane_point(:,1,m)'), round(Candidate_plane_point(:,2,m)'), round(Candidate_plane_point(:,3,m)'));
        
        p1 = Candidate_plane_point(:,1,m)';
        p2 = Candidate_plane_point(:,2,m)';
        p3 = Candidate_plane_point(:,3,m)';
        p4 = Candidate_plane_point(:,2,m)'+Candidate_plane_point(:,3,m)';

        XYZ = [p1;p2;p3;p4];
        X=XYZ(:,1);
        Y =XYZ(:,2);
        Z =XYZ(:,3);
        Tri = [1 2 4; 1 4 3];

        h=trimesh(Tri,X,Y,Z);
        set(h,'FaceAlpha',0.5);
        
        subplot 122, imre(tar, 3, i); 
        subplot 231, imre(src, 1, slands_next(1,m)); hold on, title([inputname{1} 'across x' num2str(round(slands_next(1,m)))]), plot(slands_next(3,m),slands_next(2,m), 'r*'),
        subplot 232, imre(src, 2, slands_next(2,m)); hold on, title([inputname{1} 'across y' num2str(round(slands_next(2,m)))]), plot(slands_next(3,m),slands_next(1,m), 'r*'),
        subplot 233, imre(src, 3, slands_next(3,m)); hold on, title([inputname{1} 'across z' num2str(round(slands_next(3,m)))]), plot(slands_next(2,m),slands_next(1,m), 'r*'),        
        subplot 234, imre(tar, 1, i); title([inputname{2} 'across x' num2str(i)])
        subplot 235, imre(tar, 2, j); title([inputname{2} 'across y' num2str(j)])
        subplot 236, imre(tar, 3, k); title([inputname{2} 'across z' num2str(k)])       
        
        MaxN1=size(tar,1);
        MinN1=1;
        MaxN2=size(tar,2);
        MinN2=1;
        MaxN3=size(tar,3);
        MinN3=1;
        while (1)
            ch= input('','s');
            switch (ch)
                case '1' 
                    i=i+1;
                case '2' 
                    i=i-1;
                case '3' 
                    j=j+1;
                case '4' 
                    j=j-1;
                case '5' 
                    k=k+1;
                case '6' 
                    k=k-1;
                case 'e'
                    break;
            end   
            if (i<MinN1)
                i=MaxN1;
            else if (i>MaxN1)
                    i=MinN1;
                end
            end   
            if (j<MinN2)
                j=MaxN2;
            else if (j>MaxN2)
                    j=MinN2;
                end
            end   
            if (k<MinN3)
                k=MaxN3;
            else if (k>MaxN3)
                    k=MinN3;
                end
            end               
%             subplot 121, imre(src, 3, slands_next(3,m)); hold on, title(inputname{1}), plot(slands_next(2,m),slands_next(1,m), 'r*'),
%             subplot 122, imre(tar, 3, i); 
        subplot 231, imre(src, 1, slands_next(1,m)); hold on, title([inputname{1} 'across x' num2str(round(slands_next(1,m)))]), plot(slands_next(3,m),slands_next(2,m), 'r*'),
        subplot 232, imre(src, 2, slands_next(2,m)); hold on, title([inputname{1} 'across y' num2str(round(slands_next(2,m)))]), plot(slands_next(3,m),slands_next(1,m), 'r*'),
        subplot 233, imre(src, 3, slands_next(3,m)); hold on, title([inputname{1} 'across z' num2str(round(slands_next(3,m)))]), plot(slands_next(2,m),slands_next(1,m), 'r*'),        
        subplot 234, imre(tar, 1, i); title([inputname{2} 'across x' num2str(i)])
        subplot 235, imre(tar, 2, j); title([inputname{2} 'across y' num2str(j)])
        subplot 236, imre(tar, 3, k); title([inputname{2} 'across z' num2str(k)]) 
        end
        subplot 231, imre(src, 1, slands_next(1,m)); hold on, title([inputname{1} 'across x' num2str(round(slands_next(1,m)))]), plot(slands_next(3,m),slands_next(2,m), 'r*'),
        subplot 232, imre(src, 2, slands_next(2,m)); hold on, title([inputname{1} 'across y' num2str(round(slands_next(2,m)))]), plot(slands_next(3,m),slands_next(1,m), 'r*'),
        subplot 233, imre(src, 3, slands_next(3,m)); hold on, title([inputname{1} 'across z' num2str(round(slands_next(3,m)))]), plot(slands_next(2,m),slands_next(1,m), 'r*'),        
        ch= input('','s');
        switch (ch)
                case '1'                    
                    subplot 235, imre(tar, 2, j); title([inputname{2} 'across y' num2str(j)])
                    subplot 236, imre(tar, 3, k); title([inputname{2} 'across z' num2str(k)])
                    subplot 234, imre(tar, 1, i); title([inputname{2} 'across x' num2str(i)]),hold on,   
                case '2'                    
                    subplot 234, imre(tar, 1, i); title([inputname{2} 'across x' num2str(i)])
                    subplot 236, imre(tar, 3, k); title([inputname{2} 'across z' num2str(k)])
                    subplot 235, imre(tar, 2, j); title([inputname{2} 'across y' num2str(j)]),hold on, 
                case '3'                    
                    subplot 234, imre(tar, 1, i); title([inputname{2} 'across x' num2str(i)])
                    subplot 235, imre(tar, 2, j); title([inputname{2} 'across y' num2str(j)]) 
                    subplot 236, imre(tar, 3, k); title([inputname{2} 'across z' num2str(k)]),hold on,
        end
%         figure(f1), subplot 121, imre(src, 3, slands_next(3,m)); hold on, title(inputname{1}), plot(slands_next(2,m),slands_next(1,m), 'r*'),
%         subplot 122, imre(tar, 3, i); title(inputname{2}), hold on,   

        text(70,-50,'select target landmarks','color','b');
%         for i = 1 : size(slands1, 2),
            [x,y] = ginput(1);
            plot(x,y,'go')
            switch ch
                case '1'
                    text(x+6,y, num2str(m),'FontSize',14,'color','g');
                    LMR = [LMR; [i,x,y]];
               case '2'
                    text(x+6,y, num2str(m),'FontSize',14,'color','g');
                    LMR = [LMR; [x,j,y]];
               case '3'
                    text(x+6,y, num2str(m),'FontSize',14,'color','g');
                    LMR = [LMR; [x,y,k]];
            end
           %  x = round(x);
           % y = round(y);     
   end

   tlands_next= LMR';
end