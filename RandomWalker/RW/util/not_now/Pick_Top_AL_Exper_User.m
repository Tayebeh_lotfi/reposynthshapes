function [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper_User( src, tar, uncertainty, invTx, invTy, src_segment, cert_nstd, maxd, ops, slands, tlands, tlands_pre )

        [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
%         uncertainty(uncertainty == max(uncertainty(:))) = min(uncertainty(:));
%         mask = ( src_segment > 0);
        mask1 = ( tar > 30);
        mask2 = ( src_segment > 0);
        se = strel('disk',10);
        mask2 = imdilate(mask2,se);
        mask = mask1 .* mask2;
        minmax = mnx(uncertainty);
        length = size(slands,2);
        pre_length = size(tlands_pre,2);        
        alpha = 0.7;
        uncertainty = uncertainty .* mask;
        a=uncertainty(mask>0);
        mn=mean((a) );
        sd=std((a));    
        cert_thres =min( minmax(2), mn+sd*cert_nstd ); % cert_nstd affects results [.5, 2]
        S=( uncertainty > cert_thres);      
        
        candidate_inds = find(S==1);
        candidates(1,:) = mx( candidate_inds )';
        candidates(2,:) = my( candidate_inds )';
    %     candidates(3,:) = mz( candidate_inds )';
    %     landmarks.X = slands(1,:); landmarks.Y = slands(2,:); landmarks.Z = slands(3,:);
        slands= pick_points(slands, candidates, ops, uncertainty, alpha);
        slands_new = slands;
        slands_new(:,1:length) = [];

        tlands_new = Read_From_User( src, tar, slands_new );%, diff_img, edge_img );
        length = size(slands,2);
        adding_length = size(slands_new,2);
%         slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
        tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);

    nseeds = size(slands,2);
end