function [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_All(src, uncertainty, dx, dy, src_segment, cert_nstd, ops, slands, tlands)
[mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2), 1:size(dx, 3) );
    mask1 = ( src > 0.1);
    mask2 = ( src_segment > 0);
    se = strel('disk',2);
    mask2 = imdilate(mask2,se);
    mask = mask1 .* mask2;
    certainty = max(uncertainty(:)) - uncertainty + min(uncertainty(:));
    ops.alpha = 0.1; %0.3
    length = size(slands,2);
    cert_thres = quantile(certainty(mask>0),.98);      
    certainty = certainty .* mask;
    S=( certainty > cert_thres); 
%     cert_thres =-log(1-cert_nstd)*mn; % cert_nstd affects results [.5, 2]
%         dockf;hist(a)
%         hold on, plot(mn, 1:10000, 'r.-'), hold on, plot(cert_thres, 1:10000, 'g.-'), 
    candidate_inds = find(S==1);
    ops.num_seed = min(ops.num_seed, size(candidate_inds,1));
    candidates(1,:) = mx( candidate_inds )';
    candidates(2,:) = my( candidate_inds )';
    slands= pick_points(slands, candidates, ops, certainty);
% %     inds=local_max( double(S)); % indices to potentiall good seeds
    slands_new=slands;
    slands_new(:, 1:length) = [];
    inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));
    nx=dx(inds);
    ny=dy(inds);

    tlands_new(1,:) =  mx( inds)+nx; 
    tlands_new(2,:) =  my( inds)+ny; 
    adding_length = size(slands_new, 2);
    tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
    nseeds = size(tlands, 2);
end
%     [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2), 1:size(dx, 3) );
% %     pre_slands = slands; pre_tlands = t_lands;
%     mask1 = ( src > 30);
%     mask2 = ( src_segment > 0);
%     se = strel('disk',2);
%     mask2 = imdilate(mask2,se);
%     mask = mask1 .* mask2;
%     uncertainty = uncertainty .* mask;
% %     inds = find(mask);
%     %         ops.num_seed = 80; %50
%     ops.alpha = 0.1; %0.3
%     length = size(slands,2);
%     a=uncertainty(mask>0);
%     mn=mean((a) ); %mn = median(a);
% %         sd=std((a));  
%         cert_nstd = 1 - cert_nstd;
% %         cert_thres =min( minmax(2), mn+sd*cert_nstd ); % cert_nstd affects results [.5, 2]
%     cert_thres =-log(1-cert_nstd)*mn; % cert_nstd affects results [.5, 2]
% %     cert_thres = mn;
%     S=( uncertainty < cert_thres);     
% 
%     inds = find(S==1);
% %     ops.num_seed = min(ops.num_seed, size(candidate_inds,1));
% %     candidates(1,:) = mx( candidate_inds )';
% %     candidates(2,:) = my( candidate_inds )';
% %     slands= pick_points(slands, candidates, ops, certainty);
% % % %     inds=local_max( double(S)); % indices to potentiall good seeds
% %     slands_new=slands;
% %     slands_new(:, 1:length) = [];
% %     inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));
%     slands_new(1,:) = mx( inds);
%     slands_new(2,:) = my(inds);
% %     indss = sub2ind(size(src), slands(1,:), slands(2,:));
%     adding_length = size(slands_new, 2);
%     slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
%     slands_next = slands';
%     slands = []; slands = unique(slands_next, 'rows'); slands = slands';
%     inds = sub2ind(size(src), slands(1,:), slands(2,:));
%     nx=dx(inds);
%     ny=dy(inds);
%     tlands = [];
% %     slands_new(indss) = [];
%     tlands(1,:) =  mx( inds)+nx; 
%     tlands(2,:) =  my( inds)+ny;
%     slands_new = slands; tlands_new = tlands;
% %     adding_length = size(slands_new, 2);
% %     tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
%     nseeds = size(tlands, 2);
