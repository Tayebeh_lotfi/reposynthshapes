function uncertainty  = Calc_Uncertainty_New(isz, pbs, dispvec, homogeneous)
    pbs = reshape(pbs, prod(isz), []);
    S = ( homogeneous > 0 );
    inds = find(S==1);
    new_pbs1 = pbs(inds(1:len(inds)/2), :);
    uncertainty = zeros(isz);
%     nlabs = size(pbs,2);
    new_pbs1 = permute(new_pbs1, [2 1]);
    nlabs = size(new_pbs1,1);
    GMat = repmat(new_pbs1, [1 1 nlabs]);
    GMat = permute(GMat, [1 3 2]);
    GMat_ = repmat(new_pbs1', [1 1 nlabs]);
    GMat_ = permute(GMat_, [3 2 1]);
    Dist = abs(GMat - GMat_);
   
    Dist2d = Calc_Dist(dispvec', dispvec');
    uncertainty(inds(1:len(inds)/2)) = sum(sum((Dist2d .* Dist)));
  
    uncertainty = reshape(uncertainty, [isz(1) isz(2)]);
%     pbs = [1 4 10; 1 3 1; 2 5 7; 4 3 6];
%     pbs = permute(pbs, [2 1]);
%     nlabs = size(pbs,1);
%     GMat = repmat(pbs, [1 1 nlabs]);
%     GMat = permute(GMat, [1 3 2]);
%     GMat_ = repmat(pbs', [1 1 nlabs]);
%     GMat_ = permute(GMat_, [3 2 1]);
%     Dist = abs(GMat - GMat_);
%     
%     dispvec = fix(5*rand(3,2));
%     Dist2d = Calc_Dist(dispvec', dispvec');
%     
%     
%     uncertainty_ap6 = sum(sum((repmat(Dist2d , [1 1 size(Dist,3)]) .* Dist)));

%     Dist(1:size(dispvec,1)+1:end) = [];
%     Dist = reshape(Dist(:), size(dispvec,1)-1, [])';
%     



end