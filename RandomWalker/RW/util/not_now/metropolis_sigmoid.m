% metropolis.m
function zVec_new = metropolis_sigmoid(dispvec, pbs, opt, pix)

%% Defining the parameters
%     mu_q = 0;
%     sig_q = 1;
    L = opt.MCMCiter;
    z_old = 1; %mu_q;
    zVec = [];
    prob_l = sqz(pbs(pix,:));
    %% Generating samples from proposal dist
    % Get a random variable between 0-1 from the gaussian distribution with mu=0, sd=1.
    z_new = floor(size(dispvec,1)*rand) + 1;
    x_new = dispvec(z_new,1); y_new = dispvec(z_new,2);
    % Mapping sample to proposal distribution.
%     z_new = sig_q * z_new + mu_q;

    % Get the first sample.
%     q = sigmoid_distribution(x_new,y_new);

    %Generate 2 new distributions to compare.
    p_new = prob_l(z_new);
    p_old = prob_l(z_old);

    prob = p_new / p_old;

    if p_new > p_old,
        z_old = z_new;
        zVec = [zVec, z_new];
    else
         r = rand(1);
            if r < prob,
                z_old = z_new;
                 zVec = [zVec, z_new];
            else
                zVec = [zVec, z_old];
            end 
    end;

    % Moving the proposal distribution to fit to the original pdf better.
    for i=2:L
        z_new = floor(size(dispvec,1)*rand) + 1;
        x_new = dispvec(z_new,1); y_new = dispvec(z_new,2);
        % Mapping sample to proposal distribution.
        %     z_new = sig_q * z_new + mu_q;

        % Get the first sample.
%         q = sigmoid_distribution(x_new,y_new);

        %Generate 2 new distributions to compare.
        p_new = prob_l(z_new);
        p_old = prob_l(z_old);
        
%         z_new = randn(1);
%         z_new = sig_q * z_new + z_old;

%         q = gauss_distribution(z_new,zVec(i-1),sig_q); 
%         p_new = complex_dist(z_new);
%         p_old = complex_dist(z_old);

        % Calc the probability based on metropolis alg.
        prob = p_new / p_old;

        if prob >1, 
            % Then we definitely use it.
            % Accepted.
            z_old = z_new;
            zVec = [zVec, z_new];

        else % prob is less than 1
            % We still might accept it.

            r = rand(1);
            % Condition to see if we accept it.
            if r < prob,
                % Yes we accept!
                z_old = z_new;
                 zVec = [zVec, z_new];
            else
                % No reject. Keep the old one.
                zVec = [zVec, z_old];
            end
        end

    end
    % Leave 300 first ones for burn-in
    zVec_new = zVec(300:L);

    %% Display the results
%     a = 0:0.01:40;
%     figure, plot(a, complex_dist(a),'r-'),title('The original pdf');
%     figure, hist(zVec_new,50), title('histogram of drawn samples using Metropolis');
end
