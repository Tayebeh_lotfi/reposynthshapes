% function Calc_Uncertainty_WCov_MEX()
function [Unc2 Unc3] = Calc_Uncertainty_WCov_MEX(isz, pbs, dispvec, h)
%     load func_variables;
    pbs = reshape(pbs, prod(isz), []);
    nlabs = size(pbs,2);
    Unc2 = zeros(isz); Unc3 = Unc2;
    dispveco.x = repmat(dispvec(:,1)',prod(isz),1);
    dispveco.y = repmat(dispvec(:,2)',prod(isz),1);
    dispvecw.x = dispveco.x .* pbs;
    dispvecw.y = dispveco.y .* pbs;
    meanw.x = sum(dispvecw.x,2);
    meanw.y = sum(dispvecw.y,2);
    data(1:nlabs,1,1:prod(isz)) = dispveco.x';
    data(1:nlabs,2,1:prod(isz)) = dispveco.y';
    
    datam(1:nlabs,1,1:prod(isz)) = repmat(meanw.x',nlabs,1);
    datam(1:nlabs,2,1:prod(isz)) = repmat(meanw.y',nlabs,1);
    epsilon = 10e-8; %nlabs-1;
    S = ( h > 0 );
    inds = find(S==1);
    for pix = 1:len(inds),
        covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),2,1) .* ...
            (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * ...
            (data(:,:,inds(pix)) - datam(:,:,inds(pix))) ) ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );
        Unc2(inds(pix)) = det(covw(:,:,pix)); %det(temp);
        Unc3(inds(pix)) = 1/2 * log( (2*pi*exp(1)) .^ size(dispvec,2) .* (Unc2(inds(pix))+epsilon) );
    end
    Unc2 = mat2gray(reshape(Unc2, [isz(1) isz(2)]));
    Unc3 = mat2gray(reshape(Unc3, [isz(1) isz(2)]));

%     save Uncertainties Unc2 Unc3;
end