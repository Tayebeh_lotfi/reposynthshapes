function [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_tst(src, tar, uncertainty, dx, dy, src_segment, cert_nstd, maxd, ops, slands, tlands)

    [mx my] = meshgrid( 1:size(dx, 2), 1:size(dx, 1) );
    mask = ( src_segment > 0);
    add_mode = 1;
    ops.num_seed = 4;

    uncertainty1 = uncertainty .* mask;
    uncertainty1(uncertainty1 == 0) = max(uncertainty1(:));
%         uncertainty1 = -uncertainty1;
    a=uncertainty(mask>0);

    mn=mean((a) );
    sd=std((a));
    cert_thres =max( 0, mn-sd*cert_nstd ); % cert_nstd affects results [.5, 2]
    S=( uncertainty1 < cert_thres);
%     inds = vl_localmax( double(S), 0.005, 2);
    inds=local_max( double(S)); % indices to potentiall good seeds

%     nx=dx(inds);
%     ny=dy(inds);    
%     s=find( ( sqrt(  ny.^2 + nx.^2  ) ) > maxd );
%     inds(s) =[];

    if ~isempty( inds )
        nx=dx(inds);
        ny=dy(inds);    
        slands_new=[];
        tlands_new=[];
        slands_new(2,:) =  mx( inds); %     slands_next(2,:) =  mx( inds);
        slands_new(1,:) =  my( inds); %    slands_next(2,:) =  my( inds);
        tlands_new(2,:) =  mx( inds)+nx; %+nx no +ny no  -ny no
        tlands_new(1,:) =  my( inds)+ny; % 

        if(ops.pick_mode == 3 || ops.pick_mode == 6)
            slands_next=[];
            tlands_next=[]; 
            slands_next = slands_new;
            tlands_next = tlands_new;
            picked_length = size(slands_new,2);
            my_rand = randperm(picked_length);
            slands_next = slands_next(:, my_rand);
            tlands_next = tlands_next(:, my_rand);
            slands_new = []; tlands_new = [];
            ops.num_seed = min(ops.num_seed, picked_length);
            slands_new = slands_next(:, 1 : ops.num_seed);
            tlands_new = tlands_next(:, 1 : ops.num_seed);
        end

            length = size(slands,2);
            adding_length = size(slands_new,2);
            slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
            tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
    end
    nseeds = size(tlands,2);
end

function [gx gy]= get_gradient ( I1 )

     [gx gy]=gradient( I1 );

     nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
     gx=gx ./ nrm;
     gy=gy ./ nrm;
end