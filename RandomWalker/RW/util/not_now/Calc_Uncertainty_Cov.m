function uncertainty_cov = Calc_Uncertainty_Cov(dispvec, pbs, isz)

    pbs = reshape(pbs, prod(isz), []);
%     nlabs = size(pbs,2);
    for ind = 1:prod(isz),
        uncertainty_cov(:,:,ind) = weightedcov(dispvec, pbs(ind,:));
    end
    uncertainty_cov = mat2gray(uncertainty_cov);
    uncertainty_cov = reshape(uncertainty_cov, [isz(1) isz(2)]);  
end