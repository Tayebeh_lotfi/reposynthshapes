function [slands_new tlands_new slands tlands nseeds] = Pick_Top_Rand_Exper_User( src, tar, uncertainty, invTx, invTy, src_segment, cert_nstd, maxd, ops, slands, tlands, tlands_pre )
    
    [sxgrid sygrid] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
    mask1 = ( tar > 30);
    mask2 = ( src_segment > 0);
    se = strel('disk',10);
    mask2 = imdilate(mask2,se);
    mask = mask1 .* mask2;
    minmax = mnx(uncertainty);   
    pick_img = src .* mask;
    a=uncertainty(mask>0);
    mn=mean((a) );
    sd=std((a));
    cert_thres =min( minmax(2), mn+sd*cert_nstd ); % cert_nstd affects results [.5, 2]
    S=( uncertainty > cert_thres);
    inds=local_max( double(S)); % indices to potentiall good seeds

    ten_perc = round(size(inds,2));  
    ops.num_seed = min(ops.num_seed, ten_perc);
%     if(ten_perc ~= 0)
        slands_new = []; tlands_new = [];
        slands_next(1, :)  = round( (size(src, 1) - 2 * maxd - 1 ) * rand(1, ten_perc)) + maxd;
        slands_next(2, :)  = round( (size(src, 1) - 2 * maxd - 1) * rand(1, ten_perc)) + maxd;
        cnt = 1; i = 1;
        while(1),
            if(pick_img(slands_next(1,i), slands_next(2,i)) > 0 && cnt <= ops.num_seed)
                slands_new(:,cnt) = slands_next(:, i); cnt = cnt+1;
            end
            i = i+1;
            if(cnt > ops.num_seed)
                break
            end
        end

    tlands_new = Read_From_User( src, tar, slands_new );%, diff_img, edge_img );
    length = size(slands,2);
    adding_length = size(slands_new,2);
    slands(:,length+1:length+adding_length) = slands_new(:, 1 : end);
    tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);%+ops.num_seed) = slands_new;
    nseeds = size(tlands,2);
end
