function [Angle, Distance] = Calc_Angle(X,Y)

    for i = 1:size(X,1),
        for j = 1:size(Y,1), %size(X,1)/2,
            Angle(i,j) = real(acosd(dot(X(i,:), Y(j,:)) / (norm(X(i,:)) * norm(Y(j,:)))));
            Distance(i,j) = sum( (( X(i,:)-Y(j,:) ).^2), 2 );
%             Angle(j,i) = Angle(i,j);
        end
    end
    Angle(isnan(Angle)) = 180;
end