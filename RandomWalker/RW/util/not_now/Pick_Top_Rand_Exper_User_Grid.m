function [slands_next tlands_next slands tlands nseeds] = Pick_Top_Rand_Exper_User_Grid( src, tar, uncertainty, invTx, invTy, src_segment, cert_nstd, maxd, ops, slands, tlands, tlands_pre )
    
    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );

    [m n] = size(src);
    start = 5;
    [seedx seedy] = ndgrid( start:round((m-2*start)/6):m-start, start:round((n-2*start)/6):n-start );
    slands_next = [ seedx(1+(reg_iter-1)*4:reg_iter*4); seedy(1+(reg_iter-1)*4:reg_iter*4)];
    tlands_next = Read_From_User_new(src_high, tar_high, resolution, slands_next, diff_img, edge_img, dx, dy, dz, pbs, dispvec);
 
    tlands = [tlands tlands_next];
    slands = [slands slands_next];
    nseeds = size(tlands,2);
end
