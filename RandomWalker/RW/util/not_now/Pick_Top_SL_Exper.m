function [slands tlands slands_new tlands_new nseeds] = Pick_Top_SL_Exper(src, uncertainty, dx, dy, src_segment, cert_nstd, ops, slands, tlands)

    [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2), 1:size(dx, 3) );
    mask1 = ( src > 0.1);
    mask2 = ( src_segment > 0);
    se = strel('disk',2);
    mask2 = imdilate(mask2,se);
    mask = mask1 .* mask2;
    certainty = max(uncertainty(:)) - uncertainty + min(uncertainty(:));
    ops.alpha = 0.1; %0.3
    length = size(slands,2);
    cert_thres = quantile(certainty(mask>0),.98);      
    certainty = certainty .* mask;
    S=( certainty > cert_thres); 

    candidate_inds = find(S==1);
    candidates(1,:) = mx( candidate_inds )';
    candidates(2,:) = my( candidate_inds )';

    slands= pick_points(slands, candidates, ops, certainty);
%     inds=local_max( double(S)); % indices to potentiall good seeds
%     slands_new = slands(:,length+1:end);
    slands_new = slands;
    slands_new(:, 1:length) = [];
    inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));

    nx=dx(inds);
    ny=dy(inds);

        tlands_new(1,:) =  mx( inds)-nx; %+ny0  +nx0.5  -nx0.7 tlands_next(2,:) =  mx( inds)-nx; originally +nx  +ny  -nx
        tlands_new(2,:) =  my( inds)-ny; % +nx0  +ny0.5  -ny0.7 tlands_next(1,:) =  my( inds)-ny; originally +ny  +nx  -ny        
%         if(add_mode == 1)
%             length = len(slands);
            adding_length = size(slands_new, 2);
%             slands(:,length+1:length+adding_length) = slands_next(:, 1 : end);
            tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
%     end
    nseeds = size(tlands, 2);
end
