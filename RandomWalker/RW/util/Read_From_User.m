function tlands1 = Read_From_User(src, tar, slands1, diff_img, edge_img )

        LMR = [];
        inputname = ['first image' 'second image'];
       if ~exist('f1', 'var'); 
            f1=dockf; 
            figure(f1), 
        else
            f2=dockf;
            figure(f2), 
        end
        figure(f1), subplot(1,2,2), imshow(src,[]); hold on; title(inputname(12:end));  
        for i = 1 : size(slands1, 2),
            x = slands1(1,i);
            y = slands1(2,i);
            plot(x,y,'ro')
            text(x+6,y, num2str(i),'FontSize',14,'color','r');
         %   x = round(x);
         %   y = round(y);
        end

        figure(f1), subplot(1,2,1), imshow(tar,[]); hold on; title(inputname(1:11));
        text(70,-50,'select target landmarks','color','b');
        for i = 1 : size(slands1, 2),
            [x,y] = ginput(1);
            plot(x,y,'go')
            text(x+6,y, num2str(i),'FontSize',14,'color','g');
          %  x = round(x);
           % y = round(y);
            LMR = [LMR; [x,y]];
        end

         tlands1= LMR';
end