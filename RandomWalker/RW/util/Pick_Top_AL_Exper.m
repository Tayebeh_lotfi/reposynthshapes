function [slands_new tlands_new slands tlands nseeds] = Pick_Top_AL_Exper(src, mask, uncertainty,invTx, invTy, ops, slands, tlands, tlands_pre)

    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
    tlands_pre = tlands;
%     mask1 = ( src > 0.2);
    
    ops.alpha = 1e-3; %0.3
    ops.mask = mask;
%     cert_thres1 = quantile(uncertainty(find(mask>0)),.85);
%     cert_thres1 = quantile(uncertainty,.85);
%     cert_thres2 = quantile(uncertainty(find(mask>0)),.999);
    S=( uncertainty == 3 ); 
%     S=( uncertainty > cert_thres1); % & uncertainty < cert_thres2);    
%     SS=( uncertainty > cert_thres2); % & uncertainty < cert_thres2);
%     slands_inds = find(SS);
%     slands = [];
%     slands(1,1) = mx( slands_inds(1) )';
%     slands(2,1) = my( slands_inds(1) )';
    length = size(slands,2);
    pre_length = length;
    
%         a = uncertainty(find(mask>0)); mn = mean(a);
%         dockf;hist(a)
%         hold on, plot(mn, 1:10000, 'r.-'), hold on, plot(cert_thres, 1:10000, 'g.-'), 
    SS = mask>0;       
    inds = find(S);
    if(sz(inds,1) ~= 0)    
        inds_real = find(SS);
        candidate_inds = inds_real(inds);
%       indexes = candidate_inds;
        candidates(1,:) = mx( candidate_inds )';
        candidates(2,:) = my( candidate_inds )';
%       SS = find(candidates(1,:) == slands(1,:) & candidates(2,:) == slands(2,:));
%       if(SS)
%           candidates(:,SS) = [];
%        end
%        candidates(3,:) = mz( candidate_inds )';
%        landmarks.X = slands(1,:); landmarks.Y = slands(2,:); landmarks.Z = slands(3,:);

% 
%        D(:,:,1) = invTx; D(:,:,2) = invTy;
%         invD = invertDefField(D);
%        invTx = invD(:,:,1); invTy = invD(:,:,2);
        Uncert = zeros(sz(mask));
        Uncert(candidate_inds) = uncertainty(inds);
        slands= pick_points(slands, candidates, ops, Uncert);
%        inds=local_max( double(S)); % indices to potentiall good seeds
%         inds = sub2ind(size(src), round(slands(1,length+1:end)), round(slands(2,length+1:end)));
        slands_new=slands;
        slands_new(:, 1:length) = [];
        inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));
%         if(MCase == 1)
            nx=invTx(inds);
            ny=invTy(inds);
%         else
%            nx=invTx(inds);
%            ny=invTy(inds);
%        end
        tlands_new(1,:) =  mx( inds)-nx; 
        tlands_new(2,:) =  my( inds)-ny; 
        adding_length = size(slands_new, 2);
        tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
    else
        slands_new = []; tlands_new = [];
    end
    nseeds = size(tlands, 2);
end
%     %        inds=local_max( double(S)); % indices to potentiall good seeds   
% 
% % %     inds=vl_localmax( double(S), 0.005, 2); % indices to potentiall good seeds   
% % %     nx=Tx(inds);
% % %     ny=Ty(inds);    
% % %     s=find( ( sqrt(  ny.^2 + nx.^2  ) ) > maxd );
% % %     inds(s) =[];
% 
% %     if ~isempty( inds )
% %         nx=Tx(inds);
% %         ny=Ty(inds);    
% %         slands=[];
% %         tlands=[];
%         slands(1,length+1:end) =  mx( inds); %     slands_next(2,:) =  mx( inds);
%         slands(2,length+1:end) =  my( inds); %    slands_next(2,:) =  my( inds);
%         tlands(1,length+1:end) =  mx( inds)-nx;% + .5 * randn(1, size(slands,2)); % -ny   yn / -nx n  / +nx y / +ny n
%         tlands(2,length+1:end) =  my( inds)-ny;% + .5 * randn(1, size(slands,2)); % -nx  
%         slands_new = slands; slands_new(:,1:length) = [];
%         tlands_new = tlands; tlands_new(:,1:length) = []; 
% %         tlands(:,1:pre_length) = tlands_pre;
%     nseeds = size(tlands,2);



% function [gx gy]= get_gradient ( I1 )
% 
%      [gx gy]=gradient( I1 );
% 
%      nrm= 1e-5 +sqrt( gx.^2 + gy.^2 );
%      gx=gx ./ nrm;
%      gy=gy ./ nrm;
% end
%     elseif(ops.pick_mode == 2 || ops.pick_mode == 5)
%          [gx gy] = get_gradient(src .* mask);
%          grad_src = sqrt( gx.^2 + gy.^2);
%         grad_uncertainty = uncertainty .* grad_src;
%         a=grad_uncertainty(mask>0);
%     end