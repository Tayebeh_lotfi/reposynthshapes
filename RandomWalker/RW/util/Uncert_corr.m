function [Mut_Info, Global_Corr] = Uncert_corr(ops)
    Unc_Mode = 1; % 1-2, 1-3, 1-4, 1-5, 1-6, 2-3, 2-4, 2-5, 2-6, 3-4, 3-5, 3-6, 4-5, 4-6, 5-6
    for Unc_Mode1 = 1 : 5,
        for Unc_Mode2 = Unc_Mode1+1 : 6,
            switch Unc_Mode1
                case 1
                    Unc = ops.Unc1;
                case 2
                    Unc = ops.Unc2;
                case 3
                    Unc = ops.Unc3;
                case 4
                    Unc = ops.Unc4_3;
                case 5
                    Unc = ops.Unc_gauss;

            end
            switch Unc_Mode2
                case 2
                    Unc_ = ops.Unc2;
                case 3
                    Unc_ = ops.Unc3;
                case 4
                    Unc_ = ops.Unc4_3;
                case 5
                    Unc_ = ops.Unc_gauss;
                case 6
                    Unc_ = ops.Unc5;
            end
            Unc_1_ = Unc_(ops.S);
            Unc1_ = Unc(ops.S);
            [Val1 Ind] = sort(Unc_1_(:));
            Val2 = Unc1_(Ind);

            gcc(:,:,Unc_Mode) = corrcoef(Unc_1_(:), Unc1_(:));
            idata1 = double(Unc_1_(:));
            idata2 = double(Unc1_(:));
            Mut_Info(Unc_Mode) = mutualinfo( idata1, idata2);
            
            dockf; plot(Val1(:),Val2(:), 'r-*'), title(['GCC = ', num2str(gcc(1,2,Unc_Mode)), ', MI = ', num2str(Mut_Info(Unc_Mode))]),
%             title(['global correlation uncert ' ...
%                 ops.Unc_str{Unc_Mode1} ' and ' ops.Unc_str{Unc_Mode2}]), ...
                xlabel(ops.Unc_str{Unc_Mode1}), ylabel(ops.Unc_str{Unc_Mode2});
            Unc_Mode = Unc_Mode + 1;
        end
    end

    Global_Corr = sqz(gcc(1,2,:))';
    [Val1 Ind] = sort(ops.Unc4_1(:));
    dockf; plot(ops.Unc4_1(:), 'b-'), hold on,
    plot(ops.Unc4_2(Ind), 'ro-'), hold on,
    plot(ops.Unc4_4(Ind), 'g-*'), hold on,
    plot(ops.Unc4_3(Ind), 'kv-.'), 
    legend('Lower bound', 'Upper Bound', 'Discrete', 'Lower\_Upper Average', 'Location', 'SouthEastOutside'),
end
