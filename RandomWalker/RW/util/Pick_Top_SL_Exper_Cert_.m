function [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert_(src, mask, uncertainty, dx, dy, ops, slands, tlands)
    [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2) );
 
    ops.alpha = 1e-1; %0.3
    ops.mask = mask;
    cert_thres1 = quantile(uncertainty(find(mask>0)),.1);
    Uncert = uncertainty .* mask;
    S=( Uncert < cert_thres1 & Uncert > 0); % & uncertainty < cert_thres2);    

    length = size(slands,2); 
%         a = uncertainty(find(mask>0)); mn = mean(a);
%         dockf;hist(a)
%         hold on, plot(mn, 1:10000, 'r.-'), hold on, plot(cert_thres, 1:10000, 'g.-'), 
    inds = find(S);
    if(sz(inds,1) ~= 0)    
        candidates(1,:) = mx( inds )';
        candidates(2,:) = my( inds )';
        slands= pick_points(slands, candidates, ops, Uncert);
        slands_new=slands;
        slands_new(:, 1:length) = [];
        inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));
        nx=dx(inds);
        ny=dy(inds);
        tlands_new(1,:) =  mx( inds)-nx; 
        tlands_new(2,:) =  my( inds)-ny; 
        adding_length = size(slands_new, 2);
        tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
    else
        slands_new = []; tlands_new = [];
    end
    nseeds = size(tlands, 2);
end
