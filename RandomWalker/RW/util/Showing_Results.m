function Showing_Results()
    clc, clear all, close all,    
    image_number = 20; lambda = 0.95; cert = 0.5;
    load '../results/good_set/AllErr_Rand_30iter_30land_80img.mat';
    load '../results/good_set/AllErr_SL_30iter_30land_80img.mat';
    load '../results/good_set/AllErr_AL_30iter_30land_80img.mat';
    
    figure,
    errorbar( mean(sqz(allerr_Rand)), std(sqz(allerr_Rand)), '--r' ), hold on
    errorbar( mean(sqz(allerr_SL)), std(sqz(allerr_SL)), '-*b' ), hold on
    errorbar( mean(sqz(allerr_AL)), std(sqz(allerr_AL)), '-.g' )
    error_measure = sprintf(['c) Field Warping MSE Error, dataset3 + 10%% noise with 6%% warping,' '\n' 'adding 2seeds']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'SL', 'AL', 'Location', 'SouthEastOutside'),
%     Show_Warping_Err(allerr_Rand, allerr_SL, allerr_AL, 1, 1, 0.95, 0.5);
%     Show_Warping_Err(AllErr_Rand, AllErr_SL, AllErr_AL, 1, 1, 0.95, 0.5);


    figure,
    errorbar( mean(sqz(AllErr_Rand)), std(sqz(AllErr_Rand)), '--r' ), hold on
%     errorbar( mean(sqz(allerr_SL_80img)), std(sqz(allerr_SL_80img)), '-*b' ), hold on
    errorbar( mean(sqz(AllErr_AL)), std(sqz(AllErr_AL)), '-.g' )
%     caption = sprintf('This is the first line.\nThis is the second line');
%     title(caption) 
    error_measure = sprintf(['Field Warping MSE Error, Ucla dataset + 1%% noise with 6%% warping,' '\n' 'adding 30seeds']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'Location', 'SouthEastOutside'),



    errorbar( mean(sqz(allerr_Rand)), std(sqz(allerr_Rand)), '--r' ), hold on
%     errorbar( mean(sqz(allerr_SL_80img)), std(sqz(allerr_SL_80img)), '-*b' ), hold on
    errorbar( mean(sqz(allerr_AL)), std(sqz(allerr_AL)), '-.g' )
    error_measure = {'Field Warping MSE Error on UCLA dataset with 5% noise,   '};
    title( [error_measure{1}]), xlabel('Iterations'), ylabel('MSE error'),
    legend('Random', 'AL', 'Location', 'SouthEastOutside'),


    errorbar( mean(sqz(allerr_field_AL1)), std(sqz(allerr_field_AL1)), '-.','color',[0.4 0.2 0], 'LineWidth', 2 ), hold on
    errorbar( mean(sqz(allerr_field_AL2)), std(sqz(allerr_field_AL2)),  '--','color',[0.1 0.7 0], 'LineWidth', 2  ), hold on
    errorbar( mean(sqz(allerr_field_AL3)), std(sqz(allerr_field_AL3)),  '-.','color',[0 0.2 0.8], 'LineWidth', 2  ), hold on
    errorbar( mean(sqz(BaseErr_AL)), std(sqz(BaseErr_AL)),  '-o','color',[0.9 0 0.3], 'LineWidth', 2  )
    error_measure = {'Field Warping MSE Error   '};
%     title( error_measure{1}), xlabel('Iterations'), ylabel('MSE error'),
    legend('AL 10iter', 'AL 4iter', 'AL 2iter', 'AL Baseline', 'Location', 'SouthEastOutside'),

    
    figure,
    plot3( 1:10, time_ord_avg1, mean(sqz(allerr_field_AL1(:,2:11))), '-o','color',[0.4 0.2 0], 'LineWidth', 2 ), hold on
    plot3( 1:4, time_ord_avg2, mean(sqz(allerr_field_AL2(:,2:5) )), '--','color',[0.1 0.7 0], 'LineWidth', 2  ), hold on
    plot3( 1:20, time_ord_avg3, mean(sqz(allerr_field_AL3(:,2:21) )), '-*','color',[0 0.2 0.8], 'LineWidth', 2  ), hold on
    plot3( 2:21, repmat(time_one_avg, [1 20]), repmat(mean(sqz(BaseErr_AL(:,2) )),[1 20]), '-.','color',[0.9 0 0.3], 'LineWidth', 2  )
    error_measure = {'Field Warping MSE Error'};xlabel('Iterations'), ylabel('Time'), zlabel('MSE error'),
%     title( error_measure{1} ), 
    legend('AL 10iter', 'AL 4iter', 'AL 2iter', 'AL Baseline', 'Location', 'SouthEastOutside'),
    grid
   
    
    figure,
    plot( time_ord_avg1, mean(sqz(allerr_field_AL1(:,2:11))), '-o','color',[0.4 0.2 0], 'LineWidth', 2 ), hold on
    plot( time_ord_avg2, mean(sqz(allerr_field_AL2(:,2:5) )), '--','color',[0.1 0.7 0], 'LineWidth', 2  ), hold on
    plot( time_ord_avg3, mean(sqz(allerr_field_AL3(:,2:21) )), '-*','color',[0 0.2 0.8], 'LineWidth', 2  ), hold on
    plot( 1:round(max(time_ord_avg3(:))), repmat(mean(sqz(BaseErr_AL(:,2) )),[1 round(max(time_ord_avg3(:)))]), '-.','color',[0.9 0 0.3], 'LineWidth', 2  )
    error_measure = {'Field Warping MSE Error   '};xlabel('Time'), ylabel('MSE error'),
    title( error_measure{1} ), 
    legend('AL 10iter', 'AL 4iter', 'AL 2iter', 'AL Baseline', 'Location', 'SouthEastOutside'),
    
  
    figure,
    plot( mean(sqz(allerr_field_AL1(:,2:11))), '-o','color',[0.4 0.2 0], 'LineWidth', 2 ), hold on
    plot( mean(sqz(allerr_field_AL2(:,2:5) )), '--','color',[0.1 0.7 0], 'LineWidth', 2  ), hold on
    plot( mean(sqz(allerr_field_AL3(:,2:21) )), '-*','color',[0 0.2 0.8], 'LineWidth', 2  ), hold on
    plot( repmat(mean(sqz(BaseErr_AL(:,2) )),[1 20]), '-.','color',[0.9 0 0.3], 'LineWidth', 2  )
    error_measure = {'Field Warping MSE Error   '};xlabel('Iterations'), ylabel('MSE error'), 
    title( error_measure{1} ), 
    legend('AL 10iter', 'AL 4iter', 'AL 2iter', 'AL Baseline', 'Location', 'SouthEastOutside'),
    
    figure,
    plot( allerr_field_AL1(:,end)  .*  time_ord1', '-o','color',[0.4 0.2 0], 'LineWidth', 2 ), hold on
    plot( allerr_field_AL2(:,end)  .*  time_ord2',  '--','color',[0.1 0.7 0], 'LineWidth', 2  ), hold on
    plot( allerr_field_AL3(:,end)  .*  time_ord3',   '-*','color',[0 0.2 0.8], 'LineWidth', 2  ), hold on
    plot( BaseErr_AL(:,end)  .* time_one',  '-.','color',[0.9 0 0.3], 'LineWidth', 2  )
    error_measure = {'Field Warping MSE Error,   '};xlabel('Images'), ylabel('MSE error'),
%     title( [error_measure{1}, ' lambda = ' num2str(lambda) ] ), 
    legend('AL 10iter', 'AL 4iter', 'AL 2iter', 'AL Baseline', 'Location', 'SouthEastOutside'),

    
    
    figure,
    errorbar( mean(sqz(allerr_Rand3D_Final)), std(sqz(allerr_Rand3D_Final)), '-or' ), hold on
%     errorbar( mean(sqz(allerr_SL_80img)), std(sqz(allerr_SL_80img)), '-*b' ), hold on
    errorbar( mean(sqz(allerr_AL3D_Final)), std(sqz(allerr_AL3D_Final)), '-*g' ), hold on
%     errorbar( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), std(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    plot( mean(sqz(repmat(allerr_ALBase3D_Final(:,2),[1 5]))), '--b' )
    error_measure = sprintf(['Field Warping MSE Error, dataset1 with 6%% warping,' '\n' 'adding 40seeds totally']);
    title( error_measure ), xlabel('Iterations'), ylabel('MSE error'), %, ' lambda = ' num2str(lambda)
    legend('Random', 'AL', 'AL Baseline', 'Location', 'SouthEastOutside'),
    
    
    

end