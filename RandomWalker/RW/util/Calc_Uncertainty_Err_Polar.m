function [Unc_ExpEr, Unc_ExpErMap] = Calc_Uncertainty_Err_Polar(X, dispvec, dx, dy)    
    start = 4;
    isz = sz(X);
    iszz = sz(dx);
    X = reshape( X, prod(iszz), [] );  
    [X1, IX] = sort(X,2);
    index = sz(IX,2)-start:sz(IX,2);
%     IX(:,index);
    XX  = X1(:,index);
    XXX = X1(:,end);
%     Disp = dispvec(IX(:,index),:);
%     Unc_ExpEr = sum( XX .* ( X * pdist2(dispvec, Disp) ), 2 ); 
    [Angles, Distance] = Calc_Angle(dispvec, dispvec);

    for pix = 1:sz(X,1),
        Angles1 = Angles(:,IX(pix,index));
        Distance1 = Distance(:,IX(pix,index));

        Angles2 = Angles(:,IX(pix,end));
        Distance2 = Distance(:,IX(pix,end));        
        
%         Disp = dispvec(IX(pix,index),:);
%         [Angles, Distance] = Calc_Angle(dispvec, Disp);
        Unc_ExpEr(pix) = sum( XX(pix,:) .* ( X(pix,:) * (Angles1 + Distance1) ), 2 );
        Unc_ExpErMap(pix) = X(pix,:) * ((Angles2 + Distance2));
    end
    
    
%     Unc_ExpEr = sum( X .* ( X * pdist2(dispvec, dispvec) ), 2 ); 
%     Disp = [dx(:) dy(:)];
%     [Angles, Distance] = Calc_Angle(dispvec, Disp);
%     
%     Unc_ExpErMap = sum( X .* ((Angles + Distance)), 2);

    Unc_ExpEr = reshape( Unc_ExpEr, isz(1), [] ); 
    Unc_ExpErMap = reshape( Unc_ExpErMap, isz(1), [] ); 
end