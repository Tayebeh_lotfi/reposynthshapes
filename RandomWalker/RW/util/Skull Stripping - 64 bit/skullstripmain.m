% Skull stripping by Graph cuts
%
% Inputs:	filename - Name of the image file entered via file-open dialog
%				box
%				Supported file format - NIfTI Analyze (.nii or .img/.hdr pair)
%			T - Threshold parameter (percentage of white matter (WM)
%				intensity
%				Range: 32 < T < 40
%				Default value = 36
%			k - Intensity parameter (controls the influence of pixel
%				intensity in determining the cut)
%				Range: 1 < k < 3
%				Default value = 2.3
% 
% Output: Skull stripped image (with '_gcut' suffix) saved in the same
%		  folder as the input imag

clear all; close all; clc;
disp('SKULL STRIPPING USING GRAPH CUTS');
[fname, pname, filterindex] = uigetfile('*.img;*.nii', 'Select image for skull stripping');
if (filterindex == 0)
	return;
end
filename = strcat(pname, fname);
T = 50;
k = 2.3;

skullstripping(filename, T, k);

