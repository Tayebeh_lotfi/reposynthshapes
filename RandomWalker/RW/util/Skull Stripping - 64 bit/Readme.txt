SKULL STRIPPING USING GRAPH CUTS - GCUT 64-bit version

This software code is based on the fully automatic skull stripping algorithm explained in the manuscript titled 'Improved skull stripping using graph cuts'. The software is written using Matlab and C. 

The algorithm consists of three steps, thresholding, removal of narrow connections using graph cuts and post-processing. There are two parameters involved, threshold parameter T given by the percentage of white matter intensity defining thresholding and the intensity parameter k that controls the influence of intensity in determining the position of the cut. T and k can take all real values from 32 to 40 and k from 1 to 3. The default values used for T and k are 36 and 2.3 respectively. If you wish to alter the values of T and k, you may do it by modifying the skullstripmain.m file.


System requirements:

2GHz processor
2GB of RAM
Windows XP or Vista 64bit version
Matlab 7.2 (R2006a) or later for Windows XP
Matlab 7.4 (R2007a) or later for Windows Vista


Memory requirements:

The code requires substantial amount of system memory for execution, especially for high resolution MR data, e.g. 1x1x1mm. In these cases, the code might fail to execute due to insufficient memory, i.e., produces 'Out of memory' error message,  To resolve this issue, do the following:

1. Set 3GB switch 
(a) For Windows XP, see http://www.mathworks.com/matlabcentral/fileexchange/9060
(b) For Windows vista, use 'BCDEdit /set increaseuserva 3072' from the command window

2. Increase virtual memory to at least 3072MB. 
(a) On Windows XP, go to My Computer -> Properties -> Advanced tab -> Performance settings -> Advanced tab -> Virtual memory change button. Set initial size and maximum size.
(b) On Windows Vista, go to My Computer -> Properties -> Advanced system settings -> Performance Settings -> Advanced tab -> Virtual memory change button. Set initial size and maximum size.


Data requirements:

Images must be stored in NIfTI Analyze format (.nii or .img/.hdr pair). The image dimensions should be no smaller than 64 pixels in all directions.


How to use:

Set the main directory as the current working directory in Matlab. Run skullstripmain.m. After execution, the result (skull stripped image) is saved in the same folder as the original image, with "_gcut" suffix, in NIfTI Analyze format. 


For technical support, send email to zvitali@ntu.edu.sg