% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set_Regress_test( AllErr, Label_set, Displace_field, Uncertainty, Landmarks, savepath, ...,
    Max_Iterations, num_seed, Time, trial_num, set_num, str, MCase, noise_sigma)

    file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Landmarks' 'Time'};    
    for i = 1:6,%6
        save (num2str([savepath 'AL_SL/Regression/ExpEr/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_ExpErU' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
    end
end