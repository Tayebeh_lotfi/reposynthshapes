function [Unc_Err_Dist Unc_Err_Dist_Map] = Calc_Uncertainty_Err_Dist2D_3D(X, dispvec, dx, dy, dz)    
    isz = sz(X);
    iszz = sz(dx);
    X = reshape( X, prod(iszz), [] );   
    Unc_Err_Dist = sum( X .* ( X * pdist2(dispvec, dispvec) ), 2 ); 
    Disp = [dx(:) dy(:) dz(:)];
    Unc_Err_Dist_Map = sum( X .* pdist2(Disp, dispvec), 2);

    Unc_Err_Dist = reshape( Unc_Err_Dist, isz(1), [] ); 
    Unc_Err_Dist_Map = reshape( Unc_Err_Dist_Map, isz(1), [] ); 
end