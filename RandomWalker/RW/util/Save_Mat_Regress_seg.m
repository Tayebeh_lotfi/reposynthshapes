% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_Regress_seg( AllErr, Landmarks, savepath, img_num, img_num2, noise_sigma, Mode)

    file_name = {'AllErr' 'Landmarks'};    
    for i = 1:2,%6
        save (num2str([savepath 'AL_SL/Regression/ExpEr/' file_name{i} num2str(img_num) '_to_img' num2str(img_num2) '_Mode' num2str(Mode) '_regress' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
    end
end