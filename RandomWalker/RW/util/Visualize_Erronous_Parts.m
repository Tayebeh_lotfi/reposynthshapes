function Visualize_Erronous_Parts(src, invTx, invTy, err, prd_err, mask)
    [mx my] = ndgrid( 1:size(invTx, 1), 1:size(invTx, 2) );
    cert_thres1 = 0; %quantile(uncertainty(find(mask>0)),.82);   
    cert_thres2 = quantile(err,.01);  %%(find(mask>0))
    S1=( err > cert_thres1 & err < cert_thres2);    
   

    cert_thres2 = quantile(prd_err,.01);  %%(find(mask>0))
    S2=( prd_err > cert_thres1 & prd_err < cert_thres2);    


    SS = mask>0;       
    inds_real = find(SS);
    inds_err = find(S1);
    candidate_inds_err = inds_real(inds_err);
    candidates_err(1,:) = mx( candidate_inds_err )';
    candidates_err(2,:) = my( candidate_inds_err )';

    inds_prd_err = find(S2);
    candidate_inds_prd_err = inds_real(inds_prd_err);
    candidates_prd_err(1,:) = mx( candidate_inds_prd_err )';
    candidates_prd_err(2,:) = my( candidate_inds_prd_err )';
    figure, subplot 121, imagesc(src), colormap(gray), hold on, plot(candidates_err(2,:), candidates_err(1,:), 'r*'), title('real error')
    subplot 122, imagesc(src), colormap(gray), hold on, plot(candidates_prd_err(2,:), candidates_prd_err(1,:), 'r*'), title('predicted error')
end
