function [slands_new tlands_new slands tlands nseeds] = Pick_Top_SL_Exper_Cert(src, mask, uncertainty, dx, dy, ops, slands, tlands)
    [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2) );
% %     mask1 = ( src > 0.1);
% %     mask2 = ( src_segment > 0);
% %     se = strel('disk',2);
% %     mask2 = imdilate(mask2,se);
% %     mask = mask1 .* mask2;
%     certainty = max(uncertainty(:)) - uncertainty + min(uncertainty(:));
    ops.alpha = 3e-1; % 0.01; %0.3
    ops.mask = mask;
    length = size(slands,2);
% %     certainty(mask>0) = min(certainty(:));
%     uncertainty = uncertainty .* mask;
%     cert_thres1 = 0; %quantile(uncertainty(find(mask>0)),.82);   
%     cert_thres2 = quantile(uncertainty,.1);  %%(find(mask>0))
%     S=( uncertainty > cert_thres1 & uncertainty < cert_thres2);    
    S=( uncertainty == 1);    
%         dockf;hist(certainty(mask>0)), hold on, plot(cert_thres, 1:10000, 'g.-')
%         hold on, plot(mn, 1:10000, 'r.-'), 
    SS = mask>0; 
    inds = find(S);
    if(sz(inds,1) ~= 0)
%         inds = find(S);
        inds_real = find(SS);
        candidate_inds = inds_real(inds);
    % candidate_inds = find(S==1);
        ops.num_seed = min(ops.num_seed, size(candidate_inds,1));
        candidates(1,:) = mx( candidate_inds )';
        candidates(2,:) = my( candidate_inds )';

        Uncert = zeros(sz(mask));
        Uncert(candidate_inds) = uncertainty(inds);
        slands= pick_points(slands, candidates, ops, Uncert);
    % %     inds=local_max( double(S)); % indices to potentiall good seeds
        slands_new=slands;
        slands_new(:, 1:length) = [];
        inds = sub2ind(size(src), round(slands_new(1,:)), round(slands_new(2,:)));

    %     if(MCase == 1)
            nx=dx(inds);
            ny=dy(inds);
    %     else
    %         nx=dx(inds);
    %         ny=dy(inds);
    %     end
    %     nx=dx(inds);
    %     ny=dy(inds);

        tlands_new(1,:) =  mx( inds)-nx; 
        tlands_new(2,:) =  my( inds)-ny; 
        adding_length = size(slands_new, 2);
        tlands(:,length+1:length+adding_length) = tlands_new(:, 1 : end);
    %     tlands(1,1) = mx(inds(1))-nx(1);
%     tlands(2,1) = my(inds(1))-ny(1);
    else
        slands_new = []; tlands_new = [];
    end
    nseeds = size(tlands, 2);
end
