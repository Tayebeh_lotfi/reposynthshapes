function mind= MIND_descriptors_extended(I,r,sigma)

% Calculation of MIND (modality independent neighbourhood descriptor)
%
% If you use this implementation please cite:
% M.P. Heinrich et al.: "MIND: Modality Independent Neighbourhood
% Descriptor for Multi-Modal Deformable Registration"
% Medical Image Analysis (2012)
%
% Contact: mattias.heinrich(at)eng.ox.ac.uk
%
% I: input volume (3D)
% r: half-width of spatial search (large values may cause: "out of memory")
% r=0 uses a six-neighbourhood (Section 3.3) other dense sampling
% sigma: Gaussian weighting for patches (Section 3.1.1)
%fa
% mind: output descriptor (4D) (Section 3.1, Eq. 4)
%

I=single(I);

if nargin<3
    sigma=0.5;
end
if nargin<2
    r=0;
end

% Filter for efficient patch SSD calculation (see Eq. 6)
filt=fspecial('gaussian',[ceil(sigma*3/2)*2+1,1],sigma);

 
[xs,ys,zs]=search_region(r);
[xs0,ys0,zs0]=search_region(0);
Dp=zeros([size(I),length(xs0)],'single');

% Calculating Gaussian weighted patch SSD using convolution
for i=1:numel(xs0)
    Dp(:,:,:,i)=volfilter((I-volshift(I,xs0(i),ys0(i),zs0(i))).^2,filt);
end

% Variance measure for Gaussian function (see Section 3.2)
V=(mean(Dp,4));


I1=zeros([size(I),length(xs0)],'single');
for i=1:numel(xs0)
    I1(:,:,:,i)=exp(-Dp(:,:,:,i)./V);
end

% normalise descriptors to a maximum of 1 (only within six-neighbourhood)
max1=max(I1,[],4);

mind=zeros([size(I),length(xs)],'single');
% descriptor calculation according to Eq. 4
if r>0
    for i=1:numel(xs)
        mind(:,:,:,i)=exp(-volfilter((I-volshift(I,xs(i),ys(i),zs(i))).^2,filt)./V)./max1;
    end
else
    % if six-neighbourhood is used, all patch distances are already calculated
    for i=1:numel(xs0)
        mind(:,:,:,i)=I1(:,:,:,i)./max1;
    end
end

%%
%   I : The input image volume (vessel volume)
%   Options : Struct with input options,
%       .FrangiScaleRange : The range of sigmas used, default [1 8]
%       .FrangiScaleRatio : Step size between sigmas, default 2
%       .FrangiAlpha : Frangi vesselness constant, treshold on Lambda2/Lambda3
%					   determines if its a line(vessel) or a plane like structure
%					   default .5;
%       .FrangiBeta  : Frangi vesselness constant, which determines the deviation
%					   from a blob like structure, default .5;
%       .FrangiC     : Frangi vesselness constant which gives
%					   the threshold between eigenvalues of noise and 
%					   vessel structure. A thumb rule is dividing the 
%					   the greyvalues of the vessels by 4 till 6, default 500;
%       .BlackWhite : Detect black ridges (default) set to true, for
%                       white ridges set to false.
%       .verbose : Sho

ops.BlackWhite =1;
ops.FrangiScaleRange=[1 8];
ops.FrangiScaleRatio=1; 
ops.FrangiBeta=0.5;
ops.FrangiC= 5;         
res=1;
b=FrangiFilter3D(double(I(1:res:end,1:res:end,1:res:end)),ops);
clf; imre(b,2,110);
ops.BlackWhite =0;
c=FrangiFilter3D(double(I(1:res:end,1:res:end,1:res:end)),ops);
 
% cbar
% %%
% for in=10:-1:1
% % incc
% % intensity, gabor, graidents 
% [mind2(:,:,:,in)] = (FrangiFilter3D(double(I),ops));
% end       


% the following can improve robustness
% (by limiting V to be in smaller range)
% val1=[0.01*(mean(V(:))),100*mean(V(:))];
% V=(min(max(V,min(val1)),max(val1)));

c2=exp(-c);
b2=exp(-b);
mind2(:,:,:,2)=c2;
mind2(:,:,:,1)=b2;

max2=max(mind2,[],4) +1e-5;

for i=1:2    
    mind(:,:,:,i+len(xs0) )=mind2(:,:,:,i)./max2;
end
% gabout1 = zeros([sz(I) 64]);gabout2=gabout1;
% 
% for f=.05:.1:.4;
%     for theta =0 % :pi/3: 2*pi - pi/3% 0 :2*pi/6: 2*pi
%     for Sx = [2 4 6]
%         Sy = Sx-1;    
%         incc;
%         [G1,G2,gabout1(:,:,inc),gabout2(:,:,inc)] = gaborfilter2( double(ff),Sx,Sx,f,theta);    
%     end
%     end
%  end    
% R=abs(gabout1);
% R(:,:,13:13+11)=abs(gabout2);
%     

function [xs,ys,zs]=search_region(r)

if r>0
    % dense sampling with half-width r
    [xs,ys,zs]=meshgrid(-r:r,-r:r,-r:r);
    xs=xs(:); ys=ys(:); zs=zs(:);
    mid=(length(xs)+1)/2;
    xs=xs([1:mid-1,mid+1:length(xs)]);
    ys=ys([1:mid-1,mid+1:length(ys)]);
    zs=zs([1:mid-1,mid+1:length(zs)]);
else
    % six-neighbourhood
    xs=[1,-1,0,0,0,0];
    ys=[0,0,1,-1,0,0];
    zs=[0,0,0,0,1,-1];
end

function vol=volfilter(vol,h,method)

if nargin<3
    method='replicate';
end
% volume filtering with 1D seperable kernel (faster than MATLAB function)
h=reshape(h,[numel(h),1,1]);
vol=imfilter(vol,h,method);
h=reshape(h,[1,numel(h),1]);
vol=imfilter(vol,h,method);
h=reshape(h,[1,1,numel(h)]);
vol=imfilter(vol,h,method);

function vol1shift=volshift(vol1,x,y,z)

[m,n,o,p]=size(vol1);

vol1shift=zeros(size(vol1));


x1s=max(1,x+1); x2s=min(n,n+x);
y1s=max(1,y+1); y2s=min(m,m+y);
z1s=max(1,z+1); z2s=min(o,o+z);

x1=max(1,-x+1); x2=min(n,n-x);
y1=max(1,-y+1); y2=min(m,m-y);
z1=max(1,-z+1); z2=min(o,o-z);

vol1shift(y1:y2,x1:x2,z1:z2,:)=vol1(y1s:y2s,x1s:x2s,z1s:z2s,:);
