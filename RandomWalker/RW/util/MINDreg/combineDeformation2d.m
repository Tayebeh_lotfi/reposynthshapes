function [u_combined,v_combined]=combineDeformation2d(u1st,v1st,u2nd,v2nd,method)

if nargin<5
    method='compositive';
end

if strcmp(method,'compositive')
    u_combined=volWarp(u1st,u2nd,v2nd)+u2nd;
    clear u1st;
    v_combined=volWarp(v1st,u2nd,v2nd)+v2nd;
    clear v1st;
else
    u_combined=u1st+u2nd;
    v_combined=v1st+v2nd;
 
end
    