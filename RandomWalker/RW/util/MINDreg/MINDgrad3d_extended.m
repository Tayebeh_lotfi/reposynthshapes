function [Sx,Sy,Sz,S,Sxi,Syi,Szi]=MINDgrad3d_extended(mind1o,mind2o, fids, w , product)
%symmetric calculation of derivatives of MIND


[m,n,o,p]=size(mind1o);
nfs=len( unique(fids));

fac = 3;
fac2= fac/2; %nfs^2;

if nargin==4 product=0;end
for f=nfs:-1:1
    mind1=mind1o(:,:,:, fids == f);
    mind2=mind2o(:,:,:, fids == f);
    A1=mean(abs(volshift(mind2,1,0,0)-mind1),4);
    A2=mean(abs(volshift(mind2,-1,0,0)-mind1),4);
    Sx2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind2,0,1,0)-mind1),4);
    A2=mean(abs(volshift(mind2,0,-1,0)-mind1),4);
    Sy2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind2,0,0,1)-mind1),4);
    A2=mean(abs(volshift(mind2,0,0,-1)-mind1),4);
    Sz2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind1,1,0,0)-mind2),4);
    A2=mean(abs(volshift(mind1,-1,0,0)-mind2),4);
    Sxi2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind1,0,1,0)-mind2),4);
    A2=mean(abs(volshift(mind1,0,-1,0)-mind2),4);
    Syi2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind1,0,0,1)-mind2),4);
    A2=mean(abs(volshift(mind1,0,0,-1)-mind2),4);
    Szi2(:,:,:,f)=A1-A2;    
    S2(:,:,:,f)=mean(abs(mind2-mind1),4);
end

if nfs> 1
if product
    f=1;
    Sx = do_nothing(Sx2(:,:,:,f));
    Sy = do_nothing(Sy2(:,:,:,f));
    Sz = do_nothing(Sz2(:,:,:,f));
    Sxi =do_nothing(Sxi2(:,:,:,f));
    Syi =do_nothing(Syi2(:,:,:,f));
    Szi =do_nothing(Szi2(:,:,:,f));
    S =do_nothing(S2(:,:,:,f));    
    for f=nfs:-1:2
    Sx = Sx  .* do_nothing(Sx2(:,:,:,f));
    Sy = Sy .* do_nothing(Sy2(:,:,:,f));
    Sz = Sz .* do_nothing(Sz2(:,:,:,f));
    Sxi = Sxi .* do_nothing(Sxi2(:,:,:,f));
    Syi = Syi  .* do_nothing(Syi2(:,:,:,f));
    Szi = Szi .* do_nothing(Szi2(:,:,:,f));
    S = S .* do_nothing(S2(:,:,:,f));
    end
else
    f=1;
    Sx = do_nothing(Sx2(:,:,:,f));
    Sy = do_nothing(Sy2(:,:,:,f));
    Sz = do_nothing(Sz2(:,:,:,f));
    Sxi =do_nothing(Sxi2(:,:,:,f));
    Syi =do_nothing(Syi2(:,:,:,f));
    Szi =do_nothing(Szi2(:,:,:,f));
    S =do_nothing(S2(:,:,:,f));
    
    if isempty(w) w=ones(1,nfs); end
    for f=nfs:-1:2
    Sx = Sx  + w(f) * do_nothing(Sx2(:,:,:,f));
    Sy = Sy + w(f)* do_nothing(Sy2(:,:,:,f));
    Sz = Sz + w(f)* do_nothing(Sz2(:,:,:,f));
    Sxi = Sxi + w(f)* do_nothing(Sxi2(:,:,:,f));
    Syi = Syi  + w(f)* do_nothing(Syi2(:,:,:,f));
    Szi = Szi + w(f)* do_nothing(Szi2(:,:,:,f));
    S = S + w(f)* do_nothing(S2(:,:,:,f));
    end
end
    S=do_nothing(S)*fac2;
    
    Sx=do_nothing(Sx)*fac;
    Sy=do_nothing(Sy)*fac;
    Sz=do_nothing(Sz)*fac;
    Sxi=do_nothing(Sxi)*fac;
    Syi=do_nothing(Syi)*fac;
    Szi=do_nothing(Szi)*fac;
end

Sx(:,[1,n],:)=0;
Sy([1,m],:,:)=0;
Sz(:,:,[1,o])=0;
Sxi(:,[1,n],:)=0;
Syi([1,m],:,:)=0;
Szi(:,:,[1,o])=0;
if 0
    %%
    clf
    subplot 221;hoff; imre(do_nothing(Sy2(:,:,:,1)) , 2,111);
    subplot 222;hoff; imre(do_nothing(Sy2(:,:,:,2)) , 2,111);
    subplot 223;hoff; imre(do_nothing(Sy2(:,:,:,1)) .* do_nothing(Sy2(:,:,:, 2)), 2,111);
    subplot 224;hoff; imre(Sy, 2,111);
    %%
end
    
function a = do_nothing(a)


function vol1shift=volshift(vol1,x,y,z)

x=round(x);
y=round(y);
z=round(z);

[m,n,o,p]=size(vol1);

vol1shift=zeros(size(vol1));

x1s=max(1,x+1);
x2s=min(n,n+x);

y1s=max(1,y+1);
y2s=min(m,m+y);

z1s=max(1,z+1);
z2s=min(o,o+z);

x1=max(1,-x+1);
x2=min(n,n-x);

y1=max(1,-y+1);
y2=min(m,m-y);

z1=max(1,-z+1);
z2=min(o,o-z);

vol1shift(y1:y2,x1:x2,z1:z2,:)=vol1(y1s:y2s,x1s:x2s,z1s:z2s,:);
