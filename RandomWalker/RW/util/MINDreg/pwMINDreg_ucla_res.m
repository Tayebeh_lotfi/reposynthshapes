% clear ol
% ol(57,40,4)=0;
names={'SSD',  'VESS', 'ENT', 'MIND', 'combined', 'combined-0.3', 'combined-0.7'...
    'weighted', 'weighted1', 'mind2'};
clc
for a=1
    for d=2:40           
        switch a
            case 1                
            s=sprintf('X:/results/ucla/symgn_ssd/%d_distances.mat', d);
            case 2
            s=sprintf('X:/results/ucla/symgn_vess2/%d_distances.mat', d);
            case 3
            s=sprintf('X:/results/ucla/symgn_ent/%d_distances.mat', d);                
            case 4
            s=sprintf('X:/results/ucla/symgn_mind/%d_distances.mat', d);                            
             case 5
            s=sprintf('X:/results/ucla/combined/%d_distances.mat', d);                            
            case 6
            s=sprintf('X:/results/ucla/symgn_combined_pt3/%d_distances.mat', d);                                         
            case 7
            s=sprintf('X:/results/ucla/symgn_combined_pt7/%d_distances.mat', d);                                         
            case 8
            s=sprintf('X:/results/ucla/symgn_weighted/%d_distances.mat', d);                                                                              
            case 9
            s=sprintf('X:/results/ucla/weighted1/%d_distances.mat', d);                                                                  
            case 10                                
             s=sprintf('X:/results/ucla/symgn_mind_r2/%d_distances.mat', d);                                                                  
            case 11                
            s=sprintf('X:/results/ucla/combined1/%d_distances.mat', d);                                                                  
            case 12
            s=sprintf('X:/results/ucla/combined1_pt7/%d_distances.mat', d);    
            case 13
            s=sprintf('X:/results/ucla/combined1_pt3/%d_distances.mat', d);    
        end
       try
       l= load(s);        
       ol(:,d,a)= l.seg_mea.dice;                    
%         s2=sprintf('X:/results/ucla/symgn_mind/%d_fld.mat', d);                            
%         load(s2, 'i')    
%         save('X:/results/ucla/mind_refine/%d_distances.mat', 'seg_mea');                            
        catch,disp(s), continue;end  
    end
end
% load('X:\results\ucla\mind\all_distances.mat')

ol( ol==0)=NaN;

%%
% tids=1:40
clf; subplot 311; 
% m1=nanmean(ol(2:end,tids,1)');
m2=nanmean(ol(1:end,tids,2)');
% m3=nanmean(ol(2:end,tids,3)');
m4=nanmean(ol(1:end,tids,4)');
m5=nanmean(ol(1:end,tids,5)');
m6=nanmean(ol(1:end,tids,6)');
% m7=nanmean(ol(1:end,tids,7)');
 m8=nanmean(ol(1:end,tids,8'));
% plot(m1, 'm.-')
hon; plot(m2, 'b.-')
% hon; plot(m3, 'r.-')
plot(m4, 'g.-')
plot(m5, 'r.-')

plot(nanmean(overlap0(2:end,tids)'), 'k.-')
legend('SSD',  'VESS', 'ENT', 'MIND', 'combined', 'Before')
legend(  'VESS', 'MIND', 'combined', 'Before')

title 'Mean Dice coeff. (DC) of 56 regions'
grid
xlabel 'i-th structure'

clf
subplot 311
bar( m2 - m4)
title 'VESS better than MIND'
grid minor
subplot 312
bar( m5 - m4)
grid minor
title 'Combined better than MIND'
subplot 313
bar( m5 - m2)
grid minor
title 'Combined better than VESS'
%%
subplot 321;
bar(m1- m3)
title 'MO(SSD) - MO(ENT)'
grid
subplot 322;
bar(m1- m2)
title 'MO(SSD) - MO(VESS)'
grid
subplot 323;
bar(m3- m2)
title 'MO(ENT) - MO(VESS)'
grid
subplot 324;
bar(m4- m1)
title 'MO(MIND) - MO(SSD)'
grid

subplot 325;
bar(m4- m2)
title 'MO(MIND) - MO(ENT)'
grid
subplot 326;
bar(m4- m3)
title 'MO(MIND) - MO(VESS)'
grid
%%
subplot 322;
bar(m1- m2)
title 'MO(SSD) - MO(VESS)'
title 'Mean Dice coeff. (DC) of 56 regions'
grid
xlabel 'i-th structure'
%%
% 
 overlap3=zeros(57,40);
 overlap4a=zeros(57,40);
 clc
for d=1:40
    try    
    s=sprintf('X:/results/ucla/mind3_masked/%d_distances.mat', d);
    j=load(s);
    overlap3(:,d)= j.seg_mea.dice;    
  catch         
      s
      continue;end  
end
%%
overlap3(overlap3==0)=NaN;

 for d=1:40
    try
    s=sprintf('X:/results/ucla/mind4a/%d_distances.mat', d)
    j=load(s);
    overlap4a(:,d)= j.seg_mea.dice;    
    catch; continue;end
end
overlap4a(overlap4a==0)=NaN;

clf; 
hon

% subplot 131;
tids=20:35;
s=load('X:/results/ucla/mind/all_distances.mat')
plot(mean(s.overlap (2:end,tids)'), '.g-')
% ops1=s.ops

s=load('X:/results/ucla/mind2/all_distances.mat')
ops2 =s.ops;
plot(mean(s.overlap (2:end,tids)'), '.k-')



s=load('X:/results/ucla/mind3/all_distances.mat');
ops3 =s.ops;
plot(mean(s.overlap (2:end,tids)'), 'ro-')
hon
% legend('\alpha=.4', '\alpha=.2', '\alpha=.2| more itns' )
title (sprintf('DCs of 57 regions (%d trials)',length( tids)))
xlabel 'Region #'
axis([ 0 58 .4 1])
grid
 


% plot(nanmean(overlap2(2:end,tids)'), 'pg-')

plot(nanmean(overlap4a(2:end,tids)'), 'sb-')


s=load('X:/results/ucla/mind4/all_distances.mat');
plot(mean(s.overlap (2:end,tids)'), '.c-')
ops4=s.ops;

legend('\alpha=.4', '\alpha=.2' ,...
    '\alpha=.2| more itns' , ...
    '\alpha=.6| more itns','\alpha=.2| more itns| vesselness',...
    'location', 'north')
% ,std(overlap (2:end,tids)') ,1)
% barweb(mean(overlap (2:end,tids)'),std(overlap (2:end,tids)') ,1)

% h=bar(mean(overlap (2:end,tids)'),'g')
% labelplot(h, num2str(unique(m1)), 1, 90)

%%
%%


clf
subplot 211
tids=2:9
s=load('X:/results/ucla/mind3/all_distances.mat');
ops3 =s.ops;
m=mean(s.overlap (2:end,tids)');
plot(m, 'ro-')
hon
% legend('\alpha=.4', '\alpha=.2', '\alpha=.2| more itns' )
title (sprintf('DCs of 57 regions (%d trials)',length( tids)))
xlabel 'Region #'
axis([ 0 58 .4 1])
grid
 
s=load('X:/results/ucla/mind3_masked/all_distances.mat');
ops3a =s.ops;
m1=nanmean(overlap3(2:end,2:9)');
plot(m1, 'sb-')
legend('\alpha=.2| more itns', 'w/ masking')



subplot 212
bar(m-m1); title 'positive: NoMask better than Mask'
% axis([ 0 58 .4 1])
grid

saveas(gcf,sprintf('%s/results/ucla/mind_res/on_settings_masking_n8', getroot ), 'png')



%%

for d=1:16
    subplot(3,6,d)
i=(sqz(mind1(:,116,:,d)));cbar
a=imre( m1, 2,116) > 0;
hoff
ims(a.*i)
end


%%
clc
for d=2:40
    ff=sprintf('ssd.%d.pbs',d);
    writePBSgeneral(...
    sprintf(['cd ~/code/matlab/matching; '...
    'ops.r =0;  ops.levels=[4,2,1]; ops.warps =[ 15 15 15]; ops.metric = ''ssd'';'...
    'pwMINDreg_ucla_all_func2(%2d, 0,''~/results/ucla/symgn_ssd/'', ops);'], d), ...
    ['x:/results/' ff] ,1, 4, '4GB',1, '' )
    disp(['qsub ' ff ])
end

%%

for d=2:40
    ff=sprintf('c3_%d.pbs',d);
    writePBSgeneral(...
    sprintf(['cd ~/code/matlab/matching; '...
    'ops.global_weights =[ .3 .7]; ops.r =0;  ops.levels=[4,2,1]; ops.warps =[ 15 15 15]; ops.metric = ''combined1'';'...
    'pwMINDreg_ucla_all_func2(%2d, 0,''~/results/ucla/combined1_pt3/'', ops);'], d), ...
    ['x:/results/' ff] ,1, 4, '4GB',1, '' )
    disp(['qsub ' ff ])
end
%%
writePBSgeneral(...
    sprintf(['cd ~/code/matlab/matching; ops.warps =[15 15 15]; ops.metric = ''weighted'';'...
    'pwMINDreg_ucla_all_func2(%2d, 0,''~/results/ucla/weighted/'', ops);'], d), ...
    'x:/results/weighted.pbs' ,1,3, '5GB',1, '', '2-40' )
%%
mask1=double(analyze75read( [Dir2 sfiles(t1).name] ));
dd=[48 45 49 52 56] +1;
clr=cmapj(5)
clf; for a=1:5    
    patch( isosurface( mask1 ==  labels(dd(a)) ), 'facecolor', clr(a,:), 'edgecolor', 'none')
end

axis([0 181 0 217 0 181])

mask1 == 0
patch( isosurface( mask1 ==  labels(dd(a)) ), 'facecolor', clr(a,:), 'edgecolor', 'none')
