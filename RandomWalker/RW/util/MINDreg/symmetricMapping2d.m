function [u1s,v1s,u2s,v2s]=symmetricMapping2d(u1,v1,u2,v2)


[u2i,v2i]=fastInverse2d(u2./2,v2./2);
[u1i,v1i]=fastInverse2d(u1./2,v1./2);
[u1s,v1s]=combineDeformation2d(u1./2,v1./2,u2i,v2i,'compositive');
[u2s,v2s]=combineDeformation2d(u2./2,v2./2,u1i,v1i,'compositive');


if 0
    %%
    
    subplot 131;
    ims(u2i(:,:,12))
    subplot 132
    ims(u1i(:,:,12))
    subplot 133
    ims(u1s(:,:,12))
    
end