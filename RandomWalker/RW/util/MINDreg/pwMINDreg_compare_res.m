function tids= pwMINDreg_compare_res(ol, i1, i2 ,tids )


r1=find(isnan( sum(ol(2:end,:,i1)) ) ==0);
r2=find(isnan( sum(ol(2:end,:,i2)) ) ==0);

if nargin<4
tids=intersect(r1, r2);
end
%%
names={'SSD',  'VESS', 'ENT', 'MIND', 'Cpt5', 'Cpt3', 'Cpt7'...
    'weighted', 'Adapt:S+M', 'MIND1-2', 'Eq', '0.7M+.3SSD', '0.3M+.7SSD' '' };

for d=1:57  pvals(d)=anova1(reshape(ol(d, tids,[ i1 i2]) ,1,[]), repGrpLabels( len(tids), names{i1},names{i2} ) ,'off'); end

ii=find(pvals<.05);

hoff;
bar(sqz(nanmean(ol(:,tids,[ i1  ]),2 )) - sqz(nanmean(ol(:,tids,[ i2 ]),2 )) )
hon;
plot(ii, ones(len(ii),1)*.5, 'k.')
axis([0 60 -.5 .5]);
xlabel(dispt(0, '(ntrials: ', len(tids), ')' ))
if 0
%%
subplot 311
barweb(sqz(nanmean(ol(:,tids,[ i1 i2  ]),2 )) , sqz(nanstd(ol(:,tids,[ i1 i2 ]),0, 2 )) )
title (sprintf('%s better than %s'), names{i1},names{i2} )

saveas(gcf,sprintf('%s/results/ucla/mind_res/latest_%s_%s_n', ...
getroot,names{i1},names{i2}, len(tids)), 'png')
end