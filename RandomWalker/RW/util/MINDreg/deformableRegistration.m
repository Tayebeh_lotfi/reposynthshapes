function [u1,v1,w1,u2,v2,w2,deformed1,deformed2, R]=deformableRegistration(vol1,vol2, ops)
% Symmetric Gauss-Newton registration
%
% Uses diffusion regularization + MIND descriptor
% 
% If you use this implementation please cite:
% M.P. Heinrich et al.: "MIND: Modality Independent Neighbourhood
% Descriptor for Multi-Modal Deformable Registration"
% Medical Image Analysis (2012)
%
% Contact: mattias.heinrich(at)eng.ox.ac.uk
%
% for details see Sec. 4.2. in paper
% vol1: input volume (3D)
% vol2: input volume (3D) needs to has the same resolution and dimensions
%
% alpha: regularisation (see Eq. 12) (higher value give smoother field)
%
% u1,v1,w1: output flow-field which transforms vol2 towards vol1
% u2,v2,w2: output flow-field which transforms vol1 towards vol2
%
% deformed1: transformed vol1 (into anatomical space of vol2)
% deformed2: transformed vol2 (into anatomical space of vol1)
%

% to run this code you need to compile (mex) the following files
% trilinearSingle.cpp
% pointsor3d2.c

if ~isfield( ops, 'alpha') ops.alpha =.3; else alpha = ops.alpha; end
% resolution levels
if ~isfield( ops, 'levels') levels=[4,2,1]; else levels= ops.levels; end
if ~isfield( ops, 'r')  ops.r =0; end
if ~isfield( ops, 'alpha')  ops.alpha =.2; end
if ~isfield( ops, 'sigma')  ops.sigma =.5; end
if ~isfield( ops, 'metric')  ops.metric = 'mind'; end
if ~isfield( ops, 'warps') warps=[8,4,2]; else warps = ops.warps; end
if ~isfield( ops, 'mask') mask =ones(sz(vol1)); else
    disp 'Will use mask'
    mask = ops.mask; end

vol1=single(vol1); vol2=single(vol2);

u1=zeros(2,2,2,'single'); v1=u1; w1=u1; u2=u1; v2=u1; w2=u1;
% initialise flow fields with 0

 % number of warps per level (increase if needed)
                           
 addpath([getroot '/code/matlab/matching/MIRT/utils/'])

                           
% h=waitbar(0,'Performing deformable (multi-modal) registration');
complexity=[];
for j=1:length(levels)
    complexity=[complexity,ones(1,warps(j)).*levels(j).^(-3)];
end
complexity=cumsum(complexity)/sum(complexity);
time0=cputime; count=0; % some calculations to determine run-time

for j=1:length(levels)

    dispt(1,'------- Level: ',j);
    if isfield( ops, 'extended_desc') && j==length(levels)
    disp 'Ext. desr'; end
       
    maxwarp=warps(j);

    hs=fspecial('gaussian',[15,1],levels(j)/2);
    vol1f=volresize(volfilter(vol1,hs),size(vol1)./levels(j));
    vol2f=volresize(volfilter(vol2,hs),size(vol2)./levels(j));
    % resize volumes for current level
    
     if isfield( ops, 'metric')         
         disp(['metric: ' num2str( ops.metric )])         
    end
    if isfield( ops, 'mask')
    vol1m=volresize(volfilter(mask,hs),size(mask)./levels(j));
    end
    
    if isfield( ops, 'InitFld')
        u2 = ops.InitFld.u2{j};
        v2 = ops.InitFld.v2{j};
        w2 = ops.InitFld.w2{j};       
        
        [u2i,v2i,w2i]=fastInverse3d(u2./2,v2./2,w2./2);
        [u1i,v1i,w1i]=fastInverse3d(u1./2,v1./2,w1./2);
        
    else
        [u1,v1,w1]=resizeFlow(u1,v1,w1,size(vol1f));
        [u2,v2,w2]=resizeFlow(u2,v2,w2,size(vol1f));
    end
    % upsample flow to current level
    


    
    for i=1:maxwarp
        
        warped1=volWarp(vol2f,u1./2,v1./2,w1./2);
        warped2=volWarp(vol1f,u2./2,v2./2,w2./2);
        % transform volumes to intermediate space                
                  
        switch lower(ops.metric)
            case 'mi'
                [MI, GMI]=mirt_MI(warped1,warped3, ops.nbins) ;          
               
            case 'vess'
                mind1=MIND_similarity2(warped1, ops.r, ops.sigma, 2);
                mind2=MIND_similarity2(warped2, ops.r, ops.sigma, 2);        
                [Sx,Sy,Sz,S,Sxi,Syi,Szi]=MINDgrad3d(mind1,mind2);
            case 'mind'
                mind1=MIND_similarity(warped1, ops.r, ops.sigma, 1);
                mind2=MIND_similarity(warped2, ops.r, ops.sigma, 1);        
                [Sx,Sy,Sz,S,Sxi,Syi,Szi]=MINDgrad3d(mind1,mind2);
            case {'mind+v', 'v+mind'}        
                if j==length(levels)
                mind1=MIND_similarity2(warped1, ops.r, ops.sigma, 3);
                mind2=MIND_similarity2(warped2, ops.r, ops.sigma, 3);   
                else
                mind1=MIND_similarity2(warped1, ops.r, ops.sigma, 1);
                mind2=MIND_similarity2(warped2, ops.r, ops.sigma, 1);   
                end
                [Sx,Sy,Sz,S,Sxi,Syi,Szi]=MINDgrad3d(mind1,mind2);
        end            
        
        % extract modality independent neighbourhood descriptor        
%         if isfield( ops, 'mask')
%         mind1=mind1.*(repmat(vol1m,[1 1 1 sz(mind1,4)]));
%         mind2=mind2.*(repmat(vol1m,[1 1 1 sz(mind1,4)]));
%         end
        
        % calculate derivates of MIND
        

        [u1,v1,w1]=oneStepSOR3d(Sxi,Syi,Szi,S,u1,v1,w1,alpha);
        [u2,v2,w2]=oneStepSOR3d(Sx,Sy,Sz,S,u2,v2,w2,alpha);
        % solve Euler-Lagrange equations using successive overrelaxation
        % includes (compositive) diffusion regularisation in cost term

        clear Sx; clear Sy; clear Sz; clear S; clear Sxi; clear Syi; clear Szi;
        [u1,v1,w1,u2,v2,w2]=symmetricMapping3d(u1,v1,w1,u2,v2,w2);
        % ensure symmetric Mapping (see Sec. 4.3)
        time1=cputime; count=count+1;
        %waitbar(complexity(count));
        disp(['remaining time is approx. ',num2str((time1-time0)/complexity(count)*(1-complexity(count))),' secs.']);
    end
    
    R.u2{j} = u2;
    R.v2{j} = v2;
    R.w2{j} =w2;
        
end

deformed2=volWarp(vol2,u1,v1,w1);
deformed1=volWarp(vol1,u2,v2,w2);
% generate final output volumes
