function [Sx,Sy,Sz,S,Sxi,Syi,Szi, Si]=MINDgrad3d_extended_adapt(mind1o,mind2o, fids, w1, w2 )
%symmetric calculation of derivatives of MIND
[m,n,o,p]=size(mind1o);
nfs=len( unique(fids));

for f=nfs:-1:1
    mind1=mind1o(:,:,:, fids == f);
    mind2=mind2o(:,:,:, fids == f);
    A1=mean(abs(volshift(mind2,1,0,0)-mind1),4);
    A2=mean(abs(volshift(mind2,-1,0,0)-mind1),4);
    Sx2(:,:,:,f)=A1-A2; % img2 is moving image; img1 fixed
    A1=mean(abs(volshift(mind2,0,1,0)-mind1),4);
    A2=mean(abs(volshift(mind2,0,-1,0)-mind1),4);
    Sy2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind2,0,0,1)-mind1),4);
    A2=mean(abs(volshift(mind2,0,0,-1)-mind1),4);
    Sz2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind1,1,0,0)-mind2),4);
    A2=mean(abs(volshift(mind1,-1,0,0)-mind2),4);
    Sxi2(:,:,:,f)=A1-A2; % img2 = fixed image
    A1=mean(abs(volshift(mind1,0,1,0)-mind2),4);
    A2=mean(abs(volshift(mind1,0,-1,0)-mind2),4);
    Syi2(:,:,:,f)=A1-A2;
    A1=mean(abs(volshift(mind1,0,0,1)-mind2),4);
    A2=mean(abs(volshift(mind1,0,0,-1)-mind2),4);
    Szi2(:,:,:,f)=A1-A2;    
    S2(:,:,:,f)=mean(abs(mind2-mind1),4);
end

Sxi = zeros(m,n,o); S=Sxi;Si=Sxi; Szi = Sxi ; Syi=Sxi;Sz=Sxi;Sy=Sxi; Sx=Sxi;

for f=nfs:-1:1
Sx = Sx + w1(:,:,:,f) .* Sx2(:,:,:,f); %w.r.t. img1
Sy = Sy + w1(:,:,:,f).* Sy2(:,:,:,f);
Sz = Sz + w1(:,:,:,f).* Sz2(:,:,:,f);

S = S + w1(:,:,:,f).*S2(:,:,:,f);

Sxi = Sxi + w2(:,:,:,f).* Sxi2(:,:,:,f); % w.r.t. img2
Syi = Syi + w2(:,:,:,f).* Syi2(:,:,:,f);
Szi = Szi + w2(:,:,:,f).* Szi2(:,:,:,f);

Si = Si + w2(:,:,:,f).*S2(:,:,:,f);
end

clear Sx2; clear Sy2; clear Sz2; clear Sxi2; clear Syi2; clear Szi2; clear S2;

Sx(:,[1,n],:)=0;
Sy([1,m],:,:)=0;
Sz(:,:,[1,o])=0;
Sxi(:,[1,n],:)=0;
Syi([1,m],:,:)=0;
Szi(:,:,[1,o])=0;

function vol1shift=volshift(vol1,x,y,z)

x=round(x);
y=round(y);
z=round(z);

[m,n,o,p]=size(vol1);

vol1shift=zeros(size(vol1));

x1s=max(1,x+1);
x2s=min(n,n+x);

y1s=max(1,y+1);
y2s=min(m,m+y);

z1s=max(1,z+1);
z2s=min(o,o+z);

x1=max(1,-x+1);
x2=min(n,n-x);

y1=max(1,-y+1);
y2=min(m,m-y);

z1=max(1,-z+1);
z2=min(o,o-z);

vol1shift(y1:y2,x1:x2,z1:z2,:)=vol1(y1s:y2s,x1s:x2s,z1s:z2s,:);
