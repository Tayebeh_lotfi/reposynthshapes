function [u2,v2]=fastInverse2d(u1,v1)
%following the approach of to Chen et al. (2008)
[m,n]=size(u1);
[x,y]=meshgrid(1:n,1:m);
x=single(x);
y=single(y);

% error 'mex not implemented' 

u2=zeros(size(u1),'single');
v2=zeros(size(v1),'single');

for i=1:10
    xu=min(max(x+u2,1),n);
    yv=min(max(y+v2,1),m);    
    %zw=min(max(z+w2,1),o);
    
       
    u2=-bilinearSingle(single(u1),xu,yv);
    v2=-bilinearSingle(single(v1),xu,yv);
    
end

