function [Unc_ExpEr, Unc_ExpErMap] = Calc_Uncertainty_Err_Dist(X, dispvec, dx, dy)    
    start = 4;
    isz = sz(X);
    iszz = sz(dx);
    X = reshape( X, prod(iszz), [] );  
    [X1, IX] = sort(X,2);
    index = sz(IX,2)-start:sz(IX,2);
%     IX(:,index);
    XX  = X1(:,index);

%     Disp = dispvec(IX(:,index),:);
%     Unc_ExpEr = sum( XX .* ( X * pdist2(dispvec, Disp) ), 2 ); 
    
    for pix = 1:sz(X,1),
        Disp = dispvec(IX(pix,index),:);
        Unc_ExpEr(pix) = sum( XX(pix,:) .* ( X(pix,:) * pdist2(dispvec, Disp ) ), 2 );
    end
    
    
%     Unc_ExpEr = sum( X .* ( X * pdist2(dispvec, dispvec) ), 2 ); 
    Disp = [dx(:) dy(:)];
    Unc_ExpErMap = sum( X .* pdist2(Disp, dispvec), 2);

    Unc_ExpEr = reshape( Unc_ExpEr, isz(1), [] ); 
    Unc_ExpErMap = reshape( Unc_ExpErMap, isz(1), [] ); 
end