function res_img = local_edge_count(x)
    edge_img = edge( x, 'canny', 0.7);
    res_img = sum(edge_img(:));
end