function p = gaussian_kernel(dispvec, X, pbs, pix, opt)
    nlabs = size(dispvec,1);
    Siginv = pinv(2*opt.Sig);
%     [x1 x2] = ind2sub(opt.isz, pix);
%     x(:,1) = repmat(x1, [nlabs 1]) + dispvec(:,1);
%     x(:,2) = repmat(x2, [nlabs 1]) + dispvec(:,2);
    for l = 1:nlabs,
        mu = dispvec(l,:);
      component(l) = 1 / sqrt(2*pi*det(2*opt.Sig)) .* exp( -(X - mu) * ...
          Siginv * (X - mu)' ) .* pbs(pix,l);
    end
    p = -pbs(pix,opt.l) .* log(sum(component));
%     dispmat = permute(dispvec,[3 2 1]);
%     scale = 1 ./ sqrt( 2*pi .* det(opt.Sig) );
%     Siginv = pinv(opt.Sig);
%     exp_compon = exp( -(dispmat - repmat(opt.mu, [1 1 nlabs])) * repmat(Siginv, [1 1 nlabs])...
%         * (dispmat - repmat(opt.mu, [1 1 nlabs]))' );
%     p = sum ( repmat(scale, [1 1 nlabs]) .* exp_compon, 3);
end