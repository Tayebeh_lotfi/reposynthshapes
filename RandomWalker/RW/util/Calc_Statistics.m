function Smooth_Val = Calc_Statistics(str)

%% loading the calculated data
%% initialization
    path1 = 'Y:/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/';
%     path2 = 'H:/students/tlotfima/RWRegistration_Final2D/results/forty_images_smooth_1seed/'; 
    path3 = 'Y:/students_less/tlotfima/RWRegistration_Final2D/results/forty_images/';  
%     path3 = 'H:/students_less/tlotfima/RWRegistration_Final2D/results/forty_images_4_seeds/'; 
    path2 = 'Y:/students_less/tlotfima/RWRegistration_Final2D/results/forty_images_regions/';
    file_name = {'AllErr' 'Label_set' 'Displace_field' 'Time' 'Uncertainty' 'Landmarks' };
    trial_num = 1;
    strt_frst = 1; end_frst = 5;
    img_set_sz = 5;
%     str = '_small';
%     strt_scd = 6; end_scd = 10; 
%     path1 = '/home/ghassan/students_less/tlotfima/RWRegistration_Final2D/data/Precomputed/';
%     path2 = '/home/ghassan/students/tlotfima/RWRegistration_Final2D/results/forty_images_smooth_1seed/';
%% creating data
    [src3D tar3D] = Concatenate(strt_frst, end_frst, path1, path2, file_name, trial_num, img_set_sz, str);
 %% reading data
    load(num2str([path1 'trial' num2str(trial_num) str '_corner.mat']));
%     load(num2str([path1 'trial' num2str(trial_num) '_simwarp' str '_corner.mat']));
   
%     load(num2str([path1 'trial' num2str(trial_num) '_large_warp_corner.mat']));

    for i = 1:6,
      load (num2str([path3 file_name{i} num2str(trial_num) str '_corner_all.mat']));         
    end
 %% Calculating the Displacement Field            
    max_iter = 5;
%     iter = 5;
    IMG_MAX_NUM = size(Label_set.AL,3);
    Uncertainty.AL_Base(:,:,3:max_iter,:) = repmat(Uncertainty.AL_Base(:,:,2,:),[1 1 max_iter-2 1]);
    Final_Disp_Field = Calc_Displace_Field(  src3D, Displace_field, Label_set, trial_num, max_iter, IMG_MAX_NUM );
%     figure, quiverr( imre(Final_Disp_Field(iter).Rand(:,:,:,1), 3, 11), imre(Final_Disp_Field(iter).Rand(:,:,:,2), 3, 11), [1 1]*5,0, 'r' );
%     figure, quiverr( imre(Final_Disp_Field(iter).AL(:,:,:,1), 3, 11), imre(Final_Disp_Field(iter).AL(:,:,:,2), 3, 11), [1 1]*5,0, 'r' ); 
%     figure, quiverr( imre(Final_Disp_Field(iter).SL(:,:,:,1), 3, 11), imre(Final_Disp_Field(iter).SL(:,:,:,2), 3, 11), [1 1]*5,0, 'r' ); 
%     figure, quiverr( imre(Final_Disp_Field(2).AL_Base(:,:,:,1), 3, 11), imre(Final_Disp_Field(2).AL_Base(:,:,:,2), 3, 11), [1 1]*5,0, 'r' ); 

%% Calculating Det of Jaccobian
%     MAX_IMG_NUM = end_scd;%40;
    for img_num = 1:IMG_MAX_NUM,
        for iter = 1: max_iter,
            T = Final_Disp_Field(img_num,iter).AL;
            Err(iter,img_num).AL(:,:,1) = Final_Disp_Field(img_num, iter).AL(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).AL(:,:,2) = Final_Disp_Field(img_num, iter).AL(:,:,2)-Ty3D(:,:,img_num);
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).AL = gxx .* gxy - gyx .* gyy;
            
            T = Final_Disp_Field(img_num,iter).SL_All;
            Err(iter,img_num).SL_All(:,:,1) = Final_Disp_Field(img_num, iter).SL_All(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).SL_All(:,:,2) = Final_Disp_Field(img_num, iter).SL_All(:,:,2)-Ty3D(:,:,img_num);
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).SL_All = gxx .* gxy - gyx .* gyy;

            T = Final_Disp_Field(img_num,iter).SL100;
            Err(iter,img_num).SL100(:,:,1) = Final_Disp_Field(img_num, iter).SL100(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).SL100(:,:,2) = Final_Disp_Field(img_num, iter).SL100(:,:,2)-Ty3D(:,:,img_num);
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).SL100 = gxx .* gxy - gyx .* gyy;

            T = Final_Disp_Field(img_num,iter).SL_Rand;
            Err(iter,img_num).SL_Rand(:,:,1) = Final_Disp_Field(img_num, iter).SL_Rand(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).SL_Rand(:,:,2) = Final_Disp_Field(img_num, iter).SL_Rand(:,:,2)-Ty3D(:,:,img_num);            
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).SL_Rand = gxx .* gxy - gyx .* gyy;

            T = Final_Disp_Field(img_num,iter).AL_Base;
            Err(iter,img_num).AL_Base(:,:,1) = Final_Disp_Field(img_num, iter).AL_Base(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).AL_Base(:,:,2) = Final_Disp_Field(img_num, iter).AL_Base(:,:,2)-Ty3D(:,:,img_num);
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).AL_Base = gxx .* gxy - gyx .* gyy;
            
            T = Final_Disp_Field(img_num,iter).Rand;
            Err(iter,img_num).Rand(:,:,1) = Final_Disp_Field(img_num, iter).Rand(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).Rand(:,:,2) = Final_Disp_Field(img_num, iter).Rand(:,:,2)-Ty3D(:,:,img_num);
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).Rand = gxx .* gxy - gyx .* gyy;  
            
            T = Final_Disp_Field(img_num,iter).RandG;
            Err(iter,img_num).RandG(:,:,1) = Final_Disp_Field(img_num, iter).RandG(:,:,1)-Tx3D(:,:,img_num);
            Err(iter,img_num).RandG(:,:,2) = Final_Disp_Field(img_num, iter).RandG(:,:,2)-Ty3D(:,:,img_num);            
            [gxx gxy] = gradient(T(:,:,1));
            [gyx gyy] = gradient(T(:,:,2));
            Smooth_Val(:,:,img_num,iter).RandG = gxx .* gxy - gyx .* gyy;
        end
    end
        
        %% Calculating Warped image set
        Warped_set = Calculate_Warped_set(src3D, Label_set, Displace_field, IMG_MAX_NUM, max_iter);
        Warp_Err = Calculate_Warping_Err(Err, IMG_MAX_NUM, max_iter);
        
        my_tar3D = tar3D(:,:,1:IMG_MAX_NUM); my_src3D = src3D(:,:,1:IMG_MAX_NUM);
        clear tar3D, clear src3D; tar3D = my_tar3D; src3D = my_src3D; clear my_tar3D, clear my_src3D;
        my_Tx3D = Tx3D(:,:,1:IMG_MAX_NUM); my_Ty3D = Ty3D(:,:,1:IMG_MAX_NUM);
        clear Tx3D; clear Ty3D; Tx3D = my_Tx3D; Ty3D = my_Ty3D; clear my_Tx3D; clear my_Ty3D;


        Show_Stat( mean_diff, mean_Uncert, mean_Jacob, Corr_Mat, AllErr );
%% Calculate Block-MI
    close all;  
    iter = 10;
    Img_num = 1;
      for img_num = 1:IMG_MAX_NUM,  
        dockf;
        number = 1;
        src = src3D(:,:,img_num);
        Unc = Uncertainty.SL_Rand(:, :, iter, img_num); %Unc = Uncertainty.SL100(:, :, iter, img_num);
        Cert = max(Unc(:)) - Unc + (min(Unc(:)));
        mask = src > 0.1;
        idata(:,:,1) = Cert.*mask; %Unc.*mask;
        idata(:,:,2) = Warp_Err(iter, img_num).SL_Rand.*mask; %idata(:,:,2) = Warp_Err(iter, img_num).SL100.*mask;
        idata1 = idata(:,:,1);
        idata2 = idata(:,:,2);
        b =9;%[23 23 8];%10; [ b b b ]
        cc = blockproc(idata, [b b], @local_corr_fun);%, 'Useparallel', 1, 'PadPartialBlocks',1);%);%
        local_correlation = cc(1:2:end,2:2:end);%,1:2:end);

        subplot(4, Img_num, number), imagesc(idata(:,:,1)), title('Certainty');
        subplot(4, Img_num, number + Img_num), imagesc(idata(:,:,2)),title('Error');
        subplot(4, Img_num, number + 2*Img_num),imagesc(local_correlation), title('Local Correlation');
        subplot(4, Img_num, number + 3*Img_num), plot(idata1(:), idata2(:), 'r*'), title('Global Correlation');
        number = number + 1;
        Mut_Info(img_num) = mutualinfo( idata1, idata2);
            %colormap(gray),xlabel('Certainty'), ylabel('Error'); title(['Local correlation At iteration' num2str(iter)]);
      end
      
      save res_trial.mat Tx3D Ty3D src3D tar3D Uncertainty Smooth_Val Warped_set Final_Disp_Field
end
%     J(1,1) = [gxx(1,1) gxy(1,1); gyx(1,1) gyy(1,1)];
% I1(isnan(I1)) = 0;
         %% Calculating Correlation of diff & Uncert
%         for img_num = 1:IMG_MAX_NUM,
%             for iter = 1: max_iter,
%                 Corr_Mat.AL(:,:,img_num,iter) = corrcoef(Diff_set.AL(:,:,img_num,iter),Uncertainty.AL(:,:,iter,img_num));
%                 Corr_Mat.SL_All(:,:,img_num,iter) = corrcoef(Diff_set.SL_All(:,:,img_num,iter),Uncertainty.SL_All(:,:,iter,img_num));
%                 Corr_Mat.SL100(:,:,img_num,iter) = corrcoef(Diff_set.SL100(:,:,img_num,iter),Uncertainty.SL100(:,:,iter,img_num));
%                 Corr_Mat.SL200(:,:,img_num,iter) = corrcoef(Diff_set.SL200(:,:,img_num,iter),Uncertainty.SL200(:,:,iter,img_num));                
%                 Corr_Mat.AL_Base(:,:,img_num,iter) = corrcoef(Diff_set.AL_Base(:,:,img_num,iter),Uncertainty.AL_Base(:,:,iter,img_num));
%                 Corr_Mat.Rand(:,:,img_num,iter) = corrcoef(Diff_set.Rand(:,:,img_num,iter),Uncertainty.Rand(:,:,iter,img_num));
%                 Corr_Mat.RandG(:,:,img_num,iter) = corrcoef(Diff_set.RandG(:,:,img_num,iter),Uncertainty.RandG(:,:,iter,img_num));
%             end
%         end
%         for i = 1 : max_iter,
%             for j = 1 : IMG_MAX_NUM,
%             Diff = Diff_set.AL(:,:,j,i); Uncert = Uncertainty.AL(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).AL; 
%             mean_diff.AL(j,i) = mean(Diff(:)); mean_Uncert.AL(j,i) = mean(Uncert(:)); mean_Jacob.AL(j,i) = mean(Jacob(:));
%             
%             Diff = Diff_set.Rand(:,:,j,i); Uncert = Uncertainty.Rand(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).Rand; 
%             mean_diff.Rand(j,i) = mean(Diff(:)); mean_Uncert.Rand(j,i) = mean(Uncert(:)); mean_Jacob.Rand(j,i) = mean(Jacob(:));
%  
%             Diff = Diff_set.RandG(:,:,j,i); Uncert = Uncertainty.RandG(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).RandG; 
%             mean_diff.RandG(j,i) = mean(Diff(:)); mean_Uncert.RandG(j,i) = mean(Uncert(:)); mean_Jacob.RandG(j,i) = mean(Jacob(:));
% 
%             Diff = Diff_set.SL_All(:,:,j,i); Uncert = Uncertainty.SL_All(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).SL_All; 
%             mean_diff.SL_All(j,i) = mean(Diff(:)); mean_Uncert.SL_All(j,i) = mean(Uncert(:)); mean_Jacob.SL_All(j,i) = mean(Jacob(:));
% 
%             Diff = Diff_set.SL100(:,:,j,i); Uncert = Uncertainty.SL100(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).SL100; 
%             mean_diff.SL100(j,i) = mean(Diff(:)); mean_Uncert.SL100(j,i) = mean(Uncert(:)); mean_Jacob.SL100(j,i) = mean(Jacob(:));
% 
%             Diff = Diff_set.SL200(:,:,j,i); Uncert = Uncertainty.SL200(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).SL200; 
%             mean_diff.SL200(j,i) = mean(Diff(:)); mean_Uncert.SL200(j,i) = mean(Uncert(:)); mean_Jacob.SL200(j,i) = mean(Jacob(:));
% 
%             Diff = Diff_set.AL_Base(:,:,j,i); Uncert = Uncertainty.AL_Base(:,:,i,j); Jacob = Smooth_Val(:,:,j,i).AL_Base; 
%             mean_diff.AL_Base(j,i) = mean(Diff(:)); mean_Uncert.AL_Base(j,i) = mean(Uncert(:)); mean_Jacob.AL_Base(j,i) = mean(Jacob(:));
%             end
%         end 
        %% Calculating the Diff of Warped image set and target set
%         Diff_set = Calc_Similarity (Warped_set, tar3D,max_iter); 
%         Diff_set.AL(isnan(Diff_set.AL)) = 0;
%         Diff_set.SL_All(isnan(Diff_set.SL_All)) = 0;
%         Diff_set.SL100(isnan(Diff_set.SL100)) = 0;
%         Diff_set.SL200(isnan(Diff_set.SL200)) = 0;        
%         Diff_set.AL_Base(isnan(Diff_set.AL_Base)) = 0;
%         Diff_set.Rand(isnan(Diff_set.Rand)) = 0;
%         Diff_set.RandG(isnan(Diff_set.RandG)) = 0;