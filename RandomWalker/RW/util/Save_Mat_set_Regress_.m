% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set_Regress_( AllErr, AllErr_Less, Label_set, Label_set_Less, Displace_field, Displace_field_Less, ...,
    Uncertainty, Uncertainty_Less, Landmarks, Landmarks_Less, savepath, Max_Iterations, num_seed, Time, ...,
    trial_num, set_num, str, MCase, noise_sigma)

    file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Landmarks' 'AllErr_Less' 'Label_set_Less' ...
        'Displace_field_Less' 'Uncertainty_Less' 'Landmarks_Less'};    
    for i = 1:10,%6
        if(MCase == 1)
            if (i>5)
                save (num2str([savepath 'AL_SL/Regression/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_without_unc' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
            else
                save (num2str([savepath 'AL_SL/Regression/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_with_unc' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
            end
            save (num2str([savepath 'AL_SL/Regression/Time' num2str(set_num) str '_trial' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']), 'Time'); 
        else
            if (i>5)
                save (num2str([savepath 'AL_SL/Regression/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_uncertainties' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
            else
                save (num2str([savepath 'AL_SL/Regression/' file_name{i} num2str(set_num) str '_trial' num2str(trial_num) '_regress_image_features' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
            end
            save (num2str([savepath 'AL_SL/Regression/Time' num2str(set_num) str '_trial' num2str(trial_num) '_noise' num2str(noise_sigma*100) '.mat']), 'Time');             
        end
    end 
end