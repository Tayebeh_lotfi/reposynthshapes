% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set_Regress_test_seg( AllErr, Label_set, Displace_field, Uncertainty, Landmarks, savepath, ...,
    Max_Iterations, num_seed, Time, set_num, str, img_num2, noise_sigma)

    file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Landmarks' 'Time'};    
    for i = 1:6,%6
        save (num2str([savepath 'AL_SL/Regression/ExpEr/' file_name{i} num2str(set_num) str '_to_img' num2str(img_num2) '_regress_ExpErU' '_noise' num2str(noise_sigma*100) '.mat']), file_name{i}); 
    end
end