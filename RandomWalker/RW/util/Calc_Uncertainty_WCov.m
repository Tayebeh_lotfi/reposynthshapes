function [Wcov SGauss] = Calc_Uncertainty_WCov(pbs, dispvec, isz, mask, epsilon_, data_mode)
%% Loading, defining, and Calculaing variables
%     load ../utils/func_variables;
%     pbs_sprd = reshape(pbs_sprd, prod(isz), []);
    pbs = reshape(pbs, prod(isz), []);
    nlabs = size(pbs,2);
    Wcov = zeros(isz); SGauss = Wcov;   
    S = ( mask > 0 );
    inds = find(S==1);   
    for pix = 1:len(inds),   

        disp = sqz(pbs(inds(pix), :));
        if(data_mode == 2)
            W_mu = sum([disp(:) .* dispvec(:,1), disp(:) .* dispvec(:,2)]);
            x_pts = dispvec(:,1) - W_mu(1);
            y_pts = dispvec(:,2) - W_mu(2);
            Sigma = [x_pts(:)'; y_pts(:)'] * diag(disp(:)) * [x_pts(:), y_pts(:)];
        else         
            W_mu = sum([disp(:) .* dispvec(:,1), disp(:) .* dispvec(:,2) disp(:) .* dispvec(:,3)]);
            x_pts = dispvec(:,1) - W_mu(1);
            y_pts = dispvec(:,2) - W_mu(2);
            z_pts = dispvec(:,3) - W_mu(3);
            Sigma = [x_pts(:)'; y_pts(:)'; z_pts(:)' ] * diag(disp(:)) * [x_pts(:), y_pts(:), z_pts(:)];
        end
        Wcov(inds(pix)) = trace(Sigma); %det(covw(:,:,pix)); %det(temp);
        SGauss(inds(pix)) = 1/2 * log( (2*pi*exp(1)) .^ size(dispvec,2) .* (Wcov(inds(pix))+epsilon_) );                        
    end
end
%         dispveco.x = repmat(dispvec(:,1)',prod(isz),1);
%         dispveco.y = repmat(dispvec(:,2)',prod(isz),1);
%         dispvecw.x = dispveco.x .* pbs;
%         dispvecw.y = dispveco.y .* pbs;
%         meanw.x = sum(dispvecw.x,2);                % weighted vectors' mean
%         meanw.y = sum(dispvecw.y,2);                % weighted vectors' mean
%         data(1:nlabs,1,1:prod(isz)) = dispveco.x';  
%         data(1:nlabs,2,1:prod(isz)) = dispveco.y';
%         if(data_mode == 3)
%             dispveco.z = repmat(dispvec(:,3)',prod(isz),1);
%             dispvecw.z = dispveco.z .* pbs;
%             meanw.z = sum(dispvecw.z,2);                % weighted vectors' mean
%             data(1:nlabs,3,1:prod(isz)) = dispveco.z';
%             datam(1:nlabs,3,1:prod(isz)) = repmat(meanw.z',nlabs,1);
%         end   
%         datam(1:nlabs,1,1:prod(isz)) = repmat(meanw.x',nlabs,1);
%         datam(1:nlabs,2,1:prod(isz)) = repmat(meanw.y',nlabs,1);   
% %% Weighted Covariance Calculation
%         if(data_mode == 3)
%             covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),3,1) .* (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * (data(:,:,inds(pix)) - datam(:,:,inds(pix))) );% ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );%temp;
%         else
%             covw(:,:,pix) = ( ( repmat(pbs(inds(pix),:),2,1) .* (data(:,:,inds(pix)) - datam(:,:,inds(pix)))' ) * (data(:,:,inds(pix)) - datam(:,:,inds(pix))) );% ./ ( 1 - sum(pbs(inds(pix),:) .^ 2) );%temp;
%         end
%         prob = sqz(pbs(inds(pix),:));
%         [value_ index_] = sort(prob,'descend');
% %         covw_pozzi(:,:,pix) = weightedcov(dispvec(index_,:), value_);
%         Wcov(inds(pix)) = det(covw(:,:,pix)); %det(temp);
%         SGauss(inds(pix)) = 1/2 * log( (2*pi*exp(1)) .^ size(dispvec,2) .* (Wcov(inds(pix))+epsilon_) );