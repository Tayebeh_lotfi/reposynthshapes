% function Save_Mat_set2( AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Solution_field, Time, trial_num, Landmarks)
function Save_Mat_set( SegErr, AllErr, savepath, Max_Iterations, num_seed, Label_set, Displace_field, Uncertainty, Time, trial_num, set_num, str, Landmarks, Noise_Level, MCase)
%     file_name = {'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Solution_field' 'Time' 'Landmarks'};
    file_name = {'SegErr' 'AllErr' 'Label_set' 'Displace_field' 'Uncertainty' 'Time' 'Landmarks'};    
    for i = 1:7,%6
        if(MCase == 1)
            save (num2str([savepath 'AL_SL/' file_name{i} num2str(set_num) str '_noise' num2str(Noise_Level-1) '_trial' num2str(trial_num) '_bsp' '.mat']), file_name{i}); 
        else
            save (num2str([savepath 'AL_SL/' file_name{i} num2str(set_num) str '_noise' num2str(Noise_Level-1) '_trial' num2str(trial_num) '_diff' '.mat']), file_name{i}); 
        end
%       save (num2str([savepath file_name{i} num2str(set_num) '_large_corner_set' num2str(trial_num) '.mat']), file_name{i});       
%       save (num2str([savepath file_name{i} num2str(trial_num) '_new2_noise5.mat']), file_name{i}); 
%       save (num2str([savepath file_name{i} num2str(trial_num) '_new2_noise10.mat']), file_name{i}); 

    end          
end