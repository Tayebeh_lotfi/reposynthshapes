function [err_pbs err_pbs_sprd] = Calculate_Prob_Error(dispvec, pbs, pbs_sprd, invTx2D, invTy2D, isz)
    
    pbs = reshape(pbs, isz, []);
    pbs_sprd = reshape(pbs_sprd, isz, []);
    GT = [invTx2D(:) invTy2D(:)];
    [~, D] = knnsearch(dispvec, GT, 'k', size(dispvec,1) );
    bin_err = 100; mn = mnx(D); dim = 2;
    err_pbs = zeros(isz,bin_err);
    err_pbs_sprd = zeros(isz,bin_err);  
    bin_cnt = linspace(mn(1), mn(2),bin_err);

    for bin_counter = 1:len(bin_cnt)-1,
        [~, ixx] = histc(D, bin_cnt(bin_counter:bin_counter+1), dim);
        err_pbs(:,bin_counter) = sum((pbs .* ixx), 2);  
        err_pbs_sprd(:,bin_counter) = sum((pbs_sprd .* ixx), 2);
    end
end

