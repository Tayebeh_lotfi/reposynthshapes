function spherical_distribution_visualization(pbs, dispvec, point)    
    dockf;
    TRI = delaunay(double(dispvec(:,1)), double(dispvec(:,2)), double(dispvec(:,3)));
    c = pbs(point(1),point(2),point(3),TRI(:,1)) + ...
        pbs(point(1),point(2),point(3),TRI(:,2)) + ...
        pbs(point(1),point(2),point(3),TRI(:,3)) + ...
        pbs(point(1),point(2),point(3),TRI(:,4));
    c = c / 4;
    trisurf(TRI, dispvec(:,1), dispvec(:,2), dispvec(:,3), c, 'EdgeColor', 'none');
    colorbar;
    axis equal;
end
