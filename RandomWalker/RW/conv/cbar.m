function h= cbar( arg )
if nargin==0
h = colorbar('location', 'EastOutside');
else
    h = colorbar('location', arg );
end