   [mx my] = ndgrid( 1:size(dx, 1), 1:size(dx, 2), 1:size(dx, 3) );
    mask1 = ( src > 0.1);
    mask2 = ( src_segment > 0);
    se = strel('disk',2);
    mask2 = imdilate(mask2,se);
    mask = mask1 .* mask2;
    certainty = max(uncertainty(:)) - uncertainty + min(uncertainty(:));
    ops.alpha = 0.1; %0.3
    length = size(slands,2);
    cert_thres = quantile(certainty(mask>0),.95);      
%     certainty(mask>0) = min(certainty(:));
    certainty = certainty .* mask;
    S=( certainty > cert_thres);    
%         dockf;hist(certainty(mask>0)), hold on, plot(cert_thres, 1:10000, 'g.-')
    candidate_inds = find(S==1);
    ops.num_seed = min(ops.num_seed, size(candidate_inds,1));
    candidates(1,:) = mx( candidate_inds )';
    candidates(2,:) = my( candidate_inds )';
    
    S1=( certainty > mean(certainty(:)));    
%         dockf;hist(certainty(mask>0)), hold on, plot(cert_thres, 1:10000, 'g.-')
    S2 = find(S1==1);
    S3 = (certainty(S2) < cert_thres);
    cand_inds = find(S3==1);
    cands(1,:) = mx( cand_inds )';
    cands(2,:) = my( cand_inds )';
    
   dockf, imagesc(src), hold on, plot(candidates(2,:), candidates(1,:), 'r*') 
   dockf, imagesc(src), hold on, plot(cands(2,:), cands(1,:), 'r*')
   hold on, imagesc(mask)
   