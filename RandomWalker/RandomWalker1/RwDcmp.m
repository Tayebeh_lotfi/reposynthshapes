clc, close all, clear,
addpath(genpath('../RW'));
addpath(genpath('../TreeBagDcmp'));
%% generate data
opt.FixA = true; opt.Src = 2; opt.thresh = .25; opt.nVarIntens = -20; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 75; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 20; opt.width = 20; opt.mag = 10; 
opt.TxrPth = '../../ImagesTextures/'; 
opt.Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.03.tiff'; % 1.1.03
opt.Txtf3 = '1.1.10.tiff';
[A_Pri1, ~, X, Y, HasOverlap] = Create_Samples5(opt);
while(HasOverlap ==0)
    [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples5(opt);
end

img=im2double(Y);
% [seeds_,boundaryseg,boundaryval] = prepare2_val(img);
[seeds_,boundaryseg,boundaryval] = prepare4_val(img);
seedsOrg = seeds_; boundaryOrg = boundaryseg;
[Xc, Yc, Zc]=size(img);
%Build graph
[points, edges]=lattice(Xc,Yc,1);
imgVals=img(:); 
% mnd = 1 - double(reshape(MIND_descriptor2d(img,4,1.2),Xc*Yc,[]));
% imgVals = mnd(:,1:20);
beta = 90; 
weights=makeweights(edges,imgVals,beta);
L=laplacian(edges,weights);
[L12,L13,L23,~] = makeWeightsSrcs(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img);
alpha = 1e-1; LL = [L,alpha*L12,alpha*L13;alpha*L12,L,alpha*L23;alpha*L13,alpha*L23,L];
seeds_ = seedsOrg; boundaryseg = boundaryOrg;
probabilities_seg=dirichletboundary(LL,seeds_(:),boundaryseg);
probS = probabilities_seg;
mxv = max(probS,[],2);
mnv = min(probS,[],2);
probS = (probS - repmat(mnv,[1,sz(probS,2)])) ...
    ./ (repmat(mxv,[1,sz(probS,2)]) - repmat(mnv,[1,sz(probS,2)]));
probS = probS ./ repmat(sum(probS,2),[1,sz(probS,2)]);
lables = sz(probS,2);
%%
iter = 0;
ed = Xc*Yc;
% [val, msk, ovlp1_, ovlp2_] = calc_two_max(probS, img); % A_Pri1);
[val, msk] = calc_max(probS, img); 
% [val_, msk] = max(probS,[],2);
msk1 = reshape(msk(1:ed),[Xc,Yc]);
msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);
msk1(msk1==1) = 0;
msk1(msk1~=0) = 1;
msk2(msk2==1) = 0;
msk2(msk2~=0) = 1;
msk3(msk3==1) = 0;
msk3(msk3~=0) = 1;
grdmsk1 = X(:,:,1)>0; grdmsk2 = X(:,:,2)>0; grdmsk3 = X(:,:,3)>0; 
seg_acc(iter+1) = 100 - (sz(find(grdmsk1~=msk1),1) + sz(find(grdmsk2~=msk2),1) + sz(find(grdmsk3~=msk3),1)) ./ (3*Xc*Yc)*100;
ovlp = (sz(find(grdmsk1&grdmsk2),1)+sz(find(grdmsk1&grdmsk3),1)+...
    sz(find(grdmsk2&grdmsk3),1)-sz(find(grdmsk1&grdmsk2&grdmsk3),1));

h = figure(1);
subplot 241, imshow(msk1), title([sprintf('First Row: Seg Results, Acc: %.2f', seg_acc(iter+1)), '%']) 
subplot 242, imshow(msk2), 
subplot 243, imshow(msk3), 
subplot 244, imshow(msk1+msk2+msk3,[]), 
subplot 245, imshow(grdmsk1), title('Second Row: Ground Truth') 
subplot 246, imshow(grdmsk2),
subplot 247, imshow(grdmsk3),
subplot 248, imshow(grdmsk1+grdmsk2+grdmsk3,[]),
%%
