function [val, msk] = calc_max3Src(X,img) % , ovlp1,ovlp2
    ed = prod(sz(img)); % floor( sz(X,1) ./ 2);
    [val, msk] = max(X,[],2);

    prob1 = X(1:ed,:);
    prob2 = X(ed+1:2*ed,:);
    prob3 = X(2*ed+1:end,:);
    [seg1,cent1] = kmeans(prob1(:,2),6,'Replicates',5,'MaxIter',1000);
    [ii1,jj1] = sort(cent1);
    [seg2,cent2] = kmeans(prob2(:,3),6,'Replicates',5,'MaxIter',1000);
    [ii2,jj2] = sort(cent2);
    [seg3,cent3] = kmeans(prob3(:,4),6,'Replicates',5,'MaxIter',1000);
    [ii3,jj3] = sort(cent3);
    if(ii1(2)<.15)
        if(ii1(3)<.1)
            idx1 = find(seg1==jj1(4) | seg1==jj1(5) | seg1==jj1(6));
        else
            idx1 = find(seg1==jj1(3) | seg1==jj1(4) | seg1==jj1(5) | seg1==jj1(6));
        end
    else
        idx1 = find(seg1~=jj1(1));
    end
    if(ii2(2)<.15)
        if(ii2(3)<.1)
            idx2 = find(seg2==jj2(4) | seg2==jj2(5) | seg2==jj2(6));
        else
            idx2 = find(seg2==jj2(3) | seg2==jj2(4) | seg2==jj2(5) | seg2==jj2(6));
        end
    else
        idx2 = find(seg2~=jj2(1));
    end
    if(ii3(2)<.15)
        if(ii3(3)<.15)
            idx3 = find(seg3==jj3(4) | seg3==jj3(5) | seg3==jj3(6));
        else
            idx3 = find(seg3==jj3(3) | seg3==jj3(4) | seg3==jj3(5) | seg3==jj3(6));
        end
    else
        idx3 = find(seg3~=jj3(1));
    end
    msk(idx1) = 2;
    msk(ed + idx2) = 3;
    msk(2*ed + idx3) = 4;
    val(idx1) = X(idx1,2); val(ed+idx2) = X(ed+idx2,3); val(2*ed+idx3) = X(2*ed+idx3,4);
end