function [Lambda,vec] = CalcProb(ops)
    img = ops.img;
    idx = find(ops.msk1&ops.msk2);
    [Xc,Yc] = size(img);
    if ops.case == 1 % values
        vec = double(ops.vect);
        val = img(idx);
        v = vec(:,end-2);
        tmp1 = permute(repmat(v,[1,len(idx)]),[2,1]);
        tmp2 = repmat(val,[1,len(vec)]);
        Lambda = exp( -ops.beta*((tmp1-tmp2).^2 ));
%         Lambda = 1-(tmp1-tmp2).^2;
    else % Mind operator of the values
        vec = double(ops.vect);
        mndimg = reshape(double(MIND_descriptor2d(img,ops.rad,ops.sigma)),Xc*Yc,[]);
        val = mndimg(idx,:);
        val(:,end) = img(idx); 
%         if(~ops.LowMem)
%             val(:,1) = img(:);
            tmp1 = permute(repmat(vec,[1,1,len(idx)]),[3,2,1]);
            tmp2 = repmat(val,[1,1,len(vec)]);
            Lambda = exp( -ops.beta*squeeze( sum( (tmp1(:,1:end-3,:)-tmp2).^2, 2) ) );
%% Low memory needed:
%         else
%             value(:,1) = img(:);
%             value(:,2) = mean(val,2);
%             vector(1:3,:) = vec(1:3,:); vector(4,:) = mean(vec(4:end,:),1);
%     %         vector = vector';
%             tmp1 = permute(repmat(vector(3:4,:),[1,1,Rw*Col]),[3,1,2]);
%             tmp2 = repmat(value,[1,1,length(vec)]);
%             Lambda = exp( -ops.beta*squeeze( sum( (tmp1-tmp2).^2, 2) ) );
% %         Lambda = 1-squeeze( sum( (tmp1(:,4:end,:)-tmp2).^2, 2) );
%         end
    end
end