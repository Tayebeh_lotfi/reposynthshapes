function [Lbl,Txt] = makeWeightsSrcs_(TxrPth,Txtf1,Txtf2,img,A) % msk1,msk2,A)    
    I1 = imread([TxrPth,Txtf1]); % 1.5.03, 1.1.01
    I2 = imread([TxrPth,Txtf2]); 
    x11 = fix((sz(I1,1)-sz(img,1))*rand(1,1))+1;
    x12 = fix((sz(I1,2)-sz(img,2))*rand(1,1))+1;
    x21 = fix((sz(I2,1)-sz(img,1))*rand(1,1))+1;
    x22 = fix((sz(I2,2)-sz(img,2))*rand(1,1))+1;
    Txt(:,:,1) = I1(x11:x11+sz(img,1)-1,x12:x12+sz(img,2)-1);
    Txt(:,:,2) = I2(x21:x21+sz(img,1)-1,x22:x22+sz(img,2)-1);
    Txt = im2double(Txt);
    [Xc, Yc, ~]=size(img);
    %Build graph
%     [points, edges]=lattice(Xc,Yc,1);
    T1 = A(1)*Txt(:,:,1); % .* msk1;
    T2 = A(2)*Txt(:,:,2); % .* msk2;
    im = (T1 + T2);
    mnd = 1 - double(reshape(MIND_descriptor2d(im,2,.2),Xc*Yc,[]));
%     idx = find(msk1&msk2);
    LblL = mnd;
    LblL(:,end+1:end+3) = [im(:),T1(:),T2(:)]; % idx
    k = 1000;
    [idx,C] = kmeans(LblL,k);
    Lbl = C';
    save('LabelsKmn.mat', 'Lbl');
end