function [seeds,boundary] = prepareO(img)
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];
    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4); ...
        sYi(5),sXi(5);sYi(6),sXi(6);sYi(7),sXi(7);sYi(8),sXi(8);sYi(9),sXi(9)]; % ]; % 
    seeds = sub2ind(sz(img),slands(:,1),slands(:,2));

    labels = [ones(1,4),repmat(2,[1,3]),repmat(3,[1,3]),repmat(4,[1,3])];
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);
    
    boundary=zeros(length(seeds),number_labels);
    boundary(1:4,:) = repmat([1,0,0,0],[4,1]);
    boundary(5:7,:) = repmat([0,1,0,0],[3,1]);
    boundary(8:10,:) = repmat([0,0,1,0],[3,1]);
    boundary(11:13,:) = repmat([0,0,0,1],[3,1]);
end