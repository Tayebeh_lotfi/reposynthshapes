clc, close all, clear,
addpath(genpath('../RW'));
addpath(genpath('../TreeBagDcmp'));
%% generate data
opt.FixA = true; opt.Src = 2; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 75; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 20; opt.width = 20; opt.mag = 10; 
opt.TxrPth = '../../ImagesTextures/'; 
opt.Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.03.tiff'; % 1.1.03
opt.Txtf3 = '1.1.10.tiff'; % '1.1.10.tiff'
[A_Pri1, ~, X, Y, HasOverlap] = Create_Samples5(opt);
while(HasOverlap ==0)
    [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples5(opt);
end

img=im2double(Y);
% [seeds_,boundaryseg,boundaryval] = prepare2_val(img);
[seeds,boundaryp,seeds_,boundaryo] = prepare4_Cmp3Src(img);
[lndmrki,lndmrkj] = ind2sub(sz(img),seeds_);
seedsOrg = seeds; boundaryOrg = boundaryp;
[Xc, Yc, ~]=size(img);
%Build graph
[points, edges]=lattice(Xc,Yc,1);
imgVals=img(:); 
beta = 90; 
weights=makeweights(edges,imgVals,beta);
L=laplacian(edges,weights);
[L12,L13,L23,~] = makeWeights3Srcs(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img);
alpha = 0; LL = [L,alpha*L12,alpha*L13;alpha*L12,L,alpha*L23;alpha*L13,alpha*L23,L]; % 1e-1
seeds = seedsOrg; boundaryp = boundaryOrg;
tic
probabilities_pro=dirichletboundary(LL,seeds(:),boundaryp);
pt1 = toc;
probS = probabilities_pro;
mxv = max(probS,[],2);
mnv = min(probS,[],2);
probS = (probS - repmat(mnv,[1,sz(probS,2)])) ...
    ./ (repmat(mxv,[1,sz(probS,2)]) - repmat(mnv,[1,sz(probS,2)]));
probS = probS ./ repmat(sum(probS,2),[1,sz(probS,2)]);
%%
seedsOrg_ = seeds_; boundaryOrg_ = boundaryo;
[Xc, Yc, Zc]=size(img);
seeds_ = seedsOrg_; boundaryo = boundaryOrg_;
tic
probabilities_or=dirichletboundary(L,seeds_(:),boundaryo);
pt2 = toc;
probS_ = probabilities_or;
mxv = max(probS_,[],2);
mnv = min(probS_,[],2);
probS_ = (probS_ - repmat(mnv,[1,sz(probS_,2)])) ...
    ./ (repmat(mxv,[1,sz(probS_,2)]) - repmat(mnv,[1,sz(probS_,2)]));
probS_ = probS_ ./ repmat(sum(probS_,2),[1,sz(probS_,2)]);
%%
iter = 0;
ed = Xc*Yc;
% [val, msk] = calc_max3Src(probS, img); 
msk = calc_mask3Src(probS, img);
% [val, msk] = max(probS,[],2);
msk1 = reshape(msk(1:ed),[Xc,Yc]);
msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);
% msk1(msk1==1) = 0;
% msk1(msk1~=0) = 1;
% msk2(msk2==1) = 0;
% msk2(msk2~=0) = 1;
% msk3(msk3==1) = 0;
% msk3(msk3~=0) = 1;
grdmsk1 = X(:,:,1)>0; grdmsk2 = X(:,:,2)>0; grdmsk3 = X(:,:,3)>0; 
seg_acc(iter+1) = 100 - (sz(find(grdmsk1~=msk1),1) + sz(find(grdmsk2~=msk2),1) + sz(find(grdmsk3~=msk3),1)) ./ (3*Xc*Yc)*100;
ovlp = (sz(find(grdmsk1&grdmsk2),1)+sz(find(grdmsk1&grdmsk3),1)+...
    sz(find(grdmsk2&grdmsk3),1)-sz(find(grdmsk1&grdmsk2&grdmsk3),1));

% [msk1_,msk2_,msk3_] = calc_max3Src_RW(probS_,img);
[val_, msk_] = max(probS_,[],2);
msk1_ = reshape(msk_==2,[Xc,Yc]);
msk2_ = reshape(msk_==3,[Xc,Yc]);
msk3_ = reshape(msk_==4,[Xc,Yc]);
seg_acc_(iter+1) = 100 - (sz(find(grdmsk1~=msk1_),1) + sz(find(grdmsk2~=msk2_),1) + sz(find(grdmsk3~=msk3_),1)) ./ (3*Xc*Yc)*100;
%% Showing Results
h = figure(1);
subplot 331, imshow(msk1), title([sprintf('First Row: Proposed Seg Results, Acc: %.2f', seg_acc(iter+1)), '%']) 
subplot 332, imshow(msk2), 
subplot 333, imshow(msk3), 
subplot 334, imshow(msk1_), title([sprintf('Second Row: RW Seg Results, Acc: %.2f', seg_acc_(iter+1)), '%']) 
subplot 335, imshow(msk2_), 
subplot 336, imshow(msk3_), 
subplot (3,4,9), imshow(img), hold on, plot(lndmrkj,lndmrki,'r*'), title('Third Row: Ground Truth with the initial seeds') 
subplot (3,4,10), imshow(grdmsk1),
subplot (3,4,11), imshow(grdmsk2),
subplot (3,4,12), imshow(grdmsk3),

