function msk = calc_mask2Src(X,img) % , ovlp1,ovlp2
    ed = prod(sz(img)); % floor( sz(X,1) ./ 2);
    msk = zeros(sz(X,1),1);

    prob1 = X(1:ed,:);
    prob2 = X(ed+1:2*ed,:);

    thresh =.1;
    idx1 = find(prob1(:,2)> thresh);
    idx2 = find(prob2(:,3)> thresh);
    msk(idx1) = 1;
    msk(ed + idx2) = 1;
end