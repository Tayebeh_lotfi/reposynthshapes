function [seeds_,boundary] = calc_high_cer_pix(prob,img,lbls, seeds_,boundary)
    S = sz(img);
    epsilon = 0; % 1e-10;
    u1 = prob .* log(prob+epsilon);
    u1(isnan(u1)) = 0;
    unc = -1/lbls*sum(u1,2);
    unc1 = unc(1:prod(S));
    unc2 = unc(prod(S)+1:end);

    prob1 = prob(1:prod(S),:);
    prob2 = prob(prod(S)+1:end,:);
    lgh = sz(boundary,1);
    
    mask = img(:) > 0; % msk = [mask;mask];
%     cert_thres = quantile(unc(find(msk>0)),.1);
    cert_thres1 = quantile(unc1(find(mask>0)),.1);
    Uncert1 = unc1 .* mask;
    idx1=find( Uncert1 < cert_thres1 & Uncert1 > 0); % Uncert1 <cert_thresh1;
    [~,lbl1] = max(prob1(idx1,:),[],2);

    cert_thres2 = quantile(unc2(find(mask>0)),.1);
    Uncert2 = unc2 .* mask;
    idx2=find( Uncert2 < cert_thres2 & Uncert2 > 0); % Uncert1 < cert_thres2);    
    [~,lbl2] = max(prob2(idx2,:),[],2);

    id1 = find(lbl1==2);
    id2 = find(lbl2==3);
    seeds_ = [seeds_;idx1(id1);idx2(id2)];
%     seeds_ = [seeds_;idx1;idx2(id2)];
%     for ii = 1:sz(idx1,1)
%         if(lbl1(ii)==2)
%             boundary(lgh+1,:) = [0,1,0];
%             lgh = lgh + 1;
%         else
%             boundary(lgh+1,:) = [1,0,0];
%             lgh = lgh + 1;
%         end
%     end
    boundary(end+1:end+sz(id1,1),:) = repmat([0,1,0],[sz(id1,1),1]);
    boundary(end+1:end+sz(id2,1),:) = repmat([1,0,0],[sz(id2,1),1]);
%     lgh = sz(boundary,1);
    
    seeds_ = [seeds_;prod(S)+idx2(id2);prod(S)+idx1(id1)];
%     seeds_ = [seeds_;prod(S)+idx2;prod(S)+idx1(id1)];
%     for ii = 1:sz(idx2,1)
%         if(lbl2(ii)==3)
%             boundary(lgh+1,:) = [0,0,1];
%             lgh = lgh + 1;
%         else
%             boundary(lgh+1,:) = [1,0,0];
%             lgh = lgh + 1;
%         end
%     end
    boundary(end+1:end+sz(id2,1),:) = repmat([0,0,1],[sz(id2,1),1]);
    boundary(end+1:end+sz(id1,1),:) = repmat([1,0,0],[sz(id1,1),1]);

end