function [seeds,Pboundary,seeds_,Oboundary] = prepareLineCmp3Src(img,numObj)
    figure, imshow(img)
    for ii = 1:numObj+1
        nline = input(['How many lines for object number',num2str(ii),'? ']);
        l = 0;
        sX{ii} = [];
        sY{ii} = [];
        for jj = 1:nline
            hFH = imfreehand();
            % Get the (x,y) coordinates.
            xy = hFH.getPosition;
            sX{ii} = [sX{ii};xy(:,1)];
            sY{ii} = [sY{ii};xy(:,2)];
        end
%         sX{ii} = xy(:,1);
%         sY{ii} = xy(:,2);
    end
    img_ = img(:,:,1);
    slands = [];
%     slands = [1,1;1,sz(img,2);sz(img_,1),1;sz(img_,1),sz(img_,2)];
    for ii = 1:numObj+1
        slands = [slands, [round(sY{ii})';round(sX{ii})']];
    end
    seeds_ = sub2ind(sz(img_),slands(1,:),slands(2,:));
    seeds = [];
    for ii = 1:numObj
        seeds = [seeds,seeds_+(ii-1)*prod(sz(img_))];
    end
%%
    labels = [];
    tNum = len(slands);
    for ii = 1:numObj+1
        sNum = len(sX{ii});
        if(ii>2)
            labels = [labels,ones(1,tNum)];
        end
        labels = [labels,repmat(ii,[1,sNum])]; 
    end
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);
    
    Pboundary=zeros(length(seeds),number_labels);
    for ii = 1:numObj+1
        ptrn(ii,:) = [zeros(1,ii-1),1,zeros(1,numObj-ii+1)];
    end
    tNum = len(slands);
    bNum = 0;
    for ii = 1:numObj+1
        sNum = len(sX{ii});
        if(ii>2)
            Pboundary(bNum+1:tNum+bNum,:) = repmat(ptrn(1,:),[tNum,1]);
            bNum = bNum+tNum;
        end
        Pboundary(bNum+1:sNum+bNum,:) = repmat(ptrn(ii,:),[sNum,1]);
        bNum = bNum+sNum;
    end

%%
    labels_ = [];
    for ii = 1:numObj+1
        sNum = len(sX{ii});
        labels_ = [labels_,repmat(ii,[1,sNum])]; 
    end
    label_adjust_=min(labels_); labels_=labels_-label_adjust_+1; %Adjust labels to be > 0
    labels_record_(labels_)=1;
    labels_present_=find(labels_record_);
    number_labels_=length(labels_present_);

    Oboundary=zeros(length(seeds_),number_labels_);
    bNum = 0;
    for ii = 1:numObj+1
        sNum = len(sX{ii});
        Oboundary(bNum+1:sNum+bNum,:) = repmat(ptrn(ii,:),[sNum,1]);
        bNum = bNum+sNum;
    end
end