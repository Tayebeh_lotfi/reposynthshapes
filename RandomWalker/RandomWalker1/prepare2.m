function [seeds_,boundary] = prepare2(img)
    [sXi, sYi, P] = impixel(img);
    sXi(end)=[]; sYi(end)=[]; P(end,:) = []; % P = P(:,1);

    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2);sYi(1),sXi(1); sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4)]; % ]; % 
    seeds = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds_ = [seeds;seeds+prod(sz(img))];
%     seeds_(13:14) = [];
%     seeds_(11:12) = []; 7:8,11:12

    labels = [ones(1,4),2,2,ones(1,8),3,3]; % four corners, class1, class1, class2, class2, 
%     labels = [ones(1,4),2,2,ones(1,6),3,3]; 
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);

    boundary=zeros(length(seeds_),number_labels);
    boundary(1:4,:) = repmat([1,0,0],[4,1]);
    boundary(5:6,:) = repmat([0,1,0],[2,1]);
    boundary(7:14,:) = repmat([1,0,0],[8,1]);
    boundary(15:16,:) = repmat([0,0,1],[2,1]);
%     boundary(7:12,:) = repmat([1,0,0],[6,1]);
%     boundary(13:14,:) = repmat([0,0,1],[2,1]);
end