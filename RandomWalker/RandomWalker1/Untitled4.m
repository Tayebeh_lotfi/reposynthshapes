figure,
for i = 1:5
    subplot (1,5,i), imshow(reshape(prob{1}(:,i),[size(img,1),size(img,2)]))
end

for i = 1:nObj
        prob{i} = X(1+(i-1)*ed:i*ed,:);
end

for i = 1:5
    subplot (4,5,i), imshow(reshape(prob{1}(:,i),[size(img,1),size(img,2)]))
    subplot (4,5,i+5), imshow(reshape(prob{2}(:,i),[size(img,1),size(img,2)]))
    subplot (4,5,i+10), imshow(reshape(prob{3}(:,i),[size(img,1),size(img,2)]))    
    subplot (4,5,i+15), imshow(reshape(prob{4}(:,i),[size(img,1),size(img,2)]))    
end
pb = prob{1}(:,1);
for i = 2:nObj
    pb = pb .* prob{i}(:,1);
end

figure, 
subplot (1,5,1), imshow(reshape(pb,[size(img,1),size(img,2)]))
for i = 1:nObj
    subplot (1,5,i+1), imshow(reshape(prob{i}(:,i+1),[size(img,1),size(img,2)]))
end

