clc, close all, clear,
addpath(genpath('../RW'));
addpath(genpath('../TreeBagDcmp'));
%% Reading image
Y = im2double(imread('F:\Research\Code\ImagesTextures\football1.jpg'));
img = Y;
%% Providing seeds
ssz = size(img);
[sXi, sYi, ~] = impixel(img);
sXi(end)=[]; sYi(end)=[];
slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4);...
        sYi(5),sXi(5);sYi(6),sXi(6);sYi(7),sXi(7);sYi(8),sXi(8)];
seeds_ = sub2ind([size(img,1),size(img,2)],slands(:,1),slands(:,2));

labels_ = [ones(1,4),repmat(2,[1,8])];
label_adjust_=min(labels_); labels_=labels_-label_adjust_+1; %Adjust labels to be > 0
labels_record_(labels_)=1;
labels_present_=find(labels_record_);
number_labels_=length(labels_present_);

Oboundary=zeros(length(seeds_),number_labels_);
Oboundary(1:4,:) = repmat([1,0],[4,1]);
Oboundary(5:12,:) = repmat([0,1],[8,1]);
[lndmrki,lndmrkj] = ind2sub(sz(img),seeds_);
%% RW
[Xc, Yc, Zc]=size(img);
%Build graph
[points, edges]=lattice(Xc,Yc,1);
imgVals=img(:); 
beta = 900; 
weights=makeweights(edges,imgVals,beta);
L=laplacian(edges,weights);
tic
probabilities_or=dirichletboundary(L,seeds_(:),Oboundary);
pt2 = toc;
probS_ = probabilities_or;
% mxv = max(probS_,[],2);
% mnv = min(probS_,[],2);
% probS_ = (probS_ - repmat(mnv,[1,sz(probS_,2)])) ...
%     ./ (repmat(mxv,[1,sz(probS_,2)]) - repmat(mnv,[1,sz(probS_,2)]));
probS_ = probS_ ./ repmat(sum(probS_,2),[1,sz(probS_,2)]);

%% Visualization
figure,

subplot 121, imshow(reshape(probS_(:,1),[Xc,Yc]),[]);
subplot 122, imshow(reshape(probS_(:,2),[Xc,Yc]),[]);
thresh = .005;
mask(:,:,1) = reshape(probS_(:,2)>thresh,[Xc,Yc]); mask(:,:,2) = mask(:,:,1); mask(:,:,3) = mask(:,:,1);
figure, subplot 121, imshow(img), subplot 122, imshow(mask .* img)

myimg = mask .* img;
mask1 = mask(:,:,1);
save('balltemp.mat', 'myimg', 'mask1');
% 
% [val_, msk_] = max(probS_,[],2);
% msk1_ = reshape(msk_==1,[Xc,Yc]);
% msk2_ = reshape(msk_==2,[Xc,Yc]);
% 
% subplot 131, imshow(msk1_), subplot 132, imshow(msk2_), 
% subplot 133, imshow(img), hold on, plot(lndmrkj,lndmrki,'r*'), title('Third Row: Ground Truth with the initial seeds') 
% 

figure,
subplot 131, imshow(grdmsk1),
subplot 132, imshow(grdmsk2),
subplot 133, imshow(grdmsk1&grdmsk2)
intersect = grdmsk1 .* grdmsk2;
image_1 = grdmsk1 .* img; image_2 = grdmsk2 .* img;
M = Xc;
index = find(intersect==1);
image_1(index) = 0;
image_2(index) = 0;
ii = 1;
counter = 0;
while(ii <= len(index) ) % >0)
% for ii = 1:size(index)
    batch = [index(ii)-M-1,index(ii)-1,index(ii)+M-1,index(ii)-M,index(ii),index(ii)+M,index(ii)-M+1,index(ii)+1,index(ii)+M+1];
    jj = 1;
    while(jj<=len(batch))
        if(find(index==batch(jj)))
            batch(jj) = [];
        else
            jj = jj+1;
        end
    end
    if(len(batch)>0)
        m = mean(img(batch));
        v = var(img(batch));
        image_1(index(ii)) = m-v;
        image_2(index(ii)) = img(index(ii)) - image_1(index(ii));
        ii = ii + 1; % index(ii) = [];
    else
        ii = ii+1;
    end
    counter = counter+1;
end


Im1 = X(:,:,1); Im2 = X(:,:,2);
index_ = find(intersect==1);

temp1 = image_1 .* intersect;
temp2 = image_2 .* intersect;

mymsk1 = (temp1 > 0);
mymsk2 = (temp2 > 0);

temp1_ = Im1*A_Pri1(1) .* mymsk1;
temp2_ = Im2*A_Pri1(2) .* mymsk2;
(sum(sum(abs(temp1 - temp1_)))/(size(find(temp1>0),1)) + sum(sum(abs( temp2 - temp2_)))/(size(find(temp2>0),1)))/2
%%
[Im1(index_(counter))*A_Pri1(1), image_1(index_(counter)), Im2(index_(counter))*A_Pri1(2), image_2(index_(counter))]
figure, subplot 121, imshow(image_1), subplot 122, imshow(image_2)
figure, subplot 121, imshow(temp1), subplot 122, imshow(temp2)
figure, subplot 121, imshow(mymsk1), subplot 122, imshow(mymsk2)
figure, subplot 121, imshow(temp1_), subplot 122, imshow(temp2_)



%%

intersect = grdmsk1 .* grdmsk2;
image_1 = grdmsk1 .* img; image_2 = grdmsk2 .* img;
M = Xc;
index = find(intersect==1);
image_1(index) = 0;
image_2(index) = 0;
ii = 1;
counter = 0;


while(len(index)>0)
% for ii = 1:size(index)
    if(counter==0)
        image_1(index(ii)) = img(index(ii)-1);
        image_2(index(ii)) = img(index(ii)) - image_1(index(ii));
        index(ii) = [];
        counter = counter + 1;
    end
    if (mod(counter,2)==0)
        image_1(index(ii)) = image_1(index(ii)-1);
        image_2(index(ii)) = img(index(ii)) - image_1(index(ii));
    else
        image_2(index(ii)) = image_2(index(ii)-1);
        image_1(index(ii)) = img(index(ii)) - image_2(index(ii));
    end
    index(ii) = [];
    counter = counter+1;
end

figure,
subplot 121, imshow(image_1), subplot 122, imshow(image_2)



%% Attempt3:
intersect = grdmsk1 .* grdmsk2;
image_1 = grdmsk1 .* img; image_2 = grdmsk2 .* img;
M = Xc;
index = find(intersect==1);
image_1(index) = 0;
image_2(index) = 0;
ii = 1;
counter = 0;


%%


% Gabor1 = gabor_filterbank(X(:,:,1),5,8,17); % 18,19,20,26,27,28,34,35,36 (19,27,35)
% Gabor2 = gabor_filterbank(X(:,:,2),5,8,17); % 6,14,38
% Gabor = Gabor2;
% M = 100;
% figure,
% for ii = 1:10
%     hold on, plot(Gabor(index(ii)-M-1,:))
%     hold on, plot(Gabor(index(ii)-1,:))
%     hold on, plot(Gabor(index(ii)+M-1,:))
%     hold on, plot(Gabor(index(ii)-M,:))
%     hold on, plot(Gabor(index(ii),:))
%     hold on, plot(Gabor(index(ii)+M,:))
%     hold on, plot(Gabor(index(ii)-M+1,:))
%     hold on, plot(Gabor(index(ii)+1,:))
%     hold on, plot(Gabor(index(ii)+M+1,:))
%     pause
% end
% 
% G1(:,1) = Gabor1(:,19); G1(:,2) = Gabor1(:,27); G1(:,3) = Gabor1(:,35);
% G2(:,1) = Gabor2(:,6); G2(:,2) = Gabor2(:,14); G2(:,3) = Gabor2(:,38);
% 
% Gmean1 = mean(G1,1);
% Gmean2 = mean(G2,1);
% 
% figure, hold on, plot(Gmean1), hold on, plot(Gmean2)
% figure, subplot 121, imshow(X(:,:,1)), subplot 122, imshow(X(:,:,2))







