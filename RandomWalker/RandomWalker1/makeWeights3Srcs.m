function [L12,L13,L23,Txt] = makeWeights3Srcs(TxrPth,Txtf1,Txtf2,Txtf3,img)    
    I1 = imread([TxrPth,Txtf1]); % 1.5.03, 1.1.01
    I2 = imread([TxrPth,Txtf2]); 
    I3 = imread([TxrPth,Txtf3]); 
    x11 = fix((sz(I1,1)-sz(img,1))*rand(1,1))+1;
    x12 = fix((sz(I1,2)-sz(img,2))*rand(1,1))+1;
    x21 = fix((sz(I2,1)-sz(img,1))*rand(1,1))+1;
    x22 = fix((sz(I2,2)-sz(img,2))*rand(1,1))+1;
    x31 = fix((sz(I3,1)-sz(img,1))*rand(1,1))+1;
    x32 = fix((sz(I3,2)-sz(img,2))*rand(1,1))+1;
    Txt(:,:,1) = I1(x11:x11+sz(img,1)-1,x12:x12+sz(img,2)-1);
    Txt(:,:,2) = I2(x21:x21+sz(img,1)-1,x22:x22+sz(img,2)-1);
    Txt(:,:,3) = I3(x31:x31+sz(img,1)-1,x32:x32+sz(img,2)-1);
    Txt = im2double(Txt);
    msk = img~=0;
    [Xc, Yc, Zc]=size(img);
    %Build graph
    [points, edges]=lattice(Xc,Yc,1);
    im1 = abs(Txt(:,:,1) - Txt(:,:,2)) .* msk;
    im2 = abs(Txt(:,:,1) - Txt(:,:,3)) .* msk;
    im3 = abs(Txt(:,:,2) - Txt(:,:,3)) .* msk;
%     imgVals=im(:); 
    mnd1 = 1 - double(reshape(MIND_descriptor2d(im1,2,.2),Xc*Yc,[]));
    mnd2 = 1 - double(reshape(MIND_descriptor2d(im2,2,.2),Xc*Yc,[]));
    mnd3 = 1 - double(reshape(MIND_descriptor2d(im3,2,.2),Xc*Yc,[]));
    imgVals1 = mnd1; % mnd(:,1:20);
    imgVals2 = mnd2; % mnd(:,1:20);
    imgVals3 = mnd3; % mnd(:,1:20);
    beta = 150; 
    weights1=makeweights(edges,imgVals1,beta);
    weights2=makeweights(edges,imgVals2,beta);
    weights3=makeweights(edges,imgVals3,beta);
    L12=laplacian(edges,weights1);    
    L13=laplacian(edges,weights2);    
    L23=laplacian(edges,weights3);    
end