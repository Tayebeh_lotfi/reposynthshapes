function [seeds,Pboundary,seeds_,Oboundary] = prepare4_Cmp2Src(img)
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];
    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4); ...
        sYi(5),sXi(5);sYi(6),sXi(6);sYi(7),sXi(7);sYi(8),sXi(8)];
    seeds_ = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds = [seeds_;seeds_+prod(sz(img))];

    labels = [ones(1,4),repmat(2,[1,4]),ones(1,12),repmat(3,[1,4])];
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);
    
    Pboundary=zeros(length(seeds),number_labels);
    Pboundary(1:4,:) = repmat([1,0,0],[4,1]);
    Pboundary(5:8,:) = repmat([0,1,0],[4,1]);
    Pboundary(9:20,:) = repmat([1,0,0],[12,1]);
    Pboundary(21:24,:) = repmat([0,0,1],[4,1]);
%%
    labels_ = [ones(1,4),repmat(2,[1,4]),repmat(3,[1,4])];
    label_adjust_=min(labels_); labels_=labels_-label_adjust_+1; %Adjust labels to be > 0
    labels_record_(labels_)=1;
    labels_present_=find(labels_record_);
    number_labels_=length(labels_present_);

    Oboundary=zeros(length(seeds_),number_labels_);
    Oboundary(1:4,:) = repmat([1,0,0],[4,1]);
    Oboundary(5:8,:) = repmat([0,1,0],[4,1]);
    Oboundary(9:12,:) = repmat([0,0,1],[4,1]);
end