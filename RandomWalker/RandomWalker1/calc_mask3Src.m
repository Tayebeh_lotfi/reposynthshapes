function msk = calc_mask3Src(X,img,nObj) % , ovlp1,ovlp2
    img_ = img(:,:,1);
    ed = prod(sz(img_)); % floor( sz(X,1) ./ 2);
    msk = zeros(ed*(nObj+1),1); % zeros(sz(X,1),1);
    thresh =.5; % .1
    for i = 1:nObj
        prob{i} = X(1+(i-1)*ed:i*ed,:);
        idx{i+1} = find(prob{i}(:,i+1)> thresh);
        msk(i*ed+idx{i+1}) = 1;
    end
    pb = prob{1}(:,1);
    for i = 2:nObj
        pb = pb .* prob{i}(:,1);
    end 
    idx{1} = find(pb>thresh);
    msk(idx{1}) = 1;
end

%     idx2 = find(prob2(:,3)> thresh);
%     idx3 = find(prob3(:,4)> thresh);
%     msk(idx1) = 1;
%     msk(ed + idx2) = 1;
%     msk(2*ed + idx3) = 1; 
