#include "mex.h"
#include <limits.h>
void Bestexemp(
const int RR,
const int NN,
const int R,
const int N,
const double *Image,
const double *IP,
const mxLogical *ToFill,
const mxLogical *SRegion,
double *Best)
{
    register int i,j,ii,jj,ii2,jj2,Row, Col, I,J,rdx, rdx2,RN=R*N,RRNN=RR*NN;
    double PatchError=0.0,Error=0.0,BestError=10000000000,MS=0.0,ns=0.0,MD;
    /* For each Patch */
    Col=NN-N+1; Row=RR-R+1;
    for (j=1; j<=Col; ++j)
    {
        J=j+N-1;
        for (i=1; i<=Row; ++i)
        {
            I=i+R-1;
            /***To Calculate Patch Error for each pixel in the current patch ***/
            for (jj=j,jj2=1; jj<=J; ++jj,++jj2)
            {
                for (ii=i,ii2=1; ii<=I; ++ii,++ii2)
                {
                    rdx=ii-1+RR*(jj-1) ;
                    if (!SRegion[rdx])
                        goto Patch;
                    rdx2=ii2-1+R*(jj2-1);
                    if (!ToFill[rdx2])
                    {
                        ns=ns+1;
                        Error=Image[rdx]-IP[rdx2];
                        PatchError+=Error*Error;
                        Error=Image[rdx+=RRNN]-IP[rdx2+=RN];
                        PatchError+=Error*Error;
                        Error=Image[rdx+=RRNN]-IP[rdx2+=RN];
                        PatchError+=Error*Error;
                        /* ns is number of known pixels in the target region */
                        MS=PatchError/ns;
                        MD=MS/log(ns); // (mn);
                    }

                }
            }
            /*** To Update the values***/
            if (PatchError < BestError)
            {
                BestError = PatchError;
                Best [0] = i; Best[1] = I;
                Best[2] = j; Best [3] = J;
                Best[4]=MD;
            }
            Patch:
            PatchError = 0.0;
        }
    }
}
void mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray
*prhs[])
{
int RR,NN,R,N;
double *Image,*IP,*Best;
mxLogical *ToFill,*SRegion,*H;
/* To assign the inputs */
RR = (int)mxGetScalar(prhs[0]);
NN = (int)mxGetScalar(prhs[1]);
R = (int)mxGetScalar(prhs[2]);
N = (int)mxGetScalar(prhs[3]);
Image = mxGetPr(prhs[4]);
IP = mxGetPr(prhs[5]);
ToFill = mxGetLogicals(prhs[6]);
SRegion = mxGetLogicals(prhs[7]);
/* Setup the output */
plhs[0] = mxCreateDoubleMatrix(5,1,mxREAL);
Best = mxGetPr(plhs[0] );
Best[0]=Best[1]=Best[2]=Best[3]=0.0;
Bestexemp(RR,NN,R,N,Image,IP,ToFill,SRegion,Best);
}