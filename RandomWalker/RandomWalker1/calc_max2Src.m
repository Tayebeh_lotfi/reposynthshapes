function [val, msk] = calc_max2Src(X,img) % , ovlp1,ovlp2
    ed = prod(sz(img));
    [val, msk] = max(X,[],2);

    prob1 = X(1:ed,:);
    prob2 = X(ed+1:end,:);
    [seg,cent] = kmeans(prob1(:,2),4);
    [ii,jj] = sort(cent);
    [seg_,cent_] = kmeans(prob2(:,3),4);
    [ii_,jj_] = sort(cent_);
    if(ii(2)>.15)
        idx1 = find(seg~=jj(1));
    else
        idx1 = find(seg==jj(3) | seg==jj(4));
    end
    idx2 = find(seg_~=jj_(1));
    msk(idx1) = 2;
    msk(ed + idx2) = 3;
    val(idx1) = X(idx1,2); val(ed+idx2) = X(ed+idx2,3);
    C = intersect(idx1,idx2);
    ovlp1 = zeros(sz(img));
    ovlp1(C) = 1;
end