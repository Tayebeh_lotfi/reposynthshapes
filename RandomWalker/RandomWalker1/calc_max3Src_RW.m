function [msk1,msk2,msk3] = calc_max3Src_RW(X,img) % , ovlp1,ovlp2
    [seg1,cent1] = kmeans(X(:,2),6,'Replicates',5,'MaxIter',1000);
    [ii1,jj1] = sort(cent1);
    [seg2,cent2] = kmeans(X(:,3),6,'Replicates',5,'MaxIter',1000);
    [ii2,jj2] = sort(cent2);
    [seg3,cent3] = kmeans(X(:,4),6,'Replicates',5,'MaxIter',1000);
    [ii3,jj3] = sort(cent3);
    if(ii1(2)<.15)
        if(ii1(3)<.1)
            idx1 = find(seg1==jj1(4) | seg1==jj1(5) | seg1==jj1(6));
        else
            idx1 = find(seg1==jj1(3) | seg1==jj1(4) | seg1==jj1(5) | seg1==jj1(6));
        end
    else
        idx1 = find(seg1~=jj1(1));
    end
    if(ii2(2)<.15)
        if(ii2(3)<.1)
            idx2 = find(seg2==jj2(4) | seg2==jj2(5) | seg2==jj2(6));
        else
            idx2 = find(seg2==jj2(3) | seg2==jj2(4) | seg2==jj2(5) | seg2==jj2(6));
        end
    else
        idx2 = find(seg2~=jj2(1));
    end
    if(ii3(2)<.15)
        if(ii3(3)<.15)
            idx3 = find(seg3==jj3(4) | seg3==jj3(5) | seg3==jj3(6));
        else
            idx3 = find(seg3==jj3(3) | seg3==jj3(4) | seg3==jj3(5) | seg3==jj3(6));
        end
    else
        idx3 = find(seg3~=jj3(1));
    end
    msk1 = zeros(sz(img)); msk2 = msk1; msk3 = msk1;
    msk1(idx1) = 1;
    msk2(idx2) = 1;
    msk3(idx3) = 1;
end