function [seeds_,Sboundary,Vboundary] = prepare1_val(img)
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];

    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2);sYi(1),sXi(1); sYi(2),sXi(2);sYi(3),sXi(3)]; % ]; % 
    seeds = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds_ = [seeds;seeds+prod(sz(img))];

    labels = [ones(1,4),2,1,2,ones(1,4),1,3,3]; % four corners, class1, class1, class2, class2, 
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);

    Vboundary=zeros(length(seeds_),number_labels);
    Vboundary(1:4,:) = repmat([0,0,0],[4,1]);
    Vboundary(5,:) = [0,img(seeds_(5)),0];
    Vboundary(6,:) = [0,0,0]; % img(seeds_(6)),0];
    Vboundary(7,:) = [0,img(seeds_(7)),0];
    Vboundary(8:12,:) = repmat([0,0,0],[5,1]);
    Vboundary(13,:) = [0,0,img(seeds_(6))];
    Vboundary(14,:) = [0,0,img(seeds_(7))]; 
    
    Sboundary(1:4,:) = repmat([1,0,0],[4,1]);
    Sboundary(5,:) = [0,1,0];
    Sboundary(6,:) = [0,0,0];
    Sboundary(7,:) = [0,1,0];
    Sboundary(8:12,:) = repmat([1,0,0],[5,1]);
    Sboundary(13:14,:) = repmat([0,0,1],[2,1]);
end