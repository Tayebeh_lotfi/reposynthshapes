function [seeds_, Sboundary, Vboundary] = prepare2_val(img)
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];

    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2);sYi(1),sXi(1); ...
        sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4);sYi(5),sXi(5); sYi(6),sXi(6)]; % ]; % 
    seeds = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds_ = [seeds;seeds+prod(sz(img));seeds+2*prod(sz(img))];

    labels = [ones(1,4),2,2,ones(1,10),3,3,ones(1,10),4,4]; % four corners, class1, class1, class2, class2, 
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);

    Vboundary=zeros(length(seeds_),number_labels);
%     Vboundary(1:4,:) = repmat([0,0,0],[4,1]);
%     Vboundary(5,:) = [0,img(seeds_(5)),0];
%     Vboundary(6,:) = [0,img(seeds_(6)),0];
%     Vboundary(7:14,:) = repmat([0,0,0],[8,1]);
%     Vboundary(15,:) = [0,0,img(seeds_(7))];
%     Vboundary(16,:) = [0,0,img(seeds_(8))];  
    
    Sboundary=zeros(length(seeds_),number_labels);
    Sboundary(1:4,:) = repmat([1,0,0,0],[4,1]);
    Sboundary(5:6,:) = repmat([0,1,0,0],[2,1]);
    Sboundary(7:16,:) = repmat([1,0,0,0],[10,1]);
    Sboundary(17:18,:) = repmat([0,0,1,0],[2,1]);
    Sboundary(19:28,:) = repmat([1,0,0,0],[10,1]);
    Sboundary(29:30,:) = repmat([0,0,0,1],[2,1]);

end