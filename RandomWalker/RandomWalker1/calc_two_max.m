function [val, msk, ovlp1,ovlp2] = calc_two_max(X,img)
    ed = prod(sz(img)); % floor( sz(X,1) ./ 2);
    [val, msk] = max(X,[],2);

    prob1 = X(1:ed,:);
    prob2 = X(ed+1:end,:);
    [seg,cent] = kmeans(prob1(:,2),4);
    [ii,jj] = sort(cent);
    [seg_,cent_] = kmeans(prob2(:,3),4);
    [ii_,jj_] = sort(cent_);
    if(ii(2)>.15)
        idx1 = find(seg~=jj(1));
        idx11 = find(seg==jj(2) | seg==jj(3));
    else
        idx1 = find(seg==jj(3) | seg==jj(4));
        idx11 = find(seg==jj(3));
    end
    idx2 = find(seg_~=jj_(1));
    idx22 = find(seg_==jj_(3) | seg_ == jj_(2));
    msk(idx1) = 2;
    msk(ed + idx2) = 3;
    val(idx1) = X(idx1,2); val(ed+idx2) = X(ed+idx2,3);
    C = intersect(idx11,idx22);
    ovlp1 = zeros(sz(img));
    ovlp1(C) = 1;
%%    
    epsilon = 0; % 1e-10;
    u1 = X .* log(X+epsilon);
    u1(isnan(u1)) = 0;
    unc = -1/sz(X,2)*sum(u1,2);
    unc1 = resc(unc(1:ed));
    unc2 = resc(unc(ed+1:end));
    mask = img(:) > 0; 
    cert_thres1 = quantile(unc1(find(mask>0)),.75);
    Uncert1 = unc1 .* mask;
    idx_1=find( Uncert1 > cert_thres1); % Uncert1 <cert_thresh1;
    cert_thres2 = quantile(unc2(find(mask>0)),.75);
    Uncert2 = unc2 .* mask;
    idx_2=find( Uncert2 > cert_thres2); % Uncert1 <cert_thresh1;
    C_ = intersect(idx_1,idx_2);
    ovlp2 = zeros(sz(img));
    ovlp2(C_)=1;

end
%% 
%     thresh1 = .5;
%     thresh2 = .2;
%     [m,n] = sort(X);
%     idx1 = find( X(1:ed,2)>thresh2 & X(1:ed,1)>thresh1);
%     idx2 = find( X(ed+1:2*ed,3)>thresh2 & X(ed+1:2*ed,1)>thresh1);
%     msk(idx1)=2; msk(ed+idx2)=3;