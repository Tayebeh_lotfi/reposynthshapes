clc, close all, clear,
addpath(genpath('../RW'));
addpath(genpath('../TreeBagDcmp'));
opt.TxrPth = '../../ImagesTextures/'; 
opt.Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.03.tiff'; % 1.1.03
opt.Txtf3 = '1.1.10.tiff'; % '1.1.10.tiff'
case_ = 2;
%%
if(case_ == 1)
    Y = imread('TestImages\TestImage1.jpg');
    Y = im2double(Y);
    % im = Y(:,:,1);
    im = Y(60:430,380:1020,:);
    img = imresize(im,[200,200]);
else
    Y = imread('TestImages\TestImage5.jpg');
    im = Y(107:280,136:327,:);
    img = im;
end
figure, imshow(img)
numObj = input('how many objects? ');
%%
% [seeds,boundaryp,seeds_,boundaryo] = prepare4_Cmp3Src(img);
% numObj = 4;
[seeds,boundaryP,seeds_,boundaryO] = prepareLineCmp3Src(img,numObj);
[lndmrki,lndmrkj] = ind2sub(sz(img),seeds_);

[Xc, Yc, ~]=size(img);
%Build graph
[points, edges]=lattice(Xc,Yc,1);
if (size(img,3)>1)
    for ii = 1:size(img,3)
        tmp = img(:,:,ii);
        imgVals(:,ii) = tmp(:);
    end
else
    imgVals=img(:); 
end
beta = 90; 
weights=makeweights(edges,imgVals,beta);
L=laplacian(edges,weights);
% L12 = L; L13 = L12; L14 = L12; L23 = L12; L24 = L12;L34 = L12;
alpha = 0; L_ = alpha*L;
% [L12,L13,L23,~] = makeWeights3Srcs(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img);
% LL = [L,alpha*L12,alpha*L13,alpha*L14;alpha*L12,L,alpha*L23,alpha*L24;...
%     alpha*L13,alpha*L23,L,alpha*L34; alpha*L14,alpha*L24,alpha*L34,L]; % 1e-1
if(numObj==2)
    LL = [L,L_;L_,L];
end
if(numObj==3)
    LL = [L,L_,L_;L_,L,L_;L_,L_,L];
end
if(numObj==4)
    LL = [L,L_,L_,L_;L_,L,L_,L_;L_,L_,L,L_;L_,L_,L_,L];
end
if(numObj==5)
    LL = [L,L_,L_,L_,L_;L_,L,L_,L_,L_;L_,L_,L,L_,L_;L_,L_,L_,L,L_;L_,L_,L_,L_,L];
end

tic
probabilities_pro=dirichletboundary(LL,round(seeds(:)),boundaryP);
pt1 = toc;
probS = probabilities_pro;
mxv = max(probS,[],2);
mnv = min(probS,[],2);
probS = (probS - repmat(mnv,[1,sz(probS,2)])) ...
    ./ (repmat(mxv,[1,sz(probS,2)]) - repmat(mnv,[1,sz(probS,2)]));
probS = probS ./ repmat(sum(probS,2),[1,sz(probS,2)]);
%%
[Xc, Yc, Zc]=size(img);
tic
probabilities_or=dirichletboundary(L,seeds_(:),boundaryO);
pt2 = toc;
probS_ = probabilities_or;
mxv = max(probS_,[],2);
mnv = min(probS_,[],2);
probS_ = (probS_ - repmat(mnv,[1,sz(probS_,2)])) ...
    ./ (repmat(mxv,[1,sz(probS_,2)]) - repmat(mnv,[1,sz(probS_,2)]));
probS_ = probS_ ./ repmat(sum(probS_,2),[1,sz(probS_,2)]);

%%
iter = 0;
ed = Xc*Yc;
msk = calc_mask3Src(probS, img, numObj);
for i = 1:numObj+1
    msk1(:,:,i) = reshape(msk(1+(i-1)*ed:i*ed),[Xc,Yc]);
end

[val_, msk_] = max(probS_,[],2);
for i = 1:numObj+1
    msk1_(:,:,i) = reshape(msk_==i,[Xc,Yc]);
end
% msk2_ = reshape(msk_==3,[Xc,Yc]);
% msk3_ = reshape(msk_==4,[Xc,Yc]);
%% Showing Results
h = figure(1);
for i = 1:numObj+1
    subplot (3,numObj+1,i), imshow(msk1(:,:,i)), if(i==1),title(sprintf('First Row: Proposed Seg Results')),end
    subplot (3,numObj+1,numObj+i+1), imshow(msk1_(:,:,i)), if(i==1), title(sprintf('Second Row: RW Seg Results')),end
    subplot (3,numObj+2,(numObj+2)*2+1), imshow(img), hold on,
        plot(lndmrkj,lndmrki,'r*'), title('Third Row: Ground Truth with the initial seeds') 
end



% 
% im2(:,:,1) = imresize(im1(:,:,1),[200,200]);
% im2(:,:,2) = imresize(im1(:,:,2),[200,200]);
% im2(:,:,3) = imresize(im1(:,:,3),[200,200]);
% 
% figure, imshow(im2(:,:,3))
% 
% img = im2(:,:,2);
