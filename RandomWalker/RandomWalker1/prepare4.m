function [seeds_,boundary, length_] = prepare4(img)
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];
    length_ = length(sXi);
    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3); sYi(4),sXi(4); ...
        sYi(5),sXi(5);sYi(6),sXi(6);sYi(7),sXi(7);sYi(8),sXi(8)]; % ]; % 
    seeds = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds_ = [seeds;seeds+prod(sz(img))];


    labels = [ones(1,4),repmat(2,[1,4]),ones(1,12),repmat(3,[1,4])];
%     [1,1,1,1,2,2,1,1,1,1,1,1,1,1,3,3]; % four corners,class1,class1,
%     class1,class1,class2,class2,class2,class2
    label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
    labels_record(labels)=1;
    labels_present=find(labels_record);
    number_labels=length(labels_present);

    boundary=zeros(length(seeds_),number_labels);
    boundary(1:4,:) = repmat([1,0,0],[4,1]);
    boundary(5:8,:) = repmat([0,1,0],[4,1]);
    boundary(9:20,:) = repmat([1,0,0],[12,1]);
    boundary(21:24,:) = repmat([0,0,1],[4,1]);
end