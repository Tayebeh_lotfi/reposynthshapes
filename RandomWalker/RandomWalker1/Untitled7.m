
m1 = msk1;
m2 = msk2;
m3 = m1 & m2;
% figure, subplot 131, imshow(m1), subplot 132, imshow(m2), subplot 133, imshow(m3)

[Xc,Yc,~] = size(img);
ed = numel(img);
s1 = find(m1>0 & m2 == 0);
s2 = find(m1 == 0 & m2 > 0);
index = [s1;s2];
vals = img(index);





N=length(L);
antiIndex=1:N;
antiIndex(index)=[];




size(L(antiIndex,antiIndex)*img(antiIndex)')

figure, imshow(reshape(10*L*img(:),[100,100]),[])



%%
m1 = msk1;
m2 = msk2;
m3 = m1 & m2;
[Xc,Yc,~] = size(img);

[points, edges]=lattice(Xc,Yc,1);
imgVals=m1(:); 
beta = 9; 
weights=makeweights(edges,imgVals,beta);
L1_=laplacian(edges,weights);
aa = diag(L1_);
figure, imshow(reshape(full(aa),[100,100]),[])
figure, imshow(reshape(full(aa).*img(:),[100,100]),[])

figure, imshow(m1)


%%
m1 = msk1;
m2 = msk2;
m3 = m1 & m2;
[Xc,Yc,~] = size(img);
beta = 90;
[points, edges]=lattice(Xc,Yc,1);

L11 = speye(numel(img));
L11 = diag(diag(L11) .* img(:));

imgVals=m1(:); % .*img(:); 
weights=makeweights(edges,imgVals,beta);
L22=1e2*laplacian(edges,weights);

imgVals=m2(:); % .*img(:); 
weights=makeweights(edges,imgVals,beta);
L33=1e2*laplacian(edges,weights);

pp = sparse(numel(img),numel(img));
L23 = zeros(numel(img),'like',pp);
L12 = zeros(numel(img),'like',pp);
L13 = zeros(numel(img),'like',pp);

mindx1 = find(m1==1);
mindx2 = find(m2==1);
% mindx3 = find(m3==1);

tmp = diag(L12);
aa = X(:,:,1);
tmp(mindx1) = -aa(mindx1); % -1;
% tmp(mindx3) = -1/2;
L12 = diag(tmp);

tmp = diag(L13);
aa = X(:,:,2);
tmp(mindx2) = -aa(mindx2); % -1;
% tmp(mindx3) = -1/2;
L13 = diag(tmp);


L22Inv = sparseinv(L22);
L33Inv = sparseinv(L33);

tmpL = L12*L22Inv*L12 + L13*L33Inv*L13;

res = (L11 - tmpL)*img(:);
figure, imshow(abs(img - reshape(res,[100,100])),[])



LL = [L11,L12,L13;L12,L22,L23;L13,L23,L33];

%%
imtmp1 = 1/2*fix(10*rand(5,5));
imtmp2 = 1/2*fix(10*rand(5,5));
imtmp1(imtmp1<2) = 0;
imtmp2(imtmp2<2) = 0;
imtmp1 = imtmp1 ./ max(imtmp1(:));
imtmp2 = imtmp2 ./ max(imtmp2(:));

imtmp = imtmp1+imtmp2;
m1tmp = imtmp1>0;
m2tmp = imtmp2>0;
m3tmp = m1tmp & m2tmp;

[Xc,Yc,~] = size(imtmp);
beta = 90;
[points, edges]=lattice(Xc,Yc,1);

L11 = speye(numel(imtmp));

imgVals=m1tmp(:).*imtmp(:); 
weights=makeweights(edges,imgVals,beta);
L22=laplacian(edges,weights);

imgVals=m2tmp(:).*imtmp(:); 
weights=makeweights(edges,imgVals,beta);
L33=laplacian(edges,weights);

pp = sparse(numel(imtmp),numel(imtmp));
L23 = zeros(numel(imtmp),'like',pp);
L12 = zeros(numel(imtmp),'like',pp);
L13 = zeros(numel(imtmp),'like',pp);

mindx1 = find(m1tmp==1);
mindx2 = find(m2tmp==1);
mindx3 = find(m3tmp==1);

tmp = diag(L12);
tmp(mindx1) = -1;
tmp(mindx3) = -1/2;
L12 = diag(tmp);

tmp = diag(L13);
tmp(mindx2) = -1;
tmp(mindx3) = -1/2;
L13 = diag(tmp);

L22Inv = sparseinv(L22);
L33Inv = sparseinv(L33);

tmpL = L12*L22Inv*L12 + L13*L33Inv*L13;

res = (L11 - tmpL)*imtmp(:);
figure, imshow(abs(imtmp - reshape(res,[100,100])),[])


%%
aa1 = sparse(zeros(25,25));
bbb = fix(2*rand(5,5));
ttt = find(bbb==1);
mmm = diag(aa1);
mmm(ttt) = -1;
aa2 = diag(mmm);
full(aa2)
[i,j] = find(full(aa2)~=0);

%%
m1 = msk1;
m2 = msk2;
m3 = m1 & m2;
m4 = m1 | m2;
img=im2double(Y);
[Xc,Yc,~] = size(img);
beta = 90;
[points, edges]=lattice(Xc,Yc,1);

L11 = speye(numel(img));
% L11 = diag(diag(L11) .* img(:));

pp = sparse(numel(img),numel(img));
L23 = zeros(numel(img),'like',pp);
L12 = zeros(numel(img),'like',pp);
L13 = zeros(numel(img),'like',pp);

mindx1 = find(m1==1);
mindx2 = find(m2==1);
% mindx3 = find(m3==1);

tmp = diag(L12);
tmp(mindx1) = 1;
L12 = diag(tmp);

tmp = diag(L13);
tmp(mindx2) = 1;
L13 = diag(tmp);

L22 = -L12; L33 = -L13;

S1 = X(:,:,1)*A_Pri1(1); S2 = X(:,:,2)*A_Pri1(2);
re1 = L11*img(:) - L12*S1(:) - L13*S2(:);

L22 = L12*img(:)/(S1(:)); % S1(:)\(L12*img(:));

size(L12*img(:)/(S1(:)))

L12*img(:)\(S1(:))
%%

imtmp1 = 1/2*fix(10*rand(5,5));
imtmp2 = 1/2*fix(10*rand(5,5));
imtmp1(imtmp1<2) = 0;
imtmp2(imtmp2<2) = 0;
imtmp1 = imtmp1 ./ max(imtmp1(:));
imtmp2 = imtmp2 ./ max(imtmp2(:));

imtmp = imtmp1+imtmp2;
m1 = imtmp1>0;
m2 = imtmp2>0;
m3 = m1 & m2;

[Xc,Yc,~] = size(imtmp);
beta = 90;
[points, edges]=lattice(Xc,Yc,1);

img = imtmp;
L11 = speye(numel(img));
% L11 = diag(diag(L11) .* img(:));

pp = sparse(numel(img),numel(img));
L23 = zeros(numel(img),'like',pp);
L12 = zeros(numel(img),'like',pp);
L13 = zeros(numel(img),'like',pp);

mindx1 = find(m1==1);
mindx2 = find(m2==1);
mindx3 = find(m3==1);

tmp = diag(L12);
tmp(mindx1) = 1;
L12 = diag(tmp);

tmp = diag(L13);
tmp(mindx2) = 1;
L13 = diag(tmp);

tmp =-diag(L12);
tmp(mindx3) = -4/2;
L22 = diag(tmp);

tmp =-diag(L13);
tmp(mindx3) = -4/2;
L33 = diag(tmp);


S1 = imtmp1; S2 = imtmp2;
re1 = L11*img(:) + L22*S1(:) + L33*S2(:);

res2 = L12*img(:)+L22*(S1(:));
res3 = L13*img(:)+L33*(S2(:));



L22_ = (L12*img(:)) / (S1(:));
L33_ = (L13*img(:)) / (S2(:));

XX = full(L22_);
XX(:,10)


res2_ = L12*img(:)-L22_*(S1(:));
res3_ = L13*img(:)-L33_*(S2(:));



