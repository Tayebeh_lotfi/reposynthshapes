path = 'F:\Research\Code\ImagesTextures\';
for ii = 1:9
    I(:,:,ii) = imread([path,'1.1.0',num2str(ii),'.tiff']);
end
for ii = 10:13
    I(:,:,ii) = imread([path,'1.1.',num2str(ii),'.tiff']);
end
for ii = 1:3
    I(:,:,ii+13) = imread([path,'1.2.0',num2str(ii),'.tiff']);
end
figure,
for ii = 1:16
    subplot(4,4,ii), imshow(I(:,:,ii))
end

%%
addpath(genpath('../RW'));
addpath(genpath('../TreeBagDcmp'));
%% generate data
opt.FixA = true; opt.Src = 2; opt.thresh = .25; opt.nVarIntens = -20; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 75; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 20; opt.width = 20; opt.mag = 10; 
opt.TxrPth = '../../ImagesTextures/'; 
opt.Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.03.tiff'; % 1.1.03
opt.Txtf3 = '1.1.10.tiff';
for ii = 1:4
    [A_Pri1, ~, X, Y, HasOverlap] = Create_Samples5(opt);
    while(HasOverlap ==0)
        [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples5(opt);
    end
    J(:,:,ii)=im2double(Y);
end

opt.Txtf1 = '1.2.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.02.tiff'; % 1.1.03
opt.Txtf3 = '1.2.10.tiff';
for ii = 5:8
    [A_Pri1, ~, X, Y, HasOverlap] = Create_Samples5(opt);
    while(HasOverlap ==0)
        [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples5(opt);
    end
    J(:,:,ii)=im2double(Y);
end

opt.Txtf1 = '1.5.03.tiff'; % 1.1.03
opt.Txtf2 = '1.2.07.tiff';
for ii = 9:12
    [A_Pri1, ~, X, Y, HasOverlap] = Create_Samples4(opt);
    while(HasOverlap ==0)
        [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples4(opt);
    end
    J(:,:,ii)=im2double(Y);
end

opt.Txtf1 = '1.1.12.tiff'; % 1.1.03
opt.Txtf2 = '1.1.06.tiff';
for ii = 13:16
    [A_Pri1, ~, X, Y, HasOverlap] = Create_Samples2(opt);
    while(HasOverlap ==0)
        [A_Pri1, A_Pri, X, Y, HasOverlap] = Create_Samples2(opt);
    end
    J(:,:,ii)=im2double(Y);
end
figure,
for ii = 1:16
    subplot(4,4,ii), imshow(J(:,:,ii))
end
