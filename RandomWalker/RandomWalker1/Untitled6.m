m1 = msk1;
m2 = msk2;
m3 = m1 & m2;
% figure, subplot 131, imshow(m1), subplot 132, imshow(m2), subplot 133, imshow(m3)

[Xc,Yc,~] = size(img);
ed = numel(img);
s1 = find(m1>0 & m2 == 0);
s2 = find(m1 == 0 & m2 > 0);
seed_ = [s1;s2];
seeds = [seed_;seed_+ed;seed_+2*ed];

L1 = -1/2 * speye(size(L));
pp = sparse(size(L,1),size(L,2));
L2 = zeros(size(L),'like',pp);

LL_ = [L,L1,L1;L1,L,L2;L1,L2,L];
a=1; b = length(seed_);
boundary(a:b) = img(seed_);

a = b+1; b = b + length(s1);
boundary(a:b) = img(s1);
a = b+1; b = b + length(s2);
boundary(a:b) = zeros(length(s2),1);

a = b+1; b = b + length(s1);
boundary(a:b) = zeros(length(s1),1); %img(s1);
a = b+1; b = b + length(s2);
boundary(a:b) = img(s2);
%% 
boundary = boundary';
%%
Values=dirichletboundary(LL_,seeds(:),boundary);

figure, 
subplot 131, imshow(reshape(Values(1:ed),[Xc,Yc]),[]);
subplot 132, imshow(reshape(Values(1+ed:2*ed),[Xc,Yc]),[]);
subplot 133, imshow(reshape(Values(1+2*ed:3*ed),[Xc,Yc]),[]);





