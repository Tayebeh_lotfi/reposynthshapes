function [msk1,msk2] = calc_max2Src_RW(X,img) % , ovlp1,ovlp2
    [seg,cent] = kmeans(X(:,2),4);
    [ii,jj] = sort(cent);
    [seg_,cent_] = kmeans(X(:,3),4);
    [ii_,jj_] = sort(cent_);
    if(ii(2)>.15)
        idx1 = find(seg~=jj(1));
    else
        idx1 = find(seg==jj(3) | seg==jj(4));
    end
    if(ii_(2)>.15)
        idx2 = find(seg_~=jj_(1));
    else
        idx2 = find(seg_==jj_(3) | seg_==jj_(4));
    end
    msk1 = zeros(sz(img)); msk2 = msk1;
    msk1(idx1) = 1;
    msk2(idx2-prod(sz(img))) = 1;
end