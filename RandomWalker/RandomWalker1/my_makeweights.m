function weights=my_makeweights(edges, vals, beta, isz, slands)
%Constants
if(nargin<2)
    weights = ones(size(edges,1),1); 
else
    EPSILON = 1e-5; % 1e-5;
%     % beta = 500; 
    multiply = 2;
% %     Compute intensity differences 

    valDistances=vals(edges(:,1))- vals(edges(:,2));
    inds = sub2ind(isz, slands(:,1), slands(:,2));
    for k = 1:size(slands,1),
        S = find(edges(:,1) == inds(k));
        valDistances(S) = valDistances(S) .* multiply;
    end
%     % valDistances=normalize(valDistances); %Normalize to [0,1]
    valDistances=resc(valDistances); %Normalize to [0,1]

%     %Compute Gaussian weights
    weights=exp(-( beta*valDistances.^2)) + EPSILON;
end
