function [InpaintedImage ,OriginalImage,C,D,fillMovie] =inpaints(ops)
    %Image is the Input Image
    %TRegion is the Target Region of the Input Image
    %SRegion is the Source Region of the Input Image
    % C is the Confidence Term
    % D is the Data Term
    % P is the Priorities
    % Example
    % function [InpaintedImage,OriginalImage,C,D]=inpaints([0,255,0]);
    % Image =imread('images9.jpg');% To read the input image
    Image = ops.img;
    m1 = ops.m1; m2 = ops.m2; fillColor = ops.fillcolor;
    Im=Image; % To assign the input image to Im
%     figure(1), imshow(Im);% To show the input image in M*N*3
%     h=imfreehand; % To draw a freehand mask
%     BW=createMask(h); % To create a mask using free hand draw
    BW = m1 & m2;
%     figure(2), imshow(BW); % To show the mask of the image
    Image = im2double(Image); % To convert a input image into double
    OriginalImage = Image;
%     imshow(BW);% To display the target region of the image
%     mask3d(:,:,1) = BW; % For the red plane
%     mask3d(:,:,2) = BW; % For the green plane
%     mask3d(:,:,3) = BW; % For the blue plane
%     Im(mask3d) = 0; % Set masked pixels to white
    Im(BW) = 0;
%     Image = Im;
%     figure(3), imshow(Im);
    G=Im; % rgb2gray(Im);
    SRegion=(G~=0);% To display the source region of the image
    TRegion=~SRegion;
    Indices = img2ind(Image);
    Size = [size(Image,1) size(Image,2)];
    % Initialize isophote values
%     [Ix(:,:,3) Iy(:,:,3)] = imgradient(Image(:,:,3));
%     [Ix(:,:,2) Iy(:,:,2)] = imgradient(Image(:,:,2));
    [Ix(:,:,1) Iy(:,:,1)] = imgradient(Image(:,:,1));
%     Ix = sum(Ix,3)/(3*255); Iy = sum(Iy,3)/(3*255);
    Ix = Ix / 255; Iy = Iy/255;
    tmp = Ix; Ix = -Iy; Iy = tmp; % Rotate gradient 90 degrees
    % Initialize confidence and data terms
    C = double(SRegion);
    D = repmat(-.1,Size);
    iteration = 1;
    if nargout==5 % 6

        fillMovie(1).cdata=uint8(Image);
        fillMovie(1).colormap=[];
        OriginalImage(1,1,:) = fillColor;
        iteration = 2;
    end
    rand('state',0);
    % While Loop until entire Target Region has been covered
    while any(TRegion(:))
    % Find contour & normalized gradients of Target region
        TRegionD = double(TRegion);
        Con = find(conv2(TRegionD,[1,1,1;1,-8,1;1,1,1],'same')>0);
        [Mx,My] = imgradient(double(~TRegion));
        M = [Mx(Con(:)) My(Con(:))];
        M = normr(M);
        M(~isfinite(M))=0;
        % Compute confidences along the Contour
        for K=Con'
            HP = getpatch(Size,K);
            q = HP(~(TRegion(HP)));
            C(K) = sum(C(q))/numel(HP);
        end
        % To find patch priorities = confidence term * data term
        D(Con) = abs(Ix(Con).*M(:,1)+Iy(Con).*M(:,2)) + 0.001;
        P = C(Con).* D(Con);
        % Find maximum priority, HP
        [unused,rdx] = max(P(:));
        p = Con(rdx(1));
        [HP,rows,columns] = getpatch(Size,p);
        ToFill = TRegion(HP);
        % Find Exemplar that minimizes Error, HQ
        [HQ,MD] = Bestexemplar(Image,Image(rows,columns,:),ToFill',SRegion);
        MDR=-MD;
        % Update Target region
        ToFill = logical(ToFill);
        TRegion(HP(ToFill)) = false;
        % Propagate confidence & isophote values
        C(HP(ToFill)) = C(p)*exp(MDR);
        if(HQ<0)
            break;
        end
        Ix(HP(ToFill)) = Ix(HQ(ToFill));
        Iy(HP(ToFill)) = Iy(HQ(ToFill));
        % Copy Image data from HQ to HP
        Indices(HP(ToFill)) = Indices(HQ(ToFill));
        Image(rows,columns) = ind2img(Indices(rows,columns),OriginalImage); % Image(rows,columns,:)
        if nargout==5 %6
            Indice = Indices;
            Indice(logical(TRegion)) = 1;
            fillMovie(iteration).cdata=uint8(ind2img(Indice,OriginalImage));
            fillMovie(iteration).colormap=[];
        end
        iteration = iteration+1;
    end
    InpaintedImage=Image;
    figure(8);
    imshow(InpaintedImage);
    
    
    function [HQ,MD] = Bestexemplar(Image,IP,ToFill,SRegion)
        R=size(IP,1); RR=size(Image,1); N=size(IP,2); NN=size(Image,2);
        Best = Bestexemp(RR,NN,R,N,Image,IP,ToFill,SRegion);
        HQ = sub2ndx(Best(1):Best(2),(Best(3):Best(4))',RR);
        MD=Best(5);
%     end


    function [HP,rows,columns] = getpatch(Size,p)
        W=3; p=p-1; Y=floor(p/Size(1))+1; p=rem(p,Size(1)); X=floor(p)+1;
        rows = max(X-W,1):min(X+W,Size(1));
        columns = (max(Y-W,1):min(Y+W,Size(2)))';
        HP = sub2ndx(rows,columns,Size(1));
%     end

    function M = sub2ndx(rows,columns,nRows)
        Row = rows(ones(length(columns),1),:);
        Col = columns(:,ones(1,length(rows)));
        M = Row+(Col-1)*nRows;
%     end

    function Image2 = ind2img(Indices,Image)
%         for i=3:-1:1,tmp=Image(:,:,i);
%             Image2(:,:,i)=tmp(Indices);
            tmp = Image;
            Image2 = tmp(Indices);
%         end
%     end

    function Indice = img2ind(Image)
    	S=size(Image); Indice=reshape(1:S(1)*S(2),S(1),S(2));
%     end
% end