function index = find_index(Srcs,img)
    msk{1} = Srcs(:,:,1)>0&Srcs(:,:,2)==0&Srcs(:,:,3)==0;
    msk{2} = Srcs(:,:,1)==0&Srcs(:,:,2)>0&Srcs(:,:,3)==0;
    msk{3} = Srcs(:,:,1)==0&Srcs(:,:,2)==0&Srcs(:,:,3)>0;
    msk{4} = Srcs(:,:,1)>0&Srcs(:,:,2)>0&Srcs(:,:,3)==0;
    msk{5} = Srcs(:,:,1)>0&Srcs(:,:,2)==0&Srcs(:,:,3)>0;
    msk{6} = Srcs(:,:,1)==0&Srcs(:,:,2)>0&Srcs(:,:,3)>0;
    msk{7} = Srcs(:,:,1)>0&Srcs(:,:,2)>0&Srcs(:,:,3)>0;
    
    for i = 1:7
        index{i} = find(msk{i}); 
        ln(i) = len(index{i});
    end
    index{8} = find(img==0);
    ln(8) = len(index{8});
    if(min(ln(:))>100)
    
        for l = 2:4
            for i = 1:7
                mL = zeros(sz(msk{i})); mR = mL; mU = mL; mD = mL;
                mR(:,l:sz(msk{i},1)) = msk{i}(:,1:sz(msk{i},1)-l+1);
                mL(:,1:sz(msk{i},1)-l+1) = msk{i}(:,l:sz(msk{i},1));
                mU(1:sz(msk{i},1)-l+1,:) = msk{i}(l:sz(msk{i},1),:);
                mD(l:sz(msk{i},1),:) = msk{i}(1:sz(msk{i},1)-l+1,:);
                m = (msk{i}>0&mR==0)|(msk{i}>0&mL==0)|(msk{i}>0&mU==0)|(msk{i}>0&mD==0);
                indedg = find(m(index{i})>0);
                index{i}(indedg) = [];
            end
        end
        for j = 1:7
            ln(j) = len(index{j});
        end
        if(min(ln(:))>50)
            for i = 1:8
                index{i} = index{i}(randperm(sz(index{i},1))); 
            end
        else
            index = [];
        end
    else
        index = [];
    end
end
%     index{1} = ind1; index{2} = ind2; index{3} = ind3; index{4} = ind12;
%     index{5} = ind13; index{6} = ind23; index{7} = ind123; index{8} = ind0;
%     index{9} = notind1; index{10} = notind2; index{11} = notind3; index{12} = notind12;
%     index{13} = notind13; index{14} = notind23; index{15} = notind123;
%     ind2 = ind2(randperm(sz(ind2,1)));
%     ind3 = ind3(randperm(sz(ind3,1))); ind12 = ind12(randperm(sz(ind12,1)));
%     ind13 = ind13(randperm(sz(ind13,1))); ind23 = ind23(randperm(sz(ind23,1)));
%     ind123 = ind123(randperm(sz(ind123,1)));

%     notind1 = notind1(randperm(sz(notind1,1))); notind2 = notind2(randperm(sz(notind2,1)));
%     notind3 = notind3(randperm(sz(notind3,1))); notind12 = notind12(randperm(sz(notind12,1)));
%     notind13 = notind13(randperm(sz(notind13,1))); notind23 = notind23(randperm(sz(notind23,1)));
%     notind123 = notind123(randperm(sz(notind123,1)));
% 

% 
%     ind2edg = find(edge(msk2,'Canny')>0);
%     ind2 = find(msk2); ind2(ind2edg) = [];
%     ind3edg = find(edge(msk3,'Canny')>0);
%     ind3 = find(msk3); ind3(ind3edg) = [];
%     ind12edg = find(edge(msk12,'Canny')>0);
%     ind12 = find(msk12); ind12(ind12edg) = [];
%     ind13edg = find(edge(msk13,'Canny')>0);
%     ind13 = find(msk13); ind13(ind13edg) = [];
%     ind23edg = find(edge(msk23,'Canny')>0);
%     ind23 = find(msk23); ind23(ind23edg) = [];
%     ind123edg = find(edge(msk123,'Canny')>0);
%     ind123 = find(msk123); ind123(ind123edg) = [];
    
%     notind1 = (1:10000)'; notind1([ind1;ind0]) = [];
%     notind2 = (1:10000)'; notind2([ind2;ind0]) = [];
%     notind3 = (1:10000)'; notind3([ind3;ind0]) = [];
%     notind12 = (1:10000)'; notind12([ind12;ind0]) = [];
%     notind13 = (1:10000)'; notind13([ind13;ind0]) = [];
%     notind23 = (1:10000)'; notind23([ind23;ind0]) = [];
%     notind123 = (1:10000)'; notind123([ind123;ind0]) = [];
% 
    