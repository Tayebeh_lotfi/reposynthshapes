function [imv, a] = prepareLearning(ops) % A_Pri1, 
    imv1 = []; imv2 = []; imv3 = []; imv12 = []; imv13 = []; imv23 = [];
    imv123 = []; imv0 = [];

    [A_Pri1, ~, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(ops);
    while(HasOverlap ==0)
        [A_Pri1, A_Pri, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(ops);
    end
    img_ = im2double(Y_);    
    [Xc, Yc, ~]=size(img_);
    a = 1:Xc*Yc; a = reshape(a,[Xc,Yc]);
    x = mod(1:Xc*Yc,Xc);
    ind_1 = find(x==0|x==1|x==2|x==Xc-1);
    ind_2 = [ind_1,1:2*Xc+2,Xc*Yc-2*Xc-1:Xc*Yc];
    a(ind_2) = [];
    a = a';
    rad = 1; sig = 0.9; 
    iter = 1;  
    while iter <= 60
        if(ops.md==1)
            feature_ = -1*ones(Xc*Yc,25);
            feature_(a,:) = [img_(a-2*Xc-2),img_(a-2*Xc-1),img_(a-2*Xc),img_(a-2*Xc+1),img_(a-2*Xc+2),...
                img_(a-Xc-2),img_(a-Xc-1),img_(a-Xc),img_(a-Xc+1),img_(a-Xc+2),...
                img_(a-2),img_(a-1),img_(a),img_(a+1),img_(a+2),...
                img_(a+Xc-2),img_(a+Xc-1),img_(a+Xc),img_(a+Xc+1),img_(a+Xc+2),...
                img_(a+2*Xc-2),img_(a+2*Xc-1),img_(a+2*Xc),img_(a+2*Xc+1),img_(a+2*Xc+2)];
        else
            immd_ = double(reshape(MIND_descriptor2d(img_,rad,sig),Xc*Yc,[]));
            feature_ = -1*ones(Xc*Yc,136);
            feature_(a,:) = [immd_(a-2*Xc-2,:),immd_(a-2*Xc-1,:),immd_(a-2*Xc,:),...
                immd_(a-2*Xc+1,:),immd_(a-2*Xc+2,:),immd_(a-Xc-2,:),immd_(a-Xc+2,:),...
                immd_(a-2,:),immd_(a,:),immd_(a+2,:),immd_(a+Xc-2,:),...
                immd_(a+Xc+2,:),immd_(a+2*Xc-2,:),immd_(a+2*Xc-1,:),immd_(a+2*Xc,:),...
                immd_(a+2*Xc+1,:),immd_(a+2*Xc+2,:)];
        end
        %Build graph
        m_ind = find_index(X_,img_);
        if(len(m_ind))
            for i = 1:sz(m_ind,2)
                id = find(feature_(m_ind{i},1)==-1);
                m_ind{i}(id)=[];
                l(i) = len(m_ind{i});
            end
            if(min(l(:))>50)
                smpls = 30;
                imv1 = [imv1;feature_(m_ind{1}(1:smpls),:)]; imv2 = [imv2;feature_(m_ind{2}(1:smpls),:)]; 
                imv3 = [imv3;feature_(m_ind{3}(1:smpls),:)]; imv12 = [imv12;feature_(m_ind{4}(1:smpls),:)]; 
                imv13 = [imv13;feature_(m_ind{5}(1:smpls),:)]; imv23 = [imv23;feature_(m_ind{6}(1:smpls),:)]; 
                imv123 = [imv123;feature_(m_ind{7}(1:smpls),:)]; imv0 = [imv0;feature_(m_ind{8}(1:smpls),:)];
                iter = iter + 1;
            end
        end
        [A_Pri1, ~, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(ops);
        while(HasOverlap ==0)
            [A_Pri1, A_Pri, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(ops);
        end
        img_ = im2double(Y_);        
    end
    imv{1} = imv1; imv{2} = imv2; imv{3} = imv3; imv{4} = imv12; imv{5} = imv13; 
    imv{6} = imv23; imv{7} = imv123; imv{8} = imv0;
end