function Learning_Data(ops)
    [imv, a] = prepareLearning(ops);
    if(ops.md==1)
        save([ops.dir,'LearningDataMat', num2str(ops.matind),'.mat'], 'imv', 'a');
    else
        save([ops.dir,'LearningDataMndMat',num2str(ops.matind),'.mat'], 'imv', 'a');
    end
end