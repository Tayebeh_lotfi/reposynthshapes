function [seeds,Pboundary,seeds_,Oboundary] = prepare2_Cmp3Src(img,segmd)
    figure,
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];
    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3); ...
        sYi(4),sXi(4);sYi(5),sXi(5);sYi(6),sXi(6)]; % ]; % 
    seeds_ = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds = [seeds_;seeds_+prod(sz(img));seeds_+2*prod(sz(img))];
    if(segmd==1)
        labels = [ones(1,4),2,2,ones(1,10),3,3,ones(1,10),4,4];
        label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
        labels_record(labels)=1;
        labels_present=find(labels_record);
        number_labels=length(labels_present);

        Pboundary=zeros(length(seeds),number_labels);
        Pboundary(1:4,:) = repmat([1,0,0,0],[4,1]);
        Pboundary(5:6,:) = repmat([0,1,0,0],[2,1]);
        Pboundary(7:16,:) = repmat([1,0,0,0],[10,1]);
        Pboundary(17:18,:) = repmat([0,0,1,0],[2,1]);
        Pboundary(19:28,:) = repmat([1,0,0,0],[10,1]);
        Pboundary(29:30,:) = repmat([0,0,0,1],[2,1]);
    else
        labels = [ones(1,4),2,2,ones(1,10),2,2,ones(1,10),2,2];
        label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
        labels_record(labels)=1;
        labels_present=find(labels_record);
        number_labels=length(labels_present);

        Pboundary=zeros(length(seeds),number_labels);
        Pboundary(1:4,:) = repmat([1,0],[4,1]);
        Pboundary(5:6,:) = repmat([0,1],[2,1]);
        Pboundary(7:16,:) = repmat([1,0],[10,1]);
        Pboundary(17:18,:) = repmat([0,1],[2,1]);
        Pboundary(19:28,:) = repmat([1,0],[10,1]);
        Pboundary(29:30,:) = repmat([0,1],[2,1]);
    end
    labels_ = [ones(1,4),2,2,3,3,4,4];
    label_adjust_=min(labels_); labels_=labels_-label_adjust_+1; %Adjust labels to be > 0
    labels_record_(labels_)=1;
    labels_present_=find(labels_record_);
    number_labels_=length(labels_present_);

    Oboundary=zeros(length(seeds_),number_labels_);
    Oboundary(1:4,:) = repmat([1,0,0,0],[4,1]);
    Oboundary(5:6,:) = repmat([0,1,0,0],[2,1]);
    Oboundary(7:8,:) = repmat([0,0,1,0],[2,1]);
    Oboundary(9:10,:) = repmat([0,0,0,1],[2,1]);
end