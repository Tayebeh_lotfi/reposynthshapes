function [A, B, C] = Create_Sample_Data_v1(opts)

optsdef=struct('sz_smpls',100,'Obj', 1, 'valA',0.85,'valB',0.15,'valC',0.35,'ScaleA',1,'ScaleB',1,'ScaleC',1, ...
    'RecW',30,'Cir1CenY',55,'RecH',20,'Cir1CenX',60, 'Cir2CenY',60,'Cir2CenX',40,'HTri',15,'Cir3CenX',40,'Cir3CenY',20, ...
    'NoisePower',-100, 'tolerance', false, 'Scale', 10, 'scale_var', .5, 'LargeParmtr', false);
if ~exist('opts','var')
    opts=struct;
end
if(opts.tolerance == true)
    r = opts.scale_var*randn(1,10);
    r(1:3) = 1 + r(1:3) * opts.Scale; 
    if opts.LargeParmtr == false
        r(4:9) = 1 + r(4:9)/5; 
    else
        r(4:9) = 1 + r(4:9)/2;
    end
    %r(4:9) = floor(opts.Scale*r(4:9));
    opts.ScaleA = opts.ScaleA * r(1); opts.ScaleB = opts.ScaleB * r(2); opts.ScaleC = opts.ScaleC * r(3);  
    opts.Cir1CenY = ceil(opts.Cir1CenY * r(4)); opts.Cir1CenX = ceil(opts.Cir1CenX * r(5)); opts.Cir2CenY = ceil(opts.Cir2CenY * r(6)); 
    opts.Cir2CenX = ceil(opts.Cir2CenX * r(7)); opts.Cir3CenY = ceil(opts.Cir3CenY * r(8)); opts.Cir3CenX = ceil(opts.Cir3CenX * r(9));
end
ops=scanparam(optsdef,opts);
M = ops.sz_smpls; N = ops.sz_smpls;
A = double(zeros(M,N));
B = A; C = A;

rad = round(20*ops.ScaleA); 
for i = min(M-3, max(1,ops.Cir1CenY-rad)):min(M,ops.Cir1CenY+rad),
    for j = min(N-3, max(1,ops.Cir1CenX-rad)):min(N,ops.Cir1CenX+rad),
        if((i-ops.Cir1CenY)^2 + (j-ops.Cir1CenX)^2 <= rad^2),
            A(i,j) = ops.valA; % 0.15;
        end
    end
end

rad = round(20*ops.ScaleB); 
for i = min(M-3, max(1,ops.Cir2CenY-rad)):min(M,ops.Cir2CenY+rad),
    for j = min(N-3, max(1,ops.Cir2CenX-rad)):min(N,ops.Cir2CenX+rad),
        if((i-ops.Cir2CenY)^2 + (j-ops.Cir2CenX)^2 <= rad^2),
            B(i,j) = ops.valB; % 0.15;
        end
    end
end

rad = round(20*ops.ScaleC); 
for i = min(M-3, max(1,ops.Cir3CenY-rad)):min(M,ops.Cir3CenY+rad),
    for j = min(N-3, max(1,ops.Cir3CenX-rad)):min(N,ops.Cir3CenX+rad),
        if((i-ops.Cir3CenY)^2 + (j-ops.Cir3CenX)^2 <= rad^2),
            C(i,j) = ops.valC; % 0.15;
        end
    end
end

end