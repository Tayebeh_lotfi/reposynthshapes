function [A_Pri1, A_Pri, tar, src, HasOverlap,Txt,Obj] = Create_Samples6(ops) % , AllOverlap
%% Initializing, Creating Input Images:
    Obj = 1; % 10;
    ops.Src = 3;
    opt.Obj = Obj; opt.sz_smpls = 100; opt.valA = 1; opt.valB = 1; opt.valC = 1;%0.25;
    if ops.LargeParmtr == false
        opt.ScaleA = 1.8; opt.ScaleB = 1.5; opt.ScaleC = 1.5; % 1.9. 1.6
    else
        opt.ScaleA = 2.1; opt.ScaleB = 1.8; opt.ScaleC = 1.8; % 2.1, 1.9
    end
    opt.Cir1CenY = 55; opt.Cir1CenX = 35; opt.Cir2CenY = 75; opt.Cir2CenX = 70; opt.Cir3CenY = 25; opt.Cir3CenX = 65;
    opt.NoisePower = -40; % noise power in db
    opt.tolerance = true; opt.scale_var = .2; opt.Scale = .2;
    opt.LargeParmtr = ops.LargeParmtr;
    [I1, I2, I3] = Create_Sample_Data_v1(opt);
    X(:,:,1) = my_norm(I1);
    X(:,:,2) = my_norm(I2);
    X(:,:,3) = my_norm(I3);
    [Rw, Col] = size(I2);
    D1 = ([I1(:),I2(:),I3(:)]');
    MatA1 = [.4, .65, .35]; % [1, 1]; % [.3, .45]; % 
    
    A_Pri1 = MatA1';
    if(ops.FixA == false)
        A_Pri = awgn(MatA1',ops.Sigpow,'measured'); 
    else
        A_Pri = MatA1';
    end
    
%     TxrPth = '../../../ImagesTextures/';
    Txt(:,:,1) = ones(sz(X(:,:,1))); % 1.1.02
    Txt(:,:,2) = 2*ones(sz(X(:,:,1))); 
    Txt(:,:,3) = 8*ones(sz(X(:,:,1))); 
    Txt = im2double(Txt);
    %%
    spacing.height = ops.height; spacing.width = ops.width; mag = ops.mag;
    for ii = 1:ops.Src,
        [tar(:,:,ii), Tx, Ty] = rBSPwarp( X(:,:,ii), [spacing.height spacing.width], mag );
    end
    src = reshape( (A_Pri'*reshape(tar,Rw*Col,[])')',[Rw,Col,[]]);
    
    temp1 = tar > ops.thresh;
    temp2 = src(:,:,1) > ops.thresh;
    HasOverlap = sum(temp1(:)) > sum(temp2(:));
%% Check for Variation over Intensities:    
%     h = fspecial('gaussian', 5, .5);
    if(ops.isVarIntens == true)
%         tar(tar<.1) = 0;
%         ind = find(tar(:) > .1);
%         NdB	= ops.nVarIntens;
%         tar(ind) = awgn(tar(ind),-NdB); % X(ind) + ops.nVarIntensity * randn(size(ind));
%         for ii = 1:ops.Src
%             tar(:,:,ii) = imfilter(tar(:,:,ii), h);
%         end
%         tar = my_norm(tar);
        tar(tar<.1) = 0;
        if(ops.AddPtrn == true)
            tar = tar .* Txt;
        end
        src = reshape( (A_Pri'*reshape(tar,Rw*Col,[])')',[Rw,Col,[]]);
    end
end