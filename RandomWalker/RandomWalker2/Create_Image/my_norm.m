function im = my_norm(im)
    im = (im-min(im(:)))/(max(im(:))-min(im(:)) + 1e-10);
end