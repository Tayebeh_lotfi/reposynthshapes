%% Learning Phase
clc, close all, clear,
addpath(genpath('../RW'));
addpath(genpath('../Test2'));

opt.TxrPth = '../../../ImagesTextures/'; 
opt.Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
opt.Txtf2 = '1.1.03.tiff'; % '1.1.02.tiff'; % 
opt.Txtf3 = '1.1.13.tiff'; % '1.5.07.tiff'; % '1.1.10.tiff'; '1.1.06.tiff'
opt.matrix = [.4, .65, .35; .4, .2, .8; .3, .65, .8; .1,.4,.8]; 

cd Learning
opt.FixA = true; opt.Src = 2; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 75; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 20; opt.width = 20; opt.mag = 10;
% opt.dir = '1_5_3-1_1_3-1_1_6/';
% opt.dir = '1_5_3-1_1_3-1_5_7/';
opt.dir = '1_5_3-1_1_3-1_1_13/';
for i=1:2
    opt.md = i;
    for j=1:4
        opt.matind = j;
        Learning_Data(opt);
    end
end

cd ../
%% Running Algorithm
opt.TxrPth = '../../ImagesTextures/'; 
opt.dir1 = ['Learning/',opt.dir];
opt.md = 1; % Intensity values vs. mind operator feature extractor
opt.matind = 3; % mixing matrix
opt.segmd=1; % four segments vs. two segments
opt.DTrmMd = 1; % 1-Dterm vs. exp(-beta*Dterm)
opt.iteration = 1;
RwSegCmprsn3Src(opt)
