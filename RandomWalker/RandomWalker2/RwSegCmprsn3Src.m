function RwSegCmprsn3Src(ops)

    if(ops.md==1)
        load([ops.dir,'LearningDataMat', num2str(ops.matind),'.mat']);
    else
        load([ops.dir,'LearningDataMndMat', num2str(ops.matind),'.mat']);
    end
    alpha = 900; gamma = .05; beta = .95; thrsh = .05; % .1; % 
%     mode=3; % mode = 1:seeds but no priors, 2:priors but no seeds, 3:priors&seeds
    mode = [1,2,3];
    Acc = zeros(ops.iteration,sz(mode,2));
    tm = zeros(ops.iteration,sz(mode,2));
    for iter = 1:ops.iteration
%% generate data
        [A_Pri1, ~, X, Y, HasOverlap,Texture,~] = Create_Samples5(ops);
        while(HasOverlap ==0)
            [A_Pri1, A_Pri, X, Y, HasOverlap,Texture,~] = Create_Samples5(ops);
        end
        % [A_Pri1, ~, X, Y, HasOverlap,Texture,~] = Create_Samples6(ops);
        % while(HasOverlap ==0)
        %     [A_Pri1, A_Pri, X, Y, HasOverlap,Texture,~] = Create_Samples6(ops);
        % end
        %%
        img=im2double(Y);
        Srcs(:,:,:,iter) = X;
        Mixtr(:,:,iter) = img;
        [seeds,boundaryp,seeds_,boundaryo] = prepare1_Cmp3Src(img,ops.segmd);
    %     [seeds,boundaryp,seeds_,boundaryo] = prepare2_Cmp3Src(img,ops.segmd);
%         [seeds,boundaryp,seeds_,boundaryo] = prepare4_Cmp3Src(img,ops.segmd);
        [lndmrki,lndmrkj] = ind2sub(sz(img),seeds_);
%         % seedsOrg = seeds; boundaryOrg = boundaryp;
        %%
        % img = Y;
        [Xc, Yc, ~]=size(img);
        ed = Xc*Yc;
        %Build graph
        tic
            [LL,Dterm] = calculateDtermLplc(imv, a, img, X,ops.md, ops.segmd, ops.DTrmMd);
        tm1 = toc;
        DtermPre = Dterm;

        Dterm = DtermPre;

        for mdctr = 1:sz(mode,2)
            [probabilities_pro,tm2]=RandomWalkerImgSegPrior(LL,Dterm,gamma,alpha,beta,seeds(:),boundaryp,mode(mdctr),ops.DTrmMd);
            probS = probabilities_pro;
            mxv = max(probS,[],2);
            mnv = min(probS,[],2);
            probS = (probS - repmat(mnv,[1,sz(probS,2)])) ...
                ./ (repmat(mxv,[1,sz(probS,2)]) - repmat(mnv,[1,sz(probS,2)]));
            probS = probS ./ repmat(sum(probS,2),[1,sz(probS,2)]);
            %% 
            if (mode(mdctr)==3 || mode(mdctr)==2)
                [val,ind_] = max(probS,[],2);
                SE = strel('disk',1);
                if(ops.segmd==1)
                    for i = 1:sz(probS,2)-1
                        m = (ind_(1+(i-1)*ed:i*ed))== i+1; 
                        Seg(:,:,i,mdctr,iter) = imopen(reshape(m,[Xc,Yc]),SE);
%                         Seg(:,:,i,mdctr,iter) = reshape(m,[Xc,Yc]);
                    end
                else
                    for i = 1:(sz(probS,1)/(Xc*Yc))
                        m = (ind_(1+(i-1)*ed:i*ed))== 2; 
                        Seg(:,:,i,mdctr,iter) = imopen(reshape(m,[Xc,Yc]),SE);
    %                     Seg(:,:,i,mdctr) = reshape(m,[Xc,Yc]);
                    end

                end
                tm(iter,mdctr) = tm1+tm2;            
            end
            if mode(mdctr) == 1
                if(ops.segmd==1)
                    for i = 1:sz(probS,2)-1
                        Seg(:,:,i,mdctr,iter) = reshape(probS(1+(i-1)*ed:i*ed,i+1)>thrsh,[Xc,Yc]);
                    end
                else
                    for i = 1:(sz(probS,1)/(Xc*Yc))
                        Seg(:,:,i,mdctr,iter) = reshape(probS(1+(i-1)*ed:i*ed,2)>thrsh,[Xc,Yc]);
                    end
                end
                tm(iter,mdctr) = tm2;
            end
            for i = 1:sz(Seg,3)
                Acc(iter,mdctr) = Acc(iter,mdctr) + sum(sum(abs( (sqz(Srcs(:,:,i,iter))>0) - sqz(Seg(:,:,i,mdctr,iter)) )));
            end
            segerrper(iter,mdctr) = (1 - Acc(iter,mdctr) / (3*Xc*Yc))*100; % (sz(find(img~=0),1))) * 100;

        end
%         txt = {'No Data Prior', 'With Data Prior'};
%         figure,
%         for i = 1:sz(Seg,3)
%             for j = 1:sz(mode,2)
%                 subplot (3,3,(j-1)*3+i), imshow(Seg(:,:,i,j)), if(i==1), ...
%                          title([sprintf('Seg Res %s, \n Acc: %.2f', txt{j}, segerrper(iter,j)), '%']) ,end
%             end
%             subplot (3,4,i+9), imshow(X(:,:,i)>0),
%         end
%         subplot (3,4,9), imshow(img), title(sprintf('Original Img, \n Ground Truth Seg'))
    end
    txt = {'Seeds', 'Data Prior', 'Seeds + Data Prior'};
    for iter = 1:ops.iteration
        figure,
        for i = 1:sz(Seg,3)
            for j = 1:sz(mode,2)
                subplot (4,3,(j-1)*3+i), imshow(Seg(:,:,i,j,iter)), if(i==1), ...
                         title([sprintf('%s, Acc: %.2f', txt{j}, segerrper(iter,j)), '%']) ,end
            end
            subplot (4,4,i+13), imshow(Srcs(:,:,i,iter)>0),
        end
        subplot (4,4,13), imshow(img), hold on, plot(lndmrkj,lndmrki,'r*'),
        title(sprintf('Original Img, \n Ground Truth Seg'))

    end
end