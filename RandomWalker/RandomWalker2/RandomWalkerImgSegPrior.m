function [pbs,tm] = RandomWalkerImgSegPrior(L,data_term,gamma,alpha,beta,index,vals,mode,DTrmMd) % alpha = 5000;
    if mode~=2
        N=length(L);
        antiIndex=1:N;
        antiIndex(index)=[];
        %Find RHS
        b=-L(antiIndex,index)*(vals);
    end
%%
    Epsilon =1e-8;
    if mode~=1
    %     data_prior(index,:) = vals;
    %     dp = data_prior(antiIndex,:)
        if(DTrmMd == 1)
            data_prior = (1-data_term)+Epsilon;
        else
            data_prior = exp(-beta*data_term);
        end
        nlabs = size(data_prior,2);
        mn1 = min(data_prior,[],2);
        mn2 = max(data_prior,[],2);
        minmat = mn1(:,ones(1,nlabs));
        maxmat = mn2(:,ones(1,nlabs));
        data_prior = (data_prior - minmat) ./ (maxmat-minmat);
        data_prior(isnan(data_prior)) = 1;
        Prior_Mat = speye(size(L,1)); %0*L
        Prior_Vec = sum(data_prior,2);
    %     nlabs = size(data_prior,2);
        Prior_Mat(1:size(Prior_Vec,1)+1:end) = Prior_Vec;
    end
    disp('Now solves the problem w/ RW...')
%% Solve system and Build output
    tic
    if mode==1
        x=L(antiIndex,antiIndex)\b;
        pbs=zeros(size(vals));
        pbs(index,:)=vals;
        pbs(antiIndex,:)=x;
    end
    if mode==2
        x = (alpha*L + gamma*Prior_Mat)\(gamma*data_prior);
        pbs = x;
    end
    if mode==3
        x = (alpha*L(antiIndex,antiIndex) + gamma*Prior_Mat(antiIndex,antiIndex))\(gamma*data_prior(antiIndex,:)+b);
        pbs=zeros(size(vals));
        pbs(index,:)=vals;
        pbs(antiIndex,:)=x;

    end
    tm = toc;
end