function [seeds,Pboundary,seeds_,Oboundary] = prepare1_Cmp3Src(img, seedmd)
    figure,
    [sXi, sYi, ~] = impixel(img);
    sXi(end)=[]; sYi(end)=[];
    slands = [1,1;1,sz(img,2);sz(img,1),1;sz(img,1),sz(img,2); ...
        sYi(1),sXi(1);sYi(2),sXi(2);sYi(3),sXi(3)]; % ]; % 
    seeds_ = sub2ind(sz(img),slands(:,1),slands(:,2));
    seeds = [seeds_;seeds_+prod(sz(img));seeds_+2*prod(sz(img))];
    if(seedmd==1)
        labels = [ones(1,4),2,ones(1,7),3,ones(1,7),4];
        label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
        labels_record(labels)=1;
        labels_present=find(labels_record);
        number_labels=length(labels_present);

        Pboundary=zeros(length(seeds),number_labels);
        Pboundary(1:4,:) = repmat([1,0,0,0],[4,1]);
        Pboundary(5,:) = [0,1,0,0];
        Pboundary(6:12,:) = repmat([1,0,0,0],[7,1]);
        Pboundary(13,:) = [0,0,1,0];
        Pboundary(14:20,:) = repmat([1,0,0,0],[7,1]);
        Pboundary(21,:) = [0,0,0,1];
    else
        labels = [ones(1,4),2,ones(1,7),2,ones(1,7),2];
        label_adjust=min(labels); labels=labels-label_adjust+1; %Adjust labels to be > 0
        labels_record(labels)=1;
        labels_present=find(labels_record);
        number_labels=length(labels_present);

        Pboundary=zeros(length(seeds),number_labels);
        Pboundary(1:4,:) = repmat([1,0],[4,1]);
        Pboundary(5,:) = [0,1];
        Pboundary(6:12,:) = repmat([1,0],[7,1]);
        Pboundary(13,:) = [0,1];
        Pboundary(14:20,:) = repmat([1,0],[7,1]);
        Pboundary(21,:) = [0,1];
    end
    %%
    labels_ = [ones(1,4),2,3,4];
    label_adjust_=min(labels_); labels_=labels_-label_adjust_+1; %Adjust labels to be > 0
    labels_record_(labels_)=1;
    labels_present_=find(labels_record_);
    number_labels_=length(labels_present_);

    Oboundary=zeros(length(seeds_),number_labels_);
    Oboundary(1:4,:) = repmat([1,0,0,0],[4,1]);
    Oboundary(5,:) = [0,1,0,0];
    Oboundary(6,:) = [0,0,1,0];
    Oboundary(7,:) = [0,0,0,1];
end