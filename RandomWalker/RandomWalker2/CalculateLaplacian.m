function [LL, DPr] = CalculateLaplacian(feature, img, imv, Srcs, seedmd, DTrmMd)
    %% Create distances
    strmthd = {'euclidean','seuclidean','squaredeuclidean','cityblock',...
        'correlation','spearman','cosine','chebychev','hamming','jaccard','mahalanobis','minkowski'};
    P = {[],nanstd(feature),[],[],[],[],[],[],[],[],nancov(feature),1.2};
    mtd = 2;
    for ii = 1:8
        if(find ([2,11,12] == mtd))
            D_{ii} = (pdist2(feature,imv{ii},strmthd{mtd},P{mtd}));
        else
            D_{ii} = (pdist2(feature,imv{ii},strmthd{mtd}));
        end
        tmp = D_{ii};
        D{ii} = sort(tmp,2);
    end
    certhr1 = 0.001; %certhr2 = .1;
%     M = cell{7};
    for ii = 1:7
% %     %     thr1 = quantile(D{i},[certhr1,certhr2],2); 
% %     %     Sel = ( D{i} > thr1(:,1) & D{i} < thr1(:,2));
%         thr1 = quantile(D{ii},certhr1,2);
%         Sel = ( D{ii} < thr1); % (:,1) & D{i} < thr1(:,2));
%         DD = D{ii} .* Sel;
%         M{ii} = sum(DD,2) ./ sum(Sel,2);
        M{ii} = min(D{ii},[],2); % (:,1);
        mx(ii) = max(M{ii});
    end
    mxMat = max(mx(:))*ones(sz(M{1}));

    [Xc, Yc, ~]=size(img); ed = Xc*Yc;
    Mat = [M{1},M{2},M{3},M{4},M{5},M{6},M{7}];
    [~,ind] = min(Mat,[],2);
    ind0 = find(img==0); ind(ind0) = 0;

    if(DTrmMd==1)
        id1 = find(ind==1|ind==4|ind==5|ind==7);
        id2 = find(ind==2|ind==4|ind==6|ind==7);
        id3 = find(ind==3|ind==5|ind==6|ind==7);
        if(seedmd==1)
            DPr = ones(3*ed,4);
            DPr(id1,2) = 0; DPr(ed+id2,3) = 0; DPr(2*ed+id3,4) = 0;
            DPr(ind0,1) = 0; DPr(ed+ind0,1) = 0; DPr(2*ed+ind0,1) = 0;
        else
            DPr = ones(3*ed,2);
            DPr(id1,2) = 0; DPr(ed+id2,2) = 0; DPr(2*ed+id3,2) = 0;
            DPr(ind0,1) = 0; DPr(ed+ind0,1) = 0; DPr(2*ed+ind0,1) = 0;
        end
        DPr(find(ind==2),1)=0;DPr(find(ind==3),1)=0; DPr(find(ind==6),1)=0;
        DPr(ed+find((ind==1)),1)=0;DPr((ed+find(ind==3)),1)=0; DPr((ed+find(ind==5)),1)=0;
        DPr(2*ed+find((ind==1)),1)=0;DPr((2*ed+find(ind==2)),1)=0; DPr((2*ed+find(ind==4)),1)=0;
    else    
        if(seedmd==1)
            DPr = [min(min(M{2},M{3}),M{6}), min(M{1},min(M{4},min(M{5},M{7}))), mxMat, mxMat;...
            min(min(M{1},M{3}),M{5}), mxMat, min(M{2},min(M{4},min(M{6},M{7}))), mxMat;...
            min(min(M{1},M{2}),M{4}), mxMat, mxMat, min(M{3},min(M{5},min(M{6},M{7})))];
        else
            DPr = [min(min(M{2},M{3}),M{6}), min(M{1},min(M{4},min(M{5},M{7})));...
            min(min(M{1},M{3}),M{5}), min(M{2},min(M{4},min(M{6},M{7})));...
            min(min(M{1},M{2}),M{4}), min(M{3},min(M{5},min(M{6},M{7})))];
        end   
%%
        DPr(ind0,2) = mxMat(ind0);
        if(seedmd==1)
            DPr(ed+ind0,3) = mxMat(ind0); DPr(2*ed+ind0,4) = mxMat(ind0);
        else
            DPr(ed+ind0,2) = mxMat(ind0); DPr(2*ed+ind0,2) = mxMat(ind0);
        end
    end
    %% Build graph
    [points, edges]=lattice(Xc,Yc,1);
    beta = 90; 
    imgVals=img(:); 
    weights=makeweights(edges,imgVals,beta);
    L=laplacian(edges,weights);
    LL = [L,zeros(sz(L)),zeros(sz(L));zeros(sz(L)),L,zeros(sz(L));zeros(sz(L)),zeros(sz(L)),L];
end    
%%
%     DPr = [min(min(M{2},M{3}),M{6}), min(M{1},min(M{4},min(M{5},M{7}))), M{2}, M{3};...
%     min(min(M{1},M{3}),M{5}), M{1}, min(M{2},min(M{4},min(M{6},M{7}))), M{3};...
%     min(min(M{1},M{2}),M{4}), M{1}, M{2}, min(M{3},min(M{5},min(M{6},M{7})))];
%     DPr = [min(min(M{2},M{3}),M{6}), M{1}, mxMat, mxMat;...
%     min(min(M{1},M{3}),M{5}), mxMat, M{2}, mxMat;...
%     min(min(M{1},M{2}),M{4}), mxMat, mxMat, M{3}];
