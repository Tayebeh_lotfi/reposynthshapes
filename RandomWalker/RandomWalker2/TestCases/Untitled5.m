[Xc,Yc] = size(img);
[points, edges]=lattice(Xc,Yc,1);
imgVals=img(:); beta = 90;
weights=makeweights(edges,imgVals,beta);
L=laplacian(edges,weights);
L_ = full(L); 
id = find(L_~=0);
vl = min(abs(L_(id)));
L12 = zeros(sz(L_)); L13 = L12; L23 = L12;
alpha = 1e-3;
% vl = 0;
L12(id) = -1*vl; L13(id) = -1*vl; L23(id) = -1*vl;
ed = Xc*Yc; step = ed+1;
%% Build laplacian matrix
L2 = L;
L(1:step:end) = 0;
LL = [L,L12,L13;L12,L,L23;L13,L23,L];
LL(1:3*Xc*Yc+1:end) = -sum(LL,1);
LL = sparse(LL);
LL2 = [L2,zeros(sz(L2)),zeros(sz(L2));zeros(sz(L2)),L2,zeros(sz(L2));zeros(sz(L2)),zeros(sz(L2)),L2];


figure, 
for i = 1:10:200
    subplot 121, plot(D1(ind1(i),1:50), '.-'), hold on
    subplot 122, plot(D1(ind2(i),1:50), '.-'), hold on   
end



%%

D{1} = D1; D{2} = D2; D{3} = D3; D{4} = D12; D{5} = D13; D{6} = D23; D{7} = D123;
Id{1} = ind1; Id{2} = ind2; Id{3} = ind3; Id{4} = ind12; Id{5} = ind13; Id{6} = ind23; Id{7} = ind123;
certhr1 = .25;
certhr2 = .75;
for i = 1:7
    for j = 1:7
        for s = 1:sz(Id{j},1) % 100 % sz(ind1,1)
            S{j} = D{i}(Id{j}(s),:);
            thres1 = quantile(S{j},certhr1);   
            thres2 = quantile(S{j},certhr2);
            indS{j} =find( S{j} > thres1 & S{j} < thres2);  
            V{i}{j}(s) = var(D{i}(Id{j}(s),indS{j}),[],2);
            m{i}{j}(s) = mean(D{i}(Id{j}(s),indS{j}),2);
        end
        
    end
end

clr = {'r','g','b','c','y','m','k'};
for j = 1:3 % 7 for indexes of segments 1,2&3 what is the variance of distances to the known segments 1-7
    figure,
    for i = 1:7
        M = V{i}{j};
%         if(len(M)~=0)
%             plot(sort(M),'g-.'), hold on,
%         else
            plot(M(1:20) ,'Color',clr{i},'LineStyle','-.'), hold on, % % M(1:20)
%         end
    end
    title(['Distance', num2str(j)]),
    legend('D1','D2','D3','D12','D13','D23','D123','Location','southeast','Orientation','vertical');
end

%%
D{1} = D1; D{2} = D2; D{3} = D3; D{4} = D12; D{5} = D13; D{6} = D23; D{7} = D123;
Id{1} = ind1; Id{2} = ind2; Id{3} = ind3; Id{4} = ind12; Id{5} = ind13; Id{6} = ind23; Id{7} = ind123;

clr = {'r','g','b','c','y','m','k'};
i=1;
certhr1 = 0.05; certhr2 = .1;
for i = 1:7
%     thr1 = quantile(D{i},[certhr1,certhr2],2); 
%     Sel = ( D{i} > thr1(:,1) & D{i} < thr1(:,2));
    thr1 = quantile(D{i},certhr1,2);
    Sel = ( D{i} < thr1); % (:,1) & D{i} < thr1(:,2));
    DD = D{i} .* Sel;
    M{i} = sum(DD,2) ./ sum(Sel,2);
end
Distances = {' D1',' D2',' D3',' D12',' D13',' D23',' D123'};
for i = 1:7
    figure,
    for j = 1:7
        plot(M{i}(Id{j}) ,'Color',clr{j},'LineStyle','-.'), hold on, % % M(1:20)
    end
    title(['Distance', Distances{i}]),
    legend('Id1','Id2','Id3','Id12','Id13','Id23','Id123','Location','southeast'); % ,'Orientation','vertical');
end

Id = index;
clr = {'r','g','b','c','y','m','k'};
Indexes = {' Id1',' Id2',' Id3',' Id12',' Id13',' Id23',' Id123'};
for i = 1:7
    figure,
    for j = 1:7
        plot(M{j}(Id{i}) ,'Color',clr{j},'LineStyle','-.'), hold on, % % M(1:20)
    end
    title(['Index ', Indexes{i}]),
    legend('D1','D2','D3','D12','D13','D23','D123','Location','southeast'); % ,'Orientation','vertical');
end

% jj=3;
% [M{1}(Id{jj}(1:10)),M{2}(Id{jj}(1:10)),M{3}(Id{jj}(1:10))]
D{1} = D1; D{2} = D2; D{3} = D3; D{4} = D12; D{5} = D13; D{6} = D23; D{7} = D123;
Id{1} = ind1; Id{2} = ind2; Id{3} = ind3; Id{4} = ind12; Id{5} = ind13; Id{6} = ind23; Id{7} = ind123;

clr = {'r','g','b','c','y','m','k'};
i=1;
certhr1 = 0.75; certhr2 = .1;
for i = 1:7
%     thr1 = quantile(D{i},[certhr1,certhr2],2); 
%     Sel = ( D{i} > thr1(:,1) & D{i} < thr1(:,2));
    thr1 = quantile(D{i},certhr1,2);
    Sel = ( D{i} < thr1); % (:,1) & D{i} < thr1(:,2));
    DD = D{i} .* Sel;
    M{i} = sum(DD,2) ./ sum(Sel,2);
end

Mat = [M{1},M{2},M{3},M{4},M{5},M{6},M{7}];
[val,ind] = min(Mat,[],2);
ind0 = find(img==0);
ind(ind0) = 0;
[Xc,Yc] = size(img);
figure, 
subplot 131, imshow(reshape(ind==1|ind==4|ind==5|ind==7,[Xc,Yc]),[])
subplot 132, imshow(reshape(ind==2|ind==4|ind==6|ind==7,[Xc,Yc]),[])
subplot 133, imshow(reshape(ind==3|ind==5|ind==6|ind==7,[Xc,Yc]),[])

id1 = find(ind==1|ind==4|ind==5|ind==7);
id2 = find(ind==2|ind==4|ind==6|ind==7);
id3 = find(ind==3|ind==5|ind==6|ind==7);

[Xc, Yc, ~]=size(img);
ed = Xc*Yc; ind = find(img==0); 
Dpr = ones(3*ed,4);
Dpr(id1,2) = 0; Dpr(ed+id2,3) = 0; Dpr(2*ed+id3,4) = 0;

figure,
subplot 131, imshow(reshape(1-DPr(1:ed,2),[Xc,Yc]))
subplot 132, imshow(reshape(1-DPr(ed+1:2*ed,3),[Xc,Yc]))
subplot 133, imshow(reshape(1-DPr(2*ed+1:3*ed,4),[Xc,Yc]))


Mat(aa(1),:)


PQ = cell(7);
for j = 1:7
%     PQ(:,j) = ( (D{j}(:,1))+(D{j}(:,2)) )/2;
    PQ{j} = D{j}(:,1);
%     PQ{j} = tmp;
end
st{j} = min(D{j},[],2);
MPQ = [PQ{1},PQ{2},PQ{3},PQ{4},PQ{5},PQ{6},PQ{7}];
[val,ind] = min(MPQ,[],2);
ind0 = find(img==0);
ind(ind0) = 0;
[Xc,Yc] = size(img);
figure, imshow(reshape(ind,[Xc,Yc]),[])



Data_Prior = [min(min(M{2},M{3}),M{6}), min(M{1},min(M{4},min(M{5},M{7}))), zeros(sz(M{1})), zeros(sz(M{1}));...
    min(min(M{1},M{3}),M{5}), zeros(sz(M{2})), min(M{2},min(M{4},min(M{6},M{7}))), zeros(sz(M{2}));...
    min(min(M{1},M{2}),M{4}), zeros(sz(M{3})), zeros(sz(M{3})), min(M{3},min(M{5},min(M{6},M{7})))];


for i = 1:5
    figure,
    for j = 1:5
        plot(D{i}(Id{j}(1:200),1) ,'Color',clr{j},'LineStyle','-.'), hold on, % % M(1:20)
    end
    title(['Distance', Distances{i}]),
    legend('Id1','Id2','Id3','Id12','Id13','Id23','Id123','Location','southeast'); % ,'Orientation','vertical');
end



[Xc, Yc, ~]=size(img);
%% Build graph
[points, edges]=lattice(Xc,Yc,1);
for i = 1:7
    Ind = [];
    for n = 1:sz(Id{i})
        ii = find(edges(:,1)==Id{i}(n) | edges(:,2)==Id{i}(n));
        Ind = [Ind;ii];
    end
    IndEdg{i} = Ind;
end
% clear D; D = M;
clr = {'r*','g*','b*','c*','y*','m*','k*'};
Indexes = {' Id1',' Id2',' Id3',' Id12',' Id13',' Id23',' Id123'};
Distances = {' D1',' D2',' D3',' D12',' D13',' D23',' D123'};
stp = 1; % sz(D{1},2);
for i = 1:7
    figure
    for j = 1:7
        plot(D{i}(edges(IndEdg{j},1),stp),D{i}(edges(IndEdg{j},2),stp),clr{j}), xlim([0,2]),ylim([0,2]),hold on
    end
    title(['Distance', Distances{i}]),
    legend('Id1','Id2','Id3','Id12','Id13','Id23','Id123','Location','southeast','Orientation','vertical');
%     title(['Index', Indexes{j}]),
%     legend('D1','D2','D3','D12','D13','D23','D123','Location','southeast','Orientation','vertical');
end
%%
% subplot 131, plot(D{1}(edges(IndEdg{1},1),1),D{1}(edges(IndEdg{1},2),1),'r*'), xlim([0,2]),ylim([0,2]),hold on
% subplot 132, plot(D{2}(edges(IndEdg{2},1),1),D{2}(edges(IndEdg{2},2),1),'b*'), xlim([0,2]),ylim([0,2]),hold on
% subplot 133, plot(D{3}(edges(IndEdg{3},1),1),D{3}(edges(IndEdg{3},2),1),'g*'), xlim([0,2]),ylim([0,2]),hold on
%             S2 = D3(ind2(s),:);
%             thres1 = quantile(S2,certhr1);   
%             thres2 = quantile(S2,certhr2);
%             indS2 =find( S2 > thres1 & S2 < thres2);  
%             S3 = D3(ind3(s),:);
%             thres1 = quantile(S3,certhr1);   
%             thres2 = quantile(S3,certhr2);
%             indS3 =find( S3 > thres1 & S3 < thres2);  
%             S12 = D3(ind12(s),:);
%             thres1 = quantile(S12,certhr1);   
%             thres2 = quantile(S12,certhr2);
%             indS12 =find( S12 > thres1 & S12 < thres2); 
%             S13 = D3(ind13(s),:);
%             thres1 = quantile(S13,certhr1);   
%             thres2 = quantile(S13,certhr2);
%             indS13 =find( S13 > thres1 & S13 < thres2); 
%             S23 = D3(ind23(s),:);
%             thres1 = quantile(S23,certhr1);   
%             thres2 = quantile(S23,certhr2);
%             indS23 =find( S23 > thres1 & S23 < thres2); 

%             V3_2(s) = var(D3(ind2(s),indS2),[],2);
%             V3_3(s) = var(D3(ind3(s),indS3),[],2);
%             V3_12(s) = var(D3(ind12(s),indS12),[],2);
%             V3_13(s) = var(D3(ind13(s),indS13),[],2);
%             V3_23(s) = var(D3(ind23(s),indS23),[],2);

% certhr1 = .1;
% certhr2 = .9;
% for s = 1:100 % sz(ind1,1)
%     S1 = D1(ind1(s),:);
%     thres1 = quantile(S1,certhr1);   
%     thres2 = quantile(S1,certhr2);
%     indS1 =find( S1 > thres1 & S1 < thres2);  
%     S2 = D1(ind2(s),:);
%     thres1 = quantile(S2,certhr1);   
%     thres2 = quantile(S2,certhr2);
%     indS2 =find( S2 > thres1 & S2 < thres2);  
%     S3 = D1(ind3(s),:);
%     thres1 = quantile(S3,certhr1);   
%     thres2 = quantile(S3,certhr2);
%     indS3 =find( S3 > thres1 & S3 < thres2);  
%     S12 = D1(ind12(s),:);
%     thres1 = quantile(S12,certhr1);   
%     thres2 = quantile(S12,certhr2);
%     indS12 =find( S12 > thres1 & S12 < thres2); 
%     S13 = D1(ind13(s),:);
%     thres1 = quantile(S13,certhr1);   
%     thres2 = quantile(S13,certhr2);
%     indS13 =find( S13 > thres1 & S13 < thres2); 
%     S23 = D1(ind23(s),:);
%     thres1 = quantile(S23,certhr1);   
%     thres2 = quantile(S23,certhr2);
%     indS23 =find( S23 > thres1 & S23 < thres2); 
%     V1_1(s) = var(D1(ind1(s),indS1),[],2);
%     V1_2(s) = var(D1(ind2(s),indS2),[],2);
%     V1_3(s) = var(D1(ind3(s),indS3),[],2);
%     V1_12(s) = var(D1(ind12(s),indS12),[],2);
%     V1_13(s) = var(D1(ind13(s),indS13),[],2);
%     V1_23(s) = var(D1(ind23(s),indS23),[],2);
% end
% % figure,
% % plot(sort(V1_1),'.-b'), hold on, 
% % plot(sort(V1_2),'.-r'), hold on,
% % plot(sort(V1_3),'.-g'), hold on,
% 
% for s = 1:100 % sz(ind1,1)
%     S1 = D2(ind1(s),:);
%     thres1 = quantile(S1,certhr1);   
%     thres2 = quantile(S1,certhr2);
%     indS1 =find( S1 > thres1 & S1 < thres2);  
%     S2 = D2(ind2(s),:);
%     thres1 = quantile(S2,certhr1);   
%     thres2 = quantile(S2,certhr2);
%     indS2 =find( S2 > thres1 & S2 < thres2);  
%     S3 = D2(ind3(s),:);
%     thres1 = quantile(S3,certhr1);   
%     thres2 = quantile(S3,certhr2);
%     indS3 =find( S3 > thres1 & S3 < thres2);  
%     S12 = D2(ind12(s),:);
%     thres1 = quantile(S12,certhr1);   
%     thres2 = quantile(S12,certhr2);
%     indS12 =find( S12 > thres1 & S12 < thres2); 
%     S13 = D2(ind13(s),:);
%     thres1 = quantile(S13,certhr1);   
%     thres2 = quantile(S13,certhr2);
%     indS13 =find( S13 > thres1 & S13 < thres2); 
%     S23 = D2(ind23(s),:);
%     thres1 = quantile(S23,certhr1);   
%     thres2 = quantile(S23,certhr2);
%     indS23 =find( S23 > thres1 & S23 < thres2); 
%     V2_1(s) = var(D2(ind1(s),indS1),[],2);
%     V2_2(s) = var(D2(ind2(s),indS2),[],2);
%     V2_3(s) = var(D2(ind3(s),indS3),[],2);
%     V2_12(s) = var(D2(ind12(s),indS12),[],2);
%     V2_13(s) = var(D2(ind13(s),indS13),[],2);
%     V2_23(s) = var(D2(ind23(s),indS23),[],2);
% end
% % figure,
% % plot(sort(V2_1),'.-b'), hold on, 
% % plot(sort(V2_2),'.-r'), hold on,
% % plot(sort(V2_3),'.-g'), hold on,
% 
% for s = 1:100 % sz(ind1,1)
%     S1 = D3(ind1(s),:);
%     thres1 = quantile(S1,certhr1);   
%     thres2 = quantile(S1,certhr2);
%     indS1 =find( S1 > thres1 & S1 < thres2);  
%     S2 = D3(ind2(s),:);
%     thres1 = quantile(S2,certhr1);   
%     thres2 = quantile(S2,certhr2);
%     indS2 =find( S2 > thres1 & S2 < thres2);  
%     S3 = D3(ind3(s),:);
%     thres1 = quantile(S3,certhr1);   
%     thres2 = quantile(S3,certhr2);
%     indS3 =find( S3 > thres1 & S3 < thres2);  
%     S12 = D3(ind12(s),:);
%     thres1 = quantile(S12,certhr1);   
%     thres2 = quantile(S12,certhr2);
%     indS12 =find( S12 > thres1 & S12 < thres2); 
%     S13 = D3(ind13(s),:);
%     thres1 = quantile(S13,certhr1);   
%     thres2 = quantile(S13,certhr2);
%     indS13 =find( S13 > thres1 & S13 < thres2); 
%     S23 = D3(ind23(s),:);
%     thres1 = quantile(S23,certhr1);   
%     thres2 = quantile(S23,certhr2);
%     indS23 =find( S23 > thres1 & S23 < thres2); 
%     V3_1(s) = var(D3(ind1(s),indS1),[],2);
%     V3_2(s) = var(D3(ind2(s),indS2),[],2);
%     V3_3(s) = var(D3(ind3(s),indS3),[],2);
%     V3_12(s) = var(D3(ind12(s),indS12),[],2);
%     V3_13(s) = var(D3(ind13(s),indS13),[],2);
%     V3_23(s) = var(D3(ind23(s),indS23),[],2);
% end
% % figure,
% % plot(sort(V3_1),'.-b'), hold on, 
% % plot(sort(V3_2),'.-r'), hold on,
% % plot(sort(V3_3),'.-g'), hold on,
% 
% % S1 = D1(ind1,:);
% % thresh1 = quantile(S1,certhr1,2);
% % thresh2 = quantile(S1,certhr2,2);
% % indS1 = find(S1 > thresh1);
% 
% % figure, 
% % plot(sort(V1_1),'.-r'), hold on,
% % plot(sort(V2_1),'.-g'), hold on,
% % plot(sort(V3_1),'.-b'), hold on,
% % 
% % figure, 
% % plot(sort(V1_3),'.-r'), hold on,
% % plot(sort(V2_3),'.-g'), hold on,
% % plot(sort(V3_3),'.-b'), hold on,
