iter = 0;
ed = Xc*Yc;
grdmsk1 = X(:,:,1)>0; grdmsk2 = X(:,:,2)>0; grdmsk3 = X(:,:,3)>0; 

[val, msk] = calc_max3Src(probS, img); 
msk1 = reshape(msk(1:ed),[Xc,Yc]);
msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);
msk1(msk1==1) = 0;
msk1(msk1~=0) = 1;
msk2(msk2==1) = 0;
msk2(msk2~=0) = 1;
msk3(msk3==1) = 0;
msk3(msk3~=0) = 1;
seg_acc(iter+1) = 100 - (sz(find(grdmsk1~=msk1),1) + sz(find(grdmsk2~=msk2),1) + sz(find(grdmsk3~=msk3),1)) ./ (3*Xc*Yc)*100;

[msk1_,msk2_,msk3_] = calc_max3Src_RW(probS_, img); 
figure,
subplot 331, imshow(msk1)
subplot 332, imshow(msk2)
subplot 333, imshow(msk3)
subplot 334, imshow(msk1_)
subplot 335, imshow(msk2_)
subplot 336, imshow(msk3_)
subplot 337, imshow(grdmsk1)
subplot 338, imshow(grdmsk2)
subplot 339, imshow(grdmsk3)

seg_acc_(iter+1) = 100 - (sz(find(grdmsk1~=msk1_),1) + sz(find(grdmsk2~=msk2_),1) + sz(find(grdmsk3~=msk3_),1)) ./ (3*Xc*Yc)*100;
[seg_acc, seg_acc_]

figure, mesh(reshape(probS_(:,2),[Xc,Yc]))
ed = Xc*Yc;
start = 2*ed+1; stop = 3*ed;
figure,
subplot 141, imshow(reshape(probS(start:stop,1),[Xc,Yc]),[])
subplot 142, imshow(reshape(probS(start:stop,2),[Xc,Yc]),[])
subplot 143, imshow(reshape(probS(start:stop,3),[Xc,Yc]),[])
subplot 144, imshow(reshape(probS(start:stop,4),[Xc,Yc]),[])

%%
iter = 0;
ed = Xc*Yc;
msk = calc_mask3Src(probS, img); 
msk1 = reshape(msk(1:ed),[Xc,Yc]);
msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);

grdmsk1 = X(:,:,1)>0; grdmsk2 = X(:,:,2)>0; grdmsk3 = X(:,:,3)>0; 
seg_acc(iter+1) = 100 - (sz(find(grdmsk1~=msk1),1) + sz(find(grdmsk2~=msk2),1) + sz(find(grdmsk3~=msk3),1)) ./ (3*Xc*Yc)*100;
ovlp = (sz(find(grdmsk1&grdmsk2),1)+sz(find(grdmsk1&grdmsk3),1)+...
    sz(find(grdmsk2&grdmsk3),1)-sz(find(grdmsk1&grdmsk2&grdmsk3),1));

[val_, msk_] = max(probS_,[],2);
msk1_ = reshape(msk_==2,[Xc,Yc]);
msk2_ = reshape(msk_==3,[Xc,Yc]);
msk3_ = reshape(msk_==4,[Xc,Yc]);
seg_acc_(iter+1) = 100 - (sz(find(grdmsk1~=msk1_),1) + sz(find(grdmsk2~=msk2_),1) + sz(find(grdmsk3~=msk3_),1)) ./ (3*Xc*Yc)*100;

[msk1_,msk2_,msk3_] = calc_max3Src_RW(probS_, img); 
figure,
subplot 331, imshow(msk1)
subplot 332, imshow(msk2)
subplot 333, imshow(msk3)
subplot 334, imshow(msk1_)
subplot 335, imshow(msk2_)
subplot 336, imshow(msk3_)
subplot 337, imshow(grdmsk1)
subplot 338, imshow(grdmsk2)
subplot 339, imshow(grdmsk3)

seg_acc_(iter+1) = 100 - (sz(find(grdmsk1~=msk1_),1) + sz(find(grdmsk2~=msk2_),1) + sz(find(grdmsk3~=msk3_),1)) ./ (3*Xc*Yc)*100;
[seg_acc, seg_acc_]



%%
[~, mymsk] = max(probS,[],2);
mask1 = reshape(mymsk(1:ed),[Xc,Yc]);
mask2 = reshape(mymsk(ed+1:2*ed),[Xc,Yc]);
mask3 = reshape(mymsk(2*ed+1:3*ed),[Xc,Yc]);
mask1(mask1==1) = 0;
mask1(mask1~=0) = 1;
mask2(mask2==1) = 0;
mask2(mask2~=0) = 1;
mask3(mask3==1) = 0;
mask3(mask3~=0) = 1;
seg_accMax(iter+1) = 100 - (sz(find(grdmsk1~=mask1),1) + sz(find(grdmsk2~=mask2),1) + sz(find(grdmsk3~=mask3),1)) ./ (3*Xc*Yc)*100;

[val_, msk_] = max(probS_,[],2);
mask1_ = reshape(msk_==2,[Xc,Yc]);
mask2_ = reshape(msk_==3,[Xc,Yc]);
mask3_ = reshape(msk_==4,[Xc,Yc]);
seg_acc_Max(iter+1) = 100 - (sz(find(grdmsk1~=mask1_),1) + sz(find(grdmsk2~=mask2_),1) + sz(find(grdmsk3~=mask3_),1)) ./ (3*Xc*Yc)*100;

figure, 
subplot 331, imshow(mask1,[])
subplot 332, imshow(mask2,[])
subplot 333, imshow(mask3,[])
subplot 334, imshow(mask1_,[])
subplot 335, imshow(mask2_,[])
subplot 336, imshow(mask3_,[])
subplot 337, imshow(grdmsk1)
subplot 338, imshow(grdmsk2)
subplot 339, imshow(grdmsk3)

[seg_accMax,seg_acc_Max]

%%
prob1 = probS(1:ed,:);
prob2 = probS(ed+1:2*ed,:);
prob3 = probS(2*ed+1:3*ed,:);
thresh =.1;
idx1 = find(prob1(:,2)> thresh);
idx2 = find(prob2(:,3)> thresh);
idx3 = find(prob3(:,4)> thresh);
newmask1 = zeros(sz(img)); newmask2 = newmask1; newmask3 = newmask2;
newmask1(idx1) = 1; newmask2(idx2) = 1; newmask3(idx3) = 1;
figure,
subplot 231, imshow(newmask1),
subplot 232, imshow(newmask2),
subplot 233, imshow(newmask3)
subplot 234, imshow(grdmsk1)
subplot 235, imshow(grdmsk2)
subplot 236, imshow(grdmsk3)

seg_acc_Max(iter+1) = 100 - (sz(find(grdmsk1~=newmask1),1) + sz(find(grdmsk2~=newmask2),1) + sz(find(grdmsk3~=newmask3),1)) ./ (3*Xc*Yc)*100;



[mask1_,mask2_] = calc_max2Src_RW(probS_,img);
figure,
subplot 321, imshow(mask1_)
subplot 322, imshow(mask2_)
subplot 323, imshow(msk1)
subplot 324, imshow(msk2)
subplot 325, imshow(grdmsk1)
subplot 326, imshow(grdmsk2)


seg_accRW(iter+1) = 100 - (sz(find(grdmsk1~=mask1_),1) + sz(find(grdmsk2~=mask2_),1)) ./ (2*Xc*Yc)*100;

%% New

msk = calc_mask3Src(probS,img);
msk1 = reshape(msk(1:ed),[Xc,Yc]);
msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);
idx1 = find(msk1==1);
idx2 = find(msk2==1);
idx3 = find(msk3==1);
img1 = zeros(sz(img)); img2 = img1; img3 = img1;
img1(idx1) = img(idx1) .* ( probS(idx1,2) ./ (probS(idx1,2) + probS(ed+idx1,3) + probS(2*ed+idx1,4)) );
img2(idx2) = img(idx2) .* ( probS(ed+idx2,3) ./ (probS(idx2,2) + probS(ed+idx2,3) + probS(2*ed+idx2,4)) );
img3(idx3) = img(idx3) .* ( probS(2*ed+idx3,4) ./ (probS(idx3,2) + probS(ed+idx3,3) + probS(2*ed+idx3,4)) );

figure, 
subplot 131, imshow(img1)
subplot 132, imshow(img2)
subplot 133, imshow(img3)


%%
imgmsk = img>0;
epsilon = 0;
prob1 = probS(1:ed,:);
prob2 = probS(ed+1:2*ed);
prob3 = probS(2*ed+1:3*ed);
prob1(:,3:end) = [];
prob2(:,2) = [];
prob2(:,end) = [];
prob3(:,2:3) = [];

u1 = prob1 .* log(prob1+epsilon);
% u1(isnan(u1)) = 0;
unc1 = reshape(-1/2*sum(u1,2),[Xc,Yc]);
figure, imshow(unc1 .* imgmsk)
% unc1 = reshape(unc(1:ed),[Xc,Yc]);
unc2 = reshape(unc(ed+1:2*ed),[Xc,Yc]);
unc3 = reshape(unc(2*ed+1:3*ed),[Xc,Yc]);

figure,
imshow(unc1)
%%
beta = .950; ed = Xc*Yc;

data_prior = exp(-beta*Dterm); % Dterm; % 
nlabs = size(data_prior,2);
mn1 = min(data_prior,[],2);
mn2 = max(data_prior,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
data_prior = (data_prior - minmat) ./ (maxmat-minmat);
% data_prior(isnan(data_prior)) = 1;
% data_prior = data_prior ./ repmat(sum(data_prior,2),[1,sz(data_prior,2)]);

figure,
subplot 321, imshow(reshape(data_prior(1:ed,1),[Xc,Yc]),[]);
subplot 322, imshow(reshape(data_prior(1:ed,2),[Xc,Yc]),[]);
subplot 323, imshow(reshape(data_prior(ed+1:2*ed,1),[Xc,Yc]),[]);
subplot 324, imshow(reshape(data_prior(ed+1:2*ed,2),[Xc,Yc]),[]);
subplot 325, imshow(reshape(data_prior(2*ed+1:3*ed,1),[Xc,Yc]),[]);
subplot 326, imshow(reshape(data_prior(2*ed+1:3*ed,2),[Xc,Yc]),[]);
%%
beta = .950; ed = Xc*Yc;

data_prior = exp(-beta*Dterm); % Dterm; % 
nlabs = size(data_prior,2);
mn1 = min(data_prior,[],2);
mn2 = max(data_prior,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
data_prior = (data_prior - minmat) ./ (maxmat-minmat);
data_prior(isnan(data_prior)) = 1;
% data_prior = data_prior ./ repmat(sum(data_prior,2),[1,sz(data_prior,2)]);


figure,
subplot 341, imshow(reshape(data_prior(1:ed,1),[Xc,Yc]),[]);
subplot 342, imshow(reshape(data_prior(1:ed,2),[Xc,Yc]),[]);
subplot 343, imshow(reshape(data_prior(1:ed,3),[Xc,Yc]),[]);
subplot 344, imshow(reshape(data_prior(1:ed,4),[Xc,Yc]),[]);
subplot 345, imshow(reshape(data_prior(ed+1:2*ed,1),[Xc,Yc]),[]);
subplot 346, imshow(reshape(data_prior(ed+1:2*ed,2),[Xc,Yc]),[]);
subplot 347, imshow(reshape(data_prior(ed+1:2*ed,3),[Xc,Yc]),[]);
subplot 348, imshow(reshape(data_prior(ed+1:2*ed,4),[Xc,Yc]),[]);
subplot (3,4,9), imshow(reshape(data_prior(2*ed+1:3*ed,1),[Xc,Yc]),[]);
subplot (3,4,10), imshow(reshape(data_prior(2*ed+1:3*ed,2),[Xc,Yc]),[]);
subplot (3,4,11), imshow(reshape(data_prior(2*ed+1:3*ed,3),[Xc,Yc]),[]);
subplot (3,4,12), imshow(reshape(data_prior(2*ed+1:3*ed,4),[Xc,Yc]),[]);
%%

ed = Xc*Yc;
figure,
subplot 321, imshow(reshape(probS(1:ed,1),[Xc,Yc]));
subplot 322, imshow(reshape(probS(1:ed,2),[Xc,Yc]));
subplot 323, imshow(reshape(probS(ed+1:2*ed,1),[Xc,Yc]));
subplot 324, imshow(reshape(probS(ed+1:2*ed,2),[Xc,Yc]));
subplot 325, imshow(reshape(probS(2*ed+1:3*ed,1),[Xc,Yc]));
subplot 326, imshow(reshape(probS(2*ed+1:3*ed,2),[Xc,Yc]));


%%

ed = Xc*Yc;
figure,
subplot 341, imshow(reshape(probS(1:ed,1),[Xc,Yc]),[]);
subplot 342, imshow(reshape(probS(1:ed,2),[Xc,Yc]),[]);
subplot 343, imshow(reshape(probS(1:ed,3),[Xc,Yc]),[]);
subplot 344, imshow(reshape(probS(1:ed,4),[Xc,Yc]),[]);
subplot 345, imshow(reshape(probS(ed+1:2*ed,1),[Xc,Yc]),[]);
subplot 346, imshow(reshape(probS(ed+1:2*ed,2),[Xc,Yc]),[]);
subplot 347, imshow(reshape(probS(ed+1:2*ed,3),[Xc,Yc]),[]);
subplot 348, imshow(reshape(probS(ed+1:2*ed,4),[Xc,Yc]),[]);
subplot (3,4,9), imshow(reshape(probS(2*ed+1:3*ed,1),[Xc,Yc]),[]);
subplot (3,4,10), imshow(reshape(probS(2*ed+1:3*ed,2),[Xc,Yc]),[]);
subplot (3,4,11), imshow(reshape(probS(2*ed+1:3*ed,3),[Xc,Yc]),[]);
subplot (3,4,12), imshow(reshape(probS(2*ed+1:3*ed,4),[Xc,Yc]),[]);



