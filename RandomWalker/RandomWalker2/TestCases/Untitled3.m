% i1 = 52; j1 = 16;
% i2 = 79; j2 = 78;
% i3 = 14; j3 = 70;
% i12 = 72; j12 = 46;
% i13 = 26; j13 = 47;
% i23 = 49; j23 = 68;
% i123 = 50; j123 = 58;
% 
% ar1 = []; ar1_ = []; r = 1;
% for ii = -r:r
%     for jj = -r:r
%         ar1 = [ar1,img(i1+ii,j1+jj)];
%         ar1_ = [ar1_,im1(i1+ii,j1+jj)];
%     end
% end
% 
% figure, subplot 121, imshow(reshape(ar1,[s,s]),[]), subplot 122, imshow(reshape(ar1_,[s,s]),[]),
r = 5; s = 2*r+1;
imv1 = reshape(imgVals1,Xc,Yc,[]);
x1 = fix((sz(imv1,1)-15)*rand(2,1))+7;
ar1 = imv1(x1(1)-r:x1(1)+r,x1(2)-r:x1(2)+r,:);
ar1 = reshape(ar1,s*s,[]);
imv = imgVals;

tmp1 = permute(repmat(ar1,[1,1,sz(imv,1)]),[3,2,1]);
tmp2 = repmat(imv,[1,1,sz(ar1,1)]);
diff1 = abs(acos(sqz(sum(tmp1.*tmp2,2)) ./ sqz((sqrt(sum(tmp1.^2,2)) .* sqrt(sum(tmp1.^2,2))))));
% diff1 = sqz(sqrt(sum( (tmp1-tmp2).^2,2)));
[i,j] = find(diff1<.003);
[i1_,j1_] = ind2sub(sz(img),i);
figure, imshow(img), hold on, plot(j1_,i1_,'r*')


imv2 = reshape(imgVals2,Xc,Yc,[]);
x2 = fix((sz(imv2,1)-15)*rand(2,1))+7;
ar2 = imv2(x1(1)-r:x1(1)+r,x1(2)-r:x1(2)+r,:);
ar2 = reshape(ar2,s*s,[]);

tmp1 = permute(repmat(ar2,[1,1,sz(imv,1)]),[3,2,1]);
tmp2 = repmat(imv,[1,1,sz(ar2,1)]);
diff2 = sqz(sqrt(sum( (tmp1-tmp2).^2,2)));
[i,j] = find(diff2<.085);
[i1_,j1_] = ind2sub(sz(img),i);
figure, imshow(img), hold on, plot(j1_,i1_,'r*')

imv3 = reshape(imgVals3,Xc,Yc,[]);
x3 = fix((sz(imv3,1)-15)*rand(2,1))+7;
ar3 = imv3(x1(1)-r:x1(1)+r,x1(2)-r:x1(2)+r,:);
ar3 = reshape(ar3,s*s,[]);

tmp1 = permute(repmat(ar3,[1,1,sz(imv,1)]),[3,2,1]);
tmp2 = repmat(imv,[1,1,sz(ar3,1)]);
diff3 = sqz(sqrt(sum( (tmp1-tmp2).^2,2)));
[i,j] = find(diff3<.085);
[i1_,j1_] = ind2sub(sz(img),i);
figure, imshow(img), hold on, plot(j1_,i1_,'r*')

%%
r = 5; s = 2*r+1;
x1 = fix((sz(im1,1)-15)*rand(2,1))+7;
ar1 = im1(x1(1)-r:x1(1)+r,x1(2)-r:x1(2)+r,:);
c = normxcorr2(ar1,img);
figure, surf(c), shading flat
[ypeak, xpeak] = find(c==max(c(:)));
yoffSet = ypeak-size(ar1,1);
xoffSet = xpeak-size(ar1,2);
hFig = figure;
hAx  = axes;
imshow(img,'Parent', hAx);
imrect(hAx, [xoffSet+1, yoffSet+1, size(ar1,2), size(ar1,1)]);

% x2 = fix((sz(im2,1)-15)*rand(2,1))+7;
x2 = x1;
ar2 = im2(x2(1)-r:x2(1)+r,x2(2)-r:x2(2)+r,:);
c = normxcorr2(ar2,img);
figure, surf(c), shading flat
[ypeak, xpeak] = find(c==max(c(:)));
yoffSet = ypeak-size(ar2,1);
xoffSet = xpeak-size(ar2,2);
hFig = figure;
hAx  = axes;
imshow(img,'Parent', hAx);
imrect(hAx, [xoffSet+1, yoffSet+1, size(ar2,2), size(ar2,1)]);

% x3 = fix((sz(im3,1)-15)*rand(2,1))+7;
x3 = x1;
ar3 = im3(x3(1)-r:x3(1)+r,x3(2)-r:x3(2)+r,:);
c = normxcorr2(ar3,img);
figure, surf(c), shading flat
[ypeak, xpeak] = find(c==max(c(:)));
yoffSet = ypeak-size(ar3,1);
xoffSet = xpeak-size(ar3,2);
hFig = figure;
hAx  = axes;
imshow(img,'Parent', hAx);
imrect(hAx, [xoffSet+1, yoffSet+1, size(ar3,2), size(ar3,1)]);

% x2 = fix((sz(im2,1)-15)*rand(2,1))+7;
x12 = x1;
ar12 = im12(x2(1)-r:x2(1)+r,x2(2)-r:x2(2)+r,:);
c = normxcorr2(ar12,img);
figure, surf(c), shading flat
[ypeak, xpeak] = find(c==max(c(:)));
yoffSet = ypeak-size(ar12,1);
xoffSet = xpeak-size(ar12,2);
hFig = figure;
hAx  = axes;
imshow(img,'Parent', hAx);
imrect(hAx, [xoffSet+1, yoffSet+1, size(ar12,2), size(ar12,1)]);

%%
mask = find(img~=0);
imv1 = reshape(imgVals1,Xc,Yc,[]);
imv = imgVals(mask,:);
r = 5; s = 2*r+1;
x1 = fix((sz(im1,1)-15)*rand(2,1))+7;
ar1 = imv1(x1(1)-r:x1(1)+r,x1(2)-r:x1(2)+r,:);
ar1 = reshape(ar1,s*s,[]);
ar1 = (ar1 - repmat(mean(ar1,2),[1,sz(ar1,2)])) ./ repmat(var(ar1,[],2),[1,sz(ar1,2)]);
imv = (imv - repmat(mean(imv,2),[1,sz(imv,2)])) ./ repmat(var(imv,[],2),[1,sz(imv,2)]);
% ar1(isnan(ar1))=0; imv(isnan(imv))=0;
tmp1 = permute(repmat(ar1,[1,1,sz(imv,1)]),[3,2,1]);
tmp2 = repmat(imv,[1,1,sz(ar1,1)]);
cor1 = sqz(1/(sz(tmp1,2)-1)*sum(tmp1 .* tmp2,2));
[ypeak, xpeak] = find(cor1==max(cor1(:)));
[i1_,j1_] = ind2sub(sz(img),ypeak);
figure, imshow(img), hold on, plot(j1_,i1_,'r*')

fsz = 1000;
indx = find(img~=0);
id = fix(len(indx)*rand(100,1))+1;
% indx(id)
for ii = 1:len(id) % (indx)
    tpl = imgVals(indx(id(ii)),:);
    for jj = 1:fsz
        t1 = imgVals1(jj,:);    
        t2 = imgVals2(jj,:);    
        t3 = imgVals3(jj,:); 
        t12 = imgVals12(jj,:); 
        t13 = imgVals13(jj,:); 
        t23 = imgVals23(jj,:); 
        t123 = imgVals123(jj,:); 
        tp(1,jj) = acos(tpl*t1' ./ (norm(tpl,'fro')*norm(t1,'fro')));
        tp(2,jj) = acos(tpl*t2' ./ (norm(tpl,'fro')*norm(t2,'fro')));
        tp(3,jj) = acos(tpl*t3' ./ (norm(tpl,'fro')*norm(t3,'fro')));
        tp(4,jj) = acos(tpl*t12' ./ (norm(tpl,'fro')*norm(t12,'fro')));
        tp(5,jj) = acos(tpl*t13' ./ (norm(tpl,'fro')*norm(t13,'fro')));
        tp(6,jj) = acos(tpl*t23' ./ (norm(tpl,'fro')*norm(t23,'fro')));
        tp(7,jj) = acos(tpl*t123' ./ (norm(tpl,'fro')*norm(t123,'fro')));
    end
    [v,c] = min(tp,[],2);
    [~,ic] = min(v,[],1);
%     for k = 1:7
%         mksz(k) = len(find(ic==k));
%     end
%     [~,mms(ii)] = max(mksz);
%     t(ii,:) = ic;
    t_(ii) = ic;
end
img_msk = zeros(sz(img));
img_msk(indx(id))=t_;
% [val,id] = max(t_,[],2);
% idd = find(val>50);
% img_msk(indx((idd))) = id(idd);
figure, imshow(img_msk==1,[])




ds1 = zeros(100);
ds12 = zeros(100);
for i = 1:100
    for j = i:100
        ds1(i,j) = sqrt(sum( (imgVals1(i,:)-imgVals1(j,:)).^2,2));
        ds12(i,j) = sqrt(sum( (imgVals1(i,:)-imgVals2(j,:)).^2,2));
    end
end
        
start = 1; stop = 3;
fsz=1000;
t1 = imgVals1(1:fsz,start:stop);
t2 = imgVals2(1:fsz,start:stop);
figure, plot3(t1(:,1),t1(:,2),t1(:,3),'r*'), hold on, plot3(t2(:,1),t2(:,2),t2(:,3),'b*')        
        



acos(X_R(:)'*XO(:) ./ (norm(X_R,'fro')*norm(XO,'fro')));