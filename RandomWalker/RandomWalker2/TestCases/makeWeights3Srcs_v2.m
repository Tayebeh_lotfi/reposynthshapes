function [LL,Dterm] = makeWeights3Srcs_v2(TxrPth,Txtf1,Txtf2,Txtf3,img,Srcs) % A_Pri1,   
    opt.FixA = true; opt.Src = 2; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
    opt.isVarIntens = true; opt.Sigpow = 75; opt.initA = true; opt.LargeParmtr = false; % true; % 
    opt.AddPtrn = true; opt.height = 20; opt.width = 20; opt.mag = 10; 
    opt.TxrPth = TxrPth; 
    opt.Txtf1 = Txtf1; % 1.5.03, 1.1.01, 1.1.02
    opt.Txtf2 = Txtf2; % 1.1.03
    opt.Txtf3 = Txtf3;
    imv1 = []; imv2 = []; imv3 = []; imv12 = []; imv13 = []; imv23 = [];
    imv123 = []; imv0 = [];
    [Xc, Yc, ~]=size(img);
    a = 1:Xc*Yc; a = reshape(a,[Xc,Yc]);
    x = mod(1:Xc*Yc,Xc);
    ind_1 = find(x==0|x==1|x==2|x==Xc-1);
    ind_2 = [ind_1,1:2*Xc+2,Xc*Xc-2*Xc-1:Xc*Yc];
    a(ind_2) = [];
    a = a';
    rad = 1; sig = 0.9;
    iter = 1;
    while iter < 7
        [A_Pri1, ~, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(opt);
        while(HasOverlap ==0)
            [A_Pri1, A_Pri, X_, Y_, HasOverlap,Texture,~] = Create_Samples5(opt);
        end
        img_ = im2double(Y_);
        immd_ = double(reshape(MIND_descriptor2d(img_,rad,sig),Xc*Yc,[]));
        feature_ = -1*ones(Xc*Yc,136);
        feature_(a,:) = [immd_(a-2*Xc-2,:),immd_(a-2*Xc-1,:),immd_(a-2*Xc,:),...
            immd_(a-2*Xc+1,:),immd_(a-2*Xc+2,:),immd_(a-Xc-2,:),immd_(a-Xc+2,:),...
            immd_(a-2,:),immd_(a,:),immd_(a+2,:),immd_(a+Xc-2,:),...
            immd_(a+Xc+2,:),immd_(a+2*Xc-2,:),immd_(a+2*Xc-1,:),immd_(a+2*Xc,:),...
            immd_(a+2*Xc+1,:),immd_(a+2*Xc+2,:)];
        %Build graph
        m_ind = find_index(X_,img_);
        for i = 1:sz(m_ind,2)
            id = find(feature_(m_ind{i},1)==-1);
            m_ind{i}(id)=[];
            l(i) = len(m_ind{i});
        end
        smpls = min(min(l(:)),100);
    %     imgVals=im(:); 
        if (smpls>30)
            imv1 = [imv1;feature_(m_ind{1}(1:smpls),:)]; imv2 = [imv2;feature_(m_ind{2}(1:smpls),:)]; 
            imv3 = [imv3;feature_(m_ind{3}(1:smpls),:)]; imv12 = [imv12;feature_(m_ind{4}(1:smpls),:)]; 
            imv13 = [imv13;feature_(m_ind{5}(1:smpls),:)]; imv23 = [imv23;feature_(m_ind{6}(1:smpls),:)]; 
            imv123 = [imv123;feature_(m_ind{7}(1:smpls),:)]; imv0 = [imv0;feature_(m_ind{8}(1:smpls),:)];
            iter = iter + 1;
        end
    end
    immd = double(reshape(MIND_descriptor2d(img,rad,sig),Xc*Yc,[]));    
    feature = -1*ones(Xc*Yc,136);
    feature(a,:) = [immd(a-2*Xc-2,:),immd(a-2*Xc-1,:),immd_(a-2*Xc,:),...
        immd(a-2*Xc+1,:),immd(a-2*Xc+2,:),immd(a-Xc-2,:),immd(a-Xc+2,:),...
        immd(a-2,:),immd(a,:),immd(a+2,:),immd(a+Xc-2,:),...
        immd(a+Xc+2,:),immd(a+2*Xc-2,:),immd(a+2*Xc-1,:),immd(a+2*Xc,:),...
        immd(a+2*Xc+1,:),immd(a+2*Xc+2,:)];
    [LL, Dterm] = CalculateLaplacian(feature,img,imv1,imv2,imv3,imv12,imv13,imv23,imv123,imv0,Srcs);
end
%             imv1 = [imv1;feature_(m_ind{1}(1:smpls),:);feature_(m_ind{4}(1:smpls),:);...
%                 feature_(m_ind{5}(1:smpls),:);feature_(m_ind{7}(1:smpls),:)]; 
%             imv2 = [imv2;feature_(m_ind{2}(1:smpls),:);feature_(m_ind{4}(1:smpls),:);...
%                 feature_(m_ind{6}(1:smpls),:);feature_(m_ind{7}(1:smpls),:)]; 
%             imv3 = [imv3;feature_(m_ind{3}(1:smpls),:);feature_(m_ind{5}(1:smpls),:);...
%                 feature_(m_ind{6}(1:smpls),:);feature_(m_ind{7}(1:smpls),:)]; 
