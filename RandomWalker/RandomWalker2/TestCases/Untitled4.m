[sXi, sYi, ~] = impixel(img);
ind = sub2ind(sz(img),sYi,sXi);
% ind(end) = [];
[ii, jj] = ind2sub(sz(img),ind);
figure, imshow(img), hold on, plot(jj, ii, 'r*')


[mnd1(ind),mnd2(ind),mnd3(ind),mnd12(ind),mnd13(ind),mnd23(ind),mnd123(ind),mnd0(ind),immd(ind)]

strmthd = {'euclidean','seuclidean','squaredeuclidean','cityblock',...
    'correlation','spearman','cosine','chebychev','hamming','jaccard','mahalanobis','minkowski'};
P = {[],nanstd(feature),[],[],[],[],[],[],[],[],nancov(feature),1.2};
mtd = 12;

if(find ([2,11,12] == mtd))
    D1_ = (pdist2(feature,imv1,strmthd{mtd},P{mtd}));
    D2_ = (pdist2(feature,imv2,strmthd{mtd},P{mtd}));
    D3_ = (pdist2(feature,imv3,strmthd{mtd},P{mtd}));
    D12_ = (pdist2(feature,imv12,strmthd{mtd},P{mtd}));
    D13_ = (pdist2(feature,imv13,strmthd{mtd},P{mtd}));
    D23_ = (pdist2(feature,imv23,strmthd{mtd},P{mtd}));
    D123_ = (pdist2(feature,imv123,strmthd{mtd},P{mtd}));
    D0_ = (pdist2(feature,imv0,strmthd{mtd},P{mtd}));
else
    D1_ = (pdist2(feature,imv1,strmthd{mtd}));
    D2_ = (pdist2(feature,imv2,strmthd{mtd}));
    D3_ = (pdist2(feature,imv3,strmthd{mtd}));
    D12_ = (pdist2(feature,imv12,strmthd{mtd}));
    D13_ = (pdist2(feature,imv13,strmthd{mtd}));
    D23_ = (pdist2(feature,imv23,strmthd{mtd}));
    D123_ = (pdist2(feature,imv123,strmthd{mtd}));
    D0_ = (pdist2(feature,imv0,strmthd{mtd}));
end
%%
[x2,y2] = ind2sub(sz(img_),m_ind{1}(1:smpls));
figure, imshow(img_), hold on, plot(y2,x2,'r*')

smpls = 15;
[x2,y2] = ind2sub(sz(img),index{2}(1:smpls));
figure, imshow(img), hold on, plot(y2,x2,'r*')

%%
msk{1} = Srcs(:,:,1)>0&Srcs(:,:,2)==0&Srcs(:,:,3)==0;
msk{2} = Srcs(:,:,1)==0&Srcs(:,:,2)>0&Srcs(:,:,3)==0;
msk{3} = Srcs(:,:,1)==0&Srcs(:,:,2)==0&Srcs(:,:,3)>0;
msk{4} = Srcs(:,:,1)>0&Srcs(:,:,2)>0&Srcs(:,:,3)==0;
msk{5} = Srcs(:,:,1)>0&Srcs(:,:,2)==0&Srcs(:,:,3)>0;
msk{6} = Srcs(:,:,1)==0&Srcs(:,:,2)>0&Srcs(:,:,3)>0;
msk{7} = Srcs(:,:,1)>0&Srcs(:,:,2)>0&Srcs(:,:,3)>0;

for i = 1:7
    index{i} = find(msk{i}); 
end
index{8} = find(img==0);
for i = 1:8
    index{i}(find(feature(index{i},1)==-1))=[];
end

for i = 1:8
    index{i} = index{i}(randperm(sz(index{i},1)));
end

for i = 1:7
    notind{i} = (1:10000)'; 
    notind{i}([index{i};index{8}]) = [];
    notind{i}(find(feature(notind{i},1)==-1))=[];
    notind{i} = notind{i}(randperm(sz(notind{i},1)));
end


l_ = [len(ind1),len(ind2),len(ind3),len(ind12),len(ind13),len(ind23),len(ind123),...
    len(notind1),len(notind2),len(notind3),len(notind12),len(notind13),len(notind23),len(notind123)];

strt = 1; st = 1; stp = 10; %sz(D1_,2);
strt_ = 1; st_ = 5; stp_ = min(min(l_(:)),50); ylimit = 2; % stp_ = 20;
for ii = 1:7
    figure,
    switch(ii)
        case 1
            for j = strt_:st_:stp_
%                 [srt,id] = sort(D1_(ind1(j),strt:st:stp));
%                 plot(strt:st:stp,srt,'b.-'), hold on,
%                 hold on, plot(strt:st:stp,D1_(notind1(j),id),'r*-'),
                subplot 121, plot(strt:st:stp,sort(D1_(ind1(j),strt:st:stp)),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,sort(D1_(ind2(j),strt:st:stp)),'r.-'), hold on,
                title('Ptrn 1'), %ylim([0,ylimit])
            end
        case 2
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,sort(D2_(ind2(j),strt:st:stp)),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,sort(D2_(notind2(j),strt:st:stp)),'r*-'), hold on,
                title('Ptrn 2'), %ylim([0,ylimit])
            end
        case 3
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D3_(ind3(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D3_(notind3(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 3'), %ylim([0,ylimit])
            end
        case 4
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D12_(ind12(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D12_(notind12(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 12'), %ylim([0,ylimit])
            end
        case 5
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D13_(ind13(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D13_(notind13(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 13'), %ylim([0,ylimit])
            end
        case 6
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D23_(ind23(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D23_(notind23(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 23'), %ylim([0,ylimit])
            end
        case 7
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D123_(ind123(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D123_(notind123(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 123'), %ylim([0,ylimit])
            end
    end
end

%%
D_1 = abs(fft(D1_,256,2)); D_2 = abs(fft(D2_,256,2)); D_3 = abs(fft(D3_,256,2));
D_12 = abs(fft(D12_,256,2)); D_13 = abs(fft(D13_,256,2)); D_23 = abs(fft(D23_,256,2));
D_123 = abs(fft(D123_,256,2)); edft = 1;
D_1(:,1:edft) = []; D_2(:,1:edft) = []; D_3(:,1:edft) = [];D_12(:,1:edft) = []; D_13(:,1:edft) = []; 
D_23(:,1:edft) = []; D_123(:,1:edft) = [];

strt = 1; st = 5; stp = 100; %sz(D1_,2);
strt_ = 1; st_ = 5; stp_ = min(min(l_(:)),50); % stp_ = 20;
for ii = 1:7
    figure,
    switch(ii)
        case 1
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_1(ind1(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_1(notind1(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 1'), %ylim([0,ylimit])
%                 pause,
            end
        case 2
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_2(ind2(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_2(notind2(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 2'), %ylim([0,ylimit])
%                 pause,
            end
        case 3
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_3(ind3(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_3(notind3(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 3'), %ylim([0,ylimit])
%                 pause,
            end
        case 4
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_12(ind12(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_12(notind12(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 12'), %ylim([0,ylimit])
%                 pause,
            end
        case 5
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_13(ind13(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_13(notind13(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 13'), %ylim([0,ylimit])
%                 pause,
            end
        case 6
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_23(ind23(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_23(notind23(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 23'), %ylim([0,ylimit])
%                 pause,
            end
        case 7
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D_123(ind123(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D_123(notind123(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 123'), %ylim([0,ylimit])
%                 pause,
            end
    end
end


%%
strt = 1; st = 5; stp = 100; %sz(D1_,2);
strt_ = 1; st_ = 5; stp_ = min(min(l_(:)),50); ylimit = 2; % stp_ = 20;
for ii = 1:7
    figure,
    switch(ii)
        case 1
            for j = strt_:st_:stp_
%                 [srt,id] = sort(D1_(ind1(j),strt:st:stp));
%                 plot(strt:st:stp,srt,'b.-'), hold on,
%                 hold on, plot(strt:st:stp,D1_(notind1(j),id),'r*-'),
                subplot 121, plot(strt:st:stp,D1_(ind1(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D1_(notind1(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 1'), %ylim([0,ylimit])
            end
        case 2
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D2_(ind2(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D2_(notind2(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 2'), %ylim([0,ylimit])
            end
        case 3
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D3_(ind3(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D3_(notind3(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 3'), %ylim([0,ylimit])
            end
        case 4
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D12_(ind12(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D12_(notind12(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 12'), %ylim([0,ylimit])
            end
        case 5
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D13_(ind13(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D13_(notind13(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 13'), %ylim([0,ylimit])
            end
        case 6
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D23_(ind23(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D23_(notind23(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 23'), %ylim([0,ylimit])
            end
        case 7
            for j = strt_:st_:stp_
                subplot 121, plot(strt:st:stp,D123_(ind123(j),strt:st:stp),'b.-'), hold on,
                subplot 122, plot(strt:st:stp,D123_(notind123(j),strt:st:stp),'r*-'), hold on,
                title('Ptrn 123'), %ylim([0,ylimit])
            end
    end
end


%%

tmp = [];
str = {'r-*','b-.','g-o','c->','m-s','k-d','y-+'};
strt = 1; st = 5; stp = sz(D1_,2);

for ii = 1:7
    for jj = 1:7
        switch(jj)
            case 1
                tmp(1,:,ii) = D1_(ind(ii),:);
            case 2
                tmp(2,:,ii) = D2_(ind(ii),:);
            case 3
                tmp(3,:,ii) = D3_(ind(ii),:);
            case 4
                tmp(4,:,ii) = D12_(ind(ii),:);
            case 5
                tmp(5,:,ii) = D13_(ind(ii),:);
            case 6
                tmp(6,:,ii) = D23_(ind(ii),:);
            case 7
                tmp(7,:,ii) = D123_(ind(ii),:);
        end
    end
end

for ii = 1:7
    figure,
    for jj = 1:7
        plot(strt:st:stp,tmp(jj,strt:st:stp,ii),str{jj}), hold on,
    end
end

for ii = 1:7
    for jj = 1:7
        tmpft(jj,:,ii) = fft(sqz(tmp(jj,:,ii)));
    end
end
for ii = 1:7
    figure,
    for jj = 1:7
        plot(strt:st:stp,abs(tmpft(jj,strt:st:stp,ii)),str{jj}), hold on,
    end
end
%%
ii = 1;
tmp2 = [];
start = -1;
stpp = 6;
for st = start:stpp
    tmp2(st-start+1,:) = D2_(ind(ii)+st,:);
end
strt = 1; st = 10; stp = sz(D2_,2);
figure,
for jj = 1:7
        plot(strt:st:stp,tmp2(jj,strt:st:stp),str{jj}), hold on,
end

%%
limitUpX = floor(max([D1_(:);D2_(:);D3_(:);D12_(:);D13_(:);D23_(:);D123_(:);D0_(:)]))+1;
limitDwnX = floor(min([D1_(:);D2_(:);D3_(:);D12_(:);D13_(:);D23_(:);D123_(:);D0_(:)]));

str = {'r*','b.','go','c>','ms','kd','y+'};
figure,
for i = 1:sz(ind,1)-1
    plot(D1_(ind(i),:),i,str{i}), hold on
    ylim([0.8,7.2])
end
figure,
for i = 1:sz(ind,1)-1
    plot(D2_(ind(i),:),i,str{i}), hold on
    ylim([0.8,7.2])
end
figure,
for i = 1:sz(ind,1)-1
    plot(D3_(ind(i),:),i,str{i}), hold on
    ylim([0.8,7.2])
end%     xlim([limitDwnX,limitUpX]);ylim([-1,1])
% end
st = 1;
for i = 1:sz(ind,1)-1
temp{i} = [min(D1_(ind(i)+st,:)),max(D1_(ind(i)+st,:));min(D2_(ind(i)+st,:)),max(D2_(ind(i)+st,:));...
    min(D3_(ind(i)+st,:)),max(D3_(ind(i)+st,:));min(D12_(ind(i)+st,:)),max(D12_(ind(i)+st,:));...
    min(D13_(ind(i)+st,:)),max(D13_(ind(i)+st,:));min(D23_(ind(i)+st,:)),max(D23_(ind(i)+st,:));...
    min(D123_(ind(i)+st,:)),max(D123_(ind(i)+st,:));min(D0_(ind(i)+st,:)),max(D0_(ind(i)+st,:))];
end

%%
i = 1; %3;
for jj = 1:6
    for st = -5:5
        switch(jj)
            case 1
                tmp(1,st+5+1) = max(D1_(ind(i)+st,:))-min(D1_(ind(i)+st,:));
            case 2
                tmp(2,st+5+1) = max(D2_(ind(i)+st,:))-min(D2_(ind(i)+st,:));
            case 3
                tmp(3,st+5+1) = max(D3_(ind(i)+st,:))-min(D3_(ind(i)+st,:));
            case 4
                tmp(4,st+5+1) = max(D12_(ind(i)+st,:))-min(D12_(ind(i)+st,:));
            case 5
                tmp(5,st+5+1) = max(D13_(ind(i)+st,:))-min(D13_(ind(i)+st,:));
            case 6
                tmp(6,st+5+1) = max(D23_(ind(i)+st,:))-min(D23_(ind(i)+st,:));
            case 7
                tmp(7,st+5+1) = max(D123_(ind(i)+st,:))-min(D123_(ind(i)+st,:));
        end
    end
end
str = {'r-*','b-.','g-o','c->','m-s','k-d','y-+'};

figure,
for jj = 1:6
    plot(1:sz(tmp,2),tmp(jj,:),str{jj}), hold on,
end

tmp2 = [];
for st = -5:5
    tmp2 = [tmp2,max(D2_(ind(i)+st,:))-min(D2_(ind(i)+st,:))];
end

tmp3 = [];
for st = -5:5
    tmp3 = [tmp3,max(D2_(ind(i)+st,:))-min(D2_(ind(i)+st,:))];
end



st = 28;max(D1_(ind(i)+st,:)) - min(D1_(ind(i)+st,:))
st = -35:35;
[ii, jj] = ind2sub(sz(img),ind(i)+st);
figure, imshow(img), hold on, plot(jj, ii, 'r*'), hold on
st = 35; 
[ii, jj] = ind2sub(sz(img),ind(i)+st);
plot(jj, ii, 'g+')
st = 0; 
[ii, jj] = ind2sub(sz(img),ind(i)+st);
plot(jj, ii, 'bo')


for i = 1:sz(ind,1)
    figure,
    plot(D1_(ind(i),:),0,'r*'), hold on
    plot(D2_(ind(i),:),0,'b.'), hold on
    plot(D3_(ind(i),:),0,'go'), hold on
    plot(D12_(ind(i),:),0,'c>'), hold on
    plot(D13_(ind(i),:),0,'ms'), hold on
%     xlim([limitDwnX,limitUpX]);ylim([-1,1])
end

[val,loc] = min([var(D1_(ind(i),:),[],2),var(D2_(ind(i),:),[],2),var(D3_(ind(i),:),[],2),...
    var(D12_(ind(i),:),[],2),var(D13_(ind(i),:),[],2),var(D23_(ind(i),:),[],2),...
    var(D123_(ind(i),:),[],2)]);%,var(D0_(ind(i),:),[],2)]);

D1_ = (pdist2(immd,imv1,'minkowski'));
D2_ = (pdist2(immd,imv2,'minkowski'));
D3_ = (pdist2(immd,imv3,'minkowski'));
D12_ = (pdist2(immd,imv12,'minkowski'));
D13_ = (pdist2(immd,imv13,'minkowski'));
D23_ = (pdist2(immd,imv23,'minkowski'));
D123_ = (pdist2(immd,imv123,'minkowski'));
D0_ = (pdist2(immd,imv0,'minkowski'));





[D1_(ind(1),1:10);D1_(ind(2),1:10);D1_(ind(3),1:10);D1_(ind(4),1:10);D1_(ind(5),1:10);D1_(ind(6),1:10);D1_(ind(7),1:10);D1_(ind(8),1:10)]
[D2_(ind(1),1:10);D2_(ind(2),1:10);D2_(ind(3),1:10);D2_(ind(4),1:10);D2_(ind(5),1:10);D2_(ind(6),1:10);D2_(ind(7),1:10);D2_(ind(8),1:10)]
[D3_(ind(1),1:10);D3_(ind(2),1:10);D3_(ind(3),1:10);D3_(ind(4),1:10);D3_(ind(5),1:10);D3_(ind(6),1:10);D3_(ind(7),1:10);D3_(ind(8),1:10)]




D1 = min(pdist2(immd,imv1,'chebychev'),[],2);
D2 = min(pdist2(immd,imv2,'chebychev'),[],2);
D3 = min(pdist2(immd,imv3,'chebychev'),[],2);
D12 = min(pdist2(immd,imv12,'chebychev'),[],2);
D13 = min(pdist2(immd,imv13,'chebychev'),[],2);
D23 = min(pdist2(immd,imv23,'chebychev'),[],2);
D123 = min(pdist2(immd,imv123,'chebychev'),[],2);
D0 = min(pdist2(immd,imv0,'chebychev'),[],2);

D1 = mean(pdist2(immd,imv1,'chebychev'),2);
D2 = mean(pdist2(immd,imv2,'chebychev'),2);
D3 = mean(pdist2(immd,imv3,'chebychev'),2);
D12 = mean(pdist2(immd,imv12,'chebychev'),2);
D13 = mean(pdist2(immd,imv13,'chebychev'),2);
D23 = mean(pdist2(immd,imv23,'chebychev'),2);
D123 = mean(pdist2(immd,imv123,'chebychev'),2);
D0 = mean(pdist2(immd,imv0,'chebychev'),2);

[D1(ind),D2(ind),D3(ind),D12(ind),D13(ind),D23(ind),D123(ind),D0(ind)]



imgVals1_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv1,1)])-...
    permute(repmat(imv1,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals2_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv2,1)])-...
    permute(repmat(imv2,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals3_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv3,1)])-...
    permute(repmat(imv3,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals12_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv12,1)])-...
    permute(repmat(imv12,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals13_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv13,1)])-...
    permute(repmat(imv13,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals23_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv23,1)])-...
    permute(repmat(imv23,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals123_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv123,1)])-...
    permute(repmat(imv123,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));
imgVals0_ = (sqz(sqrt(sum((repmat(immd,[1,1,sz(imv0,1)])-...
    permute(repmat(imv0,[1,1,sz(immd,1)]),[3,2,1])).^2,2))));


[imgVals1_(ind,1:10),imgVals2_(ind,1:10),imgVals3_(ind,1:10),...
    imgVals12_(ind,1:10),imgVals13_(ind,1:10),imgVals23_(ind,1:10),...
    imgVals123_(ind,1:10),imgVals0_(ind,1:10)]


imgVals1 = var(imgVals1_,[],2);
imgVals2 = var(imgVals2_,[],2);
[imgVals1(ind),imgVals2(ind),imgVals3(ind),imgVals12(ind),imgVals13(ind),imgVals23(ind),imgVals123(ind),imgVals0(ind)]


%%
tmp1 = [];
ii = 1;
for jj = 1:5000
    tmp1(jj,:) = D1_(jj,:);
end
tm1 = D1_(ind(ii),:);
str = {'r-*','b-.','g-o','c->','m-s','k-d','y-+'};
strt = 1; st = 20; stp = sz(tmp1,2);
figure,
for jj = 1:5000
    plot(strt:st:stp,tmp1(jj,strt:st:stp),'b.-'), hold on,
end
hold on, plot(strt:st:stp,tm1(strt:st:stp),'r.-')



tmp2 = [];
ii = 2;
for jj = 1:5000
    tmp2(jj,:) = D2_(jj,:);
end
tm2 = D2_(ind(ii),:);
figure,
for jj = 1:5000
    plot(strt:st:stp,tmp2(jj,strt:st:stp),'b.-'), hold on,
end
hold on, plot(strt:st:stp,tm2(strt:st:stp),'r.-')
