function [LL, DPr] = CalculateLaplacian_v1(feature, img, imv1, imv2,imv3,imv12,imv13,imv23,imv123,imv0,Srcs)
    %% Create distances
    strmthd = {'euclidean','seuclidean','squaredeuclidean','cityblock',...
        'correlation','spearman','cosine','chebychev','hamming','jaccard','mahalanobis','minkowski'};
    P = {[],nanstd(feature),[],[],[],[],[],[],[],[],nancov(feature),1.2};
    mtd = 1;
    if(find ([2,11,12] == mtd))
        D1_ = (pdist2(feature,imv1,strmthd{mtd},P{mtd}));
        D2_ = (pdist2(feature,imv2,strmthd{mtd},P{mtd}));
        D3_ = (pdist2(feature,imv3,strmthd{mtd},P{mtd}));
        D12_ = (pdist2(feature,imv12,strmthd{mtd},P{mtd}));
        D13_ = (pdist2(feature,imv13,strmthd{mtd},P{mtd}));
        D23_ = (pdist2(feature,imv23,strmthd{mtd},P{mtd}));
        D123_ = (pdist2(feature,imv123,strmthd{mtd},P{mtd}));
        D0_ = (pdist2(feature,imv0,strmthd{mtd},P{mtd}));
    else
        D1_ = (pdist2(feature,imv1,strmthd{mtd}));
        D2_ = (pdist2(feature,imv2,strmthd{mtd}));
        D3_ = (pdist2(feature,imv3,strmthd{mtd}));
        D12_ = (pdist2(feature,imv12,strmthd{mtd}));
        D13_ = (pdist2(feature,imv13,strmthd{mtd}));
        D23_ = (pdist2(feature,imv23,strmthd{mtd}));
        D123_ = (pdist2(feature,imv123,strmthd{mtd}));
        D0_ = (pdist2(feature,imv0,strmthd{mtd}));
    end
    %% Make data priors
    D1 = sort(D1_,2);
    D2 = sort(D2_,2);
    D3 = sort(D3_,2);
    D12 = sort(D12_,2);
    D13 = sort(D13_,2);
    D23 = sort(D23_,2);
    D123 = sort(D123_,2);
%     D0 = sort(D0_,2);
    D{1} = D1; D{2} = D2; D{3} = D3; D{4} = D12; D{5} = D13; D{6} = D23; D{7} = D123;
    certhr1 = 0.05; %certhr2 = .1;
    for i = 1:7
    %     thr1 = quantile(D{i},[certhr1,certhr2],2); 
    %     Sel = ( D{i} > thr1(:,1) & D{i} < thr1(:,2));
        thr1 = quantile(D{i},certhr1,2);
        Sel = ( D{i} < thr1); % (:,1) & D{i} < thr1(:,2));
        DD = D{i} .* Sel;
        M{i} = sum(DD,2) ./ sum(Sel,2);
    end

    DPr = [min(min(M{2},M{3}),M{6}), min(M{1},min(M{4},min(M{5},M{7}))), zeros(sz(M{1})), zeros(sz(M{1}));...
    min(min(M{1},M{3}),M{5}), zeros(sz(M{2})), min(M{2},min(M{4},min(M{6},M{7}))), zeros(sz(M{2}));...
    min(min(M{1},M{2}),M{4}), zeros(sz(M{3})), zeros(sz(M{3})), min(M{3},min(M{5},min(M{6},M{7})))];

    [Xc, Yc, ~]=size(img);
    %% Build graph
    [points, edges]=lattice(Xc,Yc,1);
    beta = 90; 
    imgVals=img(:); 
    weights=makeweights(edges,imgVals,beta);
    L=laplacian(edges,weights);
    LL = [L,zeros(sz(L)),zeros(sz(L));zeros(sz(L)),L,zeros(sz(L));zeros(sz(L)),zeros(sz(L)),L];
end    
%%     
%     ind0 = find(img==0);
%     ind = (1:10000)'; 
%     noind_ = find(feature(ind,1)==-1);
%     noind = [noind_;ind0];
% 
%     ap = 3.9; s = sz(D1,2); 
%     mD1 = exp(-ap*mean(D1(:,1:s),2)); mD1(noind) = 0;
%     mD2 = exp(-ap*mean(D2(:,1:s),2));mD3 = exp(-ap*mean(D3(:,1:s),2));
%     mD12 = exp(-ap*mean(D12(:,1:s),2)); mD13 = exp(-ap*mean(D13(:,1:s),2));mD23 = exp(-ap*mean(D23(:,1:s),2));
%     mD123 = exp(-ap*mean(D123(:,1:s),2));
%     v = [mD1,mD2,mD3,mD12,mD13,mD23,mD123];
%     % ind([noind;ind0]) = [];
%     [f,id] = min(v,[],2);
%     id(noind) = 0;
%     id12 = find(id==4); id13 = find(id==5); id23 = find(id==6); id123 = find(id==7);
% %     id12(id12~=4) = []; id13(id13~=5) = []; id23(id23~=6) = []; id123(id123~=7) = [];
%     [Xc, Yc, ~]=size(img);
%     %% Build graph
%     [points, edges]=lattice(Xc,Yc,1);
%     beta = 90; 
%     weights12=makeweights(edges,mD12,beta);
%     weights13=makeweights(edges,mD13,beta);
%     weights23=makeweights(edges,mD23,beta);
%     imgVals=img(:); 
%     weights=makeweights(edges,imgVals,beta);
%     L=laplacian(edges,weights);
%     ed = Xc*Yc; alpha = 1e-2; step = ed+1;
%     %% Build laplacian matrix
%     L(1:step:end) = 0;
%     L12=laplacian(edges,weights12); L12(1:step:end) = -mD12;    
%     L13=laplacian(edges,weights13); L13(1:step:end) = -mD13; 
%     L23=laplacian(edges,weights23); L23(1:step:end) = -mD23; 
%     LL = zeros(3*ed,3*ed);
%     LL(id12,ed+1:2*ed)=alpha*L12(id12,:);
%     LL(ed+id12,1:ed)=alpha*L12(id12,:);
%     LL(id13,2*ed+1:3*ed)=alpha*L13(id13,:);
%     LL(2*ed+id13,1:ed)=alpha*L13(id13,:);
%     LL(ed+id23,2*ed+1:3*ed)=alpha*L23(id23,:);
%     LL(2*ed+id23,ed+1:2*ed)=alpha*L23(id23,:);
%     LL(1:ed,1:ed)=L; LL(ed+1:2*ed,ed+1:2*ed) = L; LL(2*ed+1:3*ed,2*ed+1:3*ed) = L;
%     LL(1:3*Xc*Yc+1:end) = -sum(LL,1);
% 
%     s = 3; % sz(D1,2);
%     mD1 = mean(D1(:,1:s),2); mD2 = mean(D2(:,1:s),2);mD3 = mean(D3(:,1:s),2);
%     mD12 = mean(D12(:,1:s),2); mD13 = mean(D13(:,1:s),2);mD23 = mean(D23(:,1:s),2);
%     mD123 = mean(D123(:,1:s),2);
%     DPr = [min(min(mD2,mD3),mD23),mD1,mD12,mD13;min(min(mD1,mD3),mD13),mD12,mD2,mD23;...
%         min(min(mD1,mD2),mD12),mD13,mD23,mD3];

%% 
% id1 = id;
% id1(id1==6) = 0;
% id1(id1==2) = 0;
% id1(id1==3) = 0;
% % id1(id1 == 4) = 0;
% % id1(id1 == 5) = 0;
% % id1(id1==7) = 0;
% id1(id1~=0) = 1;
% 
% id2 = id;
% id2(id2==5) = 0;
% id2(id2==1) = 0;
% id2(id2==3) = 0;
% % id2(id2 == 4) = 0;
% % id2(id2 == 6) = 0;
% % id2(id2==7) = 0;
% id2(id2~=0) = 1;
% 
% id3 = id;
% id3(id3==4) = 0;
% id3(id3==1) = 0;
% id3(id3==2) = 0;
% % id3(id3 == 5) = 0;
% % id3(id3 == 6) = 0;
% % id3(id3==7) = 0;
% id3(id3~=0) = 1;
% figure, 
% subplot 131, imshow(reshape(id1,[100,100]),[])
% subplot 132, imshow(reshape(id2,[100,100]),[])
% subplot 133, imshow(reshape(id3,[100,100]),[])
% 
% id12 = id;
% id12(id12~=4) = 0;
% id12(id12~=0) = 1;
% figure, imshow(reshape(id12,[100,100]),[])
% 
% id13 = id;
% id13(id13~=5) = 0;
% id13(id13~=0) = 1;
% figure, imshow(reshape(id13,[100,100]),[])
% 
% id23 = id;
% id23(id23~=6) = 0;
% id23(id23~=0) = 1;
% figure, imshow(reshape(id23,[100,100]),[])
% 
% id123 = id;
% id123(id123~=7) = 0;
% id123(id123~=0) = 1;
% figure, imshow(reshape(id123,[100,100]),[])
% 
