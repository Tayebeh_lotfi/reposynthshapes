[Xc,Yc] = size(img);
ed = prod(sz(img));

figure,
subplot 141, imshow(reshape(probS(1:ed,1),[Xc,Yc]),[])
subplot 142, imshow(reshape(probS(1:ed,2),[Xc,Yc]),[])
subplot 143, imshow(reshape(probS(1:ed,3),[Xc,Yc]),[])
subplot 144, imshow(reshape(probS(1:ed,4),[Xc,Yc]),[])

figure,
for i = 1:sz(msk,2)
    subplot 131, imshow(reshape(msk(1:ed,i),[100,100]),[])
    subplot 132, imshow(reshape(msk(ed+1:2*ed,i),[100,100]),[])
    subplot 133, imshow(reshape(msk(2*ed+1:3*ed,i),[100,100]),[])
    pause
end

figure,
subplot 141, imshow(reshape(probS(ed+1:2*ed,1),[Xc,Yc]),[])
subplot 142, imshow(reshape(probS(ed+1:2*ed,2),[Xc,Yc]),[])
subplot 143, imshow(reshape(probS(ed+1:2*ed,3),[Xc,Yc]),[])
subplot 144, imshow(reshape(probS(ed+1:2*ed,4),[Xc,Yc]),[])

figure, imshow(reshape(probS(ed+1:2*ed,2)+probS(ed+1:2*ed,3)+probS(ed+1:2*ed,4),[Xc,Yc]),[])

figure,
subplot 141, imshow(reshape(probS(2*ed+1:end,1),[Xc,Yc]),[])
subplot 142, imshow(reshape(probS(2*ed+1:end,2),[Xc,Yc]),[])
subplot 143, imshow(reshape(probS(2*ed+1:end,3),[Xc,Yc]),[])
subplot 144, imshow(reshape(probS(2*ed+1:end,4),[Xc,Yc]),[])

figure, mesh(reshape(probS(2*ed+1:end,4),[Xc,Yc]))


figure, 
for i = 1:20
    subplot 121, imshow(reshape(imgVals(:,i),[100,100]),[])
    subplot 122, imshow(reshape(imgVals1(:,i),[100,100]),[])
    pause
end

figure,
subplot 131, imshow(reshape(probS(1:ed,2),[100,100]),[])
subplot 132, imshow(reshape(probS(ed+1:2*ed,3),[100,100]),[])
subplot 133, imshow(reshape(probS(2*ed+1:3*ed,4),[100,100]),[])


seg1_ = zeros(len(seg1),1);
seg2_ = zeros(len(seg2),1);
seg3_ = zeros(len(seg3),1);
for ii = 1:len(jj1)
    idx = find(seg1==jj1(ii));
    seg1_(idx) = ii;
end
for ii = 1:len(jj2)
    idx = find(seg2==jj2(ii));
    seg2_(idx) = ii;
end
for ii = 1:len(jj3)
    idx = find(seg3==jj3(ii));
    seg3_(idx) = ii;
end
figure, 
subplot 131, imshow(reshape(seg1_,[100,100]),[])
subplot 132, imshow(reshape(seg2_,[100,100]),[])
subplot 133, imshow(reshape(seg3_,[100,100]),[])
figure, 
subplot 131, imshow(reshape(msk(1:ed),[100,100]),[])
subplot 132, imshow(reshape(msk(ed+1:2*ed),[100,100]),[])
subplot 133, imshow(reshape(msk(2*ed+1:3*ed),[100,100]),[])


C_ = intersect(idx_1,idx_2);
ovlp2 = zeros(sz(img));
ovlp2(C_)=1;

C = intersect(idx1,idx2);
ovlp12 = zeros(sz(img));
ovlp12(C) = 1;
%%    
epsilon = 0; % 1e-10;
u1 = X .* log(X+epsilon);
u1(isnan(u1)) = 0;
unc = -1/sz(X,2)*sum(u1,2);
unc1 = resc(unc(1:ed));
unc2 = resc(unc(ed+1:end));
mask = img(:) > 0; 
cert_thres1 = quantile(unc1(find(mask>0)),.75);
Uncert1 = unc1 .* mask;
idx_1=find( Uncert1 > cert_thres1); % Uncert1 <cert_thresh1;
cert_thres2 = quantile(unc2(find(mask>0)),.75);
Uncert2 = unc2 .* mask;
idx_2=find( Uncert2 > cert_thres2); % Uncert1 <cert_thresh1;
C_ = intersect(idx_1,idx_2);
ovlp2 = zeros(sz(img));
ovlp2(C_)=1;

%%
ed = prod(sz(img));
prob1 = probS(1:ed,2);
prob2 = probS(ed+1:2*ed,3);
prob3 = probS(2*ed+1:end,4);

[seg1,cent1] = kmeans(prob1,6,'Replicates',5,'MaxIter',1000); % 'Distance', 'sqeuclidean' (default)| 'cityblock' | 'cosine' | 'correlation' 
[ii1,jj1] = sort(cent1);
[seg2,cent2] = kmeans(prob2,6,'Replicates',5,'MaxIter',1000);
[ii2,jj2] = sort(cent2);
[seg3,cent3] = kmeans(prob3,6,'Replicates',5,'MaxIter',1000);
[ii3,jj3] = sort(cent3);



TxrPth = '../../ImagesTextures/'; 
Txtf1 = '1.5.03.tiff'; % 1.5.03, 1.1.01, 1.1.02
Txtf2 = '1.1.03.tiff'; % 1.1.03
dnum = 10;
for i = 1:dnum
    I1 = imread([TxrPth,Txtf1]); % 1.1.02
    I2 = imread([TxrPth,Txtf2]); 
    x11 = fix((sz(I1,1)-sz(img,1))*rand(1,1))+1;
    x12 = fix((sz(I1,2)-sz(img,2))*rand(1,1))+1;
    x21 = fix((sz(I2,1)-sz(img,1))*rand(1,1))+1;
    x22 = fix((sz(I2,2)-sz(img,2))*rand(1,1))+1;
    Txt(:,:,1,i) = I1(x11:x11+sz(img,1)-1,x12:x12+sz(img,2)-1);
    Txt(:,:,2,i) = I2(x21:x21+sz(img,1)-1,x22:x22+sz(img,2)-1);
end
Txt = im2double(Txt);
Tx = sqz(sum(Txt,4)/dnum);
figure, 
subplot 221, imshow(Tx(:,:,1),[])
subplot 222, imshow(Tx(:,:,2),[])
subplot 223, imshow(Txt(:,:,1,3),[])
subplot 224, imshow(Txt(:,:,2,3),[])

for k = 1:2
    for ii = 1:sz(img,1)
        for jj = 1:sz(img,2)
            T = sqz(Txt(ii,jj,k,:));
            T = ( T - mean(T) );%  ./ var(T);
            [N,edges] = histcounts(T,5);
            [val,ind] = max(N);
            Tx(ii,jj,k) = edges(ind)+edges(ind+1);
        end
    end
end
figure, 
subplot 221, imshow(Tx(:,:,1),[])
subplot 222, imshow(Tx(:,:,2),[])
subplot 223, imshow(Txt(:,:,1,3),[])
subplot 224, imshow(Txt(:,:,2,3),[])



[val,msk] = max(probS,[],2);
figure, imshow(reshape(msk(1:ed),[100,100]),[])
figure, imshow(reshape(msk(ed+1:2*ed),[100,100]),[])

%%
