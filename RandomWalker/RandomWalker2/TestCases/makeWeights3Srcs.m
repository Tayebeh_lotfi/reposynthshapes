function [LL,Dterm,Txt] = makeWeights3Srcs(TxrPth,Txtf1,Txtf2,Txtf3,img,Txt1,A_Pri1, Srcs)    
    I1 = imread([TxrPth,Txtf1]); % 1.5.03, 1.1.01
    I2 = imread([TxrPth,Txtf2]); 
    I3 = imread([TxrPth,Txtf3]); 
    x11 = fix((sz(I1,1)-sz(img,1))*rand(1,1))+1;
    x12 = fix((sz(I1,2)-sz(img,2))*rand(1,1))+1;
    x21 = fix((sz(I2,1)-sz(img,1))*rand(1,1))+1;
    x22 = fix((sz(I2,2)-sz(img,2))*rand(1,1))+1;
    x31 = fix((sz(I3,1)-sz(img,1))*rand(1,1))+1;
    x32 = fix((sz(I3,2)-sz(img,2))*rand(1,1))+1;
    Txt(:,:,1) = I1(x11:x11+sz(img,1)-1,x12:x12+sz(img,2)-1);
    Txt(:,:,2) = I2(x21:x21+sz(img,1)-1,x22:x22+sz(img,2)-1);
    Txt(:,:,3) = I3(x31:x31+sz(img,1)-1,x32:x32+sz(img,2)-1);
    Txt = im2double(Txt);
%%
%     Txt = Txt1;
    [Xc, Yc, ~]=size(img);
    %Build graph
    [points, edges]=lattice(Xc,Yc,1);
    im1 = A_Pri1(1)*Txt(:,:,1); im2 = A_Pri1(2)*Txt(:,:,2); im3 = A_Pri1(3)*Txt(:,:,3);
    im12 = A_Pri1(1)*Txt(:,:,1) + A_Pri1(2)*Txt(:,:,2);
    im13 = A_Pri1(1)*Txt(:,:,1) + A_Pri1(3)*Txt(:,:,3);
    im23 = A_Pri1(2)*Txt(:,:,2) + A_Pri1(3)*Txt(:,:,3);
    im123 = A_Pri1(1)*Txt(:,:,1) + A_Pri1(2)*Txt(:,:,2) + A_Pri1(3)*Txt(:,:,3);
    samples = 1000;
%     imgVals=im(:); 
    rad = 1; sig = .9;
    mnd1 = double(reshape(MIND_descriptor2d(im1,rad,sig),Xc*Yc,[]));
    mnd2 = double(reshape(MIND_descriptor2d(im2,rad,sig),Xc*Yc,[]));
    mnd3 = double(reshape(MIND_descriptor2d(im3,rad,sig),Xc*Yc,[]));
    mnd12 = double(reshape(MIND_descriptor2d(im12,rad,sig),Xc*Yc,[]));
    mnd13 = double(reshape(MIND_descriptor2d(im13,rad,sig),Xc*Yc,[]));
    mnd23 = double(reshape(MIND_descriptor2d(im23,rad,sig),Xc*Yc,[]));
    mnd123 = double(reshape(MIND_descriptor2d(im123,rad,sig),Xc*Yc,[]));
    immd = double(reshape(MIND_descriptor2d(img,rad,sig),Xc*Yc,[]));
    mnd0 = repmat(immd(1,:),[Xc*Yc,1]); % sz(mnd1,2)
    s = fix(sz(im1,1)*rand(samples,1))+1;
    imv1 = mnd1(s,:); imv2 = mnd2(s,:); imv3 = mnd3(s,:); imv12 = mnd12(s,:);
    imv13 = mnd13(s,:); imv23 = mnd23(s,:); imv123 = mnd123(s,:); imv0 = mnd0(s,:);

    imgVals1 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv1,1)])-...
        permute(repmat(imv1,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals2 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv2,1)])-...
        permute(repmat(imv2,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals3 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv3,1)])-...
        permute(repmat(imv3,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals12 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv12,1)])-...
        permute(repmat(imv12,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals13 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv13,1)])-...
        permute(repmat(imv13,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals23 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv23,1)])-...
        permute(repmat(imv23,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals123 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv123,1)])-...
        permute(repmat(imv123,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    imgVals0 = min(sqz(sqrt(sum((repmat(immd,[1,1,sz(imv0,1)])-...
        permute(repmat(imv0,[1,1,sz(immd,1)]),[3,2,1])).^2,2))),[],2);
    A01 = min(imgVals0,min(min(imgVals2,imgVals3),imgVals23));
    A11 = min(min(imgVals1,imgVals12),imgVals13);
    A02 = min(imgVals0,min(min(imgVals1,imgVals3),imgVals13));
    A22 = min(min(imgVals2,imgVals12),imgVals23);
    A03 = min(imgVals0,min(min(imgVals1,imgVals2),imgVals12));
    A33 = min(min(imgVals3,imgVals23),imgVals13);
%     G01 = max(imgVals2,imgVals3); G01_23 = max(G01,imgVals23);
%     G02 = max(imgVals1,imgVals3); G02_13 = max(G02,imgVals13);
%     G03 = max(imgVals1,imgVals2); G03_12 = max(G03,imgVals12);
%     AA = max(max(G01_23,G02_13),G03_12);
%     G01 = min(min(imgVals2,imgVals3),imgVals23);
%     G11 = min();
%     Dterm = [A01,imgVals1,min(imgVals1,imgVals12),min(imgVals1,imgVals13);...
%         A02,min(imgVals2,imgVals12),imgVals2,min(imgVals2,imgVals23);...
%         A03,min(imgVals3,imgVals13),min(imgVals3,imgVals23),imgVals3];
%     Dterm = [A01,A11,AA,AA;A02,AA,A22,AA;A03,AA,AA,A33];
    Dterm = [A01,A11,imgVals12,imgVals13;A02,imgVals12,A22,imgVals23;A03,imgVals13,imgVals23,A33];
%     Dterm = [A01,A11;A02,A22;A03,A33];
%         Dterm = [imgVals0,min(min(imgVals1,imgVals12),imgVals13),...
%         min(min(imgVals2,imgVals12),imgVals23),...
%         min(min(imgVals3,imgVals13),imgVals23)];
    indx12 = find(min(imgVals1,imgVals12)-imgVals12==0); % find(min(imgVals2,imgVals12)-imgVals12==0)];
    indx13 = find(min(imgVals1,imgVals13)-imgVals13==0); % find(min(imgVals3,imgVals13)-imgVals13==0)];
    indx23 = find(min(imgVals2,imgVals23)-imgVals23==0); % find(min(imgVals3,imgVals23)-imgVals23==0)];
    beta = 90; 
%     weights1=makeweights(edges,imgVals1,beta);
%     weights2=makeweights(edges,imgVals2,beta);
%     weights3=makeweights(edges,imgVals3,beta);
    weights12=makeweights(edges,imgVals12,beta);
    weights13=makeweights(edges,imgVals13,beta);
    weights23=makeweights(edges,imgVals23,beta);
%     weights0=makeweights(edges,imgVals0,beta);
%     L0=laplacian(edges,weights0);    

    imgVals=img(:); 
    beta = 90; 
    weights=makeweights(edges,imgVals,beta);
    L=laplacian(edges,weights);
    ed = Xc*Yc; alpha = 1e-4; step = ed+1;
    L(1:step:end) = 0;
%     L11=laplacian(edges,weights1);    
%     L22=laplacian(edges,weights2);    
%     L33=laplacian(edges,weights3);    
    L12=laplacian(edges,weights12); L12(1:step:end) = -imgVals12;    
    L13=laplacian(edges,weights13); L13(1:step:end) = -imgVals13; 
    L23=laplacian(edges,weights23); L23(1:step:end) = -imgVals23; 
    LL = zeros(3*ed,3*ed); % [L,zeros(sz(L)),zeros(sz(L));zeros(sz(L)),L,zeros(sz(L));zeros(sz(L)),zeros(sz(L)),L];
    LL(indx12,ed+1:2*ed)=alpha*L12(indx12,:);
    LL(ed+indx12,1:ed)=alpha*L12(indx12,:);
    LL(indx13,2*ed+1:3*ed)=alpha*L13(indx13,:);
    LL(2*ed+indx13,1:ed)=alpha*L13(indx13,:);
    LL(ed+indx23,2*ed+1:3*ed)=alpha*L23(indx23,:);
    LL(2*ed+indx23,ed+1:2*ed)=alpha*L23(indx23,:);
    LL(1:ed,1:ed)=L; LL(ed+1:2*ed,ed+1:2*ed) = L; LL(2*ed+1:3*ed,2*ed+1:3*ed) = L;
    LL(1:3*Xc*Yc+1:end) = -sum(LL,1);
%     L = [L11,L12,L13;L12,L22,L23;L13,L23,L33];
end