MIND_descriptor2d(img,rad,sig)
I = edge(msk1,'Canny');

[min(min(A_Pri(1)*Txt(:,:,1))),max(max(A_Pri(1)*Txt(:,:,1)));...
    min(min(A_Pri(2)*Txt(:,:,2))),max(max(A_Pri(2)*Txt(:,:,2)));...
    min(min(A_Pri(3)*Txt(:,:,3))),max(max(A_Pri(3)*Txt(:,:,3)))]



figure,
thresh = 0.5;
subplot 231, imshow(reshape(probS(1:ed,2)>thresh,[Xc,Yc]).*mask)
subplot 232, imshow(reshape(probS(ed+1:2*ed,3)>thresh,[Xc,Yc]).*mask);
subplot 233, imshow(reshape(probS(2*ed+1:3*ed,4)>thresh,[Xc,Yc]).*mask);
    
subplot 234, imshow(X(:,:,1)>0)
subplot 235, imshow(X(:,:,2)>0);
subplot 236, imshow(X(:,:,3)>0);





[val,ind_] = max(probS,[],2);
for i = 1:sz(probS,2)-1
    m = (ind_(1+(i-1)*ed:i*ed))== i+1; % m2 = (ind_(ed+1:2*ed))==3; m3 = (ind_(2*ed+1:3*ed))==4;
    Seg(:,:,i) = reshape(m,[Xc,Yc]);
end
figure,
for i = 1:3
subplot (2,3,i), imshow(Seg(:,:,i)), if(i==1), title('Segmentation Results'), end
subplot (2,3,i+3), imshow(X(:,:,i)>0), if(i==1), title('Ground Truth'), end
end


figure,
thresh = 0.5;
subplot 231, imshow(reshape(probS(1:ed,2)>thresh,[Xc,Yc]))
subplot 232, imshow(reshape(probS(ed+1:2*ed,2)>thresh,[Xc,Yc]));
subplot 233, imshow(reshape(probS(2*ed+1:3*ed,2)>thresh,[Xc,Yc]));
    
subplot 234, imshow(X(:,:,1)>0)
subplot 235, imshow(X(:,:,2)>0);
subplot 236, imshow(X(:,:,3)>0);

I = reshape(probS(1:ed,2)>thresh,[Xc,Yc]);
J = X(:,:,1)>0;
figure, imshow(J)
figure, imshow(I)
figure, imshow(abs(I-J),[]) 


[Xc, Yc, ~]=size(img);
ed = Xc*Yc;
ind = find(img==0);
DPr(ind,2) = mxMat(ind);
DPr(ed+ind,3) = mxMat(ind);
DPr(2*ed+ind,4) = mxMat(ind);

beta = .95;
data_prior = exp(-beta*DPr);
nlabs = size(data_prior,2);
mn1 = min(data_prior,[],2);
mn2 = max(data_prior,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
data_prior = (data_prior - minmat) ./ (maxmat-minmat);
data_prior(isnan(data_prior)) = 1;

sz(find(isnan(data_prior)))
figure, imshow(reshape(data_prior(1:ed,2),[100,100]),[])

figure, imshow(reshape(data_prior(ed+1:2*ed,3),[100,100]),[])
figure, imshow(reshape(data_prior(2*ed+1:3*ed,4),[100,100]),[])

DPr1 = [min(min(M{2},M{3}),M{6}), M{1}, mxMat, mxMat;...
    min(min(M{1},M{3}),M{5}), mxMat, M{2}, mxMat;...
    min(min(M{1},M{2}),M{4}), mxMat, mxMat, M{3}];


nlabs = size(DPr,2);
mn1 = min(DPr,[],2);
mn2 = max(DPr,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
DPr = (DPr - minmat) ./ (maxmat-minmat);
figure,
subplot 131, imshow(reshape(1-DPr(1:ed,2),[100,100]),[])
subplot 132, imshow(reshape(1-DPr(ed+1:2*ed,3),[100,100]),[])
subplot 133, imshow(reshape(1-DPr(2*ed+1:3*ed,4),[100,100]),[])




    certhr1 = 0.001; % 05; %certhr2 = .1;
%     M = cell{7};
    for ii = 1:7
        thr1 = quantile(D{ii},certhr1,2);
        Sel = ( D{ii} < thr1); % (:,1) & D{i} < thr1(:,2));
        DD = D{ii} .* Sel;
        M{ii} = sum(DD,2) ./ sum(Sel,2);
%         M{ii} = min(D{ii},[],2); % (:,1);
        ta(:,ii) = sum(Sel,2);
        ta(:,ii) = ta(randperm(sz(ta,1)));
        mx(ii) = max(M{ii});
    end
    mxMat = max(mx(:))*ones(sz(M{1}));

    ss = 1000;
    figure, hist(ta(1:ss,2))




[Xc, Yc, ~]=size(img);
ed = Xc*Yc; ind = find(img==0); 
Dpr = ones
ii=0;
tic
for i = 1:10000
    ii = ii+1;
end
tm = toc;
thrsh = .05;
for i = 1:sz(probS,2)-1
    S(:,:,i) = reshape(probS(1+(i-1)*ed:i*ed,i+1)>thrsh,[Xc,Yc]);
end
figure,
for i = 1:sz(probS,2)-1 
    subplot(2,3,i), imshow(S(:,:,i))
    subplot (2,3,i+3), imshow(X(:,:,i)>0)
end


for i = 1:sz(probS,2)-1
    for j = 1:sz(probS,2)
        pb(:,:,i,j) = reshape(probS(1+(i-1)*ed:i*ed,j),[Xc,Yc]);
    end
end

figure,
for i = 1:sz(probS,2)-1 
    for j = 1:sz(probS,2)
        subplot(3,4,(i-1)*4+j), imshow(pb(:,:,i,j))
    end
end



    for ii = 1:7
        M{ii} = min(D{ii},[],2); % (:,1);
        mx(ii) = max(M{ii});
    end

Acc = zeros(ops.iteration,sz(mode,2));
tm = zeros(ops.iteration,sz(mode,2));
SE = strel('disk',2);   
for mdctr = 1:sz(mode,2)
    [probabilities_pro,tm2]=RandomWalkerImgSegPrior(LL,Dterm,gamma,alpha,beta,seeds(:),boundaryp,mode(mdctr),ops.DTrmMd);
    probS = probabilities_pro;
    mxv = max(probS,[],2);
    mnv = min(probS,[],2);
    probS = (probS - repmat(mnv,[1,sz(probS,2)])) ...
        ./ (repmat(mxv,[1,sz(probS,2)]) - repmat(mnv,[1,sz(probS,2)]));
    probS = probS ./ repmat(sum(probS,2),[1,sz(probS,2)]);
    %% 
    if mode(mdctr)==3
        [val,ind_] = max(probS,[],2);
        if(ops.segmd==1)
            for i = 1:sz(probS,2)-1
                m = (ind_(1+(i-1)*ed:i*ed))== i+1; 
                II = imfill(imopen(reshape(m,[Xc,Yc]),SE),'holes');
%                 II = imfill(Seg(:,:,i,mdctr),'holes');    
                CC = bwconncomp(II);
                if CC.NumObjects>1
                     for xx = 1:CC.NumObjects
                        p(xx) = sz(CC.PixelIdxList{xx},1);
                    end
                    [ix,v] = sort(p,'descend');
                    for yy = 2:len(v)
                        II(CC.PixelIdxList{v(yy)}) = 0;
                    end
                    clear p;
                end
                Seg(:,:,i,mdctr,iter) = II;
%                 Seg(:,:,i,mdctr,iter) = reshape(m,[Xc,Yc]);
            end
        else
            for i = 1:(sz(probS,1)/(Xc*Yc))
                m = (ind_(1+(i-1)*ed:i*ed))== 2; 
                Seg(:,:,i,mdctr,iter) = imopen(reshape(m,[Xc,Yc]),SE);
%                     Seg(:,:,i,mdctr) = reshape(m,[Xc,Yc]);
            end

        end
        tm(iter,mdctr) = tm1+tm2;            
    end
    if mode(mdctr) == 1
        if(ops.segmd==1)
            for i = 1:sz(probS,2)-1
                Seg(:,:,i,mdctr,iter) = reshape(probS(1+(i-1)*ed:i*ed,i+1)>thrsh,[Xc,Yc]);
            end
        else
            for i = 1:(sz(probS,1)/(Xc*Yc))
                Seg(:,:,i,mdctr,iter) = reshape(probS(1+(i-1)*ed:i*ed,2)>thrsh,[Xc,Yc]);
            end
        end
        tm(iter,mdctr) = tm2;
    end
    for i = 1:sz(Seg,3)
        Acc(iter,mdctr) = Acc(iter,mdctr) + sum(sum(abs( (sqz(Srcs(:,:,i,iter))>0) - sqz(Seg(:,:,i,mdctr,iter)) )));
    end
    segerrper(iter,mdctr) = (1 - Acc(iter,mdctr) / (3*Xc*Yc))*100; % (sz(find(img~=0),1))) * 100;

end



certhr1 = 0.1; %certhr2 = .1;
for ii = 1:7
%     thr1 = quantile(D{ii},certhr1,2);
%     Sel = ( D{ii} < thr1); % (:,1) & D{i} < thr1(:,2));
%     DD = D{ii} .* Sel;
%     M{ii} = sum(DD,2) ./ sum(Sel,2);
    M{ii} = min(D{ii},[],2); % (:,1);
    mx(ii) = max(M{ii});
end
mxMat = max(mx(:))*ones(sz(M{1}));

[Xc, Yc, ~]=size(img); ed = Xc*Yc;
Mat = [M{1},M{2},M{3},M{4},M{5},M{6},M{7}];
[vald, idd] = sort(Mat,2); 
notind = find( (vald(:,2)-vald(:,1)) ./ max(vald,[],2) <.02);

[~,ind] = min(Mat,[],2);
ind0 = find(img==0); ind(ind0) = 0;
ind(notind) = 0;

id1 = find(ind==1|ind==4|ind==5|ind==7);
id2 = find(ind==2|ind==4|ind==6|ind==7);
id3 = find(ind==3|ind==5|ind==6|ind==7);
if(seedmd==1)
    DPr = ones(3*ed,4);
    DPr(id1,2) = 0; DPr(ed+id2,3) = 0; DPr(2*ed+id3,4) = 0;
    DPr(ind0,1) = 0; DPr(ed+ind0,1) = 0; DPr(2*ed+ind0,1) = 0;
else
    DPr = ones(3*ed,2);
    DPr(id1,2) = 0; DPr(ed+id2,2) = 0; DPr(2*ed+id3,2) = 0;
    DPr(ind0,1) = 0; DPr(ed+ind0,1) = 0; DPr(2*ed+ind0,1) = 0;
end
DPr(find(ind==2),1)=0;DPr(find(ind==3),1)=0; DPr(find(ind==6),1)=0;
DPr(ed+find((ind==1)),1)=0;DPr((ed+find(ind==3)),1)=0; DPr((ed+find(ind==5)),1)=0;
DPr(2*ed+find((ind==1)),1)=0;DPr((2*ed+find(ind==2)),1)=0; DPr((2*ed+find(ind==4)),1)=0;

nlabs = size(DPr,2);
mn1 = min(DPr,[],2);
mn2 = max(DPr,[],2);
minmat = mn1(:,ones(1,nlabs));
maxmat = mn2(:,ones(1,nlabs));
DPr = (DPr - minmat) ./ (maxmat-minmat);
figure,
subplot 131, imshow(reshape(1-DPr(1:ed,2),[100,100]),[])
subplot 132, imshow(reshape(1-DPr(ed+1:2*ed,3),[100,100]),[])
subplot 133, imshow(reshape(1-DPr(2*ed+1:3*ed,4),[100,100]),[])


n=1; m = 100;
Mat(index{n}(m),:)
[ix,iy] = ind2sub(sz(img),index{n}(m)); figure, imshow(img), hold on, plot(iy,ix,'r*')

clr = {'r.-','g.-','b.-','c.-','y.-','m.-','k.-'};
step = 1000;
figure,
for i = 1:7
    subplot(2,4,i)
    for j = 1:step:sz(imv{i},1)
        plot(abs(fft(imv{i}(j,:))),clr{i}), hold on
    end
end
m = 2; n = 500;
subplot (2,4,8), plot(abs(fft(feature(index{m}(n),:))),'r.-')


for i = 1:7
    fftMat(:,:,i) = fft(imv{i},[],2);
end


%%
%     Prob1 = reshape(probS(1:ed,:),[Xc,Yc,sz(probS,2)]);
%     Prob2 = reshape(probS(ed+1:2*ed,:),[Xc,Yc,sz(probS,2)]);
%     Prob3 = reshape(probS(1+2*ed:3*ed,:),[Xc,Yc,sz(probS,2)]);
%     if(ops.seedmd==1)
%         figure,
%         subplot 341, imshow(Prob1(:,:,1));
%         subplot 342, imshow(Prob1(:,:,2));
%         subplot 343, imshow(Prob1(:,:,3));
%         subplot 344, imshow(Prob1(:,:,4));
%         subplot 345, imshow(Prob2(:,:,1));
%         subplot 346, imshow(Prob2(:,:,2));
%         subplot 347, imshow(Prob2(:,:,3));
%         subplot 348, imshow(Prob2(:,:,4));
%         subplot (3,4,9), imshow(Prob3(:,:,1));
%         subplot (3,4,10), imshow(Prob3(:,:,2));
%         subplot (3,4,11), imshow(Prob3(:,:,3));
%         subplot (3,4,12), imshow(Prob3(:,:,4));
% 
%         mask = img~=0;
%         figure,
%         thresh = 0.5;
%         subplot 131, imshow( (Prob1(:,:,2)>thresh)); %  .* mask)
%         subplot 132, imshow( (Prob2(:,:,3)>thresh)); %  .* mask);
%         subplot 133, imshow( (Prob3(:,:,4)>thresh)); %  .* mask);
% 
%     else
%         figure,
%         subplot 321, imshow(Prob1(:,:,1));
%         subplot 322, imshow(Prob1(:,:,2));
%         subplot 323, imshow(Prob2(:,:,1));
%         subplot 324, imshow(Prob2(:,:,2));
%         subplot 325, imshow(Prob3(:,:,1));
%         subplot 326, imshow(Prob3(:,:,2));
%         
% %         mask = img~=0;
%         figure,
%         thresh = 0.5;
%         subplot 131, imshow((Prob1(:,:,2)>thresh)); %  .* mask)
%         subplot 132, imshow((Prob2(:,:,2)>thresh)); %  .* mask);
%         subplot 133, imshow((Prob3(:,:,2)>thresh)); %  .* mask);
%         
%     end
%%
%     figure,
%     thresh = 0.5;
%     SE = strel('disk',5);
%     subplot 131, imshow(imopen(reshape(probS(1:ed,2)>thresh,[Xc,Yc]).*mask,SE))
%     subplot 132, imshow(imopen(reshape(probS(ed+1:2*ed,3)>thresh,[Xc,Yc]).*mask,SE));
%     subplot 133, imshow(imopen(reshape(probS(2*ed+1:3*ed,4)>thresh,[Xc,Yc]).*mask,SE));

% % [LL,Dterm,Txture] = makeWeights3Srcs(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img,Texture,A_Pri1,X);
% [LL,Dterm] = makeWeights3Srcs_v1(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img,X); % A_Pri1, 
% % [LL,Dterm] = makeWeights3Srcs_v2(opt.TxrPth,opt.Txtf1,opt.Txtf2,opt.Txtf3,img,X);

% % [points, edges]=lattice(Xc,Yc,1);
% % imgVals=img(:); 
% % beta = 90; 
% % weights=makeweights(edges,imgVals,beta);
% % L=laplacian(edges,weights);
% % LL = [L,zeros(sz(L)),zeros(sz(L));zeros(sz(L)),L,zeros(sz(L));zeros(sz(L)),zeros(sz(L)),L];
% % alpha = 0; LL = [L,alpha*L12,alpha*L13;alpha*L12,L,alpha*L23;alpha*L13,alpha*L23,L]; % 1e-1
% % seeds = seedsOrg; boundaryp = boundaryOrg;
%%
% figure,
% subplot 321, imshow(reshape(probS(1:ed,1),[Xc,Yc]));
% subplot 322, imshow(reshape(probS(1:ed,2),[Xc,Yc]));
% subplot 323, imshow(reshape(probS(ed+1:2*ed,1),[Xc,Yc]));
% subplot 324, imshow(reshape(probS(ed+1:2*ed,2),[Xc,Yc]));
% subplot 325, imshow(reshape(probS(2*ed+1:3*ed,1),[Xc,Yc]));
% subplot 326, imshow(reshape(probS(2*ed+1:3*ed,2),[Xc,Yc]));

% figure,
% subplot 331, imshow(reshape(probS(1:ed,1),[Xc,Yc]));
% subplot 332, imshow(reshape(probS(1:ed,2),[Xc,Yc]));
% subplot 333, imshow(reshape(probS(1:ed,3),[Xc,Yc]));
% subplot 334, imshow(reshape(probS(ed+1:2*ed,1),[Xc,Yc]));
% subplot 335, imshow(reshape(probS(ed+1:2*ed,2),[Xc,Yc]));
% subplot 336, imshow(reshape(probS(ed+1:2*ed,3),[Xc,Yc]));
% subplot 337, imshow(reshape(probS(2*ed+1:3*ed,1),[Xc,Yc]));
% subplot 338, imshow(reshape(probS(2*ed+1:3*ed,2),[Xc,Yc]));
% subplot 339, imshow(reshape(probS(2*ed+1:3*ed,3),[Xc,Yc]));
%%
% iter = 0;
% ed = Xc*Yc;
% msk = calc_mask3Src(probS, img); 
% msk1 = reshape(msk(1:ed),[Xc,Yc]);
% msk2 = reshape(msk(ed+1:2*ed),[Xc,Yc]);
% msk3 = reshape(msk(2*ed+1:3*ed),[Xc,Yc]);
% 
% grdmsk1 = X(:,:,1)>0; grdmsk2 = X(:,:,2)>0; grdmsk3 = X(:,:,3)>0; 
% seg_acc(iter+1) = 100 - (sz(find(grdmsk1~=msk1),1) + sz(find(grdmsk2~=msk2),1) + sz(find(grdmsk3~=msk3),1)) ./ (3*Xc*Yc)*100;
% ovlp = (sz(find(grdmsk1&grdmsk2),1)+sz(find(grdmsk1&grdmsk3),1)+...
%     sz(find(grdmsk2&grdmsk3),1)-sz(find(grdmsk1&grdmsk2&grdmsk3),1));
% 
% [val_, msk_] = max(probS_,[],2);
% msk1_ = reshape(msk_==2,[Xc,Yc]);
% msk2_ = reshape(msk_==3,[Xc,Yc]);
% msk3_ = reshape(msk_==4,[Xc,Yc]);
% seg_acc_(iter+1) = 100 - (sz(find(grdmsk1~=msk1_),1) + sz(find(grdmsk2~=msk2_),1) + sz(find(grdmsk3~=msk3_),1)) ./ (3*Xc*Yc)*100;
% %% Showing Results
% h = figure(1);
% subplot 331, imshow(msk1), title([sprintf('First Row: Proposed Seg Results, Acc: %.2f', seg_acc(iter+1)), '%']) 
% subplot 332, imshow(msk2), 
% subplot 333, imshow(msk3), 
% subplot 334, imshow(msk1_), title([sprintf('Second Row: RW Seg Results, Acc: %.2f', seg_acc_(iter+1)), '%']) 
% subplot 335, imshow(msk2_), 
% subplot 336, imshow(msk3_), 
% subplot (3,4,9), imshow(img), hold on, plot(lndmrkj,lndmrki,'r*'), title('Third Row: Ground Truth with the initial seeds') 
% subplot (3,4,10), imshow(grdmsk1),
% subplot (3,4,11), imshow(grdmsk2),
% subplot (3,4,12), imshow(grdmsk3),
    