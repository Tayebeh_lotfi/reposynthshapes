function C = myreadimage(pxds, index)
    C = niftiread(pxds.Files{index});
end