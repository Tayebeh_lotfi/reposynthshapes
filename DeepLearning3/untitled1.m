[~, yIntegers1] = max(predictions, [], 3);
[~, tIntegers1] = max(response, [], 3);


yIntegers2 = predictions > .5;
tIntegers2 = response > 0;
numObservations = size(yIntegers2,1) * size(yIntegers2,2) * size(yIntegers2, 3)* size(yIntegers2, 4);
numObservations1 = size(yIntegers1,1) * size(yIntegers1,2) * size(yIntegers1, 4);
numObservations2 = numel(find(response>0));
acc1 = 100 * ( sum(sum(sum( yIntegers1 == tIntegers1, 1 ), 2), 4)/ numObservations1 );

acc2 = 100 * (sum( sum(sum(sum( yIntegers2 == tIntegers2, 1 ), 2), 3), 4)/ numObservations );
acc3 = 100 * (sum( sum(sum(sum( yIntegers2 .* tIntegers2, 1 ), 2), 3), 4)/ numObservations2 );


figure,
t = 1;
for i = 1:4
subplot 121, imshow(yIntegers2(:,:,i,t))
subplot 122, imshow(tIntegers2(:,:,i,t))
pause
end