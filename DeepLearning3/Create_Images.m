clc, close all, clear,
addpath(genpath('../Code'));
load learn.mat
for ii = 1:size(image,3)
    img = repmat(image(:,:,ii),[1,1,3]);
    Lbl = Lbls(:,:,:,ii);
    imwrite(img,['Learn\Image\Img',num2str(ii),'.tiff']);
    niftiwrite(Lbl,['Learn\Label\Lbl',num2str(ii),'.nii']);
end