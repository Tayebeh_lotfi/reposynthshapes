function v1n = resc(v1, mn, mx)
%%%%%%%%%%%%%%%%%%%%%%%%%%
% function v1 = resc(v1, mn, mx)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%
 
v1 = double(v1);
d = mnx(  v1 );

if len(unique(v1))==1 v1n =ones(sz(v1)); return; end
    
if d(1) <0
v1n = ( v1+ abs( d(1) ) )/diff(d);
else
v1n = ( v1- abs( d(1) ) )/diff(d);
end

if nargin>1
    d2 = [mn mx ]; 
    if d2(1) <0
    v1n=(v1 + abs( d2(1) ))/ diff(d2);
    else
    v1n=(v1 - abs( d2(1) ))/ diff(d2);
    end
end


end