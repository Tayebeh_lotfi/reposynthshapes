clc; close all; clear
imgDir = 'Learn\Image'; % 'Learn\ImgTst'; %

imds = imageDatastore(imgDir);
% Display one of the images.

I = readimage(imds, 1);
figure
imshow(I)

classes = [
    "Obj1"
    "Obj2"
    "Obj3"
    "Obj4"
    ];

labelIDs = SynthPixelLabelIDs_v1();

labelDir = 'Learn\Label\'; % 'Learn\LblTst\'; %

% pxds = mypixelLabelDatastore(labelDir,classes,labelIDs);
pxds = mpixelLabelDatastore(labelDir,classes,labelIDs,'ReadFcn',@customerRead);
% Read and display one of the pixel-labeled images by overlaying it on top of an image.

C = readimage(pxds, 1);

%%


tbl = table();
tbl.Name = pxds.ClassNames; % cellstr(classes);
counts = zeros(1,size(classes,1));
N = zeros(1,size(classes,1));

for ii = 1:size(pxds.Files,1)
    I = niftiread(pxds.Files{ii});
    pix = numel(I(:,:,1));
    c = 0;
    for jj = 1:size(I,3)
        tmp(jj) = numel(find(I(:,:,jj) > 0 ));
        if (numel(find(I(:,:,jj) == 0 )) < numel(I(:,:,jj)))
            c = c + numel(I(:,:,jj));
        end
    end
    counts = counts + tmp;
    idx = find(tmp);
    N(idx) = N(idx) + c;
end
tbl.PixelCount      = counts';
tbl.ImagePixelCount = N'; 
% 

%%

% Visualize the pixel counts by class.

frequency = tbl.PixelCount/sum(tbl.PixelCount);

figure
bar(1:numel(classes),frequency)
xticks(1:numel(classes))
xticklabels(tbl.Name)
xtickangle(45)
ylabel('Frequency')


%%
imageFolder = 'Learn\Image\'; % 'Learn\ImgTst'; %
imds = imageDatastore(imageFolder);

labelFolder = 'Learn\Label\'; % 'Learn\LblTst'; %
pxds = mpixelLabelDatastore(labelFolder,classes,labelIDs,'ReadFcn',@customerRead);
%%
[imdsTrain, imdsTest, pxdsTrain, pxdsTest] = partitionData_v2(imds,pxds,'ReadFcn',@customerRead);
% The 60/40 split results in the following number of training and test images:

numTrainingImages = numel(imdsTrain.Files);
numTestingImages = numel(imdsTest.Files);


% As shown earlier, the classes in CamVid are not balanced. To improve training, you can use class weighting to balance the classes. Use the pixel label counts computed earlier with countEachLabel and calculate the median frequency class weights [1].

imageFreq = tbl.PixelCount ./ tbl.ImagePixelCount;
classWeights = median(imageFreq) ./ imageFreq;

% As shown earlier, the classes in CamVid are not balanced. To improve training, 
% you can use class weighting to balance the classes. Use the pixel label counts computed earlier with 
% countEachLabel and calculate the median frequency class weights [1].

imageFreq = tbl.PixelCount ./ tbl.ImagePixelCount;
classWeights = median(imageFreq) ./ imageFreq;

% Specify the class weights using a pixelClassificationLayer.

pxLayer = pixelClassificationLayer('Name','labels','ClassNames', tbl.Name, 'ClassWeights', classWeights);

layers = [
    imageInputLayer([200 200 3],'Name','inputImage')

    convolution2dLayer([3 3],64,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv1_1')
    batchNormalizationLayer('Name','bn_conv1_1')
    reluLayer('Name','relu1_1')
    
    convolution2dLayer([3 3],64,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv1_2')
    batchNormalizationLayer('Name','bn_conv1_2')
    reluLayer('Name','relu1_2')
    maxPooling2dLayer(2,'Stride',[2 2],'Name','pool1','HasUnpoolingOutputs',true)

    convolution2dLayer([3 3], 128,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv2_1')
    batchNormalizationLayer('Name','bn_conv2_1')
    reluLayer('Name','relu2_1')
    
    convolution2dLayer([3 3], 128,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv2_2')
    batchNormalizationLayer('Name','bn_conv2_2')
    reluLayer('Name','relu2_2')
    maxPooling2dLayer(2,'Stride',[2 2],'Name','pool2','HasUnpoolingOutputs',true)    
    
    convolution2dLayer([3 3], 256,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv3_1')
    batchNormalizationLayer('Name','bn_conv3_1')
    reluLayer('Name','relu3_1')
    
    convolution2dLayer([3 3], 256,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv3_2')
    batchNormalizationLayer('Name','bn_conv3_2')
    reluLayer('Name','relu3_2')
    
    convolution2dLayer([3 3], 256,'Padding',[1 1 1 1],'Stride',[1 1],'Name','conv3_3')
    batchNormalizationLayer('Name','bn_conv3_3')
    reluLayer('Name','relu3_3')
    maxPooling2dLayer(2,'Stride',[2 2],'Name','pool3','HasUnpoolingOutputs',true)    
    
    
    maxUnpooling2dLayer('Name','decoder3_unpool');
    convolution2dLayer([3 3], 256,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder3_conv3')
    batchNormalizationLayer('Name','decoder3_bn_3')
    reluLayer('Name','decoder3_relu3')
    
    convolution2dLayer([3 3], 256,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder3_conv2')
    batchNormalizationLayer('Name','decoder3_bn_2')
    reluLayer('Name','decoder3_relu2')

    convolution2dLayer([3 3], 128,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder3_conv1')
    batchNormalizationLayer('Name','decoder3_bn_1')
    reluLayer('Name','decoder3_relu1')
    
    maxUnpooling2dLayer('Name','decoder2_unpool');
    convolution2dLayer([3 3], 128,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder2_conv2')
    batchNormalizationLayer('Name','decoder2_bn_2')
    reluLayer('Name','decoder2_relu2')

    convolution2dLayer([3 3], 64,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder2_conv1')
    batchNormalizationLayer('Name','decoder2_bn_1')
    reluLayer('Name','decoder2_relu1')
    
    maxUnpooling2dLayer('Name','decoder1_unpool');
    convolution2dLayer([3 3], 64,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder1_conv2')
    batchNormalizationLayer('Name','decoder1_bn_2')
    reluLayer('Name','decoder1_relu2')
    
    convolution2dLayer([3 3], 4,'Padding',[1 1 1 1],'Stride',[1 1],'Name','decoder1_conv1')
    batchNormalizationLayer('Name','decoder1_bn_1')
    reluLayer('Name','decoder1_relu1')
    
    softmaxLayer('Name','softmax')
    ];

lgraph = layerGraph(layers);

lgraph = addLayers(lgraph, pxLayer);
lgraph = connectLayers(lgraph, 'softmax' ,'labels');



lgraph = connectLayers(lgraph,'pool1/indices','decoder1_unpool/indices');
lgraph = connectLayers(lgraph,'pool1/size','decoder1_unpool/size');

lgraph = connectLayers(lgraph,'pool2/indices','decoder2_unpool/indices');
lgraph = connectLayers(lgraph,'pool2/size','decoder2_unpool/size');

lgraph = connectLayers(lgraph,'pool3/indices','decoder3_unpool/indices');
lgraph = connectLayers(lgraph,'pool3/size','decoder3_unpool/size');


% figure
% plot(lgraph)
%%

options = trainingOptions('sgdm', ...
    'Momentum', 0.9, ...
    'InitialLearnRate', 1e-3, ...
    'L2Regularization', 0.0005, ...
    'MaxEpochs', 100, ... % 100
    'MiniBatchSize', 2, ...
    'Shuffle', 'every-epoch', ...
    'VerboseFrequency', 2, ...
    'ExecutionEnvironment', 'gpu');

augmenter = imageDataAugmenter('RandXReflection',true,...
    'RandXTranslation', [-10 10], 'RandYTranslation',[-10 10]);

% Combine the training data and data augmentation selections using pixelLabelImageSource. The pixelLabelImageSource reads batches of training data, applies data augmentation, and sends the augmented data to the training algorithm.

datasource = pixelLabelImageSource(imdsTrain,pxdsTrain,...
    'DataAugmentation',augmenter);


[net, info] = trainNetwork(datasource,lgraph,options);

save('myownnet','net')
% save('myownnetnew','net')

