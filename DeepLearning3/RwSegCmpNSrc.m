clc, close all, clear,
addpath(genpath('../Code'));
%% generate data
opt.FixA = false; opt.Src = 4; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 15; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 25; opt.width = 25; opt.mag = 12; opt.smpl = 3;
smpl = opt.smpl;
opt.TxrPth = '../ImageTextures/'; 
opt.Txtf1 = '1.3.03.tiff'; 
opt.Txtf2 = '1.3.06.tiff'; 
opt.Txtf3 = '1.3.09.tiff'; 
opt.Txtf4 = '1.3.11.tiff';
for ii = 1:3000
    [~, A_Pri, MixMat, LblMat, X, X_Orig, Y, Shapes] = Create_Samples_(opt);

    for jj = 1: smpl
        img = repmat(Y(:,:,jj),[1,1,3]);
        Lbl(:,:,1) = 1*uint8(Shapes(:,:,1,jj)>0);
        Lbl(:,:,2) = 2*uint8(Shapes(:,:,2,jj)>0);
        Lbl(:,:,3) = 3*uint8(Shapes(:,:,3,jj)>0);
        Lbl(:,:,4) = 4*uint8(Shapes(:,:,4,jj)>0);
        
        imwrite(img,['Learn\Image\Img',num2str((ii-1)*smpl+jj),'.tiff']);
        niftiwrite(Lbl,['Learn\Label\Lbl',num2str((ii-1)*smpl+jj),'.nii']);
        label(:,:,:,jj) = Lbl;
    end
    Lbls(:,:,:,(ii-1)*smpl+1:ii*smpl) = label;

    image(:,:,(ii-1)*smpl+1:ii*smpl) = Y;
    src(:,:,:,(ii-1)*smpl+1:ii*smpl) = X;
    srcOrg(:,:,:,ii) = X_Orig;
    MixMatAll(:,:,ii) = MixMat;
    
end

save('Learn\learn.mat','image','src','Lbls','srcOrg','MixMatAll','-v7.3');