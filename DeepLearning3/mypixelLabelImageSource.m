function datasource = mypixelLabelImageSource(imdsTrain,pxdsTrain,fieldName,augmenter)
    datasource.Images = imdsTrain.Files;
    datasource.PixelLabelData = pxdsTrain.Files;
    datasource.ClassNames = pxdsTrain.ClassNames;
    if( strcmp(fieldName,'DataAugmentation') )
        datasource.DataAugmentation = augmenter;
    end
    datasource.OutputSize = [];
    datasource.OutputSizeMode = 'resize';
    datasource.ColorPreprocessing = 'none';
    datasource.BackgroundExecution = 0;
end