function g=cmapg(num)
if nargin==1
g=colormap (gray(num));
else
g=colormap (gray(255));
end