function sz =sz(arg, arg2)
if nargin==1
   sz= size( arg );
else
   sz= size( arg , arg2);
end