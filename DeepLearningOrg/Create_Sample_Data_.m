function [A, B, C, D] = Create_Sample_Data_(opts)

    optsdef=struct('sz_smpls',200,'Obj', 1, 'valA',0.85,'valB',0.15,'valC',0.35,'valD',0.65,...
        'ScaleA',1,'ScaleB',1,'ScaleC',1, 'ScaleD',1, ...
        'CenY1',105,'CenX1',145, 'CenY2',145,'CenX2',70,'CenX3',25,'CenY3',55,'CenX4',75,'CenY4',115, ...
        'NoisePower',-100, 'tolerance', false, 'Scale', 10, 'scale_var', 2.5, 'LargeParmtr', false);
    if ~exist('opts','var')
        opts=struct;
    end
    if(opts.tolerance == true)
        r = opts.scale_var*randn(1,12);
        r(1:4) = 1 + r(1:4) * opts.Scale; 
        if opts.LargeParmtr == false
            r(5:12) = 1 + r(5:12)/3; 
        else
            r(5:12) = 1 + r(5:12);
        end
        %r(4:9) = floor(opts.Scale*r(4:9));
        opts.ScaleA = opts.ScaleA * r(1); opts.ScaleB = opts.ScaleB * r(2); opts.ScaleC = opts.ScaleC * r(3); opts.ScaleD = opts.ScaleD * r(4);  
        opts.CenY1 = ceil(opts.CenY1 * r(5)); opts.CenX1 = ceil(opts.CenX1 * r(6)); opts.CenY2 = ceil(opts.CenY2 * r(7)); 
        opts.CenX2 = ceil(opts.CenX2 * r(8)); opts.CenY3 = ceil(opts.CenY3 * r(9)); opts.CenX3 = ceil(opts.CenX3 * r(10));
        opts.CenX4 = ceil(opts.CenX4 * r(11)); opts.CenY4 = ceil(opts.CenY4 * r(12)); 
    end
    ops=scanparam(optsdef,opts);
    M = ops.sz_smpls; N = ops.sz_smpls;
    A = double(zeros(M,N));
    B = A; C = A; D = A;

    rad = round(20*ops.ScaleA); 
    for i = min(M-3, max(1,ops.CenY1-rad)):min(M,ops.CenY1+rad)
        for j = min(N-3, max(1,ops.CenX1-rad)):min(N,ops.CenX1+rad)
            if((i-ops.CenY1)^2 + (j-ops.CenX1)^2 <= rad^2)
                A(i,j) = ops.valA; % 0.15;
            end
        end
    end

    rad = round(20*ops.ScaleB); 
    id = max(3, max(1,ops.CenY2-rad));
    iu = min(M-3,ops.CenY2+rad);
    jd = max(3, max(1,ops.CenX2-rad));
    ju = min(N-3,ops.CenX2+rad);
    B(id:iu,jd:ju) = ops.valB;
%     for i = min(M-3, max(1,ops.CenY2-rad)):min(M,ops.CenY2+rad)
%         for j = min(N-3, max(1,ops.CenX2-rad)):min(N,ops.CenX2+rad)
%             if((i-ops.CenY2)^2 + (j-ops.CenX2)^2 <= rad^2)
%                 B(i,j) = ops.valB; % 0.15;
%             end
%         end
%     end

    rad1 = round(20*ops.ScaleC); 
    rad2 = round(30*ops.ScaleC);
    id = min(3, max(1,ops.CenY3-rad1));
    iu = min(M-3,ops.CenY3+rad1);
    jd = min(3, max(1,ops.CenX3-rad2));
    ju = min(N-3,ops.CenX3+rad2);
    C(id:iu,jd:ju) = ops.valC;
%     rad = round(20*ops.ScaleC); 
%     for i = min(M-3, max(1,ops.CenY3-rad)):min(M,ops.CenY3+rad)
%         for j = min(N-3, max(1,ops.CenX3-rad)):min(N,ops.CenX3+rad)
%             if((i-ops.CenY3)^2 + (j-ops.CenX3)^2 <= rad^2)
%                 C(i,j) = ops.valC; % 0.15;
%             end
%         end
%     end

%     D = zeros(M,N);
    rad_y = round(35*ops.ScaleD);
    rad_x = round(35*ops.ScaleD);
    
    for i = min(M-3, max(1,ops.CenY4)):min(M,ops.CenY4+rad_y)
        for j = min(N-3, max(1,ops.CenX4)):min(N,ops.CenX4+rad_x)
            if((j-ops.CenX4)/(i-ops.CenY4) <= rad_x/rad_y)
                D(i,j) = ops.valD;
            end
        end
    end
    
%     
%     figure, imshow(D)
%     hold on, plot(min(N,ops.CenX4+rad_x), min(M,ops.CenY4+rad_y), 'b*')
%     hold on, plot(ops.CenX4,ops.CenY4,'r*')
%     
%     
%     D = zeros(M,N);
%     rad_y = round(25*ops.ScaleD);
%     rad_x = round(25*ops.ScaleD);
    
    for i = max(3,ops.CenY4+rad_y):-1:max(1, ops.CenY4)
        for j = max(3,ops.CenX4-rad_x):max(1, ops.CenX4)
            if((ops.CenX4 - j) / (i-ops.CenY4) <= rad_x/rad_y)
                D(i,j) = ops.valD;
            end
        end
    end
    
    D(201:end,:) = [];
    D(:,201:end) = [];
%     figure, imshow(D)
%     hold on, plot(min(N,ops.CenX4+rad_x), min(M,ops.CenY4+rad_y), 'g*')
%     hold on, plot(max(3,ops.CenX4-rad_x),max(3,ops.CenY4+rad_y), 'b*')
%     hold on, plot(ops.CenX4,ops.CenY4,'r*')
%     
%     
%     rad = round(20*ops.ScaleD);
%     for i = min(M-3, max(1,ops.CenY4-rad)):min(M,ops.CenY4+rad)
%         for j = min(N-3, max(1,ops.CenX4-rad)):min(N,ops.CenX4+rad)
%             if((i-ops.CenY4)^2 + (j-ops.CenX4)^2 <= rad^2)
%                 D(i,j) = ops.valD; % 0.15;
%             end
%         end
%     end
end