function imds = resizeImages(imds, imageFolder)
% Resize images to [360 480].

while hasdata(imds)
    % Read an image.
    [I,info] = read(imds);

    % Resize image.
    I = imresize(I,[360 480]);

    % Write to disk.
    [~, filename, ext] = fileparts(info.Filename);
    imwrite(I,[imageFolder filename ext])
end

imds = imageDatastore(imageFolder);
end