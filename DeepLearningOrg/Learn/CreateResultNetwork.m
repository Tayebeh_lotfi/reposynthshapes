clc, close all, clear,
addpath(genpath('../Code'));
%% generate data
opt.FixA = false; opt.Src = 4; opt.thresh = .25; opt.nVarIntens = -45; Ntrial = 20;% 20; % 
opt.isVarIntens = true; opt.Sigpow = 15; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 25; opt.width = 25; opt.mag = 12; showit = 0;
opt.TxrPth = '../ImageTextures/'; 
opt.Txtf1 = '1.3.03.tiff'; 
opt.Txtf2 = '1.3.06.tiff'; 
opt.Txtf3 = '1.3.09.tiff'; 
opt.Txtf4 = '1.3.11.tiff';
seg_cnt = zeros(Ntrial,1);

load mynet;
for iter = 1:Ntrial
    [A_Pri, ~, X, Y, HasOverlap] = Create_Samples_(opt);
    % [A_Pri, ~, X, Y, HasOverlap] = Create_Samples(opt);
    while(HasOverlap ==0)
        [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);
    %     [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);

    end
    img = repmat(Y,[1,1,3]);
    Srcs = X;
    [Xc_,Yc_,NObj] = size(Srcs);
    Lbls = find_lables(Srcs);
    %% Ground Truth Seg
    seg(:,:,1) = (Srcs(:,:,1)==0 & Srcs(:,:,2)==0 & Srcs(:,:,3)==0 & Srcs(:,:,4)==0);
    seg(:,:,2) = Srcs(:,:,1)>0;
    seg(:,:,3) = Srcs(:,:,2)>0;
    seg(:,:,4) = Srcs(:,:,3)>0;
    seg(:,:,5) = Srcs(:,:,4)>0;
    %% Learning Seg
    imageSize = [360 480 3];
    I = imresize(img,[360 480]);

    % newoutput = synthnet(newinput);
    C = semanticseg(I, net);
    C = uint8(C);
    [Xc,Yc] = size(C);
    res = zeros(Xc_,Yc_,NObj+1);
    tmp = zeros(Xc,Yc); tmp1 = tmp; tmp2 = tmp; tmp3 = tmp; tmp4 = tmp;
    for ii = 1:NObj
        switch ii
            case 1
                x1 = find(C==2|C==6|C==7|C==8|C==12|C==13|C==14|C==16);
                tmp1(x1) = 1;
                tmp1 = imresize(tmp1,[Xc_,Yc_]);
            case 2
                x1 = find(C==3|C==6|C==9|C==10|C==12|C==13|C==15|C==16);
                tmp2(x1) = 1;
                tmp2 = imresize(tmp2,[Xc_,Yc_]);
            case 3
                x1 = find(C==4|C==7|C==9|C==11|C==12|C==14|C==15|C==16);
                tmp3(x1) = 1;
                tmp3 = imresize(tmp3,[Xc_,Yc_]);
            case 4
                x1 = find(C==5|C==8|C==10|C==11|C==13|C==14|C==15|C==16);
                tmp4(x1) = 1;
                tmp4 = imresize(tmp4,[Xc_,Yc_]);
        end
    end

    tmp(find(C==1)) = 1;
    tmp = imresize(tmp,[Xc_,Yc_]);
    res(:,:,1) = tmp; res(:,:,2) = tmp1; res(:,:,3) = tmp2; 
    res(:,:,4) = tmp3; res(:,:,5) = tmp4;
    
    for ii = 2:NObj+1
        gmsk = seg(:,:,ii)>0;
        rmsk = res(:,:,ii)>0;
        seg_cnt(iter) = seg_cnt(iter) + (sz( find(gmsk~=rmsk ) , 1) );
    end
    ovlp = (sz(find(seg(:,:,1)==0),1));
    acc(iter) = 100 - seg_cnt(iter) / (NObj*Xc_*Yc_)*100;
    iou(iter) = 100 - seg_cnt(iter) / (NObj*ovlp)*100;
    
    
    %% Compare
    if(showit==1)
        figure,
        for ii = 1:5
            subplot(3,5,ii), imshow(res(:,:,ii)), if(ii==1), title('Segmentation (Deep Learning)'), end
            subplot(3,5,ii+5), imshow(seg(:,:,ii)), if(ii==1), title('Ground Truth'), end
            if(ii<5), subplot(3,5,ii+11), imshow(Srcs(:,:,ii)), end
        end
        subplot(3,5,11), imshow(img), title('Mixture and Original Sources')
    end
end
%%
% B = labeloverlay(I, C, 'Colormap', cmap, 'Transparency',0.4);
% figure
% imshow(B)
% pixelLabelColorbar(cmap, classes);
% % L = uint8(Lbls);
% L = imresize(L,[360 480],'nearest');