function newVals=RWinvert(L, index, labels, data_prior, alpha, isz)

N=length(L);
antiIndex=1:N;
antiIndex(index)=[];
numOfGroups = size(labels,2);

%Apply the modification in Grady paper
%http://cns.bu.edu/~lgrady/grady2005multilabel.pdf
%get the lambda for the unseeded points only

if exist('data_prior','var')     
%     data_prior(index,:)=[];
    Prior_Mat = speye(size(L,1)); %0*L
    Prior_Vec = sum(data_prior,2);
    Prior_Mat(1:prod(isz)+1:end) = Prior_Vec;
%     alpha = 5000;
    bTotal = data_prior; % - alpha*L(antiIndex,index)*(labels); % ( 1-alpha )*data_prior;
    L_u = alpha*L + Prior_Mat; % (alpha)*L(antiIndex,antiIndex);
%     sizeLu = size(L_u,1);
%     L_u(1:sizeLu+1:end) = L_u(1:sizeLu+1:end) + (1-alpha);
%     for i = 1 : sizeLu,
%         L_u(i,i) = L_u(i,i) + (1-alpha);
%     end
else
    bTotal = - L(antiIndex,index)*(labels);
    L_u = L(antiIndex,antiIndex);
end

disp 'Now inverts the system... '
%replace the following with an iterative solver
x=L_u\bTotal;

if exist('data_prior','var')  
    newVals = x;
else
    newVals=zeros([length(L) numOfGroups]); % previous version
    newVals(index,:)=labels;
    newVals(antiIndex,:)=x;
end

disp 'done'

