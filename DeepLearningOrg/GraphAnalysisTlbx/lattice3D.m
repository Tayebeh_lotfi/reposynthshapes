function [points,edges] = lattice3D(X,Y,Z, connect )
%Function [points,edges]=lattice(X,Y,connectivity ) generates a 2D, 
%"4-connected" Cartesian lattice with dimensions X and Y. 
%
%Inputs:    X/Y - The dimensions of the lattice (i.e., the number of 
%               nodes will equal X*Y)
%           connect - Optional iInteger indicating the connectivity of 
%               the lattice
%               0 - 4-connected (Default)
%               1 - 8-connected
%               K>1 - Radially connected with radius = K
%
%Outputs:   points - Nx2 lattice nodes
%           edges - Mx2 lattice edges
%
%
%6/5/03 - Leo Grady
%========================================================================%

%Check for single point
if X*Y*Z==1
    points=[1;1;1];
    edges=[];
    return
end

%Read connect argument
if nargin <= 3
    connect=0;
end


    %Generate points
if(connect == 1)
    %%
    rangex=1:(X);
    rangey=1:(Y);
    rangez=1:(Z);
%     [x y]=meshgrid(rangey,rangex);
    [x y z]=ndgrid(rangex,rangey,rangez);
    points=[x(:),y(:),z(:)];
%     SeedInd = sub2ind( [ X Y Z], points(:,1), points(:,2),points(:,3));
%     SeedInd = reshape(SeedInd, [X Y Z]);
    SeedInd = reshape( 1:X*Y*Z, [X Y Z] );
    e1 = [];
    for i = 1 : X-1
     s= [ reshape(SeedInd(i,:,:), 1, [])' reshape(SeedInd(i+1,:,:), 1, [])']';             
     e1 = [e1 s];
    end    
    e2 = [];    
    for j = 1 : Y-1        
        s=[ reshape(SeedInd(:,j,:), 1, [])' reshape(SeedInd(:,j+1,:), 1, [])' ]';
        e2 = [e2 s];
    end
    e3 = [];
    for k = 1 : Z-1
        s= [reshape(SeedInd(:,:,k), 1, [])' reshape(SeedInd(:,:,k+1), 1, [])']';                
        e3 = [e3 s];
    end
    edges=[e1 e2 e3]';

end
if 0
    %%
    clf
    for i  = 1 : len(edges),    
        [p1 p2 p3] = ind2sub([X Y Z], edges(i,1));
        [q1 q2 q3] = ind2sub([X Y Z], edges(i,2));
        plot3([p1 q1], [p2 q2], [p3 q3]), hold on
    end
    hold on, plot3(points(:,1), points(:,2), points(:,3), 'r*');
end
end

