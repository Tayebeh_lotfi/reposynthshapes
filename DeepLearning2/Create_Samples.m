function [A_Pri1, A_Pri, tar, src, HasOverlap] = Create_Samples(ops) % , AllOverlap
%% Initializing, Creating Input Images:
    Obj = 1; % 10;
    ops.Src = 4;
    opt.Obj = Obj; opt.sz_smpls = 200; opt.valA = 1; opt.valB = 1; opt.valC = 1; opt.valD = 1;%0.25;
    if ops.LargeParmtr == false
        opt.ScaleA = 3.7; opt.ScaleB = 3.4; opt.ScaleC = 3.3; opt.ScaleD = 3.2; % 1.9. 1.6
    else
        opt.ScaleA = 4.9; opt.ScaleB = 4.3; opt.ScaleC = 4.5; opt.ScaleD = 4.1;% 2.1, 1.9
    end
    opt.CenY1 = 105; opt.CenX1 = 145; opt.CenY2 = 145; opt.CenX2 = 70; 
    opt.CenY3 = 25; opt.CenX3 = 55; opt.CenY4 = 75; opt.CenX4 = 115;
    opt.NoisePower = -40; % noise power in db
    opt.tolerance = true; opt.scale_var = .2; opt.Scale = .2;
    opt.LargeParmtr = ops.LargeParmtr;
    [I1, I2, I3, I4] = Create_Sample_Data(opt);
    X(:,:,1) = resc(I1); % my_norm(I1);
    X(:,:,2) = resc(I2); % my_norm(I2);
    X(:,:,3) = resc(I3); % my_norm(I3);
    X(:,:,4) = resc(I4);
    [Rw, Col] = size(I2);
    D1 = ([I1(:),I2(:),I3(:),I4(:)]');
    MatA1 = [.4, .65, .35, .85]; % [.35, .45, .55, .75]; % [.65, .35, .85, .4]; %  [1, 1]; % [.3, .45]; % 
    
    A_Pri1 = MatA1';
    if(ops.FixA == false)
        A_Pri = awgn(MatA1',ops.Sigpow,'measured'); 
    else
        A_Pri = MatA1';
    end
    
%     TxrPth = '../../../ImagesTextures/';
    I1 = imread([ops.TxrPth,ops.Txtf1]); % 1.1.02
    I2 = imread([ops.TxrPth,ops.Txtf2]); 
    I3 = imread([ops.TxrPth,ops.Txtf3]); 
    I4 = imread([ops.TxrPth,ops.Txtf4]); 
    x11 = fix((sz(I1,1)-sz(X,1))*rand(1,1))+1;
    x12 = fix((sz(I1,2)-sz(X,2))*rand(1,1))+1;
    x21 = fix((sz(I2,1)-sz(X,1))*rand(1,1))+1;
    x22 = fix((sz(I2,2)-sz(X,2))*rand(1,1))+1;
    x31 = fix((sz(I3,1)-sz(X,1))*rand(1,1))+1;
    x32 = fix((sz(I3,2)-sz(X,2))*rand(1,1))+1;
    x41 = fix((sz(I4,1)-sz(X,1))*rand(1,1))+1;
    x42 = fix((sz(I4,2)-sz(X,2))*rand(1,1))+1;
    Txt(:,:,1) = I1(x11:x11+sz(X,1)-1,x12:x12+sz(X,2)-1);
    Txt(:,:,2) = I2(x21:x21+sz(X,1)-1,x22:x22+sz(X,2)-1);
    Txt(:,:,3) = I3(x31:x31+sz(X,1)-1,x32:x32+sz(X,2)-1);
    Txt(:,:,4) = I4(x41:x41+sz(X,1)-1,x42:x42+sz(X,2)-1);
    Txt = im2double(Txt);
    %%
    spacing.height = ops.height; spacing.width = ops.width; mag = ops.mag;
    for ii = 1:ops.Src,
        [tar(:,:,ii), Tx, Ty] = rBSPwarp( X(:,:,ii), [spacing.height spacing.width], mag );
    end
    src = reshape( (A_Pri'*reshape(tar,Rw*Col,[])')',[Rw,Col,[]]);
    
    temp1 = tar > ops.thresh;
    temp2 = src(:,:,1) > ops.thresh;
    HasOverlap = sum(temp1(:)) > sum(temp2(:));
    
%     temp1 = tar(:,:,1) > ops.thresh;
%     temp2 = tar(:,:,2) > ops.thresh;
%     temp3 = src(:,:,1) > ops.thresh;
%     AllOverlap = (sum(temp1(:)) == sum(temp3(:))) || ...
%         (sum(temp2(:)) == sum(temp3(:)));

%% Check for Variation over Intensities:    
    h = fspecial('gaussian', 5, .5);
    if(ops.isVarIntens == true)
        tar(tar<.1) = 0;
        ind = find(tar(:) > .1);
        NdB	= ops.nVarIntens;
        tar(ind) = awgn(tar(ind),-NdB); % X(ind) + ops.nVarIntensity * randn(size(ind));
        for ii = 1:ops.Src
            tar(:,:,ii) = imfilter(tar(:,:,ii), h);
        end
        tar = resc(tar); % my_norm(tar);
        tar(tar<.1) = 0;
        if(ops.AddPtrn == true)
            tar = tar .* Txt;
        end
        src = reshape( (A_Pri'*reshape(tar,Rw*Col,[])')',[Rw,Col,[]]);
    end
    tar = uint8(tar * 256);
    src = uint8(src*256);
end