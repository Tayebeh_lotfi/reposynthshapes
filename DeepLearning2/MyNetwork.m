clc; close all; clear
imgDir = 'Learn\Image';

imds = imageDatastore(imgDir);
% Display one of the images.

I = readimage(imds, 1);
% I = histeq(I);
figure
imshow(I)

classes = [
    "Obj1"
    "Obj2"
    "Obj3"
    "Obj4"
%     "Obj12"
%     "Obj13"
%     "Obj14"
%     "Obj23"
%     "Obj24"
%     "Obj34"
%     "Obj123"
%     "Obj124"
%     "Obj134"
%     "Obj234"
%     "Obj1234"
    ];

labelIDs = SynthPixelLabelIDs_v1();

labelDir = 'Learn\Label\';

% pxds = mypixelLabelDatastore(labelDir,classes,labelIDs);
pxds = mpixelLabelDatastore(labelDir,classes,labelIDs,'ReadFcn',@customerRead);
% Read and display one of the pixel-labeled images by overlaying it on top of an image.

C = readimage(pxds, 1);

% cmap = SynthColorMap;
% B = labeloverlay(I,C,'ColorMap',cmap);
% 
% figure
% imshow(B)
% pixelLabelColorbar(cmap,classes);

%%
% To see the distribution of class labels in the CamVid dataset, use countEachLabel. This function counts the number of pixels by class label.

% tbl = countEachLabel(pxds)

%%
% C = myreadimage(pxds, 1);
% 


tbl = table();
tbl.Name = pxds.ClassNames; % cellstr(classes);
counts = zeros(1,size(classes,1));
N = zeros(1,size(classes,1));

for ii = 1:size(pxds.Files,1)
%     I = readimage(pxds, ii);
    I = niftiread(pxds.Files{ii});
    pix = numel(I(:,:,1));
    c = 0;
    for jj = 1:size(I,3)
        tmp(jj) = numel(find(I(:,:,jj) > 0 ));
        if (numel(find(I(:,:,jj) == 0 )) < numel(I(:,:,jj)))
            c = c + numel(I(:,:,jj));
        end
    end
    counts = counts + tmp;
    idx = find(tmp);
    N(idx) = N(idx) + c;
end
tbl.PixelCount      = counts';
tbl.ImagePixelCount = N'; 
% 

%%

% Visualize the pixel counts by class.

frequency = tbl.PixelCount/sum(tbl.PixelCount);

figure
bar(1:numel(classes),frequency)
xticks(1:numel(classes))
xticklabels(tbl.Name)
xtickangle(45)
ylabel('Frequency')


%%
imageFolder = 'Learn\imagesResized\';
%imds = resizeImages(imds,imageFolder);
imds = imageDatastore(imageFolder);

% labelFolder = fullfile(outputFolder,'labelsResized',filesep);
labelFolder = 'Learn\labelsResized\';
% pxds = mypixelLabelDatastore(labelFolder,classes,labelIDs);
% pxds = pixelLabelDatastore(labelFolder,classes,labelIDs);
pxds = mpixelLabelDatastore(labelFolder,classes,labelIDs,'ReadFcn',@customerRead);
%pxds = resizePixelLabels(pxds,labelFolder);
%%
[imdsTrain, imdsTest, pxdsTrain, pxdsTest] = partitionData_v2(imds,pxds,'ReadFcn',@customerRead);
% The 60/40 split results in the following number of training and test images:

numTrainingImages = numel(imdsTrain.Files);
numTestingImages = numel(imdsTest.Files);


imageSize = [360 480 3];
numClasses = numel(classes);
lgraph = segnetLayers(imageSize,numClasses,'vgg16');

% As shown earlier, the classes in CamVid are not balanced. To improve training, you can use class weighting to balance the classes. Use the pixel label counts computed earlier with countEachLabel and calculate the median frequency class weights [1].

imageFreq = tbl.PixelCount ./ tbl.ImagePixelCount;
classWeights = median(imageFreq) ./ imageFreq;

% As shown earlier, the classes in CamVid are not balanced. To improve training, 
% you can use class weighting to balance the classes. Use the pixel label counts computed earlier with 
% countEachLabel and calculate the median frequency class weights [1].

imageFreq = tbl.PixelCount ./ tbl.ImagePixelCount;
classWeights = median(imageFreq) ./ imageFreq;

% Specify the class weights using a pixelClassificationLayer.

pxLayer = pixelClassificationLayer('Name','labels','ClassNames', tbl.Name, 'ClassWeights', classWeights);

% Update the SegNet network with the new pixelClassificationLayer by removing the current pixelClassificationLayer 
% and adding the new layer. The current pixelClassificationLayer is named 'pixelLabels'. 
% Remove it using removeLayers, add the new one using|addLayers|, and connect the new layer to the rest of the network using connectLayers.

lgraph = removeLayers(lgraph, 'pixelLabels');
lgraph = addLayers(lgraph, pxLayer);
lgraph = connectLayers(lgraph, 'softmax' ,'labels');

%%
% The optimization algorithm used for training is stochastic gradient decent with momentum (SGDM). Use trainingOptions to specify the hyperparameters used for SGDM.

options = trainingOptions('sgdm', ...
    'Momentum', 0.9, ...
    'InitialLearnRate', 1e-3, ...
    'L2Regularization', 0.0005, ...
    'MaxEpochs', 100, ...
    'MiniBatchSize', 2, ...
    'Shuffle', 'every-epoch', ...
    'VerboseFrequency', 2, ...
    'ExecutionEnvironment', 'gpu');

% Data augmentation is used during training to provide more examples to the network because it helps improve the accuracy of the network. 
% Here, random left/right reflection and random X/Y translation of +/- 10 pixels is used for data augmentation.
% Use the imageDataAugmenter to specify these data augmentation parameters.

augmenter = imageDataAugmenter('RandXReflection',true,...
    'RandXTranslation', [-10 10], 'RandYTranslation',[-10 10]);

% Combine the training data and data augmentation selections using pixelLabelImageSource. The pixelLabelImageSource reads batches of training data, applies data augmentation, and sends the augmented data to the training algorithm.

datasource = pixelLabelImageSource(imdsTrain,pxdsTrain,...
    'DataAugmentation',augmenter);
% Startl training using trainNetwork if the doTraining flag is true. Otherwise, load a pretrained network. Note: Training takes about 5 hours on an NVIDIA� Titan X and can take even longer depending on your GPU hardware.

doTraining = true; % false; % 
if doTraining
    [net, info] = trainNetwork(datasource,lgraph,options);
else
    data = load('pretrainedSegNet\segnetVGG16CamVid');
    net = data.net;
end


% As a quick sanity check, run the trained network on one test image.

I = read(imdsTest);
[~,~,C] = semanticseg(I, net);
% Display the results.
S = C > 0.1;
% 
% B = labeloverlay(I, C, 'Colormap', cmap, 'Transparency',0.4);
% figure
% imshow(B)
% pixelLabelColorbar(cmap, classes);

% Compare the results in C with the expected ground truth stored in pxdsTest. 
% The green and magenta regions highlight areas where the segmentation results differ from the expected ground truth.

expectedResult = read(pxdsTest);
actual = S;
% actual = uint8(C);
expected = uint8(expectedResult);
% imshowpair(actual, expected)

% Visually, the semantic segmentation results overlap well for classes such as road, sky, and building. 
% However, smaller objects like pedestrians and cars are not as accurate. 
% The amount of overlap per class can be measured using the intersection-over-union (IoU) metric, also known as the Jaccard index. 
% Use the jaccard function to measure IoU.

iou = jaccard(C, expectedResult);
table(classes,iou)

% To measure accuracy for multiple test images, run semanticseg on the entire test set.
tempdir = 'Learn\TempRes';
pxdsResults = semanticseg(imdsTest,net,'WriteLocation',tempdir,'Verbose',false);
% semanticseg returns the results for the test set as a pixelLabelDatastore object. 
% The actual pixel label data for each test image in imdsTest is written to disk in the location 
% specified by the 'WriteLocation' parameter. Use evaluateSemanticSegmentation to measure semantic segmentation metrics on the test set results.

metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTest,'Verbose',false);
% evaluateSemanticSegmentation returns various metrics for the entire dataset, for individual classes, 
% and for each test image. To see the dataset level metrics, inspect metrics.DataSetMetrics .

metrics.DataSetMetrics

% The dataset metrics provide a high-level overview of the network performance. To see the impact each class has on the overall performance, 
% inspect the per-class metrics using metrics.ClassMetrics.

metrics.ClassMetrics
save('mynet','net')
% 
% 
% 'ExecutionEnvironment' � Hardware resource for training network
% 'auto' (default) | 'cpu' | 'gpu' | 'multi-gpu' | 'parallel'
% 

