function pxds = resizePixelLabels(pxds, labelFolder)
% Resize pixel label data to [360 480].
classes = pxds.ClassNames;
while hasdata(pxds)
    % Read the pixel data.
    [C,info] = read(pxds);

    % Convert from categorical to uint8.
    L = uint8(C);

    % Resize the data. Use 'nearest' interpolation to
    % preserve label IDs.
    L = imresize(L,[360 480],'nearest');

    % Write the data to disk.
    [~, filename, ext] = fileparts(info.Filename);
    imwrite(L,[labelFolder filename ext])
end

labelIDs = 1:numel(classes);
pxds = pixelLabelDatastore(labelFolder,classes,labelIDs);
end
