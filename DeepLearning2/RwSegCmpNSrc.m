clc, close all, clear,
addpath(genpath('../Code'));
%% generate data
opt.FixA = false; opt.Src = 4; opt.thresh = .25; opt.nVarIntens = -45; % 20; % 
opt.isVarIntens = true; opt.Sigpow = 15; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 25; opt.width = 25; opt.mag = 12; 
opt.TxrPth = '../ImageTextures/'; 
opt.Txtf1 = '1.3.03.tiff'; 
opt.Txtf2 = '1.3.06.tiff'; 
opt.Txtf3 = '1.3.09.tiff'; 
opt.Txtf4 = '1.3.11.tiff';
for ii = 1:700
    [A_Pri, ~, X, Y, HasOverlap] = Create_Samples_(opt);
    % [A_Pri, ~, X, Y, HasOverlap] = Create_Samples(opt);
    while(HasOverlap ==0)
        [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);
    %     [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);

    end

%     img=im2double(Y);
    img = repmat(Y,[1,1,3]);
    Srcs = X;
    % figure, imshow(Y,[])
    Lbls = find_lables(Srcs);
    image(:,:,ii) = Y;
    src(:,:,:,ii) = X;
    labels(:,:,:,ii) = Lbls;
    imwrite(img,['Learn\Image\Img',num2str(ii),'.tiff']);
    imwrite(Lbls,['Learn\Label\Lbl',num2str(ii),'.tiff']);
end

save('Learn\learn.mat','image','src','labels','-v7.3');