function cmap = SynthColorMap()
% Define the colormap used by CamVid dataset.

cmap = [
    128 128 128  % "Bckgrnd"
    000 128 064  % "Obj1"
    128 000 000  % "Obj2"
    064 192 000  % "Obj3"
    064 000 064  % "Obj4"
    192 000 128  % "Obj12"
    192 192 128  % "Obj13"
    000 000 064  % "Obj14"
    128 064 128  % "Obj23"
    128 000 192  % "Obj24"
    192 000 064  % "Obj34"
    000 000 192  % "Obj123"
    064 192 128  % "Obj124"
    128 128 192  % "Obj134"
    128 128 000  % "Obj234"
    192 192 000  % "Obj1234"   
   ];
% Normalize between [0 1].
cmap = cmap ./ 255;
end