clc, close all, clear,
addpath(genpath('../Code'));
%% generate data
opt.FixA = false; % true; %  
opt.Src = 4; opt.thresh = .25; opt.nVarIntens = -45; Ntrial = 20; % 1; % 20; % 1; % 
opt.isVarIntens = true; opt.Sigpow = 15; opt.initA = true; opt.LargeParmtr = false; % true; % 
opt.AddPtrn = true; opt.height = 25; opt.width = 25; opt.mag = 12; showit = 0; thrsh = .25;
opt.TxrPth = '../ImageTextures/'; 
opt.Txtf1 = '1.3.03.tiff'; 
opt.Txtf2 = '1.3.06.tiff'; 
opt.Txtf3 = '1.3.09.tiff'; 
opt.Txtf4 = '1.3.11.tiff';
seg_cnt = zeros(Ntrial,1);

load myownnet
for iter = 1:Ntrial

    [A_Pri, ~, X, Y, HasOverlap] = Create_Samples_(opt);
    while(HasOverlap ==0)
        [A_Pri, A_Pri_, X, Y, HasOverlap] = Create_Samples_(opt);
    end

    img = repmat(Y,[1,1,3]);
    Srcs = X;
    [Xc_,Yc_,NObj] = size(Srcs);
    Lbls = find_lables(Srcs);
    %% Ground Truth Seg
    seg(:,:,1) = (Srcs(:,:,1)==0 & Srcs(:,:,2)==0 & Srcs(:,:,3)==0 & Srcs(:,:,4)==0);
    seg(:,:,2) = Srcs(:,:,1)>0;
    seg(:,:,3) = Srcs(:,:,2)>0;
    seg(:,:,4) = Srcs(:,:,3)>0;
    seg(:,:,5) = Srcs(:,:,4)>0;
    %% Learning Seg
    I = img;
    [~,~,C] = semanticseg(I, net);

    [Xc,Yc,~] = size(C);
    res = zeros(Xc_,Yc_,NObj+1);
    for ii = 1:NObj
        C_(:,:,ii) = imresize(C(:,:,ii),[Xc_,Yc_]);
    end
    for ii = 2:NObj+1
        res(:,:,ii) = C_(:,:,ii-1)>thrsh;
        res(:,:,1) = res(:,:,1) | res(:,:,ii);
    end
    res(:,:,1) = 1 - res(:,:,1);
    for ii = 2:NObj+1
        gmsk = seg(:,:,ii)>0;
        rmsk = res(:,:,ii)>0;
        seg_cnt(iter) = seg_cnt(iter) + (sz( find(gmsk~=rmsk ) , 1) );
    end
    ovlp = (sz(find(seg(:,:,1)==0),1));
    acc(iter) = 100 - seg_cnt(iter) / (NObj*Xc_*Yc_)*100;
    iou(iter) = 100 - seg_cnt(iter) / (NObj*ovlp)*100;
%% Compare
    if(showit==1)
        figure,
        for ii = 1:5
            subplot(3,5,ii), imshow(res(:,:,ii)), if(ii==1), title('Segmentation (Deep Learning)'), end
            subplot(3,5,ii+5), imshow(seg(:,:,ii)), if(ii==1), title('Ground Truth'), end
            if(ii<5), subplot(3,5,ii+11), imshow(Srcs(:,:,ii)), end
        end
        subplot(3,5,11), imshow(img), title('Mixture and Original Sources')
    end
end