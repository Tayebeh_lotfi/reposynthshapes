clc, close all, clear,
addpath(genpath('../Code'));
load learn.mat
for ii = 1:size(image,3)
    Lbl(:,:,1) = 1*uint8(src(:,:,1,ii)>0); % 10*uint8(src(:,:,1,ii)>0);
    Lbl(:,:,2) = 2*uint8(src(:,:,2,ii)>0); % 50*uint8(src(:,:,2,ii)>0);
    Lbl(:,:,3) = 3*uint8(src(:,:,3,ii)>0); % 100*uint8(src(:,:,3,ii)>0);
    Lbl(:,:,4) = 4*uint8(src(:,:,4,ii)>0); % 150*uint8(src(:,:,4,ii)>0);
    img = repmat(image(:,:,ii),[1,1,3]);
    imres = imresize(img,[360 480]);
    Lblres(:,:,1) = imresize(Lbl(:,:,1),[360 480]);
    Lblres(:,:,2) = imresize(Lbl(:,:,2),[360 480]);
    Lblres(:,:,3) = imresize(Lbl(:,:,3),[360 480]);
    Lblres(:,:,4) = imresize(Lbl(:,:,4),[360 480]);

    imwrite(img,['Learn\Image\Img',num2str(ii),'.tiff']);
    imwrite(imres,['Learn\imagesResized\Img',num2str(ii),'.tiff']);
    niftiwrite(Lbl,['Learn\Label\Lbl',num2str(ii),'.nii']);
    niftiwrite(Lblres,['Learn\labelsResized\Lbl',num2str(ii),'.nii']);
end